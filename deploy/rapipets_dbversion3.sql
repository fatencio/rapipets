-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 16-08-2017 a las 16:37:30
-- Versión del servidor: 5.5.51-38.2
-- Versión de PHP: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rapipets_dbversion3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
  `animal_id` int(11) NOT NULL,
  `animal_nombre` varchar(255) DEFAULT NULL,
  `animal_conraza` tinyint(4) NOT NULL DEFAULT '0',
  `animal_contamanios` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `animal`
--

INSERT INTO `animal` (`animal_id`, `animal_nombre`, `animal_conraza`, `animal_contamanios`) VALUES
(1, 'Perro', 1, 1),
(2, 'Gato', 1, 0),
(3, 'Roedores', 0, 0),
(4, 'Peces', 0, 0),
(5, 'Aves', 0, 0),
(6, 'Reptiles', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo`
--

CREATE TABLE IF NOT EXISTS `articulo` (
  `articulo_id` int(11) NOT NULL,
  `articulo_marca_id` int(11) DEFAULT NULL,
  `articulo_codigo` text NOT NULL,
  `articulo_rubro_id` int(11) NOT NULL,
  `articulo_detalle` text NOT NULL,
  `articulo_medicados` tinyint(4) DEFAULT NULL,
  `articulo_nombre` text NOT NULL,
  `articulo_animal_id` int(11) NOT NULL,
  `articulo_edad` varchar(50) NOT NULL,
  `articulo_imagen` varchar(255) NOT NULL DEFAULT 'noimage.gif',
  `articulo_creador` varchar(9) NOT NULL DEFAULT 'GeoTienda'
) ENGINE=InnoDB AUTO_INCREMENT=655 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo`
--

INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(159, 66, 'A01', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Cachorros</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Calcio, Antioxidantes, Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'V50 Junior Razas Pequeñas', 1, 'Cachorros', '_151177498.png', 'GeoTienda'),
(160, 66, 'A02', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:</span><br />\r\n<span style="color:#FF0000">* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Cachorros</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Medianos y Grandes</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Calcio, Antioxidantes, Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'V50 Junior Razas Medianas y Grandes', 1, 'Cachorros', '_378636118.png', 'GeoTienda'),
(161, 66, 'A03', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Todas</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Omega 3 y 6, Antioxidantes, L- Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'V37 Sobrepeso', 1, 'Adultos', '_250386375.png', 'GeoTienda'),
(162, 66, 'A04', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Omega 3 y 6, Antioxidantes, Minerales, Vitaminas.</span></span></p>\r\n', NULL, 'V35 Adulto Mantenimiento Razas Pequeñas', 1, 'Adultos', '_555339862.png', 'GeoTienda'),
(163, 66, 'A05', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Medianas y Grandes</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Omega 3 y 6, Antioxidantes, Minerales, Vitaminas.</span></span></p>\r\n', NULL, 'V35 Adulto Mantenimiento Razas Medianas y Grandes', 1, 'Adultos', '_144798975.png', 'GeoTienda'),
(164, 66, 'A06', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Cachorros</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas del huevo y la leche, Desarrollo dental y &oacute;seo, Modulacion de las defensas.</span></span></p>\r\n', NULL, 'Balanced Puppy Small Breed', 1, 'Cachorros', '_97226533.png', 'GeoTienda'),
(165, 66, 'A07', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Cachorros</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Medianas</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas del huevo y la leche, Desarrollo dental y &oacute;seo, Modulacion de las defensas.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Balanced Puppy Medium Breed', 1, 'Cachorros', '_184233090.png', 'GeoTienda'),
(166, 66, 'A08', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Cachorros</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Grandes</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas del huevo y la leche, Modulacion de las defensas, Desarrollo dental y &oacute;seo, Salud osteoarticular.</span>&nbsp;</span></p>\r\n', NULL, 'Balanced Puppy Large Breed', 1, 'Cachorros', '_320050538.png', 'GeoTienda'),
(167, 66, 'A09', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium&nbsp;</span><br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">Modulacion de las defensas,&nbsp;M&uacute;sculos fuertes, Control del sarro y la halitosis, Cuidado &oacute;seo.&nbsp;</span></span></p>\r\n', NULL, 'Balanced Adult Dog Small Breed', 1, 'Adultos', '_541675995.png', 'GeoTienda'),
(168, 66, 'A10', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Medianas</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">Modulacion de las defensas,&nbsp;M&uacute;sculos fuertes, Control del sarro y la halitosis, Cuidado &oacute;seo.</span></span>&nbsp;</p>\r\n', NULL, 'Balanced Adult Dog Medium Breed', 1, 'Adultos', '_985885654.png', 'GeoTienda'),
(169, 66, 'A11', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Grandes</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:&nbsp;</span><span style="color:#000000">Modulacion de las defensas, M&uacute;sculos fuertes, Control del sarro y la halitosis, Salud osteoarticular.</span></span></p>\r\n', NULL, 'Balanced Adult Dog Large Breed', 1, 'Adultos', '_796635840.png', 'GeoTienda'),
(170, 66, 'A12', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Edad:</span>&nbsp;<span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Gigantes</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">Modulacion de las defensas,&nbsp;M&uacute;sculos fuertes, Control del sarro y la halitosis, Salud &oacute;steoarticular.</span>&nbsp;</span></p>\r\n', NULL, 'Balanced Adult Dog Giant Breeds', 1, 'Adultos', '_723953871.png', 'GeoTienda'),
(171, 66, 'A13', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alta calidad, Salud &oacute;steoarticular, Modulaci&oacute;n de las defensas, Salud renal, Salud card&iacute;aca, Control del sarro y la halitosis.</span></span></p>\r\n', NULL, 'Senior Small Breed', 1, 'Senior', '_425792918.png', 'GeoTienda'),
(172, 66, 'A14', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alta calidad, Salud &oacute;steoarticular, Modulaci&oacute;n de las defensas, Salud renal, Salud card&iacute;aca, Control del sarro y la halitosis.</span></span></p>\r\n', NULL, 'Senior Medium Breed', 1, 'Senior', '_600301940.png', 'GeoTienda'),
(173, 66, 'A15', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alta calidad, Salud &oacute;steoarticular, Modulaci&oacute;n de las defensas, Salud renal, Salud card&iacute;aca, Control del sarro y la halitosis.</span></span></p>\r\n', NULL, 'Senior Large Breed', 1, 'Senior', '_455825549.png', 'GeoTienda'),
(174, 66, 'A16', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajo en calor&iacute;as, Metabolismo de las grasas, M&uacute;sculos fuertes, Salud Card&iacute;aca, Salud &oacute;steoraticular, Control de sarro y la halitosis.</span></span></p>\r\n', NULL, 'Control de Peso', 1, 'Adultos', '_665758884.png', 'GeoTienda'),
(175, 66, 'A17', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima nutrici&oacute;n y gran digestibilidad, Fortalecimiento del sistema inmunol&oacute;gico, Piel sana y mayor brillo del pelaje.</span></span></p>\r\n', NULL, 'Premium Adult', 1, 'Adultos', '_427998608.png', 'GeoTienda'),
(176, 66, 'A18', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima nutrici&oacute;n y gran digestibilidad, Fortalecimiento del sistema inmunol&oacute;gico, Piel sana y mayor brillo del pelaje, Desarrollo y crecimiento muscular.</span></span></p>\r\n', NULL, 'Premium Puppy', 1, 'Cachorros', '_553031026.png', 'GeoTienda'),
(177, 66, 'A19', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptimo crecimiento, Estimulantes de defensas, Pelo brillante.</span></span></p>\r\n', NULL, 'Complete Junior Razas Pequeñas', 1, 'Cachorros', '_377934715.png', 'GeoTienda'),
(178, 66, 'A20', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 18 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptimo crecimiento, Estimulantes de defensas, Pelo brillante.</span></span></p>\r\n', NULL, 'Complete Junior Razas Medianas y Grandes', 1, 'Cachorros', '_540857622.png', 'GeoTienda'),
(179, 66, 'A21', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor vitalidad, Pelo brillante, Sabor rico y natural Irresistible.</span></span></p>\r\n', NULL, 'Complete Adult Raza Pequeña (Carne)', 1, 'Adultos', '_841691782.png', 'GeoTienda'),
(180, 66, 'A22', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor vitalidad, Pelo brillante, Sabor rico y natural Irresistible.</span></span></p>\r\n', NULL, 'Complete Adult Raza Pequeña (Pollo)', 1, 'Adultos', '_539294039.png', 'GeoTienda'),
(181, 66, 'A23', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor vitalidad, Pelo brillante, Sabor rico y natural.</span></span></p>\r\n', NULL, 'Complete Adult Raza Medianas y Grandes (Carne)', 1, 'Adultos', '_405750383.png', 'GeoTienda'),
(182, 67, 'A24', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">De 1 a&nbsp;2 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Todas</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, (DHA,EPA).</span></span></p>\r\n', NULL, 'Puppy Complete', 1, 'Cachorros', '_94555255.png', 'GeoTienda'),
(183, 67, 'A25', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Hasta 2 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Grandes</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, (DHA, EPA).</span></span></p>\r\n', NULL, 'Puppy Large Breed', 1, 'Cachorros', '_226838053.png', 'GeoTienda'),
(184, 67, 'A26', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Hasta 1 a&ntilde;o</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, (DHA, EPA).</span></span></p>\r\n', NULL, 'Puppy Small Breed', 1, 'Cachorros', '_815529235.png', 'GeoTienda'),
(185, 67, 'A27', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">De 2 a 7 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Todas</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adult Dog Complete', 1, 'Adultos', '_744396319.png', 'GeoTienda'),
(186, 67, 'A28', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, Calcio y F&oacute;sforo, Acidos grasos EPA.</span></span></p>\r\n', NULL, 'Adult Dog Large Breed', 1, 'Adultos', '_235612337.png', 'GeoTienda'),
(187, 67, 'A29', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">De 1 a 7 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Minerales, Calcio y F&oacute;sforo,&nbsp;Taurina (Promueve&nbsp;un coraz&oacute;n fuerte y saludable).</span></span></p>\r\n', NULL, 'Adult Dog Small Breed', 1, 'Adultos', '_226743491.png', 'GeoTienda'),
(188, 67, 'A30', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span>&nbsp;<span style="color:#000000">Medianas y Grandes</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Salmon y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitamina (C, E), Minerales, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Sensitive Skin Dog Adult Razas Medianas y Grandes', 1, 'Adultos', '_552728981.png', 'GeoTienda'),
(189, 67, 'A31', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span>&nbsp;<span style="color:#000000">Medianas y Grandes</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">20% menos de calorias,&nbsp;Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitamina (C, E), Minerales, Isoflavonas (Antioxidantes naturales)<br />\r\n&nbsp;(Antioxidantes naturales)</span></span></p>\r\n', NULL, 'Reduced Calorie Dog Razas Medianas y Grandes', 1, 'Adultos', '_700502359.png', 'GeoTienda'),
(190, 67, 'A32', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Salmon y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Vitaminas (A, D), Zinc, Omega 3 y 6 (Piel y pelo saludable), Calcio, Magnesio.</span></span>&nbsp;<br />\r\n&nbsp;</p>\r\n', NULL, 'Delicate Dog Small Breed', 1, 'Adultos', '_605474620.png', 'GeoTienda'),
(191, 67, 'A33', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Prote&iacute;nas, Vitaminas, Antioxidantes, Amino&aacute;cidos</span>.</span></p>\r\n', NULL, 'Exigent Small Breed', 1, 'Adultos', '_830619538.png', 'GeoTienda'),
(192, 67, 'A34', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Mayor a 7 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span>&nbsp;<span style="color:#000000">Medianas y Grandes</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">28% Prote&iacute;nas, 12% grasas, Contiene (EPA) Omega 3 y Glucosamina, (Mantiene las articulaciones saludables) Formulado con Antioxidandes, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Active Mind Adult 7+ Razas Medianas y Grandes', 1, 'Adultos', '_306384313.png', 'GeoTienda'),
(193, 67, 'A35', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Mayor a 7 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span><br />\r\n<span style="color:#FF0000">* Sabor:</span> <span style="color:#000000">Pollo y Arroz</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">29% Prote&iacute;nas, 15% Grasa, Contiene (EPA) Omega 3 y Glucosamina, (Mantiene las articulaciones saludables) Formulado con Antioxidandes, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Active Mind Adult 7+ Razas Pequeñas', 1, 'Adultos', '_451950681.png', 'GeoTienda'),
(194, 67, 'A36', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad:</span> <span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span><br />\r\n<span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Todas</span>&nbsp;<br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Moderado contenido en grasas y fibras, Altamente digerible,&nbsp;Niveles adecuados de &aacute;cidos Omega 3 y 6 (Piel y pelo saludable).</span><br />\r\n<span style="color:#FF0000">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span>&nbsp;</p>\r\n', 1, 'Veterinary Diets En Gastroenteric', 1, 'Todas', '_435829698.png', 'GeoTienda'),
(195, 67, 'A37', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad: </span><span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza: </span><span style="color:#000000">Todas</span><span style="color:#FF0000">&nbsp;<br />\r\n* Analisis Nutricional:</span> <span style="color:#000000">Reducido en sodio, disminuye la hipertensi&oacute;n. Cantidad limitada de prote&iacute;na de alta calidad&nbsp;que ayuda a reducir la p&eacute;rdida de masa corporal magra. Bajo en f&oacute;sforo para ayudar al funcionamiento renal. Niveles adecuados de Omega 3 y 6&nbsp;que ayudan a reducir la hipertensi&oacute;n glomerular y a minimizar&nbsp;la&nbsp;inflamaci&oacute;n&nbsp;glomerular.</span>&nbsp;<br />\r\n<span style="color:#FF0000">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Veterinary Diets NF Kidney Function', 1, 'Adultos', '_583616850.png', 'GeoTienda'),
(196, 67, 'A38', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:</span><br />\r\n<span style="color:#FF0000">* Calidad: </span><span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">Adultos</span><br />\r\n<span style="color:#FF0000">* Raza: </span><span style="color:#000000">Todas</span><span style="color:#FF0000">&nbsp;<br />\r\n* Analisis Nutricional: </span><span style="color:#000000">Bajo en calor&iacute;as y grasas. Alto contenido proteico. Con fibra&nbsp;natural. Contiene Isoflavonas, que ayudan en la reducci&oacute;n de la acumulaci&oacute;n de peso y grasa.</span><span style="color:#FF0000">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span>&nbsp;</p>\r\n', 1, 'Veterinary Diets OM Overweight Management', 1, 'Adultos', '_558270167.png', 'GeoTienda'),
(197, 68, 'A39', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Crecimiento saludable, Intestino protegido, Alta digestibilidad.</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Puppy Small Breed', 1, 'Cachorros', '_575052616.png', 'GeoTienda'),
(199, 68, 'A40', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 10 meses hasta los 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alta digestibilidad,&nbsp;Omega 3 y 6, (Piel y pelo saludable), Vitaminas (A, C, E), Acido f&oacute;lico, Zinc y cobre</span></span><span style="color:#000000"><span style="font-size:16px">, Proteccion dental.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Adult Small Breed', 1, 'Adultos', '_744486163.png', 'GeoTienda'),
(200, 68, 'A41', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional:&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">Sulfato de Condroitina, Glucosamina (Articulaciones protegidas), Alta digestibilidad (Proteinas), Taurina y L-carnitina&nbsp;(Reduccion de grasas) (Salud Cardiovascular), Intestino sadulable.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Senior Small Breed ', 1, 'Senior', '_484519171.png', 'GeoTienda'),
(201, 68, 'A42', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Crecimiento saludable, Alta digestibilidad, Pelaje suave y brillante.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Puppy Medium Breed  ', 1, 'Cachorros', '_594455376.png', 'GeoTienda'),
(202, 68, 'A43', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 12 meses hasta los 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Pelaje suave y brillante, Proteccion dental, Articulaciones protegidas.</span></span><br />\r\n&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Adult Medium Breed ', 1, 'Adultos', '_369804217.png', 'GeoTienda'),
(204, 68, 'A44', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 7&nbsp;a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Articulaciones protegidas, Alta digestibilidad, Salud cardiovascular, Intestino saludable.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Senior Medium Breed ', 1, 'Senior', '_969416643.png', 'GeoTienda'),
(205, 68, 'A45', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 18 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Crecimiento optimo, Alta digestibilidad, Articulaciones protegidas, Pelaje suave y brillante.</span></span></p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, 'Puppy Large Breed ', 1, 'Cachorros', '_414406091.png', 'GeoTienda'),
(206, 68, 'A46', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 18 meses hasta los 6&nbsp;a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Articulaciones protegidas, Protecion dental, Pelaje suave y brillante, Defensas naturales reforzadas.</span></span></p>\r\n', NULL, 'Adult Large Breed ', 1, 'Adultos', '_348605979.png', 'GeoTienda'),
(207, 68, 'A47', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 6&nbsp;a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Articulaciones protegidas, Alta digestibilidad, Salud cardiovascular, Intestino saludable.</span></span></p>\r\n', NULL, 'Senior Large Breed  ', 1, 'Senior', '_509308721.png', 'GeoTienda'),
(208, 69, 'A48', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Leche y Calcio</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteina, Leche, Calcio, Vitaminas, Minerales.</span></span></p>\r\n', NULL, 'Selección Cachorros (Carne, Leche y Calcio) ', 1, 'Cachorros', '_342051583.png', 'GeoTienda'),
(209, 69, 'A49', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo, Arroz y Cereales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Hidratos de carbono, Fibras, Omega (3, 6 y 9)</span></span></p>\r\n', NULL, 'Selección (Pollo, Arroz y Cereales)', 1, 'Adultos', '_812956787.png', 'GeoTienda'),
(210, 69, 'A50', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Hidratos de carbono, Fibras, Omega (3, 6 y 9)</span></span></p>\r\n', NULL, 'Selección (Carne y Vegetales) ', 1, 'Adultos', '_18796728.png', 'GeoTienda'),
(211, 69, 'A51', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Hidratos de carbono, Fibras, Omega (3, 6 y 9)</span></span></p>\r\n', NULL, 'Selección (Carne)', 1, 'Adultos', '_286729422.png', 'GeoTienda'),
(212, 70, 'A52', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Bulldog Franc&eacute;s</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas (C, E), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Bulldog Francés Adult', 1, 'Adultos', '_928608145.png', 'GeoTienda'),
(213, 70, 'A53', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Caniche Poodle</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Manano-Oligosac&aacute;ridos (MOS), Antioxidantes, Vitaminas (C, E), Lute&iacute;na y Taurina.</span></span></p>\r\n', NULL, 'Poodle 33 Junior', 1, 'Cachorros', '_901224102.png', 'GeoTienda'),
(214, 70, 'A54', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De mas de 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Caniche Poodle</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitaminas (C, E), Lute&iacute;na y Taurina, Zeaxantina (Soporte de la vision).</span></span></p>\r\n', NULL, 'Poodle 30 Adult', 1, 'Adultos', '_829781688.png', 'GeoTienda'),
(215, 70, 'A55', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Yorkshire Terrier</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas, Calcio y F&oacute;sforo, Manano-Oligosac&aacute;ridos (MOS) y Antioxidantes, Vitaminas (C, E), Lute&iacute;na y Taurina.</span></span></p>\r\n', NULL, 'Yorkshire Terrir 29 Junior', 1, 'Cachorros', '_647383185.png', 'GeoTienda'),
(216, 70, 'A56', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Yorkshire Terrier</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas, Calcio y F&oacute;sforo, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina.</span></span></p>\r\n', NULL, 'Yorkshire Terrier 28 Adult', 1, 'Adultos', '_528795398.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(217, 70, 'A57', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 8 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Chihuahua</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina.</span></span></p>\r\n', NULL, 'Chihuahua 28 Adult', 1, 'Adultos', '_178519343.png', 'GeoTienda'),
(218, 70, 'A58', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Teckel</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, EPA-DHA (con efecto anti-inflamatorio),</span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Dachshund 28 Adult', 1, 'Adultos', '_107452357.png', 'GeoTienda'),
(219, 70, 'A59', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Schnauzer Miniature</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Miniature Schnauzer 25 Adult', 1, 'Adultos', '_618205599.png', 'GeoTienda'),
(220, 70, 'A60', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Bulldog</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, 4 Vitaminas del grupo B, Omega 3 (EPA y DHA)</span></span><span style="color:#000000"><span style="font-size:16px">, &nbsp;Antioxidantes, Vitaminas (C, E), Lute&iacute;na y Taurina, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Bulldog 24 Adult', 1, 'Adultos', '_775558734.png', 'GeoTienda'),
(221, 70, 'A61', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Ovejero&nbsp;Alem&aacute;n</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:rgb(0, 0, 0); font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, mananooligosac&aacute;ridos (MOS),</span>&nbsp;<span style="color:#000000"><span style="font-size:16px">Antioxidantes, Vitaminas (C, E), Lute&iacute;na y Taurina, Condroitina y Glucosamina (Articulaciones saludables).</span></span></p>\r\n', NULL, 'German Shepherd 30 Junior', 1, 'Cachorros', '_620746390.png', 'GeoTienda'),
(222, 70, 'A62', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Ovejero&nbsp;Alem&aacute;n</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina, Condroitina y Glucosamina (Articulaciones saludables).</span></span></p>\r\n', NULL, 'German Shepherd 24 Adult', 1, 'Adultos', '_754573179.png', 'GeoTienda'),
(223, 70, 'A63', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad: </span><span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">De 2 a 15 meses</span><br />\r\n<span style="color:#FF0000">* Raza: </span><span style="color:#000000">Labrador Retriever</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional: </span><span style="color:#000000">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, Antioxidantes,&nbsp;Vitaminas (C, E), Lute&iacute;na y Taurina, L-Carnitina (Reduccion de grasas), Condroitina y Glucosamina (Articulaciones saludables), (EPA-DHA).&nbsp;</span></span></p>\r\n', NULL, 'Labrador Retriever 33 Junior', 1, 'Cachorros', '_365962603.png', 'GeoTienda'),
(224, 70, 'A64', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Labrador Retriever</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Calcio, F&oacute;sforo, Omega 3 Antioxidantes,&nbsp;Vitaminas (A, B), Zinc y Amino&aacute;cidos, Condroitina y Glucosamina (Articulaciones saludables), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Labrador Retriever 30 Adult', 1, 'Adultos', '_681339927.png', 'GeoTienda'),
(225, 70, 'A65', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Golden Retriever</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, F&oacute;sforo, Omega 3 (EPA-DHA), Antioxidantes,&nbsp;Vitamina (C, E), Taurina, Lute&iacute;na, Condroitina y Glucosamina (Articulaciones saludables), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Golden Retriever 29 Junior', 1, 'Cachorros', '_886331737.png', 'GeoTienda'),
(226, 70, 'A66', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Golden Retriever</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Calcio, F&oacute;sforo, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes,&nbsp;Vitaminas, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Golden Retriever 25 Adult', 1, 'Adultos', '_191971316.png', 'GeoTienda'),
(227, 70, 'A67', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 2 a 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Boxer</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Calcio, F&oacute;sforo, Antioxidantes,&nbsp;Vitamina (C, E), Taurina, Lute&iacute;na, Condroitina y Glucosamina (Articulaciones saludables), (EPA-DHA), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Boxer 30 Junior', 1, 'Cachorros', '_894310796.png', 'GeoTienda'),
(228, 70, 'A68', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De m&aacute;s de 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Boxer</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas 26%&nbsp;altamente digestibles , L-Carnitina (Reduccion de grasas), Antioxidantes, Vitamina (C, E), Taurina, Sulfato de Condroitina y Glucosamina (Articulaciones saludables), Omega 3 (EPA-DHA)</span></span></p>\r\n', NULL, 'Boxer 26 Adult', 1, 'Adultos', '_607682704.png', 'GeoTienda'),
(229, 70, 'A69', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Muy Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, Antioxidantes, Manano-Oligosac&aacute;ridos (Defensas naturales).</span></span></p>\r\n', NULL, 'X-Small Junior', 1, 'Cachorros', '_467329639.png', 'GeoTienda'),
(230, 70, 'A70', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Muy Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Omega 3 y 6 (Piel y pelo saludable), Vitaminas y Amino&aacute;cidos.</span></span></p>\r\n', NULL, 'X-Small Adult', 1, 'Adultos', '_861259728.png', 'GeoTienda'),
(231, 70, 'A71', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 2 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Muy Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Final de gestaci&oacute;n y lactancia, Ideal para el destete, F&aacute;cil rehidrataci&oacute;n, <strong>START COMPLEX</strong> (Seguridad digestiva, Defensas naturales)</span></span></p>\r\n', NULL, 'Mini Starter', 1, 'Cachorros', '_159128961.png', 'GeoTienda'),
(232, 70, 'A72', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Calcio, Antioxidantes.</span></span></p>\r\n', NULL, 'Mini Junior', 1, 'Cachorros', '_677830551.png', 'GeoTienda'),
(233, 70, 'A73', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 10 meses hasta los 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (27%), L-Carnitina (Reduccion de grasas), (EPA-DHA), Calcio&nbsp;(politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Mini Adult', 1, 'Adultos', '_654887481.png', 'GeoTienda'),
(234, 70, 'A74', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 10 meses hasta los 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Muy Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, Antioxidantes, Vitamina (E, C), Lute&iacute;na, Taurina, L-Carnitina (Reduccion de grasas), Calcio&nbsp;(politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Mini Indoor Adult', 1, 'Adultos', '_678133435.png', 'GeoTienda'),
(235, 70, 'A75', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 8 a&ntilde;os hasta los 12 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (27%), Antioxidantes, L-Carnitina (Reduccion de grasas), Calcio&nbsp;(politrifosfato de sodio).</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Mini Adult 8+', 1, 'Adultos', '_804828695.png', 'GeoTienda'),
(236, 70, 'A76', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 12 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Piel saludable y un pelaje brillante</span>&nbsp;<span style="font-size:16px">(EPA-DHA), Antioxidantes, Fosforo, Calcio&nbsp;(politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Mini Ageing 12+', 1, 'Senior', '_991022564.png', 'GeoTienda'),
(237, 70, 'A77', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Antioxidantes, Omega 3 y 6, (EPA-DHA), (Piel y pelo saludable), Amino&aacute;cido y 4 vitaminas del grupo B,&nbsp;Calcio&nbsp;(politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Mini Dermacomfort', 1, 'Adultos', '_682257890.png', 'GeoTienda'),
(238, 70, 'A78', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 10 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (30%) moderado aporte de grasas (11%), L-Carnitina (Reduccion de grasas), Calcio&nbsp;(politrifosfato de sodio).</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Mini Weight Care', 1, 'Adultos', '_790704633.png', 'GeoTienda'),
(239, 70, 'A79', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 2 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Final de gestaci&oacute;n y lactancia, Ideal para el destete, F&aacute;cil rehidrataci&oacute;n, <strong>START COMPLEX</strong> (Seguridad digestiva&nbsp;- Defensas naturales).</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Medium Starter', 1, 'Cachorros', '_164173449.png', 'GeoTienda'),
(240, 70, 'A80', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (L.I.P) altamente digestibles, prebi&oacute;ticos (FOS-MOS), Calcio, F&oacute;sforo, Antioxidantes, Vitamina (E, C), Lute&iacute;na y Taurina.</span></span></p>\r\n', NULL, 'Medium Junior', 1, 'Cachorros', '_483673667.png', 'GeoTienda'),
(241, 70, 'A81', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 12 meses hasta los 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Antioxidantes, Vitamina (E, C), Lute&iacute;na y Taurina, Omega 3 (EPA-DHA).</span></span></p>\r\n', NULL, 'Medium Adult', 1, 'Adultos', '_635992632.png', 'GeoTienda'),
(242, 70, 'A82', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 7 hasta los 10 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Antioxidantes, Omega 3 (EPA-DHA), Calcio (politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Medium Adult 7+', 1, 'Senior', '_265513324.png', 'GeoTienda'),
(243, 70, 'A83', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 10 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas y fibras altamente digestibles, Antioxidantes, Omega 3 (EPA-DHA), Calcio (Politrifosfato de sodio).</span></span></p>\r\n', NULL, 'Medium Ageing 10+', 1, 'Senior', '_516443333.png', 'GeoTienda'),
(244, 70, 'A84', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Omega 3, 6&nbsp;(Piel y pelo saludable)(EPA-DHA), Antioxidantes, Amino&aacute;cido y 4 vitaminas del grupo B.</span></span></p>\r\n', NULL, 'Medium Dermacomfort', 1, 'Adultos', '_355264044.png', 'GeoTienda'),
(245, 70, 'A85', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="font-size:16px"><span style="color:#000000">Prote&iacute;nas altamente digestibles y bajo nivel de grasas, Enriquecido con L-Carnitina (Reduccion de grasas), Complejo de Antioxidantes.</span>&nbsp;</span></p>\r\n', NULL, 'Medium Weight Care', 1, 'Adultos', '_31183755.png', 'GeoTienda'),
(246, 70, 'A86', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 2 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Final de gestaci&oacute;n y lactancia, Ideal para el destete, F&aacute;cil rehidrataci&oacute;n, <strong>START COMPLEX:</strong> (Seguridad digestiva&nbsp;- Defensas naturales).</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Maxi Starter', 1, 'Cachorros', '_148820751.png', 'GeoTienda'),
(247, 70, 'A87', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (L.I.P) Altamente digestibles, Prebi&oacute;ticos (FOS-MOS),&nbsp;<br />\r\n&nbsp; Calcio, Condroitina y Glucosamina (Articulaciones saludables), Acidos grasos (EPA-DHA), Antioxidantes, Vitamina (E,C), Lute&iacute;na, Taurina.</span></span></p>\r\n', NULL, 'Maxi Junior', 1, 'Cachorros', '_412288853.png', 'GeoTienda'),
(248, 70, 'A88', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 15 meses hasta los 5 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas altamente digestibles, Condroitina y Glucosamina (Articulaciones saludables), Omega 3 (EPA-DHA) Piel saludable.</span></span></p>\r\n', NULL, 'Maxi Adult', 1, 'Adultos', '_245169323.png', 'GeoTienda'),
(249, 70, 'A89', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 5 a&ntilde;os hasta los 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas altamente digestibles, Aporte adecuado de fibras dietarias, Condroitina y Glucosamina (Articulaciones saludables), Omega 3 (EPA-DHA), Exclusivo complejo de antioxidantes.</span></span></p>\r\n', NULL, 'Maxi Adult 5+', 1, 'Adultos', '_466075582.png', 'GeoTienda'),
(250, 70, 'A90', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 8 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas altamente digestibles, Contenido adaptado de fibras, &nbsp;Condroitina y Glucosamina (Articulaciones saludables), Omega 3 (EPA-DHA) Piel y pelo saludable, Exclusivo complejo de antioxidantes.</span></span></p>\r\n', NULL, 'Maxi Ageing 8+', 1, 'Senior', '_711688407.png', 'GeoTienda'),
(251, 70, 'A91', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas altamente digestibles, Omega (3, 6)&nbsp;(EPA-DHA) Piel y pelo saludable, Antioxidantes, Complejo de Amino&aacute;cido, y 4 vitaminas del grupo B.</span></span></p>\r\n', NULL, 'Maxi Dermacomfort', 1, 'Adultos', '_79493019.png', 'GeoTienda'),
(252, 70, 'A92', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas altamente digestibles&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">(25% m&iacute;n), moderado nivel de grasas,&nbsp;Enriquecido con L-Carnitina&nbsp;(Estimula la utilizaci&oacute;n de las grasas y evita su dep&oacute;sito como grasa corporal), Soporte osteoarticular, Apetito satisfecho.</span></span></p>\r\n', NULL, 'Maxi Weight Care', 1, 'Adultos', '_746763599.png', 'GeoTienda'),
(253, 70, 'A93', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 2 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Final de gestaci&oacute;n y lactancia, Ideal para el destete, F&aacute;cil rehidrataci&oacute;n, <strong>START COMPLEX:</strong> Seguridad digestiva&nbsp;- Defensa naturales.</span></span></p>\r\n', NULL, 'Giant Starter', 1, 'Cachorros', '_597933223.png', 'GeoTienda'),
(257, 70, 'A94', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 8 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (L.I.P) altamente digestibles, Prebi&oacute;ticos (FOS-MOS), L-Carnitina (Reduccion de grasas), Condroitina, Glucosamina (Articulaciones saludables), Acidos grasos (EPA-DHA), Antioxidantes, Vitamina (E,C), Lute&iacute;na, Taurina.</span></span></p>\r\n', NULL, 'Giant Puppy', 1, 'Cachorros', '_731772676.png', 'GeoTienda'),
(258, 70, 'A95', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 8 hasta los 18/24 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (L.I.P) altamente digestibles, Prebi&oacute;ticos (FOS-MOS), L-Carnitina (Reduccion de grasas), Condroitina, Glucosamina (Articulaciones saludables), Acidos grasos (EPA-DHA) Antioxidantes, Vitamina (E,C), Lute&iacute;na, Taurina.</span></span></p>\r\n', NULL, 'Giant Junior', 1, 'Cachorros', '_159816849.png', 'GeoTienda'),
(259, 70, 'A96', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de los 18/24 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (L.I.P) altamente digestibles, Condroitina y Glucosamina (Articulaciones saludables), Antioxidantes, Taurina, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Giant Adult', 1, 'Adultos', '_714288484.png', 'GeoTienda'),
(260, 70, 'A97', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 10/24 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima digestibilidad, Alta en proteinas y nivel de energia,<br />\r\n&nbsp; Niveles balanceados de calcio y fosforo, Alta palatabilidad.</span></span></p>\r\n', NULL, 'Club Performance Junior', 1, 'Cachorros', '_74437723.PNG', 'GeoTienda'),
(261, 70, 'A98', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima digestibilidad, Enriquezido con vitaminas, Alta palatabilidad, Contiene Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'Club Performance Adult', 1, 'Adultos', '_936595669.PNG', 'GeoTienda'),
(262, 70, 'A99', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima digestibilidad, Energia controlada, Saciedad, Apetencia reforzada.</span></span></p>\r\n', NULL, 'Club Performance Weight Control', 1, 'Adultos', '_730820253.png', 'GeoTienda'),
(263, 70, 'A100', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Cuidado de la piel, Tolerancia digestiva.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Skin Care Adult', 1, 'Adultos', '_495904200.png', 'GeoTienda'),
(264, 70, 'A101', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajo cobre, Proteinas vegetal.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Hepatic (HF 16)', 1, 'Adultos', '_633236666.png', 'GeoTienda'),
(265, 70, 'A102', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Glucomodulacion, Elevado contenido de proteinas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Diabetic', 1, 'Adultos', '_943049115.png', 'GeoTienda'),
(266, 70, 'A103', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteina hidrolizada, Barrera cutanea, Omega 3 (EPA/DHA).</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Hypoallergenic (DR 21)', 1, 'Adultos', '_254716799.png', 'GeoTienda'),
(267, 70, 'A104', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteina hidrolizada, Bajos RSS, Skin barrier, Control del sarro</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Hypoallergenic Small Dog (HSD 24)', 1, 'Adultos', '_191190335.png', 'GeoTienda'),
(268, 70, 'A105', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Stress control, Tolerancia digestiva, Skin barrier, Control de sarro.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Calm', 1, 'Adultos', '_441589420.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(269, 70, 'A106', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Alta energia, Facil rehidratacion, Crecimiento optimo.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'GastroIntestinal Junior (GIJ 29)', 1, 'Cachorros', '_475786270.png', 'GeoTienda'),
(270, 70, 'A107', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Alta energia, Alta palatabilidad, Omega 3 (EPA-DHA)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'GastroIntestinal Canine (GI 25)', 1, 'Adultos', '_43241771.png', 'GeoTienda'),
(271, 70, 'A108', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Calorias moderadas, Alta palatabilidad, Omega 3 (EPA-DHA)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'GastroIntestinal Moderate Calorie (GIM 23)', 1, 'Adultos', '_246008043.png', 'GeoTienda'),
(272, 70, 'A109', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Refuerzo vascular, Soporte renal, Balance electrolitico, Alto en Taurina y Luteina (Soporte cardiaco).</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Cardiac (EC26)', 1, 'Adultos', '_803872511.png', 'GeoTienda'),
(273, 70, 'A110', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alta proteina, Soporte articular, Balance de nutrientes, Piel y pelo saludable&nbsp;(Omega 3 y 6)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Obesity Management (DP 34)', 1, 'Adultos', '_566856680.png', 'GeoTienda'),
(274, 70, 'A111', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alta proteina, Balance de nutrientes, Soporte articular, Alto contenido en fibra.</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Satiety Support Weight Management (SAT 30)', 1, 'Adultos', '_553868987.png', 'GeoTienda'),
(275, 70, 'A112', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajo fosforo, Soporte vascular, Omega 3 (EPA-DHA), Seguridad digestiva.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Renal (RF 16)', 1, 'Adultos', '_978641297.PNG', 'GeoTienda'),
(276, 70, 'A113', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Disolucion de estruvita, Dilucion urinaria, PH controlado, Bajo magnesio.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Urinary (LP 18)', 1, 'Adultos', '_401765537.PNG', 'GeoTienda'),
(277, 70, 'A114', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">GLM, Omega 3 (EPA-DHA), Energia moderada, Complejo antioxidante.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Mobility Support (MS 25)', 1, 'Adultos', '_266708557.PNG', 'GeoTienda'),
(278, 70, 'A115', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">GLM, Omega 3 (EPA-DHA), Energia moderada, Tolerancia digestiva.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Mobility Larger Dogs (MLD 26)', 1, 'Adultos', '_345232990.png', 'GeoTienda'),
(279, 71, 'A116', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Cereales y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas (24%) y minerales, Cereales y Fibras, Omega (3 y 6)</span></span></p>\r\n', NULL, 'Perros Cachorros (Carne, Cereales y Leche)', 1, 'Cachorros', '_796174007.png', 'GeoTienda'),
(280, 71, 'A117', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas (21%) y minerales, Cereales y Fibras, Omega (3 y 6)</span></span></p>\r\n', NULL, 'Perros Adultos (Carne)', 1, 'Adultos', '_818429293.png', 'GeoTienda'),
(281, 71, 'A118', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo, Carne, Cereales y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas (21%) y minerales, Cereales y fibras, Omega (3 y 6)</span></span></p>\r\n', NULL, 'Perros Adultos (Pollo, Carne, Cereales y Arroz)', 1, 'Adultos', '_782825670.png', 'GeoTienda'),
(282, 71, 'A119', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas (21%)&nbsp;y minerales, Cereales y fibras, Omega (3 y 6)</span></span></p>\r\n', NULL, 'Perros Adultos Raza Pequeñas (Carne)', 1, 'Adultos', '_763601073.png', 'GeoTienda'),
(283, 72, 'A120', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Calcio y Fosforo, Antioxidantes, Vitamina E</span></span></p>\r\n', NULL, 'Vital Protection Cachorro (Carne, Pollo y Leche)', 1, 'Cachorros', '_329565476.PNG', 'GeoTienda'),
(284, 72, 'A121', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Cereales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas y Fibras naturales.</span></span></p>\r\n', NULL, 'Vital Protection Adulto (Carne, Pollo y Cereales)', 1, 'Adultos', '_6222879.PNG', 'GeoTienda'),
(285, 72, 'A122', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas y Fibras naturales.</span></span></p>\r\n', NULL, 'Vital Protection Adulto (Carne y Vegetales)', 1, 'Adultos', '_44518728.PNG', 'GeoTienda'),
(286, 72, 'A123', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Zinc, Omega 6</span></span></p>\r\n', NULL, 'Vital Protection Adulto Raza Pequeñas (Carne y Vegetales)', 1, 'Adultos', '_990788907.PNG', 'GeoTienda'),
(287, 72, 'A124', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Antioxidantes, como la Vitamina E</span></span></p>\r\n', NULL, 'Vital Protection Senior +7 (Carne y Pollo)', 1, 'Senior', '_941747748.PNG', 'GeoTienda'),
(288, 72, 'A125', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Tiernos trocitos cocidos al vapor</span></span></p>\r\n', NULL, 'Pouch Cachorros', 1, 'Cachorros', '_447169140.PNG', 'GeoTienda'),
(289, 72, 'A126', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Tiernos trocitos cocidos al vapor</span></span></p>\r\n', NULL, 'Pouch Adulto Razas Pequeñas (Carne)', 1, 'Adultos', '_783395810.PNG', 'GeoTienda'),
(290, 72, 'A127', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Tiernos trocitos cocidos al vapor</span></span></p>\r\n', NULL, 'Pouch Adulto Razas Pequeñas (Pollo)', 1, 'Adultos', '_12863065.PNG', 'GeoTienda'),
(291, 72, 'A128', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Carne jugosa cocida lentamente al vapor.</span></span></p>\r\n', NULL, 'Latas Cachorros ', 1, 'Cachorros', '_855223536.PNG', 'GeoTienda'),
(292, 72, 'A129', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Carne jugosa cocida lentamente al vapor.</span></span></p>\r\n', NULL, 'Latas Adulto (Carne) ', 1, 'Adultos', '_240381002.PNG', 'GeoTienda'),
(293, 72, 'A130', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Carne jugosa cocida lentamente al vapor.</span></span></p>\r\n', NULL, 'Latas Adulto (Pollo) ', 1, 'Adultos', '_617796180.PNG', 'GeoTienda'),
(294, 72, 'A131', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Combinacion de los mejores ingredientes naturales con&nbsp;tiernos trocitos blandos de carne.</span></span></p>\r\n', NULL, 'Balance Natural Adultos', 1, 'Adultos', '_415438579.png', 'GeoTienda'),
(299, 74, 'A136', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Cereales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de Proteinas, Antioxidantes, Vitaminas, &nbsp;A, D3, E, K3, B1, B2, B5, B6, B12, Minerales.</span></span></p>\r\n', NULL, 'Cachorros Nutribon Plus (Mix de sabores)', 1, 'Cachorros', '_104530150.PNG', 'GeoTienda'),
(300, 74, 'A137', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo, Cereales y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">19% de Proteinas, Antioxidantes, Vitaminas, A, D3, E, K3, B1, B2, B5, B6, B12, Minerales</span></span></p>\r\n', NULL, ' Adultos Nutribon Plus (Mix de sabores)', 1, 'Adultos', '_551718354.PNG', 'GeoTienda'),
(301, 75, 'A138', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Frutas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="font-size:16px"><span style="color:#000000">Anan&aacute; y Papaya (Enzimas proteol&iacute;ticas)&nbsp;Yogurth (Fuente de probi&oacute;ticos)&nbsp;Pulpa (Efecto prebi&oacute;tico, mejoras en heces)&nbsp;Omega 3 (Piel y pelo sano) Arvejas (Absorci&oacute;n controlada de glucosa)</span></span></p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_565015939.png', 'GeoTienda'),
(302, 75, 'A139', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Frutas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Anan&aacute; y papaya (Enzimas proteol&iacute;ticas) Zanahoria (Antioxidantes naturales) Pulpa (Efecto prebi&oacute;tico, mejoras en heces) Omega 3&nbsp;(Piel y pelo sano)&nbsp;<br />\r\nArvejas (Absorci&oacute;n controlada de glucosa).</span></span></p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_115206631.png', 'GeoTienda'),
(303, 76, 'A140', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y&nbsp;6 (Piel y pelo saludable), Aminoacidos, Vitaminas E, Selenio, Minerales.</span></span></p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_350654943.png', 'GeoTienda'),
(304, 76, 'A141', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional:&nbsp;</span></span><span style="font-size:16px"><span style="color:#000000">Omega 3 y 6 (Piel y pelo saludable) Aminoacidos, Hexametafosfato de sodio (Reduce deposito de sarro).</span></span></p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_703490884.png', 'GeoTienda'),
(305, 77, 'A142', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Hexametafosfato de sodio (Higiene bucal), Vitaminas E, Prote&iacute;nas y Minerales, Calcio y Fosforo.</span></span></p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_568747338.png', 'GeoTienda'),
(306, 77, 'A143', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable) Hexametafosfato de sodio&nbsp;(Higiene dental) Vitamina&nbsp;E, Prote&iacute;nas y Minerales, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adulto (Mordida Pequeña)', 1, 'Adultos', '_732208066.png', 'GeoTienda'),
(307, 77, 'A144', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable)&nbsp;Hexametafosfato de sodio&nbsp;(Higiene dental),&nbsp;Vitamina E, Prote&iacute;nas y Minerales,&nbsp;Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adulto (Mordida Grande)', 1, 'Adultos', '_559181001.PNG', 'GeoTienda'),
(308, 77, 'A145', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable) Hexametafosfato de sodio (Higiene dental),&nbsp;Vitamina E, Prote&iacute;nas, Minerales, Calcio y F&oacute;sforo, Acido linoleico conjugado (ALC) ayuda a reducir la grasa corporal y a prevenir la obesidad.</span></span></p>\r\n', NULL, 'Adulto (Bajas Calorías)', 1, 'Adultos', '_564083001.png', 'GeoTienda'),
(309, 78, 'A146', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Cereales y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras Naturales, Calcio y Fosforo, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Cachorros (Carne, Cereales y Vegetales)', 1, 'Cachorros', '_657146953.PNG', 'GeoTienda'),
(310, 78, 'A147', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Cereales y Vegetales</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span>&nbsp;<span style="color:#000000">Omega&nbsp;3, 6 y 9 (Piel y pelo saludable) Proteinas.</span></span></p>\r\n', NULL, 'Adultos Razas Pequeñas (Carne, Cereales y Vegetales)', 1, 'Adultos', '_868923905.PNG', 'GeoTienda'),
(311, 78, 'A148', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianos y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras Naturales, Proteinas.</span></span></p>\r\n', NULL, 'Adultos Medianos y Grandes (Carne, Pollo y Vegetales)', 1, 'Adultos', '_697735808.PNG', 'GeoTienda'),
(312, 78, 'A149', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a&ntilde;os a mas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Cereales y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas de alta calidad, Fibras naturales, Optimo niveles de fosforo (Colabora con la proteccion renal)</span></span></p>\r\n', NULL, 'Edad Avanzada (Carne, Cereales y Vegetales)', 1, 'Senior', '_936363487.PNG', 'GeoTienda'),
(313, 79, 'A150', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas y Minerales.</span></span></p>\r\n', NULL, 'Puppies', 1, 'Cachorros', '_728906290.PNG', 'GeoTienda'),
(314, 79, 'A151', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a mas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas y Minerales.</span></span></p>\r\n', NULL, 'Adults', 1, 'Adultos', '_756172765.PNG', 'GeoTienda'),
(315, 80, 'A152', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="font-size:16px">&nbsp;<br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Proteinas</span>&nbsp;<span style="color:#000000">de alto valor biol&oacute;gico, Acidos grasos&nbsp;EPA (Eicosapentaenoico) y el DHA (Docosahexaenoico),&nbsp;Omega 3 y 6&nbsp;(Piel y Pelo saludable)</span></span></p>\r\n', NULL, 'Perros Cachorros', 1, 'Cachorros', '_997704535.png', 'GeoTienda'),
(318, 80, 'A153', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alto valor biol&oacute;gico, Omega 3 y 6,(Piel y Pelo saludable) Extracto de un vegetal&nbsp;(Yucca Schidigera)&nbsp;disminuye los malos olores&nbsp;(Heces)</span></span></p>\r\n', NULL, 'Perros Adultos', 1, 'Adultos', '_814925656.png', 'GeoTienda'),
(319, 80, 'A154', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional:&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alto valor biol&oacute;gico, Omega 3 y 6,(Piel y Pelo saludable), Amino&aacute;cido llamado (L-carnitina)</span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;Extracto de un vegetal&nbsp;(Yucca Schidigera)&nbsp;disminuye los malos olores&nbsp;(Heces)</span></span></p>\r\n', NULL, 'Perros Adultos Light', 1, 'Adultos', '_108191715.png', 'GeoTienda'),
(320, 81, 'A155', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 Meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as y Medianas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 DHA, Calcio, Fosforo.</span></span></p>\r\n', NULL, 'Smart Puppy Small & Medium Breed', 1, 'Cachorros', '_324394794.png', 'GeoTienda'),
(321, 81, 'A156', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 Meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 DHA, Calcio, Fosforo.</span></span></p>\r\n', NULL, 'Smart Puppy Large & Giant Breed', 1, 'Cachorros', '_384046349.png', 'GeoTienda'),
(322, 81, 'A157', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as y Medianas&nbsp;</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Aceite de pescado, Vitamina E, Calcio.</span></span></p>\r\n', NULL, 'Adult Small & Medium Breed', 1, 'Adultos', '_941899186.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(323, 81, 'A158', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Aceite de pescado, Vitamina E, Calcio.</span></span></p>\r\n', NULL, 'Adult Large & Giant Breed', 1, 'Adultos', '_561592590.png', 'GeoTienda'),
(324, 82, 'A159', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Articular', 1, 'Todas', '_3530048.png', 'GeoTienda'),
(325, 82, 'A160', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Cardiaco', 1, 'Todas', '_727074692.png', 'GeoTienda'),
(326, 82, 'A161', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Gastrointestinal', 1, 'Todas', '_151561520.png', 'GeoTienda'),
(327, 82, 'A162', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Renal ', 1, 'Adultos', '_899375295.png', 'GeoTienda'),
(328, 82, 'A163', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Sensibilidad', 1, 'Todas', '_612859024.png', 'GeoTienda'),
(329, 83, 'A164', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">28% de Proteinas, Calcio y Fosforo, (Tocoferoles) mezclados fuente de Vitamina E.</span></span></p>\r\n', NULL, 'Puppy Small Breed', 1, 'Cachorros', '_192866968.png', 'GeoTienda'),
(330, 83, 'A165', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">28% de Proteinas, Calcio y Fosforo, (Tocoferoles) mezclados fuente de Vitamina E.</span></span></p>\r\n', NULL, 'Puppy Medium y Large Breed', 1, 'Cachorros', '_75015944.png', 'GeoTienda'),
(331, 83, 'A166', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">25% de Proteinas, Amino&aacute;cidos, Inulina (Mayor salud digestiva), Vitaminas, Minerales y Zeolita (Ayuda a producir una conformaci&oacute;n fecal firme)</span></span></p>\r\n', NULL, 'Adult Small Breed', 1, 'Adultos', '_256582651.png', 'GeoTienda'),
(332, 83, 'A167', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">24% de Proteinas, Amino&aacute;cidos, Omega 3 y 6 (Piel y Pelo saludables) Inulina (Mayor salud digestiva), Vitaminas, Minerales y Zeolita (Ayuda a producir una conformaci&oacute;n fecal firme)</span></span></p>\r\n', NULL, 'Adult Medium y Large Breed', 1, 'Adultos', '_53367713.png', 'GeoTienda'),
(333, 83, 'A168', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">28% de Proteinas, Amino&aacute;cidos, Inulina (Mayor salud digestiva), Vitaminas (C, E), Minerales.</span></span></p>\r\n', NULL, 'Adult 7+', 1, 'Adultos', '_328000738.png', 'GeoTienda'),
(334, 83, 'A169', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">27% de Proteinas de alta digestibilidad y bajo en grasas, Fuentes de fibra alimentaria, Inulina (Mayor salud digestiva), Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Reduced Calorie', 1, 'Adultos', '_125070581.png', 'GeoTienda'),
(335, 83, 'A170', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">27% de Proteinas de alta calidad, Tocoferoles (Fuente en Vitaminas E), Omega 3 y 6 (Piel y Pelo saludable), Calcio y Fosforo.</span></span></p>\r\n', NULL, 'Formula Puppy', 1, 'Cachorros', '_32058286.png', 'GeoTienda'),
(336, 83, 'A171', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">22% de Proteinas de alta calidad, Tocoferoles (Fuente&nbsp;en Vitamina&nbsp;E), Omega 3 y 6 (Piel y Pelo saludable)</span></span></p>\r\n', NULL, 'Formula Adult', 1, 'Adultos', '_917615415.png', 'GeoTienda'),
(337, 84, 'A172', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas (C, E)&nbsp;Selenio, Zinc, Calcio, F&oacute;sforo y Amino&aacute;cidos, Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_82159904.PNG', 'GeoTienda'),
(338, 84, 'A173', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Antioxidantes, Omega 3 y 6 (Piel y pelo saludable),&nbsp;Prote&iacute;nas, Fibras naturales, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_771249887.png', 'GeoTienda'),
(339, 85, 'A174', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable)&nbsp;(DHA),&nbsp;Antioxidantes, Vitaminas (C, E),&nbsp;L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Puppy Small Breed', 1, 'Cachorros', '_132485701.png', 'GeoTienda'),
(340, 85, 'A175', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable) (DHA),&nbsp;Antioxidantes, Vitaminas (C, E), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Puppy Medium Breed', 1, 'Cachorros', '_717200905.png', 'GeoTienda'),
(341, 85, 'A176', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta 2 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable) (DHA),&nbsp;Antioxidantes, Vitaminas (C, E), L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Puppy Large Breed', 1, 'Cachorros', '_183652757.png', 'GeoTienda'),
(342, 85, 'A177', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Adult Small Breed', 1, 'Adultos', '_294337120.png', 'GeoTienda'),
(343, 85, 'A178', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E)&nbsp;Calcio,&nbsp;L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Adult Medium Breed', 1, 'Adultos', '_544351976.png', 'GeoTienda'),
(344, 85, 'A179', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina&nbsp;(C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Adult Large Breed', 1, 'Adultos', '_830931277.png', 'GeoTienda'),
(345, 85, 'A180', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Senior Small Breed', 1, 'Senior', '_418812207.png', 'GeoTienda'),
(346, 85, 'A181', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Senior Medium Breed', 1, 'Senior', '_380974488.png', 'GeoTienda'),
(347, 85, 'A182', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 5 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Senior Large Breed', 1, 'Senior', '_155942223.png', 'GeoTienda'),
(348, 85, 'A183', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (DHA) (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Adult Weight Control Small Breed', 1, 'Adultos', '_6719303.png', 'GeoTienda'),
(349, 85, 'A184', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Adult Weight Control Medium Breed', 1, 'Adultos', '_998928064.png', 'GeoTienda'),
(350, 85, 'A185', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes y Gigantes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Adult Weight Control Large Breed', 1, 'Adultos', '_833324437.png', 'GeoTienda'),
(351, 85, 'A186', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Premium Performance', 1, 'Adultos', '_813986211.png', 'GeoTienda'),
(352, 85, 'A187', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para el English Springer Spaniel</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), Taurina, 3D Dental defense.</span></span></p>\r\n', NULL, 'Cocker Spaniel', 1, 'Adultos', '_381679419.PNG', 'GeoTienda'),
(353, 85, 'A188', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Welsh Corgis, Petit Basset Griffon Vandeen y Dandie Dinmont Terriers</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Glucosamina (Articulaciones saludables), Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Dachshund', 1, 'Adultos', '_459953657.PNG', 'GeoTienda'),
(354, 85, 'A189', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Scottish Terrier, Welsh Terriers y West Highland Terrier</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), Taurina, 3D Dental defense.</span></span></p>\r\n', NULL, 'Miniature Schnauzer', 1, 'Adultos', '_152044321.PNG', 'GeoTienda'),
(355, 85, 'A190', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Lhasa Apso y Pekin&eacute;s</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Glucosamina (Articulaciones saludables), Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Shih Tzu', 1, 'Adultos', '_462529545.PNG', 'GeoTienda'),
(356, 85, 'A191', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Bichon Frise y Havan&eacute;s</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E y Beta-caroteno</span></span><span style="color:#000000"><span style="font-size:16px">), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Toy Poodle', 1, 'Adultos', '_825748607.PNG', 'GeoTienda'),
(357, 85, 'A192', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Setter Ingl&eacute;s, Setter Irland&eacute;s, Golden y Doberman Pinschers</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Glucosamina (Articulaciones saludables), Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E y Beta-caroteno), Calcio, L-Carnitina (Reduccion de grasas), Taurina, 3D Dental defense.</span></span></p>\r\n', NULL, 'Boxer', 1, 'Adultos', '_163259518.PNG', 'GeoTienda'),
(358, 85, 'A193', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Curly Coated, Chesapeake Bay Retrievers y Golden Retriever</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Glucosamina (Articulaciones saludables), Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), Taurina, 3D Dental defense.</span></span></p>\r\n', NULL, 'Labrador Retriever', 1, 'Adultos', '_30799334.PNG', 'GeoTienda'),
(359, 85, 'A194', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad: </span><span style="color:#000000">S&uacute;per Premium</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">De 1 a mas a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Raza: </span><span style="color:#000000">Tambi&eacute;n para Pastor Belga</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional: </span><span style="color:#000000">Proteinas, Glucosamina (Articulaciones saludables), Omega 3 y 6 (Piel y pelo saludables), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'German Shepherd', 1, 'Adultos', '_457192781.PNG', 'GeoTienda'),
(360, 85, 'A195', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Mast&iacute;n Franc&eacute;s, Cane Corso, Mast&iacute;n Argentino y Rodesiano</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: &nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">Alto en proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), Taurina, 3D Dental defense.</span></span></p>\r\n', NULL, 'Rottweiler', 1, 'Adultos', '_209685817.PNG', 'GeoTienda'),
(361, 85, 'A196', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Tambi&eacute;n para Cavalier y Malt&eacute;s</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Minerales, Omega 3 y 6 (Piel y pelo saludable), Antioxidantes, Vitamina (C, E), Calcio, L-Carnitina (Reduccion de grasas), 3D Dental defense.</span></span></p>\r\n', NULL, 'Yorkshire Terrier', 1, 'Adultos', '_362740356.PNG', 'GeoTienda'),
(362, 86, 'A197', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Cereales y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas &nbsp;(A, D y E), Calcio, F&oacute;sforo, Minerales.</span></span></p>\r\n', NULL, 'Cachorros (Carne, Cereales y Leche)', 1, 'Cachorros', '_218917302.png', 'GeoTienda'),
(363, 86, 'A198', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne asada con Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas (A, D&nbsp;y E), Calcio, F&oacute;sforo, Minerales.</span></span></p>\r\n', NULL, 'Adultos (Carne y Vegetales)', 1, 'Adultos', '_826565271.png', 'GeoTienda'),
(364, 86, 'A199', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne con Cereales y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas (A, D&nbsp;y E), Calcio, F&oacute;sforo, Minerales.</span></span></p>\r\n', NULL, 'Adultos (Carne, Cereales y Arroz)', 1, 'Adultos', '_553647319.png', 'GeoTienda'),
(365, 86, 'A200', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo grillado con Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas (A, D&nbsp;y E), Calcio, F&oacute;sforo, Minerales.</span></span></p>\r\n', NULL, 'Adultos (Pollo y Vegetales)', 1, 'Adultos', '_631976224.png', 'GeoTienda'),
(366, 86, 'A201', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salsa de Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas, Calcio, F&oacute;sforo, Minerales.</span></span></p>\r\n', NULL, 'Adultos (Salsa de Carne)', 1, 'Adultos', '_357599100.PNG', 'GeoTienda'),
(367, 87, 'A202', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">20% de proteinas, Alto valor nutricional.</span></span></p>\r\n', NULL, 'Criadores Razas Pequeñas', 1, 'Adultos', '_149314874.png', 'GeoTienda'),
(368, 87, 'A203', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas, Alto valor nutricional.</span></span></p>\r\n', NULL, 'Criadores Cachorros', 1, 'Cachorros', '_97173690.png', 'GeoTienda'),
(369, 87, 'A204', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">20% de proteinas, Alto valor nutricional.</span></span></p>\r\n', NULL, 'Criadores Adultos (Carne y Pollo)', 1, 'Adultos', '_121187745.png', 'GeoTienda'),
(370, 87, 'A205', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">20% de proteinas, Alto valor nutricional.</span></span></p>\r\n', NULL, 'Criadores Adultos (Carne y Vegetales)', 1, 'Adultos', '_435322742.png', 'GeoTienda'),
(371, 87, 'A206', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">30% de proteinas.</span></span></p>\r\n', NULL, 'Premium Cachorros', 1, 'Cachorros', '_120438264.png', 'GeoTienda'),
(372, 87, 'A207', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">23% de proteinas.</span></span></p>\r\n', NULL, 'Premium Adultos', 1, 'Adultos', '_921604713.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(373, 87, 'A208', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">32% de proteinas.</span></span></p>\r\n', NULL, 'Etiqueta Negra Cachorros', 1, 'Cachorros', '_52987031.png', 'GeoTienda'),
(374, 87, 'A209', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas.</span></span></p>\r\n', NULL, 'Etiqueta Negra Razas Pequeñas', 1, 'Adultos', '_639026761.png', 'GeoTienda'),
(375, 87, 'A210', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas.</span></span></p>\r\n', NULL, 'Etiqueta Negra Razas Medianas y Grandes', 1, 'Adultos', '_721107393.png', 'GeoTienda'),
(376, 88, 'A211', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">23 Vitaminas, Antioxidantes,&nbsp;Minerales, Calcio, DHA (Ayuda el desarrollo del cerebro y la vision)</span></span></p>\r\n', NULL, 'Cachorros Razas Pequeñas', 1, 'Cachorros', '_257981340.PNG', 'GeoTienda'),
(377, 88, 'A212', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">23 Vitaminas, Antioxidantes, Minerales, Calcio, DHA (Ayuda el desarrollo del cerebro y la vision)</span></span></p>\r\n', NULL, 'Cachorros Razas Medianas y Grandes', 1, 'Cachorros', '_263638141.png', 'GeoTienda'),
(378, 88, 'A213', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">23% de proteinas, Taurina (optimiza la salud de sus &oacute;rganos vitales incluyendo un corazon sano), Minerales, Calcio, Proteinas.</span></span></p>\r\n', NULL, 'Adultos Razas Pequeñas', 1, 'Adultos', '_204920495.png', 'GeoTienda'),
(379, 88, 'A214', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras naturales, Prote&iacute;nas, Calcio, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Adultos Razas Medianas y Grandes', 1, 'Adultos', '_926232189.png', 'GeoTienda'),
(380, 88, 'A215', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a mas a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Glucosamina (Articulaciones m&aacute;s saludables), Vitaminas (C,&nbsp;E), Antioxidantes, Fibras, 25 %&nbsp;Proteinas.</span></span></p>\r\n', NULL, 'Edad Madura', 1, 'Senior', '_793226930.png', 'GeoTienda'),
(381, 88, 'A216', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="font-size:16px"><span style="color:#000000">Standard</span><br />\r\n<span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="font-size:16px"><span style="color:#000000">Todas</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional:</span><span style="color:#000000"> 20% menos de calorias, 25% de proteinas (Proporcion adecuada de proteinas y grasas), Enriquecido en fibras naturales.</span></span></p>\r\n', NULL, 'Sano y en Forma (Reduced Calorie)', 1, 'Adultos', '_258570354.png', 'GeoTienda'),
(382, 88, 'A217', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional:&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (Pescado, Pollo y huevo) Omega 3 y 6 (Piel y pelo saludables) Fibras naturales, Vitaminas y Minerales, Calcio.</span></span></p>\r\n', NULL, 'Essentials Cachorros', 1, 'Cachorros', '_180731412.png', 'GeoTienda'),
(383, 88, 'A218', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional:&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">Proteinas (Pescado, Pollo y huevo) Omega 3 y 6 (Piel y pelo saludables) Fibras naturales, Vitaminas y Minerales, Calcio.</span></span></p>\r\n', NULL, 'Essentials Adultos', 1, 'Adultos', '_379814099.png', 'GeoTienda'),
(384, 88, 'A219', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 3 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con trigo integral, Calcio (Dientes y huesos fuertes)</span></span></p>\r\n', NULL, 'Abrazzos Galletas Junior', 1, 'Cachorros', '_223865427.png', 'GeoTienda'),
(385, 88, 'A220', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con trigo integral, Omega 6 (Pelo brillante)</span></span></p>\r\n', NULL, 'Abrazzos Galletas Mini', 1, 'Todas', '_108485764.png', 'GeoTienda'),
(386, 88, 'A221', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con trigo integral, Omega 6 (Pelo brillante)</span></span></p>\r\n', NULL, 'Abrazzos Galletas Maxi', 1, 'Todas', '_455547100.png', 'GeoTienda'),
(387, 88, 'A222', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Zanahoria, Pollo y Espinaca</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con trigo integral, Omega 6 (Pelo brillante)</span></span></p>\r\n', NULL, 'Abrazzos Galletas Duo', 1, 'Todas', '_524295831.png', 'GeoTienda'),
(388, 88, 'A223', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Manzana</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con Manzanas y trigo integral, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Abrazzos Bocaditos Tartitas', 1, 'Todas', '_226366302.png', 'GeoTienda'),
(389, 88, 'A224', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Manzana y Banana</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Hecho con frutas naturales y trigo integral, Vitaminas y Minerales.</span></span></p>\r\n', NULL, 'Abrazzos Bocaditos Mix de Frutas', 1, 'Todas', '_450789541.png', 'GeoTienda'),
(390, 89, 'A225', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad: </span><span style="color:#000000">Standard</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">Cachorros</span><br />\r\n<span style="color:#FF0000">* Raza: </span><span style="color:#000000">Todas</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional: </span><span style="color:#000000">Antioxidantes BHT, BHA, TBHQ, Vitaminas A, D3, E, K, B1, B2, B6, &nbsp; &nbsp; B12, Calcio y F&oacute;sforo, Prote&iacute;na 21%.</span></span></p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_323238127.png', 'GeoTienda'),
(391, 89, 'A226', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Antioxidantes, Vitaminas A, D3, E, K, B1, B2, B6, B12, Calcio y F&oacute;sforo, Prote&iacute;na 18%, Fibra 4%.</span></span></p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_188486803.png', 'GeoTienda'),
(392, 90, 'A227', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Vitaminas A, C, B1, B2, B6, B12, D3, E, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Puppy', 1, 'Cachorros', '_964266258.png', 'GeoTienda'),
(393, 90, 'A228', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Vitaminas A, C, B1, B2, B6, B12, D3, E, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_52553684.png', 'GeoTienda'),
(394, 90, 'A229', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable), Vitaminas A, C, B1, B2, B6, B12, D3, E, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Adultos Razas Pequeñas', 1, 'Adultos', '_849370052.png', 'GeoTienda'),
(395, 91, 'A230', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 12/15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 32%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E y C, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Cachorros', 1, 'Cachorros', '_838700476.png', 'GeoTienda'),
(396, 91, 'A231', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 12/15 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 32%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E y C, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Cachorros Mordida Pequeña', 1, 'Cachorros', '_90923087.png', 'GeoTienda'),
(397, 91, 'A232', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 12/15 meses en adelante</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 26%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E y C, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Adultos', 1, 'Adultos', '_358868306.png', 'GeoTienda'),
(398, 91, 'A233', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De los 12/15 meses en adelante</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 26%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E y C, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Adultos Mordida Pequeña', 1, 'Adultos', '_63083325.png', 'GeoTienda'),
(399, 91, 'A234', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Raza:</span> <span style="color:#000000">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 28%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E y C, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal), L-Carnitina (Reducci&oacute;n de peso controlada)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Light Mordida Pequeña', 1, 'Adultos', '_203152889.png', 'GeoTienda'),
(400, 91, 'A235', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 28%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E, (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Criadores', 1, 'Todas', '_365281773.png', 'GeoTienda'),
(401, 91, 'A236', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 30%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E, C (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal)</span></span></p>\r\n', NULL, 'Competencia', 1, 'Todas', '_764133178.png', 'GeoTienda'),
(402, 91, 'A237', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 15%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E, C (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal), Manganeso y Zing organicos.</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Ultra Osteoarticular', 1, 'Todas', '_898750591.png', 'GeoTienda'),
(403, 91, 'A238', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas 35%, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) Vitaminas A, D3, E, C (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal), Manganeso y Zing organicos.</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Ultra Vita Plus', 1, 'Todas', '_633640614.png', 'GeoTienda'),
(404, 92, 'A239', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Cachorros</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Omega 3 y 6 (Piel y pelo saludable), Glucosamina y Condroitin Sulfato (Regenerador y protector osteoarticular) (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal), Taurina y L-carnitina, Vitamina A,D3,K,E,C, B1, B2, B5, B6, B12</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Premium Cachorros', 1, 'Cachorros', '_767241808.png', 'GeoTienda'),
(405, 92, 'A240', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Aminoacidos, Omega 3 y 6 (Piel y pelo saludable), (MOS) Manano-Oligosacaridos (Prebioticos, controlan la flora intestinal),&nbsp;<br />\r\nTaurina y L-carnitina, Vitamina A,D3,K,E,C, B1, B2, B5, B6, B12</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Premium Adultos', 1, 'Adultos', '_293467541.png', 'GeoTienda'),
(406, 66, 'A241', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional:</span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;Calcio, Vitaminas, Omega 3 y 6 (Piel y pelo saludable) Antioxidantes naturales, Taurina, Glucosamina y Condroitrin Sulfato&nbsp;(Protege el desarrolo de las articulaciones)&nbsp;</span></span></p>\r\n', NULL, 'V56 Kitten', 2, 'Cachorros', '_983199035.png', 'GeoTienda'),
(407, 66, 'A242', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7 a&ntilde;os</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Bajo contenido en fosforo, Aminoacidos, Antioxidantes, Minerales y Vitaminas, Taurina (Proteccion cardiaca) Omega 3 y 6 (Piel y pelo saludable)</span>&nbsp;</span></p>\r\n', NULL, 'VSR Senior', 2, 'Senior', '_112532146.png', 'GeoTienda'),
(408, 66, 'A243', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Omega 3 y 6 (Piel y pelo saludable) Antioxidantes naturales, Aminoacidos, Taurina (Proteccion cardiaca) L-Carnitina (Favorece la metabolizacion de las grasas)</span></span></p>\r\n', NULL, 'V43 Sobrepeso', 2, 'Adultos', '_463869099.png', 'GeoTienda'),
(409, 66, 'A244', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Omega 3 y 6 (Piel y pelo saludable) Antioxidantes, Aminoacidos, Minerales y Vitaminas, Taurina (Proteccion cardiaca)</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;</span></span></p>\r\n', NULL, 'V42 Mantenimiento', 2, 'Adultos', '_432686103.png', 'GeoTienda'),
(410, 66, 'A245', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajo en magnesio, PH regulado, Omega 3 y 6 (Piel y pelo saludable) Antioxidantes, Aminoacidos, Minerales y Vitaminas, Taurina (Proteccion cardiaca)</span></span><span style="color:#FF0000"><span style="font-size:16px"> &nbsp;</span></span></p>\r\n', NULL, 'VPH Controlado', 2, 'Adultos', '_936282617.png', 'GeoTienda'),
(411, 66, 'A246', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alto en energia, Glucosamina y Condroitin Sulfato (Proteccion osteo-articular) Aminoacidos, Antioxidantes, Minerales y Vitaminas, Taurina (Proteccion cardiaca) PH regulado, Omega 3 y 6 (Piel y pelo saludable)</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;</span></span></p>\r\n', NULL, 'V46', 2, 'Adultos', '_233648669.png', 'GeoTienda'),
(412, 66, 'A247', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1&nbsp;a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas del huevo y la leche, Modulaci&oacute;n de las defensas, Desarrollo dental y &oacute;seo.</span></span></p>\r\n', NULL, 'Balanced Kitten', 2, 'Cachorros', '_815554172.png', 'GeoTienda'),
(413, 66, 'A248', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Salud de v&iacute;as urinarias bajas, Salud card&iacute;aca, Hairball control.</span></span></p>\r\n', NULL, 'Balanced Adult Cat', 2, 'Adultos', '_623343085.png', 'GeoTienda'),
(414, 66, 'A249', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alta calidad, Salud &oacute;steoarticular, Modulaci&oacute;n de las defensas, Salud renal, Hairball control.</span></span></p>\r\n', NULL, 'Balanced Senior Cat', 2, 'Senior', '_4173711.png', 'GeoTienda'),
(415, 66, 'A250', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Metabolismo de las grasas, M&uacute;sculos fuertes, Salud Card&iacute;aca, Salud &oacute;steoarticular.</span></span></p>\r\n', NULL, 'Balanced Control de Peso Gato Adulto', 2, 'Adultos', '_566483520.png', 'GeoTienda'),
(416, 66, 'A251', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajos niveles de minerales, PH dual.</span></span></p>\r\n', NULL, 'Balanced Control de PH Gato Adulto', 2, 'Adultos', '_197759059.png', 'GeoTienda'),
(417, 66, 'A252', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">&Oacute;ptima nutrici&oacute;n y gran digestibilidad, Mayor vitalidad, Heces firmes y de escaso olor, Previene trastornos urinarios, Adecuado balance de &aacute;cidos grasos omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'Premium Gato', 2, 'Adultos', '_699884138.png', 'GeoTienda'),
(418, 66, 'A253', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Inmunidad potenciada, &Oacute;ptimo crecimiento, Rico y natural, Sabor irresistible, Sin colorantes artificiales.</span></span></p>\r\n', NULL, 'Complete Kitten', 2, 'Cachorros', '_79965771.png', 'GeoTienda'),
(421, 66, 'A254', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 12 meses y hasta 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Pelo mas sano y brillante, Tracto urinario, Doble proteccion, Rico y natural, Sabor irresistible, Sin colorantes artificiales.</span></span></p>\r\n', NULL, 'Complete Adult', 2, 'Adultos', '_834448541.png', 'GeoTienda'),
(422, 66, 'A255', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Silueta ideal, L-Carnitina (Reduccion de grasas), Pelo mas sano y brillante, Tracto urinario, Doble proteccion, Rico y natural, Sabor irresistible, Sin colorantes artificiales.</span></span></p>\r\n', NULL, 'Complete Adult Control de Peso', 2, 'Adultos', '_586391041.png', 'GeoTienda'),
(423, 66, 'A256', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mas de 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mejor calidad de vida, Pelo mas sano y brillante, Tracto urinario, Doble proteccion, Rico y natural, Sabor irresistible, Sin colorantes artificiales.</span></span></p>\r\n', NULL, 'Complete Senior', 2, 'Senior', '_400542764.png', 'GeoTienda'),
(424, 67, 'A257', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">40% de Proteinas, 18% de Grasas, Calostro (Prebi&oacute;ticos y anticuerpos naturales), Antioxidantes, DHA, Vitaminas (A, E), Taurina.</span></span></p>\r\n', NULL, 'Kitten Gatitos', 2, 'Cachorros', '_920459996.png', 'GeoTienda'),
(425, 67, 'A258', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">36% de Proteinas, 15% de Grasas, Antioxidantes, Vitaminas E,<br />\r\nMinerales, Tecnologia <strong>TARTAR CONTROL</strong> (Reduccion de sarro dental en un 41%) Tecnologia <strong>STONE NEUTRAL</strong> (Previene formaciion de cristales) en el tracto urinario.</span></span></p>\r\n', NULL, 'Adult', 2, 'Adultos', '_797530014.png', 'GeoTienda'),
(426, 67, 'A259', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 o mas a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">37% de Proteinas, 18% de Grasas, Aminoacidos, Antioxidantes naturales, Vitaminas (B, E), Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Adult 7+ ', 2, 'Adultos', '_483729569.png', 'GeoTienda'),
(427, 67, 'A260', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">40% de Proteinas, 10% de Grasas, Vitamina E, Fibras naturales.</span></span></p>\r\n', NULL, 'Reduced Calorie', 2, 'Adultos', '_165269598.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(428, 67, 'A261', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">36% de Proteinas, 15% de Grasas, Vitamina (A, E), Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Sensitive', 2, 'Adultos', '_791563961.png', 'GeoTienda'),
(429, 67, 'A262', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">40% de Proteinas, 12% de Grasas, Antioxidantes, Aminoacidos, Omega 3 y 6 (Piel y pelo saludable), Vitamina E, Fibras naturales.</span></span></p>\r\n', NULL, 'Sterilized', 2, 'Adultos', '_4541520.png', 'GeoTienda'),
(430, 67, 'A263', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">40% de Proteinas, 11% de Grasas, Vitamina E,&nbsp;Tecnolog&iacute;a<strong> Optitract with Dual Stone Protection</strong>&nbsp;(Combinaci&oacute;n exclusiva de nutrientes, previene&nbsp;la formaci&oacute;n de cristales de estruvita y oxalato manteniendo y protegiendo la salud del tracto urinario).</span></span></p>\r\n', NULL, 'Urinary', 2, 'Adultos', '_205011752.png', 'GeoTienda'),
(432, 70, 'A271', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Siamese</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, FOS (Fructo-oligosac&aacute;ridos), MOS (Manano-oligosac&aacute;ridos) y el complejo de antioxidante que incluye Lute&iacute;na, Taurina, Vitaminas (C, E), L-carnitina ayuda a favorecer el metabolismo de las grasas.</span></span></p>\r\n', NULL, 'Siamese 38', 2, 'Adultos', '_383217214.png', 'GeoTienda'),
(433, 70, 'A272', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Persa</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, FOS (Fructo-oligosac&aacute;ridos), MOS (Manano-oligosac&aacute;ridos) y el complejo de antioxidantes. Complejos&nbsp;de nutrientes activos <strong>Beauty Shine Complex</strong>, que ayuda a limitar la irritaci&oacute;n d&eacute;rmica, reforzar la funci&oacute;n de &ldquo;barrera&rdquo; de la piel e intensificar el color, el brillo y la suavidad del pelaje.</span></span></p>\r\n', NULL, 'Persian 30', 2, 'Adultos', '_808074231.png', 'GeoTienda'),
(434, 70, 'A273', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 4 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px"><strong>NATURAL DEFENCE</strong> (Refuerzo de defensas naturales) Antioxidantes, (Vitaminas E,C, Taurina, Lute&iacute;na) (MOS)(manano-oligosac&aacute;ridos) Seguridad digestiva reforzada (Proteinas altamente digestibles) EPA/DHA.</span></span></p>\r\n', NULL, 'BabyCat 34', 2, 'Cachorros', '_990017126.png', 'GeoTienda'),
(435, 70, 'A274', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles (L.I.P), Acidos grasos (EPA-DHA), Antioxidantes (Vitaminas E,C, Taurina, Lute&iacute;na) y Manano-oligosac&aacute;ridos, que estimulan la producci&oacute;n de anticuerpos</span><span style="font-size:16px">.</span></span></p>\r\n', NULL, 'Kitten 36', 2, 'Cachorros', '_279448827.png', 'GeoTienda'),
(436, 70, 'A275', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo y equilibrado, Mantenimiento del peso ideal, Regulaci&oacute;n de bolas de pelo.</span></span></p>\r\n', NULL, 'Fit 32', 2, 'Adultos', '_378401182.png', 'GeoTienda'),
(437, 70, 'A276', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas altamente digestibles, Arroz, Fructo-Oligosac&aacute;ridos, Pulpa de remolacha y Aceite de pescado (Rico en omega 3)</span></span></p>\r\n', NULL, 'Sensible 33', 2, 'Adultos', '_635557556.png', 'GeoTienda'),
(438, 70, 'A277', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Contenido moderado de energ&iacute;a (aporte cal&oacute;rico moderado 3946 Kcal/kg)</span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;(L-Carnitina) ayuda a mejorar el metabolismo de las grasas, Regulaci&oacute;n de bolas de pelo.</span></span></p>\r\n', NULL, 'Indoor 27', 2, 'Adultos', '_177280055.png', 'GeoTienda'),
(439, 70, 'A278', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas (35%) Altamente digestibles (L.I.P), Vitamina (A,B), Acidos grasos esenciales Omega 3 y 6 (Piel y pelo saludable), (EPA-DHA) (Ac. Linoleico), Oligoelementos Quelados (Zinc) y Amino&aacute;cidos azufrados.</span></span></p>\r\n', NULL, 'Indoor Long Hair', 2, 'Adultos', '_115169875.png', 'GeoTienda'),
(440, 70, 'A279', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a 12 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Antioxidantes, Vitamina (C, E), Taurina y Lute&iacute;na, Acidos grasos Omega 3 y 6 (Piel y pelo saludable)&nbsp;(EPA-DHA) Amino&aacute;cidos azufrados, presencia de Glucosamina y Condroitina (condroprotectores)&nbsp;articulaciones saludables.</span></span></p>\r\n', NULL, 'Indoor 7+', 2, 'Senior', '_207051570.png', 'GeoTienda'),
(441, 70, 'A280', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mantenimiento del peso ideal, Belleza del pelaje.</span></span></p>\r\n', NULL, 'Exigent 35/30 ', 2, 'Adultos', '_363523182.png', 'GeoTienda'),
(442, 70, 'A281', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 7 a 12 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Complejo de Antioxidantes, Vitamina (C, E), Taurina y Lute&iacute;na, Glucosamina y Condroitina (Condroprotectores) articulaciones saludables, Acidos grasos Omega 3 (EPA-DHA) por su marcado efecto antiinflamatorio.</span></span></p>\r\n', NULL, 'Active 7+', 2, 'Senior', '_509477282.png', 'GeoTienda'),
(443, 70, 'A282', 5, '<p><span style="font-size:16px"><span style="color:#FF0000">Descripcion:<br />\r\n* Calidad: </span><span style="color:#000000">Super Premium</span><br />\r\n<span style="color:#FF0000">* Edad: </span><span style="color:#000000">A partir de 12 a&ntilde;os</span><br />\r\n<span style="color:#FF0000">* Analisis Nutricional: </span><span style="color:#000000">SALUD RENAL - Contenido adaptado de fosforo, ESTIMULACI&Oacute;N DEL APETITO, Complejo de Antioxidantes, (EPA-DHA) y Tript&oacute;fano</span></span><span style="color:#000000">,&nbsp;<span style="font-size:16px">reconocido por sus efectos apaciguantes.</span></span></p>\r\n', NULL, 'Ageing +12', 2, 'Senior', '_755249692.png', 'GeoTienda'),
(444, 70, 'A283', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Sistema urinario saludable, Mantenimiento del peso ideal.</span></span></p>\r\n', NULL, 'Instinctive in jelly', 2, 'Adultos', '_572069615.png', 'GeoTienda'),
(445, 70, 'A284', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px"><strong>SHINY HAIR COMPLEX:</strong> &aacute;cidos grasos Omega 3 y 6 (Piel y pelo saludable), Vitaminas B, Biotina y Zinc, Sistema urinario saludable, Mantenimiento del peso ideal.</span></span></p>\r\n', NULL, 'Intense Beauty', 2, 'Adultos', '_510549869.png', 'GeoTienda'),
(446, 67, 'A30', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitamina (C, E), &nbsp;Minerales, Calcio y F&oacute;sforo.</span></span></p>\r\n', NULL, 'Sensitive Skin Dog Adult Razas Pequeñas', 1, 'Adultos', '_863920233.png', 'GeoTienda'),
(447, 67, 'A31', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">S&uacute;per Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">20% menos de calorias, Prote&iacute;nas, Omega 3 y 6 (Piel y pelo saludable), Vitamina (C, E), Minerales, Isoflavonas (Antioxidantes naturales).</span></span></p>\r\n', NULL, 'Reduced Calorie Razas Pequeñas', 1, 'Adultos', '_501255878.png', 'GeoTienda'),
(448, 67, 'A39', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente &uacute;nica de prote&iacute;na hidrolizada de bajo peso molecular para ayudar a evitar las reacciones al&eacute;rgicas. Carbohidratos purificados para ayudar a evitar las reacciones al&eacute;rgicas. Omega 3 para ayudar a maximizar el proceso antiinflamatorio natural.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Veterinary Diets HA Hypoallergenic', 1, 'Todas', '_229459088.png', 'GeoTienda'),
(449, 72, 'A132', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Tiernos trocitos cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adulto Razas Grandes (Carne)', 1, 'Adultos', '_753224023.png', 'GeoTienda'),
(450, 72, 'A133', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Tiernos trocitos cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adulto Razas Grandes (Pollo)', 1, 'Adultos', '_585385393.png', 'GeoTienda'),
(451, 73, 'A133', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Desarrollo cognitivo, Optimo crecimiento, Disminuye olor de las heces, Sistema inmune reforzado.</span></span></p>\r\n', NULL, 'Puppis Medium y Large Breed', 1, 'Cachorros', '_368427778.png', 'GeoTienda'),
(452, 73, 'A134', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteccion dental, Control de peso, Salud articular (Glocosamina, Condroitin sulfato), Piel y pelo saludable (Acidos grasos).</span></span></p>\r\n', NULL, 'Adults Small Breed', 1, 'Adultos', '_509956999.png', 'GeoTienda'),
(453, 73, 'A135', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteccion dental, Control de peso, Salud articular (Glocosamina, Condroitin sulfato), Piel y pelo saludable (Acidos grasos).</span></span></p>\r\n', NULL, 'Adults Medium y Large Breed', 1, 'Adultos', '_515251232.png', 'GeoTienda'),
(454, 73, 'A136', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Control de peso, Huesos fuertes, Salud digestiva, Sistema inmune reforzado.</span></span></p>\r\n', NULL, 'Adult All Breed Original Recipe', 1, 'Adultos', '_574255815.png', 'GeoTienda'),
(455, 73, 'A137', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Cordero y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteccion dental, Salud articular, Sistema inmune reforzado.</span></span></p>\r\n', NULL, 'Adult All Breed Special Recipe', 1, 'Adultos', '_435423731.png', 'GeoTienda'),
(456, 73, 'A132', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Peque&ntilde;as</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Desarrollo cognitivo, Optimo crecimiento, Disminuye olor de las heces, Sistema inmune reforzado.</span></span></p>\r\n', NULL, 'Puppis Small Breed', 1, 'Cachorros', '_851151978.png', 'GeoTienda'),
(460, 95, 'A230', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuerte en Proteinas, Calcio y Fosforo.</span></span></p>\r\n', NULL, 'Junior (Carne y Pollo)', 1, 'Cachorros', '_540360385.png', 'GeoTienda'),
(461, 95, 'A231', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Calcio y Fosforo, Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Adultos (Carne)', 1, 'Adultos', '_716628415.png', 'GeoTienda'),
(462, 95, 'A232', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Calcio y Fosforo, Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Adultos (Pollo y Arroz)', 1, 'Adultos', '_389399203.png', 'GeoTienda'),
(463, 95, 'A233', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pollo y Vegetales</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Calcio y Fosforo, Omega 3 y 6 (Piel y pelo saludable)</span></span></p>\r\n', NULL, 'Adultos (Carne, Pollo y Vegetales)', 1, 'Adultos', '_476338588.png', 'GeoTienda'),
(464, 67, 'A264', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alta calidad en proteinas, Bajo Fosforo, Reducido en Sodio, Niveles adecuados de Omega 3 y 6.</span></span><span style="color:#FF0000"><span style="font-size:16px"> &nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Veterinary Diets NF Kidney Function', 2, 'Adultos', '_812243210.png', 'GeoTienda'),
(467, 77, '', 5, '<p>test</p>\r\n', NULL, 'Nuevo producto para gato 2', 2, 'Cachorros', '25_545236226.jpg', 'Local'),
(469, NULL, '', 6, '<p><span style="color:#000000"><strong><span style="font-size:16px">El agarre est&aacute; recubierto con material soft, haci&eacute;ndolo c&oacute;modo, suave al tacto y agradable para tu mano.<br />\r\nEste modelo est&aacute; hecho con dobla cinta cosida y posee un accesorio porta objetos.<br />\r\nLargo: 125 cm<br />\r\nAncho de cinta: 1.5 cm<br />\r\nColor: Negro&nbsp;</span></strong></span></p>\r\n', NULL, 'Correa (Creado por el local)', 1, '', '30_876831850.JPG', 'Local'),
(470, 67, 'A265', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Bajo en calorias y grasas, Alto contenido proteico, Fibras naturales, Estruvita y Oxalato (Ayuda a evitar el desarrollo de cristales en las vias urinarias)</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Veterinary Diets OM Overweight Management', 2, 'Adultos', '_56137029.png', 'GeoTienda'),
(471, 67, 'A266', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Todas las edades</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas hidrolizada de bajo peso molecular (Evita reacciones alergicas), Carbohidratos purificados, Excelente sabor.</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Veterinary Diets HA Hypoallergenic', 2, 'Todas', '_714997338.png', 'GeoTienda'),
(472, 95, 'A267', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo enriquecido con 12 vitaminas, Calcio y Fosforo.</span></span></p>\r\n', NULL, 'Gatitos', 2, 'Cachorros', '_826213532.png', 'GeoTienda'),
(473, 95, 'A268', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo enriquecido con 12 vitaminas, Omega 3 y 6.</span></span></p>\r\n', NULL, 'Gatos Adultos', 2, 'Adultos', '_637325081.png', 'GeoTienda'),
(474, 68, 'A268', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De el destete hasta los 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Antioxidantes, Vitaminas (A,C), Taurina, Acido folico, Cobre,&nbsp;Fibras Vegetales (MOS), (FOS), Omega 3 y 6 (Piel y pelo saludable), Zinc.</span></span></p>\r\n', NULL, 'Kitten', 2, 'Cachorros', '_284966372.png', 'GeoTienda'),
(475, 68, 'A269', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras Vegetales (MOS) Mananooligosac&aacute;ridos&nbsp;</span></span><span style="color:#000000"><span style="font-size:16px">, (FOS) Fructooligosac&aacute;ridos, Taurina.</span></span></p>\r\n', NULL, 'Adult Cat', 2, 'Adultos', '_378488416.png', 'GeoTienda'),
(476, 68, 'A270', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor a 7&nbsp;a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Prote&iacute;nas de alta asimilaci&oacute;n, Sulfato de Condroitina y Glucosamina&nbsp;(Articulaciones saludables)&nbsp;Omega 3, Taurina, L-Carnitina (Reduccion de grasas).</span></span></p>\r\n', NULL, 'Senior Cat', 2, 'Senior', '_604189245.png', 'GeoTienda'),
(481, NULL, '', 6, '<p>test</p>\r\n', NULL, 'Accesorio con imagen', 1, '', '23_3214399.JPG', 'Local'),
(483, NULL, '', 6, '', NULL, 'Accesorio con imagen 2', 2, '', '23_752410575.JPG', 'Local'),
(486, NULL, '', 6, '<p>Correa negra Correa negra.</p>\r\n', NULL, 'Correa (Creado por MASCOTAS)', 1, '', '25_695769711.JPG', 'Local'),
(487, NULL, '', 7, '<p>PastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastillasPastilla</p>\r\n', NULL, 'Pastilla (Creado por MASCOTAS)', 1, '', '25_918091394.JPG', 'Local'),
(489, 70, 'A285', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Sistema urinario saludable, Mantenimiento del peso ideal.</span></span></p>\r\n', NULL, 'Instinctive in gravy', 2, 'Adultos', '_411900717.png', 'GeoTienda'),
(490, 70, 'A286', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Reduce el riesgo de formacion de calculos urinarios, Anima al gato a beber m&aacute;s agua.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>EN CASO DE DUDAS CONSULTE CON SU VETERINARIO.</strong></span></span>&nbsp;</p>\r\n', NULL, 'Urinary Care', 2, 'Adultos', '_298011374.PNG', 'GeoTienda'),
(491, 70, 'A287', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras alimentarias&nbsp;incluyendo el psyllium rico en muc&iacute;lagos y fibras insolubles que ayudan a estimular el tr&aacute;nsito intestinal</span>.<span style="font-size:16px">&nbsp;Favorece la eliminaci&oacute;n en las heces del pelo ingerido, impidiendo que se acumule en el est&oacute;mago y finalmente lo regurgite.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>EN CASO DE DUDAS CONSULTE CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Hairball Care', 2, 'Adultos', '_763549907.png', 'GeoTienda'),
(492, 70, 'A288', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">-17% bajo en calorias, L-Carnitina (Reduccion de grasas), Fibras y Psyllium.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>EN CASO DE DUDAS CONSULTE CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Light', 2, 'Adultos', '_265567486.png', 'GeoTienda'),
(493, 70, 'A289', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas de alta calidad (L.I.P), Omega 3 y 6 (Piel y pelo saludable), Vitaminas B, Aminoacidos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>EN CASO DE DUDAS CONSULTE CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Hair y Skin Care', 2, 'Adultos', '_55301011.png', 'GeoTienda'),
(494, 70, 'A290', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px"><strong>DIGESTIVE CARE</strong> contiene proteinas altamente digestibles (L.I.P), Complejo de fibras, prebi&oacute;ticos (FOS) y fibras (incluyendo psyllium) para promover el equilibrio de la flora intestinal y ayudar a regular el tr&aacute;nsito intestinal.</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>EN CASO DE DUDAS CONSULTE CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Digestive Care', 2, 'Adultos', '_176982407.png', 'GeoTienda'),
(495, 70, 'A291', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta los 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Optima Digestibilidad, Tolerancia Digestiva, Apetencia Reforzada,&nbsp;<br />\r\n&nbsp; Vitaminas y Minerales Adaptados al Crecimiento.</span></span></p>\r\n', NULL, 'Club Performance Feline Kitten', 2, 'Cachorros', '_879478243.png', 'GeoTienda'),
(496, 70, 'A292', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Optima Digestibilidad, Apetencia Reforzada, Niveles de Vitaminas y Minerales Adaptados.</span></span></p>\r\n', NULL, 'Club Performance Feline Adult', 2, 'Adultos', '_698624823.png', 'GeoTienda'),
(497, 70, 'A293', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Contenido moderado de prote&iacute;nas y de alta calidad. Alto contenido de &aacute;cidos grasos esenciales, Bajo cobre (contribuye a reducir su acumulaci&oacute;n en las c&eacute;lulas del h&iacute;gado)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Hepatic', 2, 'Adultos', '_63132262.png', 'GeoTienda'),
(498, 70, 'A294', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteina Hidrolizada (bajo peso molecular altamente digestible y de muy baja alergenicidad) Barrera cutanea (Complejo de ayuda a mantener sana la piel).</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Hypoallergenic (DR 25)', 2, 'Adultos', '_237916167.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(499, 70, 'A295', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Stress control,&nbsp;Skin barrier, Tolerancia digestiva, Control de bolas de pelo.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Calm', 2, 'Adultos', '_538386513.png', 'GeoTienda'),
(500, 70, 'A296', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Alta energia, Alta palatabilidad, EPA/DHA (Acidos Eicosapentaenoico y Docosahexaenoico &aacute;c. grasos Omega 3 de cadena larga&nbsp;ayudan a reducir la inflamaci&oacute;n de la mucosa intestinal manteniendo un tracto digestivo saludable)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Gastrointestinal (GI 32)', 2, 'Adultos', '_26175911.png', 'GeoTienda'),
(501, 70, 'A297', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Alta energia, Alta palatabilidad, EPA/DHA (Acidos Eicosapentaenoico y Docosahexaenoico &aacute;c. grasos Omega 3 de cadena larga&nbsp;ayudan a reducir la inflamaci&oacute;n de la mucosa intestinal manteniendo un tracto digestivo saludable)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Gastrointestinal Moderate Calorie (GIM 35)', 2, 'Adultos', '_877260558.png', 'GeoTienda'),
(502, 70, 'A298', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Seguridad digestiva, Alta energia, EPA/DHA (Acidos Eicosapentaenoico y Docosahexaenoico &aacute;c. grasos Omega 3 de cadena larga&nbsp;ayudan a reducir la inflamaci&oacute;n de la mucosa intestinal manteniendo un tracto digestivo saludable)<br />\r\nComplejo antioxidante (Vitamina E, C, Taurina y Lute&iacute;na) ayuda a preservar el ADN celular, a promover la salud del sistema inmune)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Fibre Response (FR 31)', 2, 'Adultos', '_499915163.png', 'GeoTienda'),
(503, 70, 'A299', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Disolusion de estruvita, Dilucion urinaria, Bajo RSS (dismunucion de iones que contribuyen a la formacion de cristales de estruvita y oxalato de calcio)<br />\r\nSoporte de la vejiga (Los glicosaminoglicanos (GAGs) naturalmente presentes en la mucosa de la vejiga sana disminuyen en el caso de cistitis idiop&aacute;tica)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Urinary S/O High Dilution', 2, 'Adultos', '_806525905.png', 'GeoTienda'),
(504, 70, 'A300', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Muy bajo en fosforo, Soporte vascular (Omega 3 de cadena larga como el EPA (Eicosapentaenoico) contribuyen a mantener la filtraci&oacute;n glomerular, mejorar la perfusi&oacute;n renal y minimizar la inflamaci&oacute;n celular, Sistema antiedad (Antioxidantes, Vitaminas E, C, Taurina y Lute&iacute;na, limita la degradaci&oacute;n del ADN celular) Seguridad digestiva.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Renal (RF 23)', 2, 'Adultos', '_471093957.png', 'GeoTienda'),
(505, 70, 'A301', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alto contenido de proteinas, Soporte articular (Sulfato de Condroitina&nbsp;y el cloruro de Glucosamina), Balance nutricional, Skin y coat (Omega 3 y 6) y elementos traza (Cobre, Zinc) promueven una piel sana y un pelaje brilloso)</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Obesity Management (DP 42)', 2, 'Adultos', '_495235990.png', 'GeoTienda'),
(506, 70, 'A302', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alto contenido de fibras, Soporte articular (Glucosamina y Condroit&iacute;n&nbsp;ayuda a mantener la movilidad en las articulaciones)</span><span style="font-size:16px">, Balance nutricional, EPA/DHA (Omega 3 y 6) y Oligoelementos (Cobre, Zinc) ayuda a promover una piel saludable y un pelaje brilloso)</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Satiety Support (SAT 34)', 2, 'Adultos', '_632781737.png', 'GeoTienda'),
(507, 70, 'A303', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Glucomodulacion (Regula la glucemia postprandial en los gatos diab&eacute;ticos) Alto contenido de proteinas.</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Diabetic', 2, 'Adultos', '_314336905.png', 'GeoTienda'),
(508, 70, 'A304', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Masa corporal magra (Elevado en proteinas)&nbsp;L-carnitina (ayuda a promover la utilizaci&oacute;n de las grasas a nivel celular y a restringir la ganancia de peso) Dilucion urinaria (cristales de estruvita y oxalato de calcio, generando un ambiente desfavorable para la formaci&oacute;n de los tipos principales de urolitos)</span></span></p>\r\n', NULL, 'Gatos Castrados Young Female', 2, 'Adultos', '_137917918.png', 'GeoTienda'),
(509, 70, 'A305', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Analisis Nutricional:</span> <span style="color:#000000">Masa corporal magra (Elevado en proteinas)&nbsp;</span></span><span style="color:#000000">&nbsp;<span style="font-size:16px">L-carnitina (Promueve la utilizaci&oacute;n de las grasas a nivel celular y ayuda a restringir la ganancia de peso) Moderado almidon (contribuye a restringir la obesidad y el desarrollo de un estado pre-diab&eacute;tico)</span></span></p>\r\n', NULL, 'Gatos Castrados Young Male', 2, 'Adultos', '_929976440.png', 'GeoTienda'),
(510, 70, 'A306', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alta Fibra (combinaci&oacute;n de fibras solubles (psylium) e insolubles fibras con alta capacidad higrosc&oacute;pica ayuda a limitar el consumo espont&aacute;neo), Peso corporal optimo (bajas calor&iacute;as, bajo en grasas y alta fibra, contribuye a mantener el peso corporal &oacute;ptimo en gatos castrados). Enriquecida con L-carnitina que ayuda a promover la utilizaci&oacute;n de las grasas a nivel celular)</span></span>&nbsp;</p>\r\n', NULL, 'Gatos Castrados Weight Control', 2, 'Adultos', '_638938414.PNG', 'GeoTienda'),
(511, 70, 'A307', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Senior</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px"><strong>Vitality Complex</strong> -&nbsp;Antioxidantes (Vitamina C, E, Lute&iacute;na, Taurina), Energia Optima.</span></span></p>\r\n', NULL, 'Mature Consult Stage 1', 2, 'Senior', '_16585029.png', 'GeoTienda'),
(512, 70, 'A308', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Senior</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Soporte de la Edad - Antioxidantes (Vitamina C, E, Lute&iacute;na, Taurina), Proteinas, Amino&aacute;cidos. Mantenimiento del Peso Ideal - &nbsp;Densidad energ&eacute;tica &oacute;ptima que cubre las necesidades de gatos maduros.</span></span></p>\r\n', NULL, 'Senior Consult Stage 2', 2, 'Senior', '_326923773.PNG', 'GeoTienda'),
(513, 83, '', 5, '', NULL, 'Test Alimento ed', 2, '', 'noimage.gif', 'Local'),
(514, NULL, '', 6, '', NULL, 'Test ed', 1, '', '23_866825108.jpg', 'Local'),
(515, 66, 'A25', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianos y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor vitalidad, Pelo brillante, Sabor rico y natural.</span></span></p>\r\n', NULL, 'Complete Adult Raza Medianas y Grandes (Pollo)', 1, 'Adultos', '_99004500.png', 'GeoTienda'),
(516, 66, 'A26', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Silueta ideal, Heces m&aacute;s firmes, Sin colorante artificiales.</span></span></p>\r\n', NULL, 'Complete Adult Control de Peso (Carne)', 1, 'Adultos', '_873303634.png', 'GeoTienda'),
(517, 66, 'A27', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">A partir de 1 a&ntilde;o</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Todas</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Silueta ideal, Heces m&aacute;s firmes, Sin colorante artificiales.</span></span></p>\r\n', NULL, 'Complete Adult Control de Peso (Pollo)', 1, 'Adultos', '_670689664.png', 'GeoTienda'),
(518, 66, 'A28', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Mayor de 7 a&ntilde;os</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Raza: </span></span><span style="color:#000000"><span style="font-size:16px">Medianas y Grandes</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Mejor calidad de vida, Pelo brillante, Sabor rico y natural.</span></span></p>\r\n', NULL, 'Complete Senior', 1, 'Senior', '_936512963.png', 'GeoTienda'),
(520, NULL, '', 6, '<p><span style="font-size:20px"><strong>Correa&nbsp;de nylon de 1.40 Mts en color negro y rojo.</strong></span></p>\r\n', NULL, 'Correa de nylon', 1, '', '23_635277340.jpg', 'Local'),
(521, 92, 'A267', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Grasas, Calostro, Omega 3 y 6 (Piel y pelo saludable) Condroitin Sulfato y Glucosamina contribuyen a mejorar las patolog&iacute;as articulares degenerativas y tendinosas, gracias a sus propiedades antiinflamatorias y regenerativas del cart&iacute;lago articular.</span></span></p>\r\n', NULL, 'Gatitos Premium', 2, 'Cachorros', '_768444946.png', 'GeoTienda'),
(522, 92, 'A268', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Proteinas, Grasas, Omega 3 y 6 (Piel y pelo saludable) Control de PH urinario.</span></span></p>\r\n', NULL, 'Gatos Adultos Premium', 2, 'Adultos', '_883972491.png', 'GeoTienda'),
(523, 73, 'A269', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras, L-Carnitina (Reduccion de grasas) Omega 3 y 6 (Piel y pelo saludable) Aminoacidos (Taurina)</span></span></p>\r\n', NULL, 'Kittens Optimal Growth', 2, 'Cachorros', '_540890019.png', 'GeoTienda'),
(524, 73, 'A270', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fibras, L-Carnitina (Reduccion de grasas) Omega 3 y 6 (Piel y pelo saludable) Aminoacidos (Taurina)</span></span></p>\r\n', NULL, 'Adults Complete Care', 2, 'Adultos', '_800158652.png', 'GeoTienda'),
(525, 73, 'A271', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Super Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Control de PH, Cuidado de las vias urinarias, Fibras, Aminoacidos (Taurina)</span></span></p>\r\n', NULL, 'Adults Urinary Care', 2, 'Adultos', '_251896425.png', 'GeoTienda'),
(526, 74, 'A272', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Frutos del mar</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">28% de Proteinas, Antioxidantes, Vitaminas: A,D3,E,K3,B1,B2,B5,B6,B12, Minerales</span></span></p>\r\n', NULL, 'Gatos', 2, 'Adultos', '_893264750.png', 'GeoTienda'),
(527, 76, 'A273', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Acidos grasos esenciales Omega 3 y 6 (Piel y pelo saludable) Zinc, Cobre, Vitamina A, Taurina (Buena vision y correcta funcion cardiaca).</span></span></p>\r\n', NULL, 'Gatos Adultos', 2, 'Adultos', '_642441165.png', 'GeoTienda'),
(528, 77, 'A274', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">30% de Proteinas de alta digestibilidad, Vitaminas y Minerales, Pulpa de Remolacha y Prebi&oacute;ticos, Omega 3 y 6 (Piel y pelo saludable), Calcio y F&oacute;sforo, Taurina (Buena vision y salud cardiaca) .</span></span></p>\r\n', NULL, 'Gatos Adultos', 2, 'Adultos', '_416316080.PNG', 'GeoTienda'),
(529, 77, 'A275', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">30% de Proteinas de alta digestibilidad, Vitaminas y Minerales, Pulpa de Remolacha y Prebi&oacute;ticos, Omega 3 y 6 (Piel y pelo saludable), Calcio y F&oacute;sforo, Taurina (Buena vision y salud cardiaca), Acido linoleico conjugado (ALC)&nbsp;(Control de sobrepeso y obecidad).</span></span></p>\r\n', NULL, 'Gatos Adultos (Bajas calorías)', 2, 'Adultos', '_863722262.PNG', 'GeoTienda'),
(530, 94, 'A276', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas y Minerales esenciales, como en la leche materna.<br />\r\nProteinas 34%, Fibra 4%, PH Controlado, Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Gatitos', 2, 'Cachorros', '_324180875.PNG', 'GeoTienda'),
(531, 94, 'A277', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span> <span style="color:#000000">De 12 meses a mas a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas, 30% de Proteinas, 3% de fibras, PH Controlado, Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Adultos (Carne y Pollo)', 2, 'Adultos', '_186279521.png', 'GeoTienda'),
(532, 94, 'A278', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 12 meses a mas a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon y Atun</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas, 30% de Proteinas, 3% de fibras, PH Controlado, Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Adultos (Salmón y Atún)', 2, 'Adultos', '_793190733.PNG', 'GeoTienda'),
(533, 80, 'A279', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas y Minerales esenciales, como en la leche materna.<br />\r\nCrecimiento &oacute;ptimo, Visi&oacute;n y coraz&oacute;n saludables, Pelaje suave y brillante (Omega 3 y 6),<br />\r\nReducci&oacute;n del olor y el volumen de las heces.</span></span></p>\r\n', NULL, 'Gatitos', 2, 'Cachorros', '_776569972.png', 'GeoTienda'),
(534, 80, 'A280', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="font-size:16px"><span style="color:#FF0000">* Edad:</span><font color="#ff0000"> </font><span style="color:#000000">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas. Alta digestibilidad,<br />\r\nVisi&oacute;n y coraz&oacute;n saludables, Pelaje suave y brillante (Omega 3y 6) Reducci&oacute;n del olor y el volumen de las heces.</span></span></p>\r\n', NULL, 'Gatos Adultos', 2, 'Adultos', '_432030511.png', 'GeoTienda'),
(535, 80, 'A281', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas, L-Carnitina (Reduccion de grasas), Pelaje suave y brillante (Omega 3 y 6) Reducci&oacute;n del olor y el volumen de las heces.</span></span></p>\r\n', NULL, 'Adultos Light', 2, 'Adultos', '_1879200.png', 'GeoTienda'),
(536, 81, 'A282', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas y Minerales esenciales, como en la leche materna.</span></span></p>\r\n', NULL, 'Kitten', 2, 'Cachorros', '_539368557.png', 'GeoTienda'),
(537, 81, 'A283', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas.</span></span></p>\r\n', NULL, 'Adult', 2, 'Adultos', '_204347348.PNG', 'GeoTienda'),
(538, 81, 'A284', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a mas a&ntilde;os</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Fuente de vitaminas y proteinas balanceadas, 35% menos de grasa que Iams Adult.</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;</span></span></p>\r\n', NULL, 'Light', 2, 'Adultos', '_340841309.PNG', 'GeoTienda'),
(539, 69, '', 5, '', NULL, 'LA Gata', 2, '', '32_321319794.jpg', 'Local'),
(540, 82, 'A285', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo con reducci&oacute;n cal&oacute;rica y sin exceso de fibra.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Obesidad', 2, 'Adultos', '_257625188.png', 'GeoTienda'),
(541, 82, 'A286', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo, aporta bajos niveles de Mg, mayor diluci&oacute;n urinaria y un pH urinario entre 6,2 a 6,4 que resulta desfavorable para la formaci&oacute;n de todo tipo de urolitos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Urinario', 2, 'Adultos', '_741483684.png', 'GeoTienda'),
(542, 82, 'A287', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo, Aceite de pescado (fuente de Omega 3), Vitaminas A, D3, E, C, B1, B2, B6 y B12</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* <strong>ALIMENTO UTILIZADO SOLO BAJO PRESCRIPCION MEDICA.</strong></span></span></p>\r\n', 1, 'Renal', 2, 'Adultos', '_981882983.png', 'GeoTienda'),
(543, 97, 'A288', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado, Arroz y Espinaca</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Enriquecido con taurina que ayuda al felino a tener una mejor visi&oacute;n y flexibilidad muscular.</span></span></p>\r\n', NULL, '(Pescado, Arroz y Espinaca)', 2, 'Adultos', '_997751685.png', 'GeoTienda'),
(544, 97, 'A289', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Arroz y Maiz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Enriquecido con taurina que ayuda al felino&nbsp;a tener una mejor visi&oacute;n y flexibilidad muscular.</span></span></p>\r\n', NULL, '(Carne, arroz y Maíz)', 2, 'Adultos', '_554128366.png', 'GeoTienda'),
(545, 97, 'A290', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo, Zanahoria y Espinaca</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Enriquecido con taurina que ayuda al felino a tener una mejor visi&oacute;n y flexibilidad muscular.</span></span></p>\r\n', NULL, 'Pollo, Zanahoria y Espinaca', 2, 'Adultos', '_689433339.png', 'GeoTienda'),
(546, 83, 'A291', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Hasta 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Equilibrio de calcio y fosforo, Acidos grasos Omega 3 y 6 (Piel y pelo saludable), Proteinas de alta digestibilidad.</span></span></p>\r\n', NULL, 'Kitten', 2, 'Cachorros', '_165513406.PNG', 'GeoTienda'),
(547, 83, 'A292', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">32% de Proteinas de alta digestibilidad, 13% Fat, Optima combinacion de ingredientes, Acidos grasos Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'Adult Cat', 2, 'Adultos', '_790099259.png', 'GeoTienda'),
(548, 83, 'A293', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Optima combinacion de ingredientes, Relacion adecuada de proteinas y calorias, Acidos grasos Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'Reduced Calorie Cat', 2, 'Adultos', '_45788821.png', 'GeoTienda'),
(549, 83, 'A294', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Colabora en reducir la formacion de cristales en la orina, Ayuda a mantener el tracto urinario saludable, Proteinas de alta digestibilidad.</span></span></p>\r\n', NULL, 'Urinary Cat', 2, 'Adultos', '_153843662.PNG', 'GeoTienda'),
(550, 84, 'A295', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas (C, E), Antioxidantes naturales, Omega 3 y 6 (Piel y pelo saludable), Proteinas de alta digestibilidad, Fibras naturales, Calcio y Fosforo.</span></span></p>\r\n', NULL, 'Gatos Adultos', 2, 'Adultos', '_236618246.png', 'GeoTienda'),
(551, 98, 'A296', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado y Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">De alto valor nutricional 32% Proteinas.</span></span></p>\r\n', NULL, 'Adultos', 2, 'Adultos', '_124272782.png', 'GeoTienda'),
(552, 90, 'A297', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Premium</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo, Omega 3 y 6 (Piel y pelo saludable).</span></span></p>\r\n', NULL, 'Cat', 2, 'Adultos', '_335792604.png', 'GeoTienda'),
(553, 99, 'A298', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento completo enriquecido con 12 vitaminas, 27% de proteinas, 4% de fibras.</span></span></p>\r\n', NULL, 'Gato Adulto', 2, 'Adultos', '_938442637.png', 'GeoTienda'),
(554, 100, 'A299', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, Con (DHA) para favorecer el desarrollo del cerebro, Vitaminas (A,C,E), Selenio (Antioxidante celular), Zinc.</span></span></p>\r\n', NULL, 'Gatitos', 2, 'Cachorros', '_854339148.png', 'GeoTienda'),
(555, 100, 'A300', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, con hierro que ayuda a desarrollar m&uacute;sculos fuertes, Vitaminas (A,C,E), Selenio (Antioxidante celular), Zinc.</span></span></p>\r\n', NULL, 'Adultos (Carne)', 2, 'Adultos', '_795435515.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(556, 100, 'A301', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, Con omega 3 y 6&nbsp;ayudan a mantener una piel y pelo saludable, Vitaminas (A,C,E), Selenio (Antioxidante celular), Zinc.</span></span></p>\r\n', NULL, 'Adultos (Pescado)', 2, 'Adultos', '_933958381.png', 'GeoTienda'),
(557, 100, 'A302', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, Contiene 3 fuentes de prote&iacute;na que ayudan a mantener su vitalidad, Vitaminas (A,C,E), Selenio (Antioxidante celular), Zinc.</span></span></p>\r\n', NULL, 'Adultos (DeliMix)', 2, 'Adultos', '_366044729.png', 'GeoTienda'),
(558, 100, 'A303', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="font-size:16px"><font color="#ff0000">* Edad: </font><font color="#000000">Adultos</font></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, Reducido en calor&iacute;as para ayudar a controlar su peso, Vitaminas (A,C,E), Selenio (Antioxidante Celular), Zinc.</span></span></p>\r\n', NULL, 'Adultos (Peso Saludable)', 2, 'Adultos', '_212565799.png', 'GeoTienda'),
(559, 100, 'A304', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a fortalecer las defensas naturales, Con fibras naturales que ayudan a controlar la formaci&oacute;n de bolas de pelo, Vitaminas (A,C,E), Selenio (Antioxidante celular), Zinc.</span></span></p>\r\n', NULL, 'Adultos (Hogareños)', 2, 'Adultos', '_466126398.png', 'GeoTienda'),
(560, 100, 'A305', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon y Pollo, Verduras</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Vitaminas y Minerales, Fibras y Antioxidantes.</span></span></p>\r\n', NULL, 'Adultos (Vida Sana)', 2, 'Adultos', '_207737133.png', 'GeoTienda'),
(561, 71, 'A306', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas, Complejo vitaminico mineral con Omega 3 y 6 (Piel y pelo saludable), Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Adultos (Pescado)', 2, 'Adultos', '_950323792.png', 'GeoTienda'),
(562, 71, 'A307', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne, Pescado y Arroz</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas, Complejo vitaminico mineral con Omega 3 y 6 (Piel y pelo saludable), Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Adultos (Carne, Pescado y Arroz)', 2, 'Adultos', '_750682117.png', 'GeoTienda'),
(563, 71, 'A308', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">26% de proteinas, Complejo vitaminico mineral con Omega 3 y 6 (Piel y pelo saludable), Taurina (Vision y salud cardiaca).</span></span></p>\r\n', NULL, 'Adultos (Pollo y Leche)', 2, 'Adultos', '_847213527.png', 'GeoTienda'),
(564, 101, 'A309', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a mantener el tracto urinario saludable, Fortificado con hierro y zinc.</span></span></p>\r\n', NULL, 'Gatitos (Carne y Leche)', 2, 'Cachorros', '_819650460.png', 'GeoTienda'),
(565, 101, 'A310', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a mantener el tracto urinario saludable.</span></span></p>\r\n', NULL, 'Adultos (Carne)', 2, 'Adultos', '_500252428.PNG', 'GeoTienda'),
(566, 101, 'A311', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a mantener el tracto urinario saludable.</span></span></p>\r\n', NULL, 'Adultos (Salmón)', 2, 'Adultos', '_770630405.PNG', 'GeoTienda'),
(567, 101, 'A312', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo y Leche</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a mantener el tracto urinario saludable.</span></span></p>\r\n', NULL, 'Adultos (Pollo y Leche)', 2, 'Adultos', '_794854173.PNG', 'GeoTienda'),
(568, 101, 'A313', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pescado</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Ayuda a mantener el tracto urinario saludable.</span></span></p>\r\n', NULL, 'Adultos (Pescado)', 2, 'Adultos', '_827764144.png', 'GeoTienda'),
(569, 101, 'A314', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Cocidos al vapor.</span></span></p>\r\n', NULL, 'Latas Gatitos (Carne)', 2, 'Cachorros', '_764830001.PNG', 'GeoTienda'),
(570, 101, '315', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Cocidos al vapor.</span></span></p>\r\n', NULL, 'Latas Adultos (Carne)', 2, 'Adultos', '_786676511.PNG', 'GeoTienda'),
(571, 101, '316', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Cocidos al vapor.</span></span></p>\r\n', NULL, 'Latas Adultos (Pollo)', 2, 'Adultos', '_414704304.PNG', 'GeoTienda'),
(572, 101, 'A317', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Atun</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Cocidos al vapor.</span></span></p>\r\n', NULL, 'Latas Adultos (Atún)', 2, 'Adultos', '_551226329.PNG', 'GeoTienda'),
(573, 101, '318', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">De 1 a 12 meses</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Gatitos (Carne)', 2, 'Cachorros', '_107894632.png', 'GeoTienda'),
(574, 101, '319', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pavo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adultos (Pavo)', 2, 'Adultos', '_973285864.png', 'GeoTienda'),
(575, 101, '320', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Carne</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adultos (Carne)', 2, 'Adultos', '_955498332.PNG', 'GeoTienda'),
(576, 101, '321', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Salmon</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adultos (Salmón)', 2, 'Adultos', '_139762478.png', 'GeoTienda'),
(577, 101, 'A322', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Pollo</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adultos (Pollo)', 2, 'Adultos', '_92635203.png', 'GeoTienda'),
(578, 101, 'A323', 5, '<p><span style="color:#FF0000"><span style="font-size:16px">Descripcion:<br />\r\n* Calidad: </span></span><span style="color:#000000"><span style="font-size:16px">Standard</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Edad: </span></span><span style="color:#000000"><span style="font-size:16px">Adultos</span></span><span style="color:#FF0000"><span style="font-size:16px">&nbsp;&nbsp; &nbsp;&nbsp;<br />\r\n* Sabor: </span></span><span style="color:#000000"><span style="font-size:16px">Sardina</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px">* Analisis Nutricional: </span></span><span style="color:#000000"><span style="font-size:16px">Alimento 100% balanceados y completos, Tiernos trocitos de carne en salsa cocidos al vapor.</span></span></p>\r\n', NULL, 'Pouch Adultos (Sardina)', 2, 'Adultos', '_477316506.png', 'GeoTienda'),
(584, 104, 'F01', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">La pipeta que brinda ultra protecci&oacute;n contra pulgas, garrapatas y mosquitos. Es tambi&eacute;n repelente de la mosca de la punta de la oreja y la mosca com&uacute;n. Power Ultra viene en 5 presentaciones distintas correspondiente al peso de la mascota.</span></span><br />\r\n<strong><span style="color:#FF0000"><span style="font-size:16px">Modo de uso:</span></span></strong><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Ultra', 1, '', '_179250164.png', 'GeoTienda'),
(585, 104, 'F02', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Pulguicida sist&eacute;mico de muy r&aacute;pida acci&oacute;n, no t&oacute;xico con gran poder de volteo gran residualidad y de pr&aacute;ctica administraci&oacute;n en perros.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Comprimidos se administra por v&iacute;a oral una vez al mes. Administrar junto con los alimentos o inmediatamente despu&eacute;s.</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'Comprimidos', 1, '', '_152856659.png', 'GeoTienda'),
(586, 104, 'F03', 7, '<p><strong><span style="color:#FF0000"><span style="font-size:16px">Descripcion:</span></span></strong><br />\r\n<span style="color:#000000"><span style="font-size:16px">Es una formulaci&oacute;n pulguicida, garrapaticida y repelente de garrapatas para caninos. Repelente de moscas dom&eacute;sticas, mosca de la punta de la oreja (Stomoxys calcitrans) y mosquitos. Previene por un per&iacute;odo de hasta cuatro semanas (28 d&iacute;as) utilizar a partir de los 2 meses de edad.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar en el sentido contrario al crecimiento del pelaje y aseg&uacute;rese de que todo el manto o pelaje est&eacute; empapado con el producto.</span></span>&nbsp;</p>\r\n', NULL, 'Plus Spray', 1, '', '_191270618.png', 'GeoTienda'),
(588, 105, 'F04', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFront Line Plus proporciona una protecci&oacute;n continua, con doble acci&oacute;n sobre pulgas adultas y sobre los estadios inmaduros (huevos y larvas) presentes en el medio ambiente. FRONTLINE&reg; Plus tambi&eacute;n elimina las garrapatas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Plus (Pequeños)', 1, '', '_496413929.png', 'GeoTienda'),
(589, 105, 'F05', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFront Line Plus proporciona una protecci&oacute;n continua, con doble acci&oacute;n sobre pulgas adultas y sobre los estadios inmaduros (huevos y larvas) presentes en el medio ambiente. FRONTLINE&reg; Plus tambi&eacute;n elimina las garrapatas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Plus (Medianos)', 1, '', '_694034561.png', 'GeoTienda'),
(590, 105, 'F06', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFront Line Plus proporciona una protecci&oacute;n continua, con doble acci&oacute;n sobre pulgas adultas y sobre los estadios inmaduros (huevos y larvas) presentes en el medio ambiente. FRONTLINE&reg; Plus tambi&eacute;n elimina las garrapatas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Plus (Grandes)', 1, '', '_690654242.png', 'GeoTienda'),
(591, 105, 'F07', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFront Line Plus proporciona una protecci&oacute;n continua, con doble acci&oacute;n sobre pulgas adultas y sobre los estadios inmaduros (huevos y larvas) presentes en el medio ambiente. FRONTLINE&reg; Plus tambi&eacute;n elimina las garrapatas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Plus (Gigantes)', 1, '', '_676637354.png', 'GeoTienda'),
(592, 105, 'F08', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nPor su pr&aacute;ctica aplicaci&oacute;n seguridad y eficacia FRONTLINE&reg; Spot-On, se ha convertido desde su lanzamiento en la pipeta m&aacute;s recomendada por los profesionales veterinarios del mundo. FRONTLINE&reg; Spot-On act&uacute;a en menos de 24 horas garantizando una continua protecci&oacute;n, eliminando pulgas adultas, piojos y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot-On (Pequeños)', 1, '', '_38027231.png', 'GeoTienda'),
(593, 105, 'F09', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nPor su pr&aacute;ctica aplicaci&oacute;n seguridad y eficacia FRONTLINE&reg; Spot-On, se ha convertido desde su lanzamiento en la pipeta m&aacute;s recomendada por los profesionales veterinarios del mundo. FRONTLINE&reg; Spot-On act&uacute;a en menos de 24 horas garantizando una continua protecci&oacute;n, eliminando pulgas adultas, piojos y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot-On (Medianos)', 1, '', '_18404335.png', 'GeoTienda'),
(594, 105, 'F10', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nPor su pr&aacute;ctica aplicaci&oacute;n seguridad y eficacia FRONTLINE&reg; Spot-On, se ha convertido desde su lanzamiento en la pipeta m&aacute;s recomendada por los profesionales veterinarios del mundo. FRONTLINE&reg; Spot-On act&uacute;a en menos de 24 horas garantizando una continua protecci&oacute;n, eliminando pulgas adultas, piojos y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot-On (Grandes)', 1, '', '_469541969.png', 'GeoTienda'),
(595, 105, 'F11', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nPor su pr&aacute;ctica aplicaci&oacute;n seguridad y eficacia FRONTLINE&reg; Spot-On, se ha convertido desde su lanzamiento en la pipeta m&aacute;s recomendada por los profesionales veterinarios del mundo. FRONTLINE&reg; Spot-On act&uacute;a en menos de 24 horas garantizando una continua protecci&oacute;n, eliminando pulgas adultas, piojos y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot-On (Gigantes)', 1, '', '_148124965.png', 'GeoTienda'),
(596, 105, 'F12', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFRONTLINE&reg;Spray, posee acci&oacute;n inmediata actuando en las primeras 4 horas tras la aplicaci&oacute;n del mismo, matando pulgas adultas, piojos y garrapatas. FRONTLINE&reg;Spray se puede aplicar en gatos y perros a partir de los 2 d&iacute;as de vida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar en el sentido contrario al crecimiento del pelaje y aseg&uacute;rese de que todo el manto o pelaje est&eacute; empapado con el producto.</span></span></p>\r\n', NULL, 'Spray', 1, '', '_277278261.png', 'GeoTienda'),
(597, 105, 'F13', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Repelencia de larga duraci&oacute;n frente a mosca brava, mosquitos y garrapatas. Eficacia demostrada contra pulgas y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Tri-Act (Mini)', 1, '', '_492664060.png', 'GeoTienda'),
(598, 105, 'F14', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Repelencia de larga duraci&oacute;n frente a mosca brava, mosquitos y garrapatas. Eficacia demostrada contra pulgas y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Tri-Act (Pequeños)', 1, '', '_446827128.png', 'GeoTienda'),
(599, 105, 'F15', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Repelencia de larga duraci&oacute;n frente a mosca brava, mosquitos y garrapatas. Eficacia demostrada contra pulgas y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Tri-Act (Medianos)', 1, '', '_888142887.png', 'GeoTienda'),
(600, 105, 'F16', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Repelencia de larga duraci&oacute;n frente a mosca brava, mosquitos y garrapatas. Eficacia demostrada contra pulgas y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Tri-Act (Grandes)', 1, '', '_401008696.png', 'GeoTienda'),
(601, 105, 'F17', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Repelencia de larga duraci&oacute;n frente a mosca brava, mosquitos y garrapatas. Eficacia demostrada contra pulgas y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Tri-Act (Gigantes)', 1, '', '_35594358.png', 'GeoTienda'),
(602, NULL, '', 6, '<p><strong>Casita para el perro espaciosa y funcional</strong></p>\r\n\r\n<p>Incluye recipientes para comida y agua, y ba&uacute;l para guardar bolsa.</p>\r\n\r\n<p>Material: madera</p>\r\n\r\n<p>Dimensiones: 1.00m x 0.70m x 0.50m de alto</p>\r\n', NULL, 'Casita de perro', 1, '', '34_675952367.jpg', 'Local'),
(603, 106, 'F18', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Advantix&reg; es un eficaz antiparasitario externo para perros que elimina pulgas, garrapatas y piojos masticadores. Adem&aacute;s es un extraordinario repelente contra garrapatas, flebotomos (los mosquitos transmisores de la leishmaniosis), mosquitos, y moscas de establos. &iexcl;Advantix&reg; es muy f&aacute;cil de poner, resistente al agua y seguro para toda la familia!</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar &uacute;nicamente sobre piel sana, Con el perro en posici&oacute;n de pie separar el pelaje entre los hombros hasta que la piel sea visible. Colocar la punta de la pipeta sobre la piel y apretar firmemente la pipeta varias veces para depositar todo su contenido directamente sobre la piel.</span></span></p>\r\n', NULL, 'Pipeta (Mini)', 1, '', '_36323254.png', 'GeoTienda'),
(604, 106, 'F19', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Advantix&reg; es un eficaz antiparasitario externo para perros que elimina pulgas, garrapatas y piojos masticadores. Adem&aacute;s es un extraordinario repelente contra garrapatas, flebotomos (los mosquitos transmisores de la leishmaniosis), mosquitos, y moscas de establos. &iexcl;Advantix&reg; es muy f&aacute;cil de poner, resistente al agua y seguro para toda la familia!</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar &uacute;nicamente sobre piel sana, Con el perro en posici&oacute;n de pie separar el pelaje entre los hombros hasta que la piel sea visible. Colocar la punta de la pipeta sobre la piel y apretar firmemente la pipeta varias veces para depositar todo su contenido directamente sobre la piel.</span></span></p>\r\n', NULL, 'Pipeta (Pequeños)', 1, '', '_266687318.png', 'GeoTienda'),
(605, 106, 'F20', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Advantix&reg; es un eficaz antiparasitario externo para perros que elimina pulgas, garrapatas y piojos masticadores. Adem&aacute;s es un extraordinario repelente contra garrapatas, flebotomos (los mosquitos transmisores de la leishmaniosis), mosquitos, y moscas de establos. &iexcl;Advantix&reg; es muy f&aacute;cil de poner, resistente al agua y seguro para toda la familia!</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar &uacute;nicamente sobre piel sana, Con el perro en posici&oacute;n de pie separar el pelaje entre los hombros hasta que la piel sea visible. Colocar la punta de la pipeta sobre la piel y apretar firmemente la pipeta varias veces para depositar todo su contenido directamente sobre la piel.</span></span></p>\r\n', NULL, 'Pipeta (Medianos)', 1, '', '_84409820.png', 'GeoTienda'),
(606, 106, 'F21', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Advantix&reg; es un eficaz antiparasitario externo para perros que elimina pulgas, garrapatas y piojos masticadores. Adem&aacute;s es un extraordinario repelente contra garrapatas, flebotomos (los mosquitos transmisores de la leishmaniosis), mosquitos, y moscas de establos. &iexcl;Advantix&reg; es muy f&aacute;cil de poner, resistente al agua y seguro para toda la familia!</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar &uacute;nicamente sobre piel sana, Con el perro en posici&oacute;n de pie separar el pelaje entre los hombros hasta que la piel sea visible. Colocar la punta de la pipeta sobre la piel y apretar firmemente la pipeta varias veces para depositar todo su contenido directamente sobre la piel.</span></span></p>\r\n', NULL, 'Pipeta (Grandes) ', 1, '', '_565043230.png', 'GeoTienda'),
(607, 107, 'F22', 7, '<p><span style="font-size:16px"><span style="color:#FF0000"><strong>Descripcion:</strong></span><br />\r\n<span style="color:#000000">Nexgard &reg; es una tableta masticable con sabor a carne y puede administrarse con y sin comida. Protege al perro frente a pulgas y garrapatas.</span><br />\r\n<span style="color:#FF0000"><strong>Modo de uso:</strong></span><span style="color:#000000">&nbsp;<br />\r\nSe administra oralmente una vez al mes, en dosis m&iacute;nimas de 2,5 mg/Kg. Es altamente palatable y puede ser ofrecido directamente al perro.</span></span></p>\r\n', NULL, 'Tableta Masticables', 1, '', '_935448464.png', 'GeoTienda'),
(608, 108, 'F23', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nAntiparasitario externo en pipetas para perros de aplicaci&oacute;n spot on, Pulguicida, garrapaticida y repelente de insectos con poder residual.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span><br />\r\n&nbsp;</p>\r\n', NULL, 'GMP Pipeta', 1, '', '_963603137.png', 'GeoTienda'),
(609, 109, 'F24', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario externo contra pulgas y garrapatas en caninos. Contiene (Imidacloprid) es un derivado nicot&iacute;nico con acci&oacute;n neuroactiva sobre par&aacute;sitos externos, y (Permetrina) tiene acci&oacute;n como insecticida, acaricida y repelente de insectos.<br />\r\nSu mecanismo de acci&oacute;n se basa en su neurotoxicidad por prolongaci&oacute;n de la activaci&oacute;n de los canales de sodio de las membranas pre y post sin&aacute;pticas causando una despolarizaci&oacute;n sostenida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Point Pipeta (Pequeños)', 1, '', '_893510617.png', 'GeoTienda'),
(610, 109, 'F25', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario externo contra pulgas y garrapatas en caninos. Contiene (Imidacloprid) es un derivado nicot&iacute;nico con acci&oacute;n neuroactiva sobre par&aacute;sitos externos, y (Permetrina) tiene acci&oacute;n como insecticida, acaricida y repelente de insectos.<br />\r\nSu mecanismo de acci&oacute;n se basa en su neurotoxicidad por prolongaci&oacute;n de la activaci&oacute;n de los canales de sodio de las membranas pre y post sin&aacute;pticas causando una despolarizaci&oacute;n sostenida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Point Pipeta (Medianos)', 1, '', '_125622972.png', 'GeoTienda'),
(611, 109, 'F26', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario externo contra pulgas y garrapatas en caninos. Contiene (Imidacloprid) es un derivado nicot&iacute;nico con acci&oacute;n neuroactiva sobre par&aacute;sitos externos, y (Permetrina) tiene acci&oacute;n como insecticida, acaricida y repelente de insectos.<br />\r\nSu mecanismo de acci&oacute;n se basa en su neurotoxicidad por prolongaci&oacute;n de la activaci&oacute;n de los canales de sodio de las membranas pre y post sin&aacute;pticas causando una despolarizaci&oacute;n sostenida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Point Pipeta (Grandes)', 1, '', '_146055167.png', 'GeoTienda'),
(612, 109, 'F27', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario externo contra pulgas y garrapatas en caninos. Contiene (Imidacloprid) es un derivado nicot&iacute;nico con acci&oacute;n neuroactiva sobre par&aacute;sitos externos, y (Permetrina) tiene acci&oacute;n como insecticida, acaricida y repelente de insectos.<br />\r\nSu mecanismo de acci&oacute;n se basa en su neurotoxicidad por prolongaci&oacute;n de la activaci&oacute;n de los canales de sodio de las membranas pre y post sin&aacute;pticas causando una despolarizaci&oacute;n sostenida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Point Pipeta (Gigantes)', 1, '', '_562739379.png', 'GeoTienda'),
(613, 110, 'F28', 7, '<p><strong><span style="color:#FF0000"><span style="font-size:16px">Descripcion:</span></span></strong><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nProtech 4 en 1 ejerce un control sobre formas inmaduras de par&aacute;sitos y vectores que trasmiten graves enfermedades a los perros y a las personas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo&nbsp;de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta 4 en 1', 1, '', '_94052398.png', 'GeoTienda'),
(614, 110, 'F29', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nProtech 4 en 1 ejerce un control sobre formas inmaduras de par&aacute;sitos y vectores que trasmiten graves enfermedades a los perros y a las personas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo&nbsp;de uso:</strong></span></span><br />\r\n<span style="font-size:16px"><span style="color:#000000">Aplicar en el sentido contrario al crecimiento del pelaje y aseg&uacute;rese de que todo el manto o pelaje est&eacute; empapado con el producto.</span></span></p>\r\n', NULL, 'Spray 4 en 1', 1, '', '_627666590.png', 'GeoTienda'),
(615, 110, 'F30', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nProtech 4 en 1 ejerce un control sobre formas inmaduras de par&aacute;sitos y vectores que trasmiten graves enfermedades a los perros y a las personas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo&nbsp;de uso:</strong></span></span><br />\r\n<span style="font-size:16px"><span style="color:#000000">Aplicar en el sentido contrario al crecimiento del pelaje y aseg&uacute;rese de que todo el manto o pelaje est&eacute; empapado con el producto.</span></span></p>\r\n', NULL, 'Spray 4 en 1 + Repuesto', 1, '', '_826128532.png', 'GeoTienda');
INSERT INTO `articulo` (`articulo_id`, `articulo_marca_id`, `articulo_codigo`, `articulo_rubro_id`, `articulo_detalle`, `articulo_medicados`, `articulo_nombre`, `articulo_animal_id`, `articulo_edad`, `articulo_imagen`, `articulo_creador`) VALUES
(616, 111, 'F31', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nIndicado para la prevenci&oacute;n y tratamiento de las infestaciones por pulgas, piojos, &aacute;caros de las sarna otod&eacute;ctica, sarc&oacute;ptica y demod&eacute;ctica y el &aacute;caro de pelaje o caspa andante Cheyletiella yasguri y los vermes intestinales Ancylostoma caninum, Toxocara canis y Uncinaria stenocephala, incluyendo sus larvas de cuarto estadio (L4) y adultos inmaduros; como adultos de Toxascaris leonina y Trichuris vulpis en caninos. Previene la enfermedad del gusano del coraz&oacute;n causada por el verme card&iacute;aco Dirofilaria immitis.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta', 1, '', '_660645958.png', 'GeoTienda'),
(617, 112, 'F32', 7, '<p><strong><span style="color:#FF0000"><span style="font-size:16px">Descripcion:</span></span></strong><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario interno para cachorros, efecto antiparasitario contra Coccidios, Giardias y gusanos redondos (ascaris - ancylostomas) durante los primeros meses de vida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">1 ml/kg de peso vivo cada 24 hs, durante 3 d&iacute;as seguidos administrar a partir de los 20 d&iacute;as de vida. Agitar bien antes de usar se sugiere no administrar a hembras pre&ntilde;adas debido a que no hay estudios realizados que avalen su uso en ese estad&iacute;o.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Antiparasitario interno ', 1, '', '_66088055.png', 'GeoTienda'),
(618, 113, 'F33', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Comprimidos palatables con liberaci&oacute;n controilada para perros de hasta 10, 20, 60 kgs. Cada comprimido es segun el peso vivo. Infestaciones por cestodes Dipylidium caninum o Taenia sp. y nematodes Toxacara canis, Toxascaris leonina; Ancylostoma caninum y Trichuris vulpis. Infestaciones por protozoos Giardia canis (formas qu&iacute;sticas). Se puede administrar en hembras gestantes y en lactaci&oacute;n.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\n1 comprimido cada peso vivo en 1 sola toma. En infestaciones severas repetir la dosis a las 24 hs. Desparasitar nuevamente entre los 15 y 21 d&iacute;as.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Antiparasitario interno (Comprimidos)', 1, '', '_684849142.png', 'GeoTienda'),
(619, 114, 'F34', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario interno para el control de los nematodes intestinales, para caninos y felinos suspensi&oacute;n oral.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nAgitar antes de usar la dosis total es de 90 mg/kg (1 mL por cada kilo de peso). Esta dosis debe ser administrada en una sola toma por v&iacute;a oral, el tratamiento deber&aacute; repetirse a los 20 d&iacute;as. Administrar a los cachorros apartir de los 15 d&iacute;as de edad.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Suspensión Cachorros', 1, '', '_548707317.png', 'GeoTienda'),
(620, 114, 'F35', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario interno para el control de los nematodes intestinales, para caninos en comprimidos palatables.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nUn 1 comprimido por peso del animal, en una sola toma. Los comprimidos podr&aacute;n ser suministrados directamente en la boca del animal o inclu&iacute;dos en el alimento.<br />\r\nFrente a parasitosis producidas por nematodes, repetir el tratamiento a los veinte (20) d&iacute;as. En caso de reinfestaciones por tenias, repetir el tratamiento cada cuarenta y cinco (45) d&iacute;as o de acuerdo al criterio del M&eacute;dico Veterinario.<br />\r\nSuministrar a los animales a partir de los cuarenta y cinco (45) d&iacute;as de edad y no menor de 2,5 kilos de peso.<br />\r\nNo utilizar en hembras en gestaci&oacute;n y/o en lactaci&oacute;n.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Plus Palatable (Comprimidos)', 1, '', '_648524271.png', 'GeoTienda'),
(621, 115, 'F36', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Collar 1,25 g&nbsp;+ 0,56 g&nbsp;para perros menor a 8 Kg elimina las garrapatas, pulgas y piojos repele las garrapatas (Antialimentacion) inodoro. Resistente al agua, collar de 38 cm<br />\r\nel collar protege de modo indirecto frente a la transmisi&oacute;n de los pat&oacute;genos Babesia canis vogeli y Ehrlichia canis por la garrapata Rhiphicephalus sanguineus y en consecuencia, se disminuye el riesgo de babesiosis canina y ehrliquiosis canina durante 7 meses.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Extraiga el collar de la bolsa protectora justo antes de su uso. Desenrolle el collar y aseg&uacute;rese de que no quedan restos de las tiras de pl&aacute;stico de la parte interna del collar. Ajuste el collar sin apretar alrededor del cuello del animal (se recomienda dejar un espacio de dos dedos entre el collar y el cuello). Pase el extremo del collar a trav&eacute;s de las hebillas. Corte el exceso de collar dejando 2 cm tras la hebilla.<br />\r\nEl perro debe llevar el collar de forma continua durante los 8 meses de periodo de protecci&oacute;n.<br />\r\nNo tratar a cachorros de menos de 7 semanas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR A SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Collar (Hasta 8 Kg)', 1, '', '_340251999.png', 'GeoTienda'),
(622, 115, 'F37', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Collar 4,50 g&nbsp;+ 2,03 g&nbsp;para perros mayor a 8 Kg elimina las garrapatas, pulgas y piojos repele las garrapatas (Antialimentacion) inodoro. Resistente al agua, collar de 70 cm<br />\r\nel collar protege de modo indirecto frente a la transmisi&oacute;n de los pat&oacute;genos Babesia canis vogeli y Ehrlichia canis por la garrapata Rhiphicephalus sanguineus y en consecuencia, se disminuye el riesgo de babesiosis canina y ehrliquiosis canina durante 7 meses.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Extraiga el collar de la bolsa protectora justo antes de su uso. Desenrolle el collar y aseg&uacute;rese de que no quedan restos de las tiras de pl&aacute;stico de la parte interna del collar. Ajuste el collar sin apretar alrededor del cuello del animal (se recomienda dejar un espacio de dos dedos entre el collar y el cuello). Pase el extremo del collar a trav&eacute;s de las hebillas. Corte el exceso de collar dejando 2 cm tras la hebilla.<br />\r\nEl perro debe llevar el collar de forma continua durante los 8 meses de periodo de protecci&oacute;n.<br />\r\nNo tratar a cachorros de menos de 7 semanas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR A SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Collar (Mas de 8 Kg)', 1, '', '_356510516.png', 'GeoTienda'),
(623, 116, 'F38', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Es un regenerador osteoarticular en comprimidos para perros, con vitamina (E y Selenio). Favorece una correcta osteog&eacute;nesis en cachorros y adultos de edad avanzada formando la matriz org&aacute;nica del hueso. Mejora los aplomos por contribuir a la correcta formaci&oacute;n de tendones y ligamentos, fortalece las c&aacute;psulas articulares y el cart&iacute;lago de la oreja. Previene los problemas de columna y act&uacute;a sobre todos los componentes de la articulaci&oacute;n.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nSegun la indicacion del veterinario.</span></span></p>\r\n', NULL, 'Regenerador osteoarticular', 1, '', '_537585385.png', 'GeoTienda'),
(624, 118, 'F39', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Condroprotector regenerador del cart&iacute;lago articular, disminuye la inflamaci&oacute;n y el dolor. Para caninos y felinos en comprimidos palatables.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nSegun la indicacion del veterinario.</span></span></p>\r\n', NULL, 'Condroprotector', 1, '', '_444005689.png', 'GeoTienda'),
(625, 117, 'F40', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antioxidante Condroprotector en comprimidos, mejora la calidad de vida de caninos y felinos en edad geri&aacute;trica. Mejora el estado general del paciente permitiendo al propietario disfrutar m&aacute;s y mejor de su mascota, mejora la piel y el pelo (dermatitis, prurito, mal olor, alopecia, seborrea).<br />\r\nProtege las articulaciones de su desgaste natural y favorece la macro y microcirculaci&oacute;n vascular. Protege la vejiga a nivel de mucosa mejora la visi&oacute;n y las funciones del S.N.C. Retrasa la aparici&oacute;n de los s&iacute;ntomas de envejecimiento.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nSegun la indicacion del veterinario.</span></span></p>\r\n', NULL, 'Geriatrico', 1, '', '_109147969.png', 'GeoTienda'),
(626, 119, 'F41', 7, '<p><span style="font-size:16px"><span style="color:#FF0000"><strong>Descripcion:</strong></span><br />\r\n<span style="color:#000000">Cicatrizante, antimi&aacute;sico, repelente, antimicrobiano, epitelizador y hemost&aacute;tico de alta adherencia de uso t&oacute;pico.<br />\r\nTratamiento preventivo y curativo de bicheras en todo tipo de heridas en las especies bovina, equina, ovina, caprina, porcina y canina. Con propiedades larvicidas, antimicrobianas, hemost&aacute;ticas y cicatrizantes. Puede utilizarse en animales de cualquier edad inclusive en reci&eacute;n nacidos.</span><br />\r\n<span style="color:#FF0000"><strong>Modo de uso:</strong></span><br />\r\n<span style="color:#000000">Agitar varias veces el envase antes de aplicar. Rociar uniformemente la herida o bichera desde una distancia de 10 cm, asegurando la penetraci&oacute;n del producto dentro de la herida. Aplicar tambi&eacute;n alrededor de la misma, formando un halo de 5 cm. En caso de bicheras profundas es necesario asegurarse la penetraci&oacute;n del producto en las cavidades.</span><br />\r\n<span style="color:#FF0000"><strong>ANTE CUALQUIER DUDA CONSULTE A SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Polvo en aerosol (Curabichera)', 1, '', '_121249570.png', 'GeoTienda'),
(627, 104, 'F42', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Power Met es una formulaci&oacute;n spot-on pulguicida, adulticida y larvicida, no t&oacute;xica con gran poder de volteo y excelente residualidad, de muy pr&aacute;ctica aplicaci&oacute;n en felinos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Met (Pipeta)', 2, '', '_467043721.png', 'GeoTienda'),
(628, 104, 'F43', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Es una formulaci&oacute;n spot-on pulguicida, adulticida y larvicida, no t&oacute;xica con gran poder de volteo y excelente residualidad, de muy pr&aacute;ctica aplicaci&oacute;n en felinos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot on (Pipeta)', 2, '', '_436441971.png', 'GeoTienda'),
(629, 105, 'F44', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFront Line Plus proporciona una protecci&oacute;n continua, con doble acci&oacute;n, sobre pulgas adultas y sobre los estadios inmaduros (huevos y larvas) presentes en el medio ambiente. FRONTLINE&reg; Plus tambi&eacute;n elimina las garrapatas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Plus (Pipeta)', 2, '', '_528256885.png', 'GeoTienda'),
(630, 105, 'F45', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nPor su pr&aacute;ctica aplicaci&oacute;n seguridad y eficacia FRONTLINE&reg; Spot-On, se ha convertido desde su lanzamiento en la pipeta m&aacute;s recomendada por los profesionales veterinarios del mundo. FRONTLINE&reg; Spot-On act&uacute;a en menos de 24 horas garantizando una continua protecci&oacute;n, eliminando pulgas adultas, piojos y garrapatas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Spot on (Pipeta)', 2, '', '_803374338.png', 'GeoTienda'),
(631, 105, 'F46', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nFRONTLINE&reg;Spray, posee acci&oacute;n inmediata actuando en las primeras 4 horas tras la aplicaci&oacute;n del mismo, matando pulgas adultas, piojos y garrapatas.<br />\r\nFRONTLINE&reg;Spray se puede aplicar en gatos y perros a partir de los 2 d&iacute;as de vida.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Aplicar en el sentido contrario al crecimiento del pelaje y aseg&uacute;rese de que todo el manto o pelaje est&eacute; empapado con el producto.</span></span></p>\r\n', NULL, 'Spray', 2, '', '_776009632.png', 'GeoTienda'),
(632, 108, 'F47', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nAntiparasitario externo en pipetas para gatos de aplicaci&oacute;n spot on, Pulguicida, garrapaticida y repelente de insectos con poder residual.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'GMP (Pipeta)', 2, '', '_849630417.png', 'GeoTienda'),
(633, 120, 'F48', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nIndicado para la prevenci&oacute;n y tratamiento de las infestaciones por pulgas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta', 2, '', '_310978702.png', 'GeoTienda'),
(634, 120, 'F49', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nIndicado para la prevenci&oacute;n y tratamiento de las infestaciones por pulgas y piojos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta (Hasta 8 Kg)', 2, '', '_263741438.png', 'GeoTienda'),
(635, 111, 'F50', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nIndicado para el tratamiento y prevenci&oacute;n de infestaciones por pulgas, mata las larvas de las pulgas del medio ambiente. Indicado para la prevenci&oacute;n del gusano del coraz&oacute;n (L3 y L4 Dirofilaria immitis). Tratamiento de infestaciones de Toxocara cati, Toxascaris leonina, Ancylostoma, tubaeforme, Ancylostoma braziliense, Uncinaria stenocephala, incluyendo estad&iacute;os larvales (L4) y estadios adultos e inmaduros adultos de las mencionadas especies. Indicado para el tratamiento y control de &aacute;caros del o&iacute;do.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta', 2, '', '_198482132.png', 'GeoTienda'),
(636, 111, 'F51', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nIndicado para el tratamiento y prevenci&oacute;n de infestaciones por pulgas, mata las larvas de las pulgas del medio ambiente. Indicado para la prevenci&oacute;n del gusano del coraz&oacute;n (L3 y L4 Dirofilaria immitis). Tratamiento de infestaciones de Toxocara cati, Toxascaris leonina, Ancylostoma tubaeforme, Ancylostoma braziliense, Uncinaria stenocephala, incluyendo estad&iacute;os larvales (L4) y estadios adultos e inmaduros adultos de las mencionadas especies. Indicado para el tratamiento y control de &aacute;caros del o&iacute;do.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span></p>\r\n', NULL, 'Pipeta (Hasta 8 Kg)', 2, '', '_92050401.png', 'GeoTienda'),
(637, 121, 'F52', 7, '<p><strong><span style="color:#FF0000"><span style="font-size:16px">Descripcion:</span></span></strong><br />\r\n<span style="color:#000000"><span style="font-size:16px">Suspension palatable para gatos con gotero dosificador.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Gatos cachorros y adultos 1 ml/ kg de peso vivo en una sola toma, en infestaciones severas repetir la dosis a las 24 hs. Desparasitar nuevamente entre los 15 y 21 d&iacute;as administrar a partir de los 20 d&iacute;as de vida.<br />\r\nAgitar bien antes de usar.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR A SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Suspension', 2, '', '_86376151.png', 'GeoTienda'),
(638, 113, 'F53', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Comprimidos palatables con liberaci&oacute;n controlada para gatos. Cada comprimido es para 4 kg de peso vivo.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Gatos cestodes y nematodes 1 comprimido cada 4 kg de peso vivo en una sola toma, en infestaciones severas repetir la dosis a las 24 hs. Desparasitar nuevamente entre los 15 y 21 d&iacute;as administrar a partir de los 20 d&iacute;as de vida.<br />\r\nPuede indicarse a partir de los 20 d&iacute;as de vida, a hembras pre&ntilde;adas y durante la lactancia.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Antiparasitario interno (Comprimidos)', 2, '', '_687017909.png', 'GeoTienda'),
(639, 115, 'F54', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Collar 1,25 g + 0,56 g para gatos elimina las garrapatas y pulgas. Repele las garrapatas (Antialimentacion) inodoro resistente al agua. Collar de 38 cm.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nExtraiga el collar de la bolsa protectora justo antes de su uso, desenrolle el collar y aseg&uacute;rese de que no quedan restos de las tiras de pl&aacute;stico de la parte interna del collar. Ajuste el collar sin apretar alrededor del cuello del animal (se recomienda dejar un espacio de dos dedos entre el collar y el cuello). Pase el extremo del collar a trav&eacute;s de las hebillas, corte el exceso de collar dejando 2 cm tras la hebilla.<br />\r\nEl gato debe llevar el collar de forma continua durante los 8 meses de periodo de protecci&oacute;n. No tratar a gatitos de menos de 10 semanas.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Collar', 2, '', '_328239206.png', 'GeoTienda'),
(640, 114, 'F55', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antiparasitario interno para el control de nematodes y tenias para felinos.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nCortar la pipeta a nivel de la parte superior marcada, vaciar el contenido de la misma directamente sobre la piel, separando el pelo del animal entre los hombros para evitar el lamido.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTAR CON SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Spot on (Antiparasitario interno)', 2, '', '_411495549.png', 'GeoTienda'),
(641, 118, 'F56', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Condroprotector regenerador del cart&iacute;lago articular, disminuye la inflamaci&oacute;n y el dolor. Para caninos y felinos en comprimidos palatables.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nSegun la indicacion del veterinario.</span></span></p>\r\n', NULL, 'Condroprotector', 2, '', '_809974775.png', 'GeoTienda'),
(642, 117, 'F57', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Antioxidante Condroprotector en Comprimidos, mejora la calidad de vida de caninos y felinos en edad geri&aacute;trica. Mejora el estado general del paciente permitiendo al propietario disfrutar m&aacute;s y mejor de su mascota. Mejora la piel y el pelo (dermatitis, prurito, mal olor, alopecia, seborrea).<br />\r\nProtege las articulaciones de su desgaste natural favorece la macro y microcirculaci&oacute;n vascular. Protege la vejiga a nivel de mucosa, mejora la visi&oacute;n y las funciones del S.N.C Retrasa la aparici&oacute;n de los s&iacute;ntomas de envejecimiento.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>Modo de uso:</strong></span></span><span style="color:#000000"><span style="font-size:16px">&nbsp;<br />\r\nSegun la indicacion del veterinario.</span></span></p>\r\n', NULL, 'Geriatrico', 2, '', '_873495563.png', 'GeoTienda'),
(643, 119, 'F58', 7, '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">Cicatrizante, antimi&aacute;sico, repelente, antimicrobiano, epitelizador y hemost&aacute;tico de alta adherencia de uso t&oacute;pico.<br />\r\nTratamiento preventivo y curativo de bicheras en todo tipo de heridas en las especies bovina, equina, ovina, caprina, porcina y canina. Con propiedades larvicidas, antimicrobianas, hemost&aacute;ticas y cicatrizantes. Puede utilizarse en animales de cualquier edad inclusive en reci&eacute;n nacidos.</span></span><br />\r\n<strong><span style="color:#FF0000"><span style="font-size:16px">Modo de uso:</span></span></strong><br />\r\n<span style="color:#000000"><span style="font-size:16px">Agitar varias veces el envase antes de aplicar. Rociar uniformemente la herida o bichera desde una distancia de 10 cm, asegurnado la penetraci&oacute;n del producto dentro de la herida. Aplicar tambi&eacute;n alrededor de la misma, formando un halo de 5 cm. En caso de bicheras profundas es necesario asegurarse la penetraci&oacute;n del producto en las cavidades.</span></span><br />\r\n<span style="color:#FF0000"><span style="font-size:16px"><strong>ANTE CUALQUIER DUDA CONSULTE A SU VETERINARIO.</strong></span></span></p>\r\n', NULL, 'Polvo en aerosol (Curabichera)', 2, '', '_351283666.png', 'GeoTienda'),
(644, NULL, '', 6, '<p><strong>Pechera reforzada con ganchos regulables.</strong></p>\r\n', NULL, 'Pechera reforzada', 1, '', '25_471115230.jpg', 'Local'),
(645, NULL, '', 5, '<p>detalle</p>\r\n', NULL, 'Test sin marca ni presentacion', 1, 'Cachorros', '23_765947262.jpg', 'Local'),
(646, NULL, '', 6, '', NULL, 'Test Accesorio', 1, '', 'noimage.gif', 'Local'),
(647, 1, '', 5, '<p><strong>Alimento balanceado 100 % completos</strong></p>\r\n', NULL, 'Ganador Perros Adultos 25 Kg', 1, 'Adultos', '25_548679296.png', 'Local'),
(648, 1, '', 5, '', NULL, 'Test Mi Marca', 1, '', 'noimage.gif', 'Local'),
(650, 1, '', 7, '', NULL, 'Test Farmacia de Otras Marcas', 1, '', 'noimage.gif', 'Local'),
(651, 1, '', 7, '<p><strong>Shampoo para Gatos antiparasitario.</strong></p>\r\n', NULL, 'Shampoo Antiparasitario 250 ML', 2, '', '25_796448285.png', 'Local'),
(652, 1, '', 5, '<p><strong>Alimentos de peces</strong></p>\r\n', NULL, 'Alimento para peces', 4, '', 'noimage.gif', 'Local'),
(653, 1, '', 7, '<p>Gotas</p>\r\n', NULL, 'Gotas para Hamster', 3, '', 'noimage.gif', 'Local'),
(654, NULL, '', 6, '<p>DE VIDRIO</p>\r\n', NULL, 'Reptilario', 6, '', 'noimage.gif', 'Local');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_presentacion`
--

CREATE TABLE IF NOT EXISTS `articulo_presentacion` (
  `articulo_presentacion_id` int(11) NOT NULL,
  `articulo_presentacion_articulo_id` int(11) NOT NULL,
  `articulo_presentacion_presentacion_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3969 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo_presentacion`
--

INSERT INTO `articulo_presentacion` (`articulo_presentacion_id`, `articulo_presentacion_articulo_id`, `articulo_presentacion_presentacion_id`) VALUES
(2914, 390, 52),
(2915, 391, 52),
(2916, 391, 59),
(2917, 392, 62),
(2918, 393, 52),
(2919, 393, 54),
(2920, 393, 59),
(2921, 394, 54),
(2922, 376, 53),
(2923, 376, 56),
(2924, 376, 58),
(2925, 376, 52),
(2926, 376, 67),
(2927, 377, 53),
(2928, 377, 56),
(2929, 377, 58),
(2930, 377, 52),
(2931, 377, 67),
(2932, 378, 53),
(2933, 378, 56),
(2934, 378, 58),
(2935, 378, 52),
(2936, 378, 67),
(2937, 379, 53),
(2938, 379, 56),
(2939, 379, 58),
(2940, 379, 52),
(2941, 379, 67),
(2942, 380, 53),
(2943, 380, 56),
(2944, 380, 58),
(2945, 380, 52),
(2946, 380, 67),
(2947, 381, 53),
(2948, 381, 56),
(2949, 381, 58),
(2950, 381, 67),
(2951, 382, 75),
(2952, 382, 69),
(2953, 382, 73),
(2954, 382, 55),
(2955, 382, 67),
(2956, 383, 75),
(2957, 383, 69),
(2958, 383, 73),
(2959, 383, 55),
(2960, 383, 67),
(2961, 384, 76),
(2962, 385, 77),
(2963, 386, 77),
(2964, 387, 77),
(2965, 388, 78),
(2966, 389, 78),
(2967, 367, 53),
(2968, 367, 52),
(2969, 368, 53),
(2970, 368, 56),
(2971, 368, 58),
(2972, 368, 67),
(2973, 369, 53),
(2974, 369, 56),
(2975, 369, 58),
(2976, 369, 52),
(2977, 369, 67),
(2978, 370, 53),
(2979, 370, 56),
(2980, 370, 58),
(2981, 370, 52),
(2982, 370, 67),
(2983, 371, 53),
(2984, 371, 52),
(2985, 372, 53),
(2986, 372, 52),
(2987, 372, 67),
(2988, 373, 53),
(2989, 373, 56),
(2990, 373, 52),
(2991, 374, 53),
(2992, 374, 52),
(2993, 375, 53),
(2994, 375, 56),
(2995, 375, 52),
(2996, 375, 67),
(2997, 362, 53),
(2998, 362, 56),
(2999, 362, 58),
(3000, 362, 52),
(3001, 362, 67),
(3002, 363, 53),
(3003, 363, 56),
(3004, 363, 58),
(3005, 363, 52),
(3006, 363, 67),
(3007, 364, 53),
(3008, 364, 56),
(3009, 364, 58),
(3010, 364, 52),
(3011, 364, 67),
(3012, 365, 53),
(3013, 365, 56),
(3014, 365, 67),
(3015, 366, 53),
(3016, 366, 56),
(3017, 366, 58),
(3018, 366, 52),
(3019, 366, 67),
(3020, 339, 57),
(3021, 339, 56),
(3022, 339, 51),
(3023, 339, 52),
(3024, 340, 56),
(3025, 340, 52),
(3026, 341, 56),
(3027, 341, 52),
(3028, 342, 57),
(3029, 342, 56),
(3030, 342, 51),
(3031, 342, 52),
(3032, 343, 56),
(3033, 343, 52),
(3034, 344, 56),
(3035, 344, 52),
(3036, 345, 57),
(3037, 345, 56),
(3038, 346, 56),
(3039, 346, 52),
(3040, 347, 56),
(3041, 347, 52),
(3042, 348, 57),
(3043, 348, 56),
(3044, 349, 56),
(3045, 349, 52),
(3046, 350, 56),
(3047, 350, 52),
(3048, 351, 56),
(3049, 351, 52),
(3050, 352, 57),
(3051, 352, 56),
(3052, 353, 57),
(3053, 353, 56),
(3054, 354, 57),
(3055, 354, 56),
(3056, 355, 57),
(3057, 355, 56),
(3058, 356, 57),
(3059, 356, 56),
(3060, 357, 55),
(3061, 358, 55),
(3062, 359, 55),
(3063, 360, 55),
(3064, 361, 57),
(3065, 361, 56),
(3066, 337, 52),
(3067, 338, 52),
(3068, 338, 74),
(3069, 329, 57),
(3070, 329, 56),
(3071, 329, 52),
(3072, 330, 57),
(3073, 330, 56),
(3074, 330, 52),
(3075, 330, 54),
(3076, 331, 57),
(3077, 331, 56),
(3078, 331, 52),
(3079, 332, 56),
(3080, 332, 52),
(3081, 332, 54),
(3082, 333, 56),
(3083, 333, 52),
(3084, 334, 56),
(3085, 334, 52),
(3086, 335, 52),
(3087, 335, 54),
(3088, 336, 52),
(3089, 336, 54),
(3090, 324, 50),
(3091, 324, 62),
(3092, 325, 50),
(3093, 325, 62),
(3094, 326, 50),
(3095, 326, 62),
(3096, 327, 50),
(3097, 327, 62),
(3098, 328, 50),
(3099, 328, 62),
(3100, 320, 50),
(3101, 320, 52),
(3102, 321, 50),
(3103, 321, 52),
(3104, 322, 50),
(3105, 322, 52),
(3106, 323, 52),
(3107, 315, 56),
(3108, 315, 51),
(3109, 315, 52),
(3110, 315, 59),
(3111, 318, 56),
(3112, 318, 51),
(3113, 318, 52),
(3114, 318, 59),
(3115, 318, 74),
(3116, 319, 56),
(3117, 319, 51),
(3118, 319, 52),
(3119, 309, 68),
(3120, 309, 69),
(3121, 309, 58),
(3122, 309, 52),
(3123, 309, 54),
(3124, 310, 68),
(3125, 310, 69),
(3126, 310, 58),
(3127, 310, 52),
(3128, 311, 68),
(3129, 311, 69),
(3130, 311, 58),
(3131, 311, 52),
(3132, 311, 54),
(3133, 312, 68),
(3134, 312, 69),
(3135, 312, 58),
(3136, 312, 52),
(3137, 313, 68),
(3138, 313, 69),
(3139, 313, 52),
(3140, 314, 68),
(3141, 314, 69),
(3142, 314, 58),
(3143, 314, 52),
(3144, 314, 54),
(3145, 305, 53),
(3146, 305, 52),
(3147, 306, 53),
(3148, 306, 64),
(3149, 307, 53),
(3150, 307, 64),
(3151, 307, 52),
(3152, 308, 53),
(3153, 308, 52),
(3154, 303, 56),
(3155, 303, 51),
(3156, 303, 52),
(3157, 304, 56),
(3158, 304, 51),
(3159, 304, 52),
(3160, 301, 57),
(3161, 301, 73),
(3162, 301, 52),
(3163, 302, 57),
(3164, 302, 73),
(3165, 302, 52),
(3166, 299, 58),
(3167, 299, 52),
(3168, 300, 58),
(3169, 300, 52),
(3170, 300, 54),
(3171, 451, 50),
(3172, 451, 51),
(3173, 451, 52),
(3174, 452, 50),
(3175, 452, 51),
(3176, 453, 50),
(3177, 453, 51),
(3178, 453, 52),
(3179, 454, 51),
(3180, 454, 54),
(3181, 455, 50),
(3182, 455, 51),
(3183, 455, 52),
(3184, 456, 50),
(3185, 456, 51),
(3186, 283, 68),
(3187, 283, 69),
(3188, 283, 51),
(3189, 283, 67),
(3190, 284, 69),
(3191, 284, 51),
(3192, 284, 67),
(3193, 285, 69),
(3194, 285, 51),
(3195, 285, 67),
(3196, 286, 69),
(3197, 286, 51),
(3198, 286, 64),
(3199, 287, 69),
(3200, 287, 64),
(3201, 288, 70),
(3202, 289, 71),
(3203, 290, 71),
(3204, 291, 72),
(3205, 292, 72),
(3206, 293, 72),
(3207, 294, 57),
(3208, 294, 60),
(3209, 294, 73),
(3210, 294, 52),
(3211, 449, 71),
(3212, 450, 71),
(3213, 182, 57),
(3214, 182, 56),
(3215, 182, 51),
(3216, 182, 52),
(3217, 183, 56),
(3218, 183, 52),
(3219, 184, 57),
(3220, 184, 56),
(3221, 184, 51),
(3222, 185, 57),
(3223, 185, 56),
(3224, 185, 51),
(3225, 185, 52),
(3226, 186, 56),
(3227, 186, 52),
(3228, 187, 57),
(3229, 187, 56),
(3230, 187, 51),
(3231, 188, 56),
(3232, 188, 52),
(3233, 189, 56),
(3234, 189, 52),
(3235, 190, 57),
(3236, 190, 56),
(3237, 190, 51),
(3238, 191, 57),
(3239, 191, 56),
(3240, 191, 51),
(3241, 192, 56),
(3242, 192, 52),
(3243, 193, 57),
(3244, 193, 56),
(3245, 193, 51),
(3246, 446, 57),
(3247, 446, 56),
(3248, 446, 51),
(3249, 447, 57),
(3250, 447, 56),
(3251, 447, 51),
(3252, 194, 53),
(3253, 194, 51),
(3254, 195, 53),
(3255, 195, 51),
(3256, 196, 53),
(3257, 196, 51),
(3258, 448, 56),
(3259, 448, 88),
(3260, 404, 56),
(3261, 404, 52),
(3262, 405, 56),
(3263, 405, 52),
(3264, 405, 54),
(3265, 279, 53),
(3266, 279, 52),
(3267, 280, 53),
(3268, 280, 56),
(3269, 280, 58),
(3270, 280, 52),
(3271, 280, 67),
(3272, 281, 53),
(3273, 281, 56),
(3274, 281, 58),
(3275, 281, 52),
(3276, 281, 67),
(3277, 282, 53),
(3278, 282, 52),
(3279, 395, 56),
(3280, 395, 52),
(3281, 396, 56),
(3282, 396, 52),
(3283, 397, 56),
(3284, 397, 52),
(3285, 398, 56),
(3286, 398, 52),
(3287, 399, 79),
(3288, 399, 51),
(3289, 399, 52),
(3290, 400, 56),
(3291, 400, 52),
(3292, 400, 54),
(3293, 401, 52),
(3294, 402, 53),
(3295, 403, 53),
(3296, 208, 53),
(3297, 208, 56),
(3298, 208, 58),
(3299, 208, 52),
(3300, 208, 59),
(3301, 209, 53),
(3302, 209, 56),
(3303, 209, 58),
(3304, 209, 52),
(3305, 209, 59),
(3306, 210, 53),
(3307, 210, 56),
(3308, 210, 58),
(3309, 210, 52),
(3310, 210, 59),
(3311, 211, 53),
(3312, 211, 56),
(3313, 211, 58),
(3314, 211, 52),
(3315, 211, 59),
(3316, 197, 57),
(3317, 197, 56),
(3318, 199, 57),
(3319, 199, 56),
(3320, 199, 51),
(3321, 200, 56),
(3322, 200, 51),
(3323, 201, 56),
(3324, 201, 51),
(3325, 201, 52),
(3326, 202, 56),
(3327, 202, 51),
(3328, 202, 52),
(3329, 204, 56),
(3330, 204, 51),
(3331, 205, 56),
(3332, 205, 51),
(3333, 205, 52),
(3334, 206, 56),
(3335, 206, 51),
(3336, 206, 52),
(3337, 207, 56),
(3338, 207, 51),
(3339, 460, 69),
(3340, 460, 62),
(3341, 460, 54),
(3342, 461, 69),
(3343, 461, 62),
(3344, 461, 52),
(3345, 461, 54),
(3346, 462, 69),
(3347, 462, 62),
(3348, 462, 52),
(3349, 462, 54),
(3350, 463, 69),
(3351, 463, 62),
(3352, 463, 52),
(3353, 463, 54),
(3354, 159, 50),
(3355, 159, 51),
(3356, 160, 50),
(3357, 160, 51),
(3358, 160, 52),
(3359, 161, 50),
(3360, 161, 51),
(3361, 161, 52),
(3362, 162, 50),
(3363, 162, 52),
(3364, 163, 50),
(3365, 163, 51),
(3366, 163, 52),
(3367, 164, 57),
(3368, 164, 56),
(3369, 164, 51),
(3370, 165, 57),
(3371, 165, 56),
(3372, 165, 55),
(3373, 165, 54),
(3374, 166, 57),
(3375, 166, 56),
(3376, 166, 52),
(3377, 166, 54),
(3378, 167, 56),
(3379, 167, 51),
(3380, 168, 56),
(3381, 168, 55),
(3382, 168, 54),
(3383, 169, 56),
(3384, 169, 52),
(3385, 169, 54),
(3386, 170, 54),
(3395, 518, 54),
(3396, 171, 56),
(3397, 171, 51),
(3398, 172, 56),
(3399, 172, 55),
(3400, 173, 56),
(3401, 173, 52),
(3402, 174, 56),
(3403, 174, 55),
(3404, 174, 54),
(3405, 175, 53),
(3406, 175, 52),
(3407, 175, 54),
(3408, 176, 53),
(3409, 176, 52),
(3410, 177, 53),
(3411, 177, 55),
(3412, 178, 53),
(3413, 178, 55),
(3414, 178, 54),
(3416, 179, 53),
(3417, 179, 55),
(3418, 180, 53),
(3419, 180, 55),
(3420, 181, 53),
(3421, 181, 55),
(3422, 181, 54),
(3423, 515, 53),
(3424, 515, 55),
(3425, 515, 54),
(3426, 516, 53),
(3427, 516, 54),
(3428, 517, 53),
(3429, 517, 54),
(3430, 518, 53),
(3431, 406, 80),
(3432, 406, 50),
(3433, 406, 51),
(3439, 212, 56),
(3440, 213, 61),
(3441, 213, 60),
(3442, 214, 57),
(3443, 214, 56),
(3444, 214, 51),
(3445, 215, 61),
(3446, 215, 60),
(3447, 216, 57),
(3448, 216, 56),
(3449, 217, 61),
(3450, 218, 56),
(3451, 219, 57),
(3452, 219, 56),
(3453, 220, 55),
(3454, 221, 55),
(3455, 222, 55),
(3456, 223, 55),
(3457, 224, 55),
(3458, 225, 55),
(3459, 226, 55),
(3460, 227, 55),
(3461, 228, 55),
(3462, 229, 57),
(3463, 230, 57),
(3464, 231, 57),
(3465, 231, 56),
(3466, 232, 57),
(3467, 232, 56),
(3468, 232, 51),
(3469, 232, 52),
(3470, 233, 57),
(3471, 233, 56),
(3472, 233, 51),
(3473, 234, 57),
(3474, 234, 56),
(3475, 235, 57),
(3476, 235, 56),
(3477, 236, 57),
(3478, 236, 56),
(3479, 237, 57),
(3480, 237, 56),
(3481, 238, 57),
(3482, 238, 56),
(3483, 520, 1),
(3484, 239, 56),
(3485, 239, 62),
(3486, 240, 57),
(3487, 240, 56),
(3488, 240, 52),
(3489, 241, 56),
(3490, 241, 52),
(3491, 242, 56),
(3492, 242, 52),
(3493, 243, 52),
(3494, 244, 56),
(3495, 244, 52),
(3496, 245, 63),
(3497, 245, 64),
(3498, 245, 65),
(3499, 246, 62),
(3500, 247, 57),
(3501, 247, 56),
(3502, 247, 52),
(3503, 248, 56),
(3504, 248, 52),
(3505, 249, 56),
(3506, 249, 52),
(3507, 250, 52),
(3508, 251, 56),
(3509, 251, 52),
(3510, 252, 56),
(3511, 252, 52),
(3512, 253, 62),
(3513, 257, 57),
(3514, 257, 52),
(3515, 258, 66),
(3516, 258, 52),
(3517, 259, 52),
(3518, 260, 52),
(3519, 261, 52),
(3520, 261, 54),
(3521, 262, 52),
(3522, 264, 50),
(3523, 264, 62),
(3524, 265, 50),
(3525, 265, 62),
(3526, 266, 50),
(3527, 266, 62),
(3528, 266, 52),
(3529, 267, 50),
(3530, 268, 50),
(3531, 269, 50),
(3532, 270, 50),
(3533, 270, 62),
(3534, 271, 62),
(3535, 272, 50),
(3536, 272, 62),
(3537, 273, 53),
(3538, 273, 51),
(3539, 273, 52),
(3540, 274, 53),
(3541, 274, 51),
(3542, 274, 52),
(3543, 275, 50),
(3544, 275, 62),
(3545, 276, 50),
(3546, 276, 62),
(3547, 277, 50),
(3548, 277, 62),
(3549, 278, 52),
(3550, 263, 50),
(3551, 263, 62),
(3552, 407, 50),
(3553, 408, 80),
(3554, 408, 50),
(3555, 409, 50),
(3556, 409, 51),
(3557, 410, 80),
(3558, 410, 50),
(3559, 410, 51),
(3560, 411, 80),
(3561, 412, 81),
(3562, 412, 50),
(3563, 412, 51),
(3564, 413, 81),
(3565, 413, 50),
(3566, 413, 51),
(3567, 414, 81),
(3568, 414, 50),
(3569, 414, 51),
(3570, 415, 81),
(3571, 415, 50),
(3572, 415, 51),
(3573, 416, 81),
(3574, 416, 50),
(3575, 416, 51),
(3576, 417, 51),
(3577, 418, 77),
(3578, 418, 53),
(3579, 418, 51),
(3580, 421, 77),
(3581, 421, 53),
(3582, 421, 55),
(3583, 422, 77),
(3584, 422, 53),
(3585, 422, 51),
(3586, 423, 77),
(3587, 423, 53),
(3588, 423, 51),
(3589, 472, 77),
(3590, 472, 52),
(3591, 473, 77),
(3592, 473, 62),
(3593, 473, 54),
(3594, 474, 81),
(3595, 474, 50),
(3596, 475, 81),
(3597, 475, 50),
(3598, 475, 51),
(3599, 476, 81),
(3600, 476, 50),
(3601, 197, 51),
(3602, 200, 52),
(3603, 204, 52),
(3604, 207, 52),
(3605, 432, 53),
(3606, 432, 51),
(3607, 433, 53),
(3608, 433, 51),
(3609, 434, 81),
(3610, 434, 50),
(3611, 435, 81),
(3612, 435, 50),
(3613, 435, 66),
(3614, 435, 62),
(3615, 436, 81),
(3616, 436, 50),
(3617, 436, 51),
(3618, 436, 52),
(3619, 437, 81),
(3620, 437, 50),
(3621, 437, 62),
(3622, 438, 81),
(3623, 438, 50),
(3624, 438, 51),
(3625, 439, 50),
(3626, 440, 81),
(3627, 440, 50),
(3628, 441, 81),
(3629, 441, 50),
(3630, 442, 50),
(3631, 443, 81),
(3632, 443, 50),
(3633, 444, 70),
(3634, 445, 70),
(3635, 489, 70),
(3636, 490, 81),
(3637, 490, 53),
(3638, 490, 51),
(3639, 491, 53),
(3640, 492, 81),
(3641, 492, 53),
(3642, 492, 51),
(3643, 493, 50),
(3644, 494, 50),
(3645, 495, 53),
(3646, 495, 51),
(3647, 496, 53),
(3648, 496, 51),
(3649, 497, 53),
(3650, 498, 50),
(3651, 499, 50),
(3652, 500, 50),
(3653, 501, 50),
(3654, 502, 50),
(3655, 503, 81),
(3656, 503, 53),
(3657, 503, 51),
(3658, 504, 77),
(3659, 504, 50),
(3660, 505, 53),
(3661, 506, 50),
(3662, 507, 53),
(3663, 508, 81),
(3664, 508, 53),
(3665, 508, 63),
(3666, 509, 81),
(3667, 509, 53),
(3668, 509, 63),
(3669, 510, 81),
(3670, 510, 53),
(3671, 510, 56),
(3672, 510, 51),
(3673, 510, 55),
(3674, 511, 53),
(3675, 511, 63),
(3676, 512, 53),
(3677, 512, 56),
(3678, 521, 57),
(3679, 522, 57),
(3680, 522, 56),
(3681, 522, 62),
(3682, 424, 57),
(3683, 424, 56),
(3684, 424, 51),
(3685, 425, 57),
(3686, 425, 56),
(3687, 425, 51),
(3688, 425, 52),
(3689, 426, 57),
(3690, 426, 56),
(3691, 426, 51),
(3692, 427, 57),
(3693, 427, 56),
(3694, 427, 51),
(3695, 428, 57),
(3696, 428, 56),
(3697, 428, 51),
(3698, 429, 57),
(3699, 429, 56),
(3700, 429, 51),
(3701, 430, 57),
(3702, 430, 56),
(3703, 430, 51),
(3704, 430, 52),
(3705, 464, 57),
(3706, 464, 56),
(3707, 470, 57),
(3708, 470, 56),
(3709, 471, 68),
(3710, 471, 63),
(3711, 523, 57),
(3712, 524, 57),
(3713, 524, 51),
(3714, 525, 57),
(3715, 525, 51),
(3716, 526, 77),
(3717, 526, 53),
(3718, 526, 58),
(3719, 526, 54),
(3720, 527, 57),
(3721, 527, 51),
(3722, 528, 57),
(3723, 528, 56),
(3724, 528, 51),
(3725, 529, 57),
(3726, 530, 81),
(3727, 530, 57),
(3728, 530, 58),
(3729, 531, 81),
(3730, 531, 57),
(3731, 531, 58),
(3732, 532, 81),
(3733, 532, 57),
(3734, 532, 58),
(3735, 532, 52),
(3736, 533, 53),
(3737, 533, 56),
(3738, 533, 51),
(3739, 534, 53),
(3740, 534, 56),
(3741, 534, 51),
(3742, 535, 53),
(3743, 535, 56),
(3744, 535, 51),
(3745, 536, 57),
(3746, 536, 56),
(3747, 537, 57),
(3748, 537, 56),
(3749, 537, 52),
(3750, 538, 57),
(3751, 538, 56),
(3752, 539, 90),
(3753, 540, 57),
(3754, 541, 57),
(3755, 542, 57),
(3756, 543, 77),
(3757, 543, 57),
(3758, 543, 56),
(3759, 543, 58),
(3760, 543, 52),
(3761, 544, 77),
(3762, 544, 57),
(3763, 544, 58),
(3764, 544, 52),
(3765, 545, 77),
(3766, 545, 57),
(3767, 545, 52),
(3768, 546, 57),
(3769, 546, 51),
(3770, 547, 57),
(3771, 547, 56),
(3772, 547, 51),
(3773, 547, 52),
(3774, 548, 57),
(3775, 548, 51),
(3776, 549, 57),
(3777, 549, 51),
(3778, 550, 62),
(3779, 551, 57),
(3780, 551, 62),
(3781, 552, 62),
(3782, 553, 62),
(3783, 554, 77),
(3784, 554, 57),
(3785, 554, 58),
(3786, 554, 52),
(3787, 555, 77),
(3788, 555, 57),
(3789, 555, 56),
(3790, 555, 58),
(3791, 555, 52),
(3792, 556, 77),
(3793, 556, 57),
(3794, 556, 56),
(3795, 556, 58),
(3796, 556, 52),
(3797, 557, 77),
(3798, 557, 57),
(3799, 557, 56),
(3800, 557, 58),
(3801, 557, 52),
(3802, 558, 77),
(3803, 558, 57),
(3804, 558, 56),
(3805, 558, 58),
(3806, 559, 77),
(3807, 559, 57),
(3808, 559, 58),
(3809, 560, 77),
(3810, 560, 57),
(3811, 560, 58),
(3812, 561, 77),
(3813, 561, 57),
(3814, 561, 56),
(3815, 561, 62),
(3816, 561, 52),
(3817, 562, 77),
(3818, 562, 57),
(3819, 562, 62),
(3820, 563, 77),
(3821, 563, 57),
(3822, 563, 62),
(3823, 564, 57),
(3824, 564, 62),
(3825, 565, 57),
(3826, 565, 62),
(3827, 566, 57),
(3828, 566, 62),
(3829, 567, 57),
(3830, 567, 56),
(3831, 567, 62),
(3832, 568, 57),
(3833, 568, 56),
(3834, 568, 62),
(3835, 568, 91),
(3836, 569, 72),
(3837, 570, 72),
(3838, 571, 72),
(3839, 572, 72),
(3840, 573, 70),
(3841, 574, 70),
(3842, 575, 70),
(3843, 576, 70),
(3844, 577, 70),
(3845, 578, 70),
(3856, 584, 87),
(3857, 584, 86),
(3858, 584, 85),
(3859, 584, 83),
(3860, 584, 84),
(3861, 585, 93),
(3862, 585, 92),
(3863, 585, 94),
(3864, 585, 95),
(3865, 585, 96),
(3866, 586, 97),
(3869, 589, 99),
(3870, 590, 100),
(3871, 591, 101),
(3873, 593, 99),
(3874, 594, 100),
(3875, 595, 101),
(3876, 596, 103),
(3877, 596, 102),
(3878, 596, 97),
(3879, 597, 104),
(3881, 599, 99),
(3882, 600, 100),
(3883, 601, 101),
(3884, 602, 1),
(3885, 603, 106),
(3886, 604, 107),
(3887, 605, 108),
(3888, 606, 109),
(3889, 607, 83),
(3890, 607, 110),
(3891, 607, 111),
(3892, 607, 112),
(3893, 608, 86),
(3894, 608, 85),
(3895, 608, 87),
(3896, 608, 114),
(3897, 608, 113),
(3898, 609, 119),
(3899, 610, 85),
(3900, 611, 86),
(3901, 612, 87),
(3902, 588, 119),
(3903, 592, 119),
(3904, 598, 84),
(3905, 613, 84),
(3906, 613, 118),
(3907, 613, 117),
(3908, 613, 116),
(3909, 613, 115),
(3910, 614, 97),
(3911, 615, 97),
(3912, 616, 108),
(3913, 616, 107),
(3914, 616, 120),
(3915, 617, 122),
(3916, 617, 121),
(3917, 618, 125),
(3918, 618, 124),
(3919, 618, 123),
(3920, 619, 121),
(3921, 620, 126),
(3922, 620, 125),
(3923, 620, 124),
(3924, 621, 127),
(3925, 622, 128),
(3926, 623, 129),
(3927, 623, 130),
(3928, 624, 129),
(3929, 625, 130),
(3930, 625, 132),
(3931, 623, 131),
(3932, 626, 133),
(3933, 627, 135),
(3934, 627, 134),
(3935, 628, 135),
(3936, 628, 134),
(3937, 629, 90),
(3938, 630, 90),
(3939, 631, 103),
(3940, 631, 97),
(3941, 631, 102),
(3942, 632, 113),
(3943, 632, 136),
(3944, 633, 134),
(3945, 634, 135),
(3946, 635, 134),
(3947, 636, 135),
(3948, 637, 121),
(3950, 638, 137),
(3951, 639, 90),
(3952, 640, 135),
(3953, 640, 134),
(3954, 641, 129),
(3955, 642, 130),
(3956, 642, 132),
(3957, 643, 133),
(3958, 644, 1),
(3959, 645, 1),
(3960, 646, 1),
(3961, 647, 1),
(3962, 648, 1),
(3964, 650, 1),
(3965, 651, 1),
(3966, 652, 1),
(3967, 653, 1),
(3968, 654, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_raza`
--

CREATE TABLE IF NOT EXISTS `articulo_raza` (
  `articulo_raza_id` int(11) NOT NULL,
  `articulo_raza_articulo_id` int(11) NOT NULL,
  `articulo_raza_raza_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=739 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo_raza`
--

INSERT INTO `articulo_raza` (`articulo_raza_id`, `articulo_raza_articulo_id`, `articulo_raza_raza_id`) VALUES
(588, 390, 1),
(589, 391, 1),
(592, 380, 1),
(593, 381, 1),
(594, 382, 1),
(595, 383, 1),
(596, 384, 1),
(597, 387, 1),
(598, 388, 1),
(599, 389, 1),
(600, 368, 1),
(601, 369, 1),
(602, 370, 1),
(603, 371, 1),
(604, 372, 1),
(605, 373, 1),
(606, 362, 1),
(607, 363, 1),
(608, 364, 1),
(609, 365, 1),
(610, 366, 1),
(611, 351, 1),
(612, 352, 13),
(613, 353, 6),
(614, 354, 7),
(615, 355, 14),
(616, 356, 15),
(617, 357, 12),
(618, 358, 10),
(619, 359, 9),
(620, 360, 16),
(621, 361, 4),
(622, 337, 1),
(623, 338, 1),
(624, 333, 1),
(625, 334, 1),
(626, 335, 1),
(627, 336, 1),
(628, 324, 1),
(629, 325, 1),
(630, 326, 1),
(631, 327, 1),
(632, 328, 1),
(633, 315, 1),
(634, 318, 1),
(635, 319, 1),
(636, 309, 1),
(637, 312, 1),
(638, 313, 1),
(639, 314, 1),
(640, 305, 1),
(641, 308, 1),
(642, 303, 1),
(643, 304, 1),
(644, 301, 1),
(645, 302, 1),
(646, 299, 1),
(647, 300, 1),
(648, 454, 1),
(649, 455, 1),
(650, 283, 1),
(651, 284, 1),
(652, 285, 1),
(653, 287, 1),
(654, 288, 1),
(655, 291, 1),
(656, 292, 1),
(657, 293, 1),
(658, 294, 1),
(659, 182, 1),
(660, 185, 1),
(661, 194, 1),
(662, 195, 1),
(663, 196, 1),
(664, 448, 1),
(665, 404, 1),
(666, 405, 1),
(667, 279, 1),
(668, 280, 1),
(669, 281, 1),
(670, 395, 1),
(671, 397, 1),
(672, 400, 1),
(673, 401, 1),
(674, 402, 1),
(675, 403, 1),
(676, 208, 1),
(677, 209, 1),
(678, 210, 1),
(679, 211, 1),
(680, 460, 1),
(681, 461, 1),
(682, 462, 1),
(683, 463, 1),
(687, 175, 1),
(688, 176, 1),
(689, 516, 1),
(690, 517, 1),
(691, 212, 2),
(692, 213, 3),
(693, 214, 3),
(694, 215, 4),
(695, 216, 4),
(696, 217, 5),
(697, 218, 6),
(698, 219, 7),
(699, 220, 8),
(700, 221, 9),
(701, 222, 9),
(702, 223, 10),
(703, 224, 10),
(704, 225, 11),
(705, 226, 11),
(706, 227, 12),
(707, 228, 12),
(708, 260, 1),
(709, 261, 1),
(710, 262, 1),
(711, 264, 1),
(712, 265, 1),
(713, 266, 1),
(714, 268, 1),
(715, 269, 1),
(716, 270, 1),
(717, 271, 1),
(718, 272, 1),
(719, 273, 1),
(720, 274, 1),
(721, 275, 1),
(722, 276, 1),
(723, 277, 1),
(724, 263, 1),
(725, 432, 17),
(726, 433, 18),
(727, 464, 19),
(728, 470, 19),
(729, 471, 19),
(730, 392, 1),
(731, 393, 1),
(736, 161, 1),
(737, 645, 12),
(738, 174, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulo_tamanio`
--

CREATE TABLE IF NOT EXISTS `articulo_tamanio` (
  `articulo_tamanio_id` int(11) NOT NULL,
  `articulo_tamanio_articulo_id` int(11) NOT NULL,
  `articulo_tamanio_tamanio_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1517 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulo_tamanio`
--

INSERT INTO `articulo_tamanio` (`articulo_tamanio_id`, `articulo_tamanio_articulo_id`, `articulo_tamanio_tamanio_id`) VALUES
(28, 137, 17),
(31, 138, 3),
(32, 138, 4),
(1197, 0, 21),
(1212, 390, 21),
(1213, 391, 21),
(1217, 376, 17),
(1218, 377, 4),
(1219, 377, 3),
(1220, 378, 17),
(1221, 379, 4),
(1222, 379, 3),
(1223, 380, 21),
(1224, 381, 21),
(1225, 382, 21),
(1226, 383, 21),
(1227, 384, 21),
(1228, 385, 17),
(1229, 386, 4),
(1230, 386, 3),
(1231, 387, 21),
(1232, 388, 21),
(1233, 389, 21),
(1234, 367, 17),
(1235, 368, 21),
(1236, 369, 21),
(1237, 370, 21),
(1238, 371, 21),
(1239, 372, 21),
(1240, 373, 21),
(1241, 374, 17),
(1242, 375, 4),
(1243, 375, 3),
(1244, 362, 21),
(1245, 363, 21),
(1246, 364, 21),
(1247, 365, 21),
(1248, 366, 21),
(1249, 339, 17),
(1250, 340, 3),
(1251, 341, 22),
(1252, 341, 4),
(1253, 342, 17),
(1254, 343, 3),
(1255, 344, 22),
(1256, 344, 4),
(1257, 345, 17),
(1258, 346, 3),
(1259, 347, 22),
(1260, 347, 4),
(1261, 348, 17),
(1262, 349, 3),
(1263, 350, 22),
(1264, 350, 4),
(1265, 351, 21),
(1266, 337, 21),
(1267, 338, 21),
(1268, 329, 17),
(1269, 330, 4),
(1270, 330, 3),
(1271, 331, 17),
(1272, 332, 4),
(1273, 332, 3),
(1274, 333, 21),
(1275, 334, 21),
(1276, 335, 21),
(1277, 336, 21),
(1278, 324, 21),
(1279, 325, 21),
(1280, 326, 21),
(1281, 327, 21),
(1282, 328, 21),
(1283, 320, 3),
(1284, 320, 17),
(1285, 321, 22),
(1286, 321, 4),
(1287, 322, 3),
(1288, 322, 17),
(1289, 323, 22),
(1290, 323, 4),
(1291, 315, 21),
(1292, 318, 21),
(1293, 319, 21),
(1294, 309, 21),
(1295, 310, 17),
(1296, 311, 4),
(1297, 311, 3),
(1298, 312, 21),
(1299, 313, 21),
(1300, 314, 21),
(1301, 305, 21),
(1302, 306, 17),
(1303, 307, 4),
(1304, 307, 3),
(1305, 308, 21),
(1306, 303, 21),
(1307, 304, 21),
(1308, 301, 21),
(1309, 302, 21),
(1310, 299, 21),
(1311, 300, 21),
(1312, 451, 4),
(1313, 451, 3),
(1314, 452, 17),
(1315, 453, 4),
(1316, 453, 3),
(1317, 454, 21),
(1318, 455, 21),
(1319, 456, 17),
(1320, 283, 21),
(1321, 284, 21),
(1322, 285, 21),
(1323, 286, 17),
(1324, 287, 21),
(1325, 288, 21),
(1326, 289, 17),
(1327, 290, 17),
(1328, 291, 21),
(1329, 292, 21),
(1330, 293, 21),
(1331, 294, 21),
(1332, 449, 4),
(1333, 449, 3),
(1334, 450, 4),
(1335, 450, 3),
(1336, 182, 21),
(1337, 183, 4),
(1338, 184, 17),
(1339, 185, 21),
(1340, 186, 4),
(1341, 187, 17),
(1342, 188, 4),
(1343, 188, 3),
(1344, 189, 4),
(1345, 189, 3),
(1346, 190, 17),
(1347, 191, 17),
(1348, 192, 4),
(1349, 192, 3),
(1350, 193, 17),
(1351, 446, 17),
(1352, 447, 17),
(1353, 194, 21),
(1354, 195, 21),
(1355, 196, 21),
(1356, 448, 21),
(1357, 404, 21),
(1358, 405, 21),
(1359, 279, 21),
(1360, 280, 21),
(1361, 281, 21),
(1362, 282, 17),
(1363, 395, 21),
(1364, 396, 17),
(1365, 397, 21),
(1366, 398, 17),
(1367, 399, 17),
(1368, 400, 21),
(1369, 401, 21),
(1370, 402, 21),
(1371, 403, 21),
(1372, 208, 21),
(1373, 209, 21),
(1374, 210, 21),
(1375, 211, 21),
(1385, 460, 21),
(1386, 461, 21),
(1387, 462, 21),
(1388, 463, 21),
(1397, 165, 3),
(1398, 166, 4),
(1399, 167, 17),
(1400, 168, 3),
(1401, 169, 4),
(1402, 170, 22),
(1411, 175, 21),
(1412, 176, 21),
(1413, 177, 17),
(1414, 178, 4),
(1415, 178, 3),
(1416, 179, 17),
(1417, 180, 17),
(1418, 181, 4),
(1419, 181, 3),
(1420, 515, 4),
(1421, 515, 3),
(1422, 516, 21),
(1423, 517, 21),
(1424, 518, 4),
(1425, 518, 3),
(1426, 229, 17),
(1427, 230, 17),
(1428, 231, 17),
(1429, 232, 17),
(1430, 233, 17),
(1431, 234, 17),
(1432, 235, 17),
(1433, 236, 17),
(1434, 237, 17),
(1435, 238, 17),
(1436, 239, 3),
(1437, 240, 3),
(1438, 241, 3),
(1439, 242, 3),
(1440, 243, 3),
(1441, 244, 3),
(1442, 245, 3),
(1443, 246, 4),
(1444, 247, 4),
(1445, 248, 4),
(1446, 249, 4),
(1447, 250, 4),
(1448, 251, 4),
(1449, 252, 4),
(1450, 253, 22),
(1451, 257, 22),
(1452, 258, 22),
(1453, 259, 22),
(1454, 260, 21),
(1455, 261, 21),
(1456, 262, 21),
(1457, 264, 21),
(1458, 265, 21),
(1459, 266, 21),
(1460, 267, 17),
(1461, 268, 21),
(1462, 269, 21),
(1463, 270, 21),
(1464, 271, 21),
(1465, 272, 21),
(1466, 273, 21),
(1467, 274, 21),
(1468, 275, 21),
(1469, 276, 21),
(1470, 277, 21),
(1471, 278, 4),
(1472, 263, 21),
(1475, 199, 17),
(1477, 200, 17),
(1478, 201, 3),
(1479, 202, 3),
(1480, 204, 3),
(1481, 205, 4),
(1482, 206, 4),
(1483, 207, 4),
(1484, 197, 17),
(1485, 392, 21),
(1486, 393, 21),
(1487, 394, 17),
(1500, 164, 17),
(1501, 163, 4),
(1502, 163, 3),
(1503, 162, 17),
(1504, 161, 21),
(1505, 160, 4),
(1506, 160, 3),
(1507, 159, 17),
(1508, 645, 22),
(1509, 645, 4),
(1510, 645, 3),
(1512, 647, 4),
(1513, 171, 17),
(1514, 172, 3),
(1515, 173, 4),
(1516, 174, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `banner_id` int(11) NOT NULL,
  `banner_nombre` varchar(255) NOT NULL,
  `banner_texto` varchar(255) NOT NULL,
  `banner_color` varchar(7) NOT NULL,
  `banner_fondo` varchar(7) NOT NULL,
  `banner_global` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `banner`
--

INSERT INTO `banner` (`banner_id`, `banner_nombre`, `banner_texto`, `banner_color`, `banner_fondo`, `banner_global`) VALUES
(5, 'Campaña 4 días ', 'Exclusivo por 4 días 10% de descuento en productos seleccionados!!!', '#000000', '#ff0000', 0),
(7, 'Campaña por 2 días  ', 'Exclusivo del día 25 al 26 descuento del 30% en todos los productos!!! ', '#ffffff', '#9900ff', 0),
(8, 'TESTEANDO', 'TESTEANDO', '#00ff00', '#000000', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('037a59da8bbcf55b3be30b1b83e1a889270fcc86', '200.41.186.154', 1465767771, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353736373737313b),
('09a3964082d9741d53ccc5fe77b1cf541ae8b00f', '190.30.71.117', 1465912278, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931323032313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('14c0525a358814dd6cb924eaaa89c5113c85706c', '190.30.71.117', 1465771460, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353737313435383b),
('1ef27cf9c9758e3ec5b08abb64a6a45351696789', '181.169.189.200', 1465674015, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637343031353b),
('208cac88cd2a729139a74481e05e0fbaf8aa67d5', '190.30.71.117', 1465677756, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637373636303b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('24b7a89928b46eb0ede41cf1ad71c09519f3f74b', '190.216.7.213', 1465848297, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353834383239373b),
('433c954678e512617af047e929aeb97dcf29b96b', '200.126.242.55', 1465854259, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353835343235393b),
('4e70df7a8b3529d1ccb6c2ffa67db83248e92468', '200.41.186.154', 1465750960, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353735303936303b),
('565eaca5930029254771ac70dddb788b1b51654c', '190.30.71.117', 1465831721, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353832393531383b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('5ab03fd38aa382b4dbf1957c02990b300dbb8f6b', '190.216.7.213', 1466087633, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363038373632353b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('60688689106a81cea7e6ff7ea60b742046d1ddfc', '181.169.189.200', 1465916905, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931363930353b),
('635e01be14baf67cb9826dc58de9ae67876d6e27', '200.41.186.154', 1465708940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353730383934303b),
('66d719ca9a2698a955ad5b836875bb7ec421b253', '190.216.7.213', 1466033993, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033333032383b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('69e3e1e7f31f2b98d268925104518cbfa631af8f', '200.41.186.154', 1465776012, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353737363031323b),
('6bdcd9322f6c25c9d47ec1b9a43153179139f677', '181.14.129.124', 1466020691, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363032303639313b),
('6e963dea58aa8f25f86db929e516590a00fd98ad', '190.216.7.213', 1466090475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039303437353b),
('727d8444cae268d08a2c26706df3e3c601d61137', '190.216.7.213', 1465998319, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353939383331393b),
('7a41617eb77c194e9ebcd23cc7a400d2813a348e', '181.14.129.124', 1466046035, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363034363033353b),
('962fcdfb34a55e82f1e17b3eff275898894df064', '190.216.7.213', 1466014050, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363031343034313b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('9a633e0afca2ba91700239a0a29d125e445fc9a2', '190.30.71.117', 1465915256, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353931353133313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('9b80f36a9db437bd0bd41321a208fc64b0ab29c6', '181.14.129.124', 1466046873, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363034363837333b),
('9ec3e518a32d1f59a664f854b88ea31b9c61a0fd', '190.216.7.213', 1466030057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033303035373b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('a8a8507cb90eab4675652b39fd5684be48fdcbe8', '200.41.186.154', 1465748734, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353734383733343b),
('ab87d84fe1e633b49244f319c0af418153ef2b44', '190.30.71.117', 1465672285, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637313336323b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('b98f542fedc544983bf8e7c1fb0b8dd55782b6f1', '190.30.71.117', 1465831822, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353833313733323b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('bdca31feb061a4e8a41f36b13dfe08d4d6ae4a52', '181.169.189.200', 1465674190, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637343139303b),
('beb3bdbd51cd8ae3742a99f01e557da6f20888f0', '200.41.186.154', 1465745975, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353734353937353b),
('c4db65d97aa04c027d001a3a17bf0e9a19bb8bec', '181.14.129.124', 1466095072, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039343938393b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('c9dc0a26aa1f277c8466dd4d850c050cbd937674', '200.41.186.154', 1465707940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353730373934303b),
('d58c5d30b442af5a0ca23c490fe9537736d0ec3a', '190.30.71.117', 1465674057, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353637323334343b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('d6edaeef83bbe63d659f2033e120c8b7c36f01fb', '190.216.7.213', 1466092964, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363039323936343b),
('dc636a83dabc80ad7bdbf554fd3cc4271b93c9db', '181.14.129.124', 1466039984, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363033393936363b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('e8631833c456bdd81d23017bc69f4d786ae502bc', '181.169.189.200', 1465667168, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636363631343b757365725f69647c693a31353b757365726e616d657c733a353a224776696465223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('eb4324b85ebf7f88b4fc41270d3d5d78754dfcc5', '190.216.7.213', 1466029599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363032393431313b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('efb0a80298b2d09b9fb8b3361c089f3d5544d5ab', '190.216.7.213', 1465822860, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353832323138333b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('f54ce72f90dfa49ff0e5a6ae6e9628cbb3da7682', '190.216.7.213', 1466018272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436363031343437353b757365725f69647c693a31303b757365726e616d657c733a383a22666174656e63696f223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b),
('f6453b4633fad7f6759b0aee8a9b3db9d3608011', '181.169.189.200', 1465667467, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636373436373b),
('fcb1362975fcd72c9b3884475e4b647ebf0c7644', '190.30.71.117', 1465671183, 0x5f5f63695f6c6173745f726567656e65726174657c693a313436353636363730313b757365725f69647c693a31323b757365726e616d657c733a373a2273706971756574223b6c6f676765645f696e7c623a313b69735f636f6e6669726d65647c623a303b69735f61646d696e7c623a303b);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `cliente_id` int(11) NOT NULL,
  `cliente_nombre` text NOT NULL,
  `cliente_email` text NOT NULL,
  `cliente_apellido` text NOT NULL,
  `cliente_telefono` text NOT NULL,
  `cliente_password` text NOT NULL,
  `cliente_direccion` text NOT NULL,
  `cliente_localidad` text NOT NULL,
  `cliente_provincia_id` int(11) NOT NULL,
  `cliente_avatar` varchar(255) NOT NULL DEFAULT 'cliente_avatar_mini.png',
  `cliente_status` int(11) NOT NULL DEFAULT '1',
  `cliente_cookie` varchar(180) NOT NULL,
  `cliente_admin` int(11) NOT NULL DEFAULT '0',
  `cliente_fecha_alta` datetime NOT NULL,
  `cliente_usuario_alta` varchar(200) NOT NULL,
  `cliente_fecha_modificacion` datetime NOT NULL,
  `cliente_usuario_modificacion` varchar(200) NOT NULL,
  `cliente_fecha_baja` datetime NOT NULL,
  `cliente_usuario_baja` varchar(200) NOT NULL,
  `cliente_fecha_suspension` datetime NOT NULL,
  `cliente_usuario_suspension` varchar(200) NOT NULL,
  `cliente_fecha_habilitacion` datetime NOT NULL,
  `cliente_usuario_habilitacion` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`cliente_id`, `cliente_nombre`, `cliente_email`, `cliente_apellido`, `cliente_telefono`, `cliente_password`, `cliente_direccion`, `cliente_localidad`, `cliente_provincia_id`, `cliente_avatar`, `cliente_status`, `cliente_cookie`, `cliente_admin`, `cliente_fecha_alta`, `cliente_usuario_alta`, `cliente_fecha_modificacion`, `cliente_usuario_modificacion`, `cliente_fecha_baja`, `cliente_usuario_baja`, `cliente_fecha_suspension`, `cliente_usuario_suspension`, `cliente_fecha_habilitacion`, `cliente_usuario_habilitacion`) VALUES
(2, 'Juan', 'jperez@gmail.com', 'Perez', '121212', '$2y$10$KGthccrgSRxtoZDaifyaZO5keJgwEeVr/uMmsP6.yxwvNuAQXJUcS', 'dirección juan perez', 'dirección juan perez', 5, '13.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(3, 'Carlos', 'crodriguez@homail.com', 'Rodriguez', '11223344', '$2y$10$OzSmoskQr1jX3G5PluG0S.gMRR3D4c6PwlorSuFX9uJrdfUWaKWFe', 'madero 1474', 'San Fernando', 5, '15.png', 0, '95d6579054f49cd9672aa7197db79f03', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-08 11:44:00', 'fatencio', '0000-00-00 00:00:00', ''),
(12, 'Francisco', 'Info@arteanimal.com', 'Atencio', '4761-8182', '$2y$10$vSiPgYIcdnvIah8.uUl8WO1tr/0VVvFQZrv/RI58QGtDM76gGdr..', '', '', 5, '8.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(13, 'German', 'Germanvans_@hotmail.com', 'Vide', '15-1234-5678', '$2y$10$UslZ3h2Gflsb4oR/NMb3TObE8BO4AkMYJ7dCFrA32hu/iMDlwxU3K', 'Sarmiento 1590', 'San Fernando', 1, '10.png', 1, '49f9df493e2adc8650e12de418859779', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 18:33:22', 'Gvide', '0000-00-00 00:00:00', ''),
(14, 'Sebastián', 'sebi_piquet@hotmail.com', 'Piquet', '4444-4444', '$2y$10$txHI2gOcI4fbHk6eKmzSP.3WJcH5gKoG/tn0wVq4SCmNcJ2SUx0g2', 'Vergara 3646 1A', 'Florida Oeste', 1, '8.png', 1, '593969cbdd9de5fbe8f20280ec1ab705', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-06 14:20:20', '', '0000-00-00 00:00:00', ''),
(17, 'Juan', 'perro@gmail.com', 'Perez', '', '$2y$10$QVlC/2F4t1yslDBVfl7xvu.Btl020ryHC83R69sSl7tXqjcu2qTn6', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(21, 'mario', 'abv@hotmail.com', 'lopez', '', '$2y$10$epMR2pFxU5urjQy9CIFWyuDdIb8JfeHJpYjCpR7fwkgjZdNrMgEBi', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(22, 'Nombre Largo', 'c@c.com', 'Apellido', '112233', '$2y$10$ZzeeAzk5YXbPg.9HS2GZAeu43giBLRd6ugOyriwLzUv0JA6MoBNWG', 'Antártida Argentina 439', 'Luján de Cuyo', 4, '15.png', 1, '09bb663a72eda2698e6b04f54e811ea8', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(27, 'Pepe', 'pepe@perez.com', 'Perez', '', '$2y$10$YiV3AP4vi1SGI5cy4mK7Be8Csntb9eAchOVi6cVC/SfkZZR0O0PA6', '', '', 5, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(28, 'Christian', 'Turkototem@hotmail.com', 'Zanabria', '1111-1111', '$2y$10$SILc6iZtE4bbKRDitWr3/e1sXivB/p65YyLwFPwFfBDlz9mjKK2Lm', 'Concordia 5353', 'Villa devoto', 5, 'cliente_avatar_mini.png', 1, '5efde6af499062193927d6a719e111f0', 0, '0000-00-00 00:00:00', '', '2017-08-12 15:41:10', 'Gvide', '0000-00-00 00:00:00', '', '2017-08-09 18:34:06', 'Gvide', '0000-00-00 00:00:00', ''),
(30, 'Fernando', 'fguzman@hotmail.com', 'Guzmán', '', '$2y$10$1GWF9D8mMMnUwEVvzl3He.dhAAi3jBFhr0RHhjbdBD59YfwcBIU92', '', '', 5, 'cliente_avatar_mini.png', 0, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 18:32:48', 'Gvide', '0000-00-00 00:00:00', ''),
(32, 'Jhonatan', 'jbojacamarin93@gmail.com', 'Marin', '', '$2y$10$GXuiD7NOsFXe8JGy8IGGAODqAD3lxYubDVUvR3iIG0H6ghXswbEP.', '', '', 5, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(34, 'May', 'piquet@mail.com', 'Laloca', '47941394', '$2y$10$4g5htI1Hoq0wKNZN39aQYOFgEZrTv5UtB7rQB2s/5K5alQp9DhPv2', '', '', 5, '2.png', 1, 'aea599c242bfe6a3d2352ba7517ba293', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(35, 'Francisco', 'fatencio@gmail.com', 'Atencio', '2615118372', '$2y$10$sQBUD98hKh9IDwOQDJdVu.kFfGs9hhR5l/wHhnKhedrV9ueJm3k6u', '', '', 5, 'cliente_avatar_mini.png', 1, '800d254d3a503f67b61edc5903f85007', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(37, 'gaston', 'gastongomez1983@gmail.com', 'gomez', '', '$2y$10$ouSRAMiw4W38JeYMl2hIrONDx2BvtIqXMHhlEQO0j.cZqDUzXWtj6', '', '', 5, 'cliente_avatar_mini.png', 1, '4a79d087787823eaa2da5c5ff1a4b194', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(39, 'elvira', 'elvirapiquet@hotmail.com', 'mama', '47991669', '$2y$10$JqzXfBsx9hFk/GDGzZfcT.oJ.Y1x9pjwn2hYN4bXrleVBWQ9YGkPq', '', '', 0, 'cliente_avatar_mini.png', 1, 'f0775db7c272f1d02f4d6d804194cdc7', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(40, 'ale', 'scopaalejandro@gmail.com', 'magno', '', '$2y$10$8nXkk4nNxyt7vzmSGF3M5Of2xn3DNpjKvAR8kHU.mQ0Vf63PmLByK', '', '', 5, '329761886.jpg', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-08 10:16:42', 'fatencio', '0000-00-00 00:00:00', ''),
(41, 'Florencia', 'florscopa@hola.com', 'Scopa', '', '$2y$10$Zc5FWjt4bK9aJneGurwp/enSgTW3NXxNXxEQfdDebhQtqHonzu.Tq', '', '', 5, '14.png', 1, '710b8f4255011ef719b3bffbe5b22313', 0, '0000-00-00 00:00:00', '', '2017-08-06 15:17:31', '', '0000-00-00 00:00:00', '', '2017-08-06 14:18:32', '', '0000-00-00 00:00:00', ''),
(42, 'Manuel', 'casa@hotmail.com', 'Limp', '4745-4420', '$2y$10$QqKaQRwFykfCr830x4kuIu5cIUQ0ijzc8teIYyfQWbBpZk5rrvgOm', '', '', 5, '2.png', 0, '47fa30f18e764962c1a98b3d62f40c95', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 18:33:35', 'Gvide', '0000-00-00 00:00:00', ''),
(43, 'Pedro', 'p@p.com', 'Carreras', '123', '$2y$10$VY1Anqh7jvsF4lVI51v0MeBwcFTIRVDXazL4B4/1nrvAR3foiiP9K', 'Antártida Argentina 439', 'Luján de Cuyo', 12, '9.png', 1, '812c040ed61f875e062e3da43754a4ef', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(45, 'Natalia', 'mnataliameyer@gmail.com', 'Meyer', '1557490567', '$2y$10$M0GoAbN1XzlApo2aCwXAHOog33h2INfkiGkhyeBoEWFO4oF4dpK8a', '', '', 5, '3.png', 1, '8f874f316ef51399f0357ac658dd2835', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(47, 'test mail', 'fff@ccc.com', 'apellido', '', '$2y$10$bmzxLR/Aoajp1hCTWoEVbecW3Ce6XtLOuBlDYM9stuq6sOQEarhiO', '', '', 5, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-05 11:30:51', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(49, 'fulanito', 'fulanito2@hotmail.com', 'menganito', '3333-3333', '$2y$10$Mnft3tihHB8Xfnw0YYfVqux2AvFKcgV/qTJa9UZpIA8rjpa4fJgpm', '', '', 5, 'cliente_avatar_mini.png', 0, '56082cc74dbd71b901ca1f20a8bb3b0a', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 18:33:06', 'Gvide', '0000-00-00 00:00:00', ''),
(50, 'Fulano', 'fulano@ito.com', 'Ito', '', '$2y$10$6vwrtsPl1R3PUPe4FnMfxeHnhO8Q0KpUGA.jpSpRBmTvGKQ./zvpu', '', '', 2, 'cliente_avatar_mini.png', 1, '618f8b9d8b3a9b75a100d8483854cbfc', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(51, 'Luciano', 'lucho@gmail.com', 'Pepe', '1111-1111', '$2y$10$hAJ/.8GYjW/z2StXkfevDusnRJ7uPzHQAecIekhsBi6vKa97kARLi', 'Monteverda 4208', 'Olivos', 1, 'cliente_avatar_mini.png', 1, 'a037491c63246a034718530946f54071', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(52, 'Federico', 'fede@gmail.com', 'Piquet', '', '$2y$10$DBDzZk7sZN2KH36Z2lj9wOzwDZOf.SJWi2Nuo/LlmzN1hE3TzNNQ2', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(53, 'Luciana', 'lucianacmp@outlook.com', 'Martinez Plastina', '', '$2y$10$pyrwsz.a882OekI1p00Jk.5yhb1swOG6AhEtS1Vox1903PKw3Gt1W', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(54, 'Test ed', 't@t.com', 'Test', '', '$2y$10$04Km91o/oZabsHbainx1G.43bZfx3S8xqKlC5gUfXzUhDDJz3nwp.', '', '', 0, 'cliente_avatar_mini.png', 0, '', 0, '0000-00-00 00:00:00', '', '2017-08-08 11:44:13', 'fatencio', '2017-08-14 22:07:30', 'fatencio', '2017-08-05 11:40:06', 'fatencio', '0000-00-00 00:00:00', ''),
(55, 'Test 2', 't2@t.com', 'Test 2', '222', '$2y$10$brLibIMajR5M6p4boLH6..DTkq9Lb2krxpNvpA/S9f9lmvmPuPh0i', 'Dir', 'Loc', 15, 'cliente_avatar_mini.png', 0, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-14 22:07:25', 'fatencio', '2017-08-08 11:44:21', 'fatencio', '0000-00-00 00:00:00', ''),
(56, 'rogelio', 'sportcarsdetailing@hotmail.com', 'pitufo', '1414-1515', '$2y$10$x5Sudx9F2PQuADCw5kNyaOO4F8gF5lLbf9SbJ3wMEb5l.Z9ZhMJ7C', 'Diaz Velez 400', 'La lucila', 1, 'cliente_avatar_mini.png', 1, 'b39c59d9c5030edaf0e36c322ce01c64', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(64, 'test', 'test@test.com', 'apellido', '', '$2y$10$HstOKMIBmfVeRnoibdtCFOzEQLdjlCNIYI0lTIJgu73JbuTPZFxci', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-05 11:31:01', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(65, 'Test cliente editado', 'fatencio@gmail.com22', 'Atencio', '4851-1788', '$2y$10$bJkQXa6cxNmPY8y5ZMaeIuq9GFrkloh9jypOZGRWUB8segzWzNcCe', 'Antártida Argentina 439', 'Luján de Cuyo', 2, 'cliente_avatar_mini.png', 1, '', 0, '2017-08-05 11:30:34', 'fatencio', '2017-08-05 11:31:13', 'fatencio', '2017-08-05 11:31:19', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(66, 'pepito', 'pepito@yahoo.com', 'don jode', '', '$2y$10$tZb3CZtRZC1Jc49pQETNa.D030K3kxN/GFjGFRucwGMZzNG9JZ1p.', '', '', 0, 'cliente_avatar_mini.png', 1, '', 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(67, 'Hola', 'quetal@hotmail.com', 'sopa', '7877-9999', '$2y$10$1I9VIF5eYjY3f7./2aIZBOJGZKK8LWjGTal4GLKuqiF8nL..n6x9O', 'alsina 1500', 'san fernando', 1, 'cliente_avatar_mini.png', 1, '', 0, '2017-08-09 20:56:19', 'Gvide', '2017-08-09 20:58:32', 'Gvide', '2017-08-09 21:17:26', '', '2017-08-09 21:16:56', 'Gvide', '0000-00-00 00:00:00', ''),
(68, 'Test', 't@t.com', 'Audit', '111', '$2y$10$IP83f8Woo/fHtNV8eZHWR.yui4/WfWbdFza1tNQbZqMH9pnnWH0V6', 'Antártida Argentina 439', 'Luján de Cuyo', 2, 'cliente_avatar_mini.png', 1, '50906f9b3b39bd58c46bff89fe9f9a3e', 0, '2017-08-14 22:07:49', 'fatencio', '0000-00-00 00:00:00', '', '2017-08-14 22:26:50', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(69, 'maria', 'maria@gmail.com', 'zarasa', '4785-123456', '$2y$10$.qor4IFjRRvjX8O9P7VfKuqsqJukLy.t45iXA8D2x.fCG/4PTnXsi', '', '', 0, 'cliente_avatar_mini.png', 0, '', 0, '2017-08-15 20:41:44', 'maria@gmail.com', '2017-08-15 20:43:59', 'maria@gmail.com', '2017-08-15 20:51:31', 'Gvide', '2017-08-15 20:51:21', 'Gvide', '2017-08-15 20:50:59', 'Gvide');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE IF NOT EXISTS `configuracion` (
  `configuracion_id` int(11) NOT NULL,
  `configuracion_item` varchar(255) NOT NULL,
  `configuracion_valor` varchar(255) NOT NULL,
  `configuracion_valor2` varchar(2500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`configuracion_id`, `configuracion_item`, `configuracion_valor`, `configuracion_valor2`) VALUES
(1, 'portada_landing', '256496818.jpg', '0px'),
(2, 'portada_local_listado', '553056203.jpg', '-129px'),
(3, 'portada_contacto_local', '120065949.jpg', '-240px'),
(4, 'portada_terminos', '271794301.jpg', '-260px'),
(5, 'portada_politicas', '365506578.jpg', '-463px'),
(6, 'portada_faq', '256496818.jpg', '0px'),
(7, 'portada_contacto', '513949373.jpg', '-291px'),
(8, 'portada_mapa', '285083283.jpg', '-11px'),
(9, 'portada_dashboard', '201924878.jpg', '-76px'),
(10, 'portada_restablecer', '201924878.jpg', ' 0px'),
(11, 'portada_beneficios', '38759271.jpg', '-323px'),
(12, 'portada_local_articulos', '284603737.jpg', '-91px'),
(13, 'local_precio_destacado', '150', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `descuento`
--

CREATE TABLE IF NOT EXISTS `descuento` (
  `descuento_id` int(11) NOT NULL,
  `descuento_nombre` varchar(50) DEFAULT NULL,
  `descuento_porcentaje` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `descuento`
--

INSERT INTO `descuento` (`descuento_id`, `descuento_nombre`, `descuento_porcentaje`) VALUES
(5, 'Sin Descuento', 0),
(26, '10% de descuento', 10),
(28, '20% de descuento', 20),
(29, '30% de descuento', 30),
(30, '40% de descuento', 40),
(31, '50% de descuento', 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE IF NOT EXISTS `local` (
  `local_id` int(11) NOT NULL,
  `local_nombre` varchar(255) NOT NULL,
  `local_nombre_titular` varchar(255) NOT NULL,
  `local_apellido_titular` varchar(255) NOT NULL,
  `local_email` varchar(255) NOT NULL,
  `local_telefono` varchar(255) NOT NULL,
  `local_email2` varchar(255) NOT NULL DEFAULT '',
  `local_telefono2` varchar(255) NOT NULL DEFAULT '',
  `local_password` varchar(60) NOT NULL,
  `local_direccion` varchar(255) NOT NULL,
  `local_localidad` varchar(255) NOT NULL,
  `local_provincia_id` int(11) NOT NULL,
  `local_latitud` varchar(50) NOT NULL DEFAULT '',
  `local_longitud` varchar(50) NOT NULL DEFAULT '',
  `local_horario_atencion` varchar(255) NOT NULL,
  `local_horario_sabados` varchar(255) NOT NULL DEFAULT '',
  `local_horario_domingos` varchar(255) NOT NULL DEFAULT '',
  `local_costo_envio` varchar(255) NOT NULL,
  `local_texto_banner` varchar(255) NOT NULL DEFAULT '',
  `local_color_banner` varchar(7) NOT NULL,
  `local_fondo_banner` varchar(7) NOT NULL,
  `local_banner_id` int(11) DEFAULT NULL,
  `local_envio_domicilio` int(11) NOT NULL DEFAULT '1',
  `local_zona_entrega` varchar(255) NOT NULL,
  `local_dias_entrega` varchar(255) NOT NULL,
  `local_categoria` varchar(50) NOT NULL,
  `local_valoracion` varchar(50) NOT NULL DEFAULT '',
  `local_avatar` varchar(255) NOT NULL DEFAULT 'local_avatar_mini.png',
  `local_status` int(11) NOT NULL DEFAULT '1',
  `local_fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `local_cookie` varchar(180) NOT NULL,
  `local_destacado` tinyint(1) NOT NULL DEFAULT '0',
  `local_urgencias` tinyint(1) NOT NULL DEFAULT '0',
  `local_telefono_urgencias` varchar(50) NOT NULL,
  `local_urgencias_domicilio` tinyint(1) NOT NULL DEFAULT '0',
  `local_urgencias_consultorio` tinyint(1) NOT NULL DEFAULT '0',
  `local_fecha_alta` datetime NOT NULL,
  `local_usuario_alta` varchar(200) NOT NULL,
  `local_fecha_modificacion` datetime NOT NULL,
  `local_usuario_modificacion` varchar(200) NOT NULL,
  `local_fecha_baja` datetime NOT NULL,
  `local_usuario_baja` varchar(200) NOT NULL,
  `local_fecha_suspension` datetime NOT NULL,
  `local_usuario_suspension` varchar(200) NOT NULL,
  `local_fecha_habilitacion` datetime NOT NULL,
  `local_usuario_habilitacion` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`local_id`, `local_nombre`, `local_nombre_titular`, `local_apellido_titular`, `local_email`, `local_telefono`, `local_email2`, `local_telefono2`, `local_password`, `local_direccion`, `local_localidad`, `local_provincia_id`, `local_latitud`, `local_longitud`, `local_horario_atencion`, `local_horario_sabados`, `local_horario_domingos`, `local_costo_envio`, `local_texto_banner`, `local_color_banner`, `local_fondo_banner`, `local_banner_id`, `local_envio_domicilio`, `local_zona_entrega`, `local_dias_entrega`, `local_categoria`, `local_valoracion`, `local_avatar`, `local_status`, `local_fecha_creacion`, `local_cookie`, `local_destacado`, `local_urgencias`, `local_telefono_urgencias`, `local_urgencias_domicilio`, `local_urgencias_consultorio`, `local_fecha_alta`, `local_usuario_alta`, `local_fecha_modificacion`, `local_usuario_modificacion`, `local_fecha_baja`, `local_usuario_baja`, `local_fecha_suspension`, `local_usuario_suspension`, `local_fecha_habilitacion`, `local_usuario_habilitacion`) VALUES
(23, 'Pájaro Loco', 'Sebastián', 'Piquet', 'loco@gmail.com', '4761-8183', '', '', '$2y$10$oWwlg4RGTUkUIuaVpo8EtOK.K8XsKcg6hTbxIdKgnxZ.F1jYsJfzG', 'Monteverde 4208', 'Olivos', 1, '-34.4995355', '-58.49884850000001', '9hs-12hs y 15hs-20hs', '9hs-13hs', '', '$50 - Gratis para compras superiores a $1000', 'Ingresa tus promociones y descuentos', '#ff0000', '#00ff00', NULL, 1, 'Zona Norte', 'Lunes a Viernes', 'Petshop', '', '977610437.png', 0, '0000-00-00 00:00:00', '379ab3d8524b968caf4790b1f87e8510', 0, 1, '1234', 0, 1, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-15 20:49:15', 'Gvide', '2017-08-15 20:48:57', 'Gvide', '0000-00-00 00:00:00', ''),
(25, 'Mascotas', 'Mario', 'Vallejos', 'el_capitan_chicho@hotmail.com', '4746-5858', 'casa@gmail.com', '1111-8888', '$2y$10$XdgrOrwPYikaOtAAsFFYguQypecNH5ibL1Mj663aya2uwKdw0Qfem', '3 de febrero 64', 'San Fernando', 1, '-34.4352954', '-58.565441899999996', 'de 9:00 a 13:00 y de 14:00 a 19:00', '9:30 a 13:30', '10:00 a 13:00', 'Gratis a partir de compras de $300', '', '#ff0000', '#0000ff', NULL, 1, 'San Fernando, Virreyes, Victoria, Beccar.', 'Lunes y Jueves 13 a 20 hs', 'Veterinaria', '', '_417170141.jpg', 1, '0000-00-00 00:00:00', 'bc2dd5738f898ed95f181b2603aef7dd', 0, 1, '15-3351-7485', 1, 1, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(28, 'Egondolas', 'jorge', 'gonzales', 'hola@hotmail.com', '4745-1111', '', '', '$2y$10$J9eEDgVbmyphglQ63S34S.yW0t4nH54Ei3WkDbhbWBejejJ8zKyrq', 'Av presidente Peron 2900', 'Victoria', 1, '-34.4512752', '-58.541155', '9:00 a 13:00 y 15:00 a 19:30', '10:00 a 13:00', '', 'Gratis, Mínimo $300', 'Prueba 24/11/2016', '', '#ffff00', NULL, 1, 'San Fernando, Virreyes, Victoria, Beccar', 'Todos los días apartir de las 16:00 hs', 'Veterinaria', '', '_864093000.jpg', 1, '2016-08-31 15:06:52', '65cea4de1ff7efbb872f96c495d3a4d4', 0, 1, '11-45-1112234', 1, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 10:24:38', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(30, 'PUPPIS', 'Juan Carlos', 'Scopa', 'german.vide@geotienda.com', '0810-777-8779', '', '', '$2y$10$705Yq/.ZfxFtq2T6tDehL.0fwU7bcNgIyfBbGhQ46UM3mPp7idAUy', 'Av. Libertador 13510', 'La Lucila', 1, '-34.4859253', '-58.486929599999996', '9:00 a 18:00hs', '9:00 a 13:00hs', '10:00 a 15:00hs', '30$', '30% OFF PIPETAS ¡Fuera pulgas y garrapatas!', '#ffffff', '#f1c232', NULL, 1, 'Capital Federal y GBA Norte', 'LU a VI', 'Petshop', '', '_649430863.jpg', 1, '2017-01-29 22:04:32', 'f4b86acd7465dfca7a69ec8cf20e9615', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '2017-08-09 11:51:59', 'Gvide', '0000-00-00 00:00:00', '', '2017-08-15 20:45:14', 'Gvide', '2017-08-15 20:52:04', 'Gvide'),
(31, 'Centropet', 'Bruno', 'Leyes', 'sebastian.piquet@geotienda.com', '4851-1788', '', '1010-1010', '$2y$10$D3iX3gB375msAEh2MnYHtewMjd/5muW8Y7x3GqTBfx1jQVlsRzDGG', 'Av. Córdoba 1557', 'Capital Federal', 5, '-34.599253', '-58.38926200000003', 'LU a VI de 10:00 a 19:30hs', '10:00 a 19:30hs', '', '$100', 'Somos el Pet-Shop más completo de Argentina!', '#ffffff', '#6fa8dc', NULL, 0, 'Olivos', 'Lunes a Viernes de 12 a 17', 'Petshop', '', '_491564355.jpg', 1, '2017-03-28 23:00:12', 'feb668a64718ab4061748c994563ac76', 0, 0, '1122334455', 1, 1, '0000-00-00 00:00:00', '', '2017-08-11 11:15:55', 'Gvide', '0000-00-00 00:00:00', '', '2017-08-06 15:30:02', '', '0000-00-00 00:00:00', ''),
(32, 'EDU', 'Eduardo', 'Gagey', 'egagey@gmail.com', '1111-1111', '', '', '$2y$10$n0RhZ/wSQNvV.Ek3NsV.h.t8pL89FO51gSuKM0wu27nw5SS8GhBNS', 'Sinclair', 'Olivos', 1, '-34.4928553', '-58.4829388', '9hs-18hs', '', '', 'GRATIS', '', '', '', NULL, 1, 'Vicente López', 'L-V', 'Petshop', '', 'local_avatar_mini.png', 1, '2017-05-23 15:23:13', '5fd1855381a5b39d998eb9f95cb595e8', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '2017-08-09 10:25:03', 'Gvide', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(33, 'TurkitoVet', 'christian', 'zanabria', 'turkototem@gmail.com', '3213211', '', '', '$2y$10$WcXgspqHnm5XtH358BinPOpJHKKMDPnuFKLXRX610RNX7.eFmxfeu', 'gervacio posadas 722', 'san isidro', 1, '-34.4651252', '-58.53339440000002', '09hs a 20hs', '09hs a 14hs', 'cerrado', '100', '2x1 en cortes de uña', '#ffff00', '#00ff00', NULL, 1, 'san isidro', 'lunes a sabado', 'Veterinaria', '', '282505583.jpg', 1, '2017-06-05 18:16:45', 'ef3c8ab912f303fc84fb2960bba9b96e', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(34, 'La Tienda de May', 'May', 'La Gordi', 'PIQUET.MARIACLARA@GMAIL.COM', '1147941394', '', '1169816204', '$2y$10$LI/p3C8fmCohVkR6bF6E3OdEq15tXKMQPXple18fbhMsC9IfCGiGu', 'BOUCHARD 1394', 'LA LUCILA', 1, '-34.4995106', '-58.49644710000001', '8h00 a 12h00 - 16h00 a 20h00', '10h00 a 13h00', '-', '$20,00', '¡¡BIENVENIDO A LA TIENDA DE MAY!!', '#660000', '#d5a6bd', NULL, 1, 'Olivos, La Lucila, Martínez', 'Lunes a Sábados', 'Petshop', '', '_783933923.jpg', 1, '2017-06-05 22:41:14', '1d42cfa4f4661e11c9266572a31ba2c1', 0, 1, '6666-6666', 0, 1, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(35, 'Minguito', 'Roberto', 'furtado', 'mingo@hotmail.com', '4141-4141', '', '', '$2y$10$kePeuRdPn93WsEPmLRP90u8WKsJZad.vaVmXgTR6yR8ExcvjYXoyy', 'madero 1474', 'San Fernando', 1, '-34.4423168', '-58.55924950000002', 'Lunes a Viernes de 10 a 14', '12 a 15', '', '', '', '', '#ffffff', NULL, 0, '', '', 'Veterinaria', '', '_63582979.jpg', 1, '2017-06-09 20:49:58', '75ca510627b6187b75b2a9a8d9a6dec6', 0, 1, '15-4574-9587', 1, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(37, 'Rinti', 'Fernando', 'Teijido', 'fer@mail.com', '1111-1111', '', '', '$2y$10$587OAEP2C2ZkJV4B1tuyBewpWssJth2UH8U4ux5UUty2.MLLTzv3C', 'Enrique de Valle Iberlucea 3028', 'Lanús', 1, '-34.7087609', '-58.392922999999996', 'LU a VI de 10:00 a 19:30hs', '9:00 a 13hs', '', '', '', '', '', NULL, 0, '', '', 'Petshop', '', '_152487214.jpg', 1, '2017-07-29 15:02:27', 'e4213b834e917c1d9d6dd4502031eb84', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(38, 'ALE', 'Alejandro', 'Scopa', 'solangecy@hotmail.com', '4836-1900', '', '', '$2y$10$.ni9NImW5qt9EPmGqmUjEe2UQz5.NcrfWdfyupVrFYf6h2.EwJWsO', 'Santiago del Estero 2026', 'Martinez', 1, '-34.50184410000001', '-58.52247729999999', 'LU a VI de 9:00 a 18:00hs', '', '', '', '', '', '', NULL, 0, '', '', 'Petshop', '', 'local_avatar_mini.png', 0, '2017-07-30 05:34:24', '', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '2017-08-09 12:21:16', 'Gvide', '2017-08-15 21:07:15', 'Gvide', '2017-08-09 18:34:39', 'Gvide', '0000-00-00 00:00:00', ''),
(39, 'FLOPPY', 'Florencia', 'Scopa', 'Florenciascopa@gmail.com', '4836-1900', '', '', '$2y$10$GmUAvcgalGcyQJNZhhBSverEq2.UYVQkd6UYDN7LHzAyMl3U6WhAy', 'Santiago del Estero 2226', 'Martinez', 1, '-34.502483', '-58.52408359999998', '9hs-18hs', '', '', '', '', '', '', NULL, 0, '', '', 'Veterinaria', '', 'local_avatar_mini.png', 1, '2017-07-30 21:58:19', 'a2624070832d749543834ed2e61a0dd3', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(40, 'Ringo', 'Ringo', 'pinto', 'ringo@hotmail.com', '2525-1212', '', '', '$2y$10$kdLu4OKMk6SzmQFSw0UogeY2y6UlMdSobOc6lIVqAucdTy4mA4VaK', 'Santiago del Estero 800', 'Martinez', 1, '-34.4954906', '-58.50872900000002', 'Lunes a Viernes de 10 a 15', '', '', '', '', '', '', NULL, 0, '', '', 'Petshop', '', 'local_avatar_mini.png', 1, '2017-07-30 23:31:58', '', 0, 0, '', 0, 0, '0000-00-00 00:00:00', '', '2017-08-09 12:18:04', 'Gvide', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(41, 'Test editado', 'Atencio', 'Atencio', 'fatencio@gmail.com1245', '2', '', '', '$2y$10$SSAiGq6VSXFc292rc0lkn.kqF2Kr3LGuInn2y9/WI0YeYHxlPA1yi', 'Antártida Argentina 439', 'Luján de Cuyo', 17, '', '', 'LU a VI de 10:00 a 19:30hs', '', '', '', '', '', '', NULL, 0, '', '', 'Veterinaria', '', 'local_avatar_mini.png', 1, '2017-08-05 14:28:02', '', 0, 0, '', 0, 0, '2017-08-05 11:28:02', 'fatencio', '2017-08-05 11:28:30', 'fatencio', '2017-08-05 11:28:33', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(42, 'test no destacado', 'Atencio', 'Atencio', 'fatencio@gmail.com3214', '2', '', '', '$2y$10$xSmYEf4NdroxzsWkXihvSeQXxPvcN7wtcCE./SsomC.e9.VK2vLle', 'Antártida Argentina 439', 'Luján de Cuyo', 18, '', '', 's', '', '', '22', '', '', '', NULL, 1, 'z', 'd', 'Petshop', '', 'local_avatar_mini.png', 1, '2017-08-09 01:06:03', '', 0, 1, '222', 0, 1, '2017-08-08 22:06:03', 'fatencio', '2017-08-08 22:09:18', 'fatencio', '2017-08-08 22:09:28', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(43, 'Gofy', 'Alejandro', 'Rica', 'local@gmail.com', '8584-7475', '', '', '$2y$10$63fwaoah4gFun67hX6vMeO2/9zwzwmnq76TBbAdG2aS7L4fAOnLMq', 'Necochea 1230', 'San Fernando', 1, '-34.4444139', '-58.55294070000002', '8:30 a 12:30 y 14:00 a 20:00 Hs', '9:30 a 14:30', '', 'Gratis', '', '', '', NULL, 1, 'San Fernando, Victoria', 'Todos los días hasta las 20:00 Hs', 'Veterinaria', '', 'local_avatar_mini.png', 1, '2017-08-09 13:30:45', '198a4bd1b46be5950d348e4a6240d2c2', 0, 1, '15-9988-2233', 1, 1, '2017-08-09 10:30:45', 'Gvide', '2017-08-09 12:21:49', 'Gvide', '0000-00-00 00:00:00', '', '2017-08-09 18:34:49', 'Gvide', '0000-00-00 00:00:00', ''),
(44, 'Pluto', 'Leonardo', 'Distacio', 'punto@gmail.com', '11111111', '', '', '$2y$10$SSfxnx/1sgS4OBPI2S6WBeiGrrBwZUwQGB88QefgbgU0kpVLGjBH6', 'Creado 1773', 'Rosario', 21, '', '', '8 a 20', '', '', '', '', '', '', NULL, 0, '', '', 'Veterinaria', '', 'local_avatar_mini.png', 0, '2017-08-14 19:09:19', '8a2a5d8c79c8178a957af370e3866253', 0, 0, '', 0, 0, '2017-08-14 16:09:19', 'Gvide', '0000-00-00 00:00:00', '', '2017-08-14 16:14:50', 'Gvide', '2017-08-14 16:14:42', 'Gvide', '0000-00-00 00:00:00', ''),
(45, 'Coco Canel ', 'Veronica', 'Prueba', 'prueba@hotmail.com', '1111-1111', '', '', '$2y$10$.v5csfj94CFaeRVH9VWUxuUADHVtstctDhbE7U9i1k6z7.OQE7rx.', 'Montevideo 1019', 'Rosario', 21, '-32.9564545', '-60.63941510000001', '9:00 a 19:00', '9:00 a 13:30', 'Cerrado', '', '', '', '', NULL, 0, '', '', 'Petshop', '', '_441027777.png', 1, '2017-08-15 00:02:21', 'fdbbc4fee6c038f4beec9d1668806ea1', 0, 0, '', 0, 0, '2017-08-14 21:02:21', 'Gvide', '2017-08-14 21:25:36', 'Gvide', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(46, 'Test', 'Teset', 'Audit', 'fa@gmail.com', '222', '', '', '$2y$10$pfb6SGwpw.wfyMm12ZQUNOh8hnYJdspwcT79bMb4ShZZyYlSelUwW', '3 de febrero 430', 'Olivos', 5, '-34.5549131', '-58.45583199999999', 'LU a VI de 10:00 a 19:30hs', '', '', '', '', '', '', NULL, 0, '', '', 'Petshop', '', 'local_avatar_mini.png', 1, '2017-08-15 01:05:05', '61dcf53a5403449e4e41a4021c9dab98', 1, 0, '', 0, 0, '2017-08-14 22:05:05', 'fatencio', '2017-08-14 22:23:07', 'fatencio', '2017-08-14 22:24:48', 'fatencio', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(47, 'Hocicos Pet Shop', 'Prueba 2', 'fin', 'petshop1@hotmail.com', '0341 15-325-9330', '', '', '$2y$10$B81N.kmDqCIbOBtqcmJvlO1pmnJdBsb5orVxCuD/vtWPmljPbDXVK', 'Santiago 565', 'Rosario', 21, '-32.9405069', '-60.655339000000026', '9:30 a 13:30 y 16:00 a 21:00 Hs', '9:30 a 14:00 Hs', 'Cerrado', '', '', '', '', NULL, 0, '', '', 'Petshop', '', '_238416152.png', 1, '2017-08-16 19:48:55', '4b5e86777dcf9931b12c292b0ac4c881', 0, 0, '', 0, 0, '2017-08-16 16:48:55', 'Gvide', '2017-08-16 16:57:45', 'petshop1@hotmail.com', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(48, 'Pippe Dog', 'Prueba 3', 'fin1', 'prueba2@hotmail.com', '4125478', '', '', '$2y$10$ZDL1i50WeUyfjjnDUKOGIuUP15wKmmA4I5ndMZIwvo8PvbDVcguei', 'Castellanos 1170', 'Rosario', 21, '-32.9445319', '-60.67638469999997', '9:00 a 13:00 y 16:00 a 20:30 Hs', '9:00 a 13:00 y 16:00 a 20:30 Hs', 'Cerrado', 'Sin cargo', '', '', '', NULL, 1, 'Zonas aledañas', 'Todos los días ', 'Petshop', '', '_41915537.png', 1, '2017-08-16 20:05:17', '', 0, 0, '', 0, 0, '2017-08-16 17:05:17', 'Gvide', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', ''),
(49, 'Veterinaria del Sol', 'Prueba 4', 'fin4', 'prueba4@hotmail.com', '4145874', '', '', '$2y$10$qvF04eEutNCrx.YOaStL3Oa545lcmXcxHUB2HtCsa/p/zsixHo81e', 'Avenida Alberdi 830', 'Rosario', 21, '-32.917778', '-60.68342799999999', '9:00 a 13:00 y 17:00 a 20:30 Hs', '9:00 a 13:00 Hs', 'Cerrado', 'Sin costo a partir de $500', '', '', '', NULL, 1, 'Zona aledañas', 'Todos los días hasta las 20:00 Hs', 'Veterinaria', '', '_528898419.png', 1, '2017-08-16 20:17:14', '', 0, 1, '..........', 1, 1, '2017-08-16 17:17:14', 'Gvide', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_articulo`
--

CREATE TABLE IF NOT EXISTS `local_articulo` (
  `local_articulo_id` int(11) NOT NULL,
  `local_articulo_local_id` int(11) NOT NULL,
  `local_articulo_articulo_presentacion_id` int(11) NOT NULL,
  `local_articulo_precio` float NOT NULL,
  `local_articulo_stock` tinyint(4) NOT NULL DEFAULT '1',
  `local_articulo_descuento_id` int(11) NOT NULL DEFAULT '0',
  `local_articulo_precio_descuento` float NOT NULL DEFAULT '0',
  `local_articulo_estado` tinyint(4) NOT NULL DEFAULT '1',
  `local_articulo_estado_presentacion` int(11) NOT NULL DEFAULT '0',
  `local_articulo_creador` char(9) NOT NULL DEFAULT 'GeoTienda'
) ENGINE=InnoDB AUTO_INCREMENT=1289 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local_articulo`
--

INSERT INTO `local_articulo` (`local_articulo_id`, `local_articulo_local_id`, `local_articulo_articulo_presentacion_id`, `local_articulo_precio`, `local_articulo_stock`, `local_articulo_descuento_id`, `local_articulo_precio_descuento`, `local_articulo_estado`, `local_articulo_estado_presentacion`, `local_articulo_creador`) VALUES
(2, 31, 2914, 550, 1, 5, 550, 1, 1, 'GeoTienda'),
(8, 25, 2918, 500, 1, 5, 500, 1, 1, 'GeoTienda'),
(9, 25, 2919, 650, 1, 5, 650, 1, 1, 'GeoTienda'),
(10, 25, 2920, 780, 1, 5, 780, 1, 1, 'GeoTienda'),
(11, 25, 2921, 750, 1, 5, 750, 1, 1, 'GeoTienda'),
(12, 25, 2917, 800, 1, 5, 800, 1, 1, 'GeoTienda'),
(30, 25, 3370, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(31, 25, 3371, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(32, 25, 3372, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(33, 25, 3373, 400, 1, 5, 400, 1, 1, 'GeoTienda'),
(35, 25, 3062, 1000, 1, 5, 1000, 1, 1, 'GeoTienda'),
(49, 25, 3431, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(50, 25, 3432, 250, 1, 5, 250, 1, 1, 'GeoTienda'),
(51, 25, 3433, 500, 1, 5, 500, 1, 1, 'GeoTienda'),
(63, 25, 3589, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(64, 25, 3590, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(73, 31, 3367, 10, 1, 26, 9, 1, 1, 'GeoTienda'),
(74, 31, 3368, 20, 0, 5, 20, 1, 1, 'GeoTienda'),
(75, 31, 3369, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(76, 31, 3359, 50, 1, 5, 50, 1, 1, 'GeoTienda'),
(77, 31, 3360, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(78, 31, 3361, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(79, 31, 3354, 60, 1, 5, 60, 1, 1, 'GeoTienda'),
(80, 31, 3355, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(125, 25, 3770, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(126, 25, 3771, 120, 1, 5, 120, 1, 1, 'GeoTienda'),
(127, 25, 3772, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(128, 25, 3773, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(129, 25, 3768, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(130, 25, 3769, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(132, 25, 3778, 250, 1, 5, 250, 1, 1, 'GeoTienda'),
(142, 25, 3761, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(143, 25, 3762, 300, 1, 26, 270, 1, 1, 'GeoTienda'),
(144, 25, 3763, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(145, 25, 3764, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(146, 25, 3756, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(147, 25, 3757, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(148, 25, 3758, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(149, 25, 3759, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(150, 25, 3760, 750, 1, 5, 750, 1, 1, 'GeoTienda'),
(152, 25, 3060, 890, 1, 5, 890, 1, 1, 'GeoTienda'),
(153, 25, 3364, 200, 1, 5, 200, 1, 0, 'GeoTienda'),
(154, 25, 3365, 400, 1, 5, 400, 1, 1, 'GeoTienda'),
(155, 25, 3366, 600, 1, 5, 600, 1, 1, 'GeoTienda'),
(160, 25, 3783, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(161, 25, 3784, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(162, 25, 3785, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(163, 25, 3786, 1000, 1, 5, 1000, 1, 1, 'GeoTienda'),
(165, 25, 3066, 720, 1, 5, 720, 1, 1, 'GeoTienda'),
(176, 25, 3787, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(177, 25, 3788, 130, 1, 5, 130, 1, 1, 'GeoTienda'),
(178, 25, 3789, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(179, 25, 3790, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(180, 25, 3791, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(183, 25, 3823, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(184, 25, 3824, 950, 1, 5, 950, 1, 1, 'GeoTienda'),
(203, 33, 3386, 1000, 1, 31, 500, 1, 1, 'GeoTienda'),
(204, 34, 3354, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(205, 34, 3355, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(206, 34, 3356, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(207, 34, 3357, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(208, 34, 3358, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(209, 34, 3359, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(210, 34, 3360, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(211, 34, 3361, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(212, 34, 3362, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(213, 34, 3363, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(214, 34, 3364, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(215, 34, 3365, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(216, 34, 3366, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(217, 34, 3367, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(218, 34, 3368, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(219, 34, 3369, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(220, 34, 3370, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(221, 34, 3371, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(222, 34, 3372, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(223, 34, 3373, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(224, 34, 3374, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(225, 34, 3375, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(226, 34, 3376, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(227, 34, 3377, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(228, 34, 3378, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(229, 34, 3379, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(230, 34, 3380, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(231, 34, 3381, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(232, 34, 3382, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(233, 34, 3383, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(234, 34, 3384, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(235, 34, 3385, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(236, 34, 3386, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(237, 34, 3396, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(238, 34, 3397, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(239, 34, 3398, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(240, 34, 3399, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(241, 34, 3400, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(242, 34, 3401, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(243, 34, 3402, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(244, 34, 3403, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(245, 34, 3404, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(246, 34, 3405, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(247, 34, 3406, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(248, 34, 3407, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(249, 34, 3408, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(250, 34, 3409, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(251, 34, 3410, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(252, 34, 3411, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(253, 34, 3412, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(254, 34, 3413, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(255, 34, 3414, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(256, 34, 3416, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(257, 34, 3417, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(258, 34, 3418, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(259, 34, 3419, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(260, 34, 3420, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(261, 34, 3421, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(262, 34, 3422, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(263, 34, 3296, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(264, 34, 3297, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(265, 34, 3298, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(266, 34, 3299, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(267, 34, 3300, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(268, 34, 3301, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(269, 34, 3302, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(270, 34, 3303, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(271, 34, 3304, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(272, 34, 3305, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(273, 34, 3306, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(274, 34, 3307, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(275, 34, 3308, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(276, 34, 3309, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(277, 34, 3310, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(278, 34, 3311, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(279, 34, 3312, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(280, 34, 3313, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(281, 34, 3314, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(282, 34, 3315, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(283, 34, 3439, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(284, 34, 3440, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(285, 34, 3441, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(286, 34, 3442, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(287, 34, 3443, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(288, 34, 3444, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(289, 34, 3445, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(290, 34, 3446, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(291, 34, 3447, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(292, 34, 3448, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(293, 34, 3449, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(294, 34, 3450, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(295, 34, 3451, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(296, 34, 3452, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(297, 34, 3453, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(298, 34, 3454, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(299, 34, 3455, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(300, 34, 3456, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(301, 34, 3457, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(302, 34, 3458, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(303, 34, 3459, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(304, 34, 3460, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(305, 34, 3461, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(306, 34, 3462, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(307, 34, 3463, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(308, 34, 3464, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(309, 34, 3465, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(310, 34, 3466, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(311, 34, 3467, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(312, 34, 3468, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(313, 34, 3469, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(314, 34, 3470, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(315, 34, 3471, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(316, 34, 3472, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(317, 34, 3473, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(318, 34, 3474, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(319, 34, 3475, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(320, 34, 3476, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(321, 34, 3477, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(322, 34, 3478, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(323, 34, 3479, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(324, 34, 3480, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(325, 34, 3481, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(326, 34, 3482, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(327, 34, 3484, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(328, 34, 3485, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(329, 34, 3486, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(330, 34, 3487, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(331, 34, 3488, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(332, 34, 3489, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(333, 34, 3490, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(334, 34, 3491, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(335, 34, 3492, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(336, 34, 3493, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(337, 34, 3494, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(338, 34, 3495, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(339, 34, 3496, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(340, 34, 3497, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(341, 34, 3498, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(342, 34, 3499, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(343, 34, 3500, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(344, 34, 3501, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(345, 34, 3502, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(346, 34, 3503, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(347, 34, 3504, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(348, 34, 3505, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(349, 34, 3506, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(350, 34, 3507, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(351, 34, 3508, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(352, 34, 3509, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(353, 34, 3510, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(354, 34, 3511, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(355, 34, 3512, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(356, 34, 3513, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(357, 34, 3514, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(358, 34, 3515, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(359, 34, 3516, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(360, 34, 3517, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(361, 34, 3518, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(362, 34, 3519, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(363, 34, 3520, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(364, 34, 3521, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(365, 34, 3550, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(366, 34, 3551, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(367, 34, 3522, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(368, 34, 3523, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(369, 34, 3524, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(370, 34, 3525, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(371, 34, 3526, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(372, 34, 3527, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(373, 34, 3528, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(374, 34, 3529, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(375, 34, 3530, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(376, 34, 3531, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(377, 34, 3532, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(378, 34, 3533, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(379, 34, 3534, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(380, 34, 3535, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(381, 34, 3536, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(382, 34, 3537, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(383, 34, 3538, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(384, 34, 3539, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(385, 34, 3540, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(386, 34, 3541, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(387, 34, 3542, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(388, 34, 3543, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(389, 34, 3544, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(390, 34, 3545, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(391, 34, 3546, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(392, 34, 3547, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(393, 34, 3548, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(394, 34, 3549, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(395, 34, 3119, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(396, 34, 3120, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(397, 34, 3121, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(398, 34, 3122, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(399, 34, 3123, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(400, 34, 3124, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(401, 34, 3125, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(402, 34, 3126, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(403, 34, 3127, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(404, 34, 3128, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(405, 34, 3129, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(406, 34, 3130, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(407, 34, 3131, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(408, 34, 3132, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(409, 34, 3133, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(410, 34, 3134, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(411, 34, 3135, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(412, 34, 3136, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(413, 34, 3137, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(414, 34, 3138, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(415, 34, 3139, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(416, 34, 3140, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(417, 34, 3141, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(418, 34, 3142, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(419, 34, 3143, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(420, 34, 3144, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(421, 34, 2922, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(422, 34, 2923, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(423, 34, 2924, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(424, 34, 2925, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(425, 34, 2926, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(426, 34, 2927, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(427, 34, 2928, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(428, 34, 2929, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(429, 34, 2930, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(430, 34, 2931, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(431, 34, 2932, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(432, 34, 2933, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(433, 34, 2934, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(434, 34, 2935, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(435, 34, 2936, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(436, 34, 2937, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(437, 34, 2938, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(438, 34, 2939, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(439, 34, 2940, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(440, 34, 2941, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(441, 34, 2942, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(442, 34, 2943, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(443, 34, 2944, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(444, 34, 2945, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(445, 34, 2946, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(446, 34, 2947, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(447, 34, 2948, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(448, 34, 2949, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(449, 34, 2950, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(450, 34, 2951, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(451, 34, 2952, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(452, 34, 2953, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(453, 34, 2954, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(454, 34, 2955, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(455, 34, 2956, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(456, 34, 2957, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(457, 34, 2958, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(458, 34, 2959, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(459, 34, 2960, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(460, 34, 2961, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(461, 34, 2962, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(462, 34, 2963, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(463, 34, 2964, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(464, 34, 2965, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(465, 34, 2966, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(466, 34, 2997, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(467, 34, 2998, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(468, 34, 2999, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(469, 34, 3000, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(470, 34, 3001, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(471, 34, 3002, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(472, 34, 3003, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(473, 34, 3004, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(474, 34, 3005, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(475, 34, 3006, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(476, 34, 3007, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(477, 34, 3008, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(478, 34, 3009, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(479, 34, 3010, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(480, 34, 3011, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(481, 34, 3012, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(482, 34, 3013, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(483, 34, 3014, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(484, 34, 3015, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(485, 34, 3016, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(486, 34, 3017, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(487, 34, 3018, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(488, 34, 3019, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(580, 34, 3655, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(581, 34, 3656, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(582, 34, 3657, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(583, 34, 3658, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(584, 34, 3659, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(585, 34, 3660, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(586, 34, 3661, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(587, 34, 3662, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(588, 34, 3663, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(589, 34, 3664, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(590, 34, 3665, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(591, 34, 3666, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(592, 34, 3667, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(593, 34, 3668, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(594, 34, 3669, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(595, 34, 3670, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(596, 34, 3671, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(597, 34, 3672, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(598, 34, 3673, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(599, 34, 3674, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(600, 34, 3675, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(601, 34, 3676, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(602, 34, 3677, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(603, 34, 3431, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(604, 34, 3432, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(605, 34, 3433, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(606, 34, 3552, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(607, 34, 3553, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(608, 34, 3554, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(609, 34, 3555, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(610, 34, 3556, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(611, 34, 3557, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(612, 34, 3558, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(613, 34, 3559, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(614, 34, 3560, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(615, 34, 3561, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(616, 34, 3562, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(617, 34, 3563, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(618, 34, 3564, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(619, 34, 3565, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(620, 34, 3566, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(621, 34, 3567, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(622, 34, 3568, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(623, 34, 3569, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(624, 34, 3570, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(625, 34, 3571, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(626, 34, 3572, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(627, 34, 3573, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(628, 34, 3574, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(629, 34, 3575, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(630, 34, 3576, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(631, 34, 3577, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(632, 34, 3578, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(633, 34, 3579, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(634, 34, 3580, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(635, 34, 3581, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(636, 34, 3582, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(637, 34, 3583, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(638, 34, 3584, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(639, 34, 3585, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(640, 34, 3586, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(641, 34, 3587, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(642, 34, 3588, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(643, 34, 3783, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(644, 34, 3784, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(645, 34, 3785, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(646, 34, 3786, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(647, 34, 3787, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(648, 34, 3788, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(649, 34, 3789, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(650, 34, 3790, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(651, 34, 3791, 1000, 1, 5, 1000, 1, 1, 'GeoTienda'),
(652, 34, 3792, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(653, 34, 3793, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(654, 34, 3794, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(655, 34, 3795, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(656, 34, 3796, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(657, 34, 3797, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(658, 34, 3798, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(659, 34, 3799, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(660, 34, 3800, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(661, 34, 3801, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(662, 34, 3802, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(663, 34, 3803, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(664, 34, 3804, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(665, 34, 3805, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(666, 34, 3806, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(667, 34, 3807, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(668, 34, 3808, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(669, 34, 3809, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(670, 34, 3810, 150, 1, 5, 150, 1, 1, 'GeoTienda'),
(671, 34, 3811, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(672, 34, 3756, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(673, 34, 3757, 65, 1, 5, 65, 1, 1, 'GeoTienda'),
(674, 34, 3758, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(675, 34, 3759, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(676, 34, 3760, 700, 1, 5, 700, 1, 1, 'GeoTienda'),
(677, 34, 3761, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(678, 34, 3762, 65, 1, 5, 65, 1, 1, 'GeoTienda'),
(679, 34, 3763, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(680, 34, 3764, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(681, 34, 3765, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(682, 34, 3766, 65, 1, 5, 65, 1, 1, 'GeoTienda'),
(683, 34, 3767, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(684, 34, 3605, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(685, 34, 3606, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(686, 34, 3607, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(687, 34, 3608, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(688, 34, 3609, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(689, 34, 3610, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(690, 34, 3611, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(691, 34, 3612, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(692, 34, 3613, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(693, 34, 3614, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(694, 34, 3615, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(695, 34, 3616, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(696, 34, 3617, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(697, 34, 3618, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(698, 34, 3619, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(699, 34, 3620, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(700, 34, 3621, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(701, 34, 3622, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(702, 34, 3623, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(703, 34, 3624, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(704, 34, 3625, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(705, 34, 3626, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(706, 34, 3627, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(707, 34, 3628, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(708, 34, 3629, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(709, 34, 3630, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(710, 34, 3631, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(711, 34, 3632, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(712, 34, 3633, 30, 1, 5, 30, 1, 1, 'GeoTienda'),
(713, 34, 3634, 30, 1, 5, 30, 1, 1, 'GeoTienda'),
(714, 34, 3635, 30, 1, 5, 30, 1, 1, 'GeoTienda'),
(715, 34, 3636, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(716, 34, 3637, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(717, 34, 3638, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(718, 34, 3639, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(719, 34, 3640, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(720, 34, 3641, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(721, 34, 3642, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(722, 34, 3643, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(723, 34, 3644, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(724, 34, 3645, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(725, 34, 3646, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(726, 34, 3647, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(727, 34, 3648, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(728, 34, 3649, 110, 1, 5, 110, 1, 1, 'GeoTienda'),
(729, 34, 3650, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(730, 34, 3651, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(731, 34, 3652, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(732, 34, 3653, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(733, 34, 3654, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(735, 34, 3884, 2000, 1, 26, 1800, 1, 1, 'Local'),
(749, 35, 3837, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(750, 35, 3838, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(751, 35, 3836, 250, 1, 5, 250, 1, 1, 'GeoTienda'),
(752, 35, 3839, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(753, 33, 3869, 100, 1, 5, 100, 0, 1, 'GeoTienda'),
(754, 33, 3873, 100, 1, 5, 100, 0, 1, 'GeoTienda'),
(768, 33, 3887, 250, 1, 5, 250, 1, 1, 'GeoTienda'),
(769, 33, 3885, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(775, 33, 3866, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(776, 33, 3856, 500, 1, 5, 500, 1, 1, 'GeoTienda'),
(777, 33, 3857, 450, 1, 5, 450, 1, 1, 'GeoTienda'),
(778, 33, 3858, 350, 1, 5, 350, 1, 1, 'GeoTienda'),
(779, 33, 3859, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(780, 33, 3860, 460, 1, 5, 460, 1, 1, 'GeoTienda'),
(790, 33, 3889, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(791, 33, 3890, 120, 1, 5, 120, 1, 1, 'GeoTienda'),
(792, 33, 3891, 150, 1, 5, 150, 1, 1, 'GeoTienda'),
(793, 33, 3892, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(798, 25, 3607, 150, 1, 5, 150, 1, 1, 'GeoTienda'),
(799, 25, 3608, 600, 1, 5, 600, 1, 1, 'GeoTienda'),
(800, 25, 3605, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(801, 25, 3606, 800, 1, 5, 800, 1, 1, 'GeoTienda'),
(805, 25, 3651, 270, 1, 5, 270, 1, 1, 'GeoTienda'),
(806, 25, 3662, 130, 1, 5, 130, 1, 1, 'GeoTienda'),
(807, 25, 3649, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(813, 25, 3524, 240, 1, 5, 240, 1, 1, 'GeoTienda'),
(814, 25, 3525, 500, 1, 5, 500, 1, 1, 'GeoTienda'),
(815, 25, 3526, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(816, 25, 3527, 550, 1, 5, 550, 1, 1, 'GeoTienda'),
(817, 25, 3528, 700, 1, 5, 700, 1, 1, 'GeoTienda'),
(821, 23, 3356, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(822, 23, 3357, 400, 1, 5, 400, 1, 1, 'GeoTienda'),
(823, 23, 3358, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(824, 23, 3354, 150, 1, 5, 150, 1, 1, 'GeoTienda'),
(825, 23, 3355, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(830, 33, 3354, 650, 1, 5, 650, 1, 1, 'GeoTienda'),
(831, 33, 3355, 500, 1, 5, 500, 1, 1, 'GeoTienda'),
(832, 33, 3356, 650, 1, 5, 650, 0, 1, 'GeoTienda'),
(833, 33, 3357, 750, 1, 5, 750, 0, 1, 'GeoTienda'),
(834, 33, 3358, 950, 1, 5, 950, 0, 1, 'GeoTienda'),
(835, 33, 3861, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(836, 33, 3862, 120, 1, 5, 120, 1, 1, 'GeoTienda'),
(837, 33, 3863, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(838, 33, 3864, 350, 1, 5, 350, 1, 1, 'GeoTienda'),
(839, 33, 3865, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(840, 33, 3876, 150, 1, 5, 150, 0, 1, 'GeoTienda'),
(841, 33, 3877, 100, 1, 5, 100, 0, 1, 'GeoTienda'),
(842, 33, 3878, 50, 1, 5, 50, 0, 1, 'GeoTienda'),
(843, 33, 3883, 100, 1, 5, 100, 0, 1, 'GeoTienda'),
(844, 33, 3879, 100, 1, 5, 100, 0, 1, 'GeoTienda'),
(845, 23, 3431, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(846, 23, 3432, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(847, 23, 3433, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(848, 23, 3555, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(849, 23, 3556, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(850, 33, 3370, 500, 1, 5, 500, 1, 0, 'GeoTienda'),
(851, 33, 3371, 700, 1, 5, 700, 1, 1, 'GeoTienda'),
(852, 33, 3372, 900, 1, 5, 900, 1, 1, 'GeoTienda'),
(853, 33, 3373, 1100, 1, 5, 1100, 1, 1, 'GeoTienda'),
(854, 34, 2914, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(855, 25, 3356, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(856, 25, 3357, 400, 1, 5, 400, 1, 1, 'GeoTienda'),
(857, 25, 3358, 800, 1, 5, 800, 1, 1, 'GeoTienda'),
(858, 25, 3354, 150, 1, 5, 150, 1, 1, 'GeoTienda'),
(859, 25, 3355, 450, 1, 5, 450, 1, 1, 'GeoTienda'),
(860, 23, 3483, 360, 1, 5, 360, 1, 1, 'Local'),
(861, 23, 3359, 250, 1, 5, 250, 1, 1, 'GeoTienda'),
(862, 23, 3360, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(863, 23, 3361, 800, 1, 5, 800, 1, 1, 'GeoTienda'),
(865, 25, 3958, 420, 1, 5, 420, 1, 1, 'Local'),
(869, 37, 2915, 800, 1, 5, 800, 1, 1, 'GeoTienda'),
(870, 37, 2916, 1200, 1, 5, 1200, 1, 1, 'GeoTienda'),
(871, 37, 2914, 750, 1, 5, 750, 1, 1, 'GeoTienda'),
(872, 39, 2922, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(873, 39, 2923, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(874, 39, 2924, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(875, 39, 2925, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(876, 39, 2926, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(877, 39, 2927, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(878, 39, 2928, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(879, 39, 2929, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(880, 39, 2930, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(881, 39, 2931, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(882, 39, 2932, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(883, 39, 2933, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(884, 39, 2934, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(885, 39, 2935, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(886, 39, 2936, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(887, 39, 2937, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(888, 39, 2938, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(889, 39, 2939, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(890, 39, 2940, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(891, 39, 2941, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(892, 39, 2942, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(893, 39, 2943, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(894, 39, 2944, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(895, 39, 2945, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(896, 39, 2946, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(897, 39, 2947, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(898, 39, 2948, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(899, 39, 2949, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(900, 39, 2950, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(901, 39, 2951, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(902, 39, 2952, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(903, 39, 2953, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(904, 39, 2954, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(905, 39, 2955, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(906, 39, 2956, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(907, 39, 2957, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(908, 39, 2958, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(909, 39, 2959, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(910, 39, 2960, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(911, 39, 2961, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(912, 39, 2997, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(913, 39, 2998, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(914, 39, 2999, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(915, 39, 3000, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(916, 39, 3001, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(917, 39, 3007, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(918, 39, 3008, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(919, 39, 3009, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(920, 39, 3010, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(921, 39, 3011, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(922, 39, 3002, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(923, 39, 3003, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(924, 39, 3004, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(925, 39, 3005, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(926, 39, 3006, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(927, 39, 3012, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(928, 39, 3013, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(929, 39, 3014, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(930, 39, 3015, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(931, 39, 3016, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(932, 39, 3017, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(933, 39, 3018, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(934, 39, 3019, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(935, 39, 3020, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(936, 39, 3021, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(937, 39, 3022, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(938, 39, 3023, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(939, 39, 3024, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(940, 39, 3025, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(941, 39, 3026, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(942, 39, 3027, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(943, 39, 3028, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(944, 39, 3029, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(945, 39, 3030, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(946, 39, 3031, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(947, 39, 3034, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(948, 39, 3035, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(949, 39, 3036, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(950, 39, 3037, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(951, 39, 3038, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(952, 39, 3039, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(953, 39, 3040, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(954, 39, 3041, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(955, 39, 3032, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(956, 39, 3033, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(957, 39, 3042, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(958, 39, 3043, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(959, 39, 3046, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(960, 39, 3047, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(961, 39, 3048, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(962, 39, 3049, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(963, 39, 3044, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(964, 39, 3045, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(965, 39, 3069, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(966, 39, 3070, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(967, 39, 3071, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(968, 39, 3072, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(969, 39, 3073, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(970, 39, 3074, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(971, 39, 3075, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(972, 39, 3076, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(973, 39, 3077, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(974, 39, 3078, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(975, 39, 3079, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(976, 39, 3080, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(977, 39, 3081, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(978, 39, 3082, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(979, 39, 3083, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(980, 39, 3084, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(981, 39, 3085, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(982, 39, 3086, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(983, 39, 3087, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(984, 39, 3088, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(985, 39, 3089, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(986, 39, 3186, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(987, 39, 3187, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(988, 39, 3188, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(989, 39, 3189, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(990, 39, 3190, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(991, 39, 3191, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(992, 39, 3192, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(993, 39, 3193, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(994, 39, 3194, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(995, 39, 3195, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(996, 39, 3196, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(997, 39, 3197, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(998, 39, 3198, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(999, 39, 3199, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1000, 39, 3200, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1001, 39, 3213, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1002, 39, 3214, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1003, 39, 3215, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1004, 39, 3216, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1005, 39, 3217, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1006, 39, 3218, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1007, 39, 3219, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1008, 39, 3220, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1009, 39, 3221, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1010, 39, 3222, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1011, 39, 3223, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1012, 39, 3224, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1013, 39, 3225, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1014, 39, 3226, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1015, 39, 3227, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1016, 39, 3228, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1017, 39, 3229, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1018, 39, 3230, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1019, 39, 3231, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1020, 39, 3232, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1021, 39, 3233, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1022, 39, 3234, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1023, 39, 3235, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1024, 39, 3236, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1025, 39, 3237, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1026, 39, 3238, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1027, 39, 3239, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1028, 39, 3240, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1029, 39, 3241, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1030, 39, 3242, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1031, 39, 3243, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1032, 39, 3244, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1033, 39, 3245, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1034, 39, 3246, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1035, 39, 3247, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1036, 39, 3248, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1037, 39, 3466, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1038, 39, 3467, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1039, 39, 3468, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1040, 39, 3469, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1041, 39, 3470, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1042, 39, 3471, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1043, 39, 3472, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1044, 39, 3477, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1045, 39, 3478, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1046, 39, 3484, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1047, 39, 3485, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1048, 39, 3486, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1049, 39, 3487, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1050, 39, 3488, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1051, 39, 3489, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1052, 39, 3490, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1053, 39, 3475, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1054, 39, 3476, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1055, 39, 3491, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1056, 39, 3492, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1057, 39, 3493, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1058, 39, 3499, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1059, 39, 3500, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1060, 39, 3501, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1061, 39, 3502, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1062, 39, 3503, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1063, 39, 3504, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1064, 39, 3505, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1065, 39, 3506, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1066, 39, 3507, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1067, 39, 3512, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1068, 39, 3513, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1069, 39, 3514, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1070, 39, 3515, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1071, 39, 3516, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1072, 39, 3517, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1073, 39, 3431, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1074, 39, 3432, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1075, 39, 3433, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1076, 39, 3552, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1077, 39, 3553, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1078, 39, 3554, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1079, 39, 3555, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1080, 39, 3556, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1081, 39, 3557, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1082, 39, 3558, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1083, 39, 3559, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1084, 39, 3560, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1085, 39, 3561, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1086, 39, 3562, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1087, 39, 3563, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1088, 39, 3564, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1089, 39, 3565, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1090, 39, 3566, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1091, 39, 3567, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1092, 39, 3568, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1093, 39, 3569, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1094, 39, 3609, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1095, 39, 3610, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1096, 39, 3611, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1097, 39, 3612, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1098, 39, 3613, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1099, 39, 3614, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1100, 39, 3630, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1101, 39, 3631, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1102, 39, 3632, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1103, 39, 3682, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1104, 39, 3683, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1105, 39, 3684, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1106, 39, 3685, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1107, 39, 3686, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1108, 39, 3687, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1109, 39, 3688, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1110, 39, 3689, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1111, 39, 3690, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1112, 39, 3691, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1113, 39, 3692, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1114, 39, 3693, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1115, 39, 3694, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1116, 39, 3695, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1117, 39, 3696, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1118, 39, 3697, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1119, 39, 3698, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1120, 39, 3699, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1121, 39, 3700, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1122, 39, 3701, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1123, 39, 3702, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1124, 39, 3703, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1125, 39, 3704, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1126, 39, 3768, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1127, 39, 3769, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1128, 39, 3770, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1129, 39, 3771, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1130, 39, 3772, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1131, 39, 3773, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1132, 39, 3783, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1133, 39, 3784, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1134, 39, 3785, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1135, 39, 3786, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1136, 39, 3787, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1137, 39, 3788, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1138, 39, 3789, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1139, 39, 3790, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1140, 39, 3791, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1141, 39, 3792, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1142, 39, 3793, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1143, 39, 3794, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1144, 39, 3795, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1145, 39, 3796, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1146, 39, 3797, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1147, 39, 3798, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1148, 39, 3799, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1149, 39, 3800, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1150, 39, 3801, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1151, 39, 3823, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1152, 39, 3824, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1153, 39, 3825, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1154, 39, 3826, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1155, 39, 3827, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1156, 39, 3828, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1157, 39, 3829, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1158, 39, 3830, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1159, 39, 3831, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1160, 39, 3832, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1161, 39, 3833, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1162, 39, 3834, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1163, 39, 3835, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1164, 39, 3933, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1165, 39, 3934, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1166, 39, 3935, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1167, 39, 3936, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1168, 39, 3937, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1169, 39, 3938, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1170, 39, 3939, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1171, 39, 3940, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1172, 39, 3941, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1173, 39, 3948, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1174, 39, 3950, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1175, 39, 3951, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1176, 39, 3952, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1177, 39, 3953, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1178, 39, 3954, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1179, 39, 3856, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1180, 39, 3857, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1181, 39, 3858, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1182, 39, 3859, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1183, 39, 3860, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1184, 39, 3861, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1185, 39, 3862, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1186, 39, 3863, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1187, 39, 3864, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1188, 39, 3865, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1189, 39, 3866, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1190, 39, 3869, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1191, 39, 3870, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1192, 39, 3871, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1193, 39, 3873, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1194, 39, 3874, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1195, 39, 3875, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1196, 39, 3876, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1197, 39, 3877, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1198, 39, 3878, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1199, 39, 3879, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1200, 39, 3881, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1201, 39, 3882, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1202, 39, 3915, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1203, 39, 3916, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1204, 39, 3917, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1205, 39, 3918, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1206, 39, 3919, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1207, 39, 3924, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1208, 39, 3925, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1210, 23, 3959, 400, 1, 30, 240, 1, 1, 'Local'),
(1216, 23, 3859, 100, 1, 5, 100, 1, 1, 'GeoTienda'),
(1217, 23, 3860, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1218, 23, 3858, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1219, 23, 3857, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1220, 23, 3856, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1222, 23, 3960, 200, 1, 5, 200, 1, 1, 'Local'),
(1224, 25, 3961, 500, 1, 5, 500, 1, 1, 'Local'),
(1226, 23, 3962, 400, 1, 5, 400, 1, 1, 'Local'),
(1230, 23, 3964, 250, 1, 5, 250, 1, 1, 'Local'),
(1232, 25, 3965, 230, 1, 5, 230, 1, 1, 'Local'),
(1235, 25, 3944, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(1236, 25, 3945, 400, 1, 5, 400, 1, 1, 'GeoTienda'),
(1237, 31, 2915, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1238, 31, 2916, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1239, 31, 2918, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1240, 31, 2919, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1241, 31, 2920, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1246, 31, 3048, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1247, 31, 3049, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1248, 31, 3063, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1253, 31, 3046, 120, 1, 5, 120, 1, 1, 'GeoTienda'),
(1254, 31, 3047, 560, 1, 5, 560, 1, 1, 'GeoTienda'),
(1255, 31, 3044, 90, 1, 5, 90, 1, 1, 'GeoTienda'),
(1256, 31, 3045, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1267, 43, 3283, 200, 1, 5, 200, 1, 1, 'GeoTienda'),
(1268, 43, 3284, 990, 1, 5, 990, 1, 1, 'GeoTienda'),
(1269, 43, 3285, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1270, 43, 3286, 600, 1, 5, 600, 1, 1, 'GeoTienda'),
(1271, 43, 3279, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1272, 43, 3280, 550, 1, 5, 550, 1, 1, 'GeoTienda'),
(1273, 43, 3281, 300, 1, 5, 300, 1, 1, 'GeoTienda'),
(1274, 43, 3282, 0, 1, 5, 0, 1, 0, 'GeoTienda'),
(1275, 43, 3294, 350, 1, 5, 350, 1, 1, 'GeoTienda'),
(1276, 43, 3295, 240, 1, 5, 240, 1, 1, 'GeoTienda'),
(1280, 25, 3966, 150, 1, 5, 150, 1, 1, 'Local'),
(1281, 25, 3967, 60, 1, 5, 60, 1, 1, 'Local'),
(1282, 25, 3968, 1000, 1, 5, 1000, 1, 1, 'Local'),
(1286, 46, 2914, 1000, 1, 5, 1000, 1, 1, 'GeoTienda'),
(1288, 45, 2914, 500, 1, 5, 500, 1, 1, 'GeoTienda');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_horario`
--

CREATE TABLE IF NOT EXISTS `local_horario` (
  `local_horario_id` int(11) NOT NULL,
  `local_horario_local_id` int(11) DEFAULT NULL,
  `local_horario_lav` tinyint(4) DEFAULT NULL,
  `local_horario_lav1` int(11) DEFAULT NULL,
  `local_horario_lav2` int(11) DEFAULT NULL,
  `local_horario_lav3` int(11) DEFAULT NULL,
  `local_horario_lav4` int(11) DEFAULT NULL,
  `local_horario_s` tinyint(4) DEFAULT NULL,
  `local_horario_s1` int(11) DEFAULT NULL,
  `local_horario_s2` int(11) DEFAULT NULL,
  `local_horario_s3` int(11) DEFAULT NULL,
  `local_horario_s4` int(11) DEFAULT NULL,
  `local_horario_d` tinyint(4) DEFAULT NULL,
  `local_horario_d1` int(11) DEFAULT NULL,
  `local_horario_d2` int(11) DEFAULT NULL,
  `local_horario_d3` int(11) DEFAULT NULL,
  `local_horario_d4` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local_horario`
--

INSERT INTO `local_horario` (`local_horario_id`, `local_horario_local_id`, `local_horario_lav`, `local_horario_lav1`, `local_horario_lav2`, `local_horario_lav3`, `local_horario_lav4`, `local_horario_s`, `local_horario_s1`, `local_horario_s2`, `local_horario_s3`, `local_horario_s4`, `local_horario_d`, `local_horario_d1`, `local_horario_d2`, `local_horario_d3`, `local_horario_d4`) VALUES
(1, 25, 1, 9, 13, 14, 19, 1, 10, 13, 0, 0, 1, 10, 13, 0, 0),
(2, 23, 1, 8, 12, 16, 20, 1, 9, 13, 0, 0, 0, 9, 12, 0, 0),
(3, 30, 1, 9, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(15, 31, 1, 8, 12, 16, 22, 1, 9, 14, 0, 0, 0, 0, 0, 0, 0),
(17, 33, 1, 9, 20, 0, 0, 1, 9, 14, 0, 0, 0, 0, 0, 0, 0),
(18, 34, 1, 9, 11, 16, 19, 1, 10, 12, 0, 0, 0, 0, 0, 0, 0),
(19, 39, 1, 9, 21, 0, 0, 1, 9, 13, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_medio_pago`
--

CREATE TABLE IF NOT EXISTS `local_medio_pago` (
  `local_medio_pago_id` int(11) NOT NULL,
  `local_medio_pago_medio_pago_id` int(11) NOT NULL,
  `local_medio_pago_medio_pago_item_id` int(11) NOT NULL,
  `local_medio_pago_local_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=601 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local_medio_pago`
--

INSERT INTO `local_medio_pago` (`local_medio_pago_id`, `local_medio_pago_medio_pago_id`, `local_medio_pago_medio_pago_item_id`, `local_medio_pago_local_id`) VALUES
(139, 10, 3, 33),
(140, 10, 4, 33),
(141, 10, 5, 33),
(142, 10, 7, 33),
(143, 10, 9, 33),
(144, 11, 8, 33),
(145, 13, 0, 33),
(308, 13, 0, 37),
(331, 10, 5, 34),
(332, 10, 7, 34),
(333, 13, 0, 34),
(401, 10, 3, 35),
(402, 10, 4, 35),
(403, 10, 5, 35),
(404, 10, 9, 35),
(405, 11, 8, 35),
(406, 11, 13, 35),
(407, 11, 14, 35),
(408, 11, 15, 35),
(409, 13, 0, 35),
(476, 10, 3, 23),
(477, 10, 4, 23),
(478, 10, 5, 23),
(479, 10, 9, 23),
(480, 10, 11, 23),
(481, 10, 12, 23),
(482, 11, 8, 23),
(483, 11, 13, 23),
(484, 11, 14, 23),
(485, 11, 15, 23),
(486, 13, 0, 23),
(518, 10, 3, 30),
(519, 10, 4, 30),
(520, 10, 5, 30),
(521, 10, 7, 30),
(522, 10, 9, 30),
(523, 11, 8, 30),
(524, 13, 0, 30),
(535, 13, 0, 40),
(536, 10, 3, 38),
(537, 10, 4, 38),
(538, 11, 8, 38),
(539, 10, 3, 43),
(540, 10, 12, 43),
(541, 11, 8, 43),
(542, 11, 15, 43),
(543, 13, 0, 43),
(559, 10, 3, 25),
(560, 10, 4, 25),
(561, 10, 5, 25),
(562, 10, 7, 25),
(563, 10, 9, 25),
(564, 10, 11, 25),
(565, 10, 12, 25),
(566, 10, 16, 25),
(567, 10, 17, 25),
(568, 10, 18, 25),
(569, 11, 8, 25),
(570, 11, 13, 25),
(571, 11, 14, 25),
(572, 11, 15, 25),
(573, 13, 0, 25),
(574, 10, 3, 31),
(575, 10, 4, 31),
(576, 10, 5, 31),
(577, 10, 7, 31),
(578, 10, 9, 31),
(579, 11, 8, 31),
(580, 10, 3, 45),
(581, 10, 4, 45),
(582, 11, 8, 45),
(583, 11, 13, 45),
(584, 11, 15, 45),
(585, 13, 0, 45),
(586, 10, 3, 46),
(587, 11, 8, 46),
(588, 11, 13, 46),
(589, 13, 0, 46),
(590, 11, 8, 47),
(591, 11, 15, 47),
(592, 13, 0, 47),
(593, 11, 8, 48),
(594, 11, 13, 48),
(595, 11, 14, 48),
(596, 13, 0, 48),
(597, 10, 3, 49),
(598, 10, 4, 49),
(599, 11, 8, 49),
(600, 13, 0, 49);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_servicio_item`
--

CREATE TABLE IF NOT EXISTS `local_servicio_item` (
  `local_servicio_item_id` int(11) NOT NULL,
  `local_servicio_item_local_id` int(11) NOT NULL,
  `local_servicio_item_servicio_item_id` int(11) NOT NULL,
  `local_servicio_item_duracion` int(11) NOT NULL DEFAULT '15',
  `local_servicio_item_precio` float NOT NULL,
  `local_servicio_item_descuento_id` int(11) NOT NULL,
  `local_servicio_item_precio_descuento` float NOT NULL,
  `local_servicio_item_status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=292 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `local_servicio_item`
--

INSERT INTO `local_servicio_item` (`local_servicio_item_id`, `local_servicio_item_local_id`, `local_servicio_item_servicio_item_id`, `local_servicio_item_duracion`, `local_servicio_item_precio`, `local_servicio_item_descuento_id`, `local_servicio_item_precio_descuento`, `local_servicio_item_status`) VALUES
(227, 23, 29, 60, 300, 5, 300, 1),
(228, 23, 31, 60, 450, 5, 450, 1),
(229, 23, 53, 45, 350, 5, 350, 1),
(230, 23, 55, 45, 500, 5, 500, 1),
(231, 23, 61, 45, 300, 5, 300, 1),
(232, 23, 62, 30, 300, 5, 300, 1),
(233, 23, 64, 15, 400, 5, 400, 1),
(234, 23, 72, 15, 500, 5, 500, 1),
(243, 30, 33, 90, 350, 5, 350, 1),
(244, 30, 34, 90, 400, 5, 400, 1),
(245, 30, 76, 30, 150, 5, 150, 1),
(246, 30, 77, 30, 150, 5, 150, 1),
(247, 30, 61, 15, 400, 5, 400, 1),
(248, 30, 62, 15, 400, 5, 400, 1),
(249, 30, 74, 30, 700, 5, 700, 1),
(250, 30, 75, 30, 700, 5, 700, 1),
(251, 39, 29, 15, 100, 5, 100, 1),
(252, 39, 31, 15, 150, 5, 150, 1),
(253, 39, 33, 15, 200, 5, 200, 1),
(254, 39, 35, 15, 250, 5, 250, 1),
(255, 39, 37, 15, 100, 5, 100, 1),
(256, 39, 39, 15, 150, 5, 150, 1),
(257, 39, 41, 15, 200, 5, 200, 1),
(258, 39, 43, 15, 250, 5, 250, 1),
(259, 39, 45, 30, 200, 5, 200, 1),
(260, 39, 46, 45, 250, 5, 250, 1),
(261, 39, 47, 30, 300, 5, 300, 1),
(262, 39, 48, 45, 350, 5, 350, 1),
(263, 39, 49, 30, 400, 5, 400, 1),
(264, 39, 50, 45, 450, 5, 450, 1),
(265, 39, 51, 30, 500, 5, 500, 1),
(266, 39, 52, 45, 550, 5, 550, 1),
(267, 39, 53, 15, 100, 5, 100, 1),
(268, 39, 54, 30, 150, 5, 150, 1),
(269, 39, 55, 30, 150, 5, 150, 1),
(270, 39, 56, 45, 300, 5, 300, 1),
(271, 39, 61, 15, 150, 5, 150, 1),
(272, 39, 62, 15, 150, 5, 150, 1),
(273, 39, 63, 15, 300, 5, 300, 1),
(274, 39, 64, 15, 350, 5, 350, 1),
(275, 39, 65, 15, 400, 5, 400, 1),
(276, 39, 66, 15, 350, 5, 350, 1),
(277, 39, 67, 15, 550, 5, 550, 1),
(278, 39, 68, 15, 600, 5, 600, 1),
(279, 39, 71, 15, 300, 5, 300, 1),
(280, 39, 72, 15, 200, 5, 200, 1),
(281, 39, 73, 15, 450, 5, 450, 1),
(282, 39, 74, 15, 200, 5, 200, 1),
(283, 39, 75, 15, 200, 5, 200, 1),
(284, 25, 29, 60, 300, 5, 300, 1),
(285, 25, 31, 60, 400, 5, 400, 1),
(286, 25, 53, 60, 300, 5, 300, 1),
(287, 25, 55, 60, 400, 5, 400, 1),
(288, 25, 61, 30, 200, 5, 200, 1),
(289, 25, 62, 30, 200, 5, 200, 1),
(290, 25, 64, 30, 400, 5, 400, 1),
(291, 25, 72, 30, 350, 5, 350, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE IF NOT EXISTS `marca` (
  `marca_id` int(11) NOT NULL,
  `marca_nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`marca_id`, `marca_nombre`) VALUES
(1, 'Otras Marcas'),
(66, 'Vital Can'),
(67, 'Pro Plan'),
(68, 'Top Nutrition'),
(69, 'Tiernitos'),
(70, 'Royal Canin'),
(71, 'Raza'),
(72, 'Pedigree'),
(73, 'Old Prince'),
(74, 'Nutribon'),
(75, 'Natural Meat'),
(76, 'Mira'),
(77, 'Mapu'),
(78, 'Kongo'),
(79, 'Kongo Gold'),
(80, 'Ken-L Ration'),
(81, 'Iams'),
(82, 'Holliday MV'),
(83, 'Excellent'),
(84, 'Exact'),
(85, 'Eukanuba'),
(86, 'Dogui'),
(87, 'Dog Selection'),
(88, 'Dog Chow'),
(89, 'Belcan'),
(90, 'Biopet'),
(91, 'Sieger'),
(92, 'Profesional Vet'),
(94, 'Kongo Gourmet'),
(95, 'Voraz'),
(97, 'Gati'),
(98, 'Cat Selection'),
(99, 'Belcat'),
(100, 'CatChow'),
(101, 'Whiskas'),
(104, 'Power'),
(105, 'FRONTLINE'),
(106, 'Advantix'),
(107, 'NexGard'),
(108, 'Ecthol'),
(109, 'Fider'),
(110, 'Protech'),
(111, 'Advocate'),
(112, 'TOTAL FULL CG'),
(113, 'TOTAL FULL LC'),
(114, 'Meltra'),
(115, 'Seresto'),
(116, 'Osteocart Plus'),
(117, 'GERIOOX'),
(118, 'Artrin'),
(119, 'BACTROVET PLATA AM'),
(120, 'Advantage'),
(121, 'TOTAL FULL ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_animal`
--

CREATE TABLE IF NOT EXISTS `marca_animal` (
  `marca_animal_id` int(11) NOT NULL,
  `marca_animal_marca_id` int(11) DEFAULT NULL,
  `marca_animal_animal_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca_animal`
--

INSERT INTO `marca_animal` (`marca_animal_id`, `marca_animal_marca_id`, `marca_animal_animal_id`) VALUES
(92, 41, 1),
(93, 41, 2),
(94, 42, 1),
(95, 43, 1),
(96, 43, 2),
(97, 44, 1),
(98, 44, 2),
(101, 45, 1),
(102, 45, 2),
(103, 46, 1),
(104, 47, 1),
(105, 47, 2),
(106, 48, 1),
(107, 48, 2),
(108, 49, 1),
(109, 50, 1),
(110, 50, 2),
(111, 51, 1),
(112, 51, 2),
(113, 52, 1),
(114, 52, 2),
(115, 53, 1),
(116, 53, 2),
(117, 54, 1),
(118, 54, 2),
(119, 55, 1),
(120, 55, 2),
(121, 56, 1),
(122, 56, 2),
(125, 57, 1),
(126, 57, 2),
(127, 58, 1),
(128, 59, 1),
(129, 60, 1),
(130, 60, 2),
(133, 61, 1),
(134, 62, 2),
(135, 63, 1),
(136, 63, 2),
(137, 64, 1),
(138, 64, 2),
(141, 40, 1),
(142, 40, 2),
(143, 66, 1),
(144, 66, 2),
(145, 67, 1),
(146, 67, 2),
(147, 68, 1),
(148, 68, 2),
(149, 69, 1),
(150, 70, 1),
(151, 70, 2),
(153, 72, 1),
(155, 73, 1),
(156, 73, 2),
(158, 75, 1),
(159, 74, 1),
(160, 74, 2),
(161, 76, 1),
(162, 76, 2),
(163, 77, 1),
(164, 77, 2),
(165, 78, 1),
(166, 79, 1),
(167, 80, 1),
(168, 80, 2),
(169, 81, 1),
(170, 81, 2),
(171, 82, 1),
(172, 82, 2),
(173, 83, 1),
(174, 83, 2),
(175, 84, 1),
(176, 84, 2),
(177, 85, 1),
(178, 86, 1),
(181, 88, 1),
(184, 90, 1),
(185, 90, 2),
(186, 91, 1),
(187, 92, 1),
(188, 92, 2),
(190, 94, 2),
(191, 95, 1),
(192, 95, 2),
(196, 97, 2),
(197, 87, 1),
(198, 98, 2),
(200, 89, 1),
(201, 99, 2),
(202, 100, 2),
(203, 101, 2),
(204, 71, 1),
(205, 71, 2),
(210, 104, 1),
(211, 104, 2),
(212, 105, 1),
(213, 105, 2),
(214, 106, 1),
(215, 107, 1),
(217, 109, 1),
(218, 110, 1),
(219, 111, 1),
(220, 111, 2),
(221, 112, 1),
(226, 116, 1),
(227, 117, 1),
(228, 117, 2),
(229, 118, 1),
(230, 118, 2),
(231, 119, 1),
(232, 119, 2),
(233, 108, 1),
(234, 108, 2),
(235, 120, 2),
(236, 113, 1),
(237, 113, 2),
(238, 121, 2),
(239, 114, 1),
(240, 114, 2),
(241, 115, 1),
(242, 115, 2),
(243, 1, 1),
(244, 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca_rubro`
--

CREATE TABLE IF NOT EXISTS `marca_rubro` (
  `marca_rubro_id` int(11) NOT NULL,
  `marca_rubro_rubro_id` int(11) NOT NULL,
  `marca_rubro_marca_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca_rubro`
--

INSERT INTO `marca_rubro` (`marca_rubro_id`, `marca_rubro_rubro_id`, `marca_rubro_marca_id`) VALUES
(1, 5, 66),
(2, 5, 67),
(3, 5, 68),
(4, 5, 69),
(5, 5, 70),
(7, 5, 72),
(8, 5, 73),
(9, 5, 74),
(10, 5, 75),
(11, 5, 76),
(12, 5, 77),
(13, 5, 78),
(14, 5, 79),
(15, 5, 80),
(16, 5, 81),
(17, 5, 82),
(18, 5, 83),
(19, 5, 84),
(20, 5, 85),
(21, 5, 86),
(23, 5, 88),
(25, 5, 90),
(26, 5, 91),
(27, 5, 92),
(29, 5, 94),
(30, 5, 95),
(33, 5, 97),
(34, 5, 87),
(35, 5, 98),
(37, 5, 89),
(38, 5, 99),
(39, 5, 100),
(40, 5, 101),
(41, 5, 71),
(45, 7, 104),
(46, 7, 105),
(47, 7, 106),
(48, 7, 107),
(50, 7, 109),
(51, 7, 110),
(52, 7, 111),
(53, 7, 112),
(58, 7, 116),
(59, 7, 117),
(60, 7, 118),
(61, 7, 119),
(62, 7, 108),
(63, 7, 120),
(64, 7, 113),
(65, 7, 121),
(66, 7, 114),
(67, 7, 115),
(68, 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mascota`
--

CREATE TABLE IF NOT EXISTS `mascota` (
  `mascota_id` int(11) NOT NULL,
  `mascota_nombre` varchar(100) NOT NULL,
  `mascota_animal_id` int(11) NOT NULL,
  `mascota_nacimiento` date NOT NULL,
  `mascota_raza` varchar(100) NOT NULL,
  `mascota_sexo` varchar(6) NOT NULL,
  `mascota_pelaje` varchar(100) NOT NULL,
  `mascota_talla` varchar(10) NOT NULL,
  `mascota_foto` varchar(255) NOT NULL,
  `mascota_cliente_id` int(11) NOT NULL,
  `mascota_local_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mascota`
--

INSERT INTO `mascota` (`mascota_id`, `mascota_nombre`, `mascota_animal_id`, `mascota_nacimiento`, `mascota_raza`, `mascota_sexo`, `mascota_pelaje`, `mascota_talla`, `mascota_foto`, `mascota_cliente_id`, `mascota_local_id`) VALUES
(1, 'Test', 1, '2010-02-04', '', 'hembra', 'Marron', 'peque', '', 22, 26),
(2, 'Anka', 1, '2008-03-20', 'Ovejero Aleman', 'hembra', 'Largo', 'grande', '13_707066582.JPG', 13, 25),
(5, 'Samantha', 1, '2010-07-29', 'Boxer', 'hembra', 'Corto marrón con manchas negras y blancas', 'grande', '3_466941581.JPG', 3, 23),
(8, 'Milo', 1, '2002-12-08', '', 'macho', '', 'mediano', '34_450450887.jpg', 34, 23),
(9, 'tody', 1, '2016-09-12', 'manto negro', 'macho', 'no muy largo y brillante', 'grande', '', 39, 30),
(13, 'Layla', 1, '2011-06-22', 'Mestizo', 'hembra', 'Corto', 'mediano', '', 13, 30),
(14, 'Flora', 2, '2012-01-01', 'Persa', 'hembra', 'Largo', 'mediano', '13_746086517.jpg', 13, 35),
(15, 'Test', 1, '2017-05-15', 'raza', 'hembra', 't', 'peque', '43_123372248.png', 43, 23),
(20, 'Chicha', 1, '2017-05-05', 'Pepe', 'hembra', 'Asd', 'mediano', '28_122964602.jpg', 28, 25),
(21, 'Mobile', 1, '2017-05-18', 'mestizo', 'hembra', 'negro', 'peque', '43_686678081.jpg', 43, 23),
(24, 'Nina', 1, '2012-10-10', 'Dogo de Burdeos', 'hembra', 'rubia linda', 'gigante', '', 50, 0),
(25, 'lila', 1, '2017-05-05', 'pepe', 'hembra', 'negro y marron', 'peque', '28_496580322.jpg', 28, 0),
(26, 'Pluto', 1, '2002-08-20', 'San bernardo', 'macho', 'Largo', 'gigante', '', 13, 33),
(27, 'Jorgelina', 2, '2017-01-05', 'siames', 'hembra', 'gris', 'peque', '', 14, 25);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medio_pago`
--

CREATE TABLE IF NOT EXISTS `medio_pago` (
  `medio_pago_id` int(11) NOT NULL,
  `medio_pago_nombre` varchar(200) NOT NULL,
  `medio_pago_items` tinyint(1) NOT NULL DEFAULT '0',
  `medio_pago_imagen` varchar(200) NOT NULL,
  `medio_pago_estado` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `medio_pago`
--

INSERT INTO `medio_pago` (`medio_pago_id`, `medio_pago_nombre`, `medio_pago_items`, `medio_pago_imagen`, `medio_pago_estado`) VALUES
(1, 'Mercado Pago', 0, 'mercado_pago.png', 0),
(10, 'Tarjetas de Crédito', 1, 'credito.png', 1),
(11, 'Tarjetas de Débito', 1, 'debito.png', 1),
(13, 'Efectivo', 0, 'efectivo - copia - copia.jpg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medio_pago_item`
--

CREATE TABLE IF NOT EXISTS `medio_pago_item` (
  `medio_pago_item_id` int(11) NOT NULL,
  `medio_pago_item_medio_pago_id` int(11) NOT NULL,
  `medio_pago_item_nombre` varchar(200) NOT NULL,
  `medio_pago_item_imagen` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `medio_pago_item`
--

INSERT INTO `medio_pago_item` (`medio_pago_item_id`, `medio_pago_item_medio_pago_id`, `medio_pago_item_nombre`, `medio_pago_item_imagen`) VALUES
(3, 10, 'VISA', '_381612058.png'),
(4, 10, 'MasterCard', '_659611304.png'),
(5, 10, 'Diners Club', '_354940168.png'),
(7, 10, 'Naranja', '_638102462.png'),
(8, 11, 'VISA', '_456163431.png'),
(9, 10, 'AMERICAN EXPRESS', '_477346644.png'),
(11, 10, 'Nativa VISA', '_863519342.png'),
(12, 10, 'CABAL', '_248008638.png'),
(13, 11, 'MasterCard', '_659611304.png'),
(14, 11, 'Maestro', '_219198946.png'),
(15, 11, 'CABAL', '_543403460.png'),
(16, 10, 'Nativa MasterCard', '_863519342.png'),
(17, 10, 'Argencard', '_749774828.png'),
(18, 10, 'Cencosud MasterCard', '_395819302.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion`
--

CREATE TABLE IF NOT EXISTS `notificacion` (
  `notificacion_id` int(11) NOT NULL,
  `notificacion_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notificacion_tipo` varchar(50) NOT NULL,
  `notificacion_texto` text NOT NULL,
  `notificacion_vista` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_leida` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_turno_id` int(11) NOT NULL,
  `notificacion_venta_id` int(11) NOT NULL,
  `notificacion_local_id` int(11) NOT NULL DEFAULT '0',
  `notificacion_cliente_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notificacion`
--

INSERT INTO `notificacion` (`notificacion_id`, `notificacion_fecha`, `notificacion_tipo`, `notificacion_texto`, `notificacion_vista`, `notificacion_leida`, `notificacion_turno_id`, `notificacion_venta_id`, `notificacion_local_id`, `notificacion_cliente_id`) VALUES
(343, '2017-08-15 16:31:35', 'aviso turno', '<b>Mascotas</b> avisó que el servicio <b>S0173</b> si fue concretado.', 1, 0, 173, 0, 25, 13),
(344, '2017-08-15 16:36:23', 'calificación turno', '<b>German Vide</b> calificó al local <b>Mascotas</b> con 4 estrellas', 1, 0, 173, 0, 25, 13),
(345, '2017-08-15 16:42:13', 'aviso turno', '<b>Mascotas</b> avisó que el servicio <b>S0171</b> no fue concretado.', 1, 1, 171, 0, 25, 13),
(346, '2017-08-15 16:45:46', 'calificación turno', '<b>German Vide</b> calificó al local <b>Mascotas</b> con 1 estrellas', 1, 1, 171, 0, 25, 13),
(347, '2017-08-15 16:48:00', 'calificación turno', '<b>German Vide</b> calificó al local <b>Pájaro Loco</b> con 4 estrellas', 1, 1, 172, 0, 23, 13),
(348, '2017-08-15 17:02:23', 'aviso turno', '<b>Pájaro Loco</b> avisó que el servicio <b>S0172</b> si fue concretado.', 1, 1, 172, 0, 23, 13),
(349, '2017-08-16 00:05:43', 'turno', '<b>Pedro Carreras</b> solicitó un turno en <b>Pájaro Loco</b>', 1, 1, 174, 0, 23, 43),
(350, '2017-08-16 00:06:24', 'cancela turno', '<b>Pedro Carreras</b> canceló un turno en <b>Pájaro Loco</b>', 1, 1, 174, 0, 23, 43),
(351, '2017-08-16 01:41:44', 'registro', '<b>maria zarasa</b> se registró en GeoTienda', 1, 1, 0, 0, 0, 69),
(352, '2017-08-16 02:23:03', 'turno', '<b>German Vide</b> solicitó un turno en <b>Mascotas</b>', 1, 1, 175, 0, 25, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion_cliente`
--

CREATE TABLE IF NOT EXISTS `notificacion_cliente` (
  `notificacion_cliente_id` int(11) NOT NULL,
  `notificacion_cliente_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notificacion_cliente_tipo` varchar(50) NOT NULL,
  `notificacion_cliente_texto` text NOT NULL,
  `notificacion_cliente_vista` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_cliente_leida` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_cliente_turno_id` int(11) NOT NULL,
  `notificacion_cliente_venta_id` int(11) NOT NULL,
  `notificacion_cliente_vacuna_id` int(11) NOT NULL,
  `notificacion_cliente_local_id` int(11) NOT NULL DEFAULT '0',
  `notificacion_cliente_cliente_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notificacion_cliente`
--

INSERT INTO `notificacion_cliente` (`notificacion_cliente_id`, `notificacion_cliente_fecha`, `notificacion_cliente_tipo`, `notificacion_cliente_texto`, `notificacion_cliente_vista`, `notificacion_cliente_leida`, `notificacion_cliente_turno_id`, `notificacion_cliente_venta_id`, `notificacion_cliente_vacuna_id`, `notificacion_cliente_local_id`, `notificacion_cliente_cliente_id`) VALUES
(16, '2017-08-17 05:00:00', 'recordatorio turno', 'El <b>17/08/2017</b> a las <b>10:15 hs.</b> tenés un turno en <b>Pájaro Loco</b>', 1, 1, 174, 0, 0, 23, 43),
(17, '2017-08-18 05:00:00', 'recordatorio turno', 'El <b>18/08/2017</b> a las <b>17:00 hs.</b> tenés un turno en <b>Mascotas</b>', 1, 1, 175, 0, 0, 25, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificacion_local`
--

CREATE TABLE IF NOT EXISTS `notificacion_local` (
  `notificacion_local_id` int(11) NOT NULL,
  `notificacion_local_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `notificacion_local_tipo` varchar(50) NOT NULL,
  `notificacion_local_texto` text NOT NULL,
  `notificacion_local_vista` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_local_leida` tinyint(4) NOT NULL DEFAULT '0',
  `notificacion_local_turno_id` int(11) NOT NULL,
  `notificacion_local_venta_id` int(11) NOT NULL,
  `notificacion_local_vacuna_id` int(11) NOT NULL,
  `notificacion_local_local_id` int(11) NOT NULL DEFAULT '0',
  `notificacion_local_cliente_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `notificacion_local`
--

INSERT INTO `notificacion_local` (`notificacion_local_id`, `notificacion_local_fecha`, `notificacion_local_tipo`, `notificacion_local_texto`, `notificacion_local_vista`, `notificacion_local_leida`, `notificacion_local_turno_id`, `notificacion_local_venta_id`, `notificacion_local_vacuna_id`, `notificacion_local_local_id`, `notificacion_local_cliente_id`) VALUES
(247, '2017-08-16 00:05:43', 'turno', '<b>Pedro Carreras</b> solicitó un turno', 0, 0, 174, 0, 0, 23, 43),
(248, '2017-08-17 05:00:00', 'recordatorio turno', 'El <b>17/08/2017</b> a las <b>10:15 hs.</b> tenés un turno con <b>Pedro Carreras</b>', 0, 0, 174, 0, 0, 23, 43),
(249, '2017-08-16 00:06:24', 'cancela turno', '<b>Pedro Carreras</b> canceló un turno', 0, 0, 174, 0, 0, 23, 43),
(250, '2017-08-16 02:23:03', 'turno', '<b>German Vide</b> solicitó un turno', 1, 1, 175, 0, 0, 25, 13),
(251, '2017-08-18 05:00:00', 'recordatorio turno', 'El <b>18/08/2017</b> a las <b>17:00 hs.</b> tenés un turno con <b>German Vide</b>', 1, 1, 175, 0, 0, 25, 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentacion`
--

CREATE TABLE IF NOT EXISTS `presentacion` (
  `presentacion_id` int(11) NOT NULL,
  `presentacion_nombre` varchar(255) DEFAULT NULL,
  `presentacion_orden` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `presentacion`
--

INSERT INTO `presentacion` (`presentacion_id`, `presentacion_nombre`, `presentacion_orden`) VALUES
(1, 'Unica Presentación', -1),
(50, '2 Kg', 14),
(51, '7,5 Kg', 23),
(52, '15 Kg', 30),
(53, '1,5 Kg', 13),
(54, '20 Kg', 31),
(55, '12 Kg', 28),
(56, '3 Kg', 18),
(57, '1 Kg', 10),
(58, '8 Kg', 24),
(59, '22 Kg', 33),
(60, '2,5 Kg', 15),
(61, '800 Gr', 8),
(62, '10 Kg', 26),
(63, '3,5 Kg', 19),
(64, '9 Kg', 25),
(65, '13 Kg', 29),
(66, '4 Kg', 20),
(67, '21 Kg', 32),
(68, '1,3 Kg', 12),
(69, '2,7 Kg', 16),
(70, '85 Gr', 2),
(71, '100 Gr', 3),
(72, '340 Gr', 5),
(73, '7 Kg', 22),
(74, '25 Kg', 34),
(75, '1,2 Kg', 11),
(76, '300 Gr', 4),
(77, '500 Gr', 7),
(78, '75 Gr', 1),
(79, '2,8 Kg', 17),
(80, '750 Gr', 7),
(81, '400 Gr', 6),
(83, 'De 2 a 4 Kg', 36),
(84, 'De 5 a 10 Kg', 37),
(85, 'De 11 a 20 Kg', 38),
(86, 'De 21 a 40 Kg', 39),
(87, 'De 41 a 60 Kg', 40),
(88, '11 Kg', 27),
(90, 'Única Presentacion', 35),
(91, '6 Kg', 21),
(92, 'De 2,5 a 5 Kg', 41),
(93, 'De 5,1 a 10 Kg', 42),
(94, 'De 10,1 a 20 Kg', 43),
(95, 'De 20,1 a 30 Kg', 44),
(96, 'De 30,1 a 40 Kg', 45),
(97, '100 ML', 75),
(99, 'De 10 a 20 Kg', 46),
(100, 'De 20 a 40 Kg', 47),
(101, ' De 40 a 60 Kg', 48),
(102, '250 ML', 77),
(103, '500 ML', 79),
(104, 'De 2 a 5 Kg', 49),
(106, 'De 1,5 a 4 Kg', 50),
(107, 'De 4 a 10 Kg', 51),
(108, 'De 10 a 25 Kg', 52),
(109, 'Mas de 25 Kg', 72),
(110, 'De 4,1 a 10 Kg', 53),
(111, 'De 10,1 a 25 Kg', 54),
(112, 'De 25,1 a 50 Kg', 55),
(113, 'Hasta 5 Kg', 68),
(114, 'De 6 a 10 Kg', 58),
(115, 'De 1 a 5 Kg', 56),
(116, 'De 11 a 25 Kg', 59),
(117, 'De 26 a 40 Kg', 60),
(118, 'Mas de 40 Kg', 73),
(119, 'De 2 a 10 Kg', 61),
(120, 'De 25 a 40 Kg', 62),
(121, '15 ML', 74),
(122, '150 ML', 76),
(123, 'De 1 a 10 Kg', 63),
(124, 'De 1 a 20 Kg', 64),
(125, 'De 1 a 60 Kg', 66),
(126, 'De 1 a 40 Kg', 65),
(127, 'Hasta 8 Kg', 69),
(128, 'Mas de 8 Kg', 71),
(129, '30 Comprimidos', 82),
(130, '120 Comprimidos', 84),
(131, '90 Comprimidos', 83),
(132, '20 Comprimidos', 81),
(133, '440 ML', 78),
(134, 'Hasta 4 Kg', 67),
(135, 'De 4 a 8 Kg', 57),
(136, 'Mas de 5 Kg', 70),
(137, '2 Comprimidos', 80);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `presentacion_rubro`
--

CREATE TABLE IF NOT EXISTS `presentacion_rubro` (
  `presentacion_rubro_id` int(11) NOT NULL,
  `presentacion_rubro_rubro_id` int(11) NOT NULL,
  `presentacion_rubro_presentacion_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `presentacion_rubro`
--

INSERT INTO `presentacion_rubro` (`presentacion_rubro_id`, `presentacion_rubro_rubro_id`, `presentacion_rubro_presentacion_id`) VALUES
(4, 7, 83),
(5, 7, 84),
(6, 7, 85),
(7, 7, 86),
(8, 7, 87),
(9, 5, 91),
(10, 7, 92),
(11, 7, 93),
(12, 7, 94),
(13, 7, 95),
(14, 7, 96),
(15, 7, 97),
(16, 7, 99),
(17, 7, 100),
(18, 7, 101),
(19, 7, 102),
(20, 7, 103),
(21, 7, 104),
(22, 7, 106),
(23, 7, 107),
(24, 7, 108),
(26, 7, 110),
(27, 7, 111),
(28, 7, 112),
(29, 7, 113),
(30, 7, 114),
(31, 7, 115),
(32, 7, 116),
(33, 7, 117),
(34, 7, 118),
(35, 7, 119),
(36, 7, 120),
(37, 7, 121),
(38, 7, 122),
(39, 7, 123),
(40, 7, 124),
(41, 7, 125),
(42, 7, 126),
(43, 7, 127),
(44, 7, 128),
(45, 5, 78),
(46, 5, 70),
(47, 5, 71),
(48, 5, 76),
(49, 5, 72),
(50, 5, 81),
(51, 5, 77),
(52, 5, 80),
(53, 5, 61),
(54, 5, 57),
(55, 5, 75),
(56, 5, 68),
(57, 5, 53),
(58, 5, 50),
(59, 5, 60),
(60, 5, 69),
(61, 5, 79),
(62, 5, 56),
(63, 5, 63),
(64, 5, 66),
(65, 5, 73),
(66, 5, 51),
(67, 5, 58),
(68, 5, 64),
(69, 5, 62),
(70, 5, 88),
(71, 5, 55),
(72, 5, 65),
(73, 5, 52),
(74, 5, 54),
(75, 5, 67),
(76, 5, 59),
(77, 5, 74),
(78, 7, 129),
(79, 7, 130),
(80, 7, 131),
(81, 7, 132),
(82, 7, 133),
(83, 7, 134),
(84, 7, 135),
(85, 7, 90),
(86, 7, 136),
(87, 7, 137),
(89, 7, 109);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `provincia_id` int(1) NOT NULL,
  `provincia_nombre` varchar(255) NOT NULL,
  `provincia_cpa` varchar(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`provincia_id`, `provincia_nombre`, `provincia_cpa`) VALUES
(1, 'Buenos Aires', 'B'),
(2, 'Catamarca', 'K'),
(3, 'Chaco', 'H'),
(4, 'Chubut', 'U'),
(5, 'Ciudad Autónoma de Buenos Aires', 'C'),
(6, 'Córdoba', 'X'),
(7, 'Corrientes', 'W'),
(8, 'Entre Ríos', 'E'),
(9, 'Formosa', 'P'),
(10, 'Jujuy', 'Y'),
(11, 'La Pampa', 'L'),
(12, 'La Rioja', 'F'),
(13, 'Mendoza', 'M'),
(14, 'Misiones', 'N'),
(15, 'Neuquén', 'Q'),
(16, 'Río Negro', 'R'),
(17, 'Salta', 'A'),
(18, 'San Juan', 'J'),
(19, 'San Luis', 'D'),
(20, 'Santa Cruz', 'Z'),
(21, 'Santa Fe', 'S'),
(22, 'Santiago del Estero', 'G'),
(23, 'Tierra del Fuego, Antártida e Islas del Atlántico Sur', 'V'),
(24, 'Tucumán', 'T');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `raza`
--

CREATE TABLE IF NOT EXISTS `raza` (
  `raza_id` int(11) NOT NULL,
  `raza_nombre` varchar(255) DEFAULT NULL,
  `raza_id_animal` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `raza`
--

INSERT INTO `raza` (`raza_id`, `raza_nombre`, `raza_id_animal`) VALUES
(1, 'Todas las Razas', 1),
(2, 'Bulldog Francés', 1),
(3, 'Caniche Poodle', 1),
(4, 'Yorkshire Terrier', 1),
(5, 'Chihuahua ', 1),
(6, 'Dachshund Teckel', 1),
(7, 'Miniature Schnauzer', 1),
(8, 'Bulldog', 1),
(9, 'Ovejero Alemán', 1),
(10, 'Labrador Retriever ', 1),
(11, 'Golden Retriever', 1),
(12, 'Boxer', 1),
(13, 'Cocker Spaniel', 1),
(14, 'Shih Tzu', 1),
(15, 'Toy Poodle', 1),
(16, 'Rottweiler', 1),
(17, 'Siamese', 2),
(18, 'Persa', 2),
(19, 'Todas las Razas', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recordatorio`
--

CREATE TABLE IF NOT EXISTS `recordatorio` (
  `recordatorio_id` int(11) NOT NULL,
  `recordatorio_venta_id` int(11) NOT NULL,
  `recordatorio_turno_id` int(11) NOT NULL,
  `recordatorio_cliente_ultimo_enviado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `recordatorio_cliente_enviados` int(11) NOT NULL DEFAULT '0',
  `recordatorio_local_ultimo_enviado` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recordatorio_local_enviados` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=416 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recordatorio`
--

INSERT INTO `recordatorio` (`recordatorio_id`, `recordatorio_venta_id`, `recordatorio_turno_id`, `recordatorio_cliente_ultimo_enviado`, `recordatorio_cliente_enviados`, `recordatorio_local_ultimo_enviado`, `recordatorio_local_enviados`) VALUES
(1, 110, 0, '2017-01-04 14:14:01', 5, '2017-01-04 14:14:01', 5),
(2, 111, 0, '2017-01-04 14:14:01', 1, '2017-01-04 14:14:01', 5),
(3, 0, 13, '2017-01-04 14:14:11', 1, '2017-01-04 14:14:11', 5),
(4, 112, 0, '2017-01-04 14:14:01', 0, '2017-01-04 14:14:01', 5),
(5, 0, 14, '2017-01-04 14:14:11', 1, '2017-01-04 14:14:11', 5),
(6, 113, 0, '2017-01-04 14:14:02', 1, '2017-01-04 14:14:02', 5),
(7, 0, 15, '2017-01-04 14:14:11', 1, '2017-01-04 14:14:11', 5),
(8, 114, 0, '2017-01-04 14:14:02', 1, '2017-01-04 14:14:02', 5),
(9, 115, 0, '2017-01-04 14:14:02', 1, '2017-01-04 14:14:02', 5),
(10, 116, 0, '2017-01-04 14:14:02', 1, '2017-01-04 14:14:02', 5),
(11, 117, 0, '2017-01-04 14:14:03', 1, '2017-01-04 14:14:03', 5),
(12, 118, 0, '2017-01-04 14:14:03', 5, '2017-01-04 14:14:03', 5),
(13, 0, 16, '2017-01-04 14:14:12', 5, '2017-01-04 14:14:11', 5),
(14, 0, 17, '2017-01-04 14:14:12', 5, '2017-01-04 14:14:12', 5),
(15, 0, 18, '2017-01-04 14:14:12', 5, '2017-01-04 14:14:12', 5),
(16, 119, 0, '2017-01-04 14:14:03', 0, '2017-01-04 14:14:03', 5),
(17, 120, 0, '2017-01-04 14:14:04', 5, '2017-01-04 14:14:04', 5),
(18, 121, 0, '2017-01-04 14:14:04', 5, '2017-01-04 14:14:04', 5),
(19, 122, 0, '2017-01-04 14:14:04', 5, '2017-01-04 14:14:04', 5),
(20, 123, 0, '2017-01-04 14:14:04', 5, '2017-01-04 14:14:04', 5),
(21, 124, 0, '2017-01-04 14:14:05', 1, '2017-01-04 14:14:05', 5),
(22, 0, 19, '2017-01-04 14:14:12', 1, '2017-01-04 14:14:12', 5),
(23, 125, 0, '2017-01-04 14:14:05', 0, '2017-01-04 14:14:05', 5),
(24, 126, 0, '2017-01-04 14:14:05', 0, '2017-01-04 14:14:05', 5),
(25, 0, 20, '2017-01-04 14:14:12', 1, '2017-01-04 14:14:12', 5),
(26, 127, 0, '2017-01-04 14:14:05', 5, '2017-01-04 14:14:05', 5),
(27, 128, 0, '2017-01-04 14:14:06', 5, '2017-01-04 14:14:06', 5),
(28, 129, 0, '2017-01-04 14:14:06', 5, '2017-01-04 14:14:06', 5),
(29, 0, 21, '2017-01-04 14:14:13', 1, '2017-01-04 14:14:13', 5),
(30, 130, 0, '2017-01-04 14:14:06', 5, '2017-01-04 14:14:06', 5),
(31, 131, 0, '2017-01-04 14:14:07', 1, '2017-01-04 14:14:07', 5),
(32, 132, 0, '2017-01-04 14:14:07', 5, '2017-01-04 14:14:07', 5),
(33, 0, 22, '2017-01-04 14:14:13', 5, '2017-01-04 14:14:13', 5),
(34, 0, 23, '2017-01-04 14:14:13', 5, '2017-01-04 14:14:13', 5),
(35, 133, 0, '2017-01-04 14:14:07', 5, '2017-01-04 14:14:07', 5),
(36, 0, 24, '2017-01-04 14:14:14', 5, '2017-01-04 14:14:14', 5),
(37, 0, 25, '2017-01-04 14:14:14', 1, '2017-01-04 14:14:14', 5),
(38, 134, 0, '2017-01-04 14:14:07', 5, '2017-01-04 14:14:07', 5),
(39, 0, 26, '2017-01-04 14:14:14', 5, '2017-01-04 14:14:14', 5),
(40, 0, 27, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(41, 0, 28, '2017-01-04 14:14:14', 5, '2017-01-04 14:14:14', 5),
(42, 0, 29, '2017-01-04 14:14:15', 5, '2017-01-04 14:14:15', 5),
(43, 0, 30, '2017-01-04 14:14:15', 5, '2017-01-04 14:14:15', 5),
(44, 0, 31, '2017-01-04 14:14:15', 5, '2017-01-04 14:14:15', 5),
(45, 0, 32, '2017-01-04 14:14:15', 1, '2017-01-04 14:14:15', 5),
(46, 135, 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(47, 0, 33, '2017-01-04 14:14:16', 1, '2017-01-04 14:14:16', 5),
(48, 136, 0, '2017-01-04 14:13:52', 5, '0000-00-00 00:00:00', 0),
(49, 0, 34, '2017-01-04 14:14:16', 5, '2017-01-04 14:14:16', 5),
(50, 0, 35, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0),
(51, 137, 0, '2017-01-04 14:14:08', 5, '2017-01-04 14:14:08', 5),
(52, 0, 36, '2017-01-04 14:14:16', 5, '2017-01-04 14:14:16', 5),
(53, 138, 0, '2017-01-04 14:14:08', 5, '2017-01-04 14:14:08', 5),
(54, 0, 37, '2017-01-04 14:14:16', 1, '2017-01-04 14:14:16', 5),
(55, 139, 0, '2017-01-04 14:14:08', 5, '2017-01-04 14:14:08', 5),
(57, 140, 0, '2017-01-04 14:14:08', 5, '2017-01-04 14:14:08', 5),
(58, 141, 0, '2017-01-04 14:14:09', 5, '2017-01-04 14:14:09', 5),
(59, 142, 0, '2017-01-04 14:14:09', 5, '2017-01-04 14:14:09', 5),
(60, 143, 0, '2017-01-04 14:14:09', 5, '2017-01-04 14:14:09', 5),
(61, 0, 38, '2017-01-04 14:14:17', 5, '2017-01-04 14:14:17', 5),
(62, 0, 39, '2017-01-04 14:14:17', 5, '2017-01-04 14:14:17', 5),
(63, 0, 40, '2017-01-04 14:14:17', 5, '2017-01-04 14:14:17', 5),
(64, 0, 41, '2017-01-04 14:14:17', 5, '2017-01-04 14:14:17', 5),
(65, 0, 42, '2017-01-04 14:14:18', 5, '2017-01-04 14:14:18', 5),
(66, 144, 0, '2017-01-04 14:14:09', 1, '2017-01-04 14:14:09', 5),
(67, 0, 43, '2017-01-04 14:14:18', 1, '2017-01-04 14:14:18', 5),
(68, 0, 44, '2017-01-04 14:14:18', 5, '2017-01-04 14:14:18', 5),
(69, 0, 45, '2017-01-04 14:14:18', 5, '2017-01-04 14:14:18', 5),
(70, 0, 46, '2017-01-04 14:14:19', 0, '2017-01-04 14:14:19', 5),
(71, 145, 0, '2017-01-04 14:14:10', 1, '2017-01-04 14:14:10', 5),
(72, 0, 47, '2017-01-04 14:14:19', 5, '2017-01-04 14:14:19', 5),
(73, 0, 48, '2017-01-04 14:14:19', 5, '2017-01-04 14:14:19', 5),
(74, 146, 0, '2017-01-04 14:14:10', 0, '2017-01-04 14:14:10', 5),
(75, 0, 49, '2017-01-04 14:14:19', 5, '2017-01-04 14:14:19', 5),
(76, 147, 0, '2017-01-04 14:14:10', 0, '2017-01-04 14:14:10', 5),
(77, 0, 50, '2017-01-05 12:15:06', 5, '2017-01-05 12:15:06', 5),
(78, 148, 0, '2017-01-06 21:39:19', 0, '2017-01-06 21:39:19', 5),
(79, 149, 0, '2017-01-05 11:37:25', 5, '0000-00-00 00:00:00', 0),
(80, 0, 51, '2017-01-06 12:00:10', 0, '2017-01-06 12:00:10', 5),
(81, 0, 52, '2017-01-06 12:00:07', 5, '2017-01-06 11:26:06', 5),
(82, 150, 0, '2017-01-06 11:26:05', 0, '2017-01-06 11:26:05', 5),
(83, 0, 53, '2017-01-17 12:00:10', 5, '2017-01-17 12:00:10', 5),
(84, 0, 54, '2017-01-06 12:00:10', 2, '2017-01-06 12:00:10', 2),
(85, 151, 0, '2017-01-07 12:00:09', 0, '2017-01-07 12:00:09', 5),
(86, 0, 55, '2017-01-07 12:00:10', 5, '2017-01-07 12:00:10', 5),
(87, 0, 56, '2017-01-07 12:00:10', 5, '2017-01-07 12:00:10', 5),
(88, 0, 57, '2017-01-07 12:00:10', 5, '2017-01-07 12:00:10', 5),
(89, 0, 58, '2017-01-07 12:00:11', 5, '2017-01-07 12:00:11', 5),
(90, 152, 0, '2017-01-07 12:00:09', 5, '2017-01-07 12:00:09', 5),
(91, 0, 59, '2017-01-07 12:00:11', 5, '2017-01-07 12:00:11', 5),
(92, 153, 0, '2017-01-08 14:44:52', 5, '2017-01-08 14:44:52', 5),
(93, 154, 0, '2017-01-13 12:00:07', 5, '2017-01-09 12:00:07', 1),
(94, 0, 60, '2017-01-13 12:00:07', 5, '2017-01-10 12:00:08', 2),
(95, 0, 61, '2017-01-16 12:00:06', 5, '2017-01-15 12:00:07', 4),
(96, 155, 0, '2017-01-22 12:00:08', 5, '2017-01-22 12:00:08', 5),
(97, 0, 62, '2017-01-30 12:00:14', 2, '2017-01-30 12:00:14', 2),
(98, 156, 0, '2017-01-22 12:00:09', 0, '2017-01-22 12:00:09', 5),
(99, 157, 0, '2017-01-23 12:00:10', 5, '2017-01-23 12:00:10', 5),
(100, 158, 0, '2017-01-23 12:00:10', 5, '2017-01-23 12:00:10', 5),
(101, 159, 0, '2017-01-17 18:48:43', 0, '0000-00-00 00:00:00', 0),
(102, 160, 0, '2017-01-23 12:00:11', 5, '2017-01-23 12:00:11', 5),
(103, 161, 0, '2017-01-23 12:00:11', 5, '2017-01-23 12:00:11', 5),
(104, 162, 0, '2017-01-17 23:18:59', 0, '0000-00-00 00:00:00', 0),
(105, 163, 0, '2017-01-17 23:23:37', 0, '0000-00-00 00:00:00', 0),
(106, 0, 63, '2017-01-23 12:00:12', 5, '2017-01-23 12:00:12', 5),
(107, 0, 64, '2017-01-23 12:00:12', 5, '2017-01-23 12:00:12', 5),
(108, 0, 65, '2017-01-23 12:00:12', 5, '2017-01-23 12:00:12', 5),
(109, 164, 0, '2017-01-17 23:54:37', 0, '0000-00-00 00:00:00', 0),
(110, 165, 0, '2017-01-23 12:00:11', 0, '2017-01-23 12:00:11', 5),
(111, 166, 0, '2017-01-23 12:00:09', 5, '0000-00-00 00:00:00', 0),
(112, 167, 0, '2017-01-18 00:01:43', 0, '0000-00-00 00:00:00', 0),
(113, 0, 66, '2017-01-23 12:00:10', 5, '0000-00-00 00:00:00', 0),
(114, 168, 0, '2017-01-18 00:14:45', 0, '0000-00-00 00:00:00', 0),
(115, 169, 0, '2017-01-18 00:17:44', 0, '0000-00-00 00:00:00', 0),
(116, 170, 0, '2017-01-18 00:20:10', 0, '0000-00-00 00:00:00', 0),
(117, 171, 0, '2017-01-18 00:23:31', 0, '0000-00-00 00:00:00', 0),
(118, 172, 0, '2017-01-18 00:26:01', 0, '0000-00-00 00:00:00', 0),
(119, 173, 0, '2017-01-18 00:28:17', 0, '0000-00-00 00:00:00', 0),
(120, 174, 0, '2017-01-18 00:30:35', 0, '0000-00-00 00:00:00', 0),
(121, 175, 0, '2017-01-18 00:32:57', 0, '0000-00-00 00:00:00', 0),
(122, 176, 0, '2017-01-27 12:00:11', 5, '2017-01-27 12:00:11', 5),
(123, 0, 67, '2017-02-01 12:00:12', 5, '2017-02-01 12:00:12', 5),
(124, 0, 68, '2017-01-22 02:00:52', 0, '0000-00-00 00:00:00', 0),
(125, 0, 69, '2017-01-22 02:02:09', 0, '0000-00-00 00:00:00', 0),
(126, 0, 70, '2017-02-05 12:00:13', 1, '2017-02-05 12:00:13', 4),
(127, 177, 0, '2017-01-28 12:00:13', 5, '2017-01-28 12:00:13', 5),
(128, 178, 0, '2017-01-28 12:00:13', 0, '2017-01-28 12:00:13', 5),
(129, 179, 0, '2017-01-28 12:00:13', 5, '2017-01-28 12:00:13', 5),
(130, 180, 0, '2017-01-28 12:00:14', 5, '2017-01-28 12:00:14', 5),
(131, 0, 71, '2017-01-31 12:00:11', 5, '2017-01-31 12:00:11', 5),
(132, 0, 72, '2017-01-29 12:00:14', 5, '2017-01-29 12:00:14', 5),
(133, 181, 0, '2017-01-29 12:00:12', 5, '2017-01-29 12:00:12', 5),
(134, 0, 73, '2017-01-29 12:00:15', 5, '2017-01-29 12:00:15', 5),
(135, 182, 0, '2017-01-29 12:00:12', 5, '2017-01-29 12:00:12', 5),
(136, 183, 0, '2017-01-30 12:00:12', 5, '2017-01-30 12:00:12', 5),
(137, 0, 74, '2017-01-30 12:00:15', 5, '2017-01-30 12:00:15', 5),
(138, 184, 0, '2017-01-30 12:00:12', 5, '2017-01-30 12:00:12', 5),
(139, 0, 75, '2017-01-30 12:00:15', 5, '2017-01-30 12:00:15', 5),
(140, 185, 0, '2017-01-31 12:00:10', 5, '2017-01-31 12:00:10', 5),
(141, 186, 0, '2017-02-01 12:00:10', 5, '2017-02-01 12:00:10', 5),
(142, 0, 76, '2017-02-01 12:00:12', 5, '2017-02-01 12:00:12', 5),
(143, 0, 77, '2017-01-28 13:41:54', 0, '0000-00-00 00:00:00', 0),
(144, 187, 0, '2017-02-03 12:00:11', 5, '2017-02-03 12:00:11', 5),
(145, 0, 78, '2017-02-04 12:00:11', 5, '2017-02-04 12:00:11', 5),
(146, 188, 0, '2017-02-03 12:00:11', 5, '2017-02-03 12:00:11', 5),
(147, 189, 0, '2017-02-03 12:00:12', 5, '2017-02-03 12:00:12', 5),
(148, 190, 0, '2017-02-03 12:00:12', 5, '2017-02-03 12:00:12', 5),
(149, 0, 79, '2017-01-30 13:11:35', 0, '0000-00-00 00:00:00', 0),
(150, 191, 0, '2017-01-30 15:18:26', 0, '0000-00-00 00:00:00', 0),
(151, 0, 80, '2017-01-30 15:56:15', 0, '0000-00-00 00:00:00', 0),
(152, 0, 81, '2017-01-30 16:37:39', 0, '0000-00-00 00:00:00', 0),
(153, 192, 0, '2017-02-05 12:00:11', 5, '2017-02-05 12:00:11', 5),
(154, 0, 82, '2017-01-30 20:46:25', 0, '0000-00-00 00:00:00', 0),
(155, 0, 83, '2017-02-05 12:00:14', 2, '2017-02-05 12:00:14', 5),
(156, 0, 84, '2017-02-05 12:00:14', 4, '2017-02-05 12:00:14', 4),
(157, 193, 0, '2017-02-06 12:00:08', 5, '2017-02-06 12:00:08', 5),
(158, 0, 85, '2017-01-31 22:56:06', 0, '0000-00-00 00:00:00', 0),
(159, 194, 0, '2017-02-06 12:00:08', 5, '2017-02-06 12:00:08', 5),
(160, 195, 0, '2017-02-07 12:00:10', 5, '2017-02-07 12:00:10', 5),
(161, 196, 0, '2017-02-01 15:32:49', 0, '0000-00-00 00:00:00', 0),
(162, 197, 0, '2017-02-07 12:00:11', 0, '2017-02-07 12:00:11', 5),
(163, 0, 86, '2017-02-01 15:49:31', 0, '0000-00-00 00:00:00', 0),
(164, 0, 87, '2017-02-05 12:00:14', 0, '2017-02-05 12:00:14', 2),
(165, 198, 0, '2017-02-01 16:48:37', 0, '0000-00-00 00:00:00', 0),
(166, 0, 88, '2017-02-01 16:50:11', 0, '0000-00-00 00:00:00', 0),
(167, 199, 0, '2017-02-07 12:00:11', 5, '2017-02-07 12:00:11', 5),
(168, 200, 0, '2017-02-07 12:00:11', 5, '2017-02-07 12:00:11', 5),
(169, 201, 0, '2017-02-02 01:53:08', 0, '0000-00-00 00:00:00', 0),
(170, 0, 89, '2017-02-02 01:55:51', 0, '0000-00-00 00:00:00', 0),
(171, 202, 0, '2017-02-07 12:00:11', 0, '2017-02-07 12:00:11', 5),
(172, 0, 90, '2017-02-05 12:00:14', 0, '2017-02-05 12:00:14', 3),
(173, 203, 0, '2017-02-02 02:06:50', 0, '0000-00-00 00:00:00', 0),
(174, 0, 91, '2017-02-02 02:11:17', 0, '0000-00-00 00:00:00', 0),
(175, 0, 92, '2017-02-05 12:00:15', 0, '2017-02-05 12:00:15', 1),
(176, 0, 93, '2017-02-02 02:26:55', 0, '0000-00-00 00:00:00', 0),
(177, 204, 0, '2017-02-09 12:00:12', 0, '2017-02-09 12:00:12', 5),
(178, 0, 94, '2017-02-03 23:28:13', 0, '0000-00-00 00:00:00', 0),
(179, 0, 95, '2017-02-05 12:00:15', 1, '2017-02-05 12:00:15', 1),
(180, 205, 0, '2017-02-03 23:44:46', 0, '0000-00-00 00:00:00', 0),
(181, 206, 0, '2017-02-10 12:00:12', 3, '2017-02-10 12:00:12', 5),
(182, 0, 96, '2017-02-05 00:47:15', 0, '0000-00-00 00:00:00', 0),
(183, 0, 97, '2017-02-05 00:49:16', 0, '0000-00-00 00:00:00', 0),
(184, 207, 0, '2017-02-10 12:00:12', 5, '2017-02-10 12:00:12', 5),
(185, 208, 0, '2017-02-13 12:00:09', 4, '2017-02-13 12:00:09', 5),
(186, 209, 0, '2017-02-13 12:00:09', 4, '2017-02-13 12:00:09', 5),
(187, 210, 0, '2017-02-13 12:00:10', 5, '2017-02-13 12:00:10', 5),
(188, 0, 98, '2017-02-07 16:33:28', 0, '0000-00-00 00:00:00', 0),
(189, 211, 0, '2017-02-13 12:00:10', 5, '2017-02-13 12:00:10', 5),
(190, 212, 0, '2017-02-07 20:51:07', 0, '0000-00-00 00:00:00', 0),
(191, 0, 99, '2017-02-14 12:00:15', 5, '2017-02-14 12:00:15', 5),
(192, 0, 100, '2017-02-14 12:00:15', 5, '2017-02-14 12:00:15', 5),
(193, 0, 101, '2017-02-14 12:00:15', 5, '2017-02-14 12:00:15', 5),
(194, 213, 0, '2017-02-14 12:00:14', 5, '2017-02-14 12:00:14', 5),
(195, 0, 102, '2017-02-14 12:00:15', 5, '2017-02-14 12:00:15', 5),
(196, 0, 103, '2017-02-18 12:00:14', 1, '2017-02-18 12:00:14', 5),
(197, 0, 104, '2017-02-14 12:00:16', 5, '2017-02-14 12:00:16', 5),
(198, 0, 105, '2017-02-14 12:00:16', 5, '2017-02-14 12:00:16', 5),
(199, 0, 106, '2017-02-14 12:00:16', 5, '2017-02-14 12:00:16', 5),
(200, 0, 107, '2017-02-18 12:00:14', 5, '2017-02-18 12:00:14', 5),
(201, 214, 0, '2017-02-16 12:00:12', 5, '2017-02-16 12:00:12', 5),
(202, 215, 0, '2017-02-16 12:00:13', 5, '2017-02-16 12:00:13', 5),
(203, 0, 108, '2017-02-18 12:00:15', 5, '2017-02-18 12:00:15', 5),
(204, 0, 109, '2017-02-20 12:00:15', 5, '2017-02-20 12:00:15', 5),
(205, 216, 0, '2017-02-18 12:00:12', 5, '2017-02-18 12:00:12', 5),
(206, 0, 110, '2017-02-22 12:00:10', 4, '2017-02-22 12:00:10', 5),
(207, 217, 0, '2017-02-19 12:00:10', 5, '2017-02-19 12:00:10', 5),
(208, 218, 0, '2017-02-19 12:00:11', 5, '2017-02-19 12:00:11', 5),
(209, 219, 0, '2017-02-14 03:41:56', 0, '0000-00-00 00:00:00', 0),
(210, 220, 0, '2017-02-19 12:00:11', 5, '2017-02-19 12:00:11', 5),
(211, 221, 0, '2017-02-20 12:00:12', 5, '2017-02-16 12:00:14', 1),
(212, 222, 0, '2017-02-21 12:00:11', 5, '2017-02-21 12:00:11', 5),
(213, 223, 0, '2017-02-21 12:00:12', 5, '2017-02-21 12:00:12', 5),
(214, 224, 0, '2017-02-21 12:00:12', 5, '2017-02-21 12:00:12', 5),
(215, 225, 0, '2017-02-21 12:00:12', 5, '2017-02-21 12:00:12', 5),
(216, 226, 0, '2017-02-15 13:58:53', 0, '0000-00-00 00:00:00', 0),
(217, 0, 111, '2017-02-15 14:28:32', 0, '0000-00-00 00:00:00', 0),
(218, 227, 0, '2017-02-21 12:00:12', 5, '2017-02-21 12:00:12', 5),
(219, 228, 0, '2017-02-16 03:42:59', 0, '0000-00-00 00:00:00', 0),
(220, 229, 0, '2017-02-16 21:00:20', 0, '0000-00-00 00:00:00', 0),
(221, 0, 112, '2017-03-01 12:00:19', 5, '2017-03-01 12:00:19', 5),
(222, 230, 0, '2017-02-18 20:54:38', 0, '0000-00-00 00:00:00', 0),
(223, 231, 0, '2017-02-22 03:05:14', 0, '0000-00-00 00:00:00', 0),
(224, 0, 113, '2017-02-22 03:47:51', 0, '0000-00-00 00:00:00', 0),
(225, 232, 0, '2017-02-22 04:17:57', 0, '0000-00-00 00:00:00', 0),
(226, 0, 114, '2017-02-22 13:59:16', 0, '0000-00-00 00:00:00', 0),
(227, 0, 115, '2017-03-01 12:00:19', 5, '2017-03-01 12:00:19', 5),
(228, 233, 0, '2017-02-28 12:00:18', 5, '2017-02-28 12:00:18', 5),
(230, 235, 0, '2017-03-01 12:00:19', 5, '2017-03-01 12:00:19', 5),
(231, 236, 0, '2017-03-01 12:00:19', 5, '2017-03-01 12:00:19', 5),
(232, 0, 116, '2017-03-02 12:40:11', 0, '0000-00-00 00:00:00', 0),
(233, 237, 0, '2017-03-08 12:00:13', 5, '2017-03-08 12:00:13', 5),
(234, 238, 0, '2017-03-08 12:00:13', 5, '2017-03-08 12:00:13', 5),
(235, 239, 0, '2017-03-08 12:00:13', 5, '2017-03-08 12:00:13', 5),
(236, 0, 117, '2017-03-08 12:00:12', 5, '2017-03-05 12:00:11', 2),
(237, 0, 118, '2017-03-10 12:00:08', 5, '2017-03-10 12:00:08', 5),
(238, 240, 0, '2017-03-06 02:25:02', 0, '0000-00-00 00:00:00', 0),
(239, 241, 0, '2017-03-06 02:37:09', 0, '0000-00-00 00:00:00', 0),
(240, 0, 119, '2017-03-06 02:42:56', 0, '0000-00-00 00:00:00', 0),
(241, 242, 0, '2017-03-13 11:00:11', 5, '0000-00-00 00:00:00', 0),
(242, 243, 0, '2017-03-16 11:00:19', 5, '2017-03-16 11:00:19', 5),
(243, 0, 120, '2017-03-22 11:00:17', 5, '2017-03-22 11:00:17', 5),
(244, 244, 0, '2017-03-16 11:00:19', 5, '2017-03-16 11:00:19', 5),
(245, 0, 121, '2017-03-16 11:00:20', 5, '2017-03-16 11:00:20', 5),
(246, 0, 122, '2017-03-18 11:00:10', 5, '2017-03-18 11:00:10', 5),
(247, 245, 0, '2017-03-17 11:00:09', 5, '2017-03-17 11:00:09', 5),
(248, 0, 123, '2017-03-20 11:00:16', 5, '2017-03-20 11:00:16', 5),
(249, 246, 0, '2017-03-18 11:00:09', 5, '2017-03-18 11:00:09', 5),
(250, 247, 0, '2017-03-14 01:47:18', 0, '0000-00-00 00:00:00', 0),
(251, 0, 124, '2017-03-14 01:51:45', 0, '0000-00-00 00:00:00', 0),
(252, 248, 0, '2017-03-14 15:31:13', 0, '0000-00-00 00:00:00', 0),
(253, 0, 125, '2017-03-20 11:00:16', 5, '2017-03-20 11:00:16', 5),
(254, 249, 0, '2017-03-14 19:11:55', 0, '0000-00-00 00:00:00', 0),
(255, 250, 0, '2017-03-15 17:17:27', 0, '0000-00-00 00:00:00', 0),
(256, 0, 126, '2017-03-15 20:09:05', 0, '0000-00-00 00:00:00', 0),
(257, 251, 0, '2017-03-23 11:00:13', 5, '2017-03-23 11:00:13', 5),
(258, 0, 127, '2017-04-04 11:00:08', 5, '2017-04-04 11:00:08', 5),
(259, 252, 0, '2017-03-24 11:00:09', 5, '2017-03-24 11:00:09', 5),
(260, 0, 128, '2017-03-24 11:00:10', 5, '2017-03-24 11:00:10', 5),
(261, 253, 0, '2017-03-24 11:00:09', 5, '2017-03-24 11:00:09', 5),
(262, 0, 129, '2017-03-24 11:00:10', 5, '2017-03-24 11:00:10', 5),
(263, 254, 0, '2017-03-26 11:00:08', 5, '2017-03-26 11:00:08', 5),
(264, 255, 0, '2017-03-26 11:00:08', 5, '2017-03-26 11:00:08', 5),
(265, 0, 130, '2017-03-26 11:00:08', 5, '2017-03-26 11:00:08', 5),
(266, 256, 0, '2017-03-26 11:00:08', 5, '2017-03-26 11:00:08', 5),
(267, 257, 0, '2017-03-23 23:02:38', 0, '0000-00-00 00:00:00', 0),
(268, 0, 131, '2017-03-23 23:35:21', 0, '0000-00-00 00:00:00', 0),
(269, 0, 132, '2017-03-24 00:08:56', 0, '0000-00-00 00:00:00', 0),
(270, 0, 133, '2017-03-24 00:19:55', 0, '0000-00-00 00:00:00', 0),
(271, 258, 0, '2017-03-30 11:00:06', 5, '2017-03-30 11:00:06', 5),
(272, 259, 0, '2017-04-02 11:00:06', 5, '2017-04-02 11:00:06', 5),
(273, 260, 0, '2017-04-11 11:00:09', 5, '2017-04-11 11:00:09', 5),
(274, 0, 134, '2017-04-12 11:00:08', 5, '2017-04-12 11:00:08', 5),
(275, 261, 0, '2017-04-11 11:00:09', 5, '2017-04-11 11:00:09', 5),
(276, 262, 0, '2017-04-11 11:00:09', 5, '2017-04-11 11:00:09', 5),
(277, 263, 0, '2017-04-13 11:00:07', 5, '2017-04-13 11:00:07', 5),
(278, 264, 0, '2017-04-13 11:00:08', 5, '2017-04-13 11:00:08', 5),
(279, 265, 0, '2017-04-14 11:00:07', 5, '2017-04-14 11:00:07', 5),
(280, 266, 0, '2017-04-25 11:00:07', 5, '2017-04-25 11:00:07', 5),
(281, 267, 0, '2017-05-04 11:00:07', 5, '2017-05-04 11:00:07', 5),
(282, 268, 0, '2017-04-28 15:38:39', 0, '0000-00-00 00:00:00', 0),
(283, 269, 0, '2017-05-10 11:00:07', 5, '2017-05-10 11:00:07', 5),
(284, 0, 135, '2017-05-10 11:00:07', 5, '2017-05-10 11:00:07', 5),
(285, 270, 0, '2017-05-10 13:54:05', 0, '2017-05-10 13:54:05', 5),
(286, 271, 0, '2017-05-11 11:00:15', 5, '2017-05-11 11:00:15', 5),
(287, 272, 0, '2017-05-15 11:00:08', 5, '2017-05-15 11:00:08', 5),
(288, 273, 0, '2017-05-15 11:00:09', 5, '2017-05-15 11:00:09', 5),
(289, 274, 0, '2017-05-16 11:00:09', 5, '2017-05-16 11:00:09', 5),
(290, 0, 136, '2017-05-16 11:00:09', 5, '2017-05-16 11:00:09', 5),
(291, 275, 0, '2017-05-17 11:00:07', 5, '2017-05-17 11:00:07', 5),
(292, 0, 137, '2017-05-12 11:49:59', 0, '0000-00-00 00:00:00', 0),
(293, 276, 0, '2017-05-18 11:00:08', 5, '2017-05-18 11:00:08', 5),
(294, 277, 0, '2017-05-21 11:00:08', 5, '2017-05-21 11:00:08', 5),
(296, 0, 139, '2017-05-22 11:00:10', 5, '2017-05-22 11:00:10', 5),
(300, 281, 0, '2017-05-16 14:52:10', 0, '0000-00-00 00:00:00', 0),
(301, 0, 140, '2017-05-22 11:00:11', 5, '2017-05-22 11:00:11', 5),
(302, 282, 0, '2017-05-16 17:10:37', 0, '0000-00-00 00:00:00', 0),
(303, 283, 0, '2017-05-16 17:57:14', 0, '0000-00-00 00:00:00', 0),
(304, 284, 0, '2017-05-23 11:00:12', 0, '2017-05-23 11:00:12', 5),
(305, 285, 0, '2017-05-23 11:00:12', 0, '2017-05-23 11:00:12', 5),
(306, 286, 0, '2017-05-23 11:00:12', 5, '2017-05-23 11:00:12', 5),
(307, 287, 0, '2017-05-23 11:00:12', 5, '2017-05-23 11:00:12', 5),
(308, 0, 141, '2017-05-23 11:00:14', 5, '2017-05-23 11:00:14', 5),
(309, 0, 142, '2017-05-23 11:00:14', 5, '2017-05-23 11:00:14', 5),
(310, 288, 0, '2017-05-24 11:00:07', 5, '2017-05-24 11:00:07', 5),
(311, 289, 0, '2017-05-24 11:00:07', 5, '2017-05-24 11:00:07', 5),
(312, 0, 143, '2017-05-24 11:00:07', 5, '2017-05-24 11:00:07', 5),
(313, 290, 0, '2017-05-24 11:00:07', 5, '2017-05-24 11:00:07', 5),
(314, 0, 144, '2017-05-24 11:00:08', 5, '2017-05-24 11:00:08', 5),
(315, 291, 0, '2017-05-24 11:00:07', 5, '2017-05-24 11:00:07', 5),
(320, 0, 145, '2017-05-29 11:00:09', 5, '2017-05-29 11:00:09', 5),
(321, 0, 146, '2017-05-30 11:00:10', 5, '2017-05-30 11:00:10', 5),
(322, 292, 0, '2017-05-30 11:00:08', 5, '2017-05-30 11:00:08', 5),
(323, 293, 0, '2017-05-24 18:15:44', 0, '0000-00-00 00:00:00', 0),
(324, 0, 147, '2017-05-30 11:00:10', 5, '2017-05-30 11:00:10', 5),
(325, 294, 0, '2017-05-31 11:00:09', 5, '2017-05-31 11:00:09', 5),
(326, 295, 0, '2017-06-01 11:00:08', 5, '2017-06-01 11:00:08', 5),
(327, 0, 148, '2017-06-01 11:00:10', 5, '2017-06-01 11:00:10', 5),
(328, 296, 0, '2017-06-01 11:00:08', 5, '2017-06-01 11:00:08', 5),
(329, 297, 0, '2017-06-01 11:00:09', 5, '2017-06-01 11:00:09', 5),
(330, 298, 0, '2017-06-01 11:00:09', 5, '2017-06-01 11:00:09', 5),
(331, 0, 149, '2017-06-01 11:00:11', 5, '2017-06-01 11:00:11', 5),
(332, 299, 0, '2017-06-02 11:00:09', 5, '2017-06-02 11:00:09', 5),
(333, 300, 0, '2017-06-02 11:00:09', 5, '2017-06-02 11:00:09', 5),
(334, 0, 150, '2017-06-03 11:00:08', 5, '2017-06-03 11:00:08', 5),
(335, 0, 151, '2017-06-02 11:00:10', 5, '2017-06-02 11:00:10', 5),
(336, 301, 0, '2017-05-30 12:49:45', 0, '0000-00-00 00:00:00', 0),
(337, 302, 0, '2017-06-05 11:00:07', 5, '2017-06-05 11:00:07', 5),
(338, 303, 0, '2017-06-05 11:00:07', 5, '2017-06-05 11:00:07', 5),
(339, 0, 152, '2017-06-05 11:00:07', 5, '2017-06-05 11:00:07', 5),
(340, 0, 153, '2017-06-07 11:00:10', 5, '2017-06-07 11:00:10', 5),
(341, 304, 0, '2017-06-02 15:28:32', 0, '0000-00-00 00:00:00', 0),
(342, 305, 0, '2017-06-08 11:00:09', 0, '2017-06-08 11:00:09', 5),
(343, 306, 0, '2017-06-08 11:00:09', 5, '2017-06-08 11:00:09', 5),
(344, 0, 154, '2017-06-08 11:00:09', 2, '2017-06-08 11:00:09', 5),
(345, 0, 155, '2017-06-08 11:00:09', 5, '2017-06-08 11:00:09', 5),
(346, 307, 0, '2017-06-07 11:00:10', 2, '2017-06-07 11:00:10', 4),
(347, 308, 0, '2017-06-04 16:25:07', 0, '0000-00-00 00:00:00', 0),
(348, 309, 0, '2017-06-09 11:00:05', 3, '2017-06-07 11:00:10', 1),
(349, 0, 156, '2017-06-09 11:00:05', 3, '2017-06-07 11:00:11', 1),
(350, 310, 0, '2017-06-05 23:31:33', 0, '0000-00-00 00:00:00', 0),
(351, 311, 0, '2017-06-09 11:00:05', 3, '2017-06-07 11:00:10', 1),
(352, 312, 0, '2017-06-11 11:00:08', 4, '2017-06-11 11:00:08', 5),
(353, 0, 157, '2017-06-12 11:00:06', 2, '2017-06-12 11:00:06', 2),
(354, 313, 0, '2017-06-24 11:00:05', 1, '2017-06-24 11:00:05', 1),
(355, 0, 158, '2017-06-29 11:00:05', 2, '2017-06-29 11:00:05', 5),
(356, 314, 0, '2017-06-26 11:00:05', 1, '2017-06-26 11:00:05', 1),
(357, 0, 159, '2017-06-29 11:00:05', 3, '2017-06-29 11:00:05', 3),
(358, 0, 160, '2017-07-01 11:00:06', 1, '2017-07-01 11:00:06', 1),
(359, 315, 0, '2017-07-12 19:36:55', 0, '0000-00-00 00:00:00', 0),
(360, 316, 0, '2017-07-16 11:00:06', 3, '2017-07-14 11:00:07', 1),
(361, 317, 0, '2017-07-16 11:00:06', 3, '2017-07-14 11:00:07', 1),
(362, 318, 0, '2017-07-16 11:00:06', 2, '0000-00-00 00:00:00', 0),
(368, 319, 0, '2017-07-17 11:00:06', 1, '2017-07-17 11:00:06', 1),
(370, 320, 0, '2017-07-17 11:00:06', 1, '2017-07-17 11:00:06', 1),
(371, 321, 0, '2017-07-18 11:00:06', 2, '2017-07-17 11:00:07', 1),
(373, 0, 161, '2017-07-17 11:00:07', 1, '2017-07-17 11:00:07', 1),
(374, 0, 162, '2017-08-01 11:00:05', 5, '0000-00-00 00:00:00', 0),
(375, 0, 163, '2017-07-31 11:00:10', 5, '0000-00-00 00:00:00', 0),
(376, 322, 0, '2017-07-23 11:00:07', 5, '0000-00-00 00:00:00', 0),
(377, 323, 0, '2017-07-23 11:00:07', 5, '0000-00-00 00:00:00', 0),
(378, 324, 0, '2017-07-20 11:00:08', 1, '2017-07-20 11:00:08', 2),
(379, 325, 0, '2017-07-23 11:00:08', 1, '2017-07-23 11:00:08', 5),
(380, 326, 0, '2017-07-24 11:00:07', 3, '2017-07-24 11:00:07', 5),
(381, 327, 0, '2017-07-25 11:00:07', 5, '2017-07-25 11:00:07', 5),
(382, 328, 0, '2017-07-22 11:00:09', 2, '2017-07-22 11:00:09', 2),
(383, 329, 0, '2017-07-28 11:00:06', 5, '2017-07-27 11:00:07', 4),
(384, 330, 0, '2017-07-28 11:00:07', 1, '2017-07-28 11:00:07', 5),
(385, 331, 0, '2017-07-31 11:00:09', 5, '2017-07-27 11:00:07', 1),
(386, 332, 0, '2017-07-28 11:00:07', 1, '2017-07-28 11:00:07', 1),
(387, 0, 164, '2017-08-01 11:00:05', 5, '2017-08-01 11:00:05', 5),
(388, 333, 0, '2017-08-04 11:00:06', 5, '2017-08-04 11:00:06', 5),
(389, 0, 165, '2017-08-02 11:00:10', 0, '2017-08-02 11:00:10', 2),
(390, 0, 166, '2017-07-31 16:38:54', 0, '0000-00-00 00:00:00', 0),
(391, 334, 0, '2017-08-06 11:00:07', 5, '2017-08-06 11:00:07', 5),
(392, 335, 0, '2017-08-01 23:12:09', 0, '0000-00-00 00:00:00', 0),
(393, 336, 0, '2017-08-08 11:00:07', 5, '2017-08-08 11:00:07', 5),
(394, 0, 167, '2017-08-10 11:00:07', 5, '2017-08-10 11:00:07', 5),
(395, 337, 0, '2017-08-03 03:07:25', 0, '0000-00-00 00:00:00', 0),
(396, 338, 0, '2017-08-09 11:00:07', 5, '2017-08-09 11:00:07', 5),
(397, 339, 0, '2017-08-05 11:00:10', 1, '2017-08-05 11:00:10', 1),
(398, 0, 168, '2017-08-09 11:00:10', 3, '2017-08-09 11:00:09', 3),
(399, 340, 0, '2017-08-10 11:00:07', 4, '2017-08-10 11:00:07', 4),
(400, 341, 0, '2017-08-05 16:04:06', 0, '0000-00-00 00:00:00', 0),
(401, 342, 0, '2017-08-12 11:00:06', 5, '2017-08-11 11:00:07', 4),
(402, 343, 0, '2017-08-13 11:00:07', 5, '2017-08-13 11:00:07', 5),
(403, 344, 0, '2017-08-07 22:10:10', 0, '0000-00-00 00:00:00', 0),
(404, 345, 0, '2017-08-14 11:00:06', 5, '2017-08-11 11:00:07', 2),
(405, 0, 169, '2017-08-10 14:22:31', 0, '0000-00-00 00:00:00', 0),
(406, 346, 0, '2017-08-11 13:49:17', 0, '0000-00-00 00:00:00', 0),
(407, 347, 0, '2017-08-11 14:22:06', 0, '0000-00-00 00:00:00', 0),
(408, 0, 170, '2017-08-11 14:41:09', 0, '0000-00-00 00:00:00', 0),
(409, 0, 171, '2017-08-15 11:00:07', 1, '2017-08-15 11:00:07', 1),
(410, 0, 172, '2017-08-11 15:45:03', 0, '0000-00-00 00:00:00', 0),
(411, 0, 173, '2017-08-15 11:00:07', 3, '2017-08-15 11:00:07', 3),
(412, 348, 0, '2017-08-15 01:23:58', 0, '0000-00-00 00:00:00', 0),
(413, 349, 0, '2017-08-15 01:26:17', 0, '0000-00-00 00:00:00', 0),
(414, 0, 174, '2017-08-15 22:05:43', 0, '0000-00-00 00:00:00', 0),
(415, 0, 175, '2017-08-16 00:23:03', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reset`
--

CREATE TABLE IF NOT EXISTS `reset` (
  `reset_id` int(10) unsigned NOT NULL,
  `reset_usuario_id` int(10) unsigned NOT NULL,
  `reset_usuario_nombre` varchar(50) NOT NULL,
  `reset_usuario_tipo` varchar(10) NOT NULL,
  `reset_token` varchar(64) NOT NULL,
  `reset_creado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `reset`
--

INSERT INTO `reset` (`reset_id`, `reset_usuario_id`, `reset_usuario_nombre`, `reset_usuario_tipo`, `reset_token`, `reset_creado`) VALUES
(5, 14, 'Sebas Usuario', 'cliente', '62875d817473d9393fa41f4994c0a842ad83a625', '2016-09-19 22:27:58'),
(8, 28, 'christian zanabria', 'cliente', '29ba6594635a037eb055d910859f64b797cfc73b', '2016-12-15 21:27:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rubro`
--

CREATE TABLE IF NOT EXISTS `rubro` (
  `rubro_id` int(11) NOT NULL,
  `rubro_nombre` varchar(200) NOT NULL,
  `rubro_conraza` tinyint(4) NOT NULL DEFAULT '0',
  `rubro_contamanios` tinyint(4) NOT NULL DEFAULT '0',
  `rubro_conpresentacion` tinyint(4) NOT NULL DEFAULT '0',
  `rubro_conmarca` tinyint(4) NOT NULL DEFAULT '0',
  `rubro_conedad` tinyint(4) NOT NULL DEFAULT '0',
  `rubro_conmedicados` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rubro`
--

INSERT INTO `rubro` (`rubro_id`, `rubro_nombre`, `rubro_conraza`, `rubro_contamanios`, `rubro_conpresentacion`, `rubro_conmarca`, `rubro_conedad`, `rubro_conmedicados`) VALUES
(5, 'Alimentos', 1, 1, 1, 1, 1, 1),
(6, 'Accesorios', 0, 0, 0, 0, 0, 0),
(7, 'Farmacia', 0, 0, 1, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE IF NOT EXISTS `servicio` (
  `servicio_id` int(11) NOT NULL,
  `servicio_nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`servicio_id`, `servicio_nombre`) VALUES
(6, 'Peluquería'),
(7, 'Clinica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio_item`
--

CREATE TABLE IF NOT EXISTS `servicio_item` (
  `servicio_item_id` int(11) NOT NULL,
  `servicio_item_nombre` varchar(255) DEFAULT NULL,
  `servicio_item_detalle` text NOT NULL,
  `servicio_item_servicio_id` int(11) DEFAULT NULL,
  `servicio_item_animal_id` int(11) NOT NULL,
  `servicio_item_imagen` varchar(255) NOT NULL DEFAULT 'noimage.gif'
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicio_item`
--

INSERT INTO `servicio_item` (`servicio_item_id`, `servicio_item_nombre`, `servicio_item_detalle`, `servicio_item_servicio_id`, `servicio_item_animal_id`, `servicio_item_imagen`) VALUES
(29, 'Baño raza pequeña', '', 6, 1, '_479565139.png'),
(30, 'Baño raza pequeña (Pelo largo)', '', 6, 1, '_714944349.png'),
(31, 'Baño raza mediana', '', 6, 1, '_620306389.png'),
(32, 'Baño raza mediana (Pelo largo)', '', 6, 1, '_963849528.png'),
(33, 'Baño raza grande', '', 6, 1, '_149352632.png'),
(34, 'Baño raza grande (Pelo largo)', '', 6, 1, '_7892407.png'),
(35, 'Baño raza gigante', '', 6, 1, '_266743832.png'),
(36, 'Baño raza gigante (Pelo largo)', '', 6, 1, '_6494244.png'),
(37, 'Corte raza pequeña', '', 6, 1, '_275657924.png'),
(38, 'Corte raza pequeña (Pelo largo)', '', 6, 1, '_657540296.png'),
(39, 'Corte raza mediana', '', 6, 1, '_897829805.png'),
(40, 'Corte raza mediana (Pelo largo)', '', 6, 1, '_368879304.png'),
(41, 'Corte raza grande', '', 6, 1, '_803456380.png'),
(42, 'Corte raza grande (Pelo largo)', '', 6, 1, '_920491221.png'),
(43, 'Corte raza gigante', '', 6, 1, '_399654563.png'),
(44, 'Corte raza gigante (Pelo largo)', '', 6, 1, '_6763812.png'),
(45, 'Baño y Corte raza pequeña', '', 6, 1, '_336335764.png'),
(46, 'Baño y Corte raza pequeña (Pelo largo)', '', 6, 1, '_685932964.png'),
(47, 'Baño y Corte raza mediana', '', 6, 1, '_932566896.png'),
(48, 'Baño y Corte raza mediana (Pelo largo)', '', 6, 1, '_960961639.png'),
(49, 'Baño y Corte raza grande', '', 6, 1, '_290770622.png'),
(50, 'Baño y Corte raza grande (Pelo largo)', '', 6, 1, '_190292364.png'),
(51, 'Baño y Corte raza gigante', '', 6, 1, '_655449430.png'),
(52, 'Baño y Corte raza gigante (Pelo largo)', '', 6, 1, '_90752137.png'),
(53, 'Baño felino', '', 6, 2, '_622946612.png'),
(54, 'Baño felino (Pelo largo)', '', 6, 2, '_526370631.png'),
(55, 'Corte felino (Pelo largo)', '', 6, 2, '_392747811.png'),
(56, 'Baño y Corte felino (Pelo largo)', '', 6, 2, '_598758952.png'),
(57, 'Corte de uñas (Perro)', '', 6, 1, '_71597822.png'),
(58, 'Corte de uñas (Gato)', '', 6, 2, '_758009825.png'),
(59, 'Corte higiénico (Perro)', '', 6, 1, '_607255167.png'),
(60, 'Corte higiénico (Gato)', '', 6, 2, '_568325329.png'),
(61, 'Consulta clínica (Perro)', '', 7, 1, '_839684964.png'),
(62, 'Consulta clínica (Gato)', '', 7, 2, '_761347650.png'),
(63, 'Vacuna quintuple (Perro)', '', 7, 1, '_148074453.png'),
(64, 'Vacuna sextuple (Perro)', '', 7, 1, '_77584840.png'),
(65, 'Vacuna antirrábica (Perro)', '', 7, 1, '_806805300.png'),
(66, 'Vacuna tos de la perrera (Perro)', '', 7, 1, '_624217285.png'),
(67, 'Vacuna quintuple + antirrábica (Perro)', '', 7, 1, '_100219232.png'),
(68, 'Vacuna sextuple + antirrábica (Perro)', '', 7, 1, '_285320207.png'),
(71, 'Vacuna antirrábica (Gato)', '', 7, 2, '_584175914.png'),
(72, 'Vacuna triple (Gato)', '', 7, 2, '_100219232.png'),
(73, 'Vacuna triple + antirrábica (Gato)', '', 7, 2, '_141461749.png'),
(74, 'Colocación de Microchip (Perro)', '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">El microchip es una c&aacute;psula del tama&ntilde;o de un grano de arroz de inyecci&oacute;n subcut&aacute;nea (el microchip se inyecta bajo la piel del perro).<br />\r\nEl chip permanece bajo la piel del perro y no es posible quitarlo ni alterarlo. Los veterinarios tienen un esc&aacute;ner para leer el contenido almacenado en el interior de la c&aacute;psula del chip para poder leer los n&uacute;meros alojados en ella. Bobby, un perro perdido con microchip, si es llevado a un veterinario se puede obtener r&aacute;pidamente los datos del propietario. El chip certifica los tratamientos recibidos contra garrapatas y par&aacute;sitos intestinales. Los chips son expedidos en ingl&eacute;s y en el idioma oficial del pa&iacute;s del animal.</span></span><br />\r\n&nbsp;</p>\r\n', 7, 1, '_807349746.png'),
(75, 'Colocación de Microchip (Gato)', '<p><span style="color:#FF0000"><span style="font-size:16px"><strong>Descripcion:</strong></span></span><br />\r\n<span style="color:#000000"><span style="font-size:16px">El microchip es una c&aacute;psula del tama&ntilde;o de un grano de arroz de inyecci&oacute;n subcut&aacute;nea (el microchip se inyecta bajo la piel del gato).<br />\r\nEl chip permanece bajo la piel del gato y no es posible quitarlo ni alterarlo. Los veterinarios tienen un esc&aacute;ner para leer el contenido almacenado en el interior de la c&aacute;psula del chip para poder leer los n&uacute;meros alojados en ella. Manuel, un gato perdido con microchip, si es llevado a un veterinario se puede obtener r&aacute;pidamente los datos del propietario. El chip certifica los tratamientos recibidos contra pulgas&nbsp;y par&aacute;sitos intestinales. Los chips son expedidos en ingl&eacute;s y en el idioma oficial del pa&iacute;s del animal.</span></span></p>\r\n', 7, 2, '_636845605.png'),
(76, 'Limpieza de oído (Perro)', '', 6, 1, '_786226326.png'),
(77, 'Limpieza de oído (Gato)', '', 6, 2, '_654024903.png'),
(78, 'Limpieza de lagrimales (Perro)', '', 6, 1, '_349318153.png'),
(79, 'Limpieza de lagrimales (Gato)', '', 6, 2, '_839559548.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tamanio`
--

CREATE TABLE IF NOT EXISTS `tamanio` (
  `tamanio_id` int(11) NOT NULL,
  `tamanio_nombre` varchar(255) DEFAULT NULL,
  `tamanio_animal_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tamanio`
--

INSERT INTO `tamanio` (`tamanio_id`, `tamanio_nombre`, `tamanio_animal_id`) VALUES
(3, 'Medianos', 1),
(4, 'Grandes', 1),
(17, 'Pequeños', 1),
(21, 'Todos los Tamaños', 1),
(22, 'Gigantes', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno`
--

CREATE TABLE IF NOT EXISTS `turno` (
  `turno_id` int(11) NOT NULL,
  `turno_local_id` int(11) NOT NULL,
  `turno_cliente_id` int(11) NOT NULL,
  `turno_cliente_telefono` varchar(255) NOT NULL,
  `turno_servicio_item_id` int(11) NOT NULL,
  `turno_fecha` date NOT NULL,
  `turno_hora` char(5) NOT NULL,
  `turno_mes` varchar(6) NOT NULL,
  `turno_estado` varchar(13) NOT NULL DEFAULT 'reservado',
  `turno_observaciones` text NOT NULL,
  `turno_fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `turno_notificado_local` tinyint(4) NOT NULL,
  `turno_notificado_cliente` tinyint(4) NOT NULL,
  `turno_precio` float NOT NULL,
  `turno_concretado_cliente` varchar(2) NOT NULL,
  `turno_puntaje_cliente` int(11) DEFAULT NULL,
  `turno_valoracion_cliente` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `turno_valoracion_aprobada` varchar(2) NOT NULL,
  `turno_concretado_local` varchar(2) NOT NULL,
  `turno_valoracion_local` varchar(200) NOT NULL,
  `turno_cancelado_por` varchar(7) NOT NULL COMMENT 'Quien cancelo el turno: cliente o local',
  `turno_cancelado_comentario` varchar(200) NOT NULL COMMENT 'Motivo por el cual se cancela el turno'
) ENGINE=InnoDB AUTO_INCREMENT=176 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno`
--

INSERT INTO `turno` (`turno_id`, `turno_local_id`, `turno_cliente_id`, `turno_cliente_telefono`, `turno_servicio_item_id`, `turno_fecha`, `turno_hora`, `turno_mes`, `turno_estado`, `turno_observaciones`, `turno_fecha_creacion`, `turno_notificado_local`, `turno_notificado_cliente`, `turno_precio`, `turno_concretado_cliente`, `turno_puntaje_cliente`, `turno_valoracion_cliente`, `turno_valoracion_aprobada`, `turno_concretado_local`, `turno_valoracion_local`, `turno_cancelado_por`, `turno_cancelado_comentario`) VALUES
(161, 25, 14, '4444-4444', 61, '2017-07-16', '10:00', '201707', 'no concretado', '', '2017-07-15 23:19:33', 0, 0, 200, 'no', 3, 'No concretado lado usuario', 'si', 'no', 'NO CONCRETADO LADO LOCAL', '', ''),
(162, 25, 43, '123', 61, '2017-07-27', '9:15', '201707', 'concretado', '', '2017-07-17 23:29:13', 0, 0, 200, '', NULL, '', '', 'si', 'test local notifica', '', ''),
(163, 25, 43, '123', 64, '2017-07-26', '11:30', '201707', 'no concretado', '', '2017-07-17 23:29:59', 0, 0, 400, '', NULL, '', '', 'no', 'test notifica no concretado', '', ''),
(164, 25, 14, '4444-4444', 61, '2017-07-27', '9:00', '201707', 'concretado', '', '2017-07-27 02:47:04', 0, 0, 200, '', NULL, '', '', '', '', '', ''),
(165, 39, 14, '4444-4444', 33, '2017-07-31', '9:00', '201707', 'concretado', '', '2017-07-31 00:48:27', 0, 0, 200, 'si', 5, 'Muy amables :)', 'si', 'si', 'Todo OK', '', ''),
(166, 39, 14, '4444-4444', 75, '2017-08-04', '9:00', '201708', 'cancelado', '', '2017-07-31 18:38:54', 0, 0, 200, '', NULL, '', '', '', '', 'cliente', 'No voy a poder ir :('),
(167, 39, 14, '4444-4444', 65, '2017-08-05', '12:45', '201708', 'concretado', '', '2017-08-02 22:05:23', 0, 0, 400, '', NULL, '', '', '', '', '', ''),
(168, 25, 13, '15-1234-5678', 31, '2017-08-06', '11:00', '201708', 'concretado', '', '2017-08-03 18:00:09', 0, 0, 400, 'si', 4, 'concretado lado cliente', 'si', 'si', 'Concretado lado LOCAL', '', ''),
(169, 25, 43, '123', 61, '2017-08-18', '10:00', '201708', 'cancelado', '', '2017-08-10 16:22:31', 0, 0, 200, '', NULL, '', '', '', '', 'cliente', 'Test cancela turno'),
(171, 25, 13, '15-1234-5678', 64, '2017-08-14', '15:15', '201708', 'no concretado', '', '2017-08-11 17:30:12', 0, 0, 400, 'no', 1, 'NO Concretado lado comprador', '', 'no', 'NO CONCRETADO LADO LOCAL', '', ''),
(173, 25, 13, '15-1234-5678', 61, '2017-08-12', '10:00', '201708', 'concretado', '', '2017-08-11 19:02:33', 0, 0, 200, 'si', 4, 'Concreto comprador', '', 'si', 'Concretado lado Local', '', ''),
(175, 25, 13, '15-1234-5678', 72, '2017-08-18', '17:00', '201708', 'reservado', '', '2017-08-16 02:23:03', 0, 0, 350, '', NULL, '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `turno_comentario`
--

CREATE TABLE IF NOT EXISTS `turno_comentario` (
  `turno_comentario_id` int(11) NOT NULL,
  `turno_comentario_turno_id` int(11) NOT NULL,
  `turno_comentario_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `turno_comentario_local_id` int(11) NOT NULL,
  `turno_comentario_cliente_id` int(11) NOT NULL,
  `turno_comentario_comentario` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `turno_comentario`
--

INSERT INTO `turno_comentario` (`turno_comentario_id`, `turno_comentario_turno_id`, `turno_comentario_fecha`, `turno_comentario_local_id`, `turno_comentario_cliente_id`, `turno_comentario_comentario`) VALUES
(1, 116, '2017-03-02 15:40:11', 0, 43, 'Test turnos cada 45 minutos'),
(2, 116, '2017-03-02 15:40:33', 0, 43, 'Nuevo mensaje al local'),
(3, 116, '2017-03-02 15:40:53', 23, 0, 'Respuesta del local'),
(4, 117, '2017-03-03 02:12:32', 0, 14, 'test'),
(5, 118, '2017-03-03 15:58:01', 0, 13, '3/3/2017'),
(6, 118, '2017-03-04 01:07:23', 23, 0, 'OK... '),
(7, 119, '2017-03-06 05:42:56', 0, 14, 'test'),
(8, 120, '2017-03-10 15:05:24', 23, 0, 'Test mensaje en turno (local al cliente)'),
(9, 120, '2017-03-10 15:05:38', 0, 43, 'Respuesta del cliente'),
(10, 120, '2017-03-10 15:06:43', 0, 43, 'Mensaje 2 del cliente'),
(11, 121, '2017-03-10 22:24:02', 0, 13, 'Prueba intercambio de mensajes lado COMPRADOR'),
(12, 121, '2017-03-10 22:34:43', 23, 0, 'Prueba intercambio de mensajes lado LOCAL'),
(13, 121, '2017-03-10 22:43:49', 0, 13, 'Prueba intercambio de mensajes lado COMPRADOR (devuelve el mensaje al local)'),
(14, 121, '2017-03-10 22:52:26', 23, 0, 'Prueba intercambio de mensajes lado LOCAL (devuelve el mensaje al comprador)'),
(15, 122, '2017-03-11 01:36:30', 0, 13, 'Prueba android'),
(16, 123, '2017-03-11 19:40:26', 0, 43, 'Mensaje al local al reservar turno'),
(17, 125, '2017-03-14 17:47:54', 0, 13, 'prueba 14/03/2017'),
(18, 126, '2017-03-15 22:09:05', 0, 13, 'prueba 15/3/2017'),
(19, 128, '2017-03-18 18:52:04', 0, 13, 'prueba 18/03/2017'),
(20, 129, '2017-03-18 20:34:34', 0, 14, 'test6'),
(21, 130, '2017-03-21 01:05:38', 0, 14, 'test notificacion'),
(22, 131, '2017-03-24 01:35:21', 0, 13, 'prueba 23/03/2017 a las 20:35'),
(23, 131, '2017-03-24 01:40:25', 23, 0, 'Prueba (Mensaje local) 20:40'),
(24, 131, '2017-03-24 01:42:50', 0, 13, 'Prueba (Mensaje comprador) 20:42'),
(25, 132, '2017-03-24 02:08:55', 0, 13, 'Prueba CANCELAR'),
(26, 132, '2017-03-24 02:10:39', 23, 0, 'Prueba (Local)'),
(27, 133, '2017-03-24 02:19:55', 0, 13, 'Prueba NO concretado'),
(28, 134, '2017-04-05 18:40:00', 0, 13, 'prueba'),
(29, 135, '2017-05-05 01:58:39', 0, 14, 'test celu'),
(30, 136, '2017-05-10 15:35:47', 0, 13, 'prueba mails'),
(31, 137, '2017-05-12 13:49:59', 0, 43, 'test'),
(32, 137, '2017-05-12 13:50:41', 0, 43, 'test mensaje'),
(34, 139, '2017-05-16 16:23:30', 0, 14, 'probando responsive'),
(35, 140, '2017-05-16 16:54:18', 0, 28, 'Prueba móvil 11:53'),
(36, 141, '2017-05-18 04:01:53', 0, 14, 'responsive 2'),
(37, 142, '2017-05-18 04:09:32', 0, 13, 'prueba 17/5/2017'),
(38, 143, '2017-05-18 23:42:34', 0, 13, 'Prueba movil'),
(39, 144, '2017-05-19 00:17:14', 0, 13, 'Prueba desde tablet'),
(40, 145, '2017-05-24 00:47:21', 0, 14, 'test responsive'),
(41, 146, '2017-05-24 19:51:16', 0, 28, 'Prueba 24/5-14:50hs'),
(42, 147, '2017-05-25 01:22:11', 0, 49, 'Prueba Turnos 24/5/2017'),
(43, 148, '2017-05-26 19:04:12', 0, 14, 'test 2'),
(44, 149, '2017-05-27 02:02:42', 0, 13, 'test tel'),
(45, 150, '2017-05-27 16:14:38', 0, 13, 'Prueba tablet 27/5/17 a las 11:14'),
(46, 150, '2017-05-27 16:19:47', 32, 0, 'prueba 27/5/17 a las 11:19'),
(47, 150, '2017-05-27 16:27:48', 0, 13, 'Prueba tablet.... A las 11:27\r\n'),
(48, 151, '2017-05-27 16:33:22', 0, 13, 'Prueba desde celular 27/5/17 a las 11:33'),
(49, 151, '2017-05-27 16:53:20', 25, 0, 'Respuesta desde tablet a las 12:05'),
(50, 151, '2017-05-27 16:55:05', 0, 13, 'Ok'),
(51, 152, '2017-05-30 17:02:11', 0, 13, '30/5/17 a las 12:02'),
(52, 154, '2017-06-03 00:55:00', 0, 13, 'Prueba 2/6/17'),
(53, 155, '2017-06-03 00:55:28', 0, 14, 'TEST TURNO ESTADOS'),
(54, 156, '2017-06-06 01:30:17', 0, 14, 'Tengan cuidado con las uñitas de mi perrtio!'),
(55, 156, '2017-06-06 01:37:32', 34, 0, 'Hola Sebastián.\r\nSu turno ha sido reservado.\r\nLo esperamos el martes 6 de junio a las 10h15 con su mascota.\r\nSaludos,\r\nLa Tienda de May'),
(56, 156, '2017-06-06 01:42:49', 0, 14, 'OK!'),
(57, 157, '2017-06-09 21:52:26', 0, 13, 'Prueba (que aparezca el teléfono)'),
(58, 157, '2017-06-10 10:58:33', 33, 0, 'Traemela y La revisamos para ver que tiene Anka \r\n'),
(59, 158, '2017-06-24 01:25:11', 0, 13, 'Prueba 23/06/2017'),
(60, 159, '2017-06-24 18:44:55', 0, 13, 'Prueba 24/6/2017'),
(61, 160, '2017-06-29 15:57:22', 0, 13, 'si concreta el cliente... del lado de la tienda el estado debe pasar a CONCRETADO'),
(62, 161, '2017-07-15 23:19:33', 0, 14, 'test'),
(63, 162, '2017-07-17 23:29:13', 0, 43, 'test notificar'),
(64, 163, '2017-07-17 23:29:59', 0, 43, 'test notificar 2'),
(65, 165, '2017-07-31 00:48:27', 0, 14, 'Mi perro es un poco nervioso, necesitan que me quede con ustedes mientras lo bañan?'),
(66, 165, '2017-07-31 00:59:13', 39, 0, 'No, no hay problema! \r\nTe esperamos!'),
(67, 165, '2017-07-31 01:03:32', 0, 14, 'Genial! aprovecho para preguntarles...necesitaría comprarle alimento al perro. Ustedes por casualidad venden? Porque no veo que tengan publicados articulos. Gracias!'),
(68, 166, '2017-07-31 18:38:54', 0, 14, 'Quiero ponerle microchip para que no se me pierda otra vez! Preg: qué medios de pago aceptan? Por que no me figura ninguno. Gracias!'),
(69, 167, '2017-08-02 22:05:23', 0, 14, 'Cuando van a vender alimentos para perros??'),
(70, 168, '2017-08-03 18:00:09', 0, 13, 'Hola que tal!!! te hago una pregunta... puedo dejar a mi perro o tengo que esperar a que lo terminen de bañar??? Gracias.'),
(71, 168, '2017-08-03 18:14:15', 25, 0, 'Hola!!! buenas tardes... como quieras si queres podes dejarlo y después te avisamos cuando este listo.'),
(72, 168, '2017-08-03 18:19:13', 0, 13, 'Ah perfecto... te lo dejo entonces, igual cualquier cosas les mando un mensaje por acá. Saludos y gracias.'),
(74, 171, '2017-08-11 17:30:12', 0, 13, 'Prueba Notificar tienda'),
(76, 173, '2017-08-11 19:02:33', 0, 13, 'prueba turnos a las'),
(77, 175, '2017-08-16 02:23:03', 0, 13, 'Prueba turno 2 días antes debe llegar el recordatorio');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `nombre`, `apellido`, `username`, `email`, `password`, `avatar`, `created_at`, `updated_at`, `is_admin`, `is_confirmed`, `is_deleted`) VALUES
(1, 'root', 'root', 'root', 'sebastian.piquet@hotmail.com', '$2y$10$0WX7fFKRJYNadmIZNru2neFtjWlc9eR0bsIf.3sMsV14nk5r5jMeq', 'default.jpg', '2016-05-03 23:01:36', '2016-05-25 06:32:17', 0, 0, 0),
(10, 'Francisco', 'Atencio', 'fatencio', 'fatencio@gmail.com', '$2y$10$paoYN1RKPhwUPu8Ak6bXFeRzsKBSJ4IllXkCRdHBT6tLxn7hz/5pW', 'default.jpg', '2016-05-25 06:31:36', NULL, 0, 0, 0),
(12, 'Sebastián', 'Piquet', 'spiquet', 'sebastian.piquet2@gmail.com', '$2y$10$m06ybS3iD4.Y8Ya9qvICgerLmYnqWSD9jscenfR3Z/8Vowrdlf0Py', 'default.jpg', '2016-05-26 10:43:52', '2017-06-03 10:41:51', 0, 0, 0),
(15, 'Germán ', 'Vide', 'Gvide', 'Germanvans@hotmail.com', '$2y$10$stqlfyrzXBpo6nD4wTbTje0/veWNiXiBT02MB7OEHNL3P8qIljOm6', 'default.jpg', '2016-05-26 16:14:43', '2016-06-09 13:17:01', 0, 0, 0),
(16, 'Christian', 'Zanabria', 'Turko', 'Turkototem@hotmail.com', '$2y$10$W1vEUik/UkBT15A.7IohauMeOu31pG1hszg1mq0iDo6NXpPVNROJW', 'default.jpg', '2016-06-29 08:23:19', '2016-06-29 09:44:50', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vacuna`
--

CREATE TABLE IF NOT EXISTS `vacuna` (
  `vacuna_id` int(11) NOT NULL,
  `vacuna_mascota_id` int(11) NOT NULL,
  `vacuna_local_id` int(11) NOT NULL,
  `vacuna_fecha` date NOT NULL,
  `vacuna_fecha_proxima` date DEFAULT NULL,
  `vacuna_fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vacuna_nombre` varchar(255) NOT NULL,
  `vacuna_peso` varchar(255) DEFAULT NULL,
  `vacuna_veterinario` varchar(255) NOT NULL,
  `vacuna_observaciones` varchar(500) NOT NULL,
  `vacuna_aplicada` varchar(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vacuna`
--

INSERT INTO `vacuna` (`vacuna_id`, `vacuna_mascota_id`, `vacuna_local_id`, `vacuna_fecha`, `vacuna_fecha_proxima`, `vacuna_fecha_creacion`, `vacuna_nombre`, `vacuna_peso`, `vacuna_veterinario`, `vacuna_observaciones`, `vacuna_aplicada`) VALUES
(3, 9, 30, '2017-02-19', NULL, '2017-02-25 03:37:34', 'Antirábica', '10kg', 'Alfredo Ramos', 'Traigalo sedado', ''),
(4, 2, 23, '2017-02-20', '2018-02-20', '2017-04-18 13:17:49', 'Sextuple', '32 Kg', 'Raúl', '', 'si'),
(5, 2, 0, '2017-03-06', '2018-03-06', '2017-03-06 03:01:31', 'Sextuple y Rabia', '30 Kg', 'Mario', '', 'si'),
(6, 11, 25, '2017-02-21', NULL, '2017-02-25 03:37:34', 'Rabia', '30 Kg', 'Mario', '', 'si'),
(8, 11, 25, '2017-02-22', NULL, '2017-02-25 03:37:34', 'Moquillo', '30 Kg', 'Mario', '', 'no'),
(9, 8, 23, '2017-02-24', NULL, '2017-02-25 03:37:34', 'Gordix', '10 kg', 'Sebastián Piquet', 'Test', 'no'),
(11, 13, 23, '2017-03-02', '2018-03-02', '2017-03-03 01:39:41', 'Rabia', '20kg', 'Dr. Piquet', 'Testeando notificaciones', 'si'),
(12, 13, 23, '2017-03-02', '2017-03-03', '2017-03-02 22:47:08', 'Rabia refuerzo', '20kg', 'Dr. Piquet', 'test', 'si'),
(15, 13, 23, '2017-03-06', '2018-03-06', '2017-03-06 03:05:36', 'Rabia refuerzo', '20kg', 'Dr. Piquet', 'test', 'si'),
(16, 13, 23, '2017-03-15', '2017-03-15', '2017-03-24 00:48:16', 'Test notificación recordatorio', '20kg', 'Dr. Piquet', 'Test notificación recordatorio', 'si'),
(17, 14, 23, '2017-01-20', '2017-03-24', '2017-03-24 01:03:04', 'Rabia y Sextuple', '12 Kg', 'Mario', 'Prueba recordatorio', 'si'),
(18, 14, 30, '2017-03-28', '2017-03-29', '2017-03-28 16:05:04', 'Rabia', '20 Kg', 'Jorge', 'Prueba recordatorio mail... notificaciones', 'si'),
(19, 14, 30, '2017-03-28', '2017-03-29', '2017-03-28 16:20:55', 'Sextuple', '22 Kg', 'Jorge', '2da prueba de notificaciones y mails (Comprador y Local)', 'si'),
(20, 21, 23, '2017-05-11', '2017-05-25', '2017-05-18 01:10:05', 'Test mobile', '12', 'Pedro', 'Test para ver en mobile', 'si'),
(21, 27, 25, '2017-07-16', '2017-07-16', '2017-07-15 23:28:54', 'Test', '23', 'Roberto', 'test', ''),
(22, 26, 33, '2017-07-17', '2017-07-18', '2017-07-18 02:23:50', 'Sextuple y Rabia', '32', 'Christian', 'Prueba hecha 17/07/2017 a las 21:23 Hs', 'si');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta`
--

CREATE TABLE IF NOT EXISTS `venta` (
  `venta_id` int(11) NOT NULL,
  `venta_tipo_envio` varchar(255) NOT NULL,
  `venta_cliente_nombre` varchar(255) DEFAULT NULL,
  `venta_cliente_direccion` varchar(255) DEFAULT NULL,
  `venta_cliente_localidad` varchar(255) DEFAULT NULL,
  `venta_cliente_provincia` varchar(100) NOT NULL,
  `venta_cliente_telefono` varchar(255) DEFAULT NULL,
  `venta_observaciones` text,
  `venta_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `venta_mes` varchar(6) NOT NULL,
  `venta_local_id` int(11) NOT NULL,
  `venta_cliente_id` int(11) NOT NULL,
  `venta_total` float NOT NULL,
  `venta_estado` varchar(13) NOT NULL DEFAULT 'pendiente',
  `venta_condicion_venta` varchar(100) NOT NULL DEFAULT '',
  `venta_concretado_cliente` varchar(2) NOT NULL,
  `venta_puntaje_cliente` int(11) DEFAULT NULL,
  `venta_valoracion_cliente` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `venta_valoracion_aprobada` varchar(2) NOT NULL,
  `venta_concretado_local` varchar(2) NOT NULL,
  `venta_valoracion_local` varchar(200) NOT NULL,
  `venta_cancelado_por` varchar(7) NOT NULL,
  `venta_cancelado_comentario` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=350 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta`
--

INSERT INTO `venta` (`venta_id`, `venta_tipo_envio`, `venta_cliente_nombre`, `venta_cliente_direccion`, `venta_cliente_localidad`, `venta_cliente_provincia`, `venta_cliente_telefono`, `venta_observaciones`, `venta_fecha`, `venta_mes`, `venta_local_id`, `venta_cliente_id`, `venta_total`, `venta_estado`, `venta_condicion_venta`, `venta_concretado_cliente`, `venta_puntaje_cliente`, `venta_valoracion_cliente`, `venta_valoracion_aprobada`, `venta_concretado_local`, `venta_valoracion_local`, `venta_cancelado_por`, `venta_cancelado_comentario`) VALUES
(191, 'A domicilio', 'German Vide', 'madero 1474', 'San Fernando', '1', '4328-2215', 'Prueba CANCELA USUARIO 30/1/17 a las 12:18', '2017-01-30 15:18:26', '201701', 30, 13, 300, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'Prueba CANCELA USUARIO 30/1/17 a las 12:43'),
(194, 'A domicilio', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '3333-3333', '', '2017-01-31 23:04:20', '201701', 30, 14, 100, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(195, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '2222-2222', 'lo paso a buscar a las q0hs', '2017-02-01 12:27:52', '201702', 30, 14, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(196, 'A domicilio', 'Pedro Carreras', 'Mi dir', 'MI loc', '5', '123456', 'test cambia telefono cliente', '2017-02-01 15:32:49', '201702', 30, 43, 100, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'test notifica al local'),
(197, 'Retiro en el local', 'Pedro Carreras', '', '', '5', '123456', '', '2017-02-01 15:40:56', '201702', 30, 43, 200, 'concretado', 'Efectivo', 'si', 5, 'todo ok!', 'si', 'si', 'Todo OK 2', '', ''),
(199, 'A domicilio', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '3333-3333', 'test guardado tel', '2017-02-02 01:36:41', '201702', 30, 14, 100, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(206, 'A domicilio', 'elvira mama', 'monteverde 4208 Olivos', 'vicente lopez', '5', '47991669', '', '2017-02-05 00:42:28', '201702', 30, 39, 2600, 'concretado', 'Efectivo', 'si', 5, 'espero tener descuento por ser clienta!', 'si', 'si', 'todo OK', '', ''),
(207, 'A domicilio', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', 'test compra masiva', '2017-02-05 01:44:40', '201702', 30, 14, 6500, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(209, 'A domicilio', 'May Laloca', 'Bouchard 1394', 'La Lucila', '5', '47941394', '', '2017-02-07 12:16:32', '201702', 30, 34, 100, 'concretado', 'Efectivo', 'si', 1, 'Ya no tenía stock de mi pedido y tuve que elegir otro producto.', 'si', 'si', 'compro otra cosa', '', ''),
(210, 'Retiro en el local', 'Pedro Carreras', '', '', '5', '123456', '', '2017-02-07 16:23:14', '201702', 30, 43, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(214, 'Retiro en el local', 'Pedro Carreras', '', '', '5', '123456', '', '2017-02-10 21:45:29', '201702', 30, 43, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(216, 'A domicilio', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', '', '2017-02-12 22:00:20', '201702', 30, 14, 100, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(219, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-02-14 03:41:56', '201702', 30, 14, 100, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'no llego'),
(245, 'Retiro en el local', 'Pedro Carreras', '', '', '5', '123456', NULL, '2017-03-11 19:37:10', '201703', 30, 43, 300, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(246, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-03-12 06:13:40', '201703', 30, 14, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(260, 'A domicilio', 'German Vide', 'madero 1474', 'San Fernando', '1', '9999-9999', NULL, '2017-04-05 18:23:01', '201704', 30, 13, 700, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(265, 'A domicilio', 'German Vide', 'Ayacucho 1010', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-04-08 16:56:59', '201704', 30, 13, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(266, 'A domicilio', 'German Vide', 'Ayacucho 500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-04-19 18:03:10', '201704', 30, 13, 1346, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(267, 'Retiro en el local', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2615118372', NULL, '2017-04-28 16:05:17', '201704', 25, 43, 308, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(268, 'A domicilio', 'German Vide', 'Libertad 500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-04-28 17:38:39', '201704', 30, 13, 1890, 'concretado', 'Efectivo', 'si', 4, 'Todo OK', 'si', 'si', 'Todo bien realizado', '', ''),
(269, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-05 01:57:23', '201705', 31, 14, 80, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(270, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-06 23:56:41', '201705', 31, 14, 2750, 'concretado', 'Efectivo', 'si', 5, 'pereecto', 'si', '', '', '', ''),
(271, 'Retiro en el local', 'German Vide', 'Libertad 500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-05-09 00:13:05', '201705', 25, 13, 2950, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(272, 'Retiro en el local', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2615118372', NULL, '2017-05-09 15:01:05', '201705', 25, 43, 1500, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(273, 'Retiro en el local', 'German Vide', 'Libertad 500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-05-09 21:27:12', '201705', 25, 13, 2400, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(274, 'A domicilio', 'German Vide', 'Libertad 500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-05-10 15:32:34', '201705', 25, 13, 3900, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(275, 'A domicilio', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2615118372', NULL, '2017-05-11 15:31:59', '201705', 25, 43, 500, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(276, 'Retiro en el local', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2614556677c', NULL, '2017-05-12 13:58:22', '201705', 25, 43, 2250, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(277, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-16 02:55:31', '201705', 31, 14, 550, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(281, 'Retiro en el local', 'christian zanabria', '', '', '5', '1111-1111', NULL, '2017-05-16 16:52:10', '201705', 25, 28, 1100, 'concretado', 'Efectivo', 'no', 1, 'Nunca aparecio', 'si', 'si', 'Gracias por su compra', '', ''),
(283, 'A domicilio', 'christian zanabria', 'Concordia 5353', 'Villa devoto', '5', '1111-1111', NULL, '2017-05-16 19:57:14', '201705', 25, 28, 780, 'concretado', 'Efectivo', 'no', 1, 'No concretado', 'si', 'si', 'concretado', '', ''),
(284, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-17 22:47:21', '201705', 31, 14, 550, 'concretado', 'Efectivo', 'si', 5, 'aviso abajo', 'si', '', '', '', ''),
(285, 'Retiro en el local', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2614556677c', NULL, '2017-05-18 00:55:52', '201705', 25, 43, 1150, 'concretado', 'Efectivo', 'si', 5, 'surge en Mendoza con la búsqueda constante de hacer y ofrecer una alimentación más consciente no sólo para nuestro cuerpo, sino para nuestro planeta y con los animales', 'si', 'si', 'Ok', '', ''),
(286, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-18 03:55:46', '201705', 31, 14, 550, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(287, 'A domicilio', 'German Vide', 'sarmiento 1500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-05-18 04:01:42', '201705', 25, 13, 200, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(288, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-18 14:24:49', '201705', 31, 14, 68, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(289, 'Retiro en el local', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', '12', '2614556677', NULL, '2017-05-18 16:01:33', '201705', 25, 43, 500, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(290, 'Retiro en el local', 'German Vide', 'sarmiento 1500', 'Vicente Lopez', '1', '9999-9999', NULL, '2017-05-19 00:04:17', '201705', 25, 13, 300, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(291, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-19 00:57:06', '201705', 31, 14, 9, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(292, 'Retiro en el local', 'christian zanabria', 'Concordia 5353', 'Villa devoto', '5', '1111-1111', NULL, '2017-05-24 19:57:00', '201705', 31, 28, 120, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(293, 'Retiro en el local', 'christian zanabria', 'Concordia 5353', 'Villa devoto', '5', '1111-1111', NULL, '2017-05-24 20:15:44', '201705', 31, 28, 60, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'cancelado desde comprador'),
(294, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-26 02:12:49', '201705', 25, 14, 100, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(295, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-26 19:02:44', '201705', 31, 14, 559, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(296, 'A domicilio', 'christian zanabria', 'Concordia 5353', 'Villa devoto', '5', '1111-1111', NULL, '2017-05-26 19:16:39', '201705', 25, 28, 270, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(298, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-26 19:32:24', '201705', 25, 14, 270, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(299, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-05-27 05:31:15', '201705', 25, 14, 850, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(300, 'A domicilio', 'German Vide', 'sarmiento 1500', 'Tigre', '1', '9999-9991', NULL, '2017-05-27 15:56:05', '201705', 25, 13, 5710, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(302, 'A domicilio', 'German Vide', 'José ingenieros 1500', 'Beccar ', '1', '9999-9990', NULL, '2017-05-30 15:49:02', '201705', 25, 13, 1440, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(303, 'Retiro en el local', 'German Vide', 'José ingenieros 1500', 'Beccar', '1', '9999-9990', NULL, '2017-05-30 17:00:06', '201705', 25, 13, 890, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(304, 'Retiro en el local', 'German Vide', 'José ingenieros 1500', 'Beccar', '1', '9999-9990', NULL, '2017-06-02 17:28:32', '201706', 25, 13, 400, 'cancelado', 'Mercadopago', '', NULL, '', '', '', '', '', ''),
(307, 'A domicilio', 'German Vide', 'Sarmiento 1500', 'Tigre', '1', '9999-9990', NULL, '2017-06-03 00:56:41', '201706', 25, 13, 800, 'concretado', 'Efectivo', 'si', 4, 'OK lado comprador.... queda hasta que es calificado y una ves calificado pasa a historial', 'si', 'si', 'Ok lado Tienda... queda hasta que es notificado. 7/6/17', '', ''),
(308, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-06-04 18:25:07', '201706', 31, 14, 9, 'cancelado', 'Mercadopago', '', NULL, '', '', '', '', '', ''),
(309, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-06-06 01:28:49', '201706', 34, 14, 1195, 'concretado', 'Efectivo', 'si', 5, 'Como siempre muy amables!', 'si', 'si', 'Se podría incluir la opción de rechazar la compra por falta de stock de alguno de estos productos.', '', ''),
(310, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-06-06 01:31:33', '201706', 34, 14, 100, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'Me equivoqué de producto!'),
(311, 'Retiro en el local', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-06-06 02:43:02', '201706', 34, 14, 3500, 'concretado', 'Efectivo', 'si', 5, 'La dueña una genia!', 'si', 'si', 'Rápido y sencillo!', '', ''),
(312, 'A domicilio', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-06-06 04:22:30', '201706', 33, 14, 500, 'concretado', 'Efectivo', 'si', 5, '????', 'si', 'si', 'todo perfecto', '', ''),
(313, 'Retiro en la tienda', 'German Vide', 'Sarmiento 1500', 'Tigre', '1', '9999-9990', NULL, '2017-06-23 03:50:12', '201706', 25, 13, 240, 'concretado', 'Efectivo', 'si', 5, 'Todo OK', 'no', 'si', 'Todo perfecto', '', ''),
(315, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-07-12 21:36:55', '201707', 34, 14, 830, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'no puedo pasar'),
(316, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-07-12 21:37:49', '201707', 34, 14, 65, 'no concretado', 'Efectivo', 'no', 2, 'pésimo negocio', 'no', 'no', 'no concretado lado LOCAL', '', ''),
(317, 'A domicilio', 'German Vide', 'Sarmiento 1500', 'Vicente Lopez', '1', '9999-9990', NULL, '2017-07-13 04:49:28', '201707', 25, 13, 2670, 'concretado', 'Efectivo', 'si', 4, 'Concretado lado COMPRADOR', 'si', 'si', 'Concretado lado LOCAL', '', ''),
(318, 'Retiro en la tienda', 'German Vide', 'Sarmiento 1500', 'Vicente Lopez', '1', '9999-9990', NULL, '2017-07-13 18:07:10', '201707', 35, 13, 200, 'concretado', 'Efectivo', 'si', 3, 'concretado lado COMPRADOR', 'si', 'no', 'NO concretado lado LOCAL', '', ''),
(319, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', '1', '4444-4444', NULL, '2017-07-15 22:54:57', '201707', 34, 14, 200, 'concretado', 'Efectivo', 'si', 5, 'todo ok', 'si', 'si', 'Sebi ratón, comprame la cuha de perro que está buenísima ;P', '', ''),
(320, 'Retiro en la tienda', 'Fulano Ito', '', '', '2', '', NULL, '2017-07-15 23:02:01', '201707', 34, 50, 65, 'concretado', 'Efectivo', 'si', 4, 'Todo Ok!!', 'si', 'si', 'Gracias Fulanito!', '', ''),
(321, 'A domicilio', 'Luciano Pepe', 'Monteverda 4208', 'Olivos', '1', '1111-1111', NULL, '2017-07-15 23:07:06', '201707', 34, 51, 65, 'concretado', 'Efectivo', 'no', 3, 'No concretado lado COMPRADOR', 'si', 'si', 'Gracias Pepe!', '', ''),
(324, 'Retiro en la tienda', 'rogelio pitufo', '', '', '', '', NULL, '2017-07-18 02:59:27', '201707', 25, 56, 1440, 'concretado', 'Efectivo', 'si', 5, 'Concretado COMPRADOR', 'si', 'si', 'CONCRETADO LADO TIENDA', '', ''),
(325, 'A domicilio', 'rogelio pitufo', 'Diaz Velez 400', 'La lucila', 'Buenos Aires', '1414-1515', NULL, '2017-07-18 03:08:50', '201707', 34, 56, 1800, 'concretado', 'Efectivo', 'no', 2, 'NO concretado lado CLIENTE', 'si', 'si', 'Gracias Rogelio! Qué la disfrutes :D', '', ''),
(328, 'A domicilio', 'German Vide', 'Quintana 1500', 'Florida', 'Buenos Aires', '9999-9994', NULL, '2017-07-19 18:52:28', '201707', 25, 13, 1230, 'no concretado', 'Efectivo', 'no', 1, 'No concretado', 'no', 'no', 'NO CONCRETADO LADO LOCAL', '', ''),
(329, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', 'Buenos Aires', '4444-4444', NULL, '2017-07-22 22:55:33', '201707', 34, 14, 65, 'concretado', 'Tarjetas de Crédito', 'si', 5, 'Optimo!', 'si', 'si', 'Quiso pagar los $65 en 12 cuotas... ratón', '', ''),
(330, 'A domicilio', 'German Vide', 'Quintana 1500', 'Florida', 'Buenos Aires', '9999-9994', NULL, '2017-07-22 23:01:49', '201707', 25, 13, 100, 'concretado', 'Tarjetas de Débito', 'si', 5, 'Concretado lado COMPRADOR', 'si', 'si', 'Concretado LOCAL', '', ''),
(331, 'Retiro en la tienda', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', 'La Rioja', '123', NULL, '2017-07-25 22:51:21', '201707', 34, 43, 1060, 'concretado', 'Efectivo', '', NULL, '', '', 'si', 'Pedro Carreras es el loco de los gatos?', '', ''),
(332, 'A domicilio', 'German Vide', 'Quintana 1500', 'Florida', 'Buenos Aires', '9999-9994', NULL, '2017-07-26 15:33:35', '201707', 25, 13, 200, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'Cancela el COMPRADOR'),
(333, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', 'Buenos Aires', '4444-4444', NULL, '2017-07-29 17:18:58', '201707', 37, 14, 750, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(335, 'A domicilio', 'German Vide', 'Quintana 1500', 'Florida', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-02 01:12:09', '201708', 25, 13, 1780, 'no concretado', 'Efectivo', 'no', 3, 'No concretado lado cliente', 'si', 'no', 'no concretado lado local', '', ''),
(337, 'A domicilio', 'German Vide', 'Roca 1270', 'Vicente Lopez', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-03 05:07:25', '201708', 25, 13, 1780, 'cancelado', 'Efectivo', '', NULL, '', '', '', '', 'cliente', 'cancela el cliente\r\n'),
(339, 'A domicilio', 'German Vide', 'Sarmiento 1590', 'San Fernando', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-03 16:27:24', '201708', 25, 13, 2180, 'concretado', 'Efectivo', 'si', 4, 'Todo en orden....llego en el tiempo que dijeron', 'si', 'si', 'Todo Ok??', '', ''),
(340, 'A domicilio', 'Pedro Carreras', 'Antártida Argentina 439', 'Luján de Cuyo', 'La Rioja', '123', NULL, '2017-08-05 16:36:49', '201708', 34, 43, 2100, 'cancelado', 'Tarjetas de Crédito', '', NULL, '', '', '', '', 'cliente', 'Test cancela pedido'),
(342, 'Retiro en la tienda', 'Sebastián Piquet', 'Vergara 3646 1A', 'Florida Oeste', 'Buenos Aires', '4444-4444', NULL, '2017-08-07 01:59:40', '201708', 25, 14, 750, 'concretado', 'Efectivo', '', NULL, '', '', 'si', 'Concretado lado tienda', '', ''),
(343, 'Retiro en la tienda', 'christian zanabria', 'Concordia 5353', 'Villa devoto', 'Ciudad Autónoma de Buenos Aires', '1111-1111', NULL, '2017-08-08 00:06:04', '201708', 33, 28, 250, 'concretado', 'Efectivo', '', NULL, '', '', '', '', '', ''),
(344, 'A domicilio', 'christian zanabria', 'Concordia 5353', 'Villa devoto', 'Ciudad Autónoma de Buenos Aires', '1111-1111', NULL, '2017-08-08 00:10:10', '201708', 25, 28, 4000, 'concretado', 'Efectivo', 'si', 4, 'Es comodo. Se podria mejor la interfaz.', 'si', 'si', 'Buen cliente', '', ''),
(345, 'A domicilio', 'German Vide', 'Sarmiento 1590', 'San Fernando', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-09 01:41:16', '201708', 25, 13, 1000, 'concretado', 'Tarjetas de Débito', '', NULL, '', '', 'si', 'Concretado (Notifico la tienda atraves del link del mail)', '', ''),
(346, 'Retiro en la tienda', 'German Vide', 'Sarmiento 1590', 'San Fernando', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-11 15:49:17', '201708', 35, 13, 500, 'cancelado', 'Tarjetas de Crédito', '', NULL, '', '', '', '', 'cliente', 'Cancelado desde el celular '),
(347, 'Retiro en la tienda', 'German Vide', 'Sarmiento 1590', 'San Fernando', 'Buenos Aires', '15-1234-5678', NULL, '2017-08-11 16:22:06', '201708', 31, 13, 1120, 'cancelado', 'Tarjetas de Débito', '', NULL, '', '', '', '', 'cliente', 'Cancela desde celular compra en Pet Shop ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_comentario`
--

CREATE TABLE IF NOT EXISTS `venta_comentario` (
  `venta_comentario_id` int(11) NOT NULL,
  `venta_comentario_venta_id` int(11) NOT NULL,
  `venta_comentario_fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `venta_comentario_local_id` int(11) NOT NULL,
  `venta_comentario_cliente_id` int(11) NOT NULL,
  `venta_comentario_comentario` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_comentario`
--

INSERT INTO `venta_comentario` (`venta_comentario_id`, `venta_comentario_venta_id`, `venta_comentario_fecha`, `venta_comentario_local_id`, `venta_comentario_cliente_id`, `venta_comentario_comentario`) VALUES
(58, 245, '2017-03-11 19:37:10', 0, 43, 'Mensaje del cliente al local (pedido)'),
(74, 260, '2017-04-05 18:23:01', 0, 13, 'prueba'),
(77, 265, '2017-04-08 16:56:59', 0, 13, 'Prueba cambio de direccion en front end y back end'),
(78, 266, '2017-04-19 18:03:10', 0, 13, 'prueba 19/4/2017'),
(79, 267, '2017-04-28 16:05:17', 0, 43, 'test'),
(80, 268, '2017-04-28 17:38:39', 0, 13, 'Producto creado por el local'),
(81, 269, '2017-05-05 01:57:23', 0, 14, 'test celu'),
(82, 271, '2017-05-09 00:13:05', 0, 13, 'prueba ''''Retiro en el local'''''),
(83, 272, '2017-05-09 15:01:05', 0, 43, 'test mail'),
(84, 273, '2017-05-09 21:27:12', 0, 13, 'Prueba'),
(85, 274, '2017-05-10 15:32:34', 0, 13, 'Prueba de mails'),
(86, 276, '2017-05-12 13:58:22', 0, 43, 'test compra'),
(89, 281, '2017-05-16 16:52:10', 0, 28, 'Para los pichichos'),
(90, 282, '2017-05-16 19:10:37', 0, 28, 'Pago con billetes de 500. Traer cambio'),
(91, 283, '2017-05-16 19:57:14', 0, 28, '14:56'),
(92, 284, '2017-05-17 22:47:21', 0, 14, 'test mails'),
(93, 284, '2017-05-17 22:48:01', 31, 0, 'Local agrega comentario 1'),
(94, 284, '2017-05-17 22:50:03', 0, 14, 'Usuario agrega comentario 2'),
(95, 284, '2017-05-17 22:50:37', 31, 0, 'Local agrega comentario 3'),
(96, 285, '2017-05-18 00:55:52', 0, 43, 'test mobile'),
(97, 285, '2017-05-18 00:57:11', 0, 43, 'surge en Mendoza con la búsqueda constante de hacer y ofrecer una alimentación más consciente no sólo para nuestro cuerpo, sino para nuestro planeta y con los animales'),
(98, 286, '2017-05-18 03:55:46', 0, 14, 'responsive 1'),
(99, 287, '2017-05-18 04:01:42', 0, 13, 'prueba'),
(100, 288, '2017-05-18 14:24:49', 0, 14, 'test'),
(101, 289, '2017-05-18 16:01:33', 0, 43, 'Prueba de compra desde mobile. Mensaje largo para ver responsive'),
(102, 290, '2017-05-19 00:04:17', 0, 13, 'Prueba desde tablet'),
(103, 291, '2017-05-19 00:57:06', 0, 14, 'test'),
(108, 292, '2017-05-24 19:57:00', 0, 28, '14:56'),
(109, 295, '2017-05-26 19:02:44', 0, 14, 'test 1'),
(110, 296, '2017-05-26 19:16:39', 0, 28, 'Asd'),
(112, 298, '2017-05-26 19:32:24', 0, 14, 'test art de gato'),
(113, 300, '2017-05-27 15:56:05', 0, 13, 'Prueba tablet 27/5/17 a las 10:55'),
(114, 300, '2017-05-27 16:42:49', 25, 0, 'Respuesta desde tablet a las 11:54'),
(115, 300, '2017-05-27 16:49:10', 0, 13, 'Respuesta desde celular a las 12:00'),
(116, 300, '2017-05-27 16:51:54', 25, 0, 'Ok\r\n'),
(117, 301, '2017-05-30 14:49:45', 0, 13, 'Prueba desde IPhone 30/5/17 a las 9:49 OK'),
(118, 302, '2017-05-30 15:49:02', 0, 13, '2da prueba IPhone'),
(119, 303, '2017-05-30 17:00:06', 0, 13, '30/5/17 a las 12:00'),
(120, 302, '2017-05-30 17:23:50', 25, 0, 'Ok desde PC'),
(121, 302, '2017-05-30 17:33:50', 0, 13, 'Respuesta desde celular'),
(122, 304, '2017-06-02 17:28:32', 0, 13, 'prueba'),
(125, 307, '2017-06-03 00:56:41', 0, 13, 'Prueba 2/6/17'),
(126, 309, '2017-06-06 01:28:49', 0, 14, 'Puedo pasar a buscarlo mañana a las 9hs?'),
(127, 310, '2017-06-06 01:31:33', 0, 14, 'Cuando van a tener descuentos??'),
(128, 310, '2017-06-06 01:35:27', 34, 0, 'Hola Sebastián.\r\nSu pedido ya está listo para ser retirado.\r\nPuede hacerlo a partir del martes 6 de junio. Tiene tiempo hasta el viernes 16 de junio.\r\nSaludos,\r\nLa Tienda de May'),
(129, 309, '2017-06-06 01:35:53', 34, 0, 'Hola Sebastián.\r\nSu pedido ya está listo para ser retirado.\r\nPuede hacerlo a partir del martes 6 de junio. Tiene tiempo hasta el viernes 16 de junio.\r\nSaludos,\r\nLa Tienda de May'),
(130, 309, '2017-06-06 01:43:14', 0, 14, 'Genial!'),
(131, 310, '2017-06-06 01:43:32', 0, 14, 'Y los descuentos? jeje'),
(132, 310, '2017-06-06 01:45:28', 34, 0, 'Lo sentimos, en este momento no contamos con descuentos.\r\n¡Esté atento para el lanzamiento de nuestras próximas promociones!'),
(133, 311, '2017-06-06 02:43:02', 0, 14, 'Me enamoré!!!'),
(134, 312, '2017-06-06 04:22:30', 0, 14, 'Cuanto tiempo más estará este descuento?'),
(135, 313, '2017-06-23 03:50:12', 0, 13, 'prueba 22/06/2017 a las 10:49'),
(136, 313, '2017-06-24 01:37:40', 0, 13, 'paso en 5 min'),
(139, 315, '2017-07-12 21:36:55', 0, 14, 'lo busco hoy'),
(140, 317, '2017-07-13 04:49:28', 0, 13, 'Prueba 12/07/2017'),
(144, 319, '2017-07-15 22:54:57', 0, 14, 'test'),
(146, 320, '2017-07-15 23:02:01', 0, 50, 'test'),
(147, 321, '2017-07-15 23:07:06', 0, 51, 'test'),
(150, 324, '2017-07-18 02:59:27', 0, 56, 'Prueba usuario nuevo 17/07/2017'),
(151, 325, '2017-07-18 03:08:50', 0, 56, 'Prueba a domicilio 17/07/2017'),
(153, 328, '2017-07-19 18:52:28', 0, 13, 'Prueba 19/07/2017 a las 13:52'),
(154, 330, '2017-07-22 23:01:49', 0, 13, 'prueba 22/07/2017'),
(156, 335, '2017-08-02 01:12:09', 0, 13, 'El timbre de mi casa es el 4C.. me lo pueden traer antes de las 16 hs??? Gracias'),
(157, 337, '2017-08-03 05:07:25', 0, 13, 'Me lo podrían traer antes de las 15 hs??? Gracias'),
(158, 339, '2017-08-03 16:27:24', 0, 13, 'Hola que tal!!! En que horario me podrían traer las bolsas de alimento??? Gracias'),
(159, 339, '2017-08-03 16:55:42', 25, 0, 'Hola buen día!!! Entregamos de 13:00 a 20:00 Hs'),
(160, 339, '2017-08-03 16:59:42', 0, 13, 'Me lo podrían traer a las 15 Hs?? porque después ya me tengo que ir a trabajar, y no hay nadie para recibirlo...'),
(161, 339, '2017-08-03 17:01:37', 25, 0, 'Si no hay problema pasamos en es horario... saludos!!'),
(162, 342, '2017-08-07 01:59:40', 0, 14, 'test cco'),
(163, 345, '2017-08-09 01:41:16', 0, 13, 'Prueba 8/8/2017 a las 20:41'),
(164, 346, '2017-08-11 15:49:17', 0, 13, 'Prueba cancela cliente 11/8/2017'),
(165, 347, '2017-08-11 16:22:06', 0, 13, 'Prueba en pet shop 11/8/2017');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `venta_detalle`
--

CREATE TABLE IF NOT EXISTS `venta_detalle` (
  `venta_detalle_id` int(11) NOT NULL,
  `venta_detalle_venta_id` int(11) NOT NULL,
  `venta_detalle_articulo_id` int(11) NOT NULL,
  `venta_detalle_presentacion_id` int(11) NOT NULL,
  `venta_detalle_precio` float NOT NULL,
  `venta_detalle_descuento` float NOT NULL,
  `venta_detalle_cantidad` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `venta_detalle`
--

INSERT INTO `venta_detalle` (`venta_detalle_id`, `venta_detalle_venta_id`, `venta_detalle_articulo_id`, `venta_detalle_presentacion_id`, `venta_detalle_precio`, `venta_detalle_descuento`, `venta_detalle_cantidad`) VALUES
(165, 191, 397, 52, 100, 0, 3),
(168, 194, 391, 52, 100, 0, 1),
(169, 195, 220, 55, 100, 0, 1),
(170, 195, 212, 56, 100, 0, 1),
(171, 196, 391, 59, 100, 0, 1),
(172, 197, 368, 53, 100, 0, 1),
(173, 197, 368, 56, 100, 0, 1),
(175, 199, 391, 52, 100, 0, 1),
(183, 206, 391, 52, 100, 0, 1),
(184, 206, 391, 59, 100, 0, 11),
(185, 206, 390, 52, 100, 0, 14),
(186, 207, 391, 52, 100, 0, 1),
(187, 207, 390, 52, 100, 0, 1),
(188, 207, 394, 54, 100, 0, 1),
(189, 207, 389, 78, 100, 0, 1),
(190, 207, 387, 77, 100, 0, 1),
(191, 207, 376, 67, 100, 0, 1),
(192, 207, 370, 52, 100, 0, 1),
(193, 207, 393, 54, 100, 0, 20),
(194, 207, 369, 53, 100, 0, 20),
(195, 207, 377, 53, 100, 0, 10),
(196, 207, 385, 77, 100, 0, 5),
(197, 207, 381, 58, 100, 0, 3),
(199, 209, 321, 50, 100, 0, 1),
(200, 210, 379, 52, 100, 0, 1),
(201, 210, 379, 67, 100, 0, 1),
(205, 214, 280, 56, 100, 0, 1),
(206, 214, 281, 56, 100, 0, 1),
(209, 216, 391, 52, 100, 0, 1),
(213, 219, 391, 52, 100, 0, 1),
(245, 245, 383, 69, 100, 0, 1),
(246, 245, 383, 75, 100, 0, 2),
(247, 246, 390, 52, 100, 0, 1),
(248, 246, 379, 56, 100, 0, 1),
(265, 260, 192, 56, 100, 0, 7),
(271, 265, 300, 54, 100, 0, 2),
(272, 266, 193, 57, 269.1, 0, 5),
(273, 267, 451, 50, 200, 0, 1),
(274, 267, 467, 76, 108, 0, 1),
(276, 269, 160, 51, 80, 0, 1),
(277, 270, 390, 52, 550, 0, 5),
(278, 271, 393, 54, 650, 0, 1),
(279, 271, 392, 62, 800, 0, 1),
(280, 271, 394, 54, 750, 0, 2),
(281, 272, 394, 54, 750, 0, 2),
(282, 273, 392, 62, 800, 0, 3),
(283, 274, 393, 59, 780, 0, 5),
(284, 275, 393, 52, 500, 0, 1),
(285, 276, 394, 54, 750, 0, 3),
(286, 277, 390, 52, 550, 0, 1),
(290, 281, 165, 57, 100, 0, 1),
(291, 281, 359, 55, 1000, 0, 1),
(296, 283, 393, 59, 780, 0, 1),
(297, 284, 390, 52, 550, 0, 1),
(298, 285, 393, 52, 500, 0, 1),
(299, 285, 393, 54, 650, 0, 1),
(300, 286, 390, 52, 550, 0, 1),
(302, 288, 164, 57, 9, 0, 2),
(303, 288, 161, 50, 50, 0, 1),
(304, 289, 393, 52, 500, 0, 1),
(305, 290, 472, 77, 100, 0, 3),
(306, 291, 164, 57, 9, 0, 1),
(311, 292, 159, 50, 60, 0, 2),
(312, 293, 159, 50, 60, 0, 1),
(313, 294, 544, 77, 100, 0, 1),
(314, 295, 164, 57, 9, 0, 1),
(315, 295, 390, 52, 550, 0, 1),
(316, 296, 544, 57, 270, 0, 1),
(318, 298, 544, 57, 270, 0, 1),
(319, 299, 544, 77, 100, 0, 1),
(320, 299, 543, 52, 750, 0, 1),
(321, 300, 357, 55, 890, 0, 5),
(322, 300, 555, 57, 130, 0, 2),
(323, 300, 406, 50, 250, 0, 1),
(324, 300, 550, 62, 250, 0, 3),
(326, 302, 337, 52, 720, 0, 2),
(327, 303, 357, 55, 890, 0, 1),
(328, 304, 564, 57, 200, 0, 2),
(331, 307, 165, 54, 400, 0, 2),
(332, 308, 164, 57, 9, 0, 1),
(333, 309, 555, 52, 1000, 0, 1),
(334, 309, 544, 57, 65, 0, 3),
(335, 310, 554, 57, 100, 0, 1),
(336, 311, 602, 1, 3500, 0, 1),
(337, 312, 170, 54, 500, 0, 1),
(338, 313, 547, 56, 120, 0, 2),
(340, 315, 543, 52, 700, 0, 1),
(341, 315, 544, 57, 65, 0, 2),
(342, 316, 543, 57, 65, 0, 1),
(343, 317, 357, 55, 890, 0, 3),
(344, 318, 571, 72, 200, 0, 1),
(350, 319, 442, 50, 200, 0, 1),
(352, 320, 544, 57, 65, 0, 1),
(353, 321, 544, 57, 65, 0, 1),
(357, 324, 337, 52, 720, 0, 2),
(358, 325, 602, 1, 1800, 0, 1),
(362, 328, 644, 1, 420, 0, 1),
(363, 328, 499, 50, 270, 0, 3),
(364, 329, 544, 57, 65, 0, 1),
(365, 330, 544, 77, 100, 0, 1),
(366, 331, 544, 57, 65, 0, 3),
(367, 331, 543, 57, 65, 0, 1),
(368, 331, 543, 52, 700, 0, 1),
(369, 331, 555, 57, 100, 0, 1),
(370, 332, 544, 77, 100, 0, 2),
(371, 333, 390, 52, 750, 0, 1),
(373, 335, 357, 55, 890, 0, 2),
(375, 337, 357, 55, 890, 0, 2),
(377, 339, 357, 55, 890, 0, 2),
(378, 339, 165, 54, 400, 0, 1),
(379, 340, 555, 57, 100, 0, 1),
(380, 340, 555, 52, 1000, 0, 2),
(383, 342, 543, 52, 750, 0, 1),
(384, 343, 605, 108, 250, 0, 1),
(385, 344, 163, 51, 400, 0, 10),
(386, 345, 647, 1, 500, 0, 2),
(387, 346, 569, 72, 250, 0, 2),
(388, 347, 350, 52, 560, 0, 2);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_animales`
--
CREATE TABLE IF NOT EXISTS `v_articulo_animales` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_animales_servicio`
--
CREATE TABLE IF NOT EXISTS `v_articulo_animales_servicio` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_animales_servicio_servicio`
--
CREATE TABLE IF NOT EXISTS `v_articulo_animales_servicio_servicio` (
`id` int(11)
,`nombre` varchar(255)
,`servicio_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_animales_servicio_servicio_item`
--
CREATE TABLE IF NOT EXISTS `v_articulo_animales_servicio_servicio_item` (
`id` int(11)
,`nombre` varchar(255)
,`servicio_id` int(11)
,`servicio_item_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_creadores`
--
CREATE TABLE IF NOT EXISTS `v_articulo_creadores` (
`id` char(9)
,`nombre` char(9)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_edades`
--
CREATE TABLE IF NOT EXISTS `v_articulo_edades` (
`id` varchar(50)
,`nombre` varchar(50)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_marcas`
--
CREATE TABLE IF NOT EXISTS `v_articulo_marcas` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_medicados`
--
CREATE TABLE IF NOT EXISTS `v_articulo_medicados` (
`id` tinyint(4)
,`nombre` varchar(2)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_presentaciones`
--
CREATE TABLE IF NOT EXISTS `v_articulo_presentaciones` (
`id` int(11)
,`nombre` varchar(255)
,`orden` int(11)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_razas`
--
CREATE TABLE IF NOT EXISTS `v_articulo_razas` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_rubros`
--
CREATE TABLE IF NOT EXISTS `v_articulo_rubros` (
`id` int(11)
,`nombre` varchar(200)
,`local_id` int(11)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_servicios`
--
CREATE TABLE IF NOT EXISTS `v_articulo_servicios` (
`id` int(11)
,`nombre` varchar(255)
,`servicio_item` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_servicios_animal`
--
CREATE TABLE IF NOT EXISTS `v_articulo_servicios_animal` (
`id` int(11)
,`nombre` varchar(255)
,`animal_id` int(11)
,`servicio_item` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_articulo_tamanios`
--
CREATE TABLE IF NOT EXISTS `v_articulo_tamanios` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_cliente_local`
--
CREATE TABLE IF NOT EXISTS `v_cliente_local` (
`tipo` varchar(7)
,`id` int(11)
,`nombre` text
,`apellido` text
,`email` text
,`password` text
,`status` int(11)
,`avatar` varchar(255)
,`cookie` varchar(180)
,`admin` bigint(20)
,`destacado` bigint(4)
,`usuario_baja` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_animales`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_animales` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_edades`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_edades` (
`id` varchar(50)
,`nombre` varchar(50)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_marcas`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_marcas` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_medicados`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_medicados` (
`id` tinyint(4)
,`nombre` varchar(2)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_presentaciones`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_presentaciones` (
`id` int(11)
,`nombre` varchar(255)
,`orden` int(11)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_razas`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_razas` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_rubros`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_rubros` (
`id` int(11)
,`nombre` varchar(200)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_geotienda_articulo_tamanios`
--
CREATE TABLE IF NOT EXISTS `v_geotienda_articulo_tamanios` (
`id` int(11)
,`nombre` varchar(255)
,`articulo_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_animales`
--
CREATE TABLE IF NOT EXISTS `v_local_animales` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_categorias`
--
CREATE TABLE IF NOT EXISTS `v_local_categorias` (
`local_id` int(11)
,`nombre` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_edades`
--
CREATE TABLE IF NOT EXISTS `v_local_edades` (
`id` varchar(50)
,`nombre` varchar(50)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_marcas`
--
CREATE TABLE IF NOT EXISTS `v_local_marcas` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_medicados`
--
CREATE TABLE IF NOT EXISTS `v_local_medicados` (
`id` tinyint(4)
,`nombre` varchar(2)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_presentaciones`
--
CREATE TABLE IF NOT EXISTS `v_local_presentaciones` (
`id` int(11)
,`nombre` varchar(255)
,`orden` int(11)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_razas`
--
CREATE TABLE IF NOT EXISTS `v_local_razas` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_rubros`
--
CREATE TABLE IF NOT EXISTS `v_local_rubros` (
`id` int(11)
,`nombre` varchar(200)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_servicios`
--
CREATE TABLE IF NOT EXISTS `v_local_servicios` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_servicios_animal`
--
CREATE TABLE IF NOT EXISTS `v_local_servicios_animal` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
,`animal_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_servicio_items`
--
CREATE TABLE IF NOT EXISTS `v_local_servicio_items` (
`id` int(11)
,`servicio_id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_servicio_items_animal`
--
CREATE TABLE IF NOT EXISTS `v_local_servicio_items_animal` (
`id` int(11)
,`servicio_id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
,`animal_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_local_tamanios`
--
CREATE TABLE IF NOT EXISTS `v_local_tamanios` (
`id` int(11)
,`nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_marca_animal`
--
CREATE TABLE IF NOT EXISTS `v_marca_animal` (
`marca_id` int(11)
,`marca_nombre` varchar(255)
,`marca_animales` text
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_marca_rubro_animal`
--
CREATE TABLE IF NOT EXISTS `v_marca_rubro_animal` (
`marca_id` int(11)
,`marca_nombre` varchar(255)
,`marca_rubros` text
,`marca_animales` text
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_presentaciones_articulo_local`
--
CREATE TABLE IF NOT EXISTS `v_presentaciones_articulo_local` (
`local_id` int(11)
,`articulo_id` int(11)
,`presentaciones_local` bigint(21)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_presentacion_rubro`
--
CREATE TABLE IF NOT EXISTS `v_presentacion_rubro` (
`presentacion_id` int(11)
,`presentacion_nombre` varchar(255)
,`presentacion_orden` int(11)
,`presentacion_rubros` text
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_servicios_local_animales`
--
CREATE TABLE IF NOT EXISTS `v_servicios_local_animales` (
`nombre` varchar(255)
,`servicio_item_id` int(11)
,`servicio_item_nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_servicios_local_servicios`
--
CREATE TABLE IF NOT EXISTS `v_servicios_local_servicios` (
`nombre` varchar(255)
,`servicio_item_id` int(11)
,`servicio_item_nombre` varchar(255)
,`local_id` int(11)
);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_venta_turno`
--
CREATE TABLE IF NOT EXISTS `v_venta_turno` (
`tipo` varchar(5)
,`id` int(11)
,`fecha_creacion` timestamp
,`numero` varchar(5)
,`fecha` varchar(10)
,`mes` varchar(6)
,`cliente_id` int(11)
,`cliente` mediumtext
,`cliente_email` text
,`local_id` int(11)
,`local` varchar(255)
,`total` float
,`estado` varchar(13)
,`forma_pago` varchar(100)
,`concretado_cliente` varchar(2)
,`puntaje_cliente` int(11)
,`valoracion_cliente` varchar(200)
,`valoracion_aprobada` varchar(2)
,`concretado_local` varchar(2)
,`valoracion_local` varchar(200)
,`cancelado_por` varchar(7)
,`cancelado_comentario` varchar(200)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_animales`
--
DROP TABLE IF EXISTS `v_articulo_animales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_animales` AS select distinct `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `animal` on((`articulo`.`articulo_animal_id` = `animal`.`animal_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_animales_servicio`
--
DROP TABLE IF EXISTS `v_articulo_animales_servicio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_animales_servicio` AS select `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from (((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) join `animal` on((`animal`.`animal_id` = `servicio_item`.`servicio_item_animal_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_animales_servicio_servicio`
--
DROP TABLE IF EXISTS `v_articulo_animales_servicio_servicio`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_animales_servicio_servicio` AS select `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`servicio`.`servicio_id` AS `servicio_id`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from (((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) join `animal` on((`animal`.`animal_id` = `servicio_item`.`servicio_item_animal_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_animales_servicio_servicio_item`
--
DROP TABLE IF EXISTS `v_articulo_animales_servicio_servicio_item`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_animales_servicio_servicio_item` AS select `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`servicio`.`servicio_id` AS `servicio_id`,`servicio_item`.`servicio_item_id` AS `servicio_item_id`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from (((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) join `animal` on((`animal`.`animal_id` = `servicio_item`.`servicio_item_animal_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_creadores`
--
DROP TABLE IF EXISTS `v_articulo_creadores`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_creadores` AS select distinct `local_articulo`.`local_articulo_creador` AS `id`,`local_articulo`.`local_articulo_creador` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from (((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_edades`
--
DROP TABLE IF EXISTS `v_articulo_edades`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_edades` AS select distinct `articulo`.`articulo_edad` AS `id`,`articulo`.`articulo_edad` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((`articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) where (`articulo`.`articulo_edad` <> '');

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_marcas`
--
DROP TABLE IF EXISTS `v_articulo_marcas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_marcas` AS select distinct `marca`.`marca_id` AS `id`,`marca`.`marca_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id`,`articulo`.`articulo_id` AS `articulo_id` from ((((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `marca` on((`articulo`.`articulo_marca_id` = `marca`.`marca_id`))) join `marca_animal` on((`marca_animal`.`marca_animal_marca_id` = `marca`.`marca_id`))) join `descuento` on((`local_articulo`.`local_articulo_descuento_id` = `descuento`.`descuento_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_medicados`
--
DROP TABLE IF EXISTS `v_articulo_medicados`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_medicados` AS select distinct `articulo`.`articulo_medicados` AS `id`,(case `articulo`.`articulo_medicados` when 0 then 'no' when 1 then 'si' end) AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((`articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) where (`articulo`.`articulo_medicados` is not null) order by `articulo`.`articulo_medicados`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_presentaciones`
--
DROP TABLE IF EXISTS `v_articulo_presentaciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_presentaciones` AS select distinct `presentacion`.`presentacion_id` AS `id`,`presentacion`.`presentacion_nombre` AS `nombre`,`presentacion`.`presentacion_orden` AS `orden`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from (((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_razas`
--
DROP TABLE IF EXISTS `v_articulo_razas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_razas` AS select distinct `raza`.`raza_id` AS `id`,`raza`.`raza_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `local` on((`local_articulo`.`local_articulo_local_id` = `local`.`local_id`))) join `articulo_raza` on((`articulo_raza`.`articulo_raza_articulo_id` = `articulo`.`articulo_id`))) join `raza` on((`articulo_raza`.`articulo_raza_raza_id` = `raza`.`raza_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_rubros`
--
DROP TABLE IF EXISTS `v_articulo_rubros`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_rubros` AS select distinct `rubro`.`rubro_id` AS `id`,`rubro`.`rubro_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id`,`articulo`.`articulo_id` AS `articulo_id` from (((`rubro` join `articulo` on((`rubro`.`rubro_id` = `articulo`.`articulo_rubro_id`))) join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_servicios`
--
DROP TABLE IF EXISTS `v_articulo_servicios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_servicios` AS select distinct `servicio`.`servicio_id` AS `id`,`servicio`.`servicio_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_servicio_item_id` AS `servicio_item`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_servicios_animal`
--
DROP TABLE IF EXISTS `v_articulo_servicios_animal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_servicios_animal` AS select distinct `servicio`.`servicio_id` AS `id`,`servicio`.`servicio_nombre` AS `nombre`,`servicio_item`.`servicio_item_animal_id` AS `animal_id`,`local_servicio_item`.`local_servicio_item_servicio_item_id` AS `servicio_item`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_articulo_tamanios`
--
DROP TABLE IF EXISTS `v_articulo_tamanios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_articulo_tamanios` AS select distinct `tamanio`.`tamanio_id` AS `id`,`tamanio`.`tamanio_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `articulo_tamanio` on((`articulo_tamanio`.`articulo_tamanio_articulo_id` = `articulo`.`articulo_id`))) join `tamanio` on((`articulo_tamanio`.`articulo_tamanio_tamanio_id` = `tamanio`.`tamanio_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_cliente_local`
--
DROP TABLE IF EXISTS `v_cliente_local`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_cliente_local` AS select 'cliente' AS `tipo`,`cliente`.`cliente_id` AS `id`,`cliente`.`cliente_nombre` AS `nombre`,`cliente`.`cliente_apellido` AS `apellido`,`cliente`.`cliente_email` AS `email`,`cliente`.`cliente_password` AS `password`,`cliente`.`cliente_status` AS `status`,`cliente`.`cliente_avatar` AS `avatar`,`cliente`.`cliente_cookie` AS `cookie`,`cliente`.`cliente_admin` AS `admin`,0 AS `destacado`,`cliente`.`cliente_usuario_baja` AS `usuario_baja` from `cliente` union select 'local' AS `tipo`,`local`.`local_id` AS `id`,`local`.`local_nombre` AS `nombre`,`local`.`local_nombre` AS `apellido`,`local`.`local_email` AS `email`,`local`.`local_password` AS `password`,`local`.`local_status` AS `status`,`local`.`local_avatar` AS `avatar`,`local`.`local_cookie` AS `cookie`,0 AS `admin`,`local`.`local_destacado` AS `destacado`,`local`.`local_usuario_baja` AS `usuario_baja` from `local`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_animales`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_animales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_animales` AS select distinct `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from (`articulo` join `animal` on((`articulo`.`articulo_animal_id` = `animal`.`animal_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_edades`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_edades`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_edades` AS select distinct `articulo`.`articulo_edad` AS `id`,`articulo`.`articulo_edad` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from `articulo` where (`articulo`.`articulo_edad` <> '');

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_marcas`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_marcas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_marcas` AS select distinct `marca`.`marca_id` AS `id`,`marca`.`marca_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from ((`articulo` join `marca` on((`articulo`.`articulo_marca_id` = `marca`.`marca_id`))) join `marca_animal` on((`marca_animal`.`marca_animal_marca_id` = `marca`.`marca_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_medicados`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_medicados`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_medicados` AS select distinct `articulo`.`articulo_medicados` AS `id`,(case `articulo`.`articulo_medicados` when 0 then 'no' when 1 then 'si' end) AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from `articulo` where (`articulo`.`articulo_medicados` is not null) order by `articulo`.`articulo_medicados`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_presentaciones`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_presentaciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_presentaciones` AS select distinct `presentacion`.`presentacion_id` AS `id`,`presentacion`.`presentacion_nombre` AS `nombre`,`presentacion`.`presentacion_orden` AS `orden`,`articulo`.`articulo_id` AS `articulo_id` from ((`articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_razas`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_razas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_razas` AS select distinct `raza`.`raza_id` AS `id`,`raza`.`raza_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from ((`articulo` join `articulo_raza` on((`articulo_raza`.`articulo_raza_articulo_id` = `articulo`.`articulo_id`))) join `raza` on((`articulo_raza`.`articulo_raza_raza_id` = `raza`.`raza_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_rubros`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_rubros`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_rubros` AS select distinct `rubro`.`rubro_id` AS `id`,`rubro`.`rubro_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from (`rubro` join `articulo` on((`rubro`.`rubro_id` = `articulo`.`articulo_rubro_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_geotienda_articulo_tamanios`
--
DROP TABLE IF EXISTS `v_geotienda_articulo_tamanios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_geotienda_articulo_tamanios` AS select distinct `tamanio`.`tamanio_id` AS `id`,`tamanio`.`tamanio_nombre` AS `nombre`,`articulo`.`articulo_id` AS `articulo_id` from ((`articulo` join `articulo_tamanio` on((`articulo_tamanio`.`articulo_tamanio_articulo_id` = `articulo`.`articulo_id`))) join `tamanio` on((`articulo_tamanio`.`articulo_tamanio_tamanio_id` = `tamanio`.`tamanio_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_animales`
--
DROP TABLE IF EXISTS `v_local_animales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_animales` AS select distinct `animal`.`animal_id` AS `id`,`animal`.`animal_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `animal` on((`articulo`.`articulo_animal_id` = `animal`.`animal_id`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_categorias`
--
DROP TABLE IF EXISTS `v_local_categorias`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_categorias` AS select `local`.`local_id` AS `local_id`,`local`.`local_categoria` AS `nombre` from `local`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_edades`
--
DROP TABLE IF EXISTS `v_local_edades`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_edades` AS select distinct `articulo`.`articulo_edad` AS `id`,`articulo`.`articulo_edad` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((`articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1) and (`articulo`.`articulo_edad` <> ''));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_marcas`
--
DROP TABLE IF EXISTS `v_local_marcas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_marcas` AS select distinct `marca`.`marca_id` AS `id`,`marca`.`marca_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `marca` on((`articulo`.`articulo_marca_id` = `marca`.`marca_id`))) join `marca_animal` on((`marca_animal`.`marca_animal_marca_id` = `marca`.`marca_id`))) join `descuento` on((`local_articulo`.`local_articulo_descuento_id` = `descuento`.`descuento_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_medicados`
--
DROP TABLE IF EXISTS `v_local_medicados`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_medicados` AS select distinct `articulo`.`articulo_medicados` AS `id`,(case `articulo`.`articulo_medicados` when 0 then 'no' when 1 then 'si' end) AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((`articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) where ((`articulo`.`articulo_medicados` is not null) and (`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1)) order by `articulo`.`articulo_medicados`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_presentaciones`
--
DROP TABLE IF EXISTS `v_local_presentaciones`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_presentaciones` AS select distinct `presentacion`.`presentacion_id` AS `id`,`presentacion`.`presentacion_nombre` AS `nombre`,`presentacion`.`presentacion_orden` AS `orden`,`local_articulo`.`local_articulo_local_id` AS `local_id` from (((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_razas`
--
DROP TABLE IF EXISTS `v_local_razas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_razas` AS select distinct `raza`.`raza_id` AS `id`,`raza`.`raza_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `presentacion` on((`articulo_presentacion`.`articulo_presentacion_presentacion_id` = `presentacion`.`presentacion_id`))) join `local` on((`local_articulo`.`local_articulo_local_id` = `local`.`local_id`))) join `articulo_raza` on((`articulo_raza`.`articulo_raza_articulo_id` = `articulo`.`articulo_id`))) join `raza` on((`articulo_raza`.`articulo_raza_raza_id` = `raza`.`raza_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_rubros`
--
DROP TABLE IF EXISTS `v_local_rubros`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_rubros` AS select distinct `rubro`.`rubro_id` AS `id`,`rubro`.`rubro_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from (((`rubro` join `articulo` on((`rubro`.`rubro_id` = `articulo`.`articulo_rubro_id`))) join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `local_articulo` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_servicios`
--
DROP TABLE IF EXISTS `v_local_servicios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_servicios` AS select distinct `servicio`.`servicio_id` AS `id`,`servicio`.`servicio_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_servicios_animal`
--
DROP TABLE IF EXISTS `v_local_servicios_animal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_servicios_animal` AS select distinct `servicio`.`servicio_id` AS `id`,`servicio`.`servicio_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id`,`servicio_item`.`servicio_item_animal_id` AS `animal_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_servicio_items`
--
DROP TABLE IF EXISTS `v_local_servicio_items`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_servicio_items` AS select distinct `servicio_item`.`servicio_item_id` AS `id`,`servicio`.`servicio_id` AS `servicio_id`,`servicio_item`.`servicio_item_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_servicio_items_animal`
--
DROP TABLE IF EXISTS `v_local_servicio_items_animal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_servicio_items_animal` AS select distinct `servicio_item`.`servicio_item_id` AS `id`,`servicio`.`servicio_id` AS `servicio_id`,`servicio_item`.`servicio_item_nombre` AS `nombre`,`local_servicio_item`.`local_servicio_item_local_id` AS `local_id`,`servicio_item`.`servicio_item_animal_id` AS `animal_id` from ((`servicio` join `servicio_item` on((`servicio`.`servicio_id` = `servicio_item`.`servicio_item_servicio_id`))) join `local_servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) where (`local_servicio_item`.`local_servicio_item_status` = 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_local_tamanios`
--
DROP TABLE IF EXISTS `v_local_tamanios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_local_tamanios` AS select distinct `tamanio`.`tamanio_id` AS `id`,`tamanio`.`tamanio_nombre` AS `nombre`,`local_articulo`.`local_articulo_local_id` AS `local_id` from ((((`local_articulo` join `articulo_presentacion` on((`local_articulo`.`local_articulo_articulo_presentacion_id` = `articulo_presentacion`.`articulo_presentacion_id`))) join `articulo` on((`articulo_presentacion`.`articulo_presentacion_articulo_id` = `articulo`.`articulo_id`))) join `articulo_tamanio` on((`articulo_tamanio`.`articulo_tamanio_articulo_id` = `articulo`.`articulo_id`))) join `tamanio` on((`articulo_tamanio`.`articulo_tamanio_tamanio_id` = `tamanio`.`tamanio_id`))) where ((`local_articulo`.`local_articulo_estado` = 1) and (`local_articulo`.`local_articulo_estado_presentacion` = 1));

-- --------------------------------------------------------

--
-- Estructura para la vista `v_marca_animal`
--
DROP TABLE IF EXISTS `v_marca_animal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_marca_animal` AS select `marca`.`marca_id` AS `marca_id`,`marca`.`marca_nombre` AS `marca_nombre`,group_concat(`animal`.`animal_nombre` separator ', ') AS `marca_animales` from ((`marca` join `marca_animal` on((`marca`.`marca_id` = `marca_animal`.`marca_animal_marca_id`))) join `animal` on((`marca_animal`.`marca_animal_animal_id` = `animal`.`animal_id`))) group by `marca`.`marca_id`,`marca`.`marca_nombre`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_marca_rubro_animal`
--
DROP TABLE IF EXISTS `v_marca_rubro_animal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_marca_rubro_animal` AS select `marca`.`marca_id` AS `marca_id`,`marca`.`marca_nombre` AS `marca_nombre`,group_concat(distinct `rubro`.`rubro_nombre` order by `rubro`.`rubro_nombre` ASC separator ', ') AS `marca_rubros`,group_concat(distinct `animal`.`animal_nombre` order by `animal`.`animal_nombre` ASC separator ', ') AS `marca_animales` from ((((`marca` left join `marca_rubro` on((`marca`.`marca_id` = `marca_rubro`.`marca_rubro_marca_id`))) left join `rubro` on((`marca_rubro`.`marca_rubro_rubro_id` = `rubro`.`rubro_id`))) left join `marca_animal` on((`marca`.`marca_id` = `marca_animal`.`marca_animal_marca_id`))) left join `animal` on((`marca_animal`.`marca_animal_animal_id` = `animal`.`animal_id`))) group by `marca`.`marca_id`,`marca`.`marca_nombre`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_presentaciones_articulo_local`
--
DROP TABLE IF EXISTS `v_presentaciones_articulo_local`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_presentaciones_articulo_local` AS select `local_articulo`.`local_articulo_local_id` AS `local_id`,`articulo_presentacion`.`articulo_presentacion_articulo_id` AS `articulo_id`,count(`articulo_presentacion`.`articulo_presentacion_articulo_id`) AS `presentaciones_local` from (`local_articulo` join `articulo_presentacion` on((`articulo_presentacion`.`articulo_presentacion_id` = `local_articulo`.`local_articulo_articulo_presentacion_id`))) group by `local_articulo`.`local_articulo_local_id`,`articulo_presentacion`.`articulo_presentacion_articulo_id` order by `local_articulo`.`local_articulo_local_id`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_presentacion_rubro`
--
DROP TABLE IF EXISTS `v_presentacion_rubro`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_presentacion_rubro` AS select `presentacion`.`presentacion_id` AS `presentacion_id`,`presentacion`.`presentacion_nombre` AS `presentacion_nombre`,`presentacion`.`presentacion_orden` AS `presentacion_orden`,group_concat(distinct `rubro`.`rubro_nombre` order by `rubro`.`rubro_nombre` ASC separator ', ') AS `presentacion_rubros` from ((`presentacion` left join `presentacion_rubro` on((`presentacion`.`presentacion_id` = `presentacion_rubro`.`presentacion_rubro_presentacion_id`))) left join `rubro` on((`presentacion_rubro`.`presentacion_rubro_rubro_id` = `rubro`.`rubro_id`))) group by `presentacion`.`presentacion_id`,`presentacion`.`presentacion_nombre`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_servicios_local_animales`
--
DROP TABLE IF EXISTS `v_servicios_local_animales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_servicios_local_animales` AS select `animal`.`animal_nombre` AS `nombre`,`servicio_item`.`servicio_item_id` AS `servicio_item_id`,`servicio_item`.`servicio_item_nombre` AS `servicio_item_nombre`,`local`.`local_id` AS `local_id` from (((`local_servicio_item` join `servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) join `animal` on((`servicio_item`.`servicio_item_animal_id` = `animal`.`animal_id`))) join `local` on((`local_servicio_item`.`local_servicio_item_local_id` = `local`.`local_id`))) group by `animal`.`animal_nombre`,`servicio_item`.`servicio_item_id`,`servicio_item`.`servicio_item_nombre`,`local`.`local_id`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_servicios_local_servicios`
--
DROP TABLE IF EXISTS `v_servicios_local_servicios`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_servicios_local_servicios` AS select `servicio`.`servicio_nombre` AS `nombre`,`servicio_item`.`servicio_item_id` AS `servicio_item_id`,`servicio_item`.`servicio_item_nombre` AS `servicio_item_nombre`,`local`.`local_id` AS `local_id` from (((`local_servicio_item` join `servicio_item` on((`local_servicio_item`.`local_servicio_item_servicio_item_id` = `servicio_item`.`servicio_item_id`))) join `servicio` on((`servicio_item`.`servicio_item_servicio_id` = `servicio`.`servicio_id`))) join `local` on((`local_servicio_item`.`local_servicio_item_local_id` = `local`.`local_id`))) group by `servicio`.`servicio_nombre`,`servicio_item`.`servicio_item_id`,`servicio_item`.`servicio_item_nombre`,`local`.`local_id`;

-- --------------------------------------------------------

--
-- Estructura para la vista `v_venta_turno`
--
DROP TABLE IF EXISTS `v_venta_turno`;

CREATE ALGORITHM=UNDEFINED DEFINER=`rapipets`@`localhost` SQL SECURITY DEFINER VIEW `v_venta_turno` AS select 'venta' AS `tipo`,`venta`.`venta_id` AS `id`,`venta`.`venta_fecha` AS `fecha_creacion`,concat('P',lpad(`venta`.`venta_id`,4,'0')) AS `numero`,date_format(`venta`.`venta_fecha`,'%Y-%m-%d') AS `fecha`,`venta`.`venta_mes` AS `mes`,`venta`.`venta_cliente_id` AS `cliente_id`,`venta`.`venta_cliente_nombre` AS `cliente`,`cliente`.`cliente_email` AS `cliente_email`,`local`.`local_id` AS `local_id`,`local`.`local_nombre` AS `local`,`venta`.`venta_total` AS `total`,`venta`.`venta_estado` AS `estado`,`venta`.`venta_condicion_venta` AS `forma_pago`,`venta`.`venta_concretado_cliente` AS `concretado_cliente`,`venta`.`venta_puntaje_cliente` AS `puntaje_cliente`,`venta`.`venta_valoracion_cliente` AS `valoracion_cliente`,`venta`.`venta_valoracion_aprobada` AS `valoracion_aprobada`,`venta`.`venta_concretado_local` AS `concretado_local`,`venta`.`venta_valoracion_local` AS `valoracion_local`,`venta`.`venta_cancelado_por` AS `cancelado_por`,`venta`.`venta_cancelado_comentario` AS `cancelado_comentario` from ((`venta` join `local` on((`local`.`local_id` = `venta`.`venta_local_id`))) join `cliente` on((`cliente`.`cliente_id` = `venta`.`venta_cliente_id`))) union select 'turno' AS `tipo`,`turno`.`turno_id` AS `id`,`turno`.`turno_fecha_creacion` AS `fecha_creacion`,concat('S',lpad(`turno`.`turno_id`,4,'0')) AS `numero`,`turno`.`turno_fecha` AS `fecha`,`turno`.`turno_mes` AS `mes`,`cliente`.`cliente_id` AS `cliente_id`,concat(`cliente`.`cliente_nombre`,' ',`cliente`.`cliente_apellido`) AS `cliente`,`cliente`.`cliente_email` AS `cliente_email`,`local`.`local_id` AS `local_id`,`local`.`local_nombre` AS `local`,`turno`.`turno_precio` AS `total`,`turno`.`turno_estado` AS `estado`,'Efectivo' AS `forma_pago`,`turno`.`turno_concretado_cliente` AS `concretado_cliente`,`turno`.`turno_puntaje_cliente` AS `puntaje_cliente`,`turno`.`turno_valoracion_cliente` AS `valoracion_cliente`,`turno`.`turno_valoracion_aprobada` AS `valoracion_aprobada`,`turno`.`turno_concretado_local` AS `concretado_local`,`turno`.`turno_valoracion_local` AS `valoracion_local`,`turno`.`turno_cancelado_por` AS `cancelado_por`,`turno`.`turno_cancelado_comentario` AS `cancelado_comentario` from ((`turno` join `local` on((`local`.`local_id` = `turno`.`turno_local_id`))) join `cliente` on((`cliente`.`cliente_id` = `turno`.`turno_cliente_id`))) order by `fecha` desc;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `animal`
--
ALTER TABLE `animal`
  ADD PRIMARY KEY (`animal_id`);

--
-- Indices de la tabla `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`articulo_id`), ADD KEY `articulo_animal_id` (`articulo_animal_id`), ADD KEY `articulo_marca_id` (`articulo_marca_id`), ADD KEY `articulo_rubro_id` (`articulo_rubro_id`), ADD KEY `articulo_rubro_id_2` (`articulo_rubro_id`);

--
-- Indices de la tabla `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
  ADD PRIMARY KEY (`articulo_presentacion_id`), ADD KEY `articulo_presentacion_presentacion_id` (`articulo_presentacion_presentacion_id`);

--
-- Indices de la tabla `articulo_raza`
--
ALTER TABLE `articulo_raza`
  ADD PRIMARY KEY (`articulo_raza_id`), ADD KEY `articulo_raza_raza_id` (`articulo_raza_raza_id`);

--
-- Indices de la tabla `articulo_tamanio`
--
ALTER TABLE `articulo_tamanio`
  ADD PRIMARY KEY (`articulo_tamanio_id`), ADD KEY `articulo_tamanio_tamanio_id` (`articulo_tamanio_tamanio_id`);

--
-- Indices de la tabla `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`banner_id`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`cliente_id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`configuracion_id`);

--
-- Indices de la tabla `descuento`
--
ALTER TABLE `descuento`
  ADD PRIMARY KEY (`descuento_id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`local_id`), ADD KEY `local_provincia_id` (`local_provincia_id`), ADD KEY `banner_id` (`local_banner_id`);

--
-- Indices de la tabla `local_articulo`
--
ALTER TABLE `local_articulo`
  ADD PRIMARY KEY (`local_articulo_id`), ADD KEY `local_articulo_local_id` (`local_articulo_local_id`), ADD KEY `local_articulo_presentacion_id` (`local_articulo_articulo_presentacion_id`), ADD KEY `local_articulo_descuento_id` (`local_articulo_descuento_id`), ADD KEY `local_articulo_presentacion_id_2` (`local_articulo_articulo_presentacion_id`);

--
-- Indices de la tabla `local_horario`
--
ALTER TABLE `local_horario`
  ADD PRIMARY KEY (`local_horario_id`), ADD UNIQUE KEY `local_horario_local_id_idx` (`local_horario_local_id`);

--
-- Indices de la tabla `local_medio_pago`
--
ALTER TABLE `local_medio_pago`
  ADD PRIMARY KEY (`local_medio_pago_id`), ADD KEY `local_medio_pago_medio_pago_item_id_idx` (`local_medio_pago_medio_pago_item_id`), ADD KEY `local_medio_pago_medio_pago_id_idx` (`local_medio_pago_medio_pago_id`);

--
-- Indices de la tabla `local_servicio_item`
--
ALTER TABLE `local_servicio_item`
  ADD PRIMARY KEY (`local_servicio_item_id`), ADD UNIQUE KEY `local_servicio_item_idx` (`local_servicio_item_local_id`,`local_servicio_item_servicio_item_id`), ADD KEY `local_servicio_item_servicio_item_id_fk` (`local_servicio_item_servicio_item_id`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`marca_id`);

--
-- Indices de la tabla `marca_animal`
--
ALTER TABLE `marca_animal`
  ADD PRIMARY KEY (`marca_animal_id`), ADD KEY `marca_animal_animal_id` (`marca_animal_animal_id`), ADD KEY `marca_animal_marca_id` (`marca_animal_marca_id`), ADD KEY `marca_animal_animal_id_2` (`marca_animal_animal_id`);

--
-- Indices de la tabla `marca_rubro`
--
ALTER TABLE `marca_rubro`
  ADD PRIMARY KEY (`marca_rubro_id`);

--
-- Indices de la tabla `mascota`
--
ALTER TABLE `mascota`
  ADD PRIMARY KEY (`mascota_id`);

--
-- Indices de la tabla `medio_pago`
--
ALTER TABLE `medio_pago`
  ADD PRIMARY KEY (`medio_pago_id`);

--
-- Indices de la tabla `medio_pago_item`
--
ALTER TABLE `medio_pago_item`
  ADD PRIMARY KEY (`medio_pago_item_id`), ADD KEY `medio_pago_item_medio_pago_id_idx` (`medio_pago_item_medio_pago_id`);

--
-- Indices de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  ADD PRIMARY KEY (`notificacion_id`);

--
-- Indices de la tabla `notificacion_cliente`
--
ALTER TABLE `notificacion_cliente`
  ADD PRIMARY KEY (`notificacion_cliente_id`);

--
-- Indices de la tabla `notificacion_local`
--
ALTER TABLE `notificacion_local`
  ADD PRIMARY KEY (`notificacion_local_id`);

--
-- Indices de la tabla `presentacion`
--
ALTER TABLE `presentacion`
  ADD PRIMARY KEY (`presentacion_id`);

--
-- Indices de la tabla `presentacion_rubro`
--
ALTER TABLE `presentacion_rubro`
  ADD PRIMARY KEY (`presentacion_rubro_id`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
  ADD PRIMARY KEY (`provincia_id`);

--
-- Indices de la tabla `raza`
--
ALTER TABLE `raza`
  ADD PRIMARY KEY (`raza_id`), ADD KEY `raza_id_animal` (`raza_id_animal`);

--
-- Indices de la tabla `recordatorio`
--
ALTER TABLE `recordatorio`
  ADD PRIMARY KEY (`recordatorio_id`);

--
-- Indices de la tabla `reset`
--
ALTER TABLE `reset`
  ADD PRIMARY KEY (`reset_id`), ADD UNIQUE KEY `reset_usuario_id` (`reset_usuario_id`);

--
-- Indices de la tabla `rubro`
--
ALTER TABLE `rubro`
  ADD PRIMARY KEY (`rubro_id`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`servicio_id`);

--
-- Indices de la tabla `servicio_item`
--
ALTER TABLE `servicio_item`
  ADD PRIMARY KEY (`servicio_item_id`), ADD KEY `servicio_item_servicio_id` (`servicio_item_servicio_id`);

--
-- Indices de la tabla `tamanio`
--
ALTER TABLE `tamanio`
  ADD PRIMARY KEY (`tamanio_id`), ADD KEY `tamanio_animal_id` (`tamanio_animal_id`);

--
-- Indices de la tabla `turno`
--
ALTER TABLE `turno`
  ADD PRIMARY KEY (`turno_id`), ADD KEY `turno_local_id` (`turno_local_id`), ADD KEY `turno_cliente_id` (`turno_cliente_id`), ADD KEY `turno_servicio_item_id` (`turno_servicio_item_id`);

--
-- Indices de la tabla `turno_comentario`
--
ALTER TABLE `turno_comentario`
  ADD PRIMARY KEY (`turno_comentario_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vacuna`
--
ALTER TABLE `vacuna`
  ADD PRIMARY KEY (`vacuna_id`);

--
-- Indices de la tabla `venta`
--
ALTER TABLE `venta`
  ADD PRIMARY KEY (`venta_id`), ADD KEY `venta_local_id` (`venta_local_id`), ADD KEY `venta_cliente_id` (`venta_cliente_id`);

--
-- Indices de la tabla `venta_comentario`
--
ALTER TABLE `venta_comentario`
  ADD PRIMARY KEY (`venta_comentario_id`);

--
-- Indices de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  ADD PRIMARY KEY (`venta_detalle_id`), ADD UNIQUE KEY `venta_detalle_idx` (`venta_detalle_venta_id`,`venta_detalle_articulo_id`,`venta_detalle_presentacion_id`), ADD KEY `venta_detalle_articulo_id_fk` (`venta_detalle_articulo_id`), ADD KEY `venta_detalle_presentacion_id_fk` (`venta_detalle_presentacion_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `animal`
--
ALTER TABLE `animal`
  MODIFY `animal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `articulo`
--
ALTER TABLE `articulo`
  MODIFY `articulo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=655;
--
-- AUTO_INCREMENT de la tabla `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
  MODIFY `articulo_presentacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3969;
--
-- AUTO_INCREMENT de la tabla `articulo_raza`
--
ALTER TABLE `articulo_raza`
  MODIFY `articulo_raza_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=739;
--
-- AUTO_INCREMENT de la tabla `articulo_tamanio`
--
ALTER TABLE `articulo_tamanio`
  MODIFY `articulo_tamanio_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1517;
--
-- AUTO_INCREMENT de la tabla `banner`
--
ALTER TABLE `banner`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `cliente_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `configuracion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `descuento`
--
ALTER TABLE `descuento`
  MODIFY `descuento_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
  MODIFY `local_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT de la tabla `local_articulo`
--
ALTER TABLE `local_articulo`
  MODIFY `local_articulo_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1289;
--
-- AUTO_INCREMENT de la tabla `local_horario`
--
ALTER TABLE `local_horario`
  MODIFY `local_horario_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `local_medio_pago`
--
ALTER TABLE `local_medio_pago`
  MODIFY `local_medio_pago_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=601;
--
-- AUTO_INCREMENT de la tabla `local_servicio_item`
--
ALTER TABLE `local_servicio_item`
  MODIFY `local_servicio_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=292;
--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `marca_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT de la tabla `marca_animal`
--
ALTER TABLE `marca_animal`
  MODIFY `marca_animal_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=245;
--
-- AUTO_INCREMENT de la tabla `marca_rubro`
--
ALTER TABLE `marca_rubro`
  MODIFY `marca_rubro_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT de la tabla `mascota`
--
ALTER TABLE `mascota`
  MODIFY `mascota_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT de la tabla `medio_pago`
--
ALTER TABLE `medio_pago`
  MODIFY `medio_pago_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT de la tabla `medio_pago_item`
--
ALTER TABLE `medio_pago_item`
  MODIFY `medio_pago_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT de la tabla `notificacion`
--
ALTER TABLE `notificacion`
  MODIFY `notificacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=353;
--
-- AUTO_INCREMENT de la tabla `notificacion_cliente`
--
ALTER TABLE `notificacion_cliente`
  MODIFY `notificacion_cliente_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `notificacion_local`
--
ALTER TABLE `notificacion_local`
  MODIFY `notificacion_local_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=252;
--
-- AUTO_INCREMENT de la tabla `presentacion`
--
ALTER TABLE `presentacion`
  MODIFY `presentacion_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=138;
--
-- AUTO_INCREMENT de la tabla `presentacion_rubro`
--
ALTER TABLE `presentacion_rubro`
  MODIFY `presentacion_rubro_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT de la tabla `provincia`
--
ALTER TABLE `provincia`
  MODIFY `provincia_id` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `raza`
--
ALTER TABLE `raza`
  MODIFY `raza_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT de la tabla `recordatorio`
--
ALTER TABLE `recordatorio`
  MODIFY `recordatorio_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=416;
--
-- AUTO_INCREMENT de la tabla `reset`
--
ALTER TABLE `reset`
  MODIFY `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `rubro`
--
ALTER TABLE `rubro`
  MODIFY `rubro_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `servicio_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `servicio_item`
--
ALTER TABLE `servicio_item`
  MODIFY `servicio_item_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=80;
--
-- AUTO_INCREMENT de la tabla `tamanio`
--
ALTER TABLE `tamanio`
  MODIFY `tamanio_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `turno`
--
ALTER TABLE `turno`
  MODIFY `turno_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=176;
--
-- AUTO_INCREMENT de la tabla `turno_comentario`
--
ALTER TABLE `turno_comentario`
  MODIFY `turno_comentario_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `vacuna`
--
ALTER TABLE `vacuna`
  MODIFY `vacuna_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT de la tabla `venta`
--
ALTER TABLE `venta`
  MODIFY `venta_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=350;
--
-- AUTO_INCREMENT de la tabla `venta_comentario`
--
ALTER TABLE `venta_comentario`
  MODIFY `venta_comentario_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=166;
--
-- AUTO_INCREMENT de la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
  MODIFY `venta_detalle_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=391;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `articulo`
--
ALTER TABLE `articulo`
ADD CONSTRAINT `articulo_animal_fk` FOREIGN KEY (`articulo_animal_id`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `articulo_marca_fk` FOREIGN KEY (`articulo_marca_id`) REFERENCES `marca` (`marca_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `articulo_rubro_fk` FOREIGN KEY (`articulo_rubro_id`) REFERENCES `rubro` (`rubro_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
ADD CONSTRAINT `presentacion_id_fk` FOREIGN KEY (`articulo_presentacion_presentacion_id`) REFERENCES `presentacion` (`presentacion_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `articulo_raza`
--
ALTER TABLE `articulo_raza`
ADD CONSTRAINT `raza_id_fk` FOREIGN KEY (`articulo_raza_raza_id`) REFERENCES `raza` (`raza_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `articulo_tamanio`
--
ALTER TABLE `articulo_tamanio`
ADD CONSTRAINT `tamanio_id_fk` FOREIGN KEY (`articulo_tamanio_tamanio_id`) REFERENCES `tamanio` (`tamanio_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `local`
--
ALTER TABLE `local`
ADD CONSTRAINT `banner_id` FOREIGN KEY (`local_banner_id`) REFERENCES `banner` (`banner_id`),
ADD CONSTRAINT `provincia_id` FOREIGN KEY (`local_provincia_id`) REFERENCES `provincia` (`provincia_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `local_articulo`
--
ALTER TABLE `local_articulo`
ADD CONSTRAINT `articulo_presentacion_id_fk` FOREIGN KEY (`local_articulo_articulo_presentacion_id`) REFERENCES `articulo_presentacion` (`articulo_presentacion_id`),
ADD CONSTRAINT `descuento_id_fk` FOREIGN KEY (`local_articulo_descuento_id`) REFERENCES `descuento` (`descuento_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `local_id_fk` FOREIGN KEY (`local_articulo_local_id`) REFERENCES `local` (`local_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `local_horario`
--
ALTER TABLE `local_horario`
ADD CONSTRAINT `local_horario_local_id_fk` FOREIGN KEY (`local_horario_local_id`) REFERENCES `local` (`local_id`);

--
-- Filtros para la tabla `local_medio_pago`
--
ALTER TABLE `local_medio_pago`
ADD CONSTRAINT `local_medio_pago_ibfk_1` FOREIGN KEY (`local_medio_pago_medio_pago_id`) REFERENCES `medio_pago` (`medio_pago_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `local_servicio_item`
--
ALTER TABLE `local_servicio_item`
ADD CONSTRAINT `local_servicio_item_local_id_fk` FOREIGN KEY (`local_servicio_item_local_id`) REFERENCES `local` (`local_id`),
ADD CONSTRAINT `local_servicio_item_servicio_item_id_fk` FOREIGN KEY (`local_servicio_item_servicio_item_id`) REFERENCES `servicio_item` (`servicio_item_id`);

--
-- Filtros para la tabla `marca_animal`
--
ALTER TABLE `marca_animal`
ADD CONSTRAINT `animal_id_fk` FOREIGN KEY (`marca_animal_animal_id`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `medio_pago_item`
--
ALTER TABLE `medio_pago_item`
ADD CONSTRAINT `medio_pago_id` FOREIGN KEY (`medio_pago_item_medio_pago_id`) REFERENCES `medio_pago` (`medio_pago_id`);

--
-- Filtros para la tabla `raza`
--
ALTER TABLE `raza`
ADD CONSTRAINT `raza_id_animal_fk` FOREIGN KEY (`raza_id_animal`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicio_item`
--
ALTER TABLE `servicio_item`
ADD CONSTRAINT `servicio_id` FOREIGN KEY (`servicio_item_servicio_id`) REFERENCES `servicio` (`servicio_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tamanio`
--
ALTER TABLE `tamanio`
ADD CONSTRAINT `tamanio_animal_fk` FOREIGN KEY (`tamanio_animal_id`) REFERENCES `animal` (`animal_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `turno`
--
ALTER TABLE `turno`
ADD CONSTRAINT `servicio_item_id_fk` FOREIGN KEY (`turno_servicio_item_id`) REFERENCES `servicio_item` (`servicio_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `turno_cliente_id_fk` FOREIGN KEY (`turno_cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `turno_local_id_fk` FOREIGN KEY (`turno_local_id`) REFERENCES `local` (`local_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta`
--
ALTER TABLE `venta`
ADD CONSTRAINT `venta_cliente_id_fk` FOREIGN KEY (`venta_cliente_id`) REFERENCES `cliente` (`cliente_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `venta_local_id_fk` FOREIGN KEY (`venta_local_id`) REFERENCES `local` (`local_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `venta_detalle`
--
ALTER TABLE `venta_detalle`
ADD CONSTRAINT `venta_detalle_articulo_id_fk` FOREIGN KEY (`venta_detalle_articulo_id`) REFERENCES `articulo` (`articulo_id`),
ADD CONSTRAINT `venta_detalle_presentacion_id_fk` FOREIGN KEY (`venta_detalle_presentacion_id`) REFERENCES `presentacion` (`presentacion_id`),
ADD CONSTRAINT `venta_detalle_venta_id_fk` FOREIGN KEY (`venta_detalle_venta_id`) REFERENCES `venta` (`venta_id`);

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`rapipets`@`localhost` EVENT `borrar_resets_vencidos` ON SCHEDULE AT '2016-09-19 18:15:41' ON COMPLETION NOT PRESERVE ENABLE DO DELETE FROM reset WHERE reset_creado <= DATE_SUB(CURTIME(), INTERVAL 2 DAY)$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
