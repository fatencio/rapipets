// Articulos con presentaciones repetidas
SELECT articulo_presentacion_id, articulo_presentacion_articulo_id, count(articulo_presentacion_presentacion_id) as repetidas FROM `articulo_presentacion` GROUP BY articulo_presentacion_articulo_id, articulo_presentacion_presentacion_id HAVING count(articulo_presentacion_presentacion_id) > 1 ORDER BY articulo_presentacion_articulo_id

// Articulos en locales con presentaciones repetidas
SELECT * FROM local_articulo where local_articulo_articulo_presentacion_id in (SELECT articulo_presentacion_id FROM `articulo_presentacion` GROUP BY articulo_presentacion_articulo_id, articulo_presentacion_presentacion_id HAVING count(articulo_presentacion_presentacion_id) > 1) ORDER BY `local_articulo_local_id` ASC

// Detalles de un articulo por su presentacion_id
SELECT * FROM `articulo` where articulo_id in (select articulo_presentacion_articulo_id from articulo_presentacion where articulo_presentacion_id = 224) ORDER BY `articulo_id` DESC

// Marcas por animal
SELECT * FROM marca INNER JOIN marca_animal ON marca_id = marca_animal_marca_id INNER JOIN animal ON animal_id = marca_animal_animal_id WHERE animal_nombre = 'Gato' ORDER BY marca_nombre

// Marcas de productos activos de un local
SELECT marca_id AS id, marca_nombre AS nombre, animal_nombre AS animal FROM `marca` JOIN `marca_animal` ON `marca_id` = `marca_animal_marca_id` JOIN `animal` ON `marca_animal_animal_id` = `animal_id` JOIN `articulo` ON `articulo_marca_id` = `marca_id` JOIN `articulo_presentacion` ON `articulo_presentacion_articulo_id` = `articulo_id` JOIN `local_articulo` ON `local_articulo_articulo_presentacion_id` = `articulo_presentacion_id` WHERE `local_articulo_local_id` = '25' GROUP BY `marca_id`, `marca_nombre`, animal_nombre ORDER BY animal_nombre, `marca_nombre`