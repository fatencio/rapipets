<?php 
	session_start(); 

	// Variables de sesion FRONTEND
	if (isset($_SESSION['token'])) $_SESSION['token'] = $_SESSION['token'];
	if (isset($_SESSION['frontend_logged_in'])) $_SESSION['frontend_logged_in'] = $_SESSION['frontend_logged_in'];
	if (isset($_SESSION['frontend_user_id'])) $_SESSION['frontend_user_id'] = $_SESSION['frontend_user_id'];
	if (isset($_SESSION['frontend_email'])) $_SESSION['frontend_email'] = $_SESSION['frontend_email'];
	if (isset($_SESSION['frontend_nombre'])) $_SESSION['frontend_nombre'] = $_SESSION['frontend_nombre'];
	if (isset($_SESSION['frontend_apellido'])) $_SESSION['frontend_apellido'] = $_SESSION['frontend_apellido'];
	if (isset($_SESSION['frontend_avatar'])) $_SESSION['frontend_avatar'] = $_SESSION['frontend_avatar'];
	if (isset($_SESSION['frontend_tipo'])) $_SESSION['frontend_tipo'] = $_SESSION['frontend_tipo'];
	if (isset($_SESSION['frontend_admin'])) $_SESSION['frontend_admin'] = $_SESSION['frontend_admin'];

	// Variables de sesion BACKEND
	if (isset($_SESSION['user_id'])) $_SESSION['user_id'] = $_SESSION['user_id'];
	if (isset($_SESSION['username'])) $_SESSION['username'] = $_SESSION['username'];
	if (isset($_SESSION['logged_in'])) $_SESSION['logged_in'] = $_SESSION['logged_in'];
	if (isset($_SESSION['is_confirmed'])) $_SESSION['is_confirmed'] = $_SESSION['is_confirmed'];
	if (isset($_SESSION['is_admin'])) $_SESSION['is_admin'] = $_SESSION['is_admin'];
?>