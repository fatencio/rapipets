<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media_model extends CI_Model {


	public function __construct()
	{
		parent::__construct();
	}

	// Verifica la extension del archivo
	public function check_image_extension($file_name){

		$valid = false;

		// Extensiones permitidas
		$limitedext = array(".gif",".jpg",".png",".jpeg");		
		
		$ext = strrchr($file_name,'.');
		$ext = strtolower($ext);

		if (in_array($ext,$limitedext)) {
			$valid = true;
		}		

		return $valid;
	}

	public function resize_image_big_thumb($imagen, $img_thumb_width, $img_big_width, $destino, $prefijo=''){

		// Carpeta de imagenes grandes y miniaturas (ambas deben tener permiso de escritura)
		$path_thumbs = IMG_FILE_PATH . $destino . "/miniaturas";	
		$path_big = IMG_FILE_PATH . $destino . "/fotos";		


		//LA IMAGEN -> variables
		$file_type = $imagen['type'];
		$file_name = $imagen['name'];
		$file_size = $imagen['size'];
		$file_tmp = $imagen['tmp_name'];


		//obtiene la extension del archivo
		$getExt = explode ('.', $file_name);
		$file_ext = $getExt[count($getExt)-1];

		//crea un nombre aleatorio para el archivo
		$rand_name = md5(time());
		$rand_name= rand(0,999999999);

		//agrega prefijo
		$rand_name= $prefijo . '_' . $rand_name;

		// Mantiene el tipo de imagen
		if($file_type == "image/pjpeg" || $file_type == "image/jpeg"){
			$new_img = imagecreatefromjpeg($file_tmp);
		}elseif($file_type == "image/x-png" || $file_type == "image/png"){
			$new_img = imagecreatefrompng($file_tmp);
		}elseif($file_type == "image/gif"){
			$new_img = imagecreatefromgif($file_tmp);
		}

		// Obtiene tamaño original
		list($width, $height) = getimagesize($file_tmp);

		// Calcula proporcion alto / ancho
		$imgratio = $width/$height;

		if ($imgratio>1){
			$newwidth = $img_thumb_width;
			$newwidthBig = $img_big_width;
			$newheight = $img_thumb_width/$imgratio;
			$newheightBig = $img_big_width/$imgratio;
		}else{
			$newheight = $img_thumb_width;
			$newheightBig = $img_big_width;
			$newwidth = $img_thumb_width*$imgratio;
			$newwidthBig = $img_big_width*$imgratio;
		}

		// Redimensiona
		$resized_img = imagecreatetruecolor($newwidth,$newheight);
		$resized_img_big = imagecreatetruecolor($newwidthBig,$newheightBig);

		imagecopyresized($resized_img, $new_img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		imagecopyresized($resized_img_big, $new_img, 0, 0, 0, 0, $newwidthBig, $newheightBig, $width, $height);

		// Guarda la imagen
		
		ImageJpeg ($resized_img,"$path_thumbs/$rand_name.$file_ext");
		//imagejpeg($dst_img,$destPath.$name);  
		ImageJpeg ($resized_img_big,"$path_big/$rand_name.$file_ext");
		ImageDestroy ($resized_img);
		ImageDestroy ($resized_img_big);
		ImageDestroy ($new_img);
		

		// Retorna el nombre del nuevo archivo creado
		return "$rand_name.$file_ext";

	}


	public function resize_imagen($imagen, $img_width, $destino, $prefijo = ''){

		// Carpeta destino de las imagenes (debe tener permiso de escritura)
		$path_portada = IMG_FILE_PATH . $destino ;		

		//LA IMAGEN -> variables
		$file_type = $imagen['type'];
		$file_name = $imagen['name'];
		$file_size = $imagen['size'];
		$file_tmp = $imagen['tmp_name'];

		//obtiene la extension del archivo
		$getExt = explode ('.', $file_name);
		$file_ext = $getExt[count($getExt)-1];

        //crea un nombre aleatorio para el archivo
		$rand_name = md5(time());
		$rand_name= rand(0,999999999);

		//agrega prefijo
		if($prefijo != '')
			$nombreimagen= $prefijo . '_' . $rand_name;
		else
			$nombreimagen= $rand_name;

		// Mantiene el tipo de imagen
		if($file_type == "image/pjpeg" || $file_type == "image/jpeg"){
			$new_img = imagecreatefromjpeg($file_tmp);
		}elseif($file_type == "image/x-png" || $file_type == "image/png"){
			$new_img = imagecreatefrompng($file_tmp);
		}elseif($file_type == "image/gif"){
			$new_img = imagecreatefromgif($file_tmp);
		}

		// Obtiene tamaño original
		list($width, $height) = getimagesize($file_tmp);

		// Calcula proporcion alto / ancho
		$imgratio = $width/$height;

		if ($imgratio>1){
			
			$newwidth = $img_width;
			$newheight = $img_width/$imgratio;

		}else{
			
			$newheight = $img_width;
            $newwidth = $img_width*$imgratio;
		}

		// Redimensiona
		$resized_img = imagecreatetruecolor($newwidth,$newheight);


		imagecopyresized($resized_img, $new_img, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
		

		// Guarda la imagen		
		ImageJpeg ($resized_img,"$path_portada/$nombreimagen.$file_ext");
		ImageDestroy ($resized_img);		
		ImageDestroy ($new_img);
		
		// Retorna el nombre del nuevo archivo creado
		return "$nombreimagen.$file_ext";
	}


	public function delete_image_big_thumb($imagen, $destino){

		$retorno = false;

		// Carpeta de imagenes grandes y miniaturas (ambas deben tener permiso de escritura)
		$path_thumbs = IMG_FILE_PATH . $destino . "/miniaturas/" . $imagen;	
		$path_big = IMG_FILE_PATH . $destino . "/fotos/" . $imagen;	


		if(unlink($path_thumbs) && unlink($path_big)) {
		     $retorno = true;
		}

		return $retorno;
	}


	public function delete_image($imagen, $destino){

		$retorno = false;

		$path = IMG_FILE_PATH . $destino . "/" . $imagen;	

		if(unlink($path)) {
		     $retorno = true;
		}

		return $retorno;
	}	
}