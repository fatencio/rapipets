<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend_model extends CI_Model
{
	var $table = 'v_cliente_local';
	var $table_reset = 'reset';

	public function __construct() 
	{	
		parent::__construct();
		$this->load->database();		
	}
	

	public function resolve_user_login($email, $password) 
	{
		$this->db->select('password');
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$this->db->where('usuario_baja', '');
		$this->db->where('status', 1);

		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password, $hash);
		
	}
			

	public function resolve_user_status($email) 
	{		
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$this->db->where('status', 1);
		$this->db->where('usuario_baja', '');

		return $this->db->count_all_results() > 0? true:false;
	}	


	public function resolve_user_cookie($email, $cookie) 
	{		
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$this->db->where('cookie', $cookie);
		$this->db->where('usuario_baja', '');

		return $this->db->count_all_results() > 0? true:false;
	}	


	public function get_user_from_email($email) 
	{		
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$this->db->where('usuario_baja', '');

		return $this->db->get()->row();
		
	}
	

	public function get_user_id_from_email($email) 
	{
		$this->db->select('id');
		$this->db->from($this->table);
		$this->db->where('email', $email);
		$this->db->where('usuario_baja', '');

		return $this->db->get()->row('id');
	}
	

	public function get_user($user_id) 
	{
		
		$this->db->from($this->table);
		$this->db->where('id', $user_id);

		return $this->db->get()->row();
	}
	

	private function hash_password($password) 
	{	
		return password_hash($password, PASSWORD_BCRYPT);	
	}
	

	private function verify_password_hash($password, $hash) 
	{
		return password_verify($password, $hash);	
	}


	public function validate_password($user_id, $password_actual) 
	{		
		$this->db->select('password');
		$this->db->from($this->table);
		$this->db->where('id', $user_id);
		$hash = $this->db->get()->row('password');
		
		return $this->verify_password_hash($password_actual, $hash);	
	}	
	

	public function check_duplicated($email)
	{
		$this->db->from($this->table);
		$this->db->where('email', $email);

		// Filtra eliminados
		$this->db->where('usuario_baja', '');

		return $this->db->count_all_results();
	}	


	public function check_duplicated_edit($id, $email)
	{
		$this->db->from($this->table);
        $this->db->where('email', $email);
		$this->db->where('id !=', $id);

		// Filtra eliminados
		$this->db->where('usuario_baja', '');
		
		return $this->db->count_all_results();
	}	


	/* RESET PASSWORD */
	public function save_reset($data)
	{
		$this->db->insert($this->table_reset, $data);
		return $this->db->affected_rows();
	}


	public function get_reset_data($token) 
	{		
		$this->db->from($this->table_reset);
		$this->db->where('reset_token', $token);

		return $this->db->get()->row();		
	}	


	public function get_activacion_data($token) 
	{		
		$this->db->from('cliente');
		$this->db->where('cliente_activacion', $token);

		return $this->db->get()->row();		
	}	

	public function delete_reset_by_id($id)
	{
	    $retorno = "";

		$this->db->where('reset_id', $id);
		
		if (!$this->db->delete($this->table_reset)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}


	public function delete_reset_by_usuario_id($usuario_id)
	{
	    $retorno = "";

		$this->db->where('reset_usuario_id', $usuario_id);
		
		if (!$this->db->delete($this->table_reset)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	
	/* fin RESET PASSWORD */
}