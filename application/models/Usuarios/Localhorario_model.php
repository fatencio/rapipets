<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localhorario_model extends CI_Model {

	var $table = 'local_horario';


	public function get_horario_servicio($local_id, $servicio)
	{
		$this->db->select( 'local_horario_lu as lu,
							local_horario_lu_h_1 as lu_h_1,
							local_horario_lu_m_1 as lu_m_1,
							local_horario_lu_h_2 as lu_h_2,
							local_horario_lu_m_2 as lu_m_2,
							local_horario_lu_h_3 as lu_h_3,
							local_horario_lu_m_3 as lu_m_3,
							local_horario_lu_h_4 as lu_h_4,
							local_horario_lu_m_4 as lu_m_4,

							local_horario_ma as ma,
							local_horario_ma_h_1 as ma_h_1,
							local_horario_ma_m_1 as ma_m_1,
							local_horario_ma_h_2 as ma_h_2,
							local_horario_ma_m_2 as ma_m_2,
							local_horario_ma_h_3 as ma_h_3,
							local_horario_ma_m_3 as ma_m_3,
							local_horario_ma_h_4 as ma_h_4,
							local_horario_ma_m_4 as ma_m_4,							

							local_horario_mi as mi,
							local_horario_mi_h_1 as mi_h_1,
							local_horario_mi_m_1 as mi_m_1,
							local_horario_mi_h_2 as mi_h_2,
							local_horario_mi_m_2 as mi_m_2,
							local_horario_mi_h_3 as mi_h_3,
							local_horario_mi_m_3 as mi_m_3,
							local_horario_mi_h_4 as mi_h_4,
							local_horario_mi_m_4 as mi_m_4,

							local_horario_ju as ju,
							local_horario_ju_h_1 as ju_h_1,
							local_horario_ju_m_1 as ju_m_1,
							local_horario_ju_h_2 as ju_h_2,
							local_horario_ju_m_2 as ju_m_2,
							local_horario_ju_h_3 as ju_h_3,
							local_horario_ju_m_3 as ju_m_3,
							local_horario_ju_h_4 as ju_h_4,
							local_horario_ju_m_4 as ju_m_4,

							local_horario_vi as vi,
							local_horario_vi_h_1 as vi_h_1,
							local_horario_vi_m_1 as vi_m_1,
							local_horario_vi_h_2 as vi_h_2,
							local_horario_vi_m_2 as vi_m_2,
							local_horario_vi_h_3 as vi_h_3,
							local_horario_vi_m_3 as vi_m_3,
							local_horario_vi_h_4 as vi_h_4,
							local_horario_vi_m_4 as vi_m_4,

							local_horario_sa as sa,
							local_horario_sa_h_1 as sa_h_1,
							local_horario_sa_m_1 as sa_m_1,
							local_horario_sa_h_2 as sa_h_2,
							local_horario_sa_m_2 as sa_m_2,
							local_horario_sa_h_3 as sa_h_3,
							local_horario_sa_m_3 as sa_m_3,
							local_horario_sa_h_4 as sa_h_4,
							local_horario_sa_m_4 as sa_m_4,

							local_horario_do as do,
							local_horario_do_h_1 as do_h_1,
							local_horario_do_m_1 as do_m_1,
							local_horario_do_h_2 as do_h_2,
							local_horario_do_m_2 as do_m_2,
							local_horario_do_h_3 as do_h_3,
							local_horario_do_m_3 as do_m_3,
							local_horario_do_h_4 as do_h_4,
							local_horario_do_m_4 as do_m_4
						')
			->from($this->table)
        	->where('local_horario_local_id', $local_id)
        	->where('local_horario_servicio', $servicio);

		$query = $this->db->get();

		return $query->row();	
	}	


/*
	public function get_horario($local_id)
	{
		$this->db->select( 'local_horario_lav as lav,
							local_horario_lav1 as lav1,
							local_horario_lav2 as lav2,
							local_horario_lav3 as lav3,
							local_horario_lav4 as lav4,
							local_horario_s as s, 
							local_horario_s1 as s1, 
							local_horario_s2 as s2, 
							local_horario_s3 as s3, 
							local_horario_s4 as s4, 
							local_horario_d as d,
							local_horario_d1 as d1,
							local_horario_d2 as d2,
							local_horario_d3 as d3,
							local_horario_d4 as d4
						');
		$this->db->from($this->table);
        $this->db->where('local_horario_local_id', $local_id);

		$query = $this->db->get();

		return $query->row();	
	}	
*/

	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	
	public function update($where, $data)
	{	
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}

	
	public function delete_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where('local_horario_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	


	// EXCEPCIONES al horario normal del local

	// Excepciones por DIA
	public function get_excepcion_dia($local_id, $servicio, $dia)
	{
		$this->db->select('local_horario_excepcion_dia_abierto as abierto');
		$this->db->from('local_horario_excepcion_dia');
        $this->db->where('local_horario_excepcion_dia_local_id', $local_id);
        $this->db->where('local_horario_excepcion_dia_servicio', $servicio);
        $this->db->where('local_horario_excepcion_dia_dia', $dia);

		$query = $this->db->get();

		return $query->row();	
	}


	public function save_excepcion_dia($data)
	{
		$this->db->insert('local_horario_excepcion_dia', $data);
		return $this->db->insert_id();
	}


	public function delete_excepcion_dia($local_id, $servicio, $dia)
	{
	    $retorno = "";

		$this->db->where('local_horario_excepcion_dia_local_id', $local_id);
		$this->db->where('local_horario_excepcion_dia_servicio', $servicio);
		$this->db->where('local_horario_excepcion_dia_dia', $dia);
		
		if (!$this->db->delete('local_horario_excepcion_dia')){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	


	// Excepciones por TURNO
	public function get_excepcion_turno($local_id, $servicio, $dia)
	{
		$this->db->select('local_horario_excepcion_turno_turno as turno');
		$this->db->from('local_horario_excepcion_turno');
        $this->db->where('local_horario_excepcion_turno_local_id', $local_id);
        $this->db->where('local_horario_excepcion_turno_servicio', $servicio);
        $this->db->where('local_horario_excepcion_turno_dia', $dia);

		$query = $this->db->get();

		return $query->result();	
	}


	public function save_excepcion_turno($data)
	{
		$this->db->insert('local_horario_excepcion_turno', $data);
		return $this->db->insert_id();
	}


	public function delete_excepcion_turno($local_id, $servicio, $dia)
	{
	    $retorno = "";

		$this->db->where('local_horario_excepcion_turno_local_id', $local_id);
		$this->db->where('local_horario_excepcion_turno_servicio', $servicio);
		$this->db->where('local_horario_excepcion_turno_dia', $dia);
		
		if (!$this->db->delete('local_horario_excepcion_turno')){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}		
}