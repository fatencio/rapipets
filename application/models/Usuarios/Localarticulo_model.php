<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LocalArticulo_model extends CI_Model {

	var $table = 'local_articulo';

	var $column_order = array('articulo_nombre', 'local_articulo_creador', 'presentacion_nombre', 'marca_nombre', 'local_articulo_precio','descuento_nombre', 'local_articulo_precio_descuento', 'local_articulo_stock', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('articulo_nombre', 'local_articulo_creador', 'presentacion_nombre', 'marca_nombre', 'local_articulo_precio','descuento_nombre', 'local_articulo_precio_descuento', 'local_articulo_stock');  // columnas con la opcion de busqueda habilitada

	var $order = array('articulo_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	// Lista de productos de un local 
	// Se llama desde Dashboard - Local - Artículos del local
	private function _get_datatables_query($id)
	{
	
		$this->db->select( 'local_articulo_id as id, 
							local_articulo_creador as tipo,
							local_nombre as local,
							articulo_nombre as articulo, 
							presentacion_id,
							presentacion_nombre as presentacion, 
							marca_nombre as marca, 
							local_articulo_precio as precio,
							descuento_porcentaje as descuento, 
							descuento_id,
							local_articulo_precio_descuento as precio_descuento, 
							local_articulo_stock as stock,
							local_articulo_estado as estado,
							local_articulo_estado_presentacion as estado_presentacion');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id', 'left');		
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('descuento', 'local_articulo_descuento_id = descuento_id', 'left');
        $this->db->where('local_articulo_local_id', $id);

		$i = 0;
	
		// Busquedas por drop-down list 

		// Tipo: (GeoTienda / Local)
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('local_articulo_creador' , $_POST['columns'][0]['search']['value']);
		}	
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables($id)
	{
		$this->_get_datatables_query($id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($id)
	{
		$this->_get_datatables_query($id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($id)
	{
		$this->db->from($this->table)
			->where('local_articulo_local_id', $id)
	        ->where('local_articulo_estado', 1)
	        ->where('local_articulo_estado_presentacion', 1);		

		return $this->db->count_all_results();
	}


	public function update($where, $data)
	{    				
		$this->db->update($this->table, $data, $where);
		
		return $this->db->affected_rows();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}	


	public function get_by_articulo_id($id)
	{       
		$this->db->distinct()
			->select( 'local_articulo_estado as estado,
						local_articulo_creador as tipo,
						articulo_id as id,
						articulo_nombre as nombre, 
						CONCAT(marca_nombre, " ", articulo_nombre) as marca_nombre,
						articulo_imagen as imagen,
						articulo_detalle as detalle,
						articulo_edad as edad,
						CASE articulo_medicados WHEN 0 THEN "no" WHEN 1 THEN "si" END as medicados,
						animal_nombre as animal,
						marca_nombre as marca, 
						rubro_nombre as rubro', false)
			->from($this->table)
			->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
			->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
			->join('animal', 'articulo_animal_id = animal_id')
			->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id')		
			->join('local', 'local_articulo_local_id = local_id')
			->join('marca', 'articulo_marca_id = marca_id', 'left')
			->join('rubro', 'articulo_rubro_id = rubro_id')
			->join('descuento', 'local_articulo_descuento_id = descuento_id')
        	->where('articulo_id', $id);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_all_by_articulo_id($id)
	{       
		$this->db->select('local_articulo_id as id,
						local_articulo_estado as estado,
						articulo_id,
						articulo_nombre as nombre, 
						CONCAT(marca_nombre, " ", articulo_nombre) as marca_nombre,
						articulo_imagen as imagen,
						articulo_detalle as detalle,
						articulo_edad as edad,
						CASE articulo_medicados WHEN 0 THEN "no" WHEN 1 THEN "si" END as medicados,
						animal_nombre as animal,
						marca_nombre as marca, 
						rubro_nombre as rubro', false)
			->from($this->table)
			->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
			->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
			->join('animal', 'articulo_animal_id = animal_id')
			->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id')		
			->join('local', 'local_articulo_local_id = local_id')
			->join('marca', 'articulo_marca_id = marca_id', 'left')
			->join('rubro', 'articulo_rubro_id = rubro_id')
			->join('descuento', 'local_articulo_descuento_id = descuento_id')
        	->where('articulo_id', $id);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_articulos_local($local_id, $busqueda = '')
    {
        $this->db->distinct();
        $this->db->select( 'articulo_id as id,
                            articulo_nombre as nombre, 
                            CONCAT(marca_nombre, " ", articulo_nombre) as marca_nombre,
                            articulo_imagen as imagen,
                            articulo_detalle as detalle,
                            articulo_edad as edad,
                            CASE articulo_medicados WHEN 0 THEN "no" WHEN 1 THEN "si" END as medicados,
                            animal_nombre as animal,
                            marca_nombre as marca, 
                            rubro_nombre as rubro', false);
        $this->db->from($this->table);
        $this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
        $this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
        $this->db->join('animal', 'articulo_animal_id = animal_id');
        $this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id');        
        $this->db->join('local', 'local_articulo_local_id = local_id');
        $this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
        $this->db->join('rubro', 'articulo_rubro_id = rubro_id');
        $this->db->join('descuento', 'local_articulo_descuento_id = descuento_id');
        $this->db->where('local_articulo_local_id', $local_id);
        $this->db->where('local_articulo_estado', 1);
        $this->db->like('articulo_nombre', $busqueda, 'both');
        $this->db->order_by('marca_nombre, articulo_nombre');


        $query = $this->db->get();

        return $query->result();        

    } 


	public function get_presentaciones_articulo_local($local_id, $articulo_id)
	{
		$this->db->select( 'local_articulo_id as id,
							local_articulo_articulo_presentacion_id as articulo_presentacion_id,
							local_articulo_estado_presentacion as estado,
							presentacion_id, 
							presentacion_nombre as nombre, 
							local_articulo_precio as precio,
							local_articulo_stock as stock,
							descuento_porcentaje as descuento, 
							descuento_id,
							local_articulo_precio_descuento as precio_descuento');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id');		
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('descuento', 'local_articulo_descuento_id = descuento_id');
        $this->db->where('local_articulo_local_id', $local_id);
		$this->db->where('articulo_id', $articulo_id);
		$this->db->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();		
	}		


	public function get_presentaciones_activas_articulo_local($local_id, $articulo_id)
	{
		$this->db->select( 'local_articulo_id as id,
							local_articulo_articulo_presentacion_id as articulo_presentacion_id,
							local_articulo_estado_presentacion as estado,
							presentacion_id, 
							presentacion_nombre as nombre, 
							local_articulo_precio as precio,
							local_articulo_stock as stock,
							descuento_porcentaje as descuento, 
							descuento_id,
							local_articulo_precio_descuento as precio_descuento');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id');		
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('descuento', 'local_articulo_descuento_id = descuento_id');
        $this->db->where('local_articulo_local_id', $local_id);
		$this->db->where('articulo_id', $articulo_id);
		$this->db->where('local_articulo_estado_presentacion', 1);
		$this->db->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();		
	}


	public function get_articulo_local($local_id, $articulo_id)
	{
		$this->db->select('local_articulo_estado as estado, local_articulo_creador as creador')
			->from($this->table)
			->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
        	->where('local_articulo_local_id', $local_id)
			->where('articulo_presentacion_articulo_id', $articulo_id)
			->limit (1);

		$query = $this->db->get();

		return $query->row();	
	}


	// Recupera presentaciones activas asociadas al articulo en cualquier local
	public function get_presentaciones_articulo_cualquier_local($articulo_id)
	{
		$presentaciones_utilizadas = array();
		
		$this->db->select('articulo_presentacion_presentacion_id as presentacion_id');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->where('articulo_presentacion_articulo_id', $articulo_id);
		$this->db->where('local_articulo_estado_presentacion', 1);

		$query = $this->db->get();

		foreach ($query->result_array() as $row)
		{
			$presentaciones_utilizadas[] = $row['presentacion_id'];
		}		

		return $presentaciones_utilizadas;
	}


	// Recupera locales que esten utilizando cualquier presentación del articulo
	public function get_locales_presentaciones_articulo($articulo_id)
	{
		$locales = array();
		
		$this->db->distinct()
        	->select( 'local_articulo_local_id as local_id')
	        ->from($this->table)
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->where('articulo_presentacion_articulo_id', $articulo_id);

		$query = $this->db->get();

		foreach ($query->result_array() as $row)
		{
			$locales[] = $row['local_id'];
		}		

		return $locales;
	}


	// Recupera presentaciones id activas de un articulo en un local
	public function get_presentaciones_id_activas_articulo_local($local_id, $articulo_id)
	{
		$presentaciones_utilizadas = array();
		
		$this->db->select('articulo_presentacion_presentacion_id as presentacion_id');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->where('articulo_presentacion_articulo_id', $articulo_id);
		$this->db->where('local_articulo_local_id', $local_id);
		$this->db->where('local_articulo_estado_presentacion', 1);

		$query = $this->db->get();

		foreach ($query->result_array() as $row)
		{
			$presentaciones_utilizadas[] = $row['presentacion_id'];
		}		

		return $presentaciones_utilizadas;
	}


	public function get_mayor_descuento_articulo_local($local_id, $articulo_id)
	{       
		$this->db->select_max('descuento_porcentaje', 'mayor_descuento');
		$this->db->join('local_articulo', 'local_articulo_descuento_id = descuento_id');
		$this->db->join('articulo_presentacion', 'articulo_presentacion_id = local_articulo_articulo_presentacion_id');
		$this->db->where('local_articulo_local_id', $local_id);	
		$this->db->where('articulo_presentacion_articulo_id', $articulo_id);	
		$query = $this->db->get('descuento');

		return $query->row()->mayor_descuento;
	}


	public function get_menor_precio_articulo_local($local_id, $articulo_id)
	{       
		$this->db->select_min('local_articulo_precio_descuento', 'menor_precio');
		$this->db->join('local_articulo', 'local_articulo_descuento_id = descuento_id');
		$this->db->join('articulo_presentacion', 'articulo_presentacion_id = local_articulo_articulo_presentacion_id');
		$this->db->where('local_articulo_local_id', $local_id);	
		$this->db->where('articulo_presentacion_articulo_id', $articulo_id);	
		$query = $this->db->get('descuento');

		return $query->row()->menor_precio;
	}


	public function get_count_marcas($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_marcas');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_edades($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_edades');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_rubros($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_rubros');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_tamanios($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_tamanios');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_razas($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_razas');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_medicados($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_medicados');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_animales($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_animales');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	


	public function get_count_presentaciones($local_id, $articulos_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_articulo_presentaciones');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('articulo_id', $articulos_id);
		$this->db->group_by('nombre');
		$this->db->order_by('orden');

		$query = $this->db->get();

		return $query->result();	
	}	


	/* 
	DASHBOARD LOCAL - TAB 'PRODUCTOS' 
	 Productos Activos: table_productos
	 Productos Inctivos: table_productos_inactivos
	*/

	var $column_order_productos = array('local_articulo_estado', 'local_articulo_creador', 'rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', 'articulo_codigo', null); 
	var $column_search_productos = array('rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', 'articulo_codigo');

	private function _get_datatables_query_productos($id, $estado)
	{
		$this->db->distinct();
		$this->db->select( 'local_articulo_creador as tipo,
							local_nombre as local,
							articulo_id as id, 
							articulo_nombre as nombre, 
							articulo_codigo as codigo, 
							animal_nombre as animal, 
							rubro_nombre as rubro, 
							articulo_medicados as medicados, 
							articulo_imagen as imagen, 
							articulo_edad as edad,
							marca_nombre as marca,
							local_articulo_estado as estado');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id', 'left');		
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('articulo_tamanio', 'articulo_id = articulo_tamanio_articulo_id', 'left');
		$this->db->join('articulo_raza', 'articulo_id = articulo_raza_articulo_id', 'left');
		$this->db->join('descuento', 'local_articulo_descuento_id = descuento_id', 'left');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');	
        $this->db->where('local_articulo_local_id', $id);

        // Filtro por estado
        $this->db->where('local_articulo_estado', $estado);

		$i = 0;
	
		// Busquedas por drop-down list 

		// Tipo: (GeoTienda / Local)
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('local_articulo_creador' , $_POST['columns'][0]['search']['value']);
		}	

		// Animal
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('animal_id' , $_POST['columns'][1]['search']['value']);
		}	

		// Rubro
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('rubro_id' , $_POST['columns'][2]['search']['value']);
		}		

		// Marca
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('marca_id' , $_POST['columns'][3]['search']['value']);
		}	

		// Edad
		if($_POST['columns'][7]['search']['value'] != ''){
			$this->db->like('articulo_edad' , $_POST['columns'][7]['search']['value']);
		}	

		// Tamanio
		if($_POST['columns'][8]['search']['value'] != ''){
			$this->db->where('articulo_tamanio_tamanio_id' , $_POST['columns'][8]['search']['value']);
		}	

		// Raza
		if($_POST['columns'][9]['search']['value'] != ''){
			$this->db->where('articulo_raza_raza_id' , $_POST['columns'][9]['search']['value']);
		}			
		
		// Presentación
		if($_POST['columns'][10]['search']['value'] != ''){
			$this->db->where('articulo_presentacion_presentacion_id' , $_POST['columns'][10]['search']['value']);
		}
		
		// Medicados
		if($_POST['columns'][11]['search']['value'] != ''){
			$this->db->where('articulo_medicados' , $_POST['columns'][11]['search']['value']);
		}	

		foreach ($this->column_search_productos as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_productos) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_productos[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_productos($id, $estado)
	{
		$this->_get_datatables_query_productos($id, $estado);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_productos($id, $estado)
	{
		$this->_get_datatables_query_productos($id, $estado);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_productos($id, $estado)
	{
		$this->db->distinct();
		$this->db->select( 'local_articulo_creador as tipo,
							local_nombre as local,
							articulo_id as id, 
							articulo_nombre as nombre, 
							articulo_codigo as codigo, 
							animal_nombre as animal, 
							rubro_nombre as rubro, 
							articulo_medicados as medicados, 
							articulo_imagen as imagen, 
							articulo_edad as edad,
							marca_nombre as marca,
							local_articulo_estado as estado');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('articulo', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id', 'left');		
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('articulo_tamanio', 'articulo_id = articulo_tamanio_articulo_id', 'left');
		$this->db->join('articulo_raza', 'articulo_id = articulo_raza_articulo_id', 'left');
		$this->db->join('descuento', 'local_articulo_descuento_id = descuento_id', 'left');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');	
        $this->db->where('local_articulo_local_id', $id);

        // Filtro por estado
        $this->db->where('local_articulo_estado', $estado);

		return $this->db->count_all_results();
	}	
	/* fin DASHBOARD LOCAL - TAB 'PRODUCTOS' */				


	// ARTICULOS DE UN LOCAL (listado que ve un cliente al ingresar a un local)

	// Filtros
	public function get_items_filtro_articulos_local($item, $local_id, $filtros_aplicados, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{   
		$filtros = "";
		$filtro_vista = "";
		$filtro_inactivos = "";
		$orden = 'nombre';

		$vista = 'v_articulo_' . $item; 

		if ($filtrar_inactivos == "true") $filtro_inactivos = " AND local_articulo_estado_presentacion = 1 ";

		// FILTROS YA APLICADOS
		// Creador
		if ($filtros_aplicados->creador != -1) $filtros .= " AND local_articulo_creador = '" . $filtros_aplicados->creador . "' ";

		// Rubro
		if ($filtros_aplicados->rubro != -1) $filtros .= " AND articulo_rubro_id = " . $filtros_aplicados->rubro . " ";

		// Animal
		if ($filtros_aplicados->animal != -1) $filtros .= " AND articulo_animal_id = " . $filtros_aplicados->animal . " ";

		// Marca
		if ($filtros_aplicados->marca != -1) $filtros .= " AND articulo_marca_id = " . $filtros_aplicados->marca . " ";

		// Raza
		if ($filtros_aplicados->raza != -1) $filtros .= " AND articulo_raza_raza_id = " . $filtros_aplicados->raza . " ";

		// Tamaño
		if ($filtros_aplicados->tamanio != -1) $filtros .= " AND articulo_tamanio_tamanio_id = " . $filtros_aplicados->tamanio . " ";

		// Edad
		if ($filtros_aplicados->edad != -1) $filtros .= " AND articulo_edad = '" . $filtros_aplicados->edad . "' ";

		// Presentación
		if ($filtros_aplicados->presentacion != -1) $filtros .= " AND articulo_presentacion_presentacion_id = '" . $filtros_aplicados->presentacion . "' ";		

		// Medicados
		if ($filtros_aplicados->medicados != -1) $filtros .= " AND articulo_medicados = '" . $filtros_aplicados->medicados . "' ";		

		// FILTRO ACTUAL
		switch ($item) 
		{
			case 'rubros':
				$filtro_vista = " AND articulo_rubro_id = " . $vista . ".id ";
				break;
			
			case 'animales':
				$filtro_vista = " AND articulo_animal_id = " . $vista . ".id ";
				break;

			case 'marcas':
				$filtro_vista = " AND articulo_marca_id = " . $vista . ".id ";
				break;	

			case 'razas':
				$filtro_vista = " AND articulo_raza_raza_id = " . $vista . ".id ";
				break;		

			case 'tamanios':
				$filtro_vista = " AND articulo_tamanio_tamanio_id = " . $vista . ".id ";
				break;	

			case 'edades':
				$filtro_vista = " AND articulo_edad = " . $vista . ".id ";
				break;		

			case 'presentaciones':
				$filtro_vista = " AND articulo_presentacion_presentacion_id = " . $vista . ".id ";
				$orden = 'orden';
				break;	

			case 'medicados':
				$filtro_vista = " AND articulo_medicados = " . $vista . ".id ";
				break;																		
		}

		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from($vista)
			->where('local_id', $local_id)
			->where("articulo_id IN (
				SELECT articulo_id 
				FROM local 
				LEFT JOIN `local_articulo` ON `local_articulo_local_id` = `local`.`local_id`
				LEFT JOIN `articulo_presentacion` ON `local_articulo_articulo_presentacion_id` = `articulo_presentacion_id`
				LEFT JOIN `articulo` ON `articulo_presentacion_articulo_id` = `articulo_id`
				LEFT JOIN `articulo_raza` ON `articulo_raza_articulo_id` = `articulo_id`
				LEFT JOIN `articulo_tamanio` ON `articulo_tamanio_articulo_id` = `articulo_id`
				WHERE local_id = " . $local_id . " 
				AND local_status = 1
				AND local_articulo_estado = " . $filtro_articulos_inactivos
				. $filtro_inactivos 
				. $filtro_vista
				. $filtros . ")", NULL, FALSE)
			->group_by('id, nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by($orden);			

		// La presentacion 1 (Unica Presentacion) queda oculta (para articulos creados por los Locales)
		if ($item == 'presentaciones') $this->db->where('id <>', 1);

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_servicios_local($local_id, $animal_id = -1)
	{       
		if ($animal_id != -1) 
			$vista = 'v_articulo_servicios_animal';
		else
			$vista = 'v_articulo_servicios';

		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from($vista)
			->where('local_id', $local_id)
			->group_by('nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by('nombre');			

		// Filtra por animal, si se seleccionó uno
		if ($animal_id != -1) $this->db->where('animal_id', $animal_id);

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_servicio_items_local($local_id, $servicio_id, $animal_id = -1)
	{       
		if ($animal_id != -1) 
			$vista = 'v_local_servicio_items_animal';
		else
			$vista = 'v_local_servicio_items';

		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from($vista)
			->where('local_id', $local_id)
			->where('servicio_id', $servicio_id)
			->group_by('id, nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by('nombre');

		// Filtra por animal, si se seleccionó uno
		if ($animal_id != -1) $this->db->where('animal_id', $animal_id);

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_animales_servicio_local($local_id, $servicio_id = -1, $servicio_item_id)
	{     
		if ($servicio_id != -1) 
		{
			if ($servicio_item_id != -1) 
				$vista = 'v_articulo_animales_servicio_servicio_item';
			else	
				$vista = 'v_articulo_animales_servicio_servicio';
		}
		else
			$vista = 'v_articulo_animales_servicio';

		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from($vista)
			->where('local_id', $local_id)
			->group_by('nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by('nombre');			

		// Filtra por servicio y servicio_item, si se seleccionó uno
		if ($servicio_id != -1) $this->db->where('servicio_id', $servicio_id);
		if ($servicio_item_id != -1) $this->db->where('servicio_item_id', $servicio_item_id);

		$query = $this->db->get();

		return $query->result();	
	}


	var $column_order_local = array('articulo_nombre', 'descuento_porcentaje', 'local_articulo_precio_descuento', null); 
	var $column_search_local = array('rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', 'articulo_codigo'); 	

	var $order_local = array('articulo_nombre' => 'asc'); // default order 


	// Listado de Artículos asociados a un local
	// Se llama cuando un cliente selecciona un local del listado de locales
	private function _get_datatables_local($local_id)
	{
		
	    $this->db->distinct()
        	->select(  'articulo_id as id,
        				articulo_creador as tipo,
	                    articulo_nombre as nombre, 
	                    CONCAT(marca_nombre, " ", articulo_nombre) as marca_nombre,
	                    articulo_imagen as imagen,
	                    articulo_detalle as detalle,
	                    articulo_edad as edad,
	                    CASE articulo_medicados WHEN 0 THEN "no" WHEN 1 THEN "si" END as medicados,
	                    animal_nombre as animal,
	                    marca_nombre as marca, 
	                    rubro_nombre as rubro', false)
	        ->from($this->table)
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
	        ->join('articulo_raza', 'articulo_raza_articulo_id = articulo_id', 'left')
	        ->join('articulo_tamanio', 'articulo_tamanio_articulo_id = articulo_id', 'left')	        
	        ->join('animal', 'articulo_animal_id = animal_id')
	        ->join('local', 'local_articulo_local_id = local_id')
	        ->join('marca', 'articulo_marca_id = marca_id', 'left')
	        ->join('rubro', 'articulo_rubro_id = rubro_id')
	        ->join('descuento', 'local_articulo_descuento_id = descuento_id')
	        ->where('local_articulo_local_id', $local_id)
	        ->where('local_articulo_estado', 1)
	        ->where('local_articulo_estado_presentacion', 1);

		$i = 0;
	
		// Busquedas por drop-down list 

		// Animal
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->where('animal_id' , $_POST['columns'][1]['search']['value']);
		}	

		// Rubro
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->where('rubro_id' , $_POST['columns'][2]['search']['value']);
		}
		
		// Marca
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->where('marca_id' , $_POST['columns'][3]['search']['value']);
		}

		// Edad
		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->where('articulo_edad' , $_POST['columns'][4]['search']['value']);
		}	

		// Tamanio
		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->where('articulo_tamanio_tamanio_id', $_POST['columns'][5]['search']['value']);
		}

		// Raza
		if($_POST['columns'][6]['search']['value'] != ''){
			$this->db->where('articulo_raza_raza_id' , $_POST['columns'][6]['search']['value']);
		}

		// Presentación
		if($_POST['columns'][7]['search']['value'] != ''){
			$this->db->where('articulo_presentacion_presentacion_id' , $_POST['columns'][7]['search']['value']);
		}	

		// Medicados
		if($_POST['columns'][8]['search']['value'] != ''){
			$this->db->where('articulo_medicados' , $_POST['columns'][8]['search']['value']);
		}	

		foreach ($this->column_search_local as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_local) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_local[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_local))
		{
			$order = $this->order_local;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_local($local_id)
	{
		$this->_get_datatables_local($local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_local($local_id)
	{
		$this->_get_datatables_local($local_id);

		$query = $this->db->get();
		
		return $query->num_rows();
	}


	public function count_all_local($local_id)
	{
	    $this->db->distinct()
        	->select( 'articulo_id as id,
                            articulo_nombre as nombre, 
                            CONCAT(marca_nombre, " ", articulo_nombre) as marca_nombre,
                            articulo_imagen as imagen,
                            articulo_detalle as detalle,
                            articulo_edad as edad,
                            CASE articulo_medicados WHEN 0 THEN "no" WHEN 1 THEN "si" END as medicados,
                            animal_nombre as animal,
                            marca_nombre as marca, 
                            rubro_nombre as rubro', false)
	        ->from($this->table)
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
	        ->join('articulo_raza', 'articulo_raza_articulo_id = articulo_id', 'left')
	        ->join('articulo_tamanio', 'articulo_tamanio_articulo_id = articulo_id', 'left')	        
	        ->join('animal', 'articulo_animal_id = animal_id')
	        ->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id')        
	        ->join('local', 'local_articulo_local_id = local_id')
	        ->join('marca', 'articulo_marca_id = marca_id', 'left')
	        ->join('rubro', 'articulo_rubro_id = rubro_id')
	        ->join('descuento', 'local_articulo_descuento_id = descuento_id')
	        ->where('local_articulo_local_id', $local_id)
	        ->where('local_articulo_estado', 1)
	        ->where('local_articulo_estado_presentacion', 1);
	        		
		return $this->db->count_all_results();
	}	

	// fin ARTICULOS DE UN LOCAL

	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('local_articulo_id', $id);
		if (!$this->db->delete($this->table)) { 
			$retorno = $this->db->error();

		}
		return $retorno;		
	}
	

	// Elimina todas las presentaciones de un articulo en el local
	public function delete_by_local_articulo($local_id, $articulo_id)
	{
		$retorno = "";

		// Presentaciones del articulo
		$presentaciones_articulo = 'SELECT articulo_presentacion_id FROM articulo_presentacion WHERE articulo_presentacion_articulo_id = ' . $articulo_id;		

		$this->db->where('local_articulo_local_id', $local_id);
		$this->db->where('local_articulo_articulo_presentacion_id IN (' . $presentaciones_articulo . ')', NULL, FALSE);		

		if (!$this->db->delete($this->table)) { 
			$retorno = $this->db->error();

		}

		return $retorno;		
	}


	// Elimina una presentacion de un articulo en cualquier local
	public function delete_local_articulo_presentacion_by_id($articulo_id, array $presentaciones_no_borrar)
	{
		$retorno = "";

		$presentaciones = implode(',', $presentaciones_no_borrar);
		
		$sql = "DELETE local_articulo FROM local_articulo 
  				JOIN articulo_presentacion ON articulo_presentacion_id = local_articulo_articulo_presentacion_id
  				WHERE articulo_presentacion_articulo_id = $articulo_id
  				AND articulo_presentacion_presentacion_id NOT IN ($presentaciones)";

		$this->db->query($sql);

		return $retorno;		
	}


	// Elimina las presentaciones de un articulo en el local, filtrado por una lista que de presentaciones que no puede eliminarse
	public function delete_by_local_articulo_filtrado($local_id, $articulo_id, array $presentaciones_no_borrar)
	{
		$retorno = "";

		$presentaciones = implode(',', $presentaciones_no_borrar);
		
		$sql = "DELETE local_articulo FROM local_articulo 
  				JOIN articulo_presentacion ON articulo_presentacion_id = local_articulo_articulo_presentacion_id
  				WHERE local_articulo_local_id = $local_id
  				AND articulo_presentacion_articulo_id = $articulo_id
  				AND articulo_presentacion_presentacion_id NOT IN ($presentaciones)";

		$this->db->query($sql);

		return $retorno;		
	}


	public function delete_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where('local_articulo_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	

}