<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localservicioitem_model extends CI_Model {

	var $table = 'local_servicio_item';


	public function delete_by_local($local_id)
	{
		
        $retorno = "";

        $this->db->where('local_servicio_item_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;	
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function get_servicios_item_local($local_id, $status, $busqueda = '')
	{

		$this->db->select( 'local_servicio_item_id,
							servicio_item_id as id,
							servicio_item_nombre as nombre, 
							servicio_item_imagen as imagen,
							servicio_item_detalle as detalle,
							local_servicio_item_precio as precio,
							local_servicio_item_precio_descuento as precio_descuento,								
							descuento_porcentaje as descuento, 
							descuento_id,
							servicio_id,
							servicio_nombre as servicio,
							servicio_codigo,
							animal_id,
							animal_nombre as animal');
		$this->db->from($this->table);
		$this->db->join('servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id');
		$this->db->join('servicio', 'servicio_item_servicio_id = servicio_id');
		$this->db->join('animal', 'servicio_item_animal_id = animal_id');
		$this->db->join('descuento', 'local_servicio_item_descuento_id = descuento_id');
        $this->db->where('local_servicio_item_local_id', $local_id);
		$this->db->where('local_servicio_item_status', $status);
		$this->db->like('servicio_item_nombre', $busqueda, 'both');
		$this->db->order_by('servicio_nombre');

		$query = $this->db->get();

		return $query->result();		
	}	

	
	public function get_by_id($id)
	{

		$this->db->select( 'local_servicio_item_duracion as duracion,
							local_servicio_item_precio as precio, 
							local_servicio_item_status as status');
		$this->db->from($this->table);
        $this->db->where('local_servicio_item_id', $id);

		$query = $this->db->get();

		return $query->row();	
	}	


	public function get_by_servicio_item_local($servicio_item_id, $local_id)
	{

		$this->db->select( 'local_servicio_item_id as id,
							local_servicio_item_duracion as duracion,
							local_servicio_item_precio as precio, 
							local_servicio_item_status as status');
		$this->db->from($this->table);
        $this->db->where('local_servicio_item_servicio_item_id', $servicio_item_id);
        $this->db->where('local_servicio_item_local_id', $local_id);

		$query = $this->db->get();

		return $query->row();	
	}

	
	public function get_count_servicios($local_id, $servicio_items_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_servicios_local_servicios');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('servicio_item_id', $servicio_items_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	

	
	public function get_count_servicio_items($local_id, $servicio, $servicio_items_id)
	{       
		$this->db->select('count(servicio_item_nombre) as cuenta, servicio_item_nombre as nombre');
		$this->db->from('v_servicios_local_servicios');
		$this->db->where('local_id', $local_id);	
		$this->db->where('nombre', $servicio);	
		$this->db->where_in('servicio_item_id', $servicio_items_id);
		$this->db->group_by('servicio_item_nombre');

		$query = $this->db->get();

		return $query->result();	
	}		


	public function get_count_animales($local_id, $servicio_items_id)
	{       
		$this->db->select('count(nombre) as cuenta, nombre');
		$this->db->from('v_servicios_local_animales');
		$this->db->where('local_id', $local_id);	
		$this->db->where_in('servicio_item_id', $servicio_items_id);
		$this->db->group_by('nombre');

		$query = $this->db->get();

		return $query->result();	
	}	
	
}