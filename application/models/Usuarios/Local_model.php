<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local_model extends CI_Model {

	var $table = 'local';

	var $column_order = array('local_categoria', 'local_nombre', 'local_nombre_titular','local_telefono', 'local_email',null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('local_categoria', 'local_nombre', 'local_nombre_titular','local_apellido_titular','local_telefono', 'local_email');  // columnas con la opcion de busqueda habilitada

	var $order = array('local_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
        $this->db-> select('local_id as id, local_categoria as categoria, local_nombre as nombre, CONCAT(local_nombre_titular, " ", local_apellido_titular) AS titular, local_telefono as telefono, local_email as email, local_direccion as direccion,  local_localidad as localidad, local_status as status, local_avatar as avatar, local_latitud as latitud, local_longitud as longitud, local_destacado as destacado, local_urgencias as urgencias, local_telefono_urgencias as telefono_urgencias, local_urgencias_domicilio as urgencias_domicilio, local_urgencias_consultorio as urgencias_consultorio ', FALSE);
        $this->db->from($this->table);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		$i = 0;

		// Filtro por categoria
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('local_categoria', $_POST['columns'][0]['search']['value']);
		}

		// Filtro por destacado
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('local_destacado', $_POST['columns'][1]['search']['value']);
		}		
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1) $this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{       
    	$this->db->select(' local_id as id, 
    						local_password as password, 
    						local_categoria as categoria, 
    						local_nombre as nombre, 
    						local_nombre_titular as nombre_titular, 
    						local_apellido_titular as apellido_titular, 
    						local_telefono as telefono, 
    						local_email as email, 
    						local_telefono2 as telefono2, 
    						local_email2 as email2, 
    						local_direccion as direccion, 
    						local_localidad as localidad, 
    						local_horario_atencion as horario_atencion, 
    						local_horario_sabados as horario_sabados, 
    						local_horario_domingos as horario_domingos, 
    						local_envio_domicilio as envio_domicilio, 
    						local_costo_envio as costo_envio, 
    						local_texto_banner as texto_banner, 
    						local_color_banner AS color_banner, 
    						local_fondo_banner as fondo_banner, 
    						local_zona_entrega as zona_entrega, 
    						local_dias_entrega as dias_entrega, 
    						local_provincia_id as id_provincia, 
    						local_avatar as avatar, 
    						local_latitud as latitud, 
    						local_longitud as longitud, 
    						local_status as status, 
    						local_fecha_creacion as fecha_creacion,
    						local_destacado as destacado,
    						local_urgencias as urgencias,
    						local_telefono_urgencias as telefono_urgencias,
    						local_urgencias_domicilio as urgencias_domicilio, 
    						local_urgencias_consultorio as urgencias_consultorio');
        $this->db->from($this->table);
		$this->db->where('local_id',$id);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_banner_local($id)
	{       
    	$this->db->select('local_texto_banner as texto, 
    					   local_color_banner AS color, 
    					   local_fondo_banner as fondo');
        $this->db->from($this->table);
		$this->db->where('local_id',$id);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_destacado($id)
	{       
    	$this->db->select('local_destacado as destacado');
        $this->db->from($this->table);
		$this->db->where('local_id',$id);

		$query = $this->db->get();

		return $query->row('destacado');
	}


	public function check_duplicated($email)
	{
		$this->db->from($this->table);
		$this->db->where('local_email', $email);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $email)
	{
		$this->db->from($this->table);
        $this->db->where('local_email', $email);
		$this->db->where('local_id !=', $id);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{	
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function update_password($where, $data)
	{
		$data['local_password'] = password_hash($data['local_password'], PASSWORD_BCRYPT);

		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		
        $retorno = "";

		$this->db->where('local_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;	
	}


	public function get_all()
	{       
		$this->db->select('local_id as id, local_nombre as nombre, local_apellido_titular as apellido , local_telefono as telefono, local_email as email, local_direccion as direccion, local_localidad as localidad');
		$this->db->from($this->table);
		$this->db->order_by('local_nombre');

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		$query = $this->db->get();

		return $query->result();
	}	


	public function buscar_por_geolocalizacion($latitud, $longitud, $limite, $busqueda = ''){

		$this->db->select('local_id as id, 
							local_categoria as categoria, 
							local_nombre as nombre, 
							local_nombre_titular as nombre_titular, 
							local_apellido_titular as apellido_titular, 
							local_telefono as telefono, 
							local_email as email,
							local_telefono2 as telefono2, 
							local_email2 as email2, 
							local_direccion as direccion, 
							local_localidad as localidad, 
							local_horario_atencion as horario_atencion, 
							local_horario_sabados as horario_sabados, 
							local_horario_domingos as horario_domingos, 
							local_costo_envio as costo_envio, 
							local_texto_banner as texto_banner, 
							local_envio_domicilio as envio_domicilio, 
							local_zona_entrega as zona_entrega, 
							local_dias_entrega as dias_entrega, 
							local_provincia_id as id_provincia, 
							local_avatar as avatar, 
							local_latitud as latitud, 
							local_longitud as longitud, 
							local_status as status,
							local_destacado as destacado,
							local_urgencias as urgencias,
							local_telefono_urgencias as telefono_urgencias,
    						local_urgencias_domicilio as urgencias_domicilio, 
    						local_urgencias_consultorio as urgencias_consultorio');
		$this->db->from($this->table);
		$this->db->where('local_status', 1);	// Carga solamente locales habilitados
		$this->db->like('local_nombre', $busqueda, 'both');
		$this->db->limit($limite);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_categorias()
	{
		$this->db->distinct();
		$this->db->select('local_categoria as nombre');
		$this->db->from($this->table);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		$query = $this->db->get();

		return $query->result();		
	}	


	public function get_categorias_locales($locales_id)
	{       
		$this->db->select('nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from('v_local_categorias')
			->join('local', 'local.local_id = v_local_categorias.local_id')		
			->where('local_usuario_baja', '')
			->where_in('v_local_categorias.local_id', $locales_id)
			->group_by('nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by('nombre');			

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_rubros_locales($locales_id)
	{       
		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from('v_local_rubros')
			->join('local', 'local.local_id = v_local_rubros.local_id')		
			->where('local_usuario_baja', '')			
			->where_in('v_local_rubros.local_id', $locales_id)
			->group_by('id, nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by('nombre');


		$query = $this->db->get();

		return $query->result();	
	}



	public function get_marcas_locales($locales_id, $animal_id = "")
	{       
		if ($animal_id != "") 
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_marcas_animal')
				->join('local', 'local.local_id = v_local_marcas_animal.local_id')		
				->where('local_usuario_baja', '')						
				->where_in('local.local_id', $locales_id)
				->where('animal_id', $animal_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}
		else
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_marcas')
				->join('local', 'local.local_id = v_local_marcas.local_id')		
				->where('local_usuario_baja', '')						
				->where_in('local.local_id', $locales_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_razas_locales($locales_id, $animal_id = "")
	{       
		if ($animal_id != "") 
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_razas_animal')
				->join('local', 'local.local_id = v_local_razas_animal.local_id')		
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->where('animal_id', $animal_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}
		else
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_razas')
				->join('local', 'local.local_id = v_local_razas.local_id')		
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_tamanios_locales($locales_id, $animal_id = "")
	{       
		if ($animal_id != "") 
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_tamanios_animal')
				->join('local', 'local.local_id = v_local_tamanios_animal.local_id')	
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->where('animal_id', $animal_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}
		else
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_tamanios')
				->join('local', 'local.local_id = v_local_tamanios.local_id')	
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_edades_locales($locales_id, $animal_id = "")
	{       
		if ($animal_id != "") 
		{
			$this->db->select('nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_edades_animal')
				->join('local', 'local.local_id = v_local_edades_animal.local_id')	
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->where('animal_id', $animal_id)
				->group_by('nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');			
		}
		else
		{
			$this->db->select('nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_edades')
				->join('local', 'local.local_id = v_local_edades.local_id')	
				->where('local_usuario_baja', '')
				->where_in('local.local_id', $locales_id)
				->group_by('nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');						
		}

		$query = $this->db->get();

		return $query->result();	
	}


	// Comentarios aprobados => $aprobado = 'si'
	// Comentarios rechazados => $aprobado = 'no'
	// Comentarios sin moderar => $aprobado = empty
	public function get_comentarios($id, $aprobado, $count = FALSE)
	{       

		$this->db->select( 'cliente_nombre AS cliente, 
							cliente_avatar,
							DATE_FORMAT(fecha, "%d/%m/%Y") AS fecha_comentario, 
							puntaje_cliente AS puntaje, 
							valoracion_cliente AS comentario,
							v_venta_turno.id AS venta_turno_id,
							tipo, 
							total', false);
		$this->db->from('v_venta_turno');
		$this->db->where('local_id', $id);	
		$this->db->where('puntaje_cliente IS NOT NULL', null, false);
		$this->db->where('valoracion_aprobada ', $aprobado);
		$this->db->join('cliente', 'v_venta_turno.cliente_id = cliente.cliente_id');
		$this->db->order_by('fecha', 'DESC');

		$query = $this->db->get();

		// Devuelve solo la cantidad de resultados
		if ($count)
			return $query->num_rows();
		else
			return $query->result();	
	}	


	// Puntaje promedio del local
	public function get_puntaje($id, $aprobado)
	{       
		$this->db->select( 'AVG(puntaje_cliente) AS puntaje', false);
		$this->db->from('v_venta_turno');
		$this->db->where('local_id', $id);	
		$this->db->where('puntaje_cliente IS NOT NULL', null, false);
		$this->db->where('valoracion_aprobada ', $aprobado);

		$query = $this->db->get();

		return $query->row()->puntaje;
	}


	public function get_mayor_descuento($id)
	{       
		$this->db->select_max('descuento_porcentaje', 'mayor_descuento');
		$this->db->join('local_articulo', 'local_articulo_descuento_id = descuento_id', 'left');
		$this->db->join('local_servicio_item', 'local_servicio_item_descuento_id = descuento_id', 'left');
		$this->db->where('local_articulo_local_id', $id);
		$this->db->or_where('local_servicio_item_local_id', $id);
		$query = $this->db->get('descuento');

		return $query->row()->mayor_descuento;
	}	


	// Valida si la imagen $name está siendo utilizada en algún local
	public function check_imagen($name)
	{
		$this->db->from($this->table);
		$this->db->where('local_avatar', $name);

		// Filtra eliminados
		$this->db->where('local_usuario_baja', '');

		return $this->db->count_all_results();
	}	


	public function get_items_filtro_locales($item, $locales_id, $filtros_aplicados)
	{   
		$filtros = "";
		$filtro_vista = "";
		$orden = 'nombre';

		$vista = 'v_local_' . $item; 

		// FILTROS YA APLICADOS
		// Rubro
		if ($filtros_aplicados->rubro != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_rubro_id = " . $filtros_aplicados->rubro . " ";

		// Animal
		if ($filtros_aplicados->animal != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_animal_id = " . $filtros_aplicados->animal . " ";

		// Marca
		if ($filtros_aplicados->marca != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_marca_id = " . $filtros_aplicados->marca . " ";

		// Raza
		if ($filtros_aplicados->raza != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_raza_raza_id = " . $filtros_aplicados->raza . " ";

		// Tamaño
		if ($filtros_aplicados->tamanio != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_tamanio_tamanio_id = " . $filtros_aplicados->tamanio . " ";

		// Edad
		if ($filtros_aplicados->edad != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_edad = '" . $filtros_aplicados->edad . "' ";

		// Presentación
		if ($filtros_aplicados->presentacion != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_presentacion_presentacion_id = '" . $filtros_aplicados->presentacion . "' ";		

		// Medicados
		if ($filtros_aplicados->medicados != -1) $filtros .= " AND local_articulo_estado = 1 AND local_articulo_estado_presentacion = 1 AND articulo_medicados = '" . $filtros_aplicados->medicados . "' ";	
			
		// Servicio
		if ($filtros_aplicados->servicio != -1) $filtros .= " AND servicio_item_servicio_id = '" . $filtros_aplicados->servicio . "' AND servicio_item_animal_id = v_local_animales.id ";

		// Servicio Item
		if ($filtros_aplicados->servicio_item != -1) $filtros .= " AND servicio_item_id = '" . $filtros_aplicados->servicio_item . "' AND servicio_item_animal_id = v_local_animales.id ";		

		$locales = implode(",", $locales_id);


		$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
			->from($vista)
			->where("local_id IN (
				SELECT local_id 
				FROM local 
				LEFT JOIN local_articulo ON local_articulo_local_id = local.local_id 
				LEFT JOIN articulo_presentacion ON local_articulo_articulo_presentacion_id = articulo_presentacion_id
				LEFT JOIN articulo ON articulo_presentacion_articulo_id = articulo_id
				LEFT JOIN articulo_raza ON articulo_raza_articulo_id = articulo_id
				LEFT JOIN articulo_tamanio ON articulo_tamanio_articulo_id = articulo_id
				LEFT JOIN local_servicio_item ON local_id = local_servicio_item_local_id
				LEFT JOIN servicio_item ON local_servicio_item_servicio_item_id = servicio_item_id
				WHERE local_id IN (" . $locales . ")"
				. $filtros . ")", NULL, FALSE)
			->group_by('id, nombre')
			->order_by('count(nombre)', 'DESC')
			->order_by($orden);			

		// La presentacion 1 (Unica Presentacion) queda oculta (para articulos creados por los Locales)
		if ($item == 'presentaciones') $this->db->where('id <>', 1);			

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_servicios_locales($locales_id, $animal_id = -1)
	{       
		if ($animal_id != -1) 
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_servicios_animal')
				->join('local', 'local.local_id = v_local_servicios_animal.local_id')		
				->where('local_usuario_baja', '')						
				->where_in('local.local_id', $locales_id)
				->where('animal_id', $animal_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}
		else
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_servicios')
				->join('local', 'local.local_id = v_local_servicios.local_id')		
				->where('local_usuario_baja', '')						
				->where_in('local.local_id', $locales_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}

		$query = $this->db->get();

		return $query->result();	
	}


	public function get_servicio_items_locales($locales_id, $servicio_id, $animal_id = -1)
	{       
		if ($animal_id != -1) 
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_servicio_items_animal')
				->join('local', 'local.local_id = v_local_servicio_items_animal.local_id')
				->where('local_usuario_baja', '')			
				->where_in('local.local_id', $locales_id)
				->where('servicio_id', $servicio_id)
				->where('animal_id', $animal_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}
		else
		{
			$this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
				->from('v_local_servicio_items')
				->join('local', 'local.local_id = v_local_servicio_items.local_id')
				->where('local_usuario_baja', '')			
				->where_in('local.local_id', $locales_id)
				->where('servicio_id', $servicio_id)
				->group_by('id, nombre')
				->order_by('count(nombre)', 'DESC')
				->order_by('nombre');
		}

		$query = $this->db->get();

		return $query->result();	
	}


	// Listado de locales (activos)
	// Se llama cuando un cliente ingresa una direccion y obtiene un listado de locales	
	var $column_order_frontend = array('local_nombre' ,null); 	// columnas con la opcion de orden habilitada
	var $column_search_frontend = array('local_nombre', 'local_direccion', 'local_localidad');  // columnas con la opcion de busqueda habilitada

	var $order_frontend = array('local_nombre' => 'asc'); // default order 

	private function _get_datatables_frontend($servicios = FALSE)
	{
		$this->db->distinct()
			->select('local_id as id, 
						local_categoria as categoria, 
						local_nombre as nombre, 
						local_nombre_titular as nombre_titular, 
						local_apellido_titular as apellido_titular, 
						local_telefono as telefono, 
						local_email as email,
						local_telefono2 as telefono2, 
						local_email2 as email2, 
						local_direccion as direccion, 
						local_localidad as localidad, 
						local_horario_atencion as horario_atencion, 
						local_horario_sabados as horario_sabados, 
						local_horario_domingos as horario_domingos, 
						local_costo_envio as costo_envio, 
						local_texto_banner as texto_banner, 
						local_envio_domicilio as envio_domicilio, 
						local_zona_entrega as zona_entrega, 
						local_dias_entrega as dias_entrega, 
						local_provincia_id as id_provincia, 
						local_avatar as avatar, 
						local_latitud as latitud, 
						local_longitud as longitud, 
						local_status as status,
						local_destacado as destacado,
						local_urgencias as urgencias,
						local_telefono_urgencias as telefono_urgencias,
    					local_urgencias_domicilio as urgencias_domicilio, 
    					local_urgencias_consultorio as urgencias_consultorio')
			->from($this->table)
	        ->join('local_articulo', 'local_articulo_local_id = local_id')			
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
	        ->join('articulo_raza', 'articulo_raza_articulo_id = articulo_id', 'left')
	        ->join('articulo_tamanio', 'articulo_tamanio_articulo_id = articulo_id', 'left')
			->where('local_status', 1)	// Filtra por locales habilitados
			->where('local_usuario_baja', '')	// Filtra locales eliminados
			->where('local_articulo_estado', 1) // Filtra por articulos activos
	        ->where('local_articulo_estado_presentacion', 1); // Filtra por presentaciones activos			

		$i = 0;

		// Busquedas por drop-down list 

		// Categoria (Pet Shop / Veterinaria)
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->where('local_categoria' , $_POST['columns'][1]['search']['value']);
		}

		// Animal
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->where('articulo_animal_id' , $_POST['columns'][2]['search']['value']);
		}	

		// Rubro
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->where('articulo_rubro_id' , $_POST['columns'][3]['search']['value']);
		}
		
		// Marca
		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->where('articulo_marca_id' , $_POST['columns'][4]['search']['value']);
		}

		// Edad
		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->where('articulo_edad' , $_POST['columns'][5]['search']['value']);
		}

		// Tamanio
		if($_POST['columns'][6]['search']['value'] != ''){
			$this->db->where('articulo_tamanio_tamanio_id', $_POST['columns'][6]['search']['value']);

		}	
		// Raza
		if($_POST['columns'][7]['search']['value'] != ''){
			$this->db->where('articulo_raza_raza_id' , $_POST['columns'][7]['search']['value']);
		}

		// Presentación
		if($_POST['columns'][8]['search']['value'] != ''){
			$this->db->where('articulo_presentacion_presentacion_id' , $_POST['columns'][8]['search']['value']);
		}	

		// Medicados
		if($_POST['columns'][9]['search']['value'] != ''){
			$this->db->where('articulo_medicados' , $_POST['columns'][9]['search']['value']);
		}			

		foreach ($this->column_search_frontend as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_frontend) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
	}


	private function _get_datatables_frontend_servicios($servicios = FALSE)
	{
		$this->db->distinct()
			->select('local_id as id, 
						local_categoria as categoria, 
						local_nombre as nombre, 
						local_nombre_titular as nombre_titular, 
						local_apellido_titular as apellido_titular, 
						local_telefono as telefono, 
						local_email as email,
						local_telefono2 as telefono2, 
						local_email2 as email2, 
						local_direccion as direccion, 
						local_localidad as localidad, 
						local_horario_atencion as horario_atencion, 
						local_horario_sabados as horario_sabados, 
						local_horario_domingos as horario_domingos, 
						local_costo_envio as costo_envio, 
						local_texto_banner as texto_banner, 
						local_envio_domicilio as envio_domicilio, 
						local_zona_entrega as zona_entrega, 
						local_dias_entrega as dias_entrega, 
						local_provincia_id as id_provincia, 
						local_avatar as avatar, 
						local_latitud as latitud, 
						local_longitud as longitud, 
						local_status as status,
						local_destacado as destacado,
						local_urgencias as urgencias,
						local_telefono_urgencias as telefono_urgencias,
    					local_urgencias_domicilio as urgencias_domicilio, 
    					local_urgencias_consultorio as urgencias_consultorio')
			->from($this->table)
	        ->join('local_servicio_item', 'local_id = local_servicio_item_local_id')
	        ->join('servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id')
			->where('local_status', 1)	// Filtra por locales habilitados
			->where('local_usuario_baja', '');	// Filtra locales eliminados

		$i = 0;

		// Busquedas por drop-down list 

		// Categoria (Pet Shop / Veterinaria)
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->where('local_categoria' , $_POST['columns'][1]['search']['value']);
		}

		// Animal
		if($_POST['columns'][2]['search']['value'] != '')
		{
			$this->db->where('servicio_item_animal_id' , $_POST['columns'][2]['search']['value']);
		}			
		
		// Servicio
		if($_POST['columns'][10]['search']['value'] != '')
		{
			$this->db->where('servicio_item_servicio_id' , $_POST['columns'][10]['search']['value']);
		}	

		// Servicio item
		if($_POST['columns'][11]['search']['value'] != '')
		{
			$this->db->where('servicio_item_id' , $_POST['columns'][11]['search']['value']);
		}			

		foreach ($this->column_search_frontend as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_frontend) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
	}

	function get_datatables_frontend()
	{
		// Filtro por Servicio
		if($_POST['columns'][10]['search']['value'] != '')
		{
			$this->_get_datatables_frontend_servicios();

			/*
			if($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);	
			*/

			$query = $this->db->get();		
		}
		else
		{
			// Filtro por Rubro
			if($_POST['columns'][3]['search']['value'] != '')
			{
				$this->_get_datatables_frontend();

				/*
				if($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);	
				*/

				$query = $this->db->get();				
			}
			// Sin filtros, o solo filtrado por Animal
			else
			{
				$this->_get_datatables_frontend();

				/*
				if($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);
				*/

				$query1 = $this->db->get_compiled_select();


				$this->_get_datatables_frontend_servicios();

				/*
				if($_POST['length'] != -1)
				$this->db->limit($_POST['length'], $_POST['start']);
				*/

				$query2 = $this->db->get_compiled_select();

				$query = $this->db->query($query1 . ' UNION ' . $query2);				
			}
		}

		return $query->result();
	}


	function get_articulos_raza_tamanio()
	{
		$articulos_raza_tamanio = array();
		$articulos_raza = array();
		$articulos_tamanio = array();

		// Raza
		if($_POST['columns'][5]['search']['value'] != '')
		{
			$this->db->select('articulo_raza_articulo_id as articulo_id');
			$this->db->from('articulo_raza');
			$this->db->where('articulo_raza_raza_id', $_POST['columns'][5]['search']['value']);

			$query = $this->db->get();

			$articulos = $query->result();

			foreach ($articulos as $articulo) 
			{
				$articulos_raza[] = $articulo->articulo_id;
			}
		}

		// Tamaño
		if($_POST['columns'][6]['search']['value'] != '')
		{
			$this->db->select('articulo_tamanio_articulo_id as articulo_id');
			$this->db->from('articulo_tamanio');
			$this->db->where('articulo_tamanio_tamanio_id', $_POST['columns'][6]['search']['value']);

			$query = $this->db->get();

			$articulos = $query->result();

			foreach ($articulos as $articulo) 
			{
				$articulos_tamanio[] = $articulo->articulo_id;
			}
		}

		// Si estan activos los dos filtros, calcula la interseccion de ambos
		if($_POST['columns'][5]['search']['value'] != '' && $_POST['columns'][6]['search']['value'] != '')
		{
			$articulos_raza_tamanio = array_intersect($articulos_raza, $articulos_tamanio);
		}
		else
		{
			// Busqueda solo por raza
			if($_POST['columns'][5]['search']['value'] != '')
			{
				$articulos_raza_tamanio = $articulos_raza;
			}
			// Busqueda solo por tamaño
			else
			{
				$articulos_raza_tamanio = $articulos_tamanio;
			}
		}

		if (sizeof($articulos_raza_tamanio) == 0) $articulos_raza_tamanio[] = -1;
		
		return $articulos_raza_tamanio;
	}


	function count_filtered_frontend()
	{
		$articulos_raza_tamanio = $this->get_articulos_raza_tamanio();

		$this->_get_datatables_frontend($articulos_raza_tamanio);

		$query = $this->db->get();

		return $query->num_rows();
	}


	public function count_all_frontend()
	{
		$this->db->from($this->table)
			->where('local_status', 1)	// Carga solamente locales habilitados
			->where('local_usuario_baja', '');	// Filtra locales eliminados
		
		return $this->db->count_all_results();
	}	
	

	public function get_all_localidades()
	{
		$this->db->distinct()
			->select('local_localidad as nombre')
			->from($this->table)
			->where('local_status', 1)	// Filtra por locales habilitados
			->where('local_usuario_baja', '')	// Filtra locales eliminados
			->order_by('local_localidad');

		$query = $this->db->get();

		return $query->result();		
	}	


	public function count_ventas($id)
	{
		$this->db->from('venta')
			->where('venta_local_id', $id);

		return $this->db->count_all_results();
	}

	
	public function count_turnos($id)
	{
		$this->db->from('turno')
			->where('turno_local_id', $id);

		return $this->db->count_all_results();
	}	
}