<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Localmediopago_model extends CI_Model {

	var $table = 'local_medio_pago';


	public function delete_by_local($local_id)
	{
		
        $retorno = "";

        $this->db->where('local_medio_pago_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;	
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	

	public function get_by_medio_pago_local($medio_pago_id, $local_id)
	{
		$this->db->select( 'local_medio_pago_id as id');
		$this->db->from($this->table);
        $this->db->where('local_medio_pago_medio_pago_id', $medio_pago_id);
        $this->db->where('local_medio_pago_local_id', $local_id);

		$query = $this->db->get();

		return $query->row();	
	}


	public function get_by_medio_pago_item_local($medio_pago_item_id, $local_id)
	{

		$this->db->select( 'local_medio_pago_id as id');
		$this->db->from($this->table);
        $this->db->where('local_medio_pago_medio_pago_item_id', $medio_pago_item_id);
        $this->db->where('local_medio_pago_local_id', $local_id);

		$query = $this->db->get();

		return $query->row();	
	}

}