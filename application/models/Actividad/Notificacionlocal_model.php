<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacionlocal_model extends CI_Model {

	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	var $table = 'notificacion_local';

	var $column_order = array('notificacion_local_fecha', 'notificacion_local_tipo', 'notificacion_local_texto', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('notificacion_local_fecha', 'notificacion_local_texto', 'notificacion_local_tipo'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('notificacion_local_fecha' => 'desc'); // default order 


	function get_datatables($local_id)
	{
		$this->_get_datatables_query($local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	private function _get_datatables_query($local_id)
	{

        $this->db->select('notificacion_local_id as id, notificacion_local_texto as texto, notificacion_local_tipo as tipo, notificacion_local_fecha as fecha, notificacion_local_venta_id as venta_id, notificacion_local_turno_id as turno_id, notificacion_local_vacuna_id as vacuna_id');
		$this->db->from($this->table);
		$this->db->where('notificacion_local_local_id', $local_id);
	
        $i = 0;
	
		// Busqueda por drop-down list en la segunda columna 
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->where('notificacion_local_tipo', $_POST['columns'][1]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function count_filtered($local_id)
	{
		$this->_get_datatables_query($local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($local_id)
	{
		$this->db->from($this->table);
		$this->db->where('notificacion_local_local_id', $local_id);
		return $this->db->count_all_results();
	}	


	public function get_by_id($id)
	{
		
        $this->db->select('notificacion_local_id as id, notificacion_local_texto as texto, notificacion_local_tipo as tipo');
		$this->db->from($this->table);
		$this->db->where('notificacion_local_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('notificacion_local_texto', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('notificacion_local_texto', $name);
		$this->db->where('notificacion_local_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{

		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('notificacion_local_id', $id);

		if (!$this->db->delete($this->table)){

		  	$retorno = $this->db->error();

	    }
	    return $retorno;
	}


	public function delete_old()
	{
		$retorno = "";

		// Resta a la fecha actual la cantidad de dias configurados para eliminar notificaciones antiguas
		$fecha = date("Y-m-d h:i:s", time() - $this->config->item('dias_notificaciones') * 86400); 

		$this->db->where('notificacion_local_fecha <', $fecha);

		if (!$this->db->delete($this->table)){

		  	$retorno = $this->db->error();

	    }
	    return $retorno;
	}


	public function get_count_novistas($local_id)
	{       
		$this->db->from($this->table);
		$this->db->where('notificacion_local_vista', '0');
		$this->db->where('notificacion_local_local_id', $local_id);

		return $this->db->count_all_results();
	}	


	public function get_novistas($local_id)
	{       
        $this->db->select('notificacion_local_id as id, notificacion_local_texto as texto, notificacion_local_tipo as tipo, notificacion_local_fecha as fecha');
		$this->db->from($this->table);
		$this->db->where('notificacion_local_vista', '0');
		$this->db->where('notificacion_local_local_id', $local_id);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_recientes($local_id)
	{       

        $this->db->select('notificacion_local_id as id, 
        				   notificacion_local_texto as texto, 
        				   notificacion_local_tipo as tipo, 
        				   notificacion_local_fecha as fecha, 
        				   notificacion_local_vista as vista, 
        				   notificacion_local_leida as leida, 
        				   notificacion_local_venta_id as venta_id, 
        				   notificacion_local_turno_id as turno_id,
        				   notificacion_local_vacuna_id as vacuna_id');
		$this->db->from($this->table);
		$this->db->where('notificacion_local_local_id', $local_id);
		$this->db->order_by('notificacion_local_fecha', 'DESC');
		$this->db->limit($this->config->item('cantidad_notificaciones_frontend'));

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_all_tipo()
	{
		$this->db->distinct();
		$this->db->select('notificacion_local_tipo as nombre');
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result();		
	}	
	
}