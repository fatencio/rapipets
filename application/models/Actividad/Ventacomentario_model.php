<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ventacomentario_model extends CI_Model {

	var $table = 'venta_comentario';


	function get_datatables($venta_id)
	{
		$this->_get_datatables_query($venta_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


    private function _get_datatables_query($venta_id)
	{

		$this->db-> select(' venta_comentario_fecha as fecha, 
							 venta_comentario_comentario as comentario,
							 local_nombre as local, 
							 CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente', false);
        $this->db->from($this->table);
        $this->db->join('local', 'local_id = venta_comentario_local_id', 'left');
        $this->db->join('cliente', 'cliente_id = venta_comentario_cliente_id', 'left');
		$this->db->where('venta_comentario_venta_id', $venta_id);
		$this->db->order_by('venta_comentario_fecha', 'asc');
	}


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get_by_id($id)
	{
		
        $this->db->select('venta_comentario_id as id, venta_comentario_texto as texto, venta_comentario_tipo as tipo');
		$this->db->from($this->table);
		$this->db->where('venta_comentario_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{

		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('venta_comentario_id', $id);

		if (!$this->db->delete($this->table)){

		  	$retorno = $this->db->error();

	    }
	    return $retorno;
	}


	public function delete_by_cliente($cliente_id)
	{
	    $retorno = "";

		$this->db->where("venta_comentario_venta_id IN (
	        SELECT `venta_id` 
	        FROM `venta`
	        WHERE `venta_cliente_id` = $cliente_id)", NULL, FALSE);

		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	

	
	public function delete_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where("venta_comentario_venta_id IN (
	        SELECT `venta_id` 
	        FROM `venta`
	        WHERE `venta_local_id` = $local_id)", NULL, FALSE);

		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	
}