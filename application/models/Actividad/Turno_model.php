<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turno_model extends CI_Model {

	var $table = 'turno';

	var $column_order = array('turno_id', 'turno_fecha', 'servicio_nombre', 'total', 'local_nombre', 'cliente_nombre', null); 	// columnas con la opcion de orden habilitada (alias)

	var $column_search = array('turno_id', 'servicio_nombre', 'turno_fecha', 'turno_hora', 'cliente_nombre', 'cliente_apellido', 'local_nombre', 'turno_precio');  // columnas con la opcion de busqueda habilitada (nombre campo)

	var $order = array('turno_fecha' => 'asc'); // default order 
	var $order_excel = array('turno_fecha' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	// Turnos del local con estado 'reservado, concretado o no concretado' 
	// y no notificados por el local, pero sí notificados por el cliente

	// Cuando el estado es 'concretado' o 'no concretado' y no ha sido notificado por el local ni el cliente
	// es un turno calificado automáticamente por el sistema, y debe ser mostrado en el tab 'Ventas' del local	

	// Se llama desde el dashboard del local > Tab 'Turnos' y desde el Backend > Turnos
	private function _get_datatables_query($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');
	
        $this->db-> select("turno_id as id, CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, turno_fecha as fecha, turno_hora as hora, CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, local_nombre as local, turno_precio as total, servicio_nombre as servicio, servicio_item_nombre as servicio_item, turno_estado as estado, turno_concretado_local as concretado_local", FALSE)
        	->from($this->table)
	        ->join('local', 'local_id = turno_local_id')
	        ->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_concretado_local', '')
		    		->where('turno_concretado_cliente <>', '')
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		$i = 0;

		// Busquedas por drop-down list 	
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('servicio_nombre' , $_POST['columns'][1]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('local_nombre' , $_POST['columns'][4]['search']['value']);
		}			

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		 

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	


		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db->from($this->table)
	        ->join('local', 'local_id = turno_local_id')
	        ->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_concretado_local', '')
		    		->where('turno_concretado_cliente <>', '')		    		
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	

		return $this->db->count_all_results();
	}


	public function get_by_id($turno_id)
	{       
        $this->db-> select("turno_id as id, 
        					CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, 
        					turno_fecha as fecha, 
        					turno_hora as hora, 
        					servicio_nombre as servicio, 
        					servicio_item_nombre as servicio_item,
        					cliente_id,
        					CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, 
        					cliente_telefono, 
        					cliente_email,   
        					cliente_avatar,
        					local_id,      					
        					local_nombre as local, 
        					local_nombre_titular, 
        					local_apellido_titular, 
        					local_telefono, 
        					local_email, 
        					local_avatar,
        					local_direccion,
        					local_localidad,        					
        					turno_precio as total, 
        					turno_estado as estado,
        					turno_concretado_local as concretado_local,
        					turno_concretado_cliente as concretado_cliente,
        					turno_puntaje_cliente as puntaje, 
        					turno_valoracion_cliente as valoracion,
        					turno_valoracion_local as valoracion_local,
        					turno_valoracion_aprobada as valoracion_aprobada,
        					turno_observaciones as observaciones,
        					turno_cancelado_por as cancelado_por,
        					turno_cancelado_comentario as cancelado_comentario", false);

        $this->db->from($this->table);
        $this->db->join('local', 'local_id = turno_local_id');
        $this->db->join('cliente', 'cliente_id = turno_cliente_id');   
        $this->db->join('servicio_item', 'servicio_item_id = turno_servicio_item_id');
        $this->db->join('servicio', 'servicio_id = servicio_item_servicio_id');        
        $this->db->where('turno_id', $turno_id);

		$query = $this->db->get();

		return $query->row();
	}		


	public function get_all_fechas()
	{
		$this->db->distinct();
		$this->db->select("EXTRACT( YEAR_MONTH FROM turno_fecha ) AS mes_anio, CONCAT(LPAD(MONTH(turno_fecha), 2, '0'), '/', YEAR(turno_fecha)) AS fecha");
		$this->db->from($this->table);
		$this->db->order_by('turno_fecha', 'desc');

		$query = $this->db->get();

		return $query->result();
	}

	public function get_all_clientes()
	{
		$this->db->distinct();
		$this->db->select('CONCAT(cliente_nombre, " " , cliente_apellido) AS nombre', FALSE);
		$this->db->from($this->table);
		$this->db->join('cliente', 'cliente_id = turno_cliente_id');
		$this->db->order_by(1);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_count_by_local_fecha($local_id, $servicio, $fecha)
	{       
		$this->db->from($this->table)
			->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
			->join('servicio', 'servicio_item_servicio_id = servicio_id')		
			->where('servicio_codigo', $servicio)
			->where('turno_local_id', $local_id)
			->where('turno_fecha', $fecha)
			->where('turno_estado <>', 'cancelado');

		return $this->db->count_all_results();
	}


	public function get_count_by_local_horario($local_id, $servicio, $fecha, $hora)
	{       
		$this->db->from($this->table)
			->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
			->join('servicio', 'servicio_item_servicio_id = servicio_id')		
			->where('servicio_codigo', $servicio)
			->where('turno_local_id', $local_id)
			->where('turno_fecha', $fecha)
			->where('turno_hora', $hora)
			->where('turno_estado <>', 'cancelado');

		return $this->db->count_all_results();
	}	


	public function get_by_local_horario($local_id, $servicio, $fecha, $hora)
	{     
		$this->db->select("CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, local_servicio_item_duracion as duracion, servicio_item_nombre as item")
			->from($this->table)
			->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
			->join('servicio', 'servicio_item_servicio_id = servicio_id')					
			->join('local_servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id')
			->where('servicio_codigo', $servicio)			
			->where('turno_local_id', $local_id)
			->where('local_servicio_item_local_id', $local_id)
			->where('turno_fecha', $fecha)
			->where('turno_hora', $hora)
			->where('turno_estado <>', 'cancelado');  

		$query = $this->db->get();

		return $query->row();
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);

        return $this->db->insert_id();
	}	


	public function update($where, $data)
	{	
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}		


	
	// Tab 'Mis Turnos' del dashboard del Cliente - Turnos Activos
	// Cuando el estado es 'concretado' o 'no concretado' y no ha sido notificado por el local ni el cliente
	// es un servicio calificado automáticamente por el sistema, y debe ser mostrado en el tab 'Historial' del cliente
	var $column_order_activos = array('turno_id', 'turno_fecha', 'servicio_nombre', 'total', 'local_nombre', 'cliente_nombre', null); 	// columnas con la opcion de orden habilitada (alias)

	var $column_search_activos = array('turno_id', 'servicio_nombre', 'turno_fecha', 'turno_hora', 'cliente_nombre', 'cliente_apellido', 'local_nombre', 'turno_precio');  // columnas con la opcion de busqueda habilitada (nombre campo)

	private function _get_datatables_query_activos($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select("turno_id as id, CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, turno_fecha as fecha, turno_hora as hora, CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, local_nombre as local, turno_precio as total, servicio_nombre as servicio, servicio_item_nombre as servicio_item, turno_estado as estado, turno_puntaje_cliente as puntaje_cliente, turno_valoracion_cliente as valoracion_cliente, turno_valoracion_aprobada as valoracion_aprobada", FALSE);
        $this->db->from($this->table)
        	->join('local', 'local_id = turno_local_id')
        	->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')
   		    ->where('turno_fecha >=', 'curdate()', FALSE)	        
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_puntaje_cliente IS NULL', null, false)
		        	->where('turno_concretado_local <>', '')		    		
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		$i = 0;

		// Busquedas por drop-down list 	
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('servicio_nombre' , $_POST['columns'][1]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('local_nombre' , $_POST['columns'][4]['search']['value']);
		}			

		foreach ($this->column_search_activos as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_activos) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	


		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_activos[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_activos($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_activos($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_activos($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_activos($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_activos($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->from($this->table)
        	->join('local', 'local_id = turno_local_id')
        	->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')		
   		    ->where('turno_fecha >=', 'curdate()', FALSE)	        
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_puntaje_cliente IS NULL', null, false)
		        	->where('turno_concretado_local <>', '')		    		
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	

		return $this->db->count_all_results();
	}	


	// Tab 'Mis Turnos' del dashboard del Cliente - Turnos Vencidos
	var $column_order_vencidos = array('turno_id', 'turno_fecha', 'servicio_nombre', 'total', 'local_nombre', 'cliente_nombre', null); 	// columnas con la opcion de orden habilitada (alias)

	var $column_search_vencidos = array('turno_id', 'servicio_nombre', 'turno_fecha', 'turno_hora', 'cliente_nombre', 'cliente_apellido', 'local_nombre', 'turno_precio');  // columnas con la opcion de busqueda habilitada (nombre campo)

	private function _get_datatables_query_vencidos($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select("turno_id as id, CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, turno_fecha as fecha, turno_hora as hora, CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, local_nombre as local, turno_precio as total, servicio_nombre as servicio, servicio_item_nombre as servicio_item, turno_estado as estado, turno_puntaje_cliente as puntaje_cliente, turno_valoracion_cliente as valoracion_cliente, turno_valoracion_aprobada as valoracion_aprobada", FALSE);

        $this->db->from($this->table)
        	->join('local', 'local_id = turno_local_id')
        	->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')	
	        ->where('turno_fecha <', 'curdate()', FALSE)
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_puntaje_cliente IS NULL', null, false)
		    		->where('turno_concretado_local <>', '')
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		$i = 0;

		// Busquedas por drop-down list 	
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('servicio_nombre' , $_POST['columns'][1]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('local_nombre' , $_POST['columns'][4]['search']['value']);
		}			

		foreach ($this->column_search_vencidos as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_vencidos) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	


		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_vencidos[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_vencidos($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_vencidos($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_vencidos($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_vencidos($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_vencidos($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->from($this->table)
        	->join('local', 'local_id = turno_local_id')
        	->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')			
	        ->where('turno_fecha <', 'curdate()', FALSE)
	    	->group_start()
		    	->group_start()
		    		->where_in('turno_estado', $estados)
		    		->where('turno_puntaje_cliente IS NULL', null, false)
		    		->where('turno_concretado_local <>', '')		    		
		    	->group_end()
		    	->or_group_start()
					->where('turno_estado', 'reservado')
		    	->group_end()
	    	->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('turno_cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('turno_local_id', $local_id);	

		return $this->db->count_all_results();
	}		


	// Turnos que siguen con estado 'reservado' después de N dias de vencida la fecha del mismo
	public function get_turnos_reservados()
	{       
		$dias = $this->config->item('dias_estado_turno');
		$where_fecha = 'turno_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';

		$this->db->select('turno_id as id, turno_estado as estado, turno_concretado_cliente as concretado_cliente, turno_concretado_local as concretado_local');		
		$this->db->from($this->table);
		$this->db->where('turno_estado', 'reservado');
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}		


	// Turnos con fecha cercana
	public function get_turnos_cercanos()
	{       
		$dias = $this->config->item('dias_recordatorio_turno');
		$where_fecha = 'turno_fecha = adddate(current_date, INTERVAL ' . $dias . ' DAY)';

        $this->db->select( "turno_id as id,
        					turno_fecha as fecha, 
        					turno_hora as hora, 
        					servicio_nombre as servicio, 
        					servicio_item_nombre as servicio_item,
        					cliente_id,
        					cliente_nombre,
        					CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, 
        					cliente_telefono, 
        					cliente_email,   
        					local_id,      					
        					local_nombre as local, 
        					local_telefono, 
        					local_email, 
        					local_direccion,
        					local_localidad,  
        					turno_observaciones as observaciones,      					
        					turno_precio as total", false);
		$this->db->from($this->table);
        $this->db->join('local', 'local_id = turno_local_id');
        $this->db->join('cliente', 'cliente_id = turno_cliente_id');   
        $this->db->join('servicio_item', 'servicio_item_id = turno_servicio_item_id');
        $this->db->join('servicio', 'servicio_id = servicio_item_servicio_id');        		
		$this->db->where('turno_puntaje_cliente IS NULL', null, false);
		$this->db->where('turno_estado <>', 'cancelado');
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}	


	function get_datatables_excel()
	{
		$this->_get_datatables_query_excel();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	// Tabla de Turnos para Excel - Se llama desde Admin -> Ventas -> Excel (table_turnos)
	private function _get_datatables_query_excel()
	{
		$estados = array('concretado', 'no concretado');

		$date = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));
 		$hoy = $date->format('Ymd');
	
        $this->db-> select("turno_id as id, CONCAT('S', LPAD(turno_id, 4, '0')) AS numero, turno_fecha as fecha, turno_hora as hora, 'Efectivo' as forma_pago, CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, local_nombre as local, turno_precio as precio, servicio_nombre as servicio, servicio_item_nombre as servicio_item, turno_estado as estado", FALSE)
        	->from($this->table)
	        ->join('local', 'local_id = turno_local_id')
	        ->join('cliente', 'cliente_id = turno_cliente_id')
	        ->join('servicio_item', 'servicio_item_id = turno_servicio_item_id')
	        ->join('servicio', 'servicio_id = servicio_item_servicio_id')
		    ->where('turno_estado <>', 'reservado');

		$i = 0;

		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('"turno"' , $_POST['columns'][0]['search']['value']);
		}

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('turno_mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local_nombre' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('CONCAT(cliente_nombre, " " , cliente_apellido)' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->where('turno_estado' , $_POST['columns'][4]['search']['value']);
		}			

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('"Efectivo"' , $_POST['columns'][5]['search']['value']);
		}			

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_excel))
		{
			$order = $this->order_excel;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}	


	public function delete_by_cliente($cliente_id)
	{
	    $retorno = "";

		$this->db->where('turno_cliente_id', $cliente_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}		


	public function delete_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where('turno_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	
}