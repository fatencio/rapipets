<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta_model extends CI_Model {

	var $table = 'venta';
	var $table_detalle = 'venta_detalle';
	var $venta_turno = 'v_venta_turno';

	var $column_order = array('numero', 'fecha', 'forma_pago', 'estado', 'cliente', 'local', 'total', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search = array('numero', 'fecha', 'cliente', 'local', 'estado', 'total', 'forma_pago');  // columnas con la opcion de busqueda habilitada

	var $order = array('fecha_creacion' => 'desc'); // default order 
	var $order_excel = array('fecha' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	// Llamado desde Admin > Ventas
	private function _get_datatables_query($cliente_id = 0, $local_id = 0)
	{
	
        $this->db-> select('id, numero, fecha, cliente, local, total, estado, tipo, forma_pago', FALSE);
        $this->db->from($this->venta_turno);

		// Muestra todas las ventas, y los turnos con estado distinto a 'reservado'
		// Genera: AND (tipo = 'venta' OR (tipo = 'turno' AND estado <> 'reservado'))

        $this->db->group_start()
		                ->where('tipo', 'venta')
		                ->or_group_start()
		                	->where('tipo', 'turno')
		                    ->where('estado <>', 'reservado')
		                ->group_end()
		          ->group_end();

		$i = 0;

	
		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('tipo' , $_POST['columns'][0]['search']['value']);
		}	

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('cliente' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->where('estado' , $_POST['columns'][4]['search']['value']);
		}			

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('forma_pago' , $_POST['columns'][5]['search']['value']);
		}	

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);					

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	private function _get_datatables_query_excel()
	{
        $this->db-> select("venta_id as id, CONCAT('P', LPAD(venta_id, 4, '0')) AS numero, venta_fecha as fecha, venta_cliente_nombre as cliente, local_nombre as local, venta_total as total, venta_estado as estado, venta_condicion_venta as forma_pago, articulo_nombre as articulo, marca_nombre as marca, presentacion_nombre as presentacion,venta_detalle_cantidad as cantidad, venta_detalle_precio as precio", FALSE);
        $this->db->from($this->table);
        $this->db->join('local', 'local_id = venta_local_id');
        $this->db->join('venta_detalle', 'venta_detalle_venta_id = venta_id');
		$this->db->join('articulo', 'venta_detalle_articulo_id = articulo_id');
		$this->db->join('presentacion', 'venta_detalle_presentacion_id = presentacion_id');
		$this->db->join('marca', 'marca_id = articulo_marca_id', 'left');

		$i = 0;
	
		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('"venta"' , $_POST['columns'][0]['search']['value']);
		}

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('venta_mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local_nombre' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('venta_cliente_nombre' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->where('venta_estado' , $_POST['columns'][4]['search']['value']);
		}			

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('venta_condicion_venta' , $_POST['columns'][5]['search']['value']);
		}	

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_excel))
		{
			$order = $this->order_excel;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	// Detalles de una venta
    private function _get_datatables_query_detalle($venta_id)
	{

		$this->db-> select(' articulo_nombre as articulo, 
							 marca_nombre as marca, 
							 presentacion_id,
							 presentacion_nombre as presentacion,
							 venta_detalle_cantidad as cantidad,
							 venta_detalle_precio as precio,
							 venta_detalle_descuento as descuento');
		$this->db->from($this->table_detalle);
		$this->db->join('articulo', 'venta_detalle_articulo_id = articulo_id');
		$this->db->join('presentacion', 'venta_detalle_presentacion_id = presentacion_id');
		$this->db->join('marca', 'marca_id = articulo_marca_id', 'left');
		$this->db->where('venta_detalle_venta_id', $venta_id);
	}


	function get_datatables($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function get_datatables_excel()
	{
		$this->_get_datatables_query_excel();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function get_datatables_detalle($venta_id)
	{
		$this->_get_datatables_query_detalle($venta_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($cliente_id = 0, $local_id = 0)
	{
		$this->db->from($this->venta_turno);

        $this->db->group_start()
		                ->where('tipo', 'venta')
		                ->or_group_start()
		                	->where('tipo', 'turno')
		                    ->where('estado <>', 'reservado')
		                ->group_end()
		          ->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);			

		return $this->db->count_all_results();
	}


	public function get_all_fechas()
	{
		$this->db->distinct();
		$this->db->select("EXTRACT( YEAR_MONTH FROM fecha ) AS mes_anio, CONCAT(LPAD(MONTH(fecha), 2, '0'), '/', YEAR(fecha)) AS fecha");
		$this->db->from($this->venta_turno);
		$this->db->order_by('fecha', 'desc');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_fechas_local($local_id)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->distinct()
			->select("EXTRACT( YEAR_MONTH FROM fecha ) AS mes_anio, CONCAT(LPAD(MONTH(fecha), 2, '0'), '/', YEAR(fecha)) AS fechas", false)
        	->from($this->venta_turno)
			->where('local_id', $local_id)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()	        	
        	->group_end()
			->order_by('fecha', 'desc');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_fechas_pedidos_local($local_id)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->distinct()
			->select("EXTRACT( YEAR_MONTH FROM fecha ) AS mes_anio, CONCAT(LPAD(MONTH(fecha), 2, '0'), '/', YEAR(fecha)) AS fecha_pedido")
    		->from($this->venta_turno)
    		->where('tipo', 'venta')
			->where('local_id', $local_id)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local', '')
	        		->where('concretado_cliente <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'pendiente')
	        	->group_end()                
        	->group_end()
			->order_by('fecha', 'desc');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_clientes()
	{
		$this->db->distinct();
		$this->db->select('cliente as nombre');
		$this->db->from($this->venta_turno);
		$this->db->order_by('cliente');

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_clientes_local($local_id)
	{

		$this->db->distinct()
			->select('cliente as nombre')
			->from($this->venta_turno)
			->where('local_id', $local_id)
			->order_by('cliente');

		$query = $this->db->get();

		return $query->result();

	}	


	public function get_clientes_ventas_local($local_id)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->distinct()
			->select('cliente as nombre')
    		->from($this->venta_turno)
			->where('local_id', $local_id)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()	        	
        	->group_end()
			->order_by('cliente');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_clientes_pedidos_local($local_id)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->distinct()
			->select('cliente as nombre')
    		->from($this->venta_turno)
    		->where('tipo', 'venta')
    		->where('local_id', $local_id)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local', '')
	        		->where('concretado_cliente <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'pendiente')
	        	->group_end()                
        	->group_end()
			->order_by('cliente');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_venta_by_id($venta_id)
	{       
        $this->db-> select("venta_id as id, 
        					CONCAT('P', LPAD(venta_id, 4, '0')) AS numero, 
        					venta_fecha as fecha, 
        					venta_cliente_nombre as cliente, 
        					venta_cliente_telefono as cliente_telefono_venta, 
        					local_id,
        					local_nombre as local, 
        					local_nombre_titular, 
        					local_apellido_titular, 
        					local_telefono, 
        					local_email, 
        					local_avatar,
        					local_direccion,
        					local_localidad,
        					local_envio_domicilio,
        					venta_total as total, 
        					venta_estado as estado, 
        					cliente_id,
        					cliente_nombre,
        					cliente_apellido,
        					cliente_telefono,        					
        					cliente_email, 
        					cliente_avatar,
        					venta_condicion_venta as condicion_venta, 
        					venta_tipo_envio as tipo_envio, 
        					venta_cliente_direccion as cliente_direccion, 
        					venta_cliente_localidad as cliente_localidad,
        					venta_cliente_provincia as cliente_provincia,
        					venta_observaciones as observaciones,
        					venta_puntaje_cliente as puntaje,
        					venta_valoracion_cliente as valoracion,
        					venta_valoracion_local as valoracion_local,
        					venta_valoracion_aprobada as valoracion_aprobada,
        					venta_concretado_local as concretado_local,
        					venta_concretado_cliente as concretado_cliente,
        					venta_cancelado_por as cancelado_por,
        					venta_cancelado_comentario as cancelado_comentario");

        $this->db->from($this->table);
        $this->db->join('local', 'local_id = venta_local_id');
        $this->db->join('cliente', 'cliente_id = venta_cliente_id');
        $this->db->where('venta_id', $venta_id);

		$query = $this->db->get();

		return $query->row();
	}		


	public function get_detalle_venta_by_id($venta_id)
	{       
		$this->db-> select(' articulo_nombre as articulo, 
							 presentacion_nombre as presentacion,
							 venta_detalle_cantidad as cantidad,
							 venta_detalle_precio as precio,
							 venta_detalle_descuento as descuento,
							 marca_nombre as marca');

		$this->db->from($this->table_detalle);
		$this->db->join('articulo', 'venta_detalle_articulo_id = articulo_id');
		$this->db->join('presentacion', 'venta_detalle_presentacion_id = presentacion_id', 'left');
		$this->db->join('marca', 'marca_id = articulo_marca_id', 'left');
		$this->db->where('venta_detalle_venta_id', $venta_id);

		$query = $this->db->get();

		return $query->result();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);

        return $this->db->insert_id();
	}


	public function save_detalle($data)
	{
		$this->db->insert($this->table_detalle, $data);

        return $this->db->insert_id();
	}	


	public function update($where, $data)
	{	
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}	


	// Tab 'Mis Pedidos' del dashboard del Cliente 
	var $column_order_pendientes = array('numero', 'fecha', 'forma_pago', 'local', 'total', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search_pendientes = array('numero', 'fecha', 'cliente', 'local', 'total', 'forma_pago', 'valoracion_cliente');  // columnas con la opcion de busqueda habilitada

	// Recupera pedidos del cliente con estado 'pendiente, concretado o no concretado' y que no tengan calificación
	// Cuando el estado es 'concretado' o 'no concretado' y no ha sido notificado por el local ni el cliente
	// es un pedido calificado automáticamente por el sistema, y debe ser mostrado en el tab 'Historial' del cliente
	private function _get_datatables_query_pendientes($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select('id, numero, fecha, cliente, local, total, estado, tipo, forma_pago, puntaje_cliente, valoracion_cliente, valoracion_aprobada, concretado_local, concretado_cliente', FALSE)
        		->from($this->venta_turno)
        		->where('tipo', 'venta')
	        	->group_start()
		        	->group_start()
		        		->where_in('estado', $estados)
		        		->where('puntaje_cliente IS NULL', null, false)
		        		->where('concretado_local <>', '')
		        	->group_end()
		        	->or_group_start()
						->where('estado', 'pendiente')
		        	->group_end()                
	        	->group_end();	                

		$i = 0;

	
		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('tipo' , $_POST['columns'][0]['search']['value']);
		}	

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('cliente' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('estado' , $_POST['columns'][4]['search']['value']);
		}			

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('forma_pago' , $_POST['columns'][5]['search']['value']);
		}	

		foreach ($this->column_search_pendientes as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pendientes) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);		

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_pendientes[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}	


	function get_datatables_pendientes($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_pendientes($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}	


	function count_filtered_pendientes($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_pendientes($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_pendientes($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->from($this->venta_turno)
    		->where('tipo', 'venta')
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('puntaje_cliente IS NULL', null, false)
		        	->where('concretado_local <>', '')	        		
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'pendiente')
	        	->group_end()                
        	->group_end();	  


		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);			

		return $this->db->count_all_results();
	}	


	/* HISTORIAL (PEDIDOS y TURNOS) */
	var $column_order_historial = array('numero', 'fecha', 'local', 'estado', 'forma_pago', 'total', 'valoracion_cliente', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search_historial = array('numero', 'fecha', 'cliente', 'local', 'total', 'forma_pago');  // columnas con la opcion de busqueda habilitada

	private function _get_datatables_query_historial($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select('id, numero, fecha, cliente, local, total, estado, tipo, forma_pago, puntaje_cliente, valoracion_cliente, valoracion_aprobada, cancelado_por, cancelado_comentario', FALSE)
        	->from($this->venta_turno)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('puntaje_cliente IS NOT NULL', null, false)
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()	        	
        	->group_end();

		$i = 0;

	
		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('tipo' , $_POST['columns'][0]['search']['value']);
		}	

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('cliente' , $_POST['columns'][3]['search']['value']);
		}	

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('estado' , $_POST['columns'][4]['search']['value']);
		}			

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('forma_pago' , $_POST['columns'][5]['search']['value']);
		}	

		foreach ($this->column_search_historial as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_historial) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);		

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_historial[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}	


	function get_datatables_historial($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_historial($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}	


	function count_filtered_historial($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_historial($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_historial($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

		$this->db->from($this->venta_turno)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('puntaje_cliente IS NOT NULL', null, false)
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()		        	
        	->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);			

		return $this->db->count_all_results();
	}	


	// Dashboard Local - Tab 'Ventas'
	var $column_order_ventas_local = array('numero', 'fecha', 'forma_pago', 'estado', 'cliente', 'total', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search_ventas_local = array('numero', 'fecha', 'cliente', 'estado', 'total', 'forma_pago');  // columnas con la opcion de busqueda habilitada


	// Muestra todas las ventas y turnos con estado 'concretado' o 'no concretado'
	// y que hayan sido notificados por el local / o turnos o ventas cancelados
	private function _get_datatables_query_ventas_local($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select('id, numero, fecha, cliente, local, total, estado, tipo, forma_pago, concretado_local, concretado_cliente', FALSE)
        	->from($this->venta_turno)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()	        	
        	->group_end();

		$i = 0;

		// Busquedas por drop-down list 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('tipo' , $_POST['columns'][0]['search']['value']);
		}	

		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('local' , $_POST['columns'][2]['search']['value']);
		}	

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->where('estado' , $_POST['columns'][3]['search']['value']);
		}		

		if($_POST['columns'][4]['search']['value'] != ''){
			$this->db->like('cliente' , $_POST['columns'][4]['search']['value']);
		}		

		if($_POST['columns'][5]['search']['value'] != ''){
			$this->db->like('forma_pago' , $_POST['columns'][5]['search']['value']);
		}	

		foreach ($this->column_search_ventas_local as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ventas_local) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);	


		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_ventas_local[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_ventas_local($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_ventas_local($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_ventas_local($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_ventas_local($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_ventas_local($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db->from($this->venta_turno)
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'cancelado')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'concretado')
	        		->where('concretado_cliente', '')
	        		->where('concretado_local', '')
	        	->group_end()	  	        	
        	->group_end();

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);			

		return $this->db->count_all_results();
	}	


	// Dashboard Local - Tab 'Pedidos'
	var $column_order_pedidos_local = array('numero', 'fecha', 'forma_pago', 'cliente', 'total', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search_pedidos_local = array('numero', 'fecha', 'cliente', 'total', 'forma_pago');  // columnas con la opcion de busqueda habilitada


	// Pedidos al local con estado 'pendiente, concretado o no concretado' 
	// y no notificados por el local, pero sí notificados por el cliente
	// Cuando el estado es 'concretado' o 'no concretado' y no ha sido notificado por el local ni el cliente
	// es un pedido calificado automáticamente por el sistema, y debe ser mostrado en el tab 'Ventas' del local
	private function _get_datatables_query_pedidos_local($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db-> select('id, numero, fecha, cliente, local, total, estado, tipo, forma_pago, concretado_local, concretado_cliente', FALSE)
    		->from($this->venta_turno)
    		->where('tipo', 'venta')
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local', '')
	        		->where('concretado_cliente <>', '')
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'pendiente')
	        	->group_end()                
        	->group_end();	
				
		$i = 0;

		// Busquedas por drop-down list 
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('mes' , $_POST['columns'][1]['search']['value']);
		}		

		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('cliente' , $_POST['columns'][3]['search']['value']);
		}			

		foreach ($this->column_search_ventas_local as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_ventas_local) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);	


		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_ventas_local[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_pedidos_local($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_pedidos_local($cliente_id, $local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_pedidos_local($cliente_id = 0, $local_id = 0)
	{
		$this->_get_datatables_query_pedidos_local($cliente_id, $local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_pedidos_local($cliente_id = 0, $local_id = 0)
	{
		$estados = array('concretado', 'no concretado');

        $this->db->from($this->venta_turno)
    		->where('tipo', 'venta')
        	->group_start()
	        	->group_start()
	        		->where_in('estado', $estados)
	        		->where('concretado_local', '')
	        		->where('concretado_cliente <>', '')	        		
	        	->group_end()
	        	->or_group_start()
					->where('estado', 'pendiente')
	        	->group_end()                
        	->group_end();	

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('local_id', $local_id);			

		return $this->db->count_all_results();
	}


	// Ventas que siguen con estado 'pendiente' después de N dias de ingresadas
	public function get_ventas_pendientes()
	{       
		$dias = $this->config->item('dias_estado_venta');
		$where_fecha = 'venta_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';

		$this->db->select('venta_id as id, venta_estado as estado, venta_concretado_cliente as concretado_cliente, venta_concretado_local as concretado_local');		
		$this->db->from($this->table);
		$this->db->where('venta_estado', 'pendiente');
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}	


	public function delete_by_cliente($cliente_id)
	{
	    $retorno = "";

		$this->db->where('venta_cliente_id', $cliente_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	


	public function delete_detalle_by_cliente($cliente_id)
	{
	    $retorno = "";

		$this->db->where("venta_detalle_venta_id IN (
	        SELECT `venta_id` 
	        FROM `venta`
	        WHERE `venta_cliente_id` = $cliente_id)", NULL, FALSE);
		
		if (!$this->db->delete($this->table_detalle)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	


	public function delete_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where('venta_local_id', $local_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}	


	public function delete_detalle_by_local($local_id)
	{
	    $retorno = "";

		$this->db->where("venta_detalle_venta_id IN (
	        SELECT `venta_id` 
	        FROM `venta`
	        WHERE `venta_local_id` = $local_id)", NULL, FALSE);
		
		if (!$this->db->delete($this->table_detalle)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}		
}