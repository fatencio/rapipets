<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recordatorio_model extends CI_Model {

	var $table = 'recordatorio';


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function get_recordatorios_cliente_calificacion_venta()
	{       
		$limite = $this->config->item('cantidad_recordatorios_calificacion');
		$dias = $this->config->item('dias_recordatorios_calificacion_venta');
		$where_fecha = 'venta_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';

        $this->db->select('"RECORDATORIO - get_recordatorios_cliente_calificacion_venta", recordatorio_id as id, recordatorio_venta_id as venta_id, recordatorio_cliente_enviados as cliente_enviados', false);
		$this->db->from($this->table);
		$this->db->join('venta', 'recordatorio_venta_id = venta_id');
		$this->db->where('venta_puntaje_cliente IS NULL', null, false);
		$this->db->where('venta_estado <>', 'cancelado');
		$this->db->where('recordatorio_cliente_enviados < ', $limite);
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_recordatorios_local_calificacion_venta()
	{       
		$limite = $this->config->item('cantidad_recordatorios_calificacion');
		$dias = $this->config->item('dias_recordatorios_calificacion_venta');
		$where_fecha = 'venta_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';
		
        $this->db->select('"RECORDATORIO - get_recordatorios_local_calificacion_venta", recordatorio_id as id, recordatorio_venta_id as venta_id, recordatorio_local_enviados as local_enviados', false);
		$this->db->from($this->table);
		$this->db->join('venta', 'recordatorio_venta_id = venta_id');
		$this->db->where('venta_concretado_local', '');
		$this->db->where('venta_estado <>', 'cancelado');
		$this->db->where('recordatorio_local_enviados < ', $limite);
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_recordatorios_cliente_calificacion_turno()
	{       
		$limite = $this->config->item('cantidad_recordatorios_calificacion');
		$dias = $this->config->item('dias_recordatorios_calificacion_turno');
		$where_fecha = 'turno_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';

        $this->db->select('"RECORDATORIO - get_recordatorios_cliente_calificacion_turno", recordatorio_id as id, recordatorio_turno_id as turno_id, recordatorio_cliente_enviados as cliente_enviados', false);
		$this->db->from($this->table);
		$this->db->join('turno', 'recordatorio_turno_id = turno_id');
		$this->db->where('turno_puntaje_cliente IS NULL', null, false);
		$this->db->where('turno_estado <>', 'cancelado');
		$this->db->where('recordatorio_cliente_enviados < ', $limite);
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_recordatorios_local_calificacion_turno()
	{       
		$limite = $this->config->item('cantidad_recordatorios_calificacion');
		$dias = $this->config->item('dias_recordatorios_calificacion_turno');
		$where_fecha = 'turno_fecha <= subdate(current_date, INTERVAL ' . $dias . ' DAY)';
		
        $this->db->select('"RECORDATORIO - get_recordatorios_local_calificacion_turno", recordatorio_id as id, recordatorio_turno_id as turno_id, recordatorio_local_enviados as local_enviados', false);
		$this->db->from($this->table);
		$this->db->join('turno', 'recordatorio_turno_id = turno_id');
		$this->db->where('turno_concretado_local', '');
		$this->db->where('turno_estado <>', 'cancelado');
		$this->db->where('recordatorio_local_enviados < ', $limite);
		$this->db->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}		


	public function update($where, $data)
	{

		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_venta($venta_id)
	{
		$retorno = "";

		$this->db->where('recordatorio_venta_id', $venta_id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}
}