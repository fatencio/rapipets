<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Valoracion_model extends CI_Model {

	var $venta_turno = 'v_venta_turno';

	var $column_order = array('numero', 'fecha', 'local', 'puntaje_cliente', 'valoracion_cliente', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search = array('numero', 'fecha', 'local', 'valoracion_cliente');  // columnas con la opcion de busqueda habilitada

	var $order = array('fecha_creacion' => 'desc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query($cliente_id)
	{
	
        $this->db-> select('id, numero, fecha, local, puntaje_cliente, valoracion_cliente, valoracion_aprobada, tipo', FALSE);
        $this->db->from($this->venta_turno);

		$i = 0;


		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables($cliente_id = 0)
	{
		$this->_get_datatables_query($cliente_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($cliente_id = 0)
	{
		$this->_get_datatables_query($cliente_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($cliente_id = 0)
	{
		$this->db->from($this->venta_turno);

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		return $this->db->count_all_results();
	}


	// Valoraciones pendientes de aprobación

	var $column_order_pendientes = array('numero', 'cliente', 'local', 'puntaje_cliente', 'valoracion_cliente', null); 	// columnas con la opcion de orden habilitada
	
	var $column_search_pendientes = array('numero', 'cliente', 'local', 'valoracion_cliente');  // columnas con la opcion de busqueda habilitada

	var $order_pendientes = array('fecha_creacion' => 'desc'); // default order 

	function get_datatables_pendientes()
	{
		$this->_get_datatables_query_pendientes();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}	


	private function _get_datatables_query_pendientes()
	{
	
        $this->db-> select('id, cliente, numero, fecha, local, puntaje_cliente, valoracion_cliente, valoracion_aprobada, tipo', FALSE);
        $this->db->from($this->venta_turno);
        $this->db->where('valoracion_aprobada ', '');
        $this->db->where('puntaje_cliente IS NOT NULL', null, false);

		$i = 0;


		foreach ($this->column_search_pendientes as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); 
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_pendientes) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_pendientes[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_pendientes))
		{
			$order = $this->order_pendientes;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function count_filtered_pendientes()
	{
		$this->_get_datatables_query_pendientes();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_pendientes()
	{
		$this->db->from($this->venta_turno);
		$this->db->where('valoracion_aprobada ', '');
		$this->db->where('puntaje_cliente IS NOT NULL', null, false);

		return $this->db->count_all_results();
	}	
}