<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marca_model extends CI_Model {

	var $table = 'marca';
	var $table_animal = 'marca_animal';
	var $table_rubro = 'marca_rubro';
	var $v_rubro_animal = 'v_marca_rubro_animal';

	var $column_order = array("marca_nombre", "marca_rubros", "marca_animales", null); 	// columnas con la opcion de orden habilitada
	var $column_search = array("marca_nombre", "marca_animales", "marca_rubros"); 		// columnas con la opcion de busqueda habilitada

    var $order = array('marca_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{

        $this->db->select("marca_id as id, marca_nombre as nombre, marca_rubros as rubros, marca_animales as animales");
        $this->db->from($this->v_rubro_animal);

		$i = 0;

		// Filtros
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('marca_rubros', $_POST['columns'][1]['search']['value']);
		}

		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('marca_animales', $_POST['columns'][2]['search']['value']);
		}
				
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$data = new stdClass();

		$this->db->select('marca_id as id, marca_nombre as nombre');		
		$this->db->from($this->table);
		$this->db->where('marca_id',$id);
		$query = $this->db->get();

		$data = $query->row();

		// Recupera animales asociados a la marca
		$this->db->select('marca_animal_animal_id as animal_id');		
		$this->db->from($this->table_animal);
		$this->db->where('marca_animal_marca_id', $id);
		$query = $this->db->get();

		$data->animales = $query->result();

		// Recupera rubros asociados a la marca
		$this->db->select('marca_rubro_rubro_id as rubro_id');		
		$this->db->from($this->table_rubro);
		$this->db->where('marca_rubro_marca_id', $id);
		$query = $this->db->get();		

		$data->rubros = $query->result();

		return $data;
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('marca_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('marca_nombre', $name);
		$this->db->where('marca_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		
        $this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


    public function save_animal($data)
	{
		$this->db->insert($this->table_animal, $data);
		return $this->db->insert_id();
	}


    public function save_rubro($data)
	{
		$this->db->insert($this->table_rubro, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";
		
		$this->db->where('marca_id', $id);
		
		if ($this->db->delete($this->table)) {

         	// Elimina animales y rubros asignados a la marca
         	$this->delete_marca_animal_by_id($id);	
         	$this->delete_marca_rubro_by_id($id);	
		} 
		// Hubo un error al eliminar
		else{

			$retorno = $this->db->error();
		}

		return $retorno;		

	}
	

	public function delete_marca_animal_by_id($id)
	{
		$this->db->where('marca_animal_marca_id', $id);
		$this->db->delete($this->table_animal);
	}	


	public function delete_marca_rubro_by_id($id)
	{
		$this->db->where('marca_rubro_marca_id', $id);
		$this->db->delete($this->table_rubro);
	}	


	public function get_all()
	{       
		$this->db->select('marca_id as id, marca_nombre as nombre');
		$this->db->from($this->table);
		$this->db->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}		


	public function get_all_by_animal()
	{       
		$this->db->select('marca_id as id, marca_nombre as nombre, marca_animal_animal_id as animal_id, animal_nombre ');
		$this->db->join('marca_animal', 'marca_id = marca_animal_marca_id');
		$this->db->join('animal', 'marca_animal_animal_id = animal_id');
		$this->db->from($this->table);
		$this->db->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_by_animal_rubro_by_id()
	{       
		$this->db->select('
			marca_id AS id, 
			marca_nombre AS nombre, 
			group_concat(DISTINCT CONCAT(rubro_id, "\\\\", animal_id) ORDER BY rubro_nombre separator " ") AS rubro_animal', false)
		->from($this->table)		
		->join('marca_animal', 'marca_id = marca_animal_marca_id', 'left')
		->join('marca_rubro', 'marca_id = marca_rubro_marca_id', 'left')
		->join('animal', 'marca_animal_animal_id = animal_id', 'left')
		->join('rubro', 'marca_rubro_rubro_id = rubro_id', 'left')
		->group_by('marca_id')
		->group_by('marca_nombre')
		->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_by_animal_rubro()
	{       
		$this->db->select('
			marca_id AS id, 
			marca_nombre AS nombre, 
			group_concat(DISTINCT CONCAT(rubro_nombre, "\\\\\\\", animal_nombre) ORDER BY rubro_nombre separator " ") AS rubro_animal', false)
		->from($this->table)		
		->join('marca_animal', 'marca_id = marca_animal_marca_id', 'left')
		->join('marca_rubro', 'marca_id = marca_rubro_marca_id', 'left')
		->join('animal', 'marca_animal_animal_id = animal_id', 'left')
		->join('rubro', 'marca_rubro_rubro_id = rubro_id', 'left')
		->group_by('marca_id')
		->group_by('marca_nombre')
		->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_by_local_by_animal_rubro($local_id, $activo)
	{       
		$this->db->select('
			marca_id AS id, 
			marca_nombre AS nombre, 
			group_concat(DISTINCT CONCAT(rubro_nombre, "\\\\\\\", animal_nombre) ORDER BY rubro_nombre separator " ") AS rubro_animal', false)
		->from('local_articulo')		
        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
		->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
		->join('animal', 'articulo_animal_id = animal_id')
		->join('marca_animal', 'marca_animal_animal_id = articulo_animal_id and articulo_marca_id = marca_animal_marca_id')
		->join('marca', 'marca_animal_marca_id = marca_id')
		->join('marca_rubro', 'marca_id = marca_rubro_marca_id')
		->join('rubro', 'marca_rubro_rubro_id = rubro_id')
	    ->where('local_articulo_local_id', $local_id)
	    ->where('local_articulo_estado', $activo)		
		->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_by_local_by_animal($local_id, $activo, $presentacion_activa)
	{      
		$this->db->distinct() 
			->select('marca_id as id, marca_nombre as nombre, marca_animal_animal_id as animal_id, animal_nombre ')
			->from('local_articulo')		
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
			->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
			->join('animal', 'articulo_animal_id = animal_id')
			->join('marca_animal', 'marca_animal_animal_id = articulo_animal_id and articulo_marca_id = marca_animal_marca_id')
			->join('marca', 'marca_animal_marca_id = marca_id')
			->join('marca_rubro', 'marca_id = marca_rubro_marca_id')
			->join('rubro', 'marca_rubro_rubro_id = rubro_id')
	        ->where('local_articulo_local_id', $local_id)
	        ->where('local_articulo_estado', $activo)
	        ->order_by('marca_nombre');

	    if ($presentacion_activa != -1)
	        $this->db->where('local_articulo_estado_presentacion', $presentacion_activa);

		$query = $this->db->get();

		return $query->result();
	}


	public function get_by_local_animal_rubro($local_id, $activo, $presentacion_activa)
	{       
		$this->db->select('
			marca_id AS id, 
			marca_nombre AS nombre, 
			group_concat(DISTINCT CONCAT(rubro_nombre, "\\\\\\\", animal_nombre) ORDER BY rubro_nombre separator " ") AS rubro_animal,
			group_concat(DISTINCT CONCAT(rubro_nombre, "\\\\", animal_nombre) ORDER BY rubro_nombre separator " ") AS rubro_animal_ajax', false)
		->from('local_articulo')		
        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
		->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
		->join('animal', 'articulo_animal_id = animal_id')
		->join('marca_animal', 'marca_animal_animal_id = articulo_animal_id and articulo_marca_id = marca_animal_marca_id')
		->join('marca', 'marca_animal_marca_id = marca_id')
		->join('marca_rubro', 'marca_id = marca_rubro_marca_id')
		->join('rubro', 'marca_rubro_rubro_id = rubro_id')
        ->where('local_articulo_local_id', $local_id)
        ->where('local_articulo_estado', $activo)		
		->group_by('marca_id')
		->group_by('marca_nombre')
		->order_by('marca_nombre');

		if ($presentacion_activa != -1)
	        $this->db->where('local_articulo_estado_presentacion', $presentacion_activa);

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_by_articulos_activos_by_animal($activo)
	{      
		$this->db->distinct() 
			->select('marca_id as id, marca_nombre as nombre, marca_animal_animal_id as animal_id, animal_nombre ')
			->from('local_articulo')		
	        ->join('articulo_presentacion', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
			->join('articulo', 'articulo_presentacion_articulo_id = articulo_id')
			->join('animal', 'articulo_animal_id = animal_id')
			->join('marca_animal', 'marca_animal_animal_id = articulo_animal_id and articulo_marca_id = marca_animal_marca_id')
			->join('marca', 'marca_animal_marca_id = marca_id')
			->join('marca_rubro', 'marca_id = marca_rubro_marca_id')
			->join('rubro', 'marca_rubro_rubro_id = rubro_id')
	        ->where('local_articulo_estado', $activo)
	        ->order_by('marca_nombre');

		$query = $this->db->get();

		return $query->result();
	}	
}