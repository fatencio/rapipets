<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo_model extends CI_Model {

	var $table = 'articulo';
	var $table_presentacion = 'articulo_presentacion';
	var $table_tamanio = 'articulo_tamanio';
	var $table_raza = 'articulo_raza';

	var $column_order = array('rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', 'articulo_codigo', null); 
	var $column_search = array('rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', 'articulo_codigo'); 	

	var $order = array('rubro_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	/*
	public function fix()
	{
		$articulos = $this->get_fix();

		foreach ($articulos as $articulo) 
		{
			$data = array(
				'articulo_detalle' => html_entity_decode($articulo->articulo_detalle, ENT_QUOTES,'UTF-8')
			);

			$this->db->update($this->table, $data, array('articulo_id' => $articulo->articulo_id));
		}		
	}


	public function get_fix()
	{       
		$this->db->select('articulo_id, articulo_detalle');
		$this->db->from($this->table);
		$this->db->like('articulo_detalle', '&lt;');

		$query = $this->db->get();

		return $query->result();
	}
	*/

	// Listado de Artículos Geotienda
	// Se llama desde Backend -> Articulos
	private function _get_datatables_query()
	{
	    $this->db->select('articulo_id as id, articulo_nombre as nombre, articulo_codigo as codigo, animal_nombre as animal, rubro_nombre as rubro, articulo_medicados as medicados, articulo_imagen as imagen, marca_nombre as marca');
		$this->db->from($this->table);
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->where('articulo_creador', 'GeoTienda');
		
		$i = 0;
	
		// Busqueda por drop-down list en la primer columna 
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('rubro_nombre', $_POST['columns'][0]['search']['value']);
		}

		// Busqueda por drop-down list en la segunda columna 
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('animal_nombre', $_POST['columns'][1]['search']['value']);
		}

		// Busqueda por drop-down list en la tercer columna 
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('marca_nombre', $_POST['columns'][2]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		$this->db->where('articulo_creador', 'GeoTienda');
		return $this->db->count_all_results();
	}


	public function get_count_presentaciones($id)
	{       
		$this->db->select('articulo_presentacion_presentacion_id');
		$this->db->from($this->table_presentacion);
		$this->db->where('articulo_presentacion_articulo_id', $id);

		$query = $this->db->get();
		return $query->num_rows();	
	}


	public function get_by_id($id, $local_id = -1)
	{
		$this->db->select('articulo_id as id, articulo_nombre as nombre, articulo_codigo as codigo, articulo_detalle as detalle, articulo_animal_id as id_animal, articulo_rubro_id as id_rubro, articulo_medicados as medicados, articulo_edad as edad, articulo_marca_id as id_marca, marca_nombre as marca, articulo_imagen as imagen');
		$this->db->from($this->table);
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->where('articulo_id',$id);
		$query = $this->db->get();

		$data = $query->row();

		// Recupera presentaciones asociadas al articulo
		$this->db->select('articulo_presentacion_id, articulo_presentacion_presentacion_id as presentacion_id, presentacion_nombre as nombre, local_articulo_precio as precio');		
		$this->db->from($this->table_presentacion);
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id');
		$this->db->join('local_articulo', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id AND local_articulo_local_id = ' . $local_id, 'left');
		$this->db->where('articulo_presentacion_articulo_id', $id);
		$this->db->order_by('presentacion_orden');
		$query = $this->db->get();

		$data->presentaciones = $query->result();


		// Recupera tamanios asociados al articulo
		$this->db->select('articulo_tamanio_tamanio_id as tamanio_id');		
		$this->db->from($this->table_tamanio);
		$this->db->where('articulo_tamanio_articulo_id', $id);
		$query = $this->db->get();

		$data->tamanios = $query->result();		


		// Recupera razas asociadas al articulo
		$this->db->select('articulo_raza_raza_id as raza_id');		
		$this->db->from($this->table_raza);
		$this->db->where('articulo_raza_articulo_id', $id);
		$query = $this->db->get();

		$data->razas = $query->result();


		return $data;
	}


	public function get_presentaciones_by_id($id)
	{
		$presentaciones_actuales = array();

		// Recupera presentaciones asociadas al articulo
		$this->db->select('presentacion_id');		
		$this->db->from($this->table_presentacion);
		$this->db->join('presentacion', 'articulo_presentacion_presentacion_id = presentacion_id');
		$this->db->where('articulo_presentacion_articulo_id', $id);
		$this->db->order_by('presentacion_orden');
		$query = $this->db->get();

		foreach ($query->result_array() as $row)
		{
			$presentaciones_actuales[] = $row['presentacion_id'];
		}		

		return $presentaciones_actuales;
	}


	public function get_articulo_presentaciones($id)
	{       
		$this->db->select('articulo_presentacion_id');
		$this->db->from($this->table_presentacion);
		$this->db->where('articulo_presentacion_articulo_id', $id);

		$query = $this->db->get();
		return $query->result();
	}


	public function get_articulo_presentaciones_by_presentacion($id, $presentaciones)
	{       
		$this->db->select('articulo_presentacion_id');
		$this->db->from($this->table_presentacion);
		$this->db->where('articulo_presentacion_articulo_id', $id);
		$this->db->where_in('articulo_presentacion_presentacion_id', $presentaciones);

		$query = $this->db->get();
		return $query->result();
	}


	public function check_duplicated($name, $animal, $marca)
	{
		$this->db->from($this->table);
		$this->db->where('articulo_nombre', $name);
		$this->db->where('articulo_animal_id', $animal);
		$this->db->where('articulo_marca_id', $marca);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name, $animal, $marca)
	{
		$this->db->from($this->table);
		$this->db->where('articulo_id !=', $id);		
		$this->db->where('articulo_nombre', $name);
		$this->db->where('articulo_animal_id', $animal);
		$this->db->where('articulo_marca_id', $marca);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
//		$data['articulo_medicados'] = (is_null($data['articulo_medicados']) ? 0 : 1);

		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

 
    public function save_presentacion($data)
	{
		$this->db->insert($this->table_presentacion, $data);
		return $this->db->insert_id();
	}


    public function save_tamanio($data)
	{
		$this->db->insert($this->table_tamanio, $data);
		return $this->db->insert_id();
	}


    public function save_raza($data)
	{
		$this->db->insert($this->table_raza, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		//$data['articulo_medicados'] = (is_null($data['articulo_medicados']) ? 0 : 1);

		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('articulo_id', $id);
		if (!$this->db->delete($this->table)) { 
			$retorno = $this->db->error();

		}
		return $retorno;		
	}


	public function delete_articulo_presentacion_by_id($id, array $presentaciones_no_borrar)
	{
		$retorno = "";

		$this->db->where('articulo_presentacion_articulo_id', $id);

		// Cuando se actualiza un articulo, se eliminan las presentaciones asociadas excepto las que se mantienen
		if (sizeof($presentaciones_no_borrar) > 0)
			$this->db->where_not_in('articulo_presentacion_presentacion_id', $presentaciones_no_borrar);
		
		if (!$this->db->delete($this->table_presentacion)) { 
			$retorno = $this->db->error();

		}

		return $retorno;	
	}	


	public function delete_articulo_tamanio_by_id($id)
	{
		$this->db->where('articulo_tamanio_articulo_id', $id);
		$this->db->delete($this->table_tamanio);
	}	


	public function delete_articulo_raza_by_id($id)
	{
		$this->db->where('articulo_raza_articulo_id', $id);
		$this->db->delete($this->table_raza);
	}		


	// Valida si la imagen $name está siendo utilizada en algún artíulo
	public function check_imagen($name)
	{
		$this->db->from($this->table);
		$this->db->where('articulo_imagen', $name);

		return $this->db->count_all_results();
	}	
		

	// Recupera razas asociadas al articulo
	public function get_razas_by_articulo_id($id){

		$this->db->select('raza_nombre as nombre');		
		$this->db->from($this->table_raza);
		$this->db->join('raza', 'articulo_raza_raza_id = raza_id');
		$this->db->where('articulo_raza_articulo_id', $id);
		$query = $this->db->get();

		return $query->result();	
	}


	// Recupera tamaños asociadas al articulo
	public function get_tamanios_by_articulo_id($id){

		$this->db->select('tamanio_nombre as nombre');		
		$this->db->from($this->table_tamanio);
		$this->db->join('tamanio', 'articulo_tamanio_tamanio_id = tamanio_id');
		$this->db->where('articulo_tamanio_articulo_id', $id);
		$query = $this->db->get();

		return $query->result();	
	}


	// Listado de Artículos Geotienda que no están asociados a un local
	// Se llama desde el Dashboard de un local - Agregar artículo

  // Filtros
  public function get_items_filtro_articulos($item, $local_id, $filtros_aplicados)
  {   
    $filtros = "";
    $filtro_vista = "";
    $orden = 'nombre';

    $vista = 'v_geotienda_articulo_' . $item; 


	// Filtra articulos ya agregadas al local
	$articulos_en_local = 'SELECT articulo_presentacion_articulo_id 
						   FROM local_articulo 
						   JOIN articulo_presentacion ON articulo_presentacion_id = local_articulo_articulo_presentacion_id 
						   WHERE local_articulo_local_id = ' . $local_id;

    // FILTROS YA APLICADOS

    // Rubro
    if ($filtros_aplicados->rubro != -1) $filtros .= " AND articulo_rubro_id = " . $filtros_aplicados->rubro . " ";

    // Animal
    if ($filtros_aplicados->animal != -1) $filtros .= " AND articulo_animal_id = " . $filtros_aplicados->animal . " ";

    // Marca
    if ($filtros_aplicados->marca != -1) $filtros .= " AND articulo_marca_id = " . $filtros_aplicados->marca . " ";

    // Raza
    if ($filtros_aplicados->raza != -1) $filtros .= " AND articulo_raza_raza_id = " . $filtros_aplicados->raza . " ";

    // Tamaño
    if ($filtros_aplicados->tamanio != -1) $filtros .= " AND articulo_tamanio_tamanio_id = " . $filtros_aplicados->tamanio . " ";

    // Edad
    if ($filtros_aplicados->edad != -1) $filtros .= " AND articulo_edad = '" . $filtros_aplicados->edad . "' ";

    // Presentación
    if ($filtros_aplicados->presentacion != -1) $filtros .= " AND articulo_presentacion_presentacion_id = '" . $filtros_aplicados->presentacion . "' ";   

    // Medicados
    if ($filtros_aplicados->medicados != -1) $filtros .= " AND articulo_medicados = '" . $filtros_aplicados->medicados . "' ";    

    // FILTRO ACTUAL
    switch ($item) 
    {
      case 'rubros':
        $filtro_vista = " AND articulo_rubro_id = " . $vista . ".id ";
        break;
      
      case 'animales':
        $filtro_vista = " AND articulo_animal_id = " . $vista . ".id ";
        break;

      case 'marcas':
        $filtro_vista = " AND articulo_marca_id = " . $vista . ".id ";
        break;  

      case 'razas':
        $filtro_vista = " AND articulo_raza_raza_id = " . $vista . ".id ";
        break;    

      case 'tamanios':
        $filtro_vista = " AND articulo_tamanio_tamanio_id = " . $vista . ".id ";
        break;  

      case 'edades':
        $filtro_vista = " AND articulo_edad = " . $vista . ".id ";
        break;    

      case 'presentaciones':
        $filtro_vista = " AND articulo_presentacion_presentacion_id = " . $vista . ".id ";
        $orden = 'orden';
        break;  

      case 'medicados':
        $filtro_vista = " AND articulo_medicados = " . $vista . ".id ";
        break;                                    
    }


    $this->db->select('id, nombre, CONCAT(nombre, " (", count(nombre), ")") as nombre_cuenta') 
      ->from($vista)
      ->where("articulo_id IN (
        SELECT `articulo_id` 
        FROM `articulo`
        LEFT JOIN `articulo_presentacion` ON `articulo_presentacion_articulo_id` = `articulo_id`
        LEFT JOIN `articulo_raza` ON `articulo_raza_articulo_id` = `articulo_id`
        LEFT JOIN `articulo_tamanio` ON `articulo_tamanio_articulo_id` = `articulo_id`
        WHERE `articulo_id` NOT IN (" . $articulos_en_local . ") 
        AND `articulo_creador` = 'GeoTienda' "
        // . $filtro_vista
        . $filtros . ")", NULL, FALSE)
      ->group_by('id, nombre')
      ->order_by('count(nombre)', 'DESC')
      ->order_by($orden);     


    $query = $this->db->get();

    return $query->result();  
  }

	private function _get_datatables_query_no_en_local($local_id)
	{
		// Filtra articulos ya agregadas al local
		$articulos_en_local = 'SELECT articulo_presentacion_articulo_id 
							   FROM local_articulo 
							   JOIN articulo_presentacion ON articulo_presentacion_id = local_articulo_articulo_presentacion_id 
							   WHERE local_articulo_local_id = ' . $local_id;

		$this->db->distinct();
	    $this->db->select('articulo_id as id, 
	    				   articulo_nombre as nombre, 
	    				   articulo_codigo as codigo, 
	    				   animal_nombre as animal, 
	    				   rubro_nombre as rubro, 
	    				   articulo_medicados as medicados, 
	    				   articulo_imagen as imagen, 
	    				   marca_nombre as marca');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'articulo_presentacion_articulo_id = articulo_id');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('articulo_tamanio', 'articulo_id = articulo_tamanio_articulo_id', 'left');
		$this->db->join('articulo_raza', 'articulo_id = articulo_raza_articulo_id', 'left');
		$this->db->where('articulo_id NOT IN (' . $articulos_en_local . ')', NULL, FALSE);
		$this->db->where('articulo_creador', 'GeoTienda');

		$i = 0;

		// Animal
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('animal_id' , $_POST['columns'][0]['search']['value']);
		}	
	
		// Rubro
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('rubro_id' , $_POST['columns'][1]['search']['value']);
		}		

		// Marca
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('marca_id' , $_POST['columns'][2]['search']['value']);
		}	

		// Edad
		if($_POST['columns'][6]['search']['value'] != ''){
			$this->db->like('articulo_edad' , $_POST['columns'][6]['search']['value']);
		}	

		// Tamanio
		if($_POST['columns'][7]['search']['value'] != ''){
			$this->db->where('articulo_tamanio_tamanio_id' , $_POST['columns'][7]['search']['value']);
		}	

		// Raza
		if($_POST['columns'][8]['search']['value'] != ''){
			$this->db->where('articulo_raza_raza_id' , $_POST['columns'][8]['search']['value']);
		}			

		// Presentacion
		if($_POST['columns'][9]['search']['value'] != ''){
			$this->db->like('articulo_presentacion_presentacion_id' , $_POST['columns'][9]['search']['value']);
		}

		// Medicados
		if($_POST['columns'][10]['search']['value'] != ''){
			$this->db->where('articulo_medicados' , $_POST['columns'][10]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_no_en_local($local_id)
	{
		$this->_get_datatables_query_no_en_local($local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_no_en_local($local_id)
	{
		$this->_get_datatables_query_no_en_local($local_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_no_en_local($local_id)
	{
		// Filtra articulos ya agregadas al local
		$articulos_en_local = 'SELECT articulo_presentacion_articulo_id 
							   FROM local_articulo 
							   JOIN articulo_presentacion ON articulo_presentacion_id = local_articulo_articulo_presentacion_id 
							   WHERE local_articulo_local_id = ' . $local_id;

		$this->db->distinct();
	    $this->db->select('articulo_id as id, 
	    				   articulo_nombre as nombre, 
	    				   articulo_codigo as codigo, 
	    				   animal_nombre as animal, 
	    				   rubro_nombre as rubro, 
	    				   articulo_medicados as medicados, 
	    				   articulo_imagen as imagen, 
	    				   marca_nombre as marca');
		$this->db->from($this->table);
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->join('articulo_tamanio', 'articulo_id = articulo_tamanio_articulo_id', 'left');
		$this->db->join('articulo_raza', 'articulo_id = articulo_raza_articulo_id', 'left');
		$this->db->where('articulo_id NOT IN (' . $articulos_en_local . ')', NULL, FALSE);
		$this->db->where('articulo_creador', 'GeoTienda');

		return $this->db->count_all_results();
	}	


	/* ARTICULOS CREADOS POR LOS LOCALES */

	var $column_order_locales = array('local_nombre', 'rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre', null); 
	var $column_search_locales = array('local_nombre', 'rubro_nombre', 'animal_nombre', 'marca_nombre', 'articulo_nombre'); 	

	var $order_locales = array('local_nombre' => 'asc'); // default order 

	// Listado de Artículos creados por los locales
	// Se llama desde Backend -> Articulos Locales
	private function _get_datatables_query_locales()
	{
		$this->db->distinct();		
	    $this->db->select('local_id, local_nombre as local, articulo_id as id, articulo_nombre as nombre, animal_nombre as animal, rubro_nombre as rubro, articulo_medicados as medicados, articulo_imagen as imagen, marca_nombre as marca');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'articulo_presentacion_articulo_id = articulo_id');		
		$this->db->join('local_articulo', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->where('articulo_creador', 'Local');
		
		$i = 0;
	
		// FILTROS

		// Local
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('local_nombre', $_POST['columns'][0]['search']['value']);
		}

		// Rubro
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('rubro_nombre', $_POST['columns'][1]['search']['value']);
		}

		// Animal
		if($_POST['columns'][2]['search']['value'] != ''){
			$this->db->like('animal_nombre', $_POST['columns'][2]['search']['value']);
		}

		// Marca
		if($_POST['columns'][3]['search']['value'] != ''){
			$this->db->like('marca_nombre', $_POST['columns'][3]['search']['value']);
		}

		foreach ($this->column_search_locales as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search_locales) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order_locales[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order_locales))
		{
			$order = $this->order_locales;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables_locales()
	{
		$this->_get_datatables_query_locales();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered_locales()
	{
		$this->_get_datatables_query_locales();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all_locales()
	{
		$this->db->distinct();		
	    $this->db->select('local_id, local_nombre as local, articulo_id as id, articulo_nombre as nombre, animal_nombre as animal, rubro_nombre as rubro, articulo_medicados as medicados, articulo_imagen as imagen, marca_nombre as marca');
		$this->db->from($this->table);
		$this->db->join('articulo_presentacion', 'articulo_presentacion_articulo_id = articulo_id');		
		$this->db->join('local_articulo', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id');
		$this->db->join('local', 'local_articulo_local_id = local_id');
		$this->db->join('animal', 'articulo_animal_id = animal_id');
		$this->db->join('rubro', 'articulo_rubro_id = rubro_id');
		$this->db->join('marca', 'articulo_marca_id = marca_id', 'left');
		$this->db->where('articulo_creador', 'Local');
		
		return $this->db->count_all_results();
	}

	/* fin ARTICULOS CREADOS POR LOS LOCALES */		
}