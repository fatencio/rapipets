<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mediopagoitem_model extends CI_Model {

	var $table = 'medio_pago_item';

	var $column_order = array('medio_pago_item_nombre', 'medio_pago_nombre', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('medio_pago_item_nombre', 'medio_pago_nombre'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('medio_pago_item_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
         
		
		$this->db->select('medio_pago_item_id as id, medio_pago_item_nombre as nombre, medio_pago_nombre as medio_pago, medio_pago_item_imagen as imagen');
        $this->db->from($this->table);
		$this->db->join('medio_pago', 'medio_pago_item_medio_pago_id = medio_pago_id');

		$i = 0;
	
		// Busqueda por drop-down list en la primer columna
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('medio_pago_nombre', $_POST['columns'][0]['search']['value']);
		}
		
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
	
		$this->db->select('medio_pago_item_id as id, medio_pago_item_nombre as nombre, medio_pago_item_medio_pago_id as medio_pago_id, medio_pago_item_imagen as imagen');
		$this->db->from($this->table);
		$this->db->where('medio_pago_item_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name, $medio_pago)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_item_nombre', $name);
		$this->db->where('medio_pago_item_medio_pago_id', $medio_pago);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name, $medio_pago)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_item_nombre', $name);
		$this->db->where('medio_pago_item_medio_pago_id', $medio_pago);
		$this->db->where('medio_pago_item_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('medio_pago_item_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;	
	}


	public function get_by_medio_pago($medio_pago_id)
	{
		$this->db->select('medio_pago_item_id as id, medio_pago_item_nombre as nombre, medio_pago_item_medio_pago_id  as medio_pago_id, medio_pago_item_imagen as imagen');
		$this->db->from($this->table);
		$this->db->where('medio_pago_item_medio_pago_id', $medio_pago_id);
		$query = $this->db->get();

		return $query->result();
	}


	public function get_by_local_by_medio_pago($local_id, $medio_pago_id)
	{      
		$this->db->select('medio_pago_item_id as id, medio_pago_item_nombre as nombre, medio_pago_item_imagen as imagen')
			->from($this->table)
		    ->join('local_medio_pago', 'local_medio_pago_medio_pago_item_id = medio_pago_item_id')
			->where('local_medio_pago_local_id', $local_id)
			->where('local_medio_pago_medio_pago_id', $medio_pago_id)
	        ->order_by('medio_pago_item_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	// Valida si la imagen $name está siendo utilizada en algún artíulo
	public function check_imagen($name)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_item_imagen', $name);

		return $this->db->count_all_results();
	}	


	// Valida que el item no esté asociado a un local
	public function check_in_local($id)
	{
		$this->db->from($this->table)
			->join('local_medio_pago', 'local_medio_pago_medio_pago_item_id = medio_pago_item_id')
			->where('medio_pago_item_id', $id);

		return $this->db->count_all_results();
	}	

}