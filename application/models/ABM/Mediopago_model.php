<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mediopago_model extends CI_Model {

	var $table = 'medio_pago';

	var $column_order = array('medio_pago_nombre','medio_pago_estado','medio_pago_items', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('medio_pago_nombre','medio_pago_estado','medio_pago_items'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('medio_pago_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
		
	    $this->db->select('medio_pago_id as id, medio_pago_nombre as nombre, medio_pago_estado as estado, medio_pago_items as items, medio_pago_imagen as imagen');
		$this->db->from($this->table);
		
		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$this->db->select('count(medio_pago_item_id) as count_items, medio_pago_id as id, medio_pago_nombre as nombre, medio_pago_estado as estado, medio_pago_items as items, medio_pago_imagen as imagen');
		$this->db->from($this->table);
		$this->db->join('medio_pago_item', 'medio_pago_item_medio_pago_id = medio_pago_id', 'left');
		$this->db->where('medio_pago_id',$id);
		$this->db->group_by('medio_pago_id, medio_pago_nombre, medio_pago_estado, medio_pago_items, medio_pago_imagen');

		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_nombre', $name);
		$this->db->where('medio_pago_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$data['medio_pago_estado'] = (is_null($data['medio_pago_estado']) ? 0 : 1);
		$data['medio_pago_items'] = (is_null($data['medio_pago_items']) ? 0 : 1);

		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$data['medio_pago_estado'] = (is_null($data['medio_pago_estado']) ? 0 : 1);
		
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('medio_pago_id', $id);
		if (!$this->db->delete($this->table)) {
		  $retorno = $this->db->error();

		}
		return $retorno;
	}


	public function get_all($activo = FALSE)
	{       
		$this->db->select('medio_pago_id as id, medio_pago_nombre as nombre, medio_pago_estado as estado, medio_pago_imagen as imagen');
		$this->db->from($this->table);

		if($activo) $this->db->where('medio_pago_estado =', '1');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_items($con_items = FALSE, $activo = FALSE)
	{       
		$this->db->select('medio_pago_id as id, medio_pago_nombre as nombre, medio_pago_estado as estado, medio_pago_items as items, medio_pago_imagen as imagen');
		$this->db->from($this->table);

		if($con_items) $this->db->where('medio_pago_items =', '1');
		if($activo) $this->db->where('medio_pago_estado =', '1');

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_by_local($local_id)
	{       
		$this->db->distinct() 
			->select('medio_pago_id as id, medio_pago_nombre as nombre, medio_pago_estado as estado, medio_pago_imagen as imagen')
			->from($this->table)
        	->join('local_medio_pago', 'local_medio_pago_medio_pago_id = medio_pago_id')
			->where('local_medio_pago_local_id', $local_id)
			->order_by('medio_pago_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	// Valida si la imagen $name está siendo utilizada en algún medio de pago
	public function check_imagen($name)
	{
		$this->db->from($this->table);
		$this->db->where('medio_pago_imagen', $name);

		return $this->db->count_all_results();
	}	
}