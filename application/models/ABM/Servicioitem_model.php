<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicioitem_model extends CI_Model {

	var $table = 'servicio_item';

	var $column_order = array('servicio_item_nombre', 'servicio_nombre', 'animal_nombre', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('servicio_item_nombre', 'servicio_nombre', 'animal_nombre'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('servicio_item_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
         
		
		$this->db->select('servicio_item_id as id, servicio_item_nombre as nombre, servicio_nombre as servicio, animal_nombre as animal, servicio_item_imagen as imagen');
        $this->db->from($this->table);
		$this->db->join('servicio', 'servicio_item_servicio_id = servicio_id');
		$this->db->join('animal', 'servicio_item_animal_id = animal_id');

		$i = 0;
	
		// Busqueda por drop-down list en la primer columna
		if($_POST['columns'][0]['search']['value'] != ''){
			$this->db->like('servicio_nombre', $_POST['columns'][0]['search']['value']);
		}
		
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
	
		$this->db->select('servicio_item_id as id, servicio_item_nombre as nombre, servicio_item_detalle as detalle, servicio_item_servicio_id, servicio_item_animal_id, servicio_item_imagen as imagen');
		$this->db->from($this->table);
		$this->db->where('servicio_item_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('servicio_item_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('servicio_item_nombre', $name);
		$this->db->where('servicio_item_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('servicio_item_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;	
	}


	public function get_by_servicio($servicio_id)
	{
		$this->db->select('servicio_item_id as id, servicio_item_nombre as nombre, servicio_item_servicio_id');
		$this->db->from($this->table);
		$this->db->where('servicio_item_servicio_id', $servicio_id);
		$query = $this->db->get();

		return $query->result();
	}


	public function get_all_servicio()
	{      
		$this->db->distinct() 
			->select('servicio_item_id as id, servicio_item_nombre as nombre, servicio_id as servicio')
			->from($this->table)
			->join('servicio', 'servicio_item_servicio_id = servicio_id')
		    ->join('local_servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id')
	        ->order_by('servicio_item_nombre');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_by_local_by_servicio($local_id)
	{      
		$this->db->distinct() 
			->select('servicio_item_id as id, servicio_item_nombre as nombre, servicio_nombre as servicio')
			->from($this->table)
			->join('servicio', 'servicio_item_servicio_id = servicio_id')
		    ->join('local_servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id')
			->where('local_servicio_item_local_id', $local_id)
	        ->order_by('servicio_item_nombre');

		$query = $this->db->get();

		return $query->result();
	}

	public function get_animales_by_local($local_id)
	{      
		$this->db->distinct() 
			->select('animal_nombre as nombre')
			->from($this->table)
			->join('animal', 'servicio_item_animal_id = animal_id')
		    ->join('local_servicio_item', 'local_servicio_item_servicio_item_id = servicio_item_id')
			->where('local_servicio_item_local_id', $local_id)
	        ->order_by('animal_nombre');

		$query = $this->db->get();

		return $query->result();
	}




	// Valida si la imagen $name está siendo utilizada en algún artíulo
	public function check_imagen($name)
	{
		$this->db->from($this->table);
		$this->db->where('servicio_item_imagen', $name);

		return $this->db->count_all_results();
	}	

}