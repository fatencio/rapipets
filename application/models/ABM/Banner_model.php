<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner_model extends CI_Model {

	var $table = 'banner';

	var $column_order = array('banner_nombre', 'banner_texto', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('banner_nombre', 'banner_texto'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('banner_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
         	
		$this->db->select('banner_id as id, banner_nombre as nombre, banner_texto as texto, banner_color as color, banner_fondo as fondo, banner_global as global');
        $this->db->from($this->table);

		$i = 0;
	
		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);
		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$this->db->select('banner_id as id, banner_nombre as nombre, banner_texto as texto, banner_color as color, banner_fondo as fondo, banner_global as global');
		$this->db->from($this->table);
		$this->db->where('banner_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function get_by_local($local_id)
	{
		$this->db->select('banner_id as id, banner_nombre as nombre, banner_texto as texto, banner_color as color, banner_fondo as fondo, banner_global as global');
		$this->db->from($this->table);
		$this->db->join('local', 'banner.banner_id = local.local_banner_id');
		$this->db->where('local_id',$local_id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('banner_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('banner_nombre', $name);
		$this->db->where('banner_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function update_all($data)
	{
		$this->db->update($this->table, $data);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{   

		$retorno = "";
		$this->db->where('banner_id', $id);
		 if (!$this->db->delete($this->table)) {
            $retorno = $this->db->error();

		 }

		 return $retorno;
	}
	

	public function get_all()
	{       
		$this->db->select('banner_id as id, banner_nombre as nombre, banner_texto as texto, banner_color as color, banner_fondo as fondo');
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result();
	}		


	public function get_global()
	{
		$this->db->select('banner_id as id, banner_nombre as nombre, banner_texto as texto, banner_color as color, banner_fondo as fondo');
		$this->db->from('banner');
		$this->db->where('banner_global', 1);

		$query = $this->db->get();

		return $query->row();
	}
}