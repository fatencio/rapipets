<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mascota_model extends CI_Model {

	var $table = 'mascota';

	var $order = array('mascota_nombre' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query($cliente_id)
	{
	
		$this->db->select('mascota_id as id, CONCAT("M", LPAD(mascota_id, 3, "0")) AS codigo, mascota_nombre as nombre, local_nombre as local, mascota_foto as foto, animal_nombre as tipo', false);
		$this->db->join('local', 'local_id = mascota_local_id', 'left');
		$this->db->join('animal', 'animal_id = mascota_animal_id');   
		
        $this->db->from($this->table);

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('mascota_cliente_id', $cliente_id);

		$order = $this->order;
		$this->db->order_by(key($order), $order[key($order)]);
	}


	function get_datatables($cliente_id = 0)
	{
		$this->_get_datatables_query($cliente_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	private function _get_datatables_query_local($local_id)
	{
	
		$this->db->select('mascota_id as id, CONCAT("M", LPAD(mascota_id, 3, "0")) AS codigo, mascota_nombre as nombre, CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente, animal_nombre as tipo', false);
		$this->db->join('cliente', 'cliente_id = mascota_cliente_id');
		$this->db->join('animal', 'animal_id = mascota_animal_id');   
		
        $this->db->from($this->table);

		// Filtra por local si corresponde
		if ($local_id != 0) $this->db->where('mascota_local_id', $local_id);

		$order = $this->order;
		$this->db->order_by(key($order), $order[key($order)]);
	}


	function get_datatables_local($local_id = 0)
	{
		$this->_get_datatables_query_local($local_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($cliente_id = 0)
	{
		$this->_get_datatables_query($cliente_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($cliente_id = 0)
	{
		$this->db->from($this->table);

		// Filtra por cliente si corresponde
		if ($cliente_id != 0) $this->db->where('cliente_id', $cliente_id);

		return $this->db->count_all_results();
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
        return $this->db->insert_id();
	}


	public function get_by_id($id)
	{       
        $this->db-> select('mascota_id as id, 
        					CONCAT("M", LPAD(mascota_id, 3, "0")) AS codigo, 
        					mascota_nombre as nombre,
        					mascota_animal_id as tipo_id,
        					animal_nombre as tipo,
        					mascota_nacimiento as nacimiento, 
        					mascota_raza as raza,
        					mascota_sexo as sexo,
        					mascota_pelaje as pelaje,
        					mascota_talla as talla,
        					mascota_foto as foto,
        					local_id,
        					local_nombre as local, 
        					cliente_id,
        					CONCAT(cliente_nombre, " " , cliente_apellido) AS cliente', false);

        $this->db->from($this->table);
        $this->db->join('animal', 'animal_id = mascota_animal_id');   
        $this->db->join('local', 'local_id = mascota_local_id', 'left');
        $this->db->join('cliente', 'cliente_id = mascota_cliente_id');     
        $this->db->where('mascota_id', $id);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_by_vacuna_id($vacuna_id)
	{       
        $this->db-> select('vacuna_mascota_id as id')
        	->from('vacuna')
        	->where('vacuna_id', $vacuna_id);

		$query = $this->db->get();

		return $query->row();
	}


	public function update($where, $data)
	{    			
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('mascota_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}
		else
		{
			// Elimina vacunas asociadas a la mascota
			$this->db->where('vacuna_mascota_id', $id);

			if (!$this->db->delete('vacuna')){
				$retorno = $this->db->error();
			}			
		}

		return $retorno;		
	}
	

	// Valida si la foto $name está siendo utilizada en alguna mascota
	public function check_foto($name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_foto', $name);

		return $this->db->count_all_results();
	}	

	/*
	public function get_by_id($id)
	{       
		$this->db->select('mascota_id as id, mascota_nombre as nombre, mascota_conraza as conraza, mascota_contamanios as contamanios');
		$this->db->from($this->table);
		$this->db->where('mascota_id',$id);
		$query = $this->db->get();

		return $query->row();
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('mascota_nombre', $name);
		$this->db->where('mascota_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function get_all()
	{       
		$this->db->select('mascota_id as id, mascota_nombre as nombre, mascota_conraza as conraza, mascota_contamanios as contamanios');
		$this->db->from($this->table);
		$query = $this->db->get();

		return $query->result();
	}	
	*/
}