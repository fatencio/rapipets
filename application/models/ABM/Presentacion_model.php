<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentacion_model extends CI_Model {

	var $table = 'presentacion';
	var $table_rubro = 'presentacion_rubro';
	var $v_presentacion_rubro = 'v_presentacion_rubro';

	var $column_order = array('presentacion_nombre', 'presentacion_rubros', null); 	// columnas con la opcion de orden habilitada
	var $column_search = array('presentacion_nombre', 'presentacion_rubros'); 		// columnas con la opcion de busqueda habilitada

	var $order = array('presentacion_orden' => 'asc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query()
	{
		
	    $this->db->select('presentacion_id as id, presentacion_nombre as nombre, presentacion_rubros as rubros');
		$this->db->from($this->v_presentacion_rubro);

		// La presentacion 1 (Unica Presentacion) queda oculta (se utiliza para articulos sin presentacion, accesorios por ej)
		$this->db->where('presentacion_id <>', 1);
		
		$i = 0;
	
		// Filtros
		if($_POST['columns'][1]['search']['value'] != ''){
			$this->db->like('presentacion_rubros', $_POST['columns'][1]['search']['value']);
		}

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if(count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
		
		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}


	function get_datatables()
	{
		$this->_get_datatables_query();

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all()
	{
		$this->db->from($this->table);

		// La presentacion 1 (Unica Presentacion) queda oculta (se utiliza para articulos sin presentacion, accesorios por ej)
		$this->db->where('presentacion_id <>', 1);

		return $this->db->count_all_results();
	}


	public function get_by_id($id)
	{
		$data = new stdClass();

		$this->db->select('presentacion_id as id, presentacion_nombre as nombre');
		$this->db->from($this->table);
		$this->db->where('presentacion_id',$id);
		$query = $this->db->get();

		$data = $query->row();

		// Recupera rubros asociados a la presentacion
		$this->db->select('presentacion_rubro_rubro_id as rubro_id');		
		$this->db->from($this->table_rubro);
		$this->db->where('presentacion_rubro_presentacion_id', $id);
		$query = $this->db->get();		

		$data->rubros = $query->result();		

		return $data;
	}


	public function check_duplicated($name)
	{
		$this->db->from($this->table);
		$this->db->where('presentacion_nombre', $name);

		return $this->db->count_all_results();
	}


	public function check_duplicated_edit($id, $name)
	{
		$this->db->from($this->table);
		$this->db->where('presentacion_nombre', $name);
		$this->db->where('presentacion_id !=', $id);

		return $this->db->count_all_results();
	}	


	public function save($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}


    public function save_rubro($data)
	{
		$this->db->insert($this->table_rubro, $data);
		return $this->db->insert_id();
	}


	public function update($where, $data)
	{
		$this->db->update($this->table, $data, $where);
		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";
		$this->db->where('presentacion_id', $id);

		if ($this->db->delete($this->table)) 
		{
         	// Elimina rubros asignados a la presentacion
         	$this->delete_presentacion_rubro_by_id($id);	
		} 
		// Hubo un error al eliminar
		else
		{
			$retorno = $this->db->error();
		}

		return $retorno;
	}


	public function delete_presentacion_rubro_by_id($id)
	{
		$this->db->where('presentacion_rubro_presentacion_id', $id);
		$this->db->delete($this->table_rubro);
	}	


	public function get_all()
	{       
		$this->db->select('presentacion_id as id, presentacion_nombre as nombre');
		$this->db->from($this->table);

		// La presentacion 1 (Unica Presentacion) queda oculta (se utiliza para articulos sin presentacion, accesorios por ej)
		$this->db->where('presentacion_id <>', 1);
		$this->db->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();
	}		


	public function get_all_by_rubro()
	{       
		$this->db->select('presentacion_id as id, presentacion_rubro_rubro_id as rubro_id, presentacion_nombre as nombre')
			->from($this->table)
			->join('presentacion_rubro', 'presentacion_id = presentacion_rubro_presentacion_id');

		// La presentacion 1 (Unica Presentacion) queda oculta (se utiliza para articulos sin presentacion, accesorios por ej)
		$this->db->where('presentacion_id <>', 1);
		$this->db->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();
	}


	public function get_by_local($local_id, $activo)
	{      
		$this->db->distinct() 
			->select('presentacion_id as id, presentacion_nombre as nombre')
			->from($this->table)
	        ->join('articulo_presentacion', 'articulo_presentacion_presentacion_id = presentacion_id')
			->join('local_articulo', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->where('local_articulo_local_id', $local_id)
	        ->where('local_articulo_estado', $activo)
	        ->where('local_articulo_estado_presentacion', $activo)
	        ->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();
	}	


	public function get_by_articulos_activos($activo)
	{      
		$this->db->distinct() 
			->select('presentacion_id as id, presentacion_nombre as nombre')
			->from($this->table)
	        ->join('articulo_presentacion', 'articulo_presentacion_presentacion_id = presentacion_id')
			->join('local_articulo', 'local_articulo_articulo_presentacion_id = articulo_presentacion_id')
	        ->where('local_articulo_estado', $activo)
	        ->order_by('presentacion_orden');

		$query = $this->db->get();

		return $query->result();
	}	
}