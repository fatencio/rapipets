<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacuna_model extends CI_Model {

	var $table = 'vacuna';

	var $order = array('vacuna_fecha' => 'desc'); // default order 


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	private function _get_datatables_query($mascota_id)
	{
	
		$this->db->select("vacuna_id as id, vacuna_fecha as fecha, COALESCE(vacuna_fecha_proxima, '') as fecha_proxima, vacuna_nombre as nombre, local_nombre as local, vacuna_aplicada as aplicada ", false);
		$this->db->join('local', 'local_id = vacuna_local_id');
        $this->db->from($this->table);

		// Filtra por mascota si corresponde
		if ($mascota_id != 0) $this->db->where('vacuna_mascota_id', $mascota_id);

		$order = $this->order;
		$this->db->order_by(key($order), $order[key($order)]);
	}


	function get_datatables($mascota_id = 0)
	{
		$this->_get_datatables_query($mascota_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	private function _get_datatables_query_local($mascota_id)
	{
	
		$this->db->select("vacuna_id as id, vacuna_fecha as fecha, COALESCE(vacuna_fecha_proxima, '') as fecha_proxima, vacuna_nombre as nombre, local_nombre as local, vacuna_aplicada as aplicada ", false);
		$this->db->join('local', 'local_id = vacuna_local_id');
        $this->db->from($this->table);

		// Filtra por mascota si corresponde
		if ($mascota_id != 0) $this->db->where('vacuna_mascota_id', $mascota_id);

		$order = $this->order;
		$this->db->order_by(key($order), $order[key($order)]);
	}


	function get_datatables_local($mascota_id = 0)
	{
		$this->_get_datatables_query_local($mascota_id);

		if($_POST['length'] != -1)
		$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();

		return $query->result();
	}


	function count_filtered($mascota_id = 0)
	{
		$this->_get_datatables_query($mascota_id);
		$query = $this->db->get();
		return $query->num_rows();
	}


	public function count_all($mascota_id = 0)
	{
		$this->db->from($this->table);

		// Filtra por cliente si corresponde
		if ($mascota_id != 0) $this->db->where('mascota_id', $mascota_id);

		return $this->db->count_all_results();
	}


	public function save($data)
	{
		$this->db->insert($this->table, $data);
        return $this->db->insert_id();
	}


	public function get_proxima_por_mascota($mascota_id)
	{       
		$date = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));
 		$hoy = $date->format('Ymd');

		$this->db->select_min('vacuna_fecha_proxima', 'fecha');        
        $this->db->from($this->table);  
        $this->db->where('vacuna_mascota_id', $mascota_id);
        $this->db->where('vacuna_fecha_proxima >=', $hoy);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_proxima($id)
	{       
		$date = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	
 		$hoy = $date->format('Ymd');

		$this->db->select_min('vacuna_fecha_proxima', 'fecha');        
        $this->db->from($this->table);  
        $this->db->where('vacuna_id', $id);
        $this->db->where('vacuna_fecha_proxima >=', $hoy);

		$query = $this->db->get();

		return $query->row();
	}


	public function get_by_id($id)
	{       
        $this->db-> select("vacuna_id as id, 
                            vacuna_nombre as nombre,
                            vacuna_fecha as fecha, 
                            vacuna_fecha_proxima as fecha_proxima, 
                            vacuna_veterinario as veterinario,
                            vacuna_aplicada as aplicada,
                            vacuna_observaciones as observaciones,
                            vacuna_peso as peso,
                            vacuna_mascota_id as mascota_id,
                            mascota_nombre as mascota,
                            cliente_id,
                            cliente_avatar,
        					cliente_nombre,
        					CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, 
                            local_id,
                            local_avatar,
                            local_nombre as local,
                            local_nombre_titular,
        					local_apellido_titular, 
        					local_telefono,
        					local_direccion,
        					local_localidad", false);
        $this->db->from($this->table);
        $this->db->join('local', 'local_id = vacuna_local_id');
        $this->db->join('mascota', 'mascota_id = vacuna_mascota_id');
        $this->db->join('cliente', 'cliente_id = mascota_cliente_id');
        $this->db->where('vacuna_id', $id);

		$query = $this->db->get();

		return $query->row();
	}


	public function update($where, $data)
	{    			
		$this->db->update($this->table, $data, $where);

		return $this->db->affected_rows();
	}


	public function delete_by_id($id)
	{
		$retorno = "";

		$this->db->where('vacuna_id', $id);
		
		if (!$this->db->delete($this->table)){
			$retorno = $this->db->error();
		}

		return $retorno;		
	}
	

	// Vacunas con fecha cercana
	public function get_vacunas_cercanas()
	{       
		$dias = $this->config->item('dias_recordatorio_vacuna');
		$where_fecha = 'vacuna_fecha_proxima = adddate(current_date, INTERVAL ' . $dias . ' DAY)';

        $this->db->select( "vacuna_id as id,
        					vacuna_nombre as nombre,
        					vacuna_fecha as fecha, 
        					vacuna_fecha_proxima as fecha_proxima, 
        					cliente_id,
        					cliente_nombre,
        					CONCAT(cliente_nombre, ' ' , cliente_apellido) as cliente, 
        					cliente_email,  
        					local_id,      					
        					local_nombre as local, 
        					local_email,         					
        					mascota_nombre as mascota,
        					vacuna_observaciones as observaciones", false)
			->from($this->table)
	        ->join('local', 'local_id = vacuna_local_id')
	        ->join('mascota', 'mascota_id = vacuna_mascota_id')
	        ->join('cliente', 'cliente_id = mascota_cliente_id')   
			->where($where_fecha, null, false);

		$query = $this->db->get();

		return $query->result();
	}
}