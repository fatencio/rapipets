<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_beneficios')">
        <i class="si si-camera text-white"></i>
    </button> 

<?php endif ?>


<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Beneficios de GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Beneficios de GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span> 


<section class="content content-full content-boxed">  
    <div class="row">
        <div class="text-center push-50-t push-50 col-sm-6">
            <img class="img-responsive" src="<?php echo BASE_PATH ?>/assets/img/frontend/beneficios.png" alt="Beneficios de GeoTienda">
        </div> 

        <div class="push-30-t push-50  col-sm-6">    
            <div class="bg-white beneficios">
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Realizá tu pedido de compra o reserva de forma simple, segura y 100% gratuita.</div>
                </div>
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Evitá llamadas telefónicas, líneas ocupadas o números inexistentes.</div>
                </div>
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Revisá la calificación de otros usuarios.</div>
                </div>    

                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Pedí el alimento de tu mascota o reservá el turno del servicio que él necesite las 24 horas del día, desde tu computadora, celular o tablet.</div>
                </div>                                    
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Chequeá todas las formas de pago que brinda cada tienda.</div>
                </div>             
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Encontrá todos los Pet Shop y Veterinarias en 2 pasos.</div>
                </div>             
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Descubrí infinidades de artículos ordenados por rubro.</div>
                </div>    
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Encontrá descuentos y promociones.</div>
                </div>             
                <div class="push-30">
                    <div class="pull-left push-10-r push--3-t push-20"><img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita_xs.png" alt="GeoTienda"></div><div class="push-10-t h5 text-celeste">Obtené una cartilla de vacunación digital para tu mascota totalmente gratuita, donde tu veterinario de confianza ingresará las vacunas aplicadas y podrás ver el historial de las mismas.  </div>
                </div>                                           
            </div>  
        </div>
    </div>

    <div class="push-20-t push-20">
    	<div class="h3 push-20-t text-muted text-center">¿Todavía no estás registrado?</div>
    </div>   
    <div class="centrado push-50">
        <span class="h4 push-10-t pull-right push-10-r">
            <a href="javascript:void(0);" id="btn_register" class="btn btn-lg btn-geotienda" onclick="return show_register();">Creá tu cuenta</a>
        </span>    
    </div>          
</section>