<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_contacto')">
        <i class="si si-camera text-white"></i>
    </button>   
<?php endif ?>

<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Contacto</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Contacto</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<section class="content content-full content-boxed form-contacto">  
    <div class="push-40">
        <div class="h3 push-20-t text-gris text-center">¿Cómo podemos ayudarte?</div>
        <div class="h5 push-20-t text-gris text-center">Por consultas o inconvenientes completá el formulario de contacto o comunicate con nosotros por mail (<a href="mailto: contacto@geotienda.com">contacto@geotienda.com</a>)</div>
    </div>   

    <!-- FORM CONSULTA -->
    <div>
        <form class="js-validation-login form-horizontal push-30-t push-30" action="#" id="form_consulta">      
            <!-- Contacto Error -->
            <div class="alert alert-danger alert-dismissable" id="div_consulta_error" style="display:none">
                <small id="consulta_error_message"></small>                 
            </div>
            <!-- END Contacto Error -->                            
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                        <input class="form-control" type="text" id="consulta-motivo" name="consulta-motivo" placeholder="Motivo de la consulta" value="<?php if (isset($consulta_motivo)) echo $consulta_motivo; ?>"  required> 
                </div>
            </div>       
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                    <input class="form-control" type="text" id="consulta-nombre" name="consulta-nombre" placeholder="Tu Nombre y Apellido o Empresa" value="<?php if (isset($consulta_nombre)) echo $consulta_nombre; ?>"  required>
                </div>
            </div>                                                       
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                    <input class="form-control" type="text" id="consulta-email" name="consulta-email" placeholder="E-mail" value="<?php if (isset($consulta_email)) echo $consulta_email; ?>"  required>
                </div>
            </div>
            <div class="form-group form-group-login">
                <div class="col-xs-12">
                    <input class="form-control" type="telefono" id="consulta-telefono" name="consulta-telefono" placeholder="Teléfono" value="<?php if (isset($consulta_telefono)) echo $consulta_telefono; ?>"  required>
                </div>
            </div>
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                    <textarea class="form-control" id="consulta-mensaje" name="consulta-mensaje" placeholder="Mensaje" value="<?php if (isset($consulta_mensaje)) echo $consulta_mensaje; ?>"  rows="4" required></textarea>
                </div>
            </div>

            <div class="obligatorios"><span>*</span> Campos obligatorios</div>

            <div class="form-group form-group-login">
                 <div id="div_recaptcha_contacto" class="g-recaptcha" data-sitekey="6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF"></div>
            </div>
        </form>   
        <div class="form-group form-group-login push-50-t push-50">
              <div class="text-center">
                <button class="btn btn-lg btn-geotienda push-10-r" id="btn_enviar_consulta" onclick="enviar_consulta()">&nbsp;&nbsp;Enviar&nbsp;&nbsp;</button>
            </div>                                                                    
        </div>
     </div>   
    <!-- fin FORM CONSULTA -->
      

    <!-- Confirmacion Consulta OK Modal -->
    <div class="modal fade" id="modal_consulta_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-content geotienda-modal-content">
                        <ul class="block-options login-close" >
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>                        
                        <h2 class="text-center text-naranja">Consulta enviada con éxito</h2>
                        <br/>
                        <h3 class="font-w300 text-center">Muy pronto nos pondremos en contacto para responder tus inquietudes.</h3>

                        <!--
                        <h5 class="font-w300 text-center text-muted push-20-t">Si no te llegó un email al buzón de entrada, por favor revisá tu correo no deseado.</h5>                        
                        -->
                        <br/><br/>

                        <div class="text-center">
                            <a href="<?php echo BASE_PATH ?>" id="btn_consulta_ok" class="btn btn-lg btn-info btn-geotienda-celeste" >Aceptar</a>
                        </div>
                        <br/>


                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- FIN Confirmacion Consulta OK Modal -->

</section>

<script>


    $(document).ready(function() {

        $('#consulta-motivo').focus();

    });

    /* FORM CONSULTA */


    // Al mostrar el modal de contacto exitoso, hace foco en el boton 'Aceptar'
    $('#modal_consulta_ok').on('shown.bs.modal', function () {
        $('#btn_consulta_ok').focus();
    })


    /*
    function show_consulta(){

        $('#consulta-motivo').val('');
        $('#consulta-nombre').val('');
        $('#consulta-email').val('');
        $('#consulta-telefono').val('');
        $('#consulta-mensaje').val('');
        $('#div_consulta_error').hide();

        $('#modal_consulta').modal('show');

    }  
    */

    // Tecla enter en inputs de form de consulta
    $('#consulta-motivo').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-motivo').val() != '') $('#consulta-nombre').focus();
        }
    });


    $('#consulta-nombre').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-nombre').val() != '') $('#consulta-email').focus();
        }
    });


    $('#consulta-email').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-email').val() != '') $('#consulta-telefono').focus();
        }
    });


    $('#consulta-telefono').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-telefono').val() != '') $('#consulta-mensaje').focus();
        }
    });


    /*
    $('#consulta-mensaje').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-mensaje').val() != '') enviar_consulta();
        }
    });    
    */

    function enviar_consulta()
    {
        var url = "<?php echo BASE_PATH ?>/Inicio/consulta";

        $('#btn_enviar_consulta').html('Enviando...');
        $('#btn_enviar_consulta').attr('disabled', true); 

        $.ajax({
            url : url ,
            type: "POST",
            data: $('#form_consulta').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                // Consulta ok
                if(data.status) 
                {
                    $('#modal_consulta_ok').modal('show');

                    $('#btn_enviar_consulta').html('Enviar');
                    $('#btn_enviar_consulta').attr('disabled', false);                          
                }
                // Fallo en consulta
                else
                {
                    mensaje_error = '';

                    for (var i = 0; i < data.error.mensaje.length; i++) 
                    {
                        mensaje_error += data.error.mensaje[i] + '<br/>';
                    }

                    $('#consulta_error_message').html(mensaje_error);
                    $('#div_consulta_error').show();      

                    $('#btn_enviar_consulta').html('Enviar');
                    $('#btn_enviar_consulta').attr('disabled', false);                                               
                }

            },
            // Error no manejado
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_error_sistema').modal('show');
                console.log(errorThrown);

                $('#btn_enviar_consulta').html('Enviar');
                $('#btn_enviar_consulta').attr('disabled', false);                   
            }
        });
    }
/* fin FORM CONSULTA */
</script>