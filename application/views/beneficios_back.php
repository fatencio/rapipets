<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_beneficios')">
        <i class="si si-camera text-white"></i>
    </button> 

<?php endif ?>


<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Beneficios de GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Beneficios de GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span> 


<section class="content content-full content-boxed">  
    <div class="text-center push-50-t push-50">
        <img class="img-responsive hidden-xs" src="<?php echo BASE_PATH ?>/assets/img/frontend/beneficios.jpg" alt="Beneficios de GeoTienda">
    </div> 

    <div class="push-30-t push-50 visible-xs">    
        <div class="push-20">

            <div class="text-center h3 font-w600">Beneficios para Pet Shops y Veterinarias</div>
            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-rocket"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Nuevo canal de ventas</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Ingresar a una nueva cartera de clientes que utilizan diariamente la plataforma.</div>
                    <div class="push-10">Encontrar nuevos clientes en tu zona de entrega que buscan comprar un producto o reservar un turno de un servicio online.</div>
                    <div class="push-10">Publicidad online gratuita: los Pet Shops o Veterinarias podrán subir fotos y descripciones de productos que vendan y servicios que brindan.</div>
                </div>
            </div>

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-star"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Compromiso GeoTienda</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Incrementar la publicidad de su negocio.</div>
                    <div class="push-10">Brindarle una plataforma para su Pet Shop o Veterinaria abierto las 24hs. los 365 días del año.</div>
                    <div class="push-10">Atención personalizada para cada uno de los negocios adheridos.</div>
                </div>
            </div>    

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-clock"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Gestión de entregas a domicilio</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Gestionar los pedidos recibidos de compra y los procesos logísticos de su negocio.</div>
                    <div class="push-10">Recibir pedidos simultáneos, sin perder a clientes por pérdida de número telefónico o lineas ocupadas.</div>
                </div>
            </div>      

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-screen-smartphone"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Tu catálogo online</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Ofrecer al cliente un catálogo de productos y servicios completo.</div>
                    <div class="push-10">Diferenciar tu negocio destacando promociones y descuentos.</div>
                </div>
            </div>     


            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-emoticon-smile"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Sin costos fijos</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Abonan cargos solamente por los pedidos recibidos de compra y reserva de turnos.</div>
                </div>
            </div>   
        </div>   
        <div class="text-center push-50-t push-50">
            <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
        </div>  
    </div>

    <div class="push-20-t push-20">
    	<div class="h3 push-20-t text-muted text-center">Adherí tu comercio a GeoTienda</div>
    </div>   
    <div class="centrado push-50">
        <span class="h4 push-10-t pull-right push-10-r">
            ¿Tenés una tienda?<br />
            <a class="push-20-l" href="javascript:void(0);" onclick="return show_registro_negocio();">Registrala acá</a>
        </span>    
        <span class="pull-right push-20-r">          
            <img src="<?php echo BASE_PATH ?>/assets/img/frontend/local.png" alt="Registra tu local">
        </span>
    </div>          
</section>