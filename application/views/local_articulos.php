<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_local_articulos')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>

<!-- PORTADA version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>    
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada" class="text-center">
            <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
        </div>
    </div>
</span>

<!-- PORTADA version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>    
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada" class="text-center">
            <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">
        </div>
    </div>
</span>

<!-- BUSCADOR version MOBILE -->
<section class="content content-full content-boxed portada-title visible-xs" style="top: <?php echo $portada_top_mobile; ?>px;">
    <div class="row">
        
        <!-- Volver al listado de locales -->                    
        <div class="col-xs-7">
            <a class="btn btn-listado-locales push-20-t" id="btn_listado_locales_m" onclick="buscar_locales_mini_m();"><i class="si si-arrow-left push-5-r" style="color:white"></i>Tiendas</a>                                                 
        </div>                          

        <!-- Boton Nueva Busqueda -->   
        <div class="col-xs-5">
            <button class="btn btn-nueva-busqueda push-20-t" data-toggle="collapse" data-target="#campos_nueva_busqueda" id="btn_nueva_busqueda_m" >Nueva Búsqueda</button>
        </div>
    </div>                                     
</section>

<!-- BUSCADOR version TABLET y LAPTOP -->
<section class="content content-full content-boxed hidden-xs portada-title" style="right: 1%; left: 1%; top: <?php echo $portada_top; ?>px;">
    <div class="row  push-5">

        <div class="col-sm-4 push-20-t">

            <!-- Volver al listado de locales -->
            <a class="btn btn-listado-locales" style="max-width: 120px" id="btn_listado_locales" onclick="buscar_locales_mini();"><i class="si si-arrow-left push-5-r" style="color:white"></i>Tiendas</a>   
                                       
        </div>

        <div class="col-sm-8 push-20-t push-5">    
            <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini" method="get">
            </form>                     
              <div class="col-sm-5">
                    <input class="form-control input-buscar-local-mini input-buscador-mini-dir font-w600 text-black" type="text" id="dir_mini" name="dir" placeholder="MI DIRECCION" required style="color:black;">
                    <div class="animated fadeInDown valida-buscador" id="valida_dir_mini" style="display:none">Ingresá tu dirección</div>                                
              </div>
              <div class="col-sm-4" id="div_localidad_mini">
                    <select id="loc_mini" name="loc" class="js-select2 form-control input-buscar-local-mini input-buscador-mini-loc font-w600 text-black" required data-placeholder="MI LOCALIDAD">
                        <option></option>
                        <?php 
                        foreach($localidades as  $localida_item){ ?>
                            <option value="<?php echo $localida_item->nombre ?>" ><?php echo $localida_item->nombre ?></option> 
                        <?php } ?>    
                    </select>                
                    <div class="animated fadeInDown valida-buscador" id="valida_loc_mini" style="display:none">Seleccioná tu localidad</div>                                
              </div>
            
              <div class="col-sm-3">
                    <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini" onclick="buscar_locales_mini();" >Buscar</a>
              </div>                         
        </div>                      
    </div>
</section>     

<div id="campos_nueva_busqueda" class="collapse push-10-t">    
<form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini_m" method="get">
</form>                   
  <div class="col-sm-5 push-5">
        <input class="form-control input-buscar-local-mini-m input-buscador-mini-dir font-w600 text-black" type="text" id="dir_mini_m" name="dir" placeholder="MI DIRECCION" required>
        <div class="animated fadeInDown valida-buscador" id="valida_dir_mini_m" style="display:none">Ingresá tu dirección</div>
  </div>
  <div class="col-sm-4 push-5" id="div_localidad_mini_m">
            <select id="loc_mini_m" name="loc" class="js-select2" required data-placeholder="MI LOCALIDAD">
                <option></option>
                <?php 
                foreach($localidades as  $localidad_item){ ?>
                    <option value="<?php echo $localidad_item->nombre ?>" ><?php echo $localidad_item->nombre ?></option> 
                <?php } ?>    
            </select>      
            <div class="animated fadeInDown valida-buscador" id="valida_loc_mini_m" style="display:none">Seleccioná tu localidad</div>  
      </div>
  <div class="col-sm-3 push-5">
        <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini_m" onclick="buscar_locales_mini_m();" >Buscar</a>               
  </div>   
</div>  

<!-- TEXTO BANNER -->        
<div  id="div_texto_banner" class="texto-banner texto-banner-responsive text-center h3 col-sm-12" style="display:none"></div>

<!-- fin BUSCADOR MINI -->

<!-- lISTADO DE ARTICULOS y FILTROS -->
<section class="content content-boxed full-pantalla">
    <div class="row push-80" style="position: relative">

        <!-- DETALLES LOCAL Y FILTROS -->        
        <div class="col-sm-2">

            <!-- DETALLES LOCAL -->
            <div class="form-inline clearfix">
                <!-- Version mobile -->
                <ul class="list list-simple list-li-clearfix visible-xs" id="div_nombre_local_mob"></ul>

                <!-- Version desktop y tablet -->
                <ul class="list list-simple list-li-clearfix hidden-xs" id="div_nombre_local"></ul>
            </div>
            <!-- fin DETALLES LOCAL -->

            <!-- Ver Productos / Servicios -->
            <div id="div_productos_servicios" class="form-inline clearfix push-10 z-index-0 pull-left div-centrado" style="display:none">
                <input type='checkbox' id='toggle_prod_serv' class="btn btn-geotienda" />
            </div>

            <div class="clearfix"></div>

            <!-- FILTROS DE SERVICIOS -->
            <div  id="div_filtros_servicios" style="display:none">
                <div class="block filtros-box">
                    <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                        <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters" data-class="hidden-xs">
                            <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                        </button>

                       <div class="js-search-filters hidden-xs text-center push-10-t">
                            <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span>  

                            <div class="div_cargando_servicios text-center">
                               <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                            </div>   

                            <!-- FILTROS APLICADOS --> 
                            <div id="div_filtros_servicios_aplicados" class="filtro-tipo push-10-t push-10" style="display: none;">
                                Filtros aplicados

                                <div id="div_filtros_servicios_aplicados_items" class="filtros-aplicados push-10-t push-10"></div>
                                <hr>
                            </div>

                            <!-- FILTROS DISPONIBLES --> 
                            <div id="div_filtros_servicios_animal" class="push-20 push-10-t"></div>
                            <div id="div_filtros_servicios_servicio" class="push-20"></div>                    
                            <div id="div_filtros_servicios_servicio_item" class="push-20"></div>                    
                        </div>
                    </div>
                </div>  
            </div>                              
            <!-- fin FILTROS DE SERVICIOS --> 

            <!-- FILTROS Y BUSCADOR DE ARTICULOS -->
            <div  id="div_filtros_articulos" style="display:none">            
                <div class="input-group push-15 pull-left">  
                    <select class="form-control orden-articulos" onchange="ordenarArticulos(this.value)">
                        <option value="nombre" >Alfabéticamente</option>
                        <option value="descuento">Mayor descuento</option>
                        <option value="precio">Menor precio</option>
                    </select>
                   <div class="input-group-btn" >
                        <span class="btn" style="cursor: auto;"><i class="fa fa-sort-amount-asc"></i></span>
                    </div>                        
                </div>    

                <div class="block filtros-box">
                    <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                        <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters" data-class="hidden-xs">
                            <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                        </button>

                        <div class="js-search-filters hidden-xs text-center push-10-t">
                            <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span>  

                            <div class="div_cargando text-center">
                               <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                            </div>   

                            <!-- FILTROS APLICADOS --> 
                            <div id="div_filtros_articulos_aplicados" class="filtro-tipo push-10-t push-10" style="display: none;">
                                Filtros aplicados

                                <div id="div_filtros_articulos_aplicados_items" class="filtros-aplicados push-10-t push-10"></div>
                                <hr>
                            </div>

                            <!-- FILTROS DISPONIBLES --> 
                            <div id="div_filtros_articulos_animal" class="push-10 push-10-t"></div>                        
                            <div id="div_filtros_articulos_rubro" class="push-10"></div>
                            <div id="div_filtros_articulos_marca" class="push-10"></div>
                            <div id="div_filtros_articulos_edad" class="push-10"></div>                            
                            <div id="div_filtros_articulos_tamanio" class="push-10"></div>                            
                            <div id="div_filtros_articulos_raza" class="push-10"></div>
                            <div id="div_filtros_articulos_presentacion" class="push-10"></div>                    
                            <div id="div_filtros_articulos_medicados" class="push-10"></div>                    
                        </div>
                    </div>
                </div>
            </div>
            <!-- fin FILTROS Y BUSCADOR DE ARTICULOS -->
        </div>
        <!-- fin DETALLES LOCAL Y FILTROS -->     
                        
        <!-- ORDEN, BUSCADOR y LISTADOS (ARTICULOS o SERVICIOS) -->
        <div class="col-sm-6" id="div_detalles_local_listados">          

            <!-- LISTADOS (ARTICULOS o SERVICIOS) -->
            <div class="form-inline clearfix">
                <!-- LISTADO DE ARTICULOS -->
                <div id="lista_articulos" style="display:none">      
                    <table id="table_articulos" class="table" cellspacing="0" width="100%">
                        <thead>
                            <tr>                                 
                                <th>Articulo</th>                                     
                                <th>Filtro Rubro</th>                                     
                                <th>Filtro Animal</th>                                     
                                <th>Filtro Marca</th>                                     
                                <th>Filtro Raza</th>                                     
                                <th>Filtro Tamanio</th>                                     
                                <th>Filtro Edad</th>                                     
                                <th>Filtro Presentacion</th>                                     
                                <th>Filtro Medicados</th>                                     
                            </tr>
                        </thead>                     
                        <tbody>
                        </tbody>
                    </table>                         
                </div>

                <!-- LISTADO DE SERVICIOS -->
                <div class="row" id="lista_servicios_local" style="display:none">       
                LISTA SERVICIOS     
                </div>                   
            </div>
            <!-- fin LISTADOS (ARTICULOS o SERVICIOS) -->
        </div>
        <!-- fin ORDEN, BUSCADOR y LISTADOS (ARTICULOS o SERVICIOS) -->

        <!-- MI PEDIDO -->            
        <div class="col-sm-4 hidden-xs" id="div_mi_pedido" style="position: absolute; top:0; right:0; bottom: 0; display: none">
            <div id="mi_pedido">
                <ul class="list list-simple list-li-clearfix">
                    <li class="local-articulos-box">
                        <span class="h5"><i class="fa fa-shopping-cart push-5-l"></i> Mi Pedido</span>                        
                        <div class="block-content" id="mas_datos_pedido">
                            <div class="pull-r-l">
                                <table class="table table-borderless table-vcenter">
                                    <tbody id="detalle-pedido">
                                    </tbody>
                                    <tfoot>
                                        <tr class="push-10-t">
                                            <td colspan="5">
                                            </td>
                                        </tr>      
                                        <tr class="success" id="pedido_vacio">
                                            <td class="text-center" colspan="4">
                                                <span class="h5 font-w600">Tu pedido está vacío</span>
                                            </td>
                                        </tr>                                                                              
                                        <tr class="success" id="total_pedido_label" style="display:none">
                                            <td class="text-right" colspan="3">
                                                <span class="text-success h4 font-w600">Total</span>
                                            </td>
                                            <td class="text-right">
                                                <div class="h4 font-w600 text-success">$<span id="total-pedido">0</span></div>
                                            </td>
                                        </tr>
                                        <tr  id="botones_pedido" style="display:none">
                                            <td class="text-center" colspan="4">
                                                <a title="Cancelar pedido" class="btn btn-sm btn-default" href="javascript:void(0)" onclick="limpiar_pedido();">
                                                    <i class="fa fa-trash-o push-5-r"></i> Limpiar
                                                </a>
                                                <a title="Procesar pedido" class="btn btn-sm btn-success" href="javascript:void(0)" onclick="finalizar_pedido();">
                                                    <i class="fa fa-check push-5-r"></i> Finalizar
                                                </a>
                                            </td>
                                        </tr>                                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>                        
                    </li>
                </ul>
            </div>        
        </div>
        <!-- fin MI PEDIDO --> 


        <!-- MI PEDIDO (MOBILE) -->            
        <div class="visible-xs">

            <div class="pedido-mobile" id="mi-pedido-mobile" style="display:none" >
                <ul class="list list-li-clearfix text-center pedido-mobile-centrado">
                    <li class="" text-center">
                        <button data-toggle="collapse" data-target="#mas_datos_pedido_mob" class="btn-ver-pedido"><i class="fa fa-shopping-cart push-10-r"></i><span id="titulo_ver_pedido_mob"> Ver mi Pedido</span></button>                        
                        <div class="block-content collapse pedido-mobile-lista" id="mas_datos_pedido_mob">
                            <div class="pull-r-l">
                                <table class="table table-borderless table-vcenter table-mi-pedido-mob">
                                    <tbody id="detalle-pedido-mob">
                                    </tbody>
                                    <tfoot>
                                        <tr class="push-10-t">
                                            <td colspan="5">
                                            </td>
                                        </tr>                                                                 
                                        <tr class="success">
                                            <td class="text-right" colspan="3">
                                                <span class="text-success h4 font-w600">Total</span>
                                            </td>
                                            <td class="text-right">
                                                <div class="h4 font-w600 text-success">$<span id="total-pedido-mob">0</span></div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-center" colspan="4">
                                                <a class="btn btn-sm btn-default push-5-r" href="javascript:void(0)" onclick="limpiar_pedido();">
                                                    <i class="fa fa-trash-o push-5-r"></i> Limpiar
                                                </a>
                                                <a class="btn btn-sm btn-success" href="javascript:void(0)" onclick="finalizar_pedido();">
                                                    <i class="fa fa-check push-5-r"></i> Finalizar
                                                </a>
                                            </td>
                                        </tr>                                            
                                    </tfoot>
                                </table>
                            </div>
                        </div>                        
                    </li>
                </ul>
            </div>        
        </div>
        <!-- fin MI PEDIDO (MOBILE) --> 

    </div>
</section>
<!-- fin lISTADO DE ARTICULOS y FILTROS -->


<!-- MODAL DETALLES ARTICULO -->
<div class="modal fade" id="modal_detalles_articulo" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <!-- Nombre y Marca -->
                            <span class="h5">
                                <span class="font-w600 text-muted" id="modal_articulo_marca"></span>
                                <br>
                                <p class="h4 font-w600" id="modal_articulo_nombre"></p>
                            </span>
                            <!-- Nombre, Marca y Precio -->                         
                        </div>
                        <div class="col-sm-6">
                            <!-- Imagen -->
                            <div class="row" style="position: relative">
                                <div class="col-xs-12 push-10 push-10 img-container">
                                    <div class="div-img-articulos-detalle" id="modal_articulo_imagen">
                                    </div>
                                </div>
                            </div>
                            <!-- FIN Imagen -->
                        </div>
                        <div class="col-sm-6" >
                            
                            <!-- Detalles -->                            
                            <div  id="modal_articulo_detalle">
                            </div>      
                       
                            <!-- Presentaciones -->  
                            <div class="h5 font-w600 push-5 push-5-t">Presentaciones</div>                          
                            <div  id="modal_articulo_presentaciones">
                            </div>                      

                        </div>

                    </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-lg btn-info btn-geotienda-celeste" type="button" data-dismiss="modal"> Cerrar</button>
                        </div>  
                        <br/>                   
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL DETALLES ARTICULO -->    


<!-- MODAL DETALLES SERVICIO -->
<div class="modal fade" id="modal_detalles_servicio" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <!-- Nombre -->
                            <div class="h5 pull-left">
                                <span class="font-w600 text-muted" id="modal_servicio_animal"></span>
                                <br>                            
                                <p class="h4 font-w600" id="modal_servicio_nombre"></p>
                            </div>  

                            <!-- Precio -->                            
                            <div id="modal_servicio_precio" class="pull-right push-30-r push-18-t"></div>                                                    
                        </div>
                        <div class="col-sm-6">
                            <!-- Imagen -->
                            <div class="row" style="position: relative">
                                <div class="col-xs-12 push-10 push-10 img-container">
                                    <div class="div-img-articulos-detalle" id="modal_servicio_imagen">
                                    </div>
                                </div>
                            </div>
                            <!-- FIN Imagen -->
                        </div>
                        <div class="col-sm-6" >
                            
                            <!-- Detalles -->                            
                            <div  id="modal_servicio_detalle"></div>     
                           
                        </div>

                    </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-lg btn-info btn-geotienda-celeste" type="button" data-dismiss="modal"> Cerrar</button>
                        </div>  
                        <br/>                   
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL DETALLES SERVICIO --> 


<!-- MODAL FINALIZAR PEDIDO -->    
<div class="modal fade" id="modal_finalizar_pedido" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                        
                    <h3 class="text-center text-naranja">Mi Pedido</h3>
                    <br/>
                    <h4 class="font-w300 text-center" id="local-finalizar-pedido"></h4>
                 </div>    

                 <form class="form-horizontal action="#" id="form_finalizar_pedido">
                     <div class="modal-overflow-contenido">
                        <div class="block-content">
                            <!-- DETALLES PEDIDO -->
                            <table class="table table-borderless table-vcenter">
                            	<tbody id="detalle-pedido-fin-pedido">
                            	</tbody>
    							<tfoot>
                                    <tr class="push-10-t">
                                        <td colspan="4">
                                        </td>
                                    </tr>                                                                              
                                    <tr class="success">
                                        <td class="text-right" colspan="3">
                                            <span class="text-success h5 font-w600">Total</span>
                                        </td>
                                        <td class="text-right">
                                            <div class="h5 font-w600 text-success">$<span id="total-pedido-finalizar-pedido">0</span></div>
                                        </td>
                                    </tr>                                        
                                </tfoot>                        	
    						</table>
                            <!-- fin DETALLES PEDIDO -->
                        </div>

                        <div class="block-content">
                            <!-- DATOS DE ENVIO -->
                                <input type="hidden" value="" name="fin_pedido_local_id" id="fin_pedido_local_id"/> 
                                <input type="hidden" value="" name="fin_pedido_cliente_id" id="fin_pedido_cliente_id"/> 
                                <input type="hidden" value="" name="fin_pedido_total" id="fin_pedido_total"/> 
                                <input type="hidden" value="" name="fin_pedido_envio" id="fin_pedido_envio"/> 
                                <input type="hidden" value="" name="fin_pedido_envio_domicilio" id="fin_pedido_envio_domicilio"/> 

                                <div class="form-body">
                                	<!-- Tipo de envio -->
        							<div class="form-group" id="div_fin_pedido_envio">
        		                        <label class="col-sm-4 control-label">Envío</label>
        		                        <div class="col-sm-7 input-obligatorio">
        		                            <select class="form-control" name="ddl_fin_pedido_envio" id="ddl_fin_pedido_envio" onchange="toggle_envio(this);" style="width: 100%;" tabindex="-1" aria-hidden="true">
        		                                <option value="-1" selected>Elegir...</option>
        		                                <option value="A domicilio">A domicilio</option>
        		                                <option value="Retiro en la tienda">Retiro en la tienda</option>
        		                            </select>
                                            <span class="help-block"></span>
        		                        </div>
        		                    </div>

                                	<!-- Nombre -->
        							<div class="form-group">
        		                        <label class="col-sm-4 control-label">Nombre</label>
        		                        <div class="col-sm-7 input-obligatorio">
        		                            <input type="text" class="form-control" name="fin_pedido_nombre" id="fin_pedido_nombre" style="width: 100%;" tabindex="-1" aria-hidden="true">
        		                            </input>
                                            <span class="help-block"></span>
        		                        </div>
        		                    </div>	

                                    <!-- Direccion -->
                                    <div class="form-group retiro-domicilio" style="display:none">
                                        <label class="col-sm-4 control-label">Dirección</label>
                                        <div class="col-sm-7 input-obligatorio">
                                            <input type="text" class="form-control" name="fin_pedido_direccion" id="fin_pedido_direccion" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            </input>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>  

                                    <!-- Localidad -->
                                    <div class="form-group retiro-domicilio" style="display:none">
                                        <label class="col-sm-4 control-label">Localidad</label>
                                        <div class="col-sm-7 input-obligatorio">
                                            <input type="text" class="form-control" name="fin_pedido_localidad" id="fin_pedido_localidad" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            </input>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>  

                                    <!-- Provincia -->
                                    <div class="form-group retiro-domicilio" style="display:none">
                                        <label class="col-sm-4 control-label">Provincia</label>
                                        <div class="col-sm-7 input-obligatorio">
                                            <input type="hidden" value="" name="fin_pedido_provincia" id="fin_pedido_provincia"/> 
                                            <select name="fin_pedido_provincia_id" id="fin_pedido_provincia_id" class="form-control" >
                                                <option selected value="">Seleccione Provincia...</option>
                                                <?php 
                                                foreach($provincias as $provincia){ ?>
                                                    <option value="<?php echo $provincia->id ?>" ><?php echo $provincia->nombre ?></option> 
                                                <?php } ?>    
                                            </select>  
                                            <span class="help-block"></span>
                                        </div>
                                    </div>

                                    <!-- Telefono -->
                                    <div class="form-group retiro-domicilio retiro-domicilio-telefono" style="display:none">
                                        <label class="col-sm-4 control-label" for="fin_pedido_telefono">Teléfono</label>
                                        <div class="col-sm-7 input-obligatorio">
                                            <input type="text" class="form-control" name="fin_pedido_telefono" id="fin_pedido_telefono" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                            </input>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>  

                                	<!-- Forma de Pago -->
        							<div class="form-group">
        		                        <label class="col-sm-4 control-label">Forma de Pago</label>
        		                        <div class="col-sm-7 input-obligatorio">
        		                            <select class="form-control" name="fin_pedido_forma_pago"  id="fin_pedido_forma_pago" style="width: 100%;" tabindex="-1" aria-hidden="true" >
        		                                <option value="-1" selected="">Elegir...</option>
        		                                <?php 
                                                foreach($medios_pago as $medios_pago){ ?>
                                                    <option value="<?php echo $medios_pago->nombre ?>" ><?php echo $medios_pago->nombre ?></option> 
                                                <?php } ?>  
        		                            </select>
                                            <span class="help-block"></span>
        		                        </div>
        		                    </div>

                                	<!-- Observaciones -->
        							<div class="form-group">
        		                        <label class="col-sm-4 control-label">Mensaje a la tienda</label>
        		                        <div class="col-sm-7">
        		                            <textarea class="form-control" placeholder="Ingresá mensaje" name="fin_pedido_observaciones" id="fin_pedido_observaciones" rows="4"></textarea>
        		                        </div>
        		                    </div>	

                                  <div class="form-group push-20-t">
                                      <label class="col-sm-4"></label>
                                      <div class="col-sm-7">
                                          <span class="red">*</span> Datos obligatorios
                                      </div>
                                  </div>                                         
                                </div>                   
    	                     <!-- fin DATOS DE ENVIO -->
                        </div>
                     </div>
                 </form>

                <div class="block-content">
                    </hr>
                    <div class="form-group form-group-login">
                        <div class="text-center">
                            <button class="btn btn-default btn-geotienda-default btn-geotienda-default push-10-r" type="button" data-dismiss="modal">Cancelar</button>                        
                            <button class="btn btn-success" id="btn_procesar_pedido" onclick="procesar_pedido();">Comprar</button>

                        </div>    
                    </div>           
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL FINALIZAR PEDIDO -->    


<!-- MODAL FIN PEDIDO o TURNO OK  -->
<div class="modal fade" id="modal_resultado_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content geotienda-modal-content">                  
                    <h3 class="font-w300 text-center resaltado-naranja" id="resultado_ok_titulo1"></h3>
                    <br/><br/>
                    <h4 class="font-w300 text-center" id="resultado_ok_titulo2"></h4>
                    <h5 class="font-w300 text-center text-muted push-20-t">Si no te llegó un email al buzón de entrada, por favor revisá tu correo no deseado.</h5>
                    <br/><br/>
                    </hr>
                    <div class="text-center">
                        <button class="btn btn-lg btn-info btn-geotienda-celeste" onclick="redirecciona_fin_pedido();">Aceptar</button>
                    </div>
                    <br/>

                </div>
            </div>
        </div>
    </div>
</div> 
<!-- MODAL FIN PEDIDO o TURNO OK  -->  


<!-- MODAL AGENDAR TURNO -->
<div class="modal fade" id="modal_agendar_turno" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content geotienda-modal-content modal-body">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                        
                    <h4 class="font-w300 pull-left" id="solicitud_turno_titulo">Solicitud de Turno</h4>
                    <div style="height:10px; clear: both;"></div>
                    <h5 class="font-w300 text-muted" id="agendar_turno_local"></h5>
                    <div style="height:5px; clear: both;"></div>
                    <h5 class="font-w300 text-muted" id="agendar_turno_servicio"></h5>

                    <form class="form-horizontal action="#" id="form_agendar_turno">
                        <div class="block-content">                        
                            <div class="form-body">

                                <div class="help-block red" style="display:none" id="div_error_turno">Seleccioná día y horario e ingresá un teléfono para reservar el turno.</div>

                                <!-- Calendario Clinica -->
                                <div class="form-group calendario col-sm-6  push-20-r" id="div_calendario_clinica" style="display: none;">
                                    <label class="control-label push-5">Seleccioná Día</label>
                                    <span class="text-danger">*</span>
                                    <div id="calendario_clinica">
                                        <?php if(isset($calendario_clinica)) echo $calendario_clinica; ?>
                                    </div>
                                </div>

                                <!-- Calendario Peluqueria -->
                                <div class="form-group calendario col-sm-6  push-20-r" id="div_calendario_peluqueria" style="display: none;">
                                    <label class="control-label push-5">Seleccioná Día</label>
                                    <span class="text-danger">*</span>
                                    <div id="calendario_peluqueria">
                                        <?php if(isset($calendario_peluqueria)) echo $calendario_peluqueria; ?>
                                    </div>
                                </div>                                

                                <!-- Horario -->
                                <div class="form-group col-sm-6">
                                    <label class="control-label push-5">Seleccioná Horario</label>
                                    <span class="text-danger">*</span>
                                    <div class="text-muted" id="fin_turno_hora">Seleccioná un día para ver los horarios disponibles
                                    </div>
                                </div>        

                                <!-- Telefono -->
                                <div class="form-group col-sm-6">
                                    <label class="control-label push-5">Teléfono</label>
                                    <div class="input-obligatorio">
                                        <input type="text" class="form-control" name="fin_turno_telefono" id="fin_turno_telefono" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                        </input>
                                        <span class="help-block"></span>
                                    </div>
                                </div>  

                                <!-- Observaciones -->
                                <div class="form-group col-sm-12">
                                    <label class="control-label push-5">Mensaje a la tienda</label>
                                    <div>
                                        <textarea class="form-control" placeholder="Ingresá mensaje"  name="fin_turno_observaciones" id="fin_turno_observaciones" rows="2"></textarea>
                                    </div>
                                </div>     

                                  <div class="form-group col-sm-12 push-10-t">
                                      <span class="red">*</span> Datos obligatorios
                                  </div>   
                            </div>
                        </div>
                    </form>
                    <div class="clear"></div>
                    <hr>
                    <div class="form-group form-group-login">
                        <div class="text-center">
                            <button class="btn btn-geotienda-violeta push-10-r" id="btn_procesar_turno" onclick="procesar_turno();">Agendar Turno</button>
                            <button class="btn btn-default btn-geotienda-default btn-geotienda-default" type="button" data-dismiss="modal">Cancelar</button>
                        </div>    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL AGENDAR TURNO -->  


<!-- MODAL DETALLES LOCAL -->
<div class="modal fade" id="modal_detalles_local" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div id="div_detalles_local"></div>
                    <div class="clearfix"></div>
                    <hr />
                    <div class="text-center">
                        <button class="btn btn-lg btn-info btn-geotienda-celeste" type="button" data-dismiss="modal"> Cerrar</button>
                    </div>  
                    <br/>

                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL DETALLES LOCAL -->  


<!-- MODAL COMENTARIOS LOCAL -->
<div class="modal fade" id="modal_comentarios_local" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div id="div_comentarios_local_header"></div>
                    <div class="clearfix"></div>
                    <div id="div_comentarios_local_list" class="modal-overflow-contenido"></div>
                     
                    <div class="clearfix"></div>
                    <div class="text-center">
                        <button class="btn btn-lg btn-info btn-geotienda-celeste" type="button" data-dismiss="modal"> Cerrar</button>
                    </div>  
                    <br/>

                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL COMENTARIOS LOCAL --> 


<script type="text/javascript">

// Articulos y Servicios local
var local_id;
var resultado;
var servicios_local; 
var categoria;
var local;
var toggle_data = '';
var local_servicio_item_id;
var servicio_item_id;
var cant_articulos;
var link_turno = '';
var link_venta = '';

// Pedido    
var pedido = [];
var total_pedido = 0;
var redirecciona_fin = 'pedidos';

// Turnos
var dia_turno;
var precio_turno;

// Filtros
var cuenta_filtros_aplicados = 0;
var cuenta_filtros_servicios_aplicados = 0;

var filtros_aplicados_rubro = '';
var filtros_aplicados_rubro_id = -1;
var select_rubro;
var filtro_rubro;
var aplicado_rubro = '';
var cuenta_filtro_rubro = 0;

var filtros_aplicados_animal = '';
var filtros_aplicados_animal_id = -1;
var select_animal;
var filtro_animal;
var aplicado_animal = '';
var cuenta_filtro_animal = 0;

var filtros_aplicados_marca = '';
var filtros_aplicados_marca_id = -1;
var select_marca;
var filtro_marca;
var aplicado_marca = '';
var cuenta_filtro_marca = 0;

var filtros_aplicados_raza = '';
var filtros_aplicados_raza_id = -1;
var select_raza;
var filtro_raza;
var aplicado_raza = '';
var cuenta_filtro_raza = 0;

var filtros_aplicados_tamanio = '';
var filtros_aplicados_tamanio_id = -1;
var select_tamanio;
var filtro_tamanio;
var aplicado_tamanio = '';
var cuenta_filtro_tamanio = 0;

var filtros_aplicados_edad = '';
var filtros_aplicados_edad_id = -1;
var select_edad;
var filtro_edad;
var aplicado_edad = '';
var cuenta_filtro_edad = 0;

var filtros_aplicados_presentacion = '';
var filtros_aplicados_presentacion_id = -1;
var select_presentacion;
var filtro_presentacion;
var aplicado_presentacion = '';
var cuenta_filtro_presentacion = 0;

var filtros_aplicados_medicados = '';
var filtros_aplicados_medicados_id = -1;
var select_medicados;
var filtro_medicados;
var aplicado_medicados = '';
var cuenta_filtro_medicados = 0;

var filtros_aplicados_servicio = '';
var filtros_aplicados_servicio_id = -1;
var select_servicio;
var filtro_servicio;
var aplicado_servicio = '';
var cuenta_filtro_servicio = 0;

var filtros_aplicados_servicio_item = '';
var filtros_aplicados_servicio_item_id = -1;
var select_servicio_item;
var filtro_servicio_item;
var aplicado_servicio_item = '';
var cuenta_filtro_servicio_item = 0;

var filtros_aplicados_servicio_animal = '';
var filtros_aplicados_servicio_animal_id = -1;
var select_servicio_animal;
var filtro_servicio_animal;
var aplicado_servicio_animal = '';
var cuenta_filtro_servicio_animal = 0;


$(document).ready(function() 
{
    // Si el usuario activo es un local, redirecciona a su dashboard
    <?php if (isset($_SESSION['frontend_logged_in'])){ ?>  

         if('<?php echo $_SESSION['frontend_tipo']; ?>' == 'local') window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos';                           
    <?php } ?>  

    <?php if(isset($_GET["turno"])){ ?>
        link_turno = '<?php echo $_GET["turno"]; ?>';
    <?php } ?>

    <?php if(isset($_GET["venta"])){ ?>
        link_venta = '<?php echo $_GET["venta"]; ?>';
    <?php } ?>

    window.scrollTo(0, 0);

    direccion = '<?php echo $direccion; ?>';
    localidad = '<?php echo $localidad; ?>';

    $('.input-buscador-mini-dir').val(direccion);
    $('.input-buscador-mini-loc').val(localidad);

    local_id = <?php echo $_GET["id"]; ?>; 
    categoria = '<?php echo $_GET["cat"]; ?>'; 

    var $select = $('#loc_mini');
    $select.select2();
    $select.val(localidad).trigger('change');

    var $select_m = $('#loc_mini_m');
    $select_m.select2();
    $select_m.val(localidad).trigger('change');    

    ver_local_articulos(local_id);  

    // Respuesta desde compra hecha por Mercadopago
    var respuesta_mp = '';

    <?php if(isset($_GET["collection_status"])){ ?>
        respuesta_mp = '<?php echo $_GET["collection_status"]; ?>';
    <?php } ?>
    
    if (respuesta_mp !== '')
    {
        var venta_id = 0;

        <?php if(isset($_GET["venta_id"])){ ?>
            venta_id = <?php echo $_GET["venta_id"]; ?>;
        <?php } ?>

        switch (respuesta_mp){

            case 'approved':
            case 'success':
                $('#resultado_ok_titulo1').html('¡Felicitaciones! Tu pedido ha sido exitoso');
                $('#resultado_ok_titulo2').html('Gracias por elegir GeoTienda. En pocos minutos recibirás un email con los datos de tu compra.');

                enviar_mail_venta(venta_id);
                actualizar_estado_venta(venta_id, 'concretado');
            break;

            case 'authorized':
            case 'pending':
                $('#resultado_ok_titulo1').html('¡Felicitaciones! Tu pedido ha sido exitoso');
                $('#resultado_ok_titulo2').html('<strong>Tu pedido será procesado una vez que completes el pago por el medio de pago que elegiste.</strong><br /><br /> Gracias por elegir GeoTienda. En pocos minutos recibirás un email con los datos de tu compra.');     

                enviar_mail_venta(venta_id);     
                actualizar_estado_venta(venta_id, 'pendiente');                                    
            break;    

            case 'failure':
            case 'rejected':
            
                $('#resultado_ok_titulo1').html('Hubo un error procesando tu pago');
                $('#resultado_ok_titulo2').html('Elige otro medio de pago, o intenta realizar el pedido nuevamente.');

                $('#btn_resultado_ok').attr('href', "<?php echo BASE_PATH ?>/Local/local_articulos?id=" + local_id + "&cat=" + categoria);       
                                        
            break;    

            default:
            
                $('#resultado_ok_titulo1').html('Hubo un error procesando tu pago');
                $('#resultado_ok_titulo2').html('Elige otro medio de pago, o intenta realizar el pedido nuevamente.');

                $('#btn_resultado_ok').attr('href', "<?php echo BASE_PATH ?>/Local/local_articulos?id=" + local_id + "&cat=" + categoria);  

            break;                     
        }

        $('#modal_resultado_ok').modal({ backdrop: 'static', keyboard: false }) // Impide cerrar el modal haciendo clic fuera de él
        $('#modal_resultado_ok').modal('show');  
    }   

    // Mantiene ventana 'Mi Pedido' siempre visible
    $("#mi_pedido").stick_in_parent({recalc_every: 10});

});


function ver_local_articulos(id)
{
    var url;

    $.ajax({
      url : "<?php echo BASE_PATH ?>/Local/ajax_local_articulos/" + id,
      cache: false,
      success: function(data){

        resultado = jQuery.parseJSON(data);

        servicios_local = resultado.servicios_item;

        mostrar_datos_local();

        local = resultado.local;

        $('#div_filtros_servicios_aplicados_items').html('');

        // Filtro por SERVICIO
        $('#div_filtros_servicios_servicio').html('');

        filtro_servicio = $('<div><div class="filtro-tipo">SERVICIO</div></div>')
            .appendTo( $('#div_filtros_servicios_servicio'));

        select_servicio= $('<ul id="ddl_servicio" class="filtro-select" style="display:inherit;" ></ul>')
            .appendTo( $('#div_filtros_servicios_servicio'));    

        // Filtro por SERVICIO ITEM
        $('#div_filtros_servicios_servicio_item').html('');

        filtro_servicio_item = $('<div><div class="filtro-tipo">ITEM</div></div>')
            .appendTo( $('#div_filtros_servicios_servicio_item'));

        select_servicio_item= $('<ul id="ddl_servicio_item" class="filtro-select" ></ul>')
            .appendTo( $('#div_filtros_servicios_servicio_item'));  

        // Filtro por ANIMAL
        $('#div_filtros_servicios_animal').html('');

        filtro_servicio_animal = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
            .appendTo( $('#div_filtros_servicios_animal'));

        select_servicio_animal= $('<ul id="ddl_servicio" class="filtro-select" style="display:inherit;" ></ul>')
            .appendTo( $('#div_filtros_servicios_animal'));    


        $('#div_filtros_servicios_servicio').hide();
        $('#div_filtros_servicios_servicio_item').hide();            
        $('#div_filtros_servicios_animal').hide();            
            
        filtro_servicio.hide();            
        filtro_servicio_item.hide();            
        filtro_servicio_animal.hide();            

        listar_servicios();
        listar_articulos();   

        // Si es una veterinaria o viene del link de turno cancelado, muestra inicialmente los servicios
        if ((local.categoria == 'Veterinaria' && link_venta == '') || link_turno != '' )
        {
            ver_servicios();

            // Para servicios usa 10 columnas
            $("#div_detalles_local_listados").removeClass("col-sm-6").addClass("col-sm-10");

            // Muestra lista de servicios
            $("#lista_servicios_local").show();

            // Si la veterinaria vende articulos, muestra los botones 'Ver Servicios' y 'Ver Productos'
            if(local.count_articulos > 0){
                $('#div_productos_servicios').show();  
            }                          
        } 
        // Petshop
        else
        {
            ver_articulos();

            // Para articulos usa 6 columnas
            $("#div_detalles_local_listados").removeClass("col-sm-10").addClass("col-sm-6");

            // Muestra 'Mi Pedido' y lista de artículos
            $('#div_mi_pedido').show();
            $("#lista_articulos").show();

            // Si el petshop ofrece servicios, muestra los botones 'Ver Servicios' y 'Ver Productos'
            if(servicios_local != ''){
                $('#toggle_prod_serv').prop('checked', true);                    
                $('#div_productos_servicios').show();
            }
        }

        $('#toggle_prod_serv').bootstrapSwitch({
            on: 'Productos',
            off: 'Servicios',
            onLabel: 'Productos',
            offLabel: 'Servicios',  
            onClass: 'btn btn-geotienda',
            offClass: 'btn btn-geotienda btn-geotienda-violeta'
        });
  
      } 
    });

}


function mostrar_datos_local()
{

    local = resultado.local;
    
    icono = 'fa fa-shopping-bag';
    color = '#89AEDC';

    htmlPuntaje = '';            
    htmlComentarios = '';
    htmlUrgencias = '';
    htmlUrgenciasModalidad = '';
    htmlUrgenciasDomicilio = '';
    htmlMediosPago = '';
    htmlMediosPagoItems = '';


    // Categoria
    if (local.categoria == 'Veterinaria'){
        icono = 'fa-plus';
        color = 'violet';      
    } 

    // Banner
    if (local.banner.texto !== null && local.banner.texto != ''){

        $("#div_texto_banner").css({"background-color":local.banner.fondo, "color": local.banner.color});
        $("#div_texto_banner").append(local.banner.texto);       

        $("#div_texto_banner").show();    

        $("#div_mi_pedido").css({"top": "60px"});                        
    } 


    // Puntaje
    if (local.puntaje){
        starsi = Math.round(local.puntaje);
        starno = 5 - starsi;

        htmlPuntaje = '<div class="pull-left push-5-t force-left-60 force-top-10" style="margin-right:10px">';    

        for (j = 0; j < starsi; ++j) {

            htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starsi.png" width="20" height="20">';
        };

        for (j = 0; j < starno; ++j) {

            htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starno.png" width="20" height="20">';
        };      

        htmlPuntaje += '</div>';           
    }

    // Comentarios
    if (local.comentarios > 0){
        if (local.comentarios == 1) comentarios = '1 comentario';
        else comentarios =  local.comentarios + ' comentarios';

        htmlComentarios = '<div class="pull-left  push-5-t force-top-10 h4">' + 
                        '<a href="javascript:void(0)" onclick="mostrar_comentarios_local()">' + comentarios + '</a>' + 
                    '</div>';
    }

    // Urgencias
    if (local.urgencias == 1)
    {
        htmlUrgenciasModalidad = '<span class="push-5-l" style="font-size: 13px;">(';

        if (local.urgencias_consultorio == 1) htmlUrgenciasModalidad += 'Atención en Consultorio';
        if (local.urgencias_consultorio  == 1 && local.urgencias_domicilio == 1) htmlUrgenciasModalidad += ' y ';
        if (local.urgencias_domicilio == 1) htmlUrgenciasModalidad += 'Visita a Domicilio';

        htmlUrgenciasModalidad += ')</span>';
                        
        htmlUrgencias = '<div class="clearfix"></div><div class="pull-left push-5-t  force-left-60 text-danger"><i class="fa fa-plus-square"></i> <strong>Urgencias 24hs.:</strong> ' + local.telefono_urgencias + htmlUrgenciasModalidad + '</div>' ;
    }

    // Medios de Pago   
    htmlMediosPago = '<div class="clearfix"></div><div style="height:10px; clear: both;"></div><div class="pull-left push-5-t"><i class="fa fa-dollar" style="color:gray"></i> <b>Medios de Pago:</b>' +
                    '<div style="clear: both;"></div><ul class="medios-pago push-5-t push--50-l">';
                    
    $.each(local.medios_pago, function(indice, medio_pago)
    {
        htmlMediosPagoItems = '';

        htmlMediosPago += '<li class="medio-pago push-25-l">' +
                                '<img src="<?php echo BASE_PATH ?>/assets/img/medios_pago/miniaturas/' + medio_pago.imagen + '" alt="' + medio_pago.nombre + '" title="' + medio_pago.nombre + '" data-pin-nopin="true" height="30">';

        //medios_pago_items = this.medio_pago_item.get_by_local_by_medio_pago(local.id, medio_pago.id);

        if (medio_pago.items)
        {                 
            $.each(medio_pago.items, function(indice, medios_pago_item)  
            {                                   
                htmlMediosPagoItems += '<li class="medio-pago push-5-t push-8-l">' +
                                        '<img src="<?php echo BASE_PATH ?>/assets/img/medios_pago/miniaturas/' + medios_pago_item.imagen + '" alt="' + medios_pago_item.nombre + '" title="' + medios_pago_item.nombre + '" data-pin-nopin="true" height="22">' +
                                        '</li>';                                        
            });
            
            htmlMediosPago += htmlMediosPagoItems + '</li>';
        }               
    });                

    htmlMediosPago += '</ul></div>';


    // Logo y Nombre
    $("#div_nombre_local").append(
        '<li class="local-articulos-box">' +

            '<div class="col-sm-12">' + 
                '<div class="pull-left div-centrado">' + 
                    '<img class="centrado" src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + local.avatar + '" width="90%">' + 
                '</div>' + 
                 '<div class="pull-right push-10-t div-centrado push-10">' +  
                    '<button class="btn btn-sm btn-info btn-geotienda-celeste centrado local-nombre-sm" id="btn_detalles_local" onclick="ver_detalles_local();">' + local.nombre + '&nbsp;&nbsp;<i class="fa fa-search-plus"></i></button>' +  
                '</div>' +                                                                                               
            '</div>' +                                                                                                                                            
        '</li>');

        // Local con Entrega a Domicilio
        if (local.envio_domicilio == '1')
        {
            htmlEntregaDomicilio = '<div class="pull-left push-5-t"><i class="fa fa-truck" style="color:gray"></i> <b>Zona de entrega: ' + local.zona_entrega + '</b></div>' +     
                '<div style="clear: both;"></div>' + 
                '<div class="pull-left push-5-t"><b><i class="fa fa-calendar" style="color:gray"></i> Días de entrega: ' + local.dias_entrega + '</b></div>' +  
                '<div style="clear: both;"></div>' + 
                '<div class="pull-left push-5-t"><b><i class="fa fa-money" style="color:gray"></i>  Costo de envío: ' + local.costo_envio + '</b></div>' +  
                '<div style="height:10px"></div><br/>';
        }
        else
        {
            htmlEntregaDomicilio = '<div class="pull-left push-5-t  force-left-60"><i class="fa fa-truck" style="color:gray"></i> <b>No hace envíos a domicilio</b></div>' ;
        }

    $("#div_nombre_local_mob").append(
        '<li class="local-articulos-box">' +

            '<div class="col-sm-12 text-center push-5">' + 
                '<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + local.avatar + '" width="80">' + 
                '<button class="btn btn-info btn-geotienda-celeste push-20-l" id="btn_buscar" onclick="ver_detalles_local();"><span class="nombre-local-mob">' + local.nombre + '</span><br />info tienda&nbsp;&nbsp;<i class="fa fa-search-plus"></i></button>' + 
            '</div>' +                                                                                                                                            
        '</li>');


    // Texto en modal Ver Detalles Local
    $("#div_detalles_local").append(

            '<div class="col-sm-12">' + 
                '<div class="text-center push-10">' + 
                    '<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + local.avatar + '" height="80">' +
                    '<span style="display:inherit" class="h4 push-10-t push-10-l font-w600">' + local.nombre + '</span>' + 
                '</div>' +                                              
                '<div style="height:10px; clear: both;"></div>' + 
                htmlPuntaje + 
                htmlComentarios +
                '<div class="clearfix"></div>' + 
                '<div style="height:20px"></div>' +   
                '<b><div class="pull-left " style="margin-right:10px">' +  
                    '<i class="fa fa-map-marker" style="color:gray"></i> ' + local.direccion + ', ' + local.localidad + 
                '</div></b>' +    
                '<div class="pull-left"><i class="si si-map" style="color:gray"></i> <?php echo $distancia; ?></div>' +  
                '<div style="height:10px"></div><br/>' +   
                htmlEntregaDomicilio +
                '<div style="clear: both;"></div>' + 
                '<div class="pull-left push-5-t"><i class="fa fa-clock-o" style="color:gray"></i> <b>Horarios de Atención:</b>' +
                '<div style="clear: both;"></div>' + 
                '<div class="pull-left push-5-t push-15-l"><b>Lu a Vi:</b> ' + local.horario_atencion + '</div>' +     
                '<div class="pull-left push-5-t push-15-l"><b>Sábados:</b> ' + local.horario_sabados + '</div>' +  
                '<div style="clear: both;"></div>' + 
                '<div class="pull-left push-5-t push-15-l push-5"><b>Domingos:</b> ' + local.horario_domingos + '</div>' +
                htmlUrgencias +
                htmlMediosPago +                                
                                                                                                                                                                                        
        '</div>');

    // Nombre del local en modal Finalizar Pedido
    $("#local-finalizar-pedido").html('Solicitado a ' + local.nombre);

    // Para locales que no hacen envio a domicilio
    if (local.envio_domicilio == '0')
    {
        $("#div_fin_pedido_envio").hide();
        $("#fin_pedido_envio").val("Retiro en la tienda");
        $("#fin_pedido_envio_domicilio").val("no");
        $('.retiro-domicilio-telefono').show();
    }
}


function mostrar_comentarios_local(){

    local = resultado.local;

    $("#div_comentarios_local_header").html('');
    $("#div_comentarios_local_list").html('');
    
    // Datos del local
    $("#div_comentarios_local_header").append(

        '<div class="col-sm-12 push-10-t">' + 
            '<div class="text-center push-10">' + 
                '<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + local.avatar + '" width="50">' +
                '<span style="display:inherit" class="h4 push-10-t push-10-l font-w600">' + local.nombre + '</span>' + 

            '</div>' +                                                                                                    
        '</div>' + 
        '<div class="col-sm-12 push-10">' + 
            '<div class="text-center push-10">' + 
                '<span class="h4 push-10-t push-10-l">Comentarios de los usuarios</span>' + 
            '</div>' +                                                                                                    
        '</div>');
    
        // Carga comentarios del local (aprobados)
        $.ajax({
          url : "<?php echo BASE_PATH ?>/Local/ajax_comentarios_local/" + local.id,
          cache: false,
          success: function(data)
          {
            data = jQuery.parseJSON(data);

            comentarios = data.comentarios;

                // Lista de comentarios del local
                for (i = 0; i < comentarios.length; ++i) 
                {

                    htmlPuntaje = '';
                    htmlComentario = '';

                    // Puntaje
                    starsi = comentarios[i].puntaje;
                    starno = 5 - starsi;

                    for (j = 0; j < starsi; ++j) {

                        htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starsi.png" width="20" height="20">';
                    };

                    for (j = 0; j < starno; ++j) {

                        htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starno.png" width="20" height="20">';
                    };  

                    // Comentarios
                    if (comentarios[i].comentario != ''){

                        htmlComentario = '<div class="clearfix"></div>' + 
                            '<div class="pull-left push-10-t"><i class="fa fa-comment-o"></i> ' + 
                                 comentarios[i].comentario + 
                            '</div>';               
                    }

                    
                    $("#div_comentarios_local_list").append(

                    // Cliente
                    '<div class="col-sm-3">' + 
                        '<div class="text-center push-10">' + 
                             '<img src="<?php echo BASE_PATH ?>/assets/img/cliente/miniaturas/' + comentarios[i].cliente_avatar + '" width="50"></a>' + 
                        '</div>' +     
                        '<div class="text-center push-10">' + 
                             comentarios[i].cliente + 
                        '</div>' +                                                                                                            
                    '</div>' +

                    // Comentario
                    '<div class="col-sm-9 push-10-t">' +  
                        '<div class="pull-left">' + 
                             htmlPuntaje + 
                        '</div>' +    
                        '<div class="pull-right">' + 
                            comentarios[i].fecha_comentario + 
                        '</div>' +       
                        htmlComentario + 
                        '<div class="clearfix"></div>' + 
                        '<div class="pull-left push-10-t font-s12">' + 
                             comentarios[i].detalle + '...&nbsp;&nbsp;&nbsp;' + 
                        '</div>' +                                                                                           
                    '</div>'+ 
                    '<div class="clearfix"></div>' + 
                    '<hr />');

                }

            } 
        });    

    $('#modal_comentarios_local').modal('show');
}


function ver_detalles_local() {
     $('#modal_detalles_local').modal('show');        
};  


function listar_servicios(){

    var oculta_agregar = '';
    var oculta_quitar = '';
    var marca_rubro = '';

    cuenta_filtro_servicio = 0;

    $('#div_filtros_servicios_aplicados_items').html('');

    // FILTROS por ANIMALES
    if (filtros_aplicados_servicio_animal == '')
    {              
        if (filtro_servicio_animal) filtro_servicio_animal.show();
        if (select_servicio_animal)
        {
            select_servicio_animal.show();      
            select_servicio_animal.html('');
        } 

        // Agrega items del filtro
        $.ajax({
           type: "POST",
           data: {'local_id': local_id},
           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_animal_servicio/" + filtros_aplicados_servicio_id + "/" + filtros_aplicados_servicio_item_id,
           success: function(data)
           {
               data_servicio_animal = jQuery.parseJSON(data);

               if (data_servicio_animal.animales)
               {
                    $('#div_filtros_servicios_animal').show();
                    if (filtro_servicio_animal) filtro_servicio_animal.show();

                    if (select_servicio_animal)
                    {
                        select_servicio_animal.show();      
                        select_servicio_animal.html('');
                    } 

                   $.each(data_servicio_animal.animales, function(indice, animal)
                    {
                        var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                        var item = $(option)
                            .appendTo(select_servicio_animal)
                            .on('click', function () 
                            {
                                $('.div_cargando_servicios').show();

                                var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                filtros_aplicados_servicio_animal = animal.nombre;
                                filtros_aplicados_servicio_animal_id = animal.id;
     
                               // Filtra por el animal seleccionado
                                servicios_local = servicios_local.filter(function (el) 
                                {
                                    return el.animal == animal.nombre;
                                }); 

                                listar_servicios();

                                cuenta_filtros_servicios_aplicados++;

                                // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                if (cuenta_filtros_servicios_aplicados == 1) $('#div_filtros_servicios_aplicados').show();
                            } )   

                        cuenta_filtro_servicio_animal++;
                    });   

                                    
               }

               if (data_servicio_animal.animales.length == 0)
               {
                    // Oculta este filtro
                    filtro_servicio_animal.hide();
                    select_servicio_animal.hide();
               }                                

           }
       });

    }

    // Filtro ya aplicado
    else
    {   
        // Oculta este filtro
        filtro_servicio_animal.hide();
        select_servicio_animal.hide();

        // Agrega item seleccionado a filtros aplicados
        aplicado_servicio_animal = $('<div />')
            .html(filtros_aplicados_servicio_animal + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
            .appendTo( $('#div_filtros_servicios_aplicados_items'))

            // Funcion para quitar filtro aplicado
            .on('click', function ()
            { 
                $('.div_cargando_servicios').show();

                // Reset del filtro actual                                    
                var val = '';

                cuenta_filtros_servicios_aplicados--;
                cuenta_filtro_servicio_animal--;
                filtros_aplicados_servicio_animal = '';
                filtros_aplicados_servicio_animal_id = -1;

                //Quita el item
                this.remove();

                servicios_local = resultado.servicios_item;

                // Filtra por servicio, si hay uno seleccionado
                if (filtros_aplicados_servicio != '')
                {
                    servicios_local = servicios_local.filter(function (el) 
                    {
                        return el.servicio == filtros_aplicados_servicio;
                    }); 
                }    

                // Filtra por servicio item, si hay uno seleccionado
                if (filtros_aplicados_servicio_item != '')
                {
                    servicios_local = servicios_local.filter(function (el) 
                    {
                        return el.nombre == filtros_aplicados_servicio_item;
                    }); 
                }                                              

                listar_servicios();                                                  

                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                if (cuenta_filtros_servicios_aplicados == 0) $('#div_filtros_servicios_aplicados').hide();

            }); 
    }   

    // FILTRO por SERVICIOS
    if (filtros_aplicados_servicio == '')
    {              
        if (filtro_servicio) filtro_servicio.show();
        if (select_servicio)
        {
            select_servicio.show();      
            select_servicio.html('');
        } 

        // Agrega items del filtro
        $.ajax({
           type: "POST",
           data: {'local_id': local_id},
           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_servicio/" + filtros_aplicados_servicio_animal_id,
           success: function(data)
           {
               data_servicio = jQuery.parseJSON(data);

               if (data_servicio.servicios)
               {
                    $('#div_filtros_servicios_servicio').show();
                    if (filtro_servicio) filtro_servicio.show();

                    if (select_servicio)
                    {
                        select_servicio.show();      
                        select_servicio.html('');
                    } 

                   $.each(data_servicio.servicios, function(indice, servicio)
                    {
                        var option = '<li class="filtro-item" value="' + servicio.id + '">' + servicio.nombre_cuenta+ '</li>';
                        var item = $(option)
                            .appendTo(select_servicio)
                            .on('click', function () 
                            {
                                $('.div_cargando_servicios').show();

                                var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                filtros_aplicados_servicio = servicio.nombre;
                                filtros_aplicados_servicio_id = servicio.id;
     
                                // Filtra por el servicio seleccionado
                                //servicios_local = resultado.servicios_item;

                                servicios_local = servicios_local.filter(function (el) 
                                {
                                    return el.servicio == servicio.nombre;
                                });  

                                listar_servicios();

                                cuenta_filtros_servicios_aplicados++;

                                // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                if (cuenta_filtros_servicios_aplicados == 1) $('#div_filtros_servicios_aplicados').show();
                            } )   

                        cuenta_filtro_servicio++;
                    });   

               }

               if (data_servicio.servicios.length == 0)
               {
                    // Oculta este filtro
                    filtro_servicio.hide();
                    select_servicio.hide();
               }                                

           }
       });

        if (filtro_servicio_item)
        {
            filtro_servicio_item.hide();
            select_servicio_item.hide();
        } 
    }

    // Filtro ya aplicado
    else
    {   
        // Oculta este filtro
        filtro_servicio.hide();
        select_servicio.hide();

        // Agrega item seleccionado a filtros aplicados
        aplicado_servicio = $('<div />')
            .html(filtros_aplicados_servicio + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
            .appendTo( $('#div_filtros_servicios_aplicados_items'))

            // Funcion para quitar filtro aplicado
            .on('click', function ()
            { 
                $('.div_cargando_servicios').show();

                // Reset del filtro actual                                    
                var val = '';

                cuenta_filtros_servicios_aplicados--;
                cuenta_filtro_servicio--;
                filtros_aplicados_servicio = '';
                filtros_aplicados_servicio_id = -1;

                //Quita el item
                this.remove();

                // Quita servicio_item si hay uno seleccionado
                if (filtros_aplicados_servicio_item != '')
                {                  
                    cuenta_filtros_servicios_aplicados--;
                    cuenta_filtro_servicio_item = 0;
                    filtros_aplicados_servicio_item = '';
                    filtros_aplicados_servicio_item_id = -1;

                    $('#div_filtros_aplicados_servicio_item').html('');
                }

                servicios_local = resultado.servicios_item;

                // Filtra por animal, si hay uno seleccionado
                if (filtros_aplicados_servicio_animal != '')
                {
                    servicios_local = servicios_local.filter(function (el) 
                    {
                        return el.animal == filtros_aplicados_servicio_animal;
                    }); 
                }

                listar_servicios();                                                  

                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                if (cuenta_filtros_servicios_aplicados == 0) $('#div_filtros_servicios_aplicados').hide();

            }); 
    }  


    // Filtro por SERVICIO ITEM
    // Solo se muestra si se seleccionó un servicio
    if (filtros_aplicados_servicio != '')
    {
        if (filtros_aplicados_servicio_item == '')
        {         
            $('#div_filtros_servicios_servicio_item').show();                       

            if (filtro_servicio_item) filtro_servicio_item.show();
            if (select_servicio_item)
            {
                select_servicio_item.show();      
                select_servicio_item.html('');
            } 

            // Agrega items del filtro
            $.ajax({
               type: "POST",
               data: {'local_id': local_id},
               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_servicio_item/" + filtros_aplicados_servicio_id + "/" + filtros_aplicados_servicio_animal_id,
               success: function(data)
               {
                   data_servicio_item = jQuery.parseJSON(data);

                   cuenta_filtro_servicio_item = 0;

                   $.each(data_servicio_item.servicio_items, function(indice, servicio_item)
                    {
                        var option = '<li class="filtro-item" value="' + servicio_item.id + '">' + servicio_item.nombre+ '</li>';
                        var item = $(option)
                            .appendTo(select_servicio_item)
                            .on('click', function () 
                            {
                                $('.div_cargando_servicios').show();

                                var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                filtros_aplicados_servicio_item = servicio_item.nombre;
                                filtros_aplicados_servicio_item_id = servicio_item.id;
     
                                // Filtra por el servicio item seleccionado
                                servicios_local = servicios_local.filter(function (el) 
                                {
                                    return el.nombre == servicio_item.nombre;
                                }); 

                                // Busca por el item seleccionado
                                listar_servicios();    

                                cuenta_filtros_servicios_aplicados++;

                                // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                if (cuenta_filtros_servicios_aplicados == 1) $('#div_filtros_servicios_aplicados').show();
                            } )   

                        cuenta_filtro_servicio_item++;
                    });   


                   if (data_servicio_item.servicio_items.length == 0)
                   {
                        // Oculta este filtro
                        filtro_servicio_item.hide();
                        select_servicio_item.hide();
                   }                                       

               }
           });
        }

        // Filtro ya aplicado
        else
        {
            // Oculta este filtro
            filtro_servicio_item.hide();
            select_servicio_item.hide();

            // Agrega item seleccionado a filtros aplicados
            aplicado_servicio_item = $('<div id="div_filtros_aplicados_servicio_item" />')
                .html(filtros_aplicados_servicio_item + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                .appendTo( $('#div_filtros_servicios_aplicados_items'))

                // Funcion para quitar filtro aplicado
                .on('click', function ()
                { 
                    $('.div_cargando_servicios').show();

                    // Reset del filtro actual                                    
                    var val = '';                              

                    cuenta_filtros_servicios_aplicados--;
                    cuenta_filtro_servicio_item = 0;
                    filtros_aplicados_servicio_item = '';
                    filtros_aplicados_servicio_item_id = -1;

                    //Quita el item
                    this.remove();

                    servicios_local = resultado.servicios_item;

                    // Filtra por el servicio actual
                    servicios_local = servicios_local.filter(function (el) 
                    {
                        return el.servicio == filtros_aplicados_servicio;
                    });                     

                    // Filtra por animal, si hay uno seleccionado
                    if (filtros_aplicados_servicio_animal != '')
                    {
                        servicios_local = servicios_local.filter(function (el) 
                        {
                            return el.animal == filtros_aplicados_servicio_animal;
                        }); 
                    }

                    listar_servicios();

                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                    if (cuenta_filtros_servicios_aplicados == 0) $('#div_filtros_servicios_aplicados').hide();

                }); 
        }  
    }      

    $('.div_cargando_servicios').hide();

    // Limpia lista anterior
    $("#lista_servicios_local").html('');
    
    if (servicios_local.length > 0)
    {

        // Lista de servicios_local
        for (i = 0; i < servicios_local.length; ++i) {

            tachado = '';
            descuento_oculto = '';

            // Servicio con Descuento
            if (servicios_local[i].precio != servicios_local[i].precio_descuento){

                tachado = 'font-w300 tachado text-muted';
            }
            // Servicio sin Descuento
            else{

                tachado = 'font-w600 text-success';
                descuento_oculto = 'oculto';
            } 

            $("#lista_servicios_local").append(
                '<div class="col-md-3 col-sm-4">' + 
                    '<div class="block">' + 
                        '<div class="block-content">' + 
                            '<div class="row push-5" style="position: relative">' +
                                // Ver detalles servicio (mobile)
                                '<div class="ver-detalle-articulo visible-xs">' + 
                                    '<a class="h4 btn btn-redondo btn-muted btn-semitransparente" title="Ver detalle" href="javascript:void(0)" onclick="mostrar_detalles_servicio(' + servicios_local[i].id  + ');"><i class="fa fa-search-plus"></i></a><br/><br/>' + 
                                '</div>' + 
                                                        
                                '<div class="push-10 img-container">' +
                                    '<div class="div-img-articulos">' +
                                        '<img src="<?php echo BASE_PATH ?>/assets/img/servicios/miniaturas/' +  servicios_local[i].imagen +'" alt="" width="100%">' + 
                                    '</div>' +                                       

                                    '<div class="circulo_descuento ' + descuento_oculto + '"><span>'+ servicios_local[i].descuento +'%</span><b></b>OFF</div>' +                             

                                    // Ver detalles servicio (desktop)    
                                    '<div class="img-options">' + 
                                        '<div class="img-options-content">' + 
                                            '<div class="btn-group push-150-t">' + 
                                                '<a class="btn btn-info" href="javascript:void(0)" onclick="mostrar_detalles_servicio(' + servicios_local[i].id  + ');"><i class="fa fa-search-plus"></i> Ver detalles</a>' +
                                            '</div>' + 
                                        '</div>' + 
                                    '</div>' +   
                                '</div>' +   
                            '</div>' +   
                            '<div class="push-10" style="height: 116px;">' + 
                                '<div class="h5 font-w600 pull-left">' +  servicios_local[i].servicio +'</div>' + 
                                '<div class="h5 font-w600 pull-right text-muted">' +  servicios_local[i].animal +'</div>' + 
                                '<div style="height:10px; clear: both;"></div>' +
                                '<div class="h6 font-w600 text-muted"> ' +  servicios_local[i].nombre +'</div>' + 
                                '<div style="height:15px; clear: both;"></div>' +

                                '<div class="h5  pull-left push-5-t ' + tachado + '">$' +  servicios_local[i].precio +'</div>' + 
                                '<div class="h5 font-w600 text-success  pull-left push-5-t push-5-l ' + descuento_oculto + '">$' +  servicios_local[i].precio_descuento +'</div>' + 
                                '<div class="pull-right ">' + 
                                    '<a class="btn btn-sm btn-geotienda-violeta" title="Agendar turno" href="javascript:void(0)" onclick="agendar_turno(' + servicios_local[i].local_servicio_item_id  + ', ' + servicios_local[i].precio_descuento + ', ' + servicios_local[i].id  + ', \'' + servicios_local[i].servicio  + ' \', \'' + servicios_local[i].servicio_codigo  + '\',  \'' + servicios_local[i].nombre + '\');"><i class="fa fa-calendar push-5-r"></i> Agendar</a>' + 
                                '</div>' +                                      
                            '</div>' + 
                        '</div>' + 
                    '</div>' + 
                '</div>');
        }
    }
    // No se encontraron servicios
    else
    {
        $("#lista_servicios_local").append(
            '<div class="div-centrado push-20-t">' + 
            ' <h4 class="push-10 centrado text-center"><i class="fa fa-search push-5-r"></i> No encontramos ningún servicio</h4>' + 
            ' <h5 class="centrado text-center">Probá seleccionando otro local o modificá los filtros seleccionados.</h5>' + 
            '</div>');
    } 
}


function listar_articulos()
{
    local = resultado.local;

    //datatables
    table_articulos = $('#table_articulos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable(
      { 
        "language": 
        {
            "lengthMenu": "&nbsp;Mostrar&nbsp; _MENU_", 
            "emptyTable": "No encontramos artículos en la tienda.",
        },
        "info": false,
        "order": [], //Initial no order.
        "lengthMenu": [[5, 10, 25, -1], ["5 artículos", "10 artículos", "25 artículos", "Todos"]],
        "pageLength": 10,
        //"lengthChange": false, 
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": 
        {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_articulos_local/" + local.id,
            "type": "POST"
        },  
        
        "columnDefs": 
        [
            {
                "targets": [ 1,2,3,4,5,6,7,8 ],
                "visible": false
            }
        ],
        initComplete: function() 
        {
            // Filtro por ANIMAL
            $('#div_filtros_articulos_animal').html('');

            filtro_animal = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
                .appendTo( $('#div_filtros_articulos_animal'));

            select_animal= $('<ul id="ddl_animal"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_animal'));

            // Filtro por RUBRO
            $('#div_filtros_articulos_rubro').html('');

            filtro_rubro = $('<div><div class="filtro-tipo">RUBRO</div></div>')
                .appendTo( $('#div_filtros_articulos_rubro'));

            select_rubro= $('<ul id="ddl_rubro"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_rubro'));

            // Filtro por MARCA
            $('#div_filtros_articulos_marca').html('');

            filtro_marca = $('<div><div class="filtro-tipo">MARCA</div></div>')
                .appendTo( $('#div_filtros_articulos_marca'));

            select_marca= $('<ul id="ddl_marca"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_marca'));    

            // Filtro por RAZA
            $('#div_filtros_articulos_raza').html('');

            filtro_raza = $('<div><div class="filtro-tipo">RAZA</div></div>')
                .appendTo( $('#div_filtros_articulos_raza'));

            select_raza= $('<ul id="ddl_raza"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_raza'));                       
                
            // Filtro por EDAD
            $('#div_filtros_articulos_edad').html('');

            filtro_edad = $('<div><div class="filtro-tipo">EDAD</div></div>')
                .appendTo( $('#div_filtros_articulos_edad'));

            select_edad= $('<ul id="ddl_edad"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_edad'));      

            // Filtro por TAMAÑO
            $('#div_filtros_articulos_tamanio').html('');

            filtro_tamanio = $('<div><div class="filtro-tipo">TAMAÑO</div></div>')
                .appendTo( $('#div_filtros_articulos_tamanio'));

            select_tamanio= $('<ul id="ddl_tamanio"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_tamanio'));    

            // Filtro por PRESENTACION
            $('#div_filtros_articulos_presentacion').html('');

            filtro_presentacion = $('<div><div class="filtro-tipo">PRESENTACIÓN</div></div>')
                .appendTo( $('#div_filtros_articulos_presentacion'));

            select_presentacion= $('<ul id="ddl_presentacion"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_presentacion'));     

            // Filtro por MEDICADOS
            $('#div_filtros_articulos_medicados').html('');

            filtro_medicados = $('<div><div class="filtro-tipo">MEDICADOS</div></div>')
                .appendTo( $('#div_filtros_articulos_medicados'));

            select_medicados= $('<ul id="ddl_medicados"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_medicados'));    


            // Oculta inicialmente filtros que dependen del Rubro y Animal seleccionado
            $('#div_filtros_articulos_marca').hide();
            $('#div_filtros_articulos_raza').hide();
            $('#div_filtros_articulos_tamanio').hide();
            $('#div_filtros_articulos_edad').hide();
            $('#div_filtros_articulos_presentacion').hide();
            $('#div_filtros_articulos_medicados').hide();                        
            filtro_marca.hide();
            filtro_edad.hide();
            filtro_tamanio.hide();
            filtro_raza.hide();
            filtro_presentacion.hide();
            filtro_medicados.hide();
        },        
        drawCallback: function() 
        {
            // Inicializa filtros
            cuenta_filtro_rubro = 0;
            cuenta_filtro_animal = 0;
            cuenta_filtro_marca = 0;
            cuenta_filtro_raza = 0;
            cuenta_filtro_tamanio = 0;
            cuenta_filtro_edad = 0;
            cuenta_filtro_presentacion = 0;
            cuenta_filtro_medicados = 0;

            $('#div_filtros_articulos_aplicados_items').html('');

            this.api().columns().every( function () 
            {
                var column = this;

                // Filtro por ANIMAL
                if (column.index() == 1)
                {
                    if (filtros_aplicados_animal == '')
                    {                                
                        if (filtro_animal) filtro_animal.show();

                        if (select_animal)
                        {
                            select_animal.show();      
                            select_animal.html('');
                        } 

                        // Agrega items del filtro
                        $.ajax({
                           type: "POST",
                           data: {'local_id': local_id},
                           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_animal/" + filtros_aplicados_rubro_id,
                           success: function(data)
                           {
                               data_animal = jQuery.parseJSON(data);

                               $.each(data_animal.animales, function(indice, animal)
                                {
                                    var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                                    var item = $(option)
                                        .appendTo(select_animal)
                                        .on('click', function () 
                                        {
                                            $('.div_cargando').show();
                                            var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                            filtros_aplicados_animal = animal.nombre;
                                            filtros_aplicados_animal_id = animal.id;
                 
                                            // Busca por el item seleccionado
                                            column
                                                .search( val ? val : '', true, false )
                                                .draw();

                                            cuenta_filtros_aplicados++;

                                            // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                            if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                        } )   

                                    cuenta_filtro_animal++;
                                });    

                           }
                       });

                        // Oculta filtros que dependen de Animal
                        if (filtro_marca)
                        {
                            filtro_marca.hide();
                            select_marca.hide();
                        } 

                        if (filtro_raza)
                        {
                            filtro_raza.hide();
                            select_raza.hide();
                        }    

                        if (filtro_tamanio)
                        {
                            filtro_tamanio.hide();
                            select_tamanio.hide();
                        }  

                        if (filtro_edad)
                        {
                            filtro_edad.hide();
                            select_edad.hide();
                        }  

                        if (filtro_presentacion)
                        {
                            filtro_presentacion.hide();
                            select_presentacion.hide();
                        }     

                        if (filtro_medicados)
                        {
                            filtro_medicados.hide();
                            select_medicados.hide();
                        }                                                                     
                    }

                    // Filtro ya aplicado
                    else
                    {
                        // Oculta este filtro
                        filtro_animal.hide();
                        select_animal.hide();

                        // Agrega item seleccionado a filtros aplicados
                        aplicado_animal = $('<div />')
                            .html(filtros_aplicados_animal + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                            .appendTo( $('#div_filtros_articulos_aplicados_items'))

                            // Funcion para quitar filtro aplicado
                            .on('click', function ()
                            { 
                                var val = '';

                                $('.div_cargando').show();

                                // Quita filtros aplicados que dependen de Animal
                                var table_articulos = $('#table_articulos').DataTable();
                                table_articulos.columns(3).search( val ? val : '', true, false );
                                table_articulos.columns(4).search( val ? val : '', true, false );
                                table_articulos.columns(5).search( val ? val : '', true, false );
                                table_articulos.columns(6).search( val ? val : '', true, false );
                                table_articulos.columns(7).search( val ? val : '', true, false );
                                table_articulos.columns(8).search( val ? val : '', true, false );

                                // Reset del filtro actual                                    
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();                                    

                                cuenta_filtros_aplicados--;
                                filtros_aplicados_animal = '';
                                filtros_aplicados_animal_id = -1;

                                //Quita el item
                                this.remove();

                                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                            }); 
                    }
                }


                // Filtro por RUBRO
                if (column.index() == 2)
                {
                    if (filtros_aplicados_rubro == '')
                    {                                
                        if (filtro_rubro) filtro_rubro.show();

                        if (select_rubro)
                        {
                            select_rubro.show();      
                            select_rubro.html('');
                        } 

                        // Agrega items del filtro
                        $.ajax({
                           type: "POST",
                           data: {'local_id': local_id},
                           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_rubro/" + filtros_aplicados_animal_id,
                           success: function(data)
                           {
                               data_rubro = jQuery.parseJSON(data);

                               $.each(data_rubro.rubros, function(indice, rubro)
                                {
                                    var option = '<li class="filtro-item" value="' + rubro.id + '">' + rubro.nombre_cuenta+ '</li>';
                                    var item = $(option)
                                        .appendTo(select_rubro)
                                        .on('click', function () 
                                        {
                                            $('.div_cargando').show();
                                            var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                            filtros_aplicados_rubro = rubro.nombre;
                                            filtros_aplicados_rubro_id = rubro.id;
                 
                                            // Busca por el item seleccionado
                                            column
                                                .search( val ? val : '', true, false )
                                                .draw();

                                            cuenta_filtros_aplicados++;

                                            // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                            if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                        } )   

                                    cuenta_filtro_rubro++;
                                }); 

                           }
                       });

                        // Oculta filtros que dependen de Rubro
                        if (filtro_marca)
                        {
                            filtro_marca.hide();
                            select_marca.hide();
                        } 

                        if (filtro_raza)
                        {
                            filtro_raza.hide();
                            select_raza.hide();
                        }    

                        if (filtro_tamanio)
                        {
                            filtro_tamanio.hide();
                            select_tamanio.hide();
                        }   

                        if (filtro_edad)
                        {
                            filtro_edad.hide();
                            select_edad.hide();
                        } 

                        if (filtro_presentacion)
                        {
                            filtro_presentacion.hide();
                            select_presentacion.hide();
                        } 

                        if (filtro_medicados)
                        {
                            filtro_medicados.hide();
                            select_medicados.hide();
                        } 

                    }

                    // Filtro ya aplicado
                    else
                    {
                        // Oculta este filtro
                        filtro_rubro.hide();
                        select_rubro.hide();

                        // Agrega item seleccionado a filtros aplicados
                        aplicado_rubro = $('<div />')
                            .html(filtros_aplicados_rubro + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                            .appendTo( $('#div_filtros_articulos_aplicados_items'))

                            // Funcion para quitar filtro aplicado
                            .on('click', function ()
                            { 
                                var val = '';

                                $('.div_cargando').show();

                                // Quita filtros aplicados que dependen de Rubro
                                var table_articulos = $('#table_articulos').DataTable();
                                table_articulos.columns(3).search( val ? val : '', true, false );
                                table_articulos.columns(4).search( val ? val : '', true, false );
                                table_articulos.columns(5).search( val ? val : '', true, false );
                                table_articulos.columns(6).search( val ? val : '', true, false );
                                table_articulos.columns(7).search( val ? val : '', true, false );
                                table_articulos.columns(8).search( val ? val : '', true, false );

                                // Reset del filtro actual                                    
                                filtros_aplicados_rubro = '';
                                filtros_aplicados_rubro_id = -1;

                                column
                                    .search( val ? val : '', true, false )
                                    .draw();                                    

                                cuenta_filtros_aplicados--;

                                //Quita el item
                                this.remove();

                                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                            });

                    }
                }


                // Filtro por MARCA
                if (column.index() == 3)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'marca' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_marca == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_marca/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_marca = jQuery.parseJSON(data);

                                   if (data_marca.marcas)
                                   {
                                        $('#div_filtros_articulos_marca').show();
                                        if (filtro_marca) filtro_marca.show();

                                        if (select_marca)
                                        {
                                            select_marca.show();      
                                            select_marca.html('');
                                        } 

                                       $.each(data_marca.marcas, function(indice, marca)
                                        {
                                            var option = '<li class="filtro-item" value="' + marca.id + '">' + marca.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_marca)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_marca = marca.nombre;
                                                    filtros_aplicados_marca_id = marca.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_marca++;   
                                        });   

                                   }

                                   if (data_marca.marcas.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_marca.hide();
                                        select_marca.hide();
                                   }                                      
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_marca.hide();
                            select_marca.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_marca = $('<div />')
                                .html(filtros_aplicados_marca + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_marca = '';
                                    filtros_aplicados_marca_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        if (filtros_aplicados_marca != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_marca = '';
                            filtros_aplicados_marca_id = -1;
                        }                                
                    }                    
                }  

                //  Filtro por EDAD
                if (column.index() == 4)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'edad' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_edad == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_edad/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_edad = jQuery.parseJSON(data);

                                   if (data_edad.edades)
                                   {
                                        $('#div_filtros_articulos_edad').show();
                                        if (filtro_edad) filtro_edad.show();

                                        if (select_edad)
                                        {
                                            select_edad.show();      
                                            select_edad.html('');
                                        } 

                                       $.each(data_edad.edades, function(indice, edad)
                                        {
                                            var option = '<li class="filtro-item" value="' + edad.nombre + '">' + edad.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_edad)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_edad = edad.nombre;
                                                    filtros_aplicados_edad_id = edad.nombre;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_edad++;   
                                        });   

                                   }
                                   
                                   if (data_edad.edades.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_edad.hide();
                                        select_edad.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_edad.hide();
                            select_edad.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_edad = $('<div />')
                                .html(filtros_aplicados_edad + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_edad = '';
                                    filtros_aplicados_edad_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_edad != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_edad = '';
                            filtros_aplicados_edad_id = -1;
                        }
                    }                     
                }  


                //  Filtro por TAMAÑO
                if (column.index() == 5)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'tamanio' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_tamanio == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_tamanio/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_tamanio = jQuery.parseJSON(data);

                                   if (data_tamanio.tamanios)
                                   {
                                        $('#div_filtros_articulos_tamanio').show();
                                        if (filtro_tamanio) filtro_tamanio.show();

                                        if (select_tamanio)
                                        {
                                            select_tamanio.show();      
                                            select_tamanio.html('');
                                        } 

                                       $.each(data_tamanio.tamanios, function(indice, tamanio)
                                        {
                                            var option = '<li class="filtro-item" value="' + tamanio.id + '">' + tamanio.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_tamanio)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_tamanio = tamanio.nombre;
                                                    filtros_aplicados_tamanio_id = tamanio.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_tamanio++;   
                                        });   

                                   }
                                   
                                   if (data_tamanio.tamanios.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_tamanio.hide();
                                        select_tamanio.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_tamanio.hide();
                            select_tamanio.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_tamanio = $('<div />')
                                .html(filtros_aplicados_tamanio + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_tamanio = '';
                                    filtros_aplicados_tamanio_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_tamanio != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_tamanio = '';
                            filtros_aplicados_tamanio_id = -1;
                        }
                    }                                         
                } 


                // Filtro por RAZA
                if (column.index() == 6)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'raza' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_raza == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_raza/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_raza = jQuery.parseJSON(data);

                                   if (data_raza.razas)
                                   {
                                        $('#div_filtros_articulos_raza').show();
                                        if (filtro_raza) filtro_raza.show();

                                        if (select_raza)
                                        {
                                            select_raza.show();      
                                            select_raza.html('');
                                        } 

                                       $.each(data_raza.razas, function(indice, raza)
                                        {
                                            var option = '<li class="filtro-item" value="' + raza.id + '">' + raza.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_raza)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_raza = raza.nombre;
                                                    filtros_aplicados_raza_id = raza.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_raza++;   
                                        });   

                                   }

                                   if (data_raza.razas.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_raza.hide();
                                        select_raza.hide();
                                   }                                   
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_raza.hide();
                            select_raza.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_raza = $('<div />')
                                .html(filtros_aplicados_raza + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_raza = '';
                                    filtros_aplicados_raza_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    }  
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_raza != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_raza = '';
                            filtros_aplicados_raza_id = -1;
                        }
                    }                                       
                }   


                //  Filtro por PRESENTACION
                if (column.index() == 7)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'presentacion' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_presentacion == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_presentacion/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_presentacion = jQuery.parseJSON(data);

                                   if (data_presentacion.presentaciones)
                                   {
                                        $('#div_filtros_articulos_presentacion').show();
                                        if (filtro_presentacion) filtro_presentacion.show();

                                        if (select_presentacion)
                                        {
                                            select_presentacion.show();      
                                            select_presentacion.html('');
                                        } 

                                       $.each(data_presentacion.presentaciones, function(indice, presentacion)
                                        {
                                            var option = '<li class="filtro-item" value="' + presentacion.id + '">' + presentacion.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_presentacion)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_presentacion = presentacion.nombre;
                                                    filtros_aplicados_presentacion_id = presentacion.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_presentacion++;   
                                        });   

                                   }
                                   
                                   if (data_presentacion.presentaciones.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_presentacion.hide();
                                        select_presentacion.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_presentacion.hide();
                            select_presentacion.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_presentacion = $('<div />')
                                .html(filtros_aplicados_presentacion + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_presentacion = '';
                                    filtros_aplicados_presentacion_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    } 
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_presentacion != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_presentacion = '';
                            filtros_aplicados_presentacion_id = -1;
                        }
                    }                                        
                }


                //  Filtro por MEDICADOS
                if (column.index() == 8)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'medicados' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_medicados == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'local_id': local_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_medicados/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id,
                               success: function(data)
                               {
                                   data_medicados = jQuery.parseJSON(data);

                                   if (data_medicados.medicados)
                                   {
                                        $('#div_filtros_articulos_medicados').show();
                                        if (filtro_medicados) filtro_medicados.show();

                                        if (select_medicados)
                                        {
                                            select_medicados.show();      
                                            select_medicados.html('');
                                        } 

                                       $.each(data_medicados.medicados, function(indice, medicados)
                                        {
                                            var option = '<li class="filtro-item" value="' + medicados.id + '">' + medicados.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_medicados)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_medicados = medicados.nombre;
                                                    filtros_aplicados_medicados_id = medicados.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                } )

                                                cuenta_filtro_medicados++;   
                                        });   

                                   }
                                   else
                                   {
                                         // Oculta este filtro
                                        filtro_medicados.hide();
                                        select_medicados.hide();                                   
                                   }
                                   
                                   if (data_medicados.medicados.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_medicados.hide();
                                        select_medicados.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_medicados.hide();
                            select_medicados.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_medicados = $('<div />')
                                .html('Medicados: ' + filtros_aplicados_medicados + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_medicados = '';
                                    filtros_aplicados_medicados_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                });
                        }                       
                    } 
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_medicados != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_medicados = '';
                            filtros_aplicados_medicados_id = -1;
                        }
                    }                                       
                }

            } );

            $('.div_cargando').hide();
        },           
        
    });
}


function ordenarArticulos(orden)
{
    var index = 0;

    switch (orden)
    {
         case 'nombre':
            index = 0;
            orden = 'asc';
            break;

         case 'descuento':
            index = 1;
            orden = 'desc';
            break;

         case 'precio':
             index = 2;
             orden = 'asc';
             break;
    }

    var table_articulos = $('#table_articulos').DataTable();
    
    table_articulos
        .columns(index)
        .order(orden)
        .draw();
}


// MOBILE - Oculta 'Ver mi pedido' y muestra 'Ocultar mi pedido'
$('#mas_datos_pedido_mob').on('show.bs.collapse', function (e) {

    $("#titulo_ver_pedido_mob").html("Ocultar mi pedido"); 
})


// MOBILE - Muestra 'Ver mi pedido'
$('#mas_datos_pedido_mob').on('hidden.bs.collapse', function (e) {

   $("#titulo_ver_pedido_mob").html("Ver mi pedido ($" + total_pedido + ")");  
})



function quitar_del_pedido(articulo_id, indice_presentacion)
{

    var btn_agrega_pedido;
    var btn_quita_pedido;
    var btn_agrega_pedido_muted;
    var btn_quita_pedido_muted;        
    var linea_a_quitar;
    var linea_a_quitar_fin_pedido;
    var key = '';


    // Identificador único de cada linea de pedido
    key = articulo_id + '_' + indice_presentacion;

    // Quita el articulo de la lista de pedido
    pedido = pedido.filter(function (el) {
              return (el.key !== key);
         });

    // Muestra boton 'Agregar al pedido' del articulo seleccionado
    btn_agrega_pedido = "#btn_agrega_pedido_" + articulo_id + "_" + indice_presentacion;
    btn_agrega_pedido_muted = "#btn_agrega_pedido_muted_" + articulo_id + "_" + indice_presentacion;
    $(btn_agrega_pedido).show();
    $(btn_agrega_pedido_muted).show();

    btn_agrega_pedido_modal = "#btn_agrega_pedido_modal_" + articulo_id + "_" + indice_presentacion;
    btn_agrega_pedido_muted_modal = "#btn_agrega_pedido_muted_modal_" + articulo_id + "_" + indice_presentacion;
    $(btn_agrega_pedido_modal).show();
    $(btn_agrega_pedido_muted_modal).show();        

    // Oculta boton 'Quitar del pedido' del articulo seleccionado
    btn_quita_pedido = "#btn_quita_pedido_" + articulo_id + "_" + indice_presentacion;
    btn_quita_pedido_muted = "#btn_quita_pedido_muted_" + articulo_id + "_" + indice_presentacion;
    $(btn_quita_pedido).hide();
    $(btn_quita_pedido_muted).hide();

    btn_quita_pedido_modal = "#btn_quita_pedido_modal_" + articulo_id + "_" + indice_presentacion;
    btn_quita_pedido_muted_modal = "#btn_quita_pedido_muted_modal_" + articulo_id + "_" + indice_presentacion;
    $(btn_quita_pedido_modal).hide();
    $(btn_quita_pedido_muted_modal).hide();

    // Quita fila de la tabla de pedidos
    linea_a_quitar = "#linea_pedido_" + articulo_id + "_" + indice_presentacion;
    $(linea_a_quitar).remove();    

    // Quita fila de la tabla de pedidos (mobile)
    linea_a_quitar_mob = "#linea_pedido_mob_" + articulo_id + "_" + indice_presentacion;
    $(linea_a_quitar_mob).remove();     

    // Quita fila de la tabla de pedidos del modal 'Finalizar Pedido'
    linea_a_quitar_fin_pedido = "#linea_pedido_fin_pedido_" + articulo_id + "_" + indice_presentacion;
    $(linea_a_quitar_fin_pedido).remove();            

    // Actualiza totales en pantalla
    actualiza_total_pantalla();        

    $("#titulo_ver_pedido_mob").html("Ocultar mi pedido");                    
}


function busca_articulo_en_pedido(articulo_id, indice_presentacion){

    var en_pedido = false;
    var key = '';

    // Identificador único de cada linea de pedido
    key = articulo_id + '_' + indice_presentacion;

    pedido_filtrado = pedido.filter(function (el) {
              return (el.key === key);
         });

    if (pedido_filtrado.length > 0) en_pedido = true;

    return en_pedido;
}


function agregar_al_pedido(articulo_id, indice_presentacion, presentacion_id){

    var articulo_seleccionado;
    var linea_pedido = [];
    var btn_agrega_pedido;
    var btn_quita_pedido;
    var btn_agrega_pedido_muted;
    var btn_quita_pedido_muted;        
    var marca_rubro = '';
    var presentacion = ''; 
    var key = '';       
    var imagen_locales = '';

    local = resultado.local;

    // Recupera datos del articulo a mostrar
    $.ajax({
      url : "<?php echo BASE_PATH ?>/local/get_articulo_by_id_presentaciones_activas/" + articulo_id + '/' + local.id,
      cache: false,
      success: function(data)
      {
        articulo = jQuery.parseJSON(data);

        // Identificador único de cada linea de pedido
        key = articulo_id + '_' + indice_presentacion;

        // Inserta articulo en array de pedidos        
        linea_pedido = {key: key, articulo_id: articulo_id, presentacion_id: presentacion_id, indice_presentacion: indice_presentacion, cantidad: 1, precio: articulo.presentacion[indice_presentacion].precio_descuento};
        pedido.push(linea_pedido);

        // Oculta boton 'Agregar al pedido' del articulo seleccionado
        btn_agrega_pedido = "#btn_agrega_pedido_" + articulo_id + "_" + indice_presentacion;
        btn_agrega_pedido_muted = "#btn_agrega_pedido_muted_" + articulo_id + "_" + indice_presentacion;
        $(btn_agrega_pedido).hide();
        $(btn_agrega_pedido_muted).hide();

        btn_agrega_pedido_modal = "#btn_agrega_pedido_modal_" + articulo_id + "_" + indice_presentacion;
        btn_agrega_pedido_muted_modal = "#btn_agrega_pedido_muted_modal_" + articulo_id + "_" + indice_presentacion;
        $(btn_agrega_pedido_modal).hide();
        $(btn_agrega_pedido_muted_modal).hide();        

        // Muestra boton 'Quitar del pedido' del articulo seleccionado
        btn_quita_pedido = "#btn_quita_pedido_" + articulo_id + "_" + indice_presentacion;
        btn_quita_pedido_muted = "#btn_quita_pedido_muted_" + articulo_id + "_" + indice_presentacion;
        $(btn_quita_pedido).show();
        $(btn_quita_pedido_muted).show();

        btn_quita_pedido_modal = "#btn_quita_pedido_modal_" + articulo_id + "_" + indice_presentacion;
        btn_quita_pedido_muted_modal = "#btn_quita_pedido_muted_modal_" + articulo_id + "_" + indice_presentacion;
        $(btn_quita_pedido_modal).show();
        $(btn_quita_pedido_muted_modal).show();

        // Actualiza total en pantalla
        actualiza_total_pantalla();

        // Si el artículo no tiene marca, muestra el rubro en su lugar
        if (articulo.marca === null) 
            marca_rubro = articulo.rubro;
        else 
            marca_rubro = articulo.marca;

        // Si el artículo tiene asignada la presentación 'Sin Presentación' (presentacion_id = 1), no la muestra
        if (articulo.presentacion[indice_presentacion].presentacion_id != 1) 
            presentacion = articulo.presentacion[indice_presentacion].nombre;

        // Las imágenes de un articulo creado por el Local están en el subdirectorio 'locales'
        if(articulo.tipo == 'Local') imagen_locales = 'locales/';

        // Agrega fila a la tabla de pedidos
        $("#detalle-pedido").append(

                    '<tr id="linea_pedido_' + articulo_id + '_' + indice_presentacion + '">' + 
                    '<td style="width: 65px;">' + 
                        '<select class="form-control cantidad-item-pedido" onchange="sumar_cantidad_pedido(this, ' + articulo_id + ', ' + indice_presentacion + ', ' + articulo.presentacion[indice_presentacion].precio_descuento  + ', ' + articulo.presentacion[indice_presentacion].presentacion_id +')">' + 
                            '<option value="1" selected="selected">1</option>' + 
                            '<option value="2">2</option>' + 
                            '<option value="3">3</option>' + 
                            '<option value="4">4</option>' + 
                            '<option value="5">5</option>' + 
                            '<option value="6">6</option>' + 
                            '<option value="7">7</option>' + 
                            '<option value="8">8</option>' + 
                            '<option value="9">9</option>' + 
                            '<option value="10">10</option>' + 
                            '<option value="11">11</option>' + 
                            '<option value="12">12</option>' + 
                            '<option value="13">13</option>' + 
                            '<option value="14">14</option>' + 
                            '<option value="15">15</option>' + 
                            '<option value="16">16</option>' + 
                            '<option value="17">17</option>' + 
                            '<option value="18">18</option>' + 
                            '<option value="19">19</option>' + 
                            '<option value="20">20</option>' +                             
                        '</select>' + 
                    '</td>' + 
                    '<td  style="padding: 1px 0; text-align: center">' + 
                        '<img class="pedido-img hidden-xs" src="<?php echo BASE_PATH ?>/assets/img/articulos/' + imagen_locales + 'miniaturas/' +  articulo.imagen +'" alt="" height="60">' + 
                    '</td>' +                                               
                    '<td>' + 
                        '<h6>' + marca_rubro + ' <span class="text-muted">' + presentacion + '</span></h6>' + 
                        '<div class="font-s12 text-muted">' + articulo.nombre + '</div>' + 
                    '</td>' + 
                    '<td class="text-right">' + 
                        '<div class="font-w600 text-success">$<span id="total_linea_pedido_' + articulo.id + '_' + indice_presentacion + '">' + articulo.presentacion[indice_presentacion].precio_descuento + '</span></div>' + 
                        '<a title="Quitar producto del pedido"  class="text-danger" href="javascript:void(0)" onclick="quitar_del_pedido(' + articulo_id + ', ' + indice_presentacion + ')"><i class="fa fa-times"></i></a>' +                         
                    '</td>' + 
                '</tr>' 

            );   


       // Agrega fila a la tabla de pedidos (version mobile)
        $("#detalle-pedido-mob").append(

                    '<tr id="linea_pedido_mob_' + articulo_id + '_' + indice_presentacion + '">' + 
                    '<td style="width: 65px;">' + 
                        '<select class="form-control cantidad-item-pedido" onchange="sumar_cantidad_pedido(this, ' + articulo_id + ', ' + indice_presentacion + ', ' + articulo.presentacion[indice_presentacion].precio  + ', ' + articulo.presentacion[indice_presentacion].presentacion_id +')">' + 
                            '<option value="1" selected="selected">1</option>' + 
                            '<option value="2">2</option>' + 
                            '<option value="3">3</option>' + 
                            '<option value="4">4</option>' + 
                            '<option value="5">5</option>' + 
                            '<option value="6">6</option>' + 
                            '<option value="7">7</option>' + 
                            '<option value="8">8</option>' + 
                            '<option value="9">9</option>' + 
                            '<option value="10">10</option>' + 
                            '<option value="11">11</option>' + 
                            '<option value="12">12</option>' + 
                            '<option value="13">13</option>' + 
                            '<option value="14">14</option>' + 
                            '<option value="15">15</option>' + 
                            '<option value="16">16</option>' + 
                            '<option value="17">17</option>' + 
                            '<option value="18">18</option>' + 
                            '<option value="19">19</option>' + 
                            '<option value="20">20</option>' +                             
                        '</select>' + 
                    '</td>' + 
                    '<td  style="padding: 1px 0; text-align: center">' + 
                        '<img class="pedido-img hidden-xs" src="<?php echo BASE_PATH ?>/assets/img/articulos/' + imagen_locales + 'miniaturas/' +  articulo.imagen +'" alt="" height="60">' + 
                    '</td>' +                                               
                    '<td>' + 
                        '<h6 style="color: #646464">' + marca_rubro + ' <span class="text-muted">' + presentacion + '</span></h6>' + 
                        '<div class="font-s12 text-muted">' + articulo.nombre + '</div>' + 
                    '</td>' + 
                    '<td class="text-right">' + 
                        '<div class="font-w600 text-success">$<span id="total_linea_pedido_mob_' + articulo.id + '_' + indice_presentacion + '">' + articulo.presentacion[indice_presentacion].precio_descuento + '</span></div>' + 
                        '<a class="text-danger" href="javascript:void(0)" onclick="quitar_del_pedido(' + articulo_id + ', ' + indice_presentacion + ')"><i class="fa fa-times"></i></a>' +                         
                    '</td>' + 
                '</tr>' 

            );  

        // Agrega fila a la tabla de pedidos del modal Finalizar Pedido
        $("#detalle-pedido-fin-pedido").append(

            '<tr id="linea_pedido_fin_pedido_' + articulo_id + '_' + indice_presentacion + '">' + 
                '<td style="width: 40px;"  id="cantidad_pedido_' + articulo_id + '_' + indice_presentacion + '"><h4>1</h4></td>' + 
                '<td  style="padding: 2px 0; text-align: center">' + 
                    '<img class="pedido-img" src="<?php echo BASE_PATH ?>/assets/img/articulos/' + imagen_locales + 'miniaturas/' +  articulo.imagen +'" alt="" height="70">' + 
                '</td>' +                                               
                '<td>' + 
                    '<h4>' + marca_rubro + ' <span class="text-muted">' + presentacion + '</span></h4>' + 
                    '<div class="font-s13 text-muted">' + articulo.nombre + '</div>' + 
                '</td>' + 
                '<td class="text-right">' + 
                    '<div class="font-w600 text-success">$<span id="total_linea_pedido_fin_pedido_' + articulo.id + '_' + indice_presentacion + '">' + articulo.presentacion[indice_presentacion].precio_descuento + '</span></div>' + 
                '</td>' + 
            '</tr>' 

            );     
        }
    });   
}


function sumar_cantidad_pedido(ddl_cantidad, articulo_id, indice_presentacion, precio, presentacion_id)
{
    var linea_pedido;
    var cantidad = parseInt(ddl_cantidad.value);
    var total_linea_pedido = "#total_linea_pedido_" + articulo_id + "_" + indice_presentacion;
    var total_linea_pedido_mob = "#total_linea_pedido_mob_" + articulo_id + "_" + indice_presentacion;
    var total_linea_pedido_fin_pedido = "#total_linea_pedido_fin_pedido_" + articulo_id + "_" + indice_presentacion;
    var key = '';

    var cantidad_pedido = "#cantidad_pedido_" + articulo_id + "_" + indice_presentacion;

    var nuevo_total = cantidad * parseInt(precio);

    // Actualiza el precio total de la linea
    $(total_linea_pedido).html(nuevo_total);
    $(total_linea_pedido_mob).html(nuevo_total);
    $(total_linea_pedido_fin_pedido).html(nuevo_total);


    // Identificador único de cada linea de pedido
    key = articulo_id + '_' + indice_presentacion;

    // Actualiza el total en lista de pedido
    pedido = pedido.filter(function (el) {
              return (el.key !== key);
         });

    linea_pedido = {key: key, articulo_id: articulo_id, presentacion_id: presentacion_id, indice_presentacion: indice_presentacion, cantidad: cantidad, precio: precio};
    pedido.push(linea_pedido);

    // Actualiza la cantidad en la linea de pedido del modal Finalizar Pedido
    $(cantidad_pedido).html('<h4>' + cantidad + '</h4>');

    actualiza_total_pantalla();
}


function actualiza_total_pantalla(){

    total_pedido = 0;

    for (var i = 0; i < pedido.length; i++) {
        total_pedido += pedido[i].cantidad * pedido[i].precio;
    }

    total_pedido = total_pedido.toFixed();
     
    $("#total-pedido").html(total_pedido);   
    $("#total-pedido-finalizar-pedido").html(total_pedido);   
    $("#fin_pedido_total").val(total_pedido);   
    
    // Version mobile
    $("#total-pedido-mob").html(total_pedido);   
    $("#titulo_ver_pedido_mob").html("Ver mi pedido ($" + total_pedido + ")");   

    // Pedido vacío
    if (total_pedido == 0){
        // Muestra label 'Tu pedido está vacío'
        $("#pedido_vacio").show();

        // Oculta total del pedido
        $("#total_pedido_label").hide();
        $("#botones_pedido").hide();    

        // Oculta 'Mi Pedido' en versión mobile  
        $("#mas_datos_pedido_mob").collapse('hide');
        $("#mi-pedido-mobile").hide();    
    }
    else{
         // Oculta label 'Tu pedido está vacío'
        $("#pedido_vacio").hide();

        // Muestra total del pedido
        $("#total_pedido_label").show();
        $("#botones_pedido").show();   

        // Muestra 'Mi Pedido' en versión mobile    
        $("#mi-pedido-mobile").show();            
    }         
}


function limpiar_pedido(){

    var btn_agrega_pedido;
    var btn_agrega_pedido_muted;
    var btn_quita_pedido;
    var btn_quita_pedido_muted;

    for (var i = 0; i < pedido.length; i++) {

        // Muestra boton 'Agregar al pedido' en cada articulo
        btn_agrega_pedido = "#btn_agrega_pedido_" + pedido[i].articulo_id + '_' + pedido[i].indice_presentacion;
        btn_agrega_pedido_muted = "#btn_agrega_pedido_muted_" + pedido[i].articulo_id + '_' + pedido[i].indice_presentacion;
        $(btn_agrega_pedido).show();
        $(btn_agrega_pedido_muted).show();

        // Oculta boton 'Quitar del pedido' en cada  articulo
        btn_quita_pedido = "#btn_quita_pedido_" + pedido[i].articulo_id + '_' + pedido[i].indice_presentacion;
        btn_quita_pedido_muted = "#btn_quita_pedido_muted_" + pedido[i].articulo_id + '_' + pedido[i].indice_presentacion;
        $(btn_quita_pedido).hide();
        $(btn_quita_pedido_muted).hide();
    }

    pedido = [];        

    $("#detalle-pedido").html('');
    $("#detalle-pedido-mob").html('');
    $("#detalle-pedido-fin-pedido").html('');

    actualiza_total_pantalla();

}


function mostrar_detalles_articulo(articulo_id){

    var oculta_agregar = '';
    var oculta_quitar = '';
    var ocultar_circulo = '';
    var marca_rubro = '';
    var presentacion = '';
    var ocultar_carrito = '';
    var sin_stock = '';
    var html_presentaciones = '';

    local = resultado.local

    // Recupera datos del articulo a mostrar
    $.ajax({
      url : "<?php echo BASE_PATH ?>/local/get_articulo_by_id_presentaciones_activas/" + articulo_id + '/' + local.id,
      cache: false,
      success: function(data)
      {
            articulo = jQuery.parseJSON(data);

            // Si el artículo no tiene marca, muestra el rubro en su lugar
            if (articulo.marca === null) 
                marca_rubro = articulo.rubro;
            else 
                marca_rubro = articulo.marca;

            $("#modal_articulo_marca").html(marca_rubro);
            $("#modal_articulo_nombre").html(articulo.nombre);

            // IMAGEN
            // Artículo creados por el Local
            if(articulo.tipo == 'Local')
              $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/locales/fotos/' +  articulo.imagen + '" alt="">');
            // Artículo Geotienda
            else
              $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/fotos/' +  articulo.imagen + '" alt="">');

            $("#modal_articulo_detalle").html(articulo.detalle);


            // Presentaciones del artículo
            for (j = 0; j < articulo.presentacion.length; ++j) {

                tachado = '';
                descuento_oculto = '';

                // Valida si la presentación ya está en el pedido
                en_pedido = busca_articulo_en_pedido(articulo_id, j);

                // Si está en el pedido, muestra botón 'Quitar del pedido'
                if (en_pedido){
                    oculta_agregar = 'style="display:none"';
                    oculta_quitar = '';
                }
                // Si no está en el pedido, muestra botón 'Agregar al pedido'                
                else{
                    oculta_agregar = '';
                    oculta_quitar = 'style="display:none"';                   
                }

                presentacion = articulo.presentacion[j].nombre;

                // Presentación con Descuento
                if (articulo.presentacion[j].precio != articulo.presentacion[j].precio_descuento){

                    tachado = 'font-w300 tachado text-muted';
                    ocultar_circulo = '';       
                }
                // Presentación sin Descuento
                else{

                    tachado = 'font-w600 text-success';
                    descuento_oculto = 'oculto';
                    ocultar_circulo = 'style="display:none"';       
                }   

                // Presentación con Stock
                if (articulo.presentacion[j].stock == '1'){

                    ocultar_carrito = '';       
                    sin_stock = 'style="display:none"';       
                }
                // Presentación sin Stock
                else{

                    ocultar_carrito = 'style="display:none"';       
                    sin_stock = '';      
                }    

                html_presentaciones += 
                    '<div class="h5 font-w600 text-muted pull-left">' + presentacion +'</div>' +

                    // Agregar al carrito
                    '<div class="pull-right push-10-l push--5-t" ' + ocultar_carrito + '>' + 
                        '<a class="btn btn-redondo btn-redondo-mini btn-celeste" title="Agregar al pedido" id="btn_agrega_pedido_muted_modal_' + articulo_id + '_' + j + '" href="javascript:void(0)" onclick="agregar_al_pedido(' + articulo_id + ', ' + j +', ' + articulo.presentacion[j].presentacion_id + ');"' + oculta_agregar + '>' + 
                            '<i class="fa fa-shopping-cart"></i> ' + 
                        '</a>' + 
                        '<a class="btn btn-redondo btn-redondo-mini btn-celeste" title="Quitar del pedido" id="btn_quita_pedido_muted_modal_' + articulo_id + '_' + j + '" href="javascript:void(0)" onclick="quitar_del_pedido(' + articulo_id + ', ' + j +');" ' +  oculta_quitar + '>' + 
                            '<i class="fa fa-minus"></i>' + 
                        '</a>' +     
                    '</div>' + 

                    // Stock
                    '<div class="pull-right push-10-l push--5-t" ' + sin_stock + '>' + 
                    '<div class="push-5-t" style="color: red;">SIN STOCK</div>' + 
                    '</div>' + 

                    '<div class="h5 pull-right push-10-l ' + tachado + '">$' +  articulo.presentacion[j].precio +'</div>' + 
                    '<br />' + 
                    '<div class="h5 font-w600 text-success pull-right push-10-l ' + descuento_oculto + '">' + 
                    '<div class="circulo_descuento_mini"' + ocultar_circulo + '><span>-'+ articulo.presentacion[j].descuento +'%</span></div>' +                
                    '&nbsp;&nbsp;$' +  articulo.presentacion[j].precio_descuento +'</div>' + 
                    '<div style="height:10px; clear: both;"></div>';
            }

            $("#modal_articulo_presentaciones").html(html_presentaciones);

            $('#modal_detalles_articulo').modal('show');
        } 
    });  

}      


function mostrar_detalles_servicio(servicio_id)
{
    var html_precio = '';

    // Recupera datos del servicio a mostrar
    servicio = resultado.servicios_item.filter(function (el) {
            return el.id == servicio_id;
        });

    $("#modal_servicio_animal").html(servicio[0].animal);
    $("#modal_servicio_nombre").html(servicio[0].nombre);
    $("#modal_servicio_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/servicios/miniaturas/' +  servicio[0].imagen  + '" alt="">');
    $("#modal_servicio_detalle").html(servicio[0].detalle);

    // Precio con descuento
    if (servicio[0].precio != servicio[0].precio_descuento)
    {
        tachado = 'font-w300 tachado text-muted';
        ocultar_circulo = '';      
        descuento_oculto = '';         
    }
    // Precio sin Descuento
    else{

        tachado = 'font-w600 text-success';
        descuento_oculto = 'oculto';
        ocultar_circulo = 'style="display:none"';       
    }

    html_precio = 
        '<div class="h4 pull-left ' + tachado + '">$' +  servicio[0].precio +'</div>' + 
        '<div class="h4 font-w600 text-success pull-left push-10-l ' + descuento_oculto + '">$' +  servicio[0].precio_descuento +'</div>';

    $("#modal_servicio_precio").html(html_precio);


    $('#modal_detalles_servicio').modal('show');
}


function finalizar_pedido(){

    var url;
    var respuesta;

	// Valida que el usuario esté loggeado
	if (!usuario_logged_in)
	{
        pantalla_retorno = 'pedido';
		show_login();
	}
    else
    {
        $('#form_finalizar_pedido')[0].reset(); // reset form
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        // Carga datos del cliente
        $.ajax({
          url : "<?php echo BASE_PATH ?>/Cliente/ajax_cargar_datos/" + user_id,
          cache: false,
          success: function(data){

            respuesta = jQuery.parseJSON(data);

            $('#fin_pedido_nombre').val(respuesta.cliente.nombre + ' ' + respuesta.cliente.apellido);
            $('#fin_pedido_direccion').val(respuesta.cliente.direccion);
            $('#fin_pedido_localidad').val(respuesta.cliente.localidad);
            $('#fin_pedido_telefono').val(respuesta.cliente.telefono);
            $('#fin_pedido_cliente_id').val(respuesta.cliente.id);                
            $('#fin_pedido_provincia_id').val(respuesta.cliente.id_provincia);
            $('#fin_pedido_provincia').val($("#fin_pedido_provincia_id option:selected").text());
          } 
        });

        $('#fin_pedido_local_id').val(local_id);

        $('#modal_finalizar_pedido').modal('show');
    } 
}        

// Si es envio a domicilio, muestra campos correspondientes
function toggle_envio(ddl_envio){

    var envio = ddl_envio.value;

    $("#fin_pedido_envio").val(envio);
    
    if (envio == 'A domicilio') 
        $('.retiro-domicilio').show();
    else 
        $('.retiro-domicilio').hide();
}


// Provincia del cliente
$("#fin_pedido_provincia_id").change(function(){
    $('[name="fin_pedido_provincia"]').val($("#fin_pedido_provincia_id option:selected").text()); 
});


function procesar_pedido()
{
    $('#btn_procesar_pedido').text('Procesando...');
    $('#btn_procesar_pedido').attr('disabled', true);

    $('.form-group').removeClass('has-error'); 
    $('.help-block').empty(); 

    var url = "<?php echo BASE_PATH ?>/Venta/ajax_procesar_pedido";


    // Serializa detalles del pedido
    var data_post = $('#form_finalizar_pedido').serializeArray();

    data_post.push({name: 'nro_lineas', value: pedido.length});

    for (var i = 0; i < pedido.length; i++) {

        data_post.push({name: 'detalle_articulo_id_' + i, value: pedido[i]["articulo_id"]});
        data_post.push({name: 'detalle_presentacion_id_' + i, value: pedido[i]["presentacion_id"]});
        data_post.push({name: 'detalle_precio_' + i, value: pedido[i]["precio"]});
        data_post.push({name: 'detalle_cantidad_' + i, value: pedido[i]["cantidad"]});
    }

    // Nombre del local
    data_post.push({name: 'local_nombre', value: local.nombre});
    
    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            // Pedido con datos del cliente ok
            if(data.status) 
            {
                // Pedido con insert ok
                if(data.venta) 
                {                    
                    // Procesa pago por Mercadopago
                    if(data.pago_con_mp)
                    {
                        window.location.replace(data.mercadopago.response.sandbox_init_point);
                    }

                    // Pago en efectivo o tarjetas al retirar la compra
                    else
                    {
                        $('#modal_finalizar_pedido').modal('hide');

                        // Mensaje pedido ok
                        $('#resultado_ok_titulo1').html('¡Felicitaciones! Tu pedido ha sido exitoso');
                        $('#resultado_ok_titulo2').html('Gracias por elegir GeoTienda. En pocos minutos recibirás un email con los datos de tu compra.');

                        // Marca para saber donde redireccionar al finalizar el pedido
                        redirecciona_fin = 'pedidos';
                        $('#modal_resultado_ok').modal({ backdrop: 'static', keyboard: false })
                        $('#modal_resultado_ok').modal('show');   
                    }
                }
                else
                {
                    $('#modal_finalizar_pedido').modal('hide');         
                    $('#modal_error_sistema').modal('show');

                    $('#btn_procesar_pedido').text('Comprar'); 
                    $('#btn_procesar_pedido').attr('disabled', false); 
                }                
            }

            // Error en datos ingresados del pedido
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]);
                }

                $('#btn_procesar_pedido').text('Comprar'); 
                $('#btn_procesar_pedido').attr('disabled', false);                     
            }

        },
        // Error al procesar el pedido
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);

            $('#modal_finalizar_pedido').modal('hide');         
            $('#modal_error_sistema').modal('show');

            $('#btn_procesar_pedido').text('Comprar'); 
            $('#btn_procesar_pedido').attr('disabled', false); 

        }
    });
    
}    


// Para ventas hechas con Mercadopago (pendiente o concretado), envía mail al cliente con los datos de su compra
function enviar_mail_venta(venta_id){

   var url = "<?php echo BASE_PATH ?>/Venta/ajax_enviar_mail_venta";

    var data_post = {
        'venta_id': venta_id
    };


    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",

        success: function(data)
        {
            console.log('enviar_mail_venta: ' + data);
        },
        // Error al enviar email
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
        }
    });
}


function actualizar_estado_venta(venta_id, estado){

   var url = "<?php echo BASE_PATH ?>/Venta/ajax_actualizar_estado_venta";

    var data_post = {
        'venta_id': venta_id,
        'estado': estado
    };


    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",

        success: function(data)
        {
            console.log('actualizar_estado_venta: ' + data);
        },
        // Error al actualizar estado de la venta
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
        }
    });

}


function redirecciona_fin_pedido(){

    window.location.href ='<?php echo BASE_PATH ?>/dashboard/' + redirecciona_fin;  
}


/* TURNOS */

function agendar_turno(id, precio, item_id, servicio, servicio_codigo, servicio_item)
{
    
    $('#solicitud_turno_titulo').html('Solicitud de turno para ' + servicio.toUpperCase());     
    $('#agendar_turno_local').html('TIENDA: ' + local.nombre); 
    $('#agendar_turno_servicio').html('SERVICIO: ' + servicio_item + ' - $' + precio); 

    // Limpia form 
    $('#div_error_turno').hide(); 
    $('#fin_turno_hora').html('Seleccioná un día para ver los horarios disponibles');
    $('.tabla div.diaactivo span').css('background-color', '#ABFF00');
    $('.tabla div.diainactivo span').css('background-color', '#999');  

    // Carga datos del cliente
    $.ajax({
      url : "<?php echo BASE_PATH ?>/Cliente/ajax_cargar_datos/" + user_id,
      cache: false,
      success: function(data)
      {
        respuesta = jQuery.parseJSON(data);

        $('#fin_turno_telefono').val(respuesta.cliente.telefono);
    
      } 
    });

    // Muestra calendario del Servicio 
    if (servicio_codigo == 'clinica') 
    {
        $('#div_calendario_clinica').show();
        $('#div_calendario_peluqueria').hide();
    }

    if (servicio_codigo == 'peluqueria') 
    {
        $('#div_calendario_clinica').hide();
        $('#div_calendario_peluqueria').show();
    }       

    local_servicio_item_id = id;
    servicio_item_id = item_id;
    precio_turno = precio;

    // Valida que el usuario esté loggeado
    if (!usuario_logged_in)
    {          
        pantalla_retorno = 'turno';
        show_login();
    }
    else
    {
        $('#modal_agendar_turno').modal('show');    
    }
}


function agendar_turno_desde_login()
{
    // Carga datos del cliente
    $.ajax({
      url : "<?php echo BASE_PATH ?>/Cliente/ajax_cargar_datos/" + user_id,
      cache: false,
      success: function(data){

        respuesta = jQuery.parseJSON(data);

        $('#fin_turno_telefono').val(respuesta.cliente.telefono);
    
      } 
    });

    $('#modal_agendar_turno').modal('show');  
}


function ver_horarios(dia, dia_mostrar, servicio, abierto)
{
    // Restaura colores por defecto 
    $('.tabla div.diaactivo span').css('background-color', '#ABFF00');
    $('.tabla div.diainactivo span').css('background-color', '#999');
 
    dia_turno = dia;

    var url = "<?php echo BASE_PATH ?>/Local/ajax_ver_horarios/" + servicio;

    var data_post = {
        'local_id': local_id, 
        'fecha': dia, 
        'fecha_mostrar': dia_mostrar, 
        'local_servicio_item_id': local_servicio_item_id
    };

    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            // Resalta dia seleccionado
            $('.a'+dia).css('background-color', '#ffbf00');   

            if(data.status) 
            {
                $('#fin_turno_hora').html(data.horarios);
            }
        },
        // Error al cargar horarios
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_agendar_turno').modal('hide');    
            $('#modal_error_sistema').modal('show');
        }
    });
}

function calendario_cambia_mes(fecha, servicio)
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_calendario_cambia_mes";

    var data_post = {
        'local_id': local_id, 
        'fecha': fecha,
        'servicio': servicio
    };

    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            switch (servicio)
            {
                 case 'clinica':
                    $('#calendario_clinica').html(data.calendario);
                    break;

                 case 'peluqueria':
                    $('#calendario_peluqueria').html(data.calendario);
                    break;
            }           
             
        },
        // Error al cargar calendario
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_agendar_turno').modal('hide');  
            $('#modal_error_sistema').modal('show');
        }
    });
}


function procesar_turno(){

    var url;
    var respuesta;


    // TODO: validar que el cliente tenga cargados los datos mínimos en su cuenta como para agendar un turno

    $('#btn_procesar_turno').text('Procesando...');
    $('#btn_procesar_turno').attr('disabled', true);

    var url = "<?php echo BASE_PATH ?>/Turno/ajax_procesar_turno";

    
     var data_post = {
        'fin_turno_local_id': local_id, 
        'fin_turno_local_nombre': local.nombre, 
        'fin_turno_cliente_id': user_id,
        'fin_turno_servicio_item_id': servicio_item_id,
        'fin_turno_fecha': dia_turno, 
        'fin_turno_hora': $('#ddl_horario').val(),
        'fin_turno_telefono' : $('#fin_turno_telefono').val(), 
        'fin_turno_observaciones' : $('#fin_turno_observaciones').val(), 
        'fin_turno_precio' : precio_turno
    };
    
    
    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            // Pedido con datos ok
            if(data.status) 
            {
                $('#modal_agendar_turno').modal('hide');    

                // Mensaje reserva de turno ok
                $('#resultado_ok_titulo1').html('¡Felicitaciones! La reserva del turno ha sido exitosa');
                $('#resultado_ok_titulo2').html('Gracias por elegir GeoTienda. En pocos minutos recibirás un email con los datos de tu reserva');                        

                // Marca para saber donde redireccionar al finalizar el pedido
                redirecciona_fin = 'turnos';
                $('#modal_resultado_ok').modal({ backdrop: 'static', keyboard: false })
                $('#modal_resultado_ok').modal('show');                     
            }

            // Error en datos ingresados del turno
            else
            {
                $('#div_error_turno').show(); 
            }
            $('#btn_procesar_turno').text('Agendar Turno'); 
            $('#btn_procesar_turno').attr('disabled', false); 

        },
        // Error al procesar el pedido
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);

            $('#modal_agendar_turno').modal('hide');         
            $('#modal_error_sistema').modal('show');

            $('#btn_procesar_turno').text('Agendar Turno'); 
            $('#btn_procesar_turno').attr('disabled', false); 

        }
    });
} 

/* fin TURNOS */


/* FUNCIONES VARIAS */

// Oculta cualquier modal al hacer 'back' en el browser o celular
$(".modal").on("shown.bs.modal", function()  { 
    var urlReplace = "#" + $(this).attr('id'); 
    history.pushState(null, null, urlReplace); 
  });

  $(window).on('popstate', function() { 
    $(".modal").modal('hide');
  });

  /* fin FUNCIONES VARIAS */    
  

 /* TOGGLE PRODUCTOS / SERVICIOS */
$('#toggle_prod_serv').change(function() {

    // Ver Productos
    if($("#toggle_prod_serv").is(':checked')) 
    {  
        ver_articulos();
    } 
    // Ver Servicios
    else 
    {  
        ver_servicios();
    }  
});


function ver_servicios()
{
    // Para servicios usa 10 columnas
    $("#div_detalles_local_listados").removeClass("col-sm-6").addClass("col-sm-10");

    // Oculta 'Mi Pedido', filtros y lista de artículos
    $('#div_mi_pedido').hide();
    $("#lista_articulos").hide();
    $("#div_filtros_articulos").hide();      

    // Muestra filtros y lista de servicios
    $("#lista_servicios_local").show();       
    $("#div_filtros_servicios").show();     
}


function ver_articulos()
{
    // Para articulos usa 6 columnas
    $("#div_detalles_local_listados").removeClass("col-sm-10").addClass("col-sm-6");

    // Muestra 'Mi Pedido', filtros y lista de artículos
    $('#div_mi_pedido').show();
    $("#lista_articulos").show();
    $("#div_filtros_articulos").show();  

    // Oculta filtros y lista de servicios
    $("#lista_servicios_local").hide();              
    $("#div_filtros_servicios").hide();             
}
/* fin TOGGLE PRODUCTOS / SERVICIOS */


// Submit del form de búsqueda de articulos 
$('.buscar-articulo-filtro').submit(function() {

    // Agrega local_id y categoria
    $('<input />').attr('type', 'hidden')
      .attr('name', "cat")
      .attr('value', '<?php echo $categoria; ?>')
      .prependTo(this);

    $('<input />').attr('type', 'hidden')
      .attr('name', "id")
      .attr('value', '<?php echo $id; ?>')
      .prependTo(this); 


});  


</script>