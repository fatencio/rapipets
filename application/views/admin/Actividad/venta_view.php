<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Ventas
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Venta/index');">Ventas</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">

            <!-- Tabla 'VENTAS' para Excel (oculta) -->
            <table id="table_ventas" cellspacing="0" width="100%" style="display: none;">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Fecha</th>
                        <th>Forma Pago</th>                        
                        <th>Estado</th>
                        <th>Cliente</th>                            
                        <th>Local</th>                                           
                        <th>Artículo</th>                                           
                        <th>Presentación</th>                                           
                        <th>Cantidad</th>                                                                   
                        <th>Precio Unitario</th>                                           
                        <th>Total</th>                                                  
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>    
            </table>   

            <!-- Tabla 'TURNOS' para Excel (oculta) -->
            <table id="table_turnos" cellspacing="0" width="100%" style="display: none;">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Fecha</th>
                        <th>Forma Pago</th>                        
                        <th>Estado</th>
                        <th>Cliente</th>                            
                        <th>Local</th>                                           
                        <th>Servicio</th>                                           
                        <th>Item</th>                                           
                        <th>Precio</th>                                           
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>    
            </table>  

            <div class="clearfix"></div>     
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Fecha</th>
                        <th>Forma Pago</th>                        
                        <th>Estado</th>
                        <th>Cliente</th>                            
                        <th>Local</th>                                           
                        <th>Total</th>                                                  
                        <th style="width:70px;">Detalle</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="width:70px;"></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>

        </div>   
    </div>         
</div>


<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 
    
     
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 6 ], 
            "className": "text-right", 
        },       
        { 
            "targets": [ -1 ], 
            "orderable": false, //set not orderable
        },          
        {
            "targets": [2,3,4,5,6], 
            "className": "hidden-xs", 
        }
        ],


        // Suma total del precio de cada venta
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column(6)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column(6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 

            // Update footer
            if (pageTotal == total)
                $( api.column(6).footer() ).html('<b>$ '+ total + '</b>');            
            else
                $( api.column(6).footer() ).html(' $'+pageTotal +' ( $ '+ total +' total)');
        },

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // Ddrop-down lists de busqueda
                if (column.index() == 0){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 100px;"><option value="">TIPO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(0).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(0).search( val ? val : '', true, false ).draw();
                        } );
     
                        select.append( '<option value="venta">venta</option>' );
                        select.append( '<option value="turno">turno</option>' );
                } 

                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 180px"><option value="">MES / AÑO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(1).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(1).search( val ? val : '', true, false ).draw();                                
                        } );
     
                    <?php foreach($fechas as $fecha){ ?>
                        select.append( '<option value="<?php echo $fecha->mes_anio ?>"><?php echo $fecha->fecha ?></option>' )
                    <?php } ?>    
                }

                if (column.index() == 2){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 424px; width: 140px;"><option value="">LOCAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                                $('#table_ventas').DataTable().columns(2).search( val ? val : '', true, false ).draw();
                                $('#table_turnos').DataTable().columns(2).search( val ? val : '', true, false ).draw();

                        } );
     
                    <?php foreach($locales as $local){ ?>
                        select.append( '<option value="<?php echo $local->nombre ?>"><?php echo $local->nombre ?></option>' )
                    <?php } ?>    
                }  

                if (column.index() == 3){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 288px; width: 130px; "><option value="">CLIENTE</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(3).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(3).search( val ? val : '', true, false ).draw();
                        } );
     
                    <?php foreach($clientes as $cliente){ ?>
                        select.append( '<option value="<?php echo $cliente->nombre ?>"><?php echo $cliente->nombre ?></option>' )
                    <?php } ?>    
                } 

                if (column.index() == 4){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 694px;"><option value="">ESTADO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(4).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(4).search( val ? val : '', true, false ).draw();
                        } );
     
                        select.append( '<option value="pendiente">pendiente</option>' );                        
                        select.append( '<option value="concretado">concretado</option>' );
                        select.append( '<option value="no concretado">no concretado</option>' );                        
                        select.append( '<option value="cancelado">cancelado</option>' );
                }       

                if (column.index() == 5){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 570px; width: 118px;"><option value="">FORMA PAGO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                            $('#table_ventas').DataTable().columns(5).search( val ? val : '', true, false ).draw();
                            $('#table_turnos').DataTable().columns(5).search( val ? val : '', true, false ).draw();
                        } );
     
                        <?php foreach($medios_pago as $medio_pago){ ?>
                            select.append( '<option value="<?php echo $medio_pago->nombre ?>"><?php echo $medio_pago->nombre ?></option>' )
                        <?php } ?>
                }                                                             

            } );

        }, 

    });


    // Tabla 'VENTAS' para Excel 
    table_ventas = $('#table_ventas').DataTable({ 
    
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "paging":   false,
        "ordering": false,
        "info":     false,        

        "dom": 'Bfrtip',
        "buttons": ['excel'],

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_excel_ventas",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 6 ], 
            "className": "text-right", 
        },       
        ],

    });


    // Tabla 'TURNOS' para Excel 
    table_turnos = $('#table_turnos').DataTable({ 
    
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "pageLength": 25,
        "paging":   false,
        "ordering": false,
        "info":     false,        

        "dom": 'Bfrtip',
        "buttons": ['excel'],

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_excel_turnos",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 6 ], 
            "className": "text-right", 
        },       
        ],

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


</script>