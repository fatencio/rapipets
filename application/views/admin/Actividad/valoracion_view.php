<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h2 class="page-heading">
                Comentarios pendientes de aprobación
            </h2>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Valoracion/index');">Comentarios Pendientes</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Pedido</th>                    
                        <th>Cliente</th>                               
                        <th>Local</th>                            
                        <th>Puntaje</th>     
                        <th>Comentario</th>     
                        <th>Acción</th>                                                       
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>  
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Carga valoraciones pendientes de aprobacion
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Valoracion/ajax_list_pendientes",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1], //last column
            "orderable": false, //set not orderable 
            "width": "140px",     
        },
        ],

    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function aprobar_comentario(id, tipo, aprobada){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Valoracion/ajax_aprobar_comentario/" + id + "/" + tipo + "/" + aprobada,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (aprobada == 'si')
                aviso('success', 'Comentario aprobado.');  
            else
                aviso('success', 'Comentario rechazado.');  

            reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al aprobar comentario. '); 
        }
    });

}

</script>
 