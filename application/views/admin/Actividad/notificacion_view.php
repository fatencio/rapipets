<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Notificaciones
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Notificacion/index');">Notificaciones</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Fecha</th>
                        <th>Tipo</th>                         
                        <th>Notificación</th>
                        <th style="width:70px;">Detalles</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th style="width:70px;"></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Notificacion/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable    
        },
        {
            "targets": [ 2 ], 
            "className": "hidden-xs"             
        }
        ],

        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                // Habilita drop-down list de busqueda solo en segunda columna
                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 100px"><option value="">TIPO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    select.append( '<option value="venta">Venta</option>' );
                    select.append( '<option value="aviso venta">Aviso venta</option>' );
                    select.append( '<option value="calificación venta">Calificación venta</option>' );
                    select.append( '<option value="mensaje venta">Mensaje venta</option>' );
                    select.append( '<option value="cancela venta">Cancelación venta</option>' );
                    select.append( '<option value="turno">Turno</option>' );
                    select.append( '<option value="aviso turno">Aviso turno</option>' );
                    select.append( '<option value="calificación turno">Calificación turno</option>' );
                    select.append( '<option value="mensaje turno">Mensaje turno</option>' );
                    select.append( '<option value="cancela turno">Cancelación turno</option>' );
                    select.append( '<option value="mascota asignada">Mascota asignada</option>' );
                    select.append( '<option value="registro">Registro</option>' );
                    select.append( '<option value="adhesión">Adhesión</option>' );
                    select.append( '<option value="consulta">Consulta</option>' );
                    select.append( '<option value="destacar">Solicitud Destacar</option>' );

                }
            } );
        },  

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


</script>