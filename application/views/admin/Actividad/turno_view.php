<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Turnos
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Turno/index');">Turnos</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Fecha</th>                        
                        <th>Servicio</th>                        
                        <th>Total</th>
                        <th>Local</th>                        
                        <th>Cliente</th>                         
                        <th style="width:70px;">Detalles</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>                         
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="width:70px;"></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 
    
     
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Turno/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            // Ultima columna sin opcion de ordenaroy
            "targets": [ -1 ], //last column
            "orderable": false, 
           
        },
        { 
            // Columnas ocultas en versión mobile
            "targets": [ 2, 3, 4, 5 ], 
            "className": "hidden-xs",  
        },     
        ],

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // Drop-down lists de busqueda
                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 100px;"><option value="">SERVICIO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($servicios as $servicio){ ?>
                        select.append( '<option value="<?php echo $servicio->nombre ?>"><?php echo $servicio->nombre ?></option>' )
                    <?php } ?>  
                } 

                /*
                if (column.index() == 2){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 100px"><option value="">MES / AÑO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($fechas as $fecha){ ?>
                        select.append( '<option value="<?php echo $fecha->mes_anio ?>"><?php echo $fecha->fecha ?></option>' )
                    <?php } ?>    
                }
                */  

                if (column.index() == 3){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 220px; width: 140px;"><option value="">CLIENTE</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($clientes as $cliente){ ?>
                        select.append( '<option value="<?php echo $cliente->nombre ?>"><?php echo $cliente->nombre ?></option>' )
                    <?php } ?>    
                } 

                if (column.index() == 4){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 370px ; width: 140px;"><option value="">LOCAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($locales as $local){ ?>
                        select.append( '<option value="<?php echo $local->nombre ?>"><?php echo $local->nombre ?></option>' )
                    <?php } ?>    
                }                                                          

            } );
        }, 

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


</script>