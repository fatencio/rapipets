<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Recordatorios
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Recordatorio/index');">Recordatorios</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            Recordatorios de Calificación (Cliente y Local)
        </div>

        <div class="block-content push-10">
            <button class="btn btn-default" onclick="enviar_recordatorio_calificacion()" id="btn_recordatorio_calificaciones"> Enviar Recordatorios</button>
        </div>   
        <div class="block-content push-10">
            <div class="alert alert-info alert-dismoissable" id="div_fin_recordatorio_calificacion" style="display:none">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h3 class="font-w300 push-15">Proceso Finalizado</h3>
                <p><b id="cant_recordatorio_calificacion_cliente"></b> recordatorios enviados a Clientes</p>
                <p><b id="cant_recordatorio_calificacion_local"></b> recordatorios enviados a Locales</p>
            </div>            
        </div>
    </div>         
</div>

<script type="text/javascript">


$(document).ready(function() {



});


function enviar_recordatorio_calificacion(){

    $('#div_fin_recordatorio_calificacion').hide();    

    $('#btn_recordatorio_calificaciones').html('Enviando... <i class="fa fa-cog fa-spin"></i>');
    $('#btn_recordatorio_calificaciones').attr('disabled', true);  

    $.ajax({
      url : BASE_PATH + '/admin/Recordatorio/enviar_recordatorio_calificacion/',
      cache: false,
      success: function(data)
      {
          resultado = jQuery.parseJSON(data);

          $('#cant_recordatorio_calificacion_cliente').html(resultado.cant_recordatorio_cliente);
          $('#cant_recordatorio_calificacion_local').html(resultado.cant_recordatorio_local);
          $('#div_fin_recordatorio_calificacion').show();    

          $('#btn_recordatorio_calificaciones').html('Enviar Recordatorios');
          $('#btn_recordatorio_calificaciones').attr('disabled', false);  
          
      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        $('#modal_error_sistema').modal('show');

        $('#btn_recordatorio_calificaciones').html('Enviar Recordatorios');
        $('#btn_recordatorio_calificaciones').attr('disabled', false);  

        console.log('enviar_recordatorio_calificacion - Error al verificar recordatorios de mails de calificaciones. ' + textStatus);
      } 
    });

}

</script>