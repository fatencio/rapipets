<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Medios de Pago
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">

                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Mediopago/index');">Medios de Pago</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_medio_pago()"><i class="glyphicon glyphicon-plus"></i> Nuevo Medio de Pago</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Habilitado</th>
                        <th>Items</th>
                        <th>Imagen</th>   
                        <th style="width:70px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    $('[name="hd_items"]').val('0');

    $("#items").change(function() 
    {
        if(this.checked) {
            $('[name="hd_items"]').val(1); 
        }
        else {
            $('[name="hd_items"]').val(0); 
        }
    });    
});



function add_medio_pago()
{
    save_method = 'add';

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'medios_pago/miniaturas/noimage.gif' ?>');  
    $('#imagen').val('noimage.gif');

    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('#edit_nombre').show();
    $('#edit_items').show();
    $('#edit_imagen').show();
    $('#tiene_items').hide();
    $('#switch_items').removeClass('switch-warning').addClass('switch-info');
    $('#items').prop('disabled', false);

    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Medio de Pago'); // Set Title to Bootstrap modal title
}


function edit_medio_pago(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="imagen"]').val(data.imagen);  
           
            estado = (data.estado == '1' ? true : false);
            $('[name="estado"]').prop('checked', estado);

            items = (data.items == '1' ? true : false);     
            $('[name="items"]').prop('checked', items);   
            

            // Para Mercado Pago permite solamente habilitar / deshabilitar 
            data.id == '1' ? $('#edit_nombre').hide() : $('#edit_nombre').show();
            data.id == '1' ? $('#edit_items').hide() : $('#edit_items').show();
            data.id == '1' ? $('#edit_imagen').hide() : $('#edit_imagen').show();

            // Si tiene items asociados
            if (data.count_items > 0)
            {
                $('#items').prop('disabled', true);
                $('#switch_items').removeClass('switch-info').addClass('switch-warning');
                $('#tiene_items').show();
                $('[name="hd_items"]').val(1);
            } 
            else
            {
                 $('#items').prop('disabled', false);
                 $('#switch_items').removeClass('switch-warning').addClass('switch-info');
                 $('#tiene_items').hide();
                 $('[name="hd_items"]').val(data.items); 
            }
            
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Medio de Pago'); // Set title to Bootstrap modal title

            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'medios_pago/fotos/' ?>'  + data.imagen);   

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
             aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_add";
        mensaje = 'Medio de Pago creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_update";
        mensaje = 'Medio de Pago modificado.';
    }

    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,                
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                 aviso('success', mensaje); 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Medio de Pago (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_medio_pago(nombre, id)
{
    if(confirm('¿Eliminar Medio de Pago "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {

                 $('#modal_form').modal('hide');
                 reload_table();
                  aviso('success', 'Medio de Pago eliminado.');  
              
                }
                else 
                {
                    aviso('danger', data.mensaje, "ERROR AL ELIMINAR MEDIO DE PAGO");  
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
            }
        });

    }
}


function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'medios_pago/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Mediopago/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Medios de Pago Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group" id="edit_nombre">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- HABILITADO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Habilitado</label>
                            <div class="col-md-9">
                                <label class="css-input switch switch-info">
                                    <input type="checkbox" name="estado"><span></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- ITEMS --> 
                        <div class="form-group" id="edit_items">
                            <input type="hidden" value="" name="hd_items"/> 
                            <label class="control-label col-md-3">Admite Items</label>
                            <div class="col-md-9">
                                <label class="css-input switch switch-info pull-left" id="switch_items">
                                    <input type="checkbox" name="items" id="items"><span></span>
                                </label>
                                <span class="text-warning pull-left push-10-l push-5-t" id="tiene_items">Tiene items asociados</span>
                            </div>
                        </div>

                        <!-- IMAGEN --> 
                        <div class="form-group" id="edit_imagen">
                            <label class="control-label col-md-2">Imagen</label>
                            <div class="col-md-10">
                                <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                <div class="clearfix"></div>  
                                <input type="hidden" value="noimage.gif" id="imagen" name="imagen" />
                                <span class="help-block"></span>
                                
                                <div class="clearfix"></div>    
                                <br/>                                                       
                                <img class="centrado" src="<?php echo IMG_PATH . 'medios_pago/miniaturas/noimage.gif' ?>" id="imagenPreview" /> 
                                

                            </div>
                        </div>                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/medios_pago/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn" id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->