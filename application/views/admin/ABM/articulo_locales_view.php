<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Artículos Locales
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Articulo/index_locales');">Artículos Locales</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Local</th>
                        <th>Rubro</th>
                        <th>Animal</th>                        
                        <th>Marca</th>
                        <th>Nombre</th>
                        <th>Imagen</th>                        
                        <th style="width:70px;">Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>                        
                        <th style="width:70px;"></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>


<script type="text/javascript">

var table;
var animal_conraza = false;
var animal_contamanio = false;
var rubro_conraza = false;
var rubro_contamanio = false;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Articulo/ajax_list_locales",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 5, -1 ], //last column
            "orderable": false, //set not orderable   
        },     
        {
            "className": "hidden-xs", "targets": [2,3],         
        }  
        ],

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // FILTROS
                // Local
                if (column.index() == 0){
                    var select = $('<select id="ddl_local_productos" name="ddl_local_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 100px"><option value="">LOCAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );

                    <?php foreach($locales as $local){ ?>
                        select.append( '<option value="<?php echo $local->nombre ?>"><?php echo $local->nombre ?></option>' );
                    <?php } ?>                    
                }

                // Rubro
                if (column.index() == 1){
                    var select = $('<select id="ddl_rubro_productos" name="ddl_rubro_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 225px"><option value="">RUBRO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );

                    <?php foreach($rubros as $rubro){ ?>
                        select.append( '<option value="<?php echo $rubro->nombre ?>"><?php echo $rubro->nombre ?></option>' );
                    <?php } ?>                    
                }


                // Animal
                if (column.index() == 2){
                    var select = $('<select id="ddl_animal_productos" name="ddl_animal_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 345px"><option value="">ANIMAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($animales as $animal){ ?>
                        select.append( '<option value="<?php echo $animal->nombre ?>"><?php echo $animal->nombre ?></option>' );
                    <?php } ?>    
                }

                // Marca
                if (column.index() == 3){
                    var select = $('<select id="ddl_marca_productos" name="ddl_marca_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 450px"><option value="">MARCA</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            /*
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            */
                            // No hace 'escape' del valor para permitir buscar por marcas con '-' (Ken-L por ejemplo)
                            var val = $(this).val();
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($marcas_select as $marca){ ?>
                        select.append( "<option class='<?php echo $marca->rubro_animal; ?>' value='<?php echo $marca->nombre ?>'><?php echo $marca->nombre ?></option>" );
                    <?php } ?>    

                    $("#ddl_marca_productos").chained("#ddl_rubro_productos, #ddl_animal_productos");
                }

                
            } );
        }, 

    });

    ocultaSeccionesRubro();
    ocultaSeccionesAnimal();

    $("#raza").chained("#animal");
    $("#tamanio").chained("#animal");

    $("#marca").chained("#rubro, #animal");     

});


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


function ocultaSeccionesAnimal()
{
    $('[id="divRaza"]').hide(); 
    $('[id="divTamanio"]').hide(); 
}


function cambioAnimal(id)
{
    // Inicialmente oculta todas las secciones que dependen del Animal seleccionado
    ocultaSeccionesAnimal();

    // Muestra las secciones habilitadas para el Animal seleccionado
    if (typeof id !== "undefined")
    {
        if (id != "")
        {
            $.ajax(
            {
                url : "<?php echo BASE_PATH ?>/admin/Animal/ajax_edit/" + id,        
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    // Raza
                    conraza = (data.conraza == '1' ? true : false);
                    if (conraza && rubro_conraza) $('[id="divRaza"]').show();

                    animal_conraza = conraza;

                    // Tamaño
                    contamanios = (data.contamanios == '1' ? true : false);
                    if (contamanios && rubro_contamanio) $('[id="divTamanio"]').show();   

                    animal_contamanio = contamanios;
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                    
                }
            });            
        }
        
    }
}


function ocultaSeccionesRubro()
{
    $('[id="divRaza"]').hide(); 
    $('[id="divTamanio"]').hide(); 
    $('[id="divPresentacion"]').hide(); 
    $('[id="divMarca"]').hide(); 
    $('[id="divEdad"]').hide(); 
    $('[id="divMedicados"]').hide(); 

    $("#con_marca").val("0");
    $("#con_presentacion").val("0");
}


function cambioRubro(id)
{
    // Inicialmente oculta todas las secciones que dependen del Rubro seleccionado
    ocultaSeccionesRubro();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (typeof id !== "undefined")
    {
        if (id != "")
        {
            $.ajax({
                url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_edit/" + id,        
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    // Raza
                    conraza = (data.conraza == '1' ? true : false);
                    if (conraza && animal_conraza) $('[id="divRaza"]').show();

                    rubro_conraza = conraza;

                    // Tamaño
                    contamanios = (data.contamanios == '1' ? true : false);
                    if (contamanios && animal_contamanio) $('[id="divTamanio"]').show();

                    rubro_contamanio = contamanios;

                    // Presentación
                    conpresentacion = (data.conpresentacion == '1' ? true : false);

                    if (conpresentacion)
                    {
                        $('[id="divPresentacion"]').show();     
                        $("#con_presentacion").val("1");       
                    } 
                    
                    // Marca
                    conmarca = (data.conmarca == '1' ? true : false);

                    if (conmarca)
                    {
                        $('[id="divMarca"]').show();
                        $("#con_marca").val("1");
                    }

                    // Edad
                    conedad = (data.conedad == '1' ? true : false);
                    if (conedad) $('[id="divEdad"]').show();

                    // Medicados
                    conmedicados = (data.conmedicados == '1' ? true : false);
                    if (conmedicados) $('[id="divMedicados"]').show();    

                    cambioAnimal($('[id="animal"]').val());
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                    
                }
            });
        }
    }
}


function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}


function ver_articulo_local(articulo_id, local_id)
{
    var marca_rubro = '';
    var presentacion = '';
    var sin_stock = '';
    var html_presentaciones = '';
    var html_presentaciones_inactivas = '';

    $.ajax({
      url : "<?php echo BASE_PATH ?>/local/get_articulo_by_id/" + articulo_id + '/' + local_id,
      cache: false,
      success: function(data){

        articulo = jQuery.parseJSON(data);

        // Si el artículo no tiene marca, muestra el rubro en su lugar
        if (articulo.marca === null) 
            marca_rubro = articulo.rubro;
        else 
            marca_rubro = articulo.marca;

        $("#modal_articulo_marca").html(marca_rubro);
        $("#modal_articulo_nombre").html(articulo.nombre);

        // Productos creados por el Local
        if(articulo.tipo == 'Local')
          $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/locales/fotos/' +  articulo.imagen + '" alt="">');
        // Productos Geotienda
        else
          $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/fotos/' +  articulo.imagen + '" alt="">');

        $("#modal_articulo_detalle").html(articulo.detalle);


        // Presentaciones del artículo
        for (j = 0; j < articulo.presentacion.length; ++j) 
        {
            tachado = '';
            descuento_oculto = '';

            presentacion = articulo.presentacion[j].nombre;

            // Presentación con Descuento
            if (articulo.presentacion[j].precio != articulo.presentacion[j].precio_descuento){

                tachado = 'font-w300 tachado text-muted';
                ocultar_circulo = '';       
            }
            // Presentación sin Descuento
            else{

                tachado = 'font-w600 text-success';
                descuento_oculto = 'oculto';
                ocultar_circulo = 'style="display:none"';       
            }   

            // Presentación con Stock
            if (articulo.presentacion[j].stock == '1'){
     
                sin_stock = 'style="display:none"';       
            }
            // Presentación sin Stock
            else{
    
                sin_stock = '';      
            }    

            // Presentaciones activas
            if (articulo.presentacion[j].estado == '1')
            {
              html_presentaciones += 
                '<div class="h5 font-w600 text-muted pull-left">' + presentacion +'</div>' +

                // Stock
                '<div class="pull-right push-10-l push--5-t" ' + sin_stock + '>' + 
                '<div class="push-5-t" style="color: red;">SIN STOCK</div>' + 
                '</div>' + 

                '<div class="h5 pull-right push-10-l ' + tachado + '">$' +  articulo.presentacion[j].precio +'</div>' + 
                '<br />' + 
                '<div class="h5 font-w600 text-success pull-right push-10-l ' + descuento_oculto + '">' + 
                '<div class="circulo_descuento_mini"' + ocultar_circulo + '><span>-'+ articulo.presentacion[j].descuento +'%</span></div>' +                
                '&nbsp;&nbsp;$' +  articulo.presentacion[j].precio_descuento +'</div>' + 
                '<div style="height:10px; clear: both;"></div>';
            }
            // Presentaciones inactivas
            else
            {
              html_presentaciones_inactivas += 
                '<div class="h5 font-w600 text-muted pull-left">' + presentacion +'</div>' +

                // Stock
                '<div class="pull-right push-10-l push--5-t" ' + sin_stock + '>' + 
                '<div class="push-5-t" style="color: red;">SIN STOCK</div>' + 
                '</div>' + 

                '<div class="h5 pull-right push-10-l ' + tachado + '">$' +  articulo.presentacion[j].precio +'</div>' + 
                '<br />' + 
                '<div class="h5 font-w600 text-success pull-right push-10-l ' + descuento_oculto + '">' + 
                '<div class="circulo_descuento_mini"' + ocultar_circulo + '><span>-'+ articulo.presentacion[j].descuento +'%</span></div>' +                
                '&nbsp;&nbsp;$' +  articulo.presentacion[j].precio_descuento +'</div>' + 
                '<div style="height:10px; clear: both;"></div>';   
            }             
        }

        if(html_presentaciones == '') html_presentaciones = '<div class="h5 font-w600 text-muted pull-left push-20">No hay presentaciones activas</div>';

        $("#modal_articulo_presentaciones").html(html_presentaciones);
        $("#modal_articulo_presentaciones_inactivas").html(html_presentaciones_inactivas);

        $('#modal_detalles_articulo').modal('show'); 
      } 
    });
}
</script>


<!-- MODAL DETALLES ARTICULO -->
<div class="modal fade" id="modal_detalles_articulo" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <!-- Nombre y Marca -->
                            <span class="h5">
                                <span class="font-w600 text-muted" id="modal_articulo_marca"></span>
                                <br>
                                <p class="h4 font-w600" id="modal_articulo_nombre"></p>
                            </span>
                            <!-- Nombre, Marca y Precio -->                         
                        </div>
                        <div class="col-sm-6">
                            <!-- Imagen -->
                            <div class="row" style="position: relative">
                                <div class="col-xs-12 push-10 push-10 img-container">
                                    <div class="div-img-articulos-detalle" id="modal_articulo_imagen">
                                    </div>
                                </div>
                            </div>
                            <!-- FIN Imagen -->
                        </div>
                        <div class="col-sm-6" >
                            
                            <!-- Detalles -->                            
                            <div  id="modal_articulo_detalle">
                            </div>      
                       
                            <!-- Presentaciones -->  
                          <div class="h5 font-w600 push-5 push-5-t">Presentaciones Activas</div>                          
                          <div  id="modal_articulo_presentaciones"></div>                      

                          <div class="h5 font-w600 push-5 push-5-t">Presentaciones Inactivas</div>                          
                          <div  id="modal_articulo_presentaciones_inactivas"></div>  

                        </div>

                    </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-info" type="button" data-dismiss="modal"> Aceptar</button>
                        </div>  
                        <br/>                   
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL DETALLES ARTICULO -->  









