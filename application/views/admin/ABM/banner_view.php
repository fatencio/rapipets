<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Banners
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Banner/index');">Banners</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_banner()"><i class="glyphicon glyphicon-plus"></i> Nuevo Banner</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Vista Previa</th>
                        <th style="width:70px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>   

        <!--
        <div class="block-header">
            <h4 class="push-5">Nivel activo</h4>        
            <button class="btn btn-success" id="btn_nivel_global" onclick="set_nivel_banner(0)">Global</button>
            <button class="btn btn-default" id="btn_nivel_grupo" onclick="set_nivel_banner(1)">Grupo</button>
            <button class="btn btn-default" id="btn_nivel_local" onclick="set_nivel_banner(2)">Local</button>
        </div>        
        -->
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Banner/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

    $("input").focusout(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    set_colorpicker();
  
});


function set_colorpicker(){

    // Colores banner
    // --------------

    // Color texto
    $('#color_texto').spectrum({   
        preferredFormat: "hex",
        showPaletteOnly: true,
        hideAfterPaletteSelect: true,        
        move: function(color) {
            cambiaColorTexto(color);
        },
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ]
    }); 

    // Color fondo
    $('#color_fondo').spectrum({   
        preferredFormat: "hex",
        showPaletteOnly: true,
        hideAfterPaletteSelect: true,        
        move: function(color) {
            cambiaColorFondo(color);
        },
        palette: [
            ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
            "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
            ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
            "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
            ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
            "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
            "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
            "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
            "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
            "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
            "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
            "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
            "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
            "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
        ]
    });    
}


function add_banner()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('[name="color_texto"]').val('white');
    $('[name="color_fondo"]').val('blue');
    $('#div_vista_previa').html(''); 
    $('#div_vista_previa').css('color', 'white');
    $('#div_vista_previa').css('background-color', 'blue');

    set_colorpicker();

    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Banner'); // Set Title to Bootstrap modal title
}

function edit_banner(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Banner/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="texto"]').val(data.texto);
            $('[name="color_texto"]').val(data.color);
            $('[name="color_fondo"]').val(data.fondo);
            $('#div_vista_previa').html(data.texto); 
            $('#div_vista_previa').css('color',  data.color);  
            $('#div_vista_previa').css('background-color',  data.fondo);  

            set_colorpicker();

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Banner'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
              aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Banner/ajax_add";
         mensaje = 'Banner creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/admin/Banner/ajax_update";
         mensaje = 'Banner modificado.';
    }


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje);  
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Banner (' + errorThrown + ')'); 
           
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


function delete_banner(nombre, id)
{
    if(confirm('¿Eliminar Banner "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Banner/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {


                $('#modal_form').modal('hide');
                reload_table();
                 aviso('success', 'Banner eliminado.');      
                }
                else {
                 aviso('danger', data.mensaje, "\nERROR AL ELIMINAR BANNER\n");  
               
                 console.log(data.error);

                 }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                   aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
            }
        });

    }
}

// Marca al banner seleccionado como 'global' (banner que aplica a todos los locales)
function switch_banner_global(id){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Banner/ajax_banner_global/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.global == 1)
                aviso('success', 'Banner global activado.');  
            else
                aviso('success', 'Banner global desactivado.');  

            reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            aviso('danger', textStatus, 'Error al asignar banner global.'); 
        }
    });

}


// Setea el nivel de los banners
// nivel 0: gobal (aplica a todos los locales)
// nivel 1: grupo (aplica a grupos de locales)
// nivel 2: local (cada local tiene su propio banner)
/*
function set_nivel_banner(nivel){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Banner/ajax_set_nivel_banner/" + nivel,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            switch(nivel){

                case 0:
                    aviso('success', 'Banner a nivel global activado.');  
                    $('#btn_nivel_global').attr('disabled',true); 
                    $("#btn_nivel_global").removeClass("btn-default").addClass("btn-success");

                    $('#btn_nivel_grupo').attr('disabled',false); 
                    $("#btn_nivel_grupo").removeClass("btn-success").addClass("btn-default");

                    $('#btn_nivel_local').attr('disabled',false); 
                    $("#btn_nivel_local").removeClass("btn-success").addClass("btn-default");
                    break;


                case 1:
                    aviso('success', 'Banner a nivel grupo activado.');  
                    $('#btn_nivel_global').attr('disabled',false); 
                    $("#btn_nivel_global").removeClass("btn-success").addClass("btn-default");

                    $('#btn_nivel_grupo').attr('disabled',true); 
                    $("#btn_nivel_grupo").removeClass("btn-default").addClass("btn-success");

                    $('#btn_nivel_local').attr('disabled',false);           
                    $("#btn_nivel_local").removeClass("btn-success").addClass("btn-default");     
                    break;

                case 2:
                    aviso('success', 'Banner a nivel local activado.');  
                    $('#btn_nivel_global').attr('disabled',false); 
                    $("#btn_nivel_global").removeClass("btn-success").addClass("btn-default");
                    
                    $('#btn_nivel_grupo').attr('disabled',false); 
                    $("#btn_nivel_grupo").removeClass("btn-success").addClass("btn-default");

                    $('#btn_nivel_local').attr('disabled',true);       
                    $("#btn_nivel_local").removeClass("btn-default").addClass("btn-success");                     
                    break;                    
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            aviso('danger', textStatus, 'Error al configurar el alcance del banner.'); 
        }
    });

}
*/

// Vista previa del banner
// -----------------------

// Texto
$('#texto').change(function() {
  $('#div_vista_previa').html($('#texto').val()); 
});


// Color texto
function cambiaColorTexto(color) {
    $('#div_vista_previa').css('color',  color.toHexString());    
}

// Color fondo
function cambiaColorFondo(color) {
    $('#div_vista_previa').css('background-color',  color.toHexString());    
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Banner Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- TEXTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Texto</label>
                            <div class="col-md-9">
                                <input id="texto" name="texto" placeholder="Texto" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- COLOR TEXTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Color Texto</label>
                            <div class="col-md-9">
                                <input id="color_texto" name="color_texto" class="form-control" type="text" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- COLOR FONDO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Color Fondo</label>
                            <div class="col-md-9">
                                <input id="color_fondo" name="color_fondo" class="form-control" type="text" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>   

                        <!-- VISTA PREVIA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Vista Previa</label>
                            <div class="col-md-9 vista-previa-banner text-center" id="div_vista_previa">
                            </div>
                        </div>                                                
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->