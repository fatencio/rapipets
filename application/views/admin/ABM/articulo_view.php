<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Artículos GeoTienda
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Articulo/index');">Artículos GeoTienda</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_articulo()"><i class="glyphicon glyphicon-plus"></i> Nuevo Artículo</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Rubro</th>
                        <th>Animal</th>                        
                        <th>Marca</th>
                        <th>Nombre</th>
                        <th>Código</th>
                        <th>Imagen</th>                        
                        <th style="width:70px;">Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>                        
                        <th style="width:70px;"></th>
                    </tr>
                </tfoot>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>


<script type="text/javascript">

var save_method; //for save method string
var table;
var animal_conraza = false;
var animal_contamanio = false;
var rubro_conraza = false;
var rubro_contamanio = false;

$(document).ready(function() {

    // ckeditor para textarea Detalle
    CKEDITOR.replace('detalle', {

        toolbarGroups: [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'colors', groups: [ 'colors' ] },            
            '/',            
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
        ],

        removeButtons: 'Save,Language,BidiRtl,BidiLtr,Strike,Superscript,Subscript,Blockquote,CreateDiv'
    });

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Articulo/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 5, -1 ], //last column
            "orderable": false, //set not orderable   
        },     
        {
            "className": "hidden-xs", "targets": [2,3],         
        }  
        ],

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // FILTROS
                // Rubro
                if (column.index() == 0){
                    var select = $('<select id="ddl_rubro_productos" name="ddl_rubro_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 100px"><option value="">RUBRO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );

                    <?php foreach($rubros as $rubro){ ?>
                        select.append( '<option value="<?php echo $rubro->nombre ?>"><?php echo $rubro->nombre ?></option>' );
                    <?php } ?>                    
                }


                // Animal
                if (column.index() == 1){
                    var select = $('<select id="ddl_animal_productos" name="ddl_animal_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 225px"><option value="">ANIMAL</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($animales as $animal){ ?>
                        select.append( '<option value="<?php echo $animal->nombre ?>"><?php echo $animal->nombre ?></option>' );
                    <?php } ?>    
                }

                // Marca
                if (column.index() == 2){
                    var select = $('<select id="ddl_marca_productos" name="ddl_marca_productos" class="form-control hidden-xs" style="position: absolute; top: -39px; left: 335px"><option value="">MARCA</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            /*
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            */
                            // No hace 'escape' del valor para permitir buscar por marcas con '-' (Ken-L por ejemplo)
                            var val = $(this).val();
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($marcas_select as $marca){ ?>
                        select.append( "<option class='<?php echo $marca->rubro_animal; ?>' value='<?php echo $marca->nombre ?>'><?php echo $marca->nombre ?></option>" );
                    <?php } ?>    

                    $("#ddl_marca_productos").chained("#ddl_rubro_productos, #ddl_animal_productos");
                }

                
            } );
        }, 

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

    ocultaSeccionesRubro();
    ocultaSeccionesAnimal();

    $("#raza").chained("#animal");
    $("#tamanio").chained("#animal");
    $("#marca").chained("#rubro, #animal");     
    $("#presentacion").chained("#rubro");
});



function add_articulo()
{
    save_method = 'add';

    // Reset del formulario
    ocultaSeccionesRubro();
    ocultaSeccionesAnimal();

    CKEDITOR.instances['detalle'].setData('');

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>');  
    $('#imagen').val('noimage.gif');
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Artículo'); // Set Title to Bootstrap modal title
}


function edit_articulo(id)
{
    save_method = 'update';

    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Articulo/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {   
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="codigo"]').val(data.codigo);
            $('[name="detalle"]').val(data.detalle);
            $('[name="rubro"]').val(data.id_rubro);
            $('[name="animal"]').val();
            $('[name="animal"]').val(data.id_animal).change();
            $('[name="marca"]').val(data.id_marca);                                 
            $('[name="edad"]').val(data.edad);
            $('[name="imagen"]').val(data.imagen);  

            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'articulos/fotos/' ?>'  + data.imagen);      

            CKEDITOR.instances.detalle.setData($('[name="detalle"]').val());

            medicados = (data.medicados == '1' ? true : false);

            $('#medicados_null').prop('checked', true);   

            if (data.medicados == '1') $('#medicados_si').prop('checked', true);    
            if (data.medicados == '0') $('#medicados_no').prop('checked', true);    

            $('[name="rubro"]').change();

            $.each(data.presentaciones_view.split(","), function(i,e){
                $("#presentacion option[value='" + e + "']").prop("selected", true);
            });      

            $.each(data.razas_view.split(","), function(i,e){
                $("#raza option[value='" + e + "']").prop("selected", true);
            });    

            $.each(data.tamanios_view.split(","), function(i,e){
                $("#tamanio option[value='" + e + "']").prop("selected", true);
            });   

            cambioRubro(data.id_rubro);
            cambioAnimal(data.id_animal);


            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Artículo'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS (' + errorThrown + ')"); 
        }
    });

}


function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


function save()
{
    var url;
    var mensaje;

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }

    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Articulo/ajax_add";
        mensaje = 'Artículo creado.';

    } else {
        url = "<?php echo BASE_PATH ?>/admin/Articulo/ajax_update";
        mensaje = 'Artículo modificado.';

    }

    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,        
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();

                aviso('success', mensaje);  

                // Avisa que hay presentaciones que no se eliminaron porque están siendo utilizadas
                if (save_method == 'update' && data.aviso_borrar_presentaciones != '')
                {
                    aviso('danger', 'Hay presentaciones que no se quitaron porque están siendo utilizadas en algún local.');                      
                }
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error al crear o modificar Artículo (' + errorThrown + ')');
            //aviso('danger', textStatus, 'Error al crear o modificar Artículo (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


function delete_articulo(nombre, id)
{
    if(confirm('¿Eliminar Artículo "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Articulo/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                     aviso('success', 'Artículo eliminado.');        
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL ELIMINAR ARTICULO");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
            }
        });

    }
}


function ocultaSeccionesAnimal()
{
    $('[id="divRaza"]').hide(); 
    $('[id="divTamanio"]').hide(); 
}


function cambioAnimal(id)
{
    // Inicialmente oculta todas las secciones que dependen del Animal seleccionado
    ocultaSeccionesAnimal();

    // Muestra las secciones habilitadas para el Animal seleccionado
    if (typeof id !== "undefined")
    {
        if (id != "")
        {
            $.ajax(
            {
                url : "<?php echo BASE_PATH ?>/admin/Animal/ajax_edit/" + id,        
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    // Raza
                    conraza = (data.conraza == '1' ? true : false);
                    if (conraza && rubro_conraza) $('[id="divRaza"]').show();

                    animal_conraza = conraza;

                    // Tamaño
                    contamanios = (data.contamanios == '1' ? true : false);
                    if (contamanios && rubro_contamanio) $('[id="divTamanio"]').show();   

                    animal_contamanio = contamanios;
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                    
                }
            });            
        }
        
    }
}


function ocultaSeccionesRubro()
{
    $('[id="divRaza"]').hide(); 
    $('[id="divTamanio"]').hide(); 
    $('[id="divPresentacion"]').hide(); 
    $('[id="divMarca"]').hide(); 
    $('[id="divEdad"]').hide(); 
    $('[id="divMedicados"]').hide(); 

    $("#con_marca").val("0");
    $("#con_presentacion").val("0");
}


function cambioRubro(id)
{
    // Inicialmente oculta todas las secciones que dependen del Rubro seleccionado
    ocultaSeccionesRubro();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (typeof id !== "undefined")
    {
        if (id != "")
        {
            $.ajax({
                url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_edit/" + id,        
                type: "GET",
                dataType: "JSON",
                success: function(data)
                {
                    // Raza
                    conraza = (data.conraza == '1' ? true : false);
                    if (conraza && animal_conraza) $('[id="divRaza"]').show();

                    rubro_conraza = conraza;

                    // Tamaño
                    contamanios = (data.contamanios == '1' ? true : false);
                    if (contamanios && animal_contamanio) $('[id="divTamanio"]').show();

                    rubro_contamanio = contamanios;

                    // Presentación
                    conpresentacion = (data.conpresentacion == '1' ? true : false);

                    if (conpresentacion)
                    {
                        $('[id="divPresentacion"]').show();     
                        $("#con_presentacion").val("1");       
                    } 
                    
                    // Marca
                    conmarca = (data.conmarca == '1' ? true : false);

                    if (conmarca)
                    {
                        $('[id="divMarca"]').show();
                        $("#con_marca").val("1");
                    }

                    // Edad
                    conedad = (data.conedad == '1' ? true : false);
                    if (conedad) $('[id="divEdad"]').show();

                    // Medicados
                    conmedicados = (data.conmedicados == '1' ? true : false);
                    if (conmedicados) $('[id="divMedicados"]').show();    

                    cambioAnimal($('[id="animal"]').val());
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                    
                }
            });
        }
    }
}


function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'articulos/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Articulo/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}


function sleep(miliseconds) {
   var currentTime = new Date().getTime();

   while (currentTime + miliseconds >= new Date().getTime()) {
   }
}
</script>



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Articulo Form</h3>
            </div>
            <div class="block-content modal-body"> 
                <div class="form">
                    <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                        <input type="hidden" value="" name="id"/> 
                        <input type="hidden" value="0" id="con_marca" name="con_marca" /> 
                        <input type="hidden" value="0" id="con_presentacion" name="con_presentacion" /> 
                        <div class="form-body">

                            <div class="col-md-1">
                            </div>
                            <div class="col-md-11">

                                <div class="col-md-4">
                                     <!-- CODIGO --> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Código</label>
                                        <div class="col-md-9">
                                            <input name="codigo" placeholder="Código" class="form-control" type="text">
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <!-- RUBRO --> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Rubro</label>
                                        <div class="col-md-9">
                                            <select id="rubro" name="rubro" class="form-control" onchange="cambioRubro(this.value)">                       
                                                <option selected value="">Seleccione...</option>
                                               
                                                <?php foreach($rubros as $rubro){ ?>
                                                    <option value="<?php echo $rubro->id ?>"><?php echo $rubro->nombre ?></option> 
                                                <?php } ?>
                                            </select>  
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <!-- ANIMAL  --> 
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Animal</label>
                                        <div class="col-md-9">
                                            <select id="animal" name="animal" class="form-control" onchange="cambioAnimal(this.value)">                       
                                                <option selected value="">Seleccione...</option>
                                               
                                                <?php foreach($animales as $animal){ ?>
                                                    <option value="<?php echo $animal->id ?>"><?php echo $animal->nombre ?></option> 
                                                <?php } ?>
                                            </select>  
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>

                             </div>

                            <!-- NOMBRE --> 
                            <div class="form-group">
                                <label class="control-label col-md-2">Nombre</label>
                                <div class="col-md-10">
                                    <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                             <!-- DETALLE --> 
                            <div class="form-group">
                                <label class="control-label col-md-2">Detalle</label>
                                <div class="col-md-10">
                                    <textarea id="detalle" name="detalle" ></textarea>       
                                    <input name="hdDetalle" placeholder="hdDetalle" class="form-control" type="text" style="display:none">                
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <!-- IMAGEN --> 
                            <div class="form-group">
                                <label class="control-label col-md-2">Imagen</label>
                                <div class="col-md-10">
                                    <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                    <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                    <div class="clearfix"></div>  
                                    <input type="hidden" value="noimage.gif" id="imagen" name="imagen" />
                                    <span class="help-block"></span>
                                    
                                    <div class="clearfix"></div>    
                                    <br/>                                                       
                                    <img class="centrado" src="<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>" id="imagenPreview" /> 
                                    

                                </div>
                            </div>

                            <!-- RAZAS --> 
                            <div class="form-group" id="divRaza">
                                <label class="control-label col-md-3">Raza</label>
                                <div class="col-md-9">
                                    <select name="raza[]" id="raza" class="form-control" multiple size="4">
                                        <?php foreach($razas as $raza){ ?>
                                            <option value="<?php echo $raza->id ?>" class="<?php echo $raza->animal_id ?>"><?php echo $raza->nombre ?></option> 
                                        <?php } ?>    
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>    

                               <!-- TAMAÑOS --> 
                            <div class="form-group" id="divTamanio">
                                <label class="control-label col-md-3">Tamaño</label>
                                <div class="col-md-9">
                                    <select name="tamanio[]" id="tamanio" class="form-control" multiple size="4">       
                                        <?php foreach($tamanios as $tamanio){ ?>
                                            <option value="<?php echo $tamanio->id ?>" class="<?php echo $tamanio->animal_id ?>"><?php echo $tamanio->nombre ?></option> 
                                        <?php } ?>    
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>    
                            
                            <!-- MEDICADOS --> 
                            <div class="form-group" id="divMedicados">
                                <label class="control-label col-md-3">Medicados</label>
                                <div class="col-md-9">
                                        <label class="push-10-r"><input name="medicados" type="radio" id="medicados_si" value="1" /> Si</label> 
                                        <label class="push-10-r"><input name="medicados" type="radio" id="medicados_no" value="0" /> No</label>
                                        <label><input name="medicados" type="radio" id="medicados_null" value="" checked="checked" /> No definido</label>
                                    <span class="help-block"></span>
                                </div>
                            </div>   

                            <!-- EDAD --> 
                            <div class="form-group" id="divEdad">
                                <label class="control-label col-md-3">Edad</label>
                                <div class="col-md-9">
                                    <select name="edad" class="form-control">                       
                                        <option selected value="">Seleccione...</option>
                                        <option  value="Cachorros">Cachorros</option>
                                        <option  value="Adultos">Adultos</option>
                                        <option  value="Senior">Senior</option>
                                        <option  value="Todas">Todas las Edades</option>                                    
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>    

                            <!-- MARCA  --> 
                            <div class="form-group" id="divMarca">
                                <label class="control-label col-md-3">Marca</label>
                                <div class="col-md-9">
                                    <select name="marca" class="form-control" id="marca" >                       
                                        <option selected value="">Seleccione...</option>
                                        <?php foreach($marcas as $marca){ ?>
                                            <option value="<?php echo $marca->id ?>" class="<?php echo $marca->rubro_animal ?>"><?php echo $marca->nombre ?></option> 
                                        <?php } ?>
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>        


                            <!-- PRESENTACIONES --> 
                            <div class="form-group"  id="divPresentacion">
                                <label class="control-label col-md-3">Presentaciones</label>
                                <div class="col-md-9">
                                    <select name="presentacion[]" id="presentacion" class="form-control" multiple size="10">
                                        <?php foreach($presentaciones as $presentacion){ ?>
                                              <option value="<?php echo $presentacion->id ?>" class="<?php echo $presentacion->rubro_id ?>"><?php echo $presentacion->nombre ?></option> 
                                        <?php } ?>
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>                                                                                 
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    


<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/articulos/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn" id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    









