<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Items Servicio
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Servicio_Item/index');">Items Servicio</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_servicio_item()"><i class="glyphicon glyphicon-plus"></i> Nuevo Item</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Servicio</th>                    
                        <th>Item</th>
                        <th>Animal</th>
                        <th style="width:180px;">Imagen</th>              
                        <th style="width:70px;">Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>                        
                        <th></th>                        
                        <th></th>                        
                        <th style="width:20px;"></th>
                    </tr>
                </tfoot>                  
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    // ckeditor para textarea Detalle
    CKEDITOR.replace('detalle', {

        toolbarGroups: [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'colors', groups: [ 'colors' ] },            
            '/',            
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
        ],

        removeButtons: 'Save,Language,BidiRtl,BidiLtr,Strike,Superscript,Subscript,Blockquote,CreateDiv'
    });

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Servicio_Item/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 3, -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // Habilita drop-down list de busqueda solo en primer columna
                if (column.index() == 0){
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 100px"><option value="">SERVICIO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () 
                        {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($servicios as $servicio){ ?>
                        select.append( '<option value="<?php echo $servicio->nombre ?>"><?php echo $servicio->nombre ?></option>' )
                    <?php } ?>   
                }
            } );
        },  

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_servicio_item()
{
    save_method = 'add';

    CKEDITOR.instances['detalle'].setData('');    

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'servicios/miniaturas/noimage.gif' ?>');  
    $('#imagen').val('noimage.gif');    
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Item'); // Set Title to Bootstrap modal title
}

function edit_servicio_item(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Servicio_Item/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="detalle"]').val(data.detalle);            
            $('[name="servicio"]').val(data.servicio_item_servicio_id);
            $('[name="animal"]').val(data.servicio_item_animal_id);
            $('[name="imagen"]').val(data.imagen);  

            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'servicios/fotos/' ?>'  + data.imagen);      

            CKEDITOR.instances.detalle.setData($('[name="detalle"]').val());                       

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Item'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
             aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false);
}

function save()
{
    var url;
    var mensaje;

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }
    
    $('#btnSave').attr('disabled',true); //set button disable    
    $('#btnSave').text('Guardando...'); //change button text

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Servicio_Item/ajax_add";
         mensaje = 'Item creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/admin/Servicio_Item/ajax_update";
         mensaje = 'Item modificado.';
    }

    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,          
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Item (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


function delete_servicio_item(nombre, id)
{
    if(confirm('¿Eliminar Item "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Servicio_Item/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Item eliminado.');                 
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL ELIMINAR ITEM");  
                    console.log(data.error);
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', data.mensaje, "ERROR AL ELIMINAR ITEM"); 
            }
        });

    }
}


function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'servicios/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Servicio_Item/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Item Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Nombre</label>
                            <div class="col-md-10">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <!-- SERVICIO --> 
                            <label class="control-label col-md-2">Servicio</label>
                            <div class="col-md-4">
                                <select name="servicio" class="form-control">                       
                                    <option selected value="">Seleccione...</option>

                                    <?php foreach($servicios as $servicio){ ?>
                                        <option value="<?php echo $servicio->id ?>"><?php echo $servicio->nombre ?></option> 
                                    <?php } ?>
                                </select>  
                                <span class="help-block"></span>
                            </div>

                            <!-- ANIMAL  --> 
                            <label class="control-label col-md-2">Animal</label>
                            <div class="col-md-4">
                                <select id="animal" name="animal" class="form-control">                       
                                    <option selected value="">Seleccione...</option>
                                   
                                    <?php foreach($animales as $animal){ ?>
                                        <option value="<?php echo $animal->id ?>"><?php echo $animal->nombre ?></option> 
                                    <?php } ?>
                                </select>  
                                <span class="help-block"></span>
                            </div>
                        </div>

                         <!-- DETALLE --> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Detalle</label>
                            <div class="col-md-10">
                                <textarea id="detalle" name="detalle" ></textarea>       
                                <input name="hdDetalle" placeholder="hdDetalle" class="form-control" type="text" style="display:none">                
                                <span class="help-block"></span>
                            </div>
                        </div>

                         <!-- IMAGEN --> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Imagen</label>
                            <div class="col-md-10">
                                <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                <div class="clearfix"></div>  
                                <input type="hidden" value="noimage.gif" id="imagen" name="imagen" />
                                <span class="help-block"></span>
                                
                                <div class="clearfix"></div>    
                                <br/>                                                       
                                <img class="centrado" src="<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>" id="imagenPreview" /> 
                                

                            </div>
                        </div>                       
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->


<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/servicios/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn" id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    