<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Locales
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Locales</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Local/index');">Locales</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_local()"><i class="glyphicon glyphicon-plus"></i> Nuevo Local</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th style="width:4px; padding-left:10px"><input type="checkbox" name="chk_all_locales" id="chk_all_locales" value=""></th>                    
                        <th>Categoría</th>
                        <th style="min-width:160px;">Nombre</th>
                        <th>Titular</th>
                        <th>Telefono</th>
                        <th>EMail</th>
                        <th>Avatar</th>
                        <th style="min-width:120px;">Acción</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>                        
                        <th style="width:20px;"></th>
                    </tr>
                </tfoot>                   
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>  

    <!-- BANNER GRUPAL --> 
    <div class="block">
        <div class="block-header">
            <div class="h4">Banner grupal</div>           
        </div>
        <div class="block-content">     
            <div class="row">
                <div class="col-md-6">    
                    <div class="form-group">
                        <div class="h5 push-5">Asignar banner a los locales seleccionados </div>   
                        <select name="ddl_banner_grupal" class="form-control" onchange="aplicar_banner_grupal(this)">                       
                            <option selected value="-1">Seleccione banner...</option>
                           
                            <?php foreach($banners as $banner){ ?>
                                <option value="<?php echo $banner->id ?>"><?php echo $banner->nombre ?></option> 
                            <?php } ?>
                    
                        </select>  
                    </div> 
                </div>

                <div class="col-md-6">    
                    <div class="form-group">
                        <div class="h5 push-5  pull-right">Quitar asignación de los locales seleccionados </div> 
                        <div class="clearfix"></div>  
                        <button class="btn btn-success  pull-right" onclick="quitar_banner_grupal()">Quitar asignación</button>
                    </div> 
                </div>                
            </div>
        </div>   
    </div>              
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;
var nivel_banner_activo_local = 'local'; // para un local en particular, puede ser 'local' / 'grupal' / 'global'
var nivel_banner_activo = 'grupal'; // para todos los locales, puede ser 'grupal' / 'global'

var table_articulos;
var local_activo;

$(document).ready(function() 
{

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Local/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, -1, -2 ], //last column
            "orderable": false, //set not orderable
        },     
        {
            "targets": [2,3], 
            "className": "hidden-xs", 
        },     
        {
            "targets": [4,5], 
            "className": "hidden-xs hidden-sm", 
        }                
        ],

        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                // Filtro por categoria
                if (column.index() == 0){
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 100px"><option value="">CATEGORÍA</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {

                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($categorias as $categoria){ ?>
                        select.append( '<option value="<?php echo $categoria->nombre ?>"><?php echo $categoria->nombre ?></option>' )
                    <?php } ?>   
                }

                // Filtro por destacado
                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 226px;"><option value="">DESTACADO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                        select.append( '<option value="1">si</option>' );
                        select.append( '<option value="0">no</option>' );                     
                }  


                // Precio destacado
                if (column.index() == 2)
                {
                    var div = $('<div style="padding: 6px !important; position: absolute; top: -66px; left: 355px;     font-weight: 300; font-size: 15px;" >Precio local destacado</div>').appendTo( $(column.footer()).empty());

                    var input = $('<input name="precio_local_destacado" id="precio_local_destacado" placeholder="Ingrese precio" class="form-control" style="padding: 6px !important; position: absolute; top: -39px; left: 360px; width: 60px;" type="number"></input>').appendTo( $(column.footer()));

                    var button = $('<button class="btn btn-success  pull-right" onclick="return actualizar_precio_destacado();" style="padding: 6px !important; position: absolute; top: -39px; left: 430px; width: 100px;">Guardar</button>').appendTo( $(column.footer()));        

                    var button = $('<span class="help-block" id="error_actualizar_destacado" style="padding: 6px !important; position: absolute; top: -39px; left: 530px; color: red;"></span>').appendTo( $(column.footer()));                    

                    get_precio_local_destacado();
                           
                }
            } );
        },  

    });

    get_nivel_banner_activo();

    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").focusout(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});


function add_local()
{
    save_method = 'add';

    // Valores por defecto
    $('#switch_envio_domicilio').prop('checked', false);
    $('[name="envio_domicilio"]').val('0'); 
    $('.envio_domicilio').hide();

    $('#switch_urgencias').prop('checked', false);    
    $('[name="urgencias"]').val('0'); 
    $('.urgencias').hide();

    $('#switch_destacado').prop('checked', false);
    $('[name="destacado"]').val('0'); 

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'local/miniaturas/local_avatar_mini.png' ?>');  
    $('#imagen').val('local_avatar_mini.png');


    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    mostrar_medios_pago_local();

    // Tab 'Informacion' por defecto activo
    $('.nav-tabs a[href="#tab-info"]').tab('show');

    $('#modal_form').modal('show'); 
    $('.modal-title').text('Nuevo Local'); 
}


function edit_local(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);
            $('[name="categoria"]').val(data.categoria);
            $('[name="nombre_titular"]').val(data.nombre_titular);
            $('[name="apellido_titular"]').val(data.apellido_titular);
            $('[name="telefono"]').val(data.telefono);
            $('[name="email"]').val(data.email);
            $('[name="telefono2"]').val(data.telefono2);
            $('[name="email2"]').val(data.email2);            
            $('[name="direccion"]').val(data.direccion);
            $('[name="localidad"]').val(data.localidad);
            $('[name="horario_atencion"]').val(data.horario_atencion);
            $('[name="horario_sabados"]').val(data.horario_sabados);
            $('[name="horario_domingos"]').val(data.horario_domingos);
            $('[name="envio_domicilio"]').val(data.envio_domicilio);
            $('[name="costo_envio"]').val(data.costo_envio);
            $('[name="zona_entrega"]').val(data.zona_entrega);
            $('[name="dias_entrega"]').val(data.dias_entrega);
            $('[name="provincia"]').val(data.id_provincia);
            $('[name="imagen"]').val(data.avatar);
            $('[name="latitud"]').val(data.latitud);
            $('[name="longitud"]').val(data.longitud);  
            $('[name="destacado"]').val(data.destacado);
            $('[name="urgencias"]').val(data.urgencias);
            $('[name="urgencias_consultorio"]').val(data.urgencias_consultorio);
            $('[name="urgencias_domicilio"]').val(data.urgencias_domicilio);
            $('[name="telefono_urgencias"]').val(data.telefono_urgencias);

            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'local/miniaturas/' ?>'  + data.avatar);    

            if (data.envio_domicilio == 1) 
            {
                $('#switch_envio_domicilio').prop('checked', true);
                $('.envio_domicilio').show();
            }
            else
            {
                $('#switch_envio_domicilio').prop('checked', false);
                $('.envio_domicilio').hide();
            }

            data.destacado == 1 ? $('#switch_destacado').prop('checked', true) : $('#switch_destacado').prop('checked', false); 

            if (data.urgencias == 1) 
            {
                $('#switch_urgencias').prop('checked', true);
                data.urgencias_consultorio == 1 ? $('#chk_urgencias_consultorio').prop('checked', true) : $('#chk_urgencias_consultorio').prop('checked', false); 
                data.urgencias_domicilio == 1 ? $('#chk_urgencias_domicilio').prop('checked', true) : $('#chk_urgencias_domicilio').prop('checked', false); 
                $('.urgencias').show();
            }
            else
            {
                $('#switch_urgencias').prop('checked', false);
                $('.urgencias').hide();
            }

            mostrar_medios_pago_local(id);

            // Tab 'Información por defecto activo'
            $('.nav-tabs a[href="#tab-info"]').tab('show');

            $('#modal_form').modal('show'); 
            $('.modal-title').text('Editar Local'); 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS DE LOCAL"); 
        }
    });
}


function mostrar_medios_pago_local(id = '')
{
    // Limpia lista anterior
    $("#lista_medios_pago").html('');

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_medios_pago_local/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            console.log(data);          
            medios_pago = data.medios_pago;
            
            if (medios_pago.length > 0)
            {
                // Lista de medios_pago
                for (i = 0; i < medios_pago.length; ++i) 
                {
                    checked = '';
                    tiene_items = false;

                    // Valida si el medio de pago está asociado al local
                    if (medios_pago[i].local_medio_pago_id)
                    {
                        checked = 'checked = "checked"';
                    }                    

                   // Items de cada medio de pago
                   htmlItems = '';

                   if (medios_pago[i].items)
                   {
                        if (medios_pago[i].items.length > 0) tiene_items = true;

                      for (j = 0; j < medios_pago[i].items.length; ++j) 
                      {
                        checked_item = '';

                        // Valida si el item está asociado al local
                        if (medios_pago[i].items[j].local_medio_pago_id)
                        {
                            checked_item = 'checked = "checked"';
                        }

                         htmlItems += 
                            '<div class="push-10-l push-8-t">' + 
                              '<div class="col-xs-12 col-sm-3" style="height: 12px;">' + 
                                  '<input type="checkbox" ' + checked_item + '" name="chkMediopago_' + medios_pago[i].id + '_' + medios_pago[i].items[j].id + '" id="chkMediopago_' + medios_pago[i].id + '_' + medios_pago[i].items[j].id + '" class="pull-left" >' + 
                                  '<p class="pull-left push-10-l" style="font-size: 13px;">' + medios_pago[i].items[j].nombre + '</p>' + 
                              '</div>' +                                 
                            '</div>' +     
                            '<div class="clearfix"></div>';                
                      }
                   }

                   if (tiene_items)
                   {
                        $("#lista_medios_pago").append(
                          '<li class="">' +
                              '<div class="push-5-l push-10 push-10-t">' + 
                                  '<h6 style="padding-top: 2px;">' + medios_pago[i].nombre + '</h6>' + 
                                  htmlItems + 
                              '</div>' +                                                                                    
                          '</li>');
                   }
                   else
                   {
                        $("#lista_medios_pago").append(
                          '<li class="">' +
                              '<div class="push-5-l push-10 push-10-t">' + 
                                '<input type="checkbox" ' + checked + '" name="chkMediopago_' + medios_pago[i].id + '_0" id="chkMediopago_' + medios_pago[i].id + '" class="push-10-r pull-left">' +
                                  '<h6 style="padding-top: 2px;">' + medios_pago[i].nombre + '</h6>' + 
                                  htmlItems + 
                              '</div>' +                                                                                    
                          '</li>');
                   }
                }
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });
}


function reload_table()
{
    table.ajax.reload(null,false); 
}

function reload_table_articulos()
{
    table_articulos.ajax.reload(null,false);  
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 

    var url;
    var continuar = false;

    // Geolocalizacion
    var geocoder = new google.maps.Geocoder();


    var address = '' + $('[name="direccion"]').val() + ',' + $('[name="localidad"]').val() + ',' + $('#provincia option:selected').text();
    
    geocoder.geocode( { 'address': address}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            $('[name="latitud"]').val(results[0].geometry.location.lat());
            $('[name="longitud"]').val(results[0].geometry.location.lng());  

            continuar_save();
        }
        // Se no pudo ubicar la direccion, confirma si grabar local de todas maneras
        else
        {
            continuar = confirm("No pudo obtenerse ubicación del Local con la dirección ingresada\n\n ¿Desea guardar de todas maneras?");

            if (continuar){

                continuar_save();
            }
            else{
                $('#btnSave').text('Guardar'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable 
            }
        }
    }); 

}


function continuar_save(){

    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Local/ajax_add";
        mensaje = 'Local creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/admin/Local/ajax_update";
        mensaje = 'Local modificado.';
    }

    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));


    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,            
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                if (data.login) 
                {
                    // Sesion expirada
                    window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                }
                else
                {                                
                    switch (data.error)
                    {
                        // Error en la transacción
                        case 'transaction':
                            $('#modal_form').modal('hide');
                            $('#modal_error_sistema').modal('show');
                            console.log('Error en insert de registro - Local->ajax_add');
                            break;

                        // Error en el envio del mail de bienvenida
                        case 'mail':
                            $('#modal_form').modal('hide');
                            $('#modal_error_sistema').modal('show');
                            console.log('Error en el envio de mail - Local->ajax_add');
                            break;

                        // Datos de insert incorrectos
                        default:

                            for (var i = 0; i < data.inputerror.length; i++) 
                            {
                                $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                                $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                            }
                            break;
                    }
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_form').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log('Error al crear o modificar Local (' + errorThrown + ')');
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


function delete_local(nombre, id)
{
    if(confirm('¿Eliminar Local "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Local/ajax_delete_full/" + id,
            type: "POST",
            dataType: "JSON",

            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Local eliminado.');                 
                }
                else
                {
                    if (data.login) 
                    {
                        // Sesion expirada
                        window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                    }
                    else
                    {                                    
                        aviso('danger', data.mensaje, "ERROR AL ELIMINAR LOCAL");  
                        console.log(data.error);
                    }
                }
            },

            error: function (jqXHR, textStatus, errorThrown)
            {
                
                aviso('danger', textStatus, 'Error al eliminar. ' + textStatus); 
            }
        });

    }
}


function edit_password(id)
{

    $('#form_password')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('[name="local_id"]').val(id);
    $('#modal_form_password').modal('show'); 
    $('.modal-title').text('Modificar Password');
}


function mail_bienvenida(id)
{
    $('#btnMail_bienvenida_' + id).html('Enviando...');
    $('#btnMail_bienvenida_' + id).attr('disabled', true); 

    var url = "<?php echo BASE_PATH ?>/admin/Local/ajax_mail_bienvenida/" + id;

    var data_post = {
        'local_id': id
    };

   
    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {        
            // Mensaje mail enviado ok
            $('#resultado_ok_titulo1').hide();
            $('#resultado_ok_titulo2').html('Mail enviado correctamente');

            $('#modal_resultado_ok').modal('show');     

            $('#btnMail_bienvenida_' + id).html(' <i class="si si-envelope"></i> &nbsp;bienvenida&nbsp;&nbsp;');
            $('#btnMail_bienvenida_' + id).attr('disabled', false);                 
        },
        // Error al procesar el pedido
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_error_sistema').modal('show');       

            $('#btnMail_bienvenida_' + id).html(' <i class="si si-envelope"></i> &nbsp;bienvenida&nbsp;&nbsp;');
            $('#btnMail_bienvenida_' + id).attr('disabled', false);                   
        }
    });  

  
}


function save_password()
{
    $('#btnSave_Password').text('Guardando...'); 
    $('#btnSave_Password').attr('disabled',true); 
    
    var url;

    url = "<?php echo BASE_PATH ?>/admin/Local/ajax_update_password";


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_password').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_password').modal('hide');
                reload_table();
                aviso('success', 'Password modificada.'); 

            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Password (' + errorThrown + ')'); 

            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 
        }
    });
}


function switch_local(id){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_activar_desactivar/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {            
                if (data.nuevo_status == 1)
                    aviso('success', 'Local habliitado.');  
                else
                    aviso('success', 'Local deshabliitado.');  
            }
            else
            {       
                // Sesion expirada
                if (data.login) window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
            }                
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al activar / desactivar. '); 
        }
    });
}


function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'local/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


$('#chk_all_locales').change(function() {
    $(".chk_locales").prop('checked', $(this).prop("checked"));
});


function articulos_local(local, id)
{

    table_articulos = $('#table_articulos').DataTable({ 

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 25,        

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Local_Articulo/ajax_list/" + id,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, -1 ], // first and last column
            "orderable": false, //set not orderable
        },
        ],
        // Filtros
        initComplete: function () 
        {
            // Limpia filtros
            $('#div_filtros_articulos').html('');

            this.api().columns().every( function () 
            {
                var column = this;

                // Filtro por TIPO (Geotienda / Local)
                if (column.index() == 0){
                    var select = $('<select class="form-control hidden-xs pull-left push-10-r push-5" style="width: 120px"><option value="">TIPO</option></select>')
                        .appendTo( $('#div_filtros_articulos'))
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );

                    select.append('<option value="GeoTienda">GeoTienda</option>');
                    select.append('<option value="Local">Local</option>');
                }
            } );
        },         

    });    

    $('.modal-title-articulos').text(' ' + local + ' - Artículos del local');
    $('#modal_articulos_local').modal('show'); 
}


$('#selectAll').change(function() {
    $(".chkDescuento").prop('checked', $(this).prop("checked"));
});


function aplicar_descuento_masivo(descuento)
{

    var porcentaje = $('option:selected', descuento).attr('porcentaje');
    var descuento_id = descuento.value;

    if (typeof descuento_id !== "undefined")
    {
        // Recupera los articulos del local seleccionados
        var data = { 'local_articulo[]' : [], 'precio[]' : [], 'stock[]' : []};

        $(".chkDescuento:checked").each(function() {
            data['local_articulo[]'].push($(this).val());
            data['precio[]'].push($(this).attr('precio'));
            data['stock[]'].push($(this).attr('stock'));

        });


        // Valida si se selecciono algun articulo
        if (data['local_articulo[]'].length > 0){

           data['descuento_id'] = descuento_id;
           data['porcentaje'] = porcentaje;

            $.ajax({
                url : "<?php echo BASE_PATH ?>/admin/Local_Articulo/ajax_descuento_masivo/",    
                type: "POST",
                data: data,
                dataType: "JSON",


                success: function(data)
                {
                    $('[name="selectAll"]').prop('checked', false);
                    $('[name="descuento"]').val('-1');  

                    reload_table_articulos();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    
                    aviso('danger', textStatus, 'Error al aplicar descuento masivo  (' + errorThrown + ')'); 

                }
            });
        }
        else{

            aviso('danger', 'DESCUENTO MASIVO\n------------------------\n\nSeleccione por lo menos un artículo para aplicar el descuento.'); 
            $('[name="descuento"]').val('-1');  
        }
 
    }
    
}


function edit_articulo_local (id, precio, descuento, stock, local, articulo)
{
    $('[name="local_articulo_id"]').val(id);
    $('[name="precio"]').val(precio);

    if(stock == "1") 
    {
      $('#chkSinStock').prop('checked', false);
    } 
    else {
      $('#chkSinStock').prop('checked', true);
    }

    $('[name="descuentoArticulo"]').val(descuento);

    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('#modal_form_articulo').modal('show'); // show bootstrap modal when complete loaded
    $('.modal-title-articulo').text(' ' + local + ' - Editar Artículo');
    $('.modal-title-descripcion-articulo').text(' ' + articulo);
}


function save_articulo_local(){

    var articulo_id = $('[name="local_articulo_id"]').val();

    var ddlDescuento = $('[name="descuentoArticulo"]');
    var descuento_id = ddlDescuento.val();
    var porcentaje = $('option:selected', ddlDescuento).attr('porcentaje');

    var precio = $('[name="precio"]').val();

    var data = {'articulo_id' : articulo_id };

    data['precio'] = precio;
    data['descuento_id'] = descuento_id;
    data['porcentaje'] = porcentaje;
   
    if($("#chkSinStock").is(':checked'))
    { 
        data['stock'] = "0"; 
    }
    else {
        data['stock'] = "1";
    }
    

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local_Articulo/ajax_update/",    
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_articulo').modal('hide');
                reload_table_articulos();
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           
            aviso('danger', textStatus,'Error al editar datos del artículo (' + errorThrown + ')'); 
        }
    });

}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Local/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}


function banner_local(id)
{
    local_activo = id;

    $('#form_banner')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('[name="banner_local_id"]').val(id);

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_banner_activo/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            // Determina nivel del banner activo
            nivel_banner_activo_local = 'local';

            if (data.global)
                nivel_banner_activo_local = 'global';
            else if (data.grupal)
                nivel_banner_activo_local = 'grupal';

            $('#lbl_nivel_banner_activo').html('Nivel ' + nivel_banner_activo_local); 

            $('#ddl_banner_grupal_local').attr('disabled', false);
            $('#btn_quitar_banner_grupal_local').attr('disabled', false);

            // Vista previa del banner activo
            switch (nivel_banner_activo_local){

                case 'global':
                    vista_previa_banner_activo(data.global.texto, data.global.color, data.global.fondo);

                    // Desactiva opciones 'Asignar banner grupal' y Quitar asignación'
                    $('#ddl_banner_grupal_local').attr('disabled', true);
                    $('#btn_quitar_banner_grupal_local').attr('disabled', true);
                    break;

                case 'grupal':
                    vista_previa_banner_activo(data.grupal.texto, data.grupal.color, data.grupal.fondo);
                    break;

                case 'local':
                    vista_previa_banner_activo(data.local.texto, data.local.color, data.local.fondo);

                    // Desactiva opción 'Quitar asignación', ya que no hay banner a nivel grupal asignado
                    $('#btn_quitar_banner_grupal_local').attr('disabled', true);
                    break;                
            }


            // Banner del local
            if (data.texto != ''){

                $('[name="texto_banner"]').val(data.local.texto);
                $('[name="fondo_banner"]').val(data.local.fondo);
                $('[name="color_banner"]').val(data.local.color);

                $('#div_vista_previa_banner').html(data.local.texto); 
                $('#div_vista_previa_banner').css('color',  data.local.color);  
                $('#div_vista_previa_banner').css('background-color',  data.local.fondo); 
            }
            else{

                $('[name="color_banner"]').val('white');
                $('[name="fondo_banner"]').val('blue');
                $('#div_vista_previa_banner').html(''); 
                $('#div_vista_previa_banner').css('color', 'white');
                $('#div_vista_previa_banner').css('background-color', 'blue');
            }


            // Colores banner del local
            // ------------------------

            // Color texto
            $('#color_banner').spectrum({   
                preferredFormat: "hex",
                showPaletteOnly: true,
                hideAfterPaletteSelect: true,        
                move: function(color) {
                    cambiaColorTextoBanner(color);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            }); 

            // Color fondo
            $('#fondo_banner').spectrum({   
                preferredFormat: "hex",
                showPaletteOnly: true,
                hideAfterPaletteSelect: true,        
                move: function(color) {
                    cambiaColorFondoBanner(color);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                    "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"], 
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)", 
                    "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)", 
                    "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)", 
                    "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)", 
                    "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)", 
                    "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                    "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                    "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                    "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)", 
                    "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            }); 

            $('#modal_banner').modal('show'); 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR EL BANNER DEL LOCAL"); 
        }
    });
}


function vista_previa_banner_activo(texto, color, fondo){

    $('#div_banner_activo').html(texto); 
    $('#div_banner_activo').css('color',  color);  
    $('#div_banner_activo').css('background-color', fondo); 

}


function save_banner()
{
    $('#btnSaveBanner').text('Guardando...'); 
    $('#btnSaveBanner').attr('disabled',true); 


    var url = "<?php echo BASE_PATH ?>/admin/Local/ajax_update_banner";

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_banner').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) 
            {
                aviso('success', 'Banner del local modificado.'); 

                // Si corresponde, actualiza vista previa del banner activo
                if (nivel_banner_activo_local == 'local')
                    vista_previa_banner_activo($('#texto_banner').val(), $('#color_banner').val(), $('#fondo_banner').val());
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error');
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
                }
            }

            $('#btnSaveBanner').text('Guardar');
            $('#btnSaveBanner').attr('disabled',false);

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al actualizar banner del local: (' + errorThrown + ')'); 

            $('#btnSaveBanner').text('Guardar');
            $('#btnSaveBanner').attr('disabled',false); 
        }
    });
}


function aplicar_banner_grupal(banner)
{
    var banner_id = banner.value;

    if (typeof banner_id !== "undefined")
    {
        // Recupera los locales seleccionados
        var data = { 'locales[]' : [] };

        $(".chk_locales:checked").each(function() {
            data['locales[]'].push($(this).val());
        });


        // Valida si se selecciono algun local
        if (data['locales[]'].length > 0){

           data['banner_id'] = banner_id;

            $.ajax({
                url : "<?php echo BASE_PATH ?>/admin/Local/ajax_banner_grupal/",    
                type: "POST",
                data: data,
                dataType: "JSON",


                success: function(data)
                {
                    $('[name="chk_all_locales"]').prop('checked', false);
                    $('[name="ddl_banner_grupal"]').val('-1');  

                    reload_table();

                    if (nivel_banner_activo == 'global')
                        aviso('success', 'Banner grupal asignado. Será mostrado cuando se desactive el banner global'); 
                    else
                        aviso('success', 'Banner grupal asignado.'); 
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    
                    aviso('danger', textStatus, 'Error al asignar banner grupal  (' + errorThrown + ')'); 

                }
            });
        }
        else{

            aviso('danger', 'Seleccione por lo menos un local para asignar el banner.'); 
            $('[name="ddl_banner_grupal"]').val('-1');  
        }
 
    }
    
}


function aplicar_banner_grupal_a_local(banner)
{
    var banner_id = banner.value;

    if (typeof banner_id !== "undefined")
    {
        // Asigna local activo
        var data = { 'locales[]' : [] };
        data['locales[]'].push(local_activo);

       data['banner_id'] = banner_id;

        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Local/ajax_banner_grupal/",    
            type: "POST",
            data: data,
            dataType: "JSON",


            success: function(data)
            {
                $('[name="ddl_banner_grupal_local"]').val('-1');  

                // Actualiza nivel del banner activo
                if (nivel_banner_activo_local == 'local' || nivel_banner_activo_local == 'grupal'){

                    vista_previa_banner_activo(data.texto, data.color, data.fondo);

                    nivel_banner_activo_local = 'grupal';
                    $('#lbl_nivel_banner_activo').html('Nivel grupal'); 
                    $('#btn_quitar_banner_grupal_local').attr('disabled', false);
                }

                aviso('success', 'Banner grupal asignado al local.'); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                
                aviso('danger', textStatus, 'Error al asignar banner grupal  (' + errorThrown + ')'); 

            }
        });
 
    }
    
}


function quitar_banner_grupal(){

    // Recupera los locales seleccionados
    var data = { 'locales[]' : [] };

    $(".chk_locales:checked").each(function() {
        data['locales[]'].push($(this).val());
    });


    // Valida si se selecciono algun local
    if (data['locales[]'].length > 0){

        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Local/ajax_quitar_banner_grupal/",    
            type: "POST",
            data: data,
            dataType: "JSON",

            success: function(data)
            {
                $('[name="chk_all_locales"]').prop('checked', false);

                reload_table();
                aviso('success', 'Banner grupal desasignado.'); 
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                
                aviso('danger', textStatus, 'Error al quitar asignación de banner grupal  (' + errorThrown + ')'); 

            }
        });
    }
    else{

        aviso('danger', 'Seleccione por lo menos un local para quitar banner asignado.'); 
    }
}


function quitar_banner_grupal_local(){

    // Asigna local activo
    var data = { 'locales[]' : [] };
    data['locales[]'].push(local_activo);


    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_quitar_banner_grupal/",    
        type: "POST",
        data: data,
        dataType: "JSON",

        success: function(data)
        {
            // Actualiza nivel del banner activo
            if (nivel_banner_activo_local == 'grupal'){

                vista_previa_banner_activo(data.texto, data.color, data.fondo);

                nivel_banner_activo_local = 'local';
                $('#lbl_nivel_banner_activo').html('Nivel local'); 
                $('#btn_quitar_banner_grupal_local').attr('disabled', true);
            }

            aviso('success', 'Banner grupal desasignado.'); 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            
            aviso('danger', textStatus, 'Error al quitar asignación de banner grupal  (' + errorThrown + ')'); 

        }
    });

}

function get_nivel_banner_activo(){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_nivel_banner/",
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            nivel_banner_activo = data.nivel;
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al obtener nivel del banner. '); 
        }
    });
}


function get_precio_local_destacado(){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_get_precio_destacado/",
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="precio_local_destacado"]').val(data.precio);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al obtener precio local destacado. '); 
        }
    });
}


// Vista previa del banner
// -----------------------

// Texto
$('#texto_banner').change(function() {
  $('#div_vista_previa_banner').html($('#texto_banner').val()); 
});


// Color texto
function cambiaColorTextoBanner(color) {
    $('#div_vista_previa_banner').css('color', color.toHexString());    
}

// Color fondo
function cambiaColorFondoBanner(color) {
    $('#div_vista_previa_banner').css('background-color', color.toHexString());    
}


$("#switch_envio_domicilio").change(function() {
    if(this.checked) {
        $('[name="envio_domicilio"]').val('1'); 
        $('.envio_domicilio').show();
    }
    else {
        $('[name="envio_domicilio"]').val('0'); 
        $('.envio_domicilio').hide();
    }
});

$("#switch_destacado").change(function() {
    if(this.checked) {
        $('[name="destacado"]').val('1'); 
    }
    else {
        $('[name="destacado"]').val('0'); 
    }
});

$("#switch_urgencias").change(function() {
    if(this.checked) {
        $('[name="urgencias"]').val('1'); 
        $('.urgencias').show();
    }
    else {
        $('[name="urgencias"]').val('0'); 
        $('.urgencias').hide();
    }
});

$("#chk_urgencias_consultorio").change(function() {
    if(this.checked) {
        $('[name="urgencias_consultorio"]').val('1'); 
    }
    else {
        $('[name="urgencias_consultorio"]').val('0'); 
    }
});

$("#chk_urgencias_domicilio").change(function() {
    if(this.checked) {
        $('[name="urgencias_domicilio"]').val('1'); 
    }
    else {
        $('[name="urgencias_domicilio"]').val('0'); 
    }
});

// LOCALES DESTACADOS
function actualizar_precio_destacado()
{
    var precio_local_destacado = $('[name="precio_local_destacado"]').val();

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_actualizar_precio_destacado/" + precio_local_destacado,    
        type: "POST",
        dataType: "JSON",

        success: function(data)
        {
            if(data.status) 
            {
                aviso('success', 'Precio actualizado.'); 

                $('#precio_local_destacado').removeClass('has-error');
                $('#error_actualizar_destacado').empty();                 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('#precio_local_destacado').addClass('has-error');
                    $('#error_actualizar_destacado').text(data.error_string[i]); 

                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al actualizar el precio  (' + errorThrown + ')'); 
        }
    });
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Local Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 

                    <ul class="nav nav-tabs" data-toggle="tabs">
                        <li class="active">
                            <a href="#tab-info" data-toggle="tab">Información</a>
                         </li>

                        <li>
                            <a href="#tab-pago" data-toggle="tab">Medios de Pago</a>
                         </li>    
                    </ul>                 
                    
                    <div class="block-content tab-content" style="border: 1px solid #ddd; border-top-color: transparent;">
                        <div class="tab-pane active" id="tab-info">
                            <!-- CATEGORIA --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Categoría</label>
                                <div class="col-md-4">
                                    <select name="categoria" class="form-control">                       
                                        <option selected value="">Seleccione...</option>
                                        <option  value="Petshop">Petshop</option>
                                        <option  value="Veterinaria">Veterinaria</option>
                                    </select>  
                                    <span class="help-block"></span>
                                </div>
                            </div>  

                            <!-- NOMBRE --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Nombre</label>
                                <div class="col-md-9">
                                    <input name="nombre" placeholder="Nombre del Local" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                            </div>

                            <!--DESTACADO --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Destacado</label>
                                <div class="col-md-4">          
                                    <input type='hidden' value='0' name='destacado' id='destacado'> 
                                    <label class="css-input css-checkbox css-checkbox-info">
                                        <input type="checkbox" name ="switch_destacado" id="switch_destacado"><span></span> ¿Local Destacado?
                                    </label>                                               
                                    <span class="help-block"></span>
                                </div>                                              
                            </div>

                            <!-- TITULAR --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Titular</label>
                                <div class="col-md-4">
                                    <input name="nombre_titular" placeholder="Nombre del Titular" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-5">
                                    <input name="apellido_titular" placeholder="Apellido del Titular" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                            
                            </div>

                            <!-- EMAIL --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">E-Mail</label>
                                <div class="col-md-4">
                                    <input name="email" placeholder="E-Mail" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-5">
                                    <input name="email2" placeholder="E-Mail Alternativo" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                            
                            </div>

                            <!-- TELEFONO --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Teléfono</label>
                                <div class="col-md-4">
                                    <input name="telefono" placeholder="Teléfono" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-5">
                                    <input name="telefono2" placeholder="Teléfono Alternativo" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                            
                            </div>

                            <!-- DIRECCION --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Dirección</label>
                                <div class="col-md-9">
                                    <input name="direccion" placeholder="Calle y Número" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>

                                 <label class="control-label col-md-3"></label> 

                                <div class="col-md-4">
                                    <input name="localidad" placeholder="Localidad" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>   

                                <div class="col-md-5">
                                    <select name="provincia" id="provincia" class="form-control" >
                                        <option selected value="0">Seleccione Provincia...</option>
                                        <?php 
                                        foreach($provincias as $provincia){ ?>
                                            <option value="<?php echo $provincia->id ?>" ><?php echo $provincia->nombre ?></option> 
                                        <?php } ?>    
                                    </select>  
                                    <span class="help-block"></span>
                                </div>    
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3"></label> 
                                <div class="col-md-4">
                                    <input name="latitud" placeholder="Latitud" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>    
                                
                                <div class="col-md-5">
                                    <input name="longitud" placeholder="Longitud" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                                                                                                            
                            </div>
                           
                            <!-- HORARIOS ATENCION --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Horarios de Atención</label>
                                <div class="col-md-4">
                                    <input name="horario_atencion" placeholder="Semana" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>

                            </div>
                           
                            <div class="form-group">
                                <label class="control-label col-md-3"> </label>
                                <div class="col-md-4">
                                    <input name="horario_sabados" placeholder="Sábados" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>        
                                <div class="col-md-5">
                                    <input name="horario_domingos" placeholder="Domingos" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                      
                            </div>

                            <!-- Urgencias 24 hs. --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Urgencias</label>
                                <div class="col-md-4">          
                                    <input type='hidden' value='0' name='urgencias' id='urgencias'> 
                                    <label class="css-input css-checkbox css-checkbox-info">
                                        <input type="checkbox" name ="switch_urgencias" id="switch_urgencias"><span></span> ¿Servicio de urgencias las 24 hs.?
                                    </label>                                               
                                    <span class="help-block"></span>
                                </div>                            
                                <div class="col-md-5 urgencias" style="display: none;">
                                    <input name="telefono_urgencias" placeholder="Teléfono Urgencias" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                     
                            </div>    

                            <div class="form-group urgencias" style="display: none;">
                                <label class="control-label col-md-3"></label>
                                <div class="col-md-4">
                                    
                                </div>                             
                                <div class="col-md-5 push--10-t">
                                    <input type='hidden' value='0' name='urgencias_consultorio' id='urgencias_consultorio'>
                                    <label class="css-input css-checkbox css-checkbox-info">
                                        <input type="checkbox" name="chk_urgencias_consultorio" id="chk_urgencias_consultorio" /><span></span> Atención en Consultorio
                                    </label>   
                                    <input type='hidden' value='0' name='urgencias_domicilio' id='urgencias_domicilio'>
                                    <label class="css-input css-checkbox css-checkbox-info push-10-l">
                                        <input type="checkbox" name ="chk_urgencias_domicilio" id="chk_urgencias_domicilio" /><span></span> Visita a Domicilio
                                    </label>                                    
                                    <span id="modalidad_urgencias" name="modalidad_urgencias"></span>
                                    <span class="help-block"></span>
                                </div>                     
                            </div>                                            

                            <!--Envios --> 
                            <div class="form-group">
                                <label class="control-label col-md-3">Envío</label>
                                <div class="col-md-4">          
                                    <input type='hidden' value='0' name='envio_domicilio' id='envio_domicilio'> 
                                    <label class="css-input css-checkbox css-checkbox-info">
                                        <input type="checkbox" name ="switch_envio_domicilio" id="switch_envio_domicilio"><span></span> ¿Hace envíos a domicilio?
                                    </label>                                               
                                    <span class="help-block"></span>
                                </div>                            
                                <div class="col-md-5 envio_domicilio">
                                    <input name="costo_envio" placeholder="Costo de Envío" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                     
                            </div>

                             <!-- Entregas --> 
                            <div class="form-group envio_domicilio">
                                <label class="control-label col-md-3">Zona y Días de Entrega</label>
                                <div class="col-md-4">
                                    <input name="zona_entrega" placeholder="Zona de Entrega" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-5">
                                    <input name="dias_entrega" placeholder="Días de Entrega" class="form-control" type="text">
                                    <span class="help-block"></span>
                                </div>                            
                            </div>                  

                             <!-- IMAGEN --> 
                            <div class="form-group">
                                <label class="control-label col-md-2">Imagen</label>
                                <div class="col-md-10">
                                    <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                    <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                    <div class="clearfix"></div>  
                                    <input type="hidden" value="local_avatar_mini.png" id="imagen" name="imagen" />
                                    <span class="help-block"></span>
                                    
                                    <div class="clearfix"></div>    
                                    <br/>                                                       
                                    <img class="centrado" src="<?php echo IMG_PATH . 'local/miniaturas/local_avatar_mini.png' ?>" id="imagenPreview" /> 
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab-pago">
                            <ul class="list list-li-clearfix push-20" id="lista_medios_pago"></ul>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    


<!-- Bootstrap modal CAMBIO PASSWORD -->
<div class="modal fade" id="modal_form_password" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Modificar Contraseña</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_password" class="form-horizontal">
                    <input type="hidden" value="" name="local_id"/> 
                    <div class="form-body">

                        <!-- NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nueva password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Nueva password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- REPITE NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Repita password</label>
                            <div class="col-md-9">
                                <input name="password2" placeholder="Repita password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_Password" onclick="save_password()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal --> 


<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/local/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                                 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    


<!-- Bootstrap modal ARTICULOS LOCAL -->
<div class="modal fade" id="modal_articulos_local" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-articulos"> Artículos del Local</h3>
            </div>
            <div class="modal-body form">
                <!-- DESCUENTO MASIVO -->
                <div class="form-group">
                    <select name="descuento" class="form-control" onchange="aplicar_descuento_masivo(this)">                       
                        <option selected value="-1">Aplicar descuento ...</option>
                       
                        <?php foreach($descuentos as $descuento){ ?>
                            <option porcentaje="<?php echo $descuento->porcentaje ?>" value="<?php echo $descuento->id ?>"><?php echo $descuento->nombre ?></option> 
                        <?php } ?>
                
                    </select>  
                </div>   

                <div id="div_filtros_articulos" style="position: absolute; left: 100px; z-index: 1;"></div>

                <div class="form-group">
                    <table id="table_articulos" class="table table-bordered table-striped js-dataTable-full"  cellpadding="10" width="100%">
                        <thead>
                            <tr>
                                <th style="width:4px; padding-left:10px"><input type="checkbox" name="selectAll" id="selectAll" value=""></th>
                                <th>Artículo</th>
                                <th>Tipo</th>
                                <th>Presentación</th>
                                <th>Marca</th>
                                <th>Precio</th>
                                <th>Descuento</th>
                                <th>Stock</th>
                                <th>Estado</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>   

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal --> 


<!-- Bootstrap modal MODIFICAR ARTICULO  -->
<div class="modal fade" id="modal_form_articulo" role="dialog" style="background-color:rgba(0, 0, 0, 0.2);">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-articulo" style="margin-bottom:6px"></h3>
                <h4 class="modal-title-descripcion-articulo"> Descripción</h4>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_articulo" class="form-horizontal">
                    <input type="hidden" value="" name="local_articulo_id" id="local_articulo_id" /> 
                    <div class="form-body">

                        <!-- PRECIO --> 
                        <div class="form-group"> 
                            <label class="control-label col-md-3">Precio</label>
                            <div class="col-md-9">
                                <input name="precio" id="precio" placeholder="Precio" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- STOCK --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Sin Stock</label>
                            <div class="col-md-9">
                                <input type="checkbox" name="chkSinStock" id="chkSinStock" value="">
                            </div>
                        </div>

                        <!-- DESCUENTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Descuento</label>
                            <div class="col-md-9">
                                <select name="descuentoArticulo" id="descuentoArticulo" class="form-control">                       
                                    <option selected value="-1">Aplicar descuento ...</option>
                                   
                                    <?php foreach($descuentos as $descuento){ ?>
                                        <option porcentaje="<?php echo $descuento->porcentaje ?>" value="<?php echo $descuento->id ?>"><?php echo $descuento->nombre ?></option> 
                                    <?php } ?>
                            
                                </select>                                  
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_Articulo" onclick="save_articulo_local()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal --> 


<!-- Bootstrap modal BANNER -->
<div class="modal fade" id="modal_banner" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Configurar Banner del Local</h3>
            </div>
            <div class="modal-body form bg-gray-light">

                <!-- BANNER ACTIVO (global / grupal / local -->
                <div class="block block-bordered">
                    <div class="block-header">
                        <h3 class="block-title">Banner Activo  <span class="text-celeste" id="lbl_nivel_banner_activo">Nivel Local</span></h3>
                    </div>
                    <div class="block-content">
                        <div class="vista-previa-banner text-center" id="div_banner_activo">
                        </div>       
  
                        <div class="row push-25-t">
                            <div class="col-md-6">    
                                <div class="form-group">
                                    <div class="h5 push-5">Asignar banner grupal</div>   
                                    <select name="ddl_banner_grupal_local" id="ddl_banner_grupal_local" class="form-control" onchange="aplicar_banner_grupal_a_local(this)">                       
                                        <option selected value="-1">Seleccione banner...</option>
                                       
                                        <?php foreach($banners as $banner){ ?>
                                            <option value="<?php echo $banner->id ?>"><?php echo $banner->nombre ?></option> 
                                        <?php } ?>
                                
                                    </select>  
                                </div> 
                            </div>

                            <div class="col-md-6">    
                                <div class="form-group">
                                    <div class="clearfix"></div>
                                    <button class="btn btn-success  pull-right push-25-t" id="btn_quitar_banner_grupal_local" onclick="quitar_banner_grupal_local()">Quitar asignación</button>
                                </div> 
                            </div>                
                        </div>
                    </div>  
                </div>
                <!-- fin ASIGNAR BANNER GRUPAL -->

                <!-- BANNER PROPIO DEL LOCAL -->
                <div class="block block-bordered">
                    <div class="block-header">

                        <div class="block-options-simple">
                            <button class="btn btn-xs btn-primary" type="button" id="btnSaveBanner" onclick="save_banner()" >Guardar</button>
                        </div>

                        <h3 class="block-title">Banner del local</h3>
                    </div>
                    <div class="block-content">                
                        <form action="#" id="form_banner" class="form-horizontal" enctype="multipart/form-data">
                            <input type="hidden" value="" name="banner_local_id"/> 
                            <div class="form-body">
                                
                                <!-- TEXTO BANNER --> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Texto</label>
                                    <div class="col-md-9">
                                        <input name="texto_banner" id="texto_banner" placeholder="Texto Banner" class="form-control" type="text">
                                        <span class="help-block"></span>
                                    </div>                                  
                                </div>

                                <!-- COLOR TEXTO --> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Color Texto</label>
                                    <div class="col-md-9">
                                        <input id="color_banner" name="color_banner" class="form-control" type="text" value="">
                                        <span class="help-block"></span>
                                    </div>
                                </div>

                                <!-- COLOR FONDO --> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Color Fondo</label>
                                    <div class="col-md-9">
                                        <input id="fondo_banner" name="fondo_banner" class="form-control" type="text" value="">
                                        <span class="help-block"></span>
                                    </div>
                                </div>  

                                <!-- VISTA PREVIA --> 
                                <div class="form-group">
                                    <label class="control-label col-md-3">Vista Previa</label>
                                    <div class="col-md-9 vista-previa-banner text-center" id="div_vista_previa_banner">
                                    </div>
                                </div>  

                            </div>
                                                        
                        </form>
                    </div>
                </div>                        
                <!-- fin BANNER PROPIO DEL LOCAL -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal BANNER -->   