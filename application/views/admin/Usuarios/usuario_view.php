<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Administradores
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Usuario/index');">Administradores</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_usuario()"><i class="glyphicon glyphicon-plus"></i> Nuevo Administrador</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Usuario</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>E-mail</th>
                        <th style="width:140px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Usuario/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
            "className": "hidden-xs", "targets": [1,2,3],              
        },
        ],
    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_usuario()
{
    save_method = 'add';

    // Si estamos creando un usuario, muestra los campos 'password'
    $('[id="div_password"]').show();
    $('[id="div_password2"]').show();

    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Administrador'); // Set Title to Bootstrap modal title
}

function edit_usuario(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Usuario/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="username"]').val(data.username);
            $('[name="nombre"]').val(data.nombre);
            $('[name="apellido"]').val(data.apellido);
            $('[name="email"]').val(data.email);
           
            // Si estamos editando, oculta los campos 'password'
            $('[id="div_password"]').hide();
            $('[id="div_password2"]').hide();

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Administrador'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
             aviso('danger', textStatus, "ERROR AL CARGAR DATOS DE USUARIO"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Usuario/ajax_add";
        mensaje = 'Usuario creado.';
    } else {
        url = "<?php echo BASE_PATH ?>/admin/Usuario/ajax_update";
        mensaje = 'Usuario modificado.';
    }


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           
            aviso('danger', textStatus, 'Error al crear o modificar Administrador (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_usuario(nombre, id)
{
    if(confirm('¿Eliminar Usuario "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Usuario/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', 'Usuario eliminado.');  
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR ELIMINANDO DATOS DE USUARIO"); 
            }
        });

    }
}


function edit_password(id)
{

    $('#form_password')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('[name="user_id"]').val(id);
    $('#modal_form_password').modal('show'); 
    $('.modal-title').text('Modificar Password');
}


function save_password()
{
    $('#btnSave_Password').text('Guardando...'); //change button text
    $('#btnSave_Password').attr('disabled',true); //set button disable 
    
    var url;

    url = "<?php echo BASE_PATH ?>/admin/Usuario/ajax_update_password";


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_password').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_password').modal('hide');
                reload_table();
                aviso('success',  'Password modificada.'); 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
             aviso('danger', textStatus, 'Error al crear o modificar Password (' + errorThrown + ')'); 
            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 

        }
    });
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Usuario Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- USUARIO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Usuario</label>
                            <div class="col-md-9">
                                <input name="username" placeholder="Usuario" class="form-control" type="text" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- APELLIDO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido</label>
                            <div class="col-md-9">
                                <input name="apellido" placeholder="Apellido" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- EMAIL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">E-mail</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="E-mail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- PASSWORD --> 
                        <div class="form-group" id="div_password">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-4">
                                <input name="password" placeholder="Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-5">
                                <input name="password2" placeholder="Repita Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>                            
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    



<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_password" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Modificar Contraseña</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_password" class="form-horizontal">
                    <input type="hidden" value="" name="user_id"/> 
                    <div class="form-body">

                        <!-- CONTRASEÑA ACTUAL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Password Actual</label>
                            <div class="col-md-9">
                                <input name="password_actual" placeholder="Password Actual" class="form-control" type="password" required>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nueva password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Nueva password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- REPITE NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Repita password</label>
                            <div class="col-md-9">
                                <input name="password2" placeholder="Repita password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_Password" onclick="save_password()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->