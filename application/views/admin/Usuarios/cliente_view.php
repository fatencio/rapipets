<!-- Page Header -->
<div class="content bg-gray-lighter header-pagina">
    <div class="row items-push">
        <div class="col-sm-7">
            <h1 class="page-heading">
                Clientes
            </h1>
        </div>
        <div class="col-sm-5 text-right hidden-xs">
            <ol class="breadcrumb push-10-t">
                <li>Inicio</li>
                <li><a class="link-effect" href="javascript:void(0);" onclick="return loadController('admin/Cliente/index');">Clientes</a></li>
            </ol>
        </div>
    </div>
</div>
<!-- END Page Header -->

<div class="content">
    <div class="block">
        <div class="block-header">
            <button class="btn btn-success" onclick="add_cliente()"><i class="glyphicon glyphicon-plus"></i> Nuevo Cliente</button>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Recargar</button>
        </div>

        <div class="block-content">
            <table id="table" class="table table-bordered table-striped js-dataTable-full"  cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Telefono</th>
                        <th>EMail</th>
                        <th>Avatar</th>                      
                        <th style="min-width:156px;">Acción</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>   
    </div>         
</div>

<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Cliente/ajax_list",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false,
        },    
        { 
            "targets": [ 4 ], 
            "className": "hidden-xs hidden-sm hidden-md",       
        },              
        {
            "targets": [2,3],
            "className": "hidden-xs",       
        }  
        ],

    });


    //set input/textarea/select event when change value, remove class error and remove text help block 
    $("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("textarea").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });

});



function add_cliente()
{
    save_method = 'add';

    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'cliente/miniaturas/cliente_avatar_mini.png' ?>');  
    $('#imagen').val('cliente_avatar_mini.png');

    // Si estamos creando un cliente, muestra los campos 'password'
    $('[id="div_password"]').show();
    $('[id="div_password2"]').show();

    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Nuevo Cliente'); // Set Title to Bootstrap modal title
}

function edit_cliente(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Cliente/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id"]').val(data.id);
            $('[name="nombre"]').val(data.nombre);

            $('[name="apellido"]').val(data.apellido);
            $('[name="telefono"]').val(data.telefono);
            $('[name="email"]').val(data.email);
            $('[name="direccion"]').val(data.direccion);
            $('[name="localidad"]').val(data.localidad);
            $('[name="provincia"]').val(data.id_provincia);
            $('[name="imagen"]').val(data.avatar);  

            $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'cliente/miniaturas/' ?>'  + data.avatar);  

            // Si estamos editando, oculta los campos 'password'
            $('[id="div_password"]').hide();
            $('[id="div_password2"]').hide();

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Editar Cliente'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS"); 
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('Guardando...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
    var mensaje;

    if(save_method == 'add') {
        url = "<?php echo BASE_PATH ?>/admin/Cliente/ajax_add";
         mensaje = 'Cliente creado.';

    } else {
        url = "<?php echo BASE_PATH ?>/admin/Cliente/ajax_update";
         mensaje = 'Cliente modificado.';
    }


    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,           
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                reload_table();
                aviso('success', mensaje); 
            }
            else
            {
                // Sesion expirada
                if (data.login) 
                {
                    window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                }
                else
                {                
                    for (var i = 0; i < data.inputerror.length; i++) 
                    {
                        $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                        $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                    }
                }
            }
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al crear o modificar Cliente (' + errorThrown + ')'); 
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

function delete_cliente(nombre, id)
{
    if(confirm('¿Eliminar Cliente "' + nombre + '"?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Cliente/ajax_delete_full/" + id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {   
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                    
                    aviso('success', 'Cliente eliminado.');                 
                }
                else
                {
                    // Sesion expirada
                    if (data.login) 
                    {
                        window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
                    }
                    else
                    {
                        aviso('danger', data.mensaje, "ERROR AL ELIMINAR CLIENTE");  
                        console.log(data.error);
                    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                 aviso('danger', textStatus, "ERROR ELIMINANDO DATOS CLIENTE");  
                 console.log(textStatus);
            }
        });

    }
}


function edit_password(id)
{

    $('#form_password')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    $('[name="cliente_id"]').val(id);
    $('#modal_form_password').modal('show'); 
    $('.modal-title').text('Modificar Password');
}


function save_password()
{
    $('#btnSave_Password').text('Guardando...'); //change button text
    $('#btnSave_Password').attr('disabled',true); //set button disable 
    
    var url;

    url = "<?php echo BASE_PATH ?>/admin/Cliente/ajax_update_password";


    // ajax adding data to database
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_password').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_password').modal('hide');
                reload_table();
                aviso('success', 'Password modificada.'); 
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          
            aviso('danger', data.mensaje, 'Error al modificar password (' + errorThrown + ')');  

            $('#btnSave_Password').text('Guardar'); //change button text
            $('#btnSave_Password').attr('disabled',false); //set button enable 

        }
    });
}


function switch_cliente(id){

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Cliente/ajax_activar_desactivar/" + id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            if (data.status)
            {
                if (data.nuevo_status == 1)
                    aviso('success', 'Cliente habliitado.');  
                else
                    aviso('success', 'Cliente deshabliitado.');  
            }
            else
            {
                // Sesion expirada
                if (data.login) window.top.location.href = "<?php echo BASE_PATH ?>/admin";                
            }
        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            console.log("errorThrown: " + errorThrown);
            aviso('danger', textStatus, 'Error al activar / desactivar.'); 
        }
    });

}

function mostrarImagenes()
{
    $('#modal_imagenes').modal('show');
}


function elegirImagen(nombreImagen)
{
    $('#imagenPreview').attr('src', '<?php echo IMG_PATH . 'cliente/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen').val(nombreImagen);

    $('#modal_imagenes').modal('hide');
}


function eliminarImagen(nombre)
{
    if(confirm('¿Eliminar imagen "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Cliente/delete_imagen/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     aviso('success', 'Imagen eliminada.');    
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
            }
        });
    }
}
</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Cliente Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-9">
                                <input name="nombre" placeholder="Nombre" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- APELLIDO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Apellido</label>
                            <div class="col-md-9">
                                <input name="apellido" placeholder="Apellido" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- EMAIL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">E-Mail</label>
                            <div class="col-md-9">
                                <input name="email" placeholder="E-Mail" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- TELEFONO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Teléfono</label>
                            <div class="col-md-9">
                                <input name="telefono" placeholder="Teléfono" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- DIRECCION --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Dirección</label>
                            <div class="col-md-9">
                                <input name="direccion" placeholder="Calle y Número" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>

                             <label class="control-label col-md-3"></label>

                            <div class="col-md-4">
                                <input name="localidad" placeholder="Localidad" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>     

                            <div class="col-md-5">
                                <select name="provincia" id="provincia" class="form-control" >
                                    <option selected value="0">Seleccione Provincia...</option>
                                    <?php 
                                    foreach($provincias as $provincia){ ?>
                                        <option value="<?php echo $provincia->id ?>" ><?php echo $provincia->nombre ?></option> 
                                    <?php } ?>    
                                </select>  
                                <span class="help-block"></span>
                            </div>                                                     
                        </div>

                        <!-- PASSWORD --> 
                        <div class="form-group" id="div_password">
                            <label class="control-label col-md-3">Password</label>
                            <div class="col-md-4">
                                <input name="password" placeholder="Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-5">
                                <input name="password2" placeholder="Repita Password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>                            
                        </div>

                         <!-- IMAGEN --> 
                        <div class="form-group">
                            <label class="control-label col-md-2">Imagen</label>
                            <div class="col-md-10">
                                <button type="button" id="btnElegirImagen" onclick="mostrarImagenes()" class="btn btn-default pull-right">o elegir una imagen del sitio</button>    

                                <input name="foto" type="file" id="foto" size="40" class="btn btn-default pull-left" >  
                                <div class="clearfix"></div>  
                                <input type="hidden" value="cliente_avatar_mini.png" id="imagen" name="imagen" />
                                <span class="help-block"></span>
                                
                                <div class="clearfix"></div>    
                                <br/>                                                       
                                <img class="centrado" src="<?php echo IMG_PATH . 'cliente/miniaturas/cliente_avatar_mini.png' ?>" id="imagenPreview" /> 
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->    

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_password" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Modificar Contraseña</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_password" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="cliente_id"/> 
                    <div class="form-body">

                        <!-- NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nueva password</label>
                            <div class="col-md-9">
                                <input name="password" placeholder="Nueva password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <!-- REPITE NUEVA CONTRASEÑA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Repita password</label>
                            <div class="col-md-9">
                                <input name="password2" placeholder="Repita password" class="form-control" type="password">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_Password" onclick="save_password()" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal --> 

<!-- Bootstrap modal de imagenes -->
<div class="modal fade" id="modal_imagenes" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del sitio</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/cliente/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->   