<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>GeoTienda</title>

        <meta name="description" content="GeoTienda">
        <meta name="author" content="Francisco Atencio">
        <meta name="robots" content="noindex, nofollow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon.png">

        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-192x192.png" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick-theme.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.dataTables.min.css">

        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo BASE_PATH ?>/assets/css/oneui.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" id="css-geotienda" href="<?php echo BASE_PATH ?>/assets/custom/css/geotienda.css">

        <!-- Custom plugins CSS -->
        <link rel="stylesheet" id="css-spectrum" href="<?php echo BASE_PATH ?>/assets/custom/css/spectrum.css">        

        <!-- END Stylesheets -->

        <!-- Librerias GeoTienda v3 -->    
        <?php include 'librerias_js.php'; ?>          
    </head>
    <body>  
        <div id="page-loader"></div>
        <!-- Page Container -->
        <!--
            Available Classes:

            'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
        -->

        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

            <!-- Menu Principal -->
            <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="side-header side-content bg-white-op">
                            <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                            <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                                <i class="fa fa-times"></i>
                            </button>
                            <a class="h5 text-white" href="<?php echo BASE_PATH ?>/admin/">
                                <span class="h4 font-w600 sidebar-mini-hide">GeoTienda Admin</span>
                            </a>
                        </div>
                        <!-- END Side Header -->

                        <!-- Side Content -->
                        <div class="side-content">
                            <ul class="nav-main">
                               
                               <!-- LOCALES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Local/index');"><i class="si si-home"></i><span class="sidebar-mini-hide">Locales</span></a>
                                </li>   
                                <li >
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Venta/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Ventas</span></a>
                                </li>    

                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Turno/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Turnos</span></a>
                                </li> 
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Servicio/index');"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;<span class="sidebar-mini-hide">Servicios</span></a>
                                </li>      
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Servicio_Item/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Items Servicio</span></a>
                                </li>  
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Banner/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Banners</span></a>
                                </li>  
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Mediopago/index');"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;<span class="sidebar-mini-hide">Medios de Pago</span></a>
                                </li>      
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Mediopago_Item/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Items Medio de Pago</span></a>
                                </li>                                
                                <li style="height:6px;"></li>  

                                <!-- CLIENTES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Cliente/index');"><i class="si si-users"></i><span class="sidebar-mini-hide">Clientes</span></a>
                                </li>    
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Valoracion/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Comentarios</span></a>
                                </li>  
                                <li style="height:6px;">

                                <!-- ARTICULOS -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Articulo/index');"><i class="si si-handbag"></i><span class="sidebar-mini-hide">Artículos GeoTienda</span></a>
                                </li>    
                                <li>
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Articulo/index_locales');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Artículos Locales</span></a>
                                <li>                                                                                                              
                                <li>
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Rubro/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Rubros</span></a>
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Presentacion/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Presentaciones</span></a>
                                </li>
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Marca/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Marcas</span></a>
                                </li>                                                  

                                <li style="height:10px;">

                                <!-- ANIMALES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Animal/index');"><i class="fa fa-paw"></i><span class="sidebar-mini-hide">Animales</span></a>
                                </li>   
                                <li>
                                    <a style="padding-top: 0 !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Tamanio/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Tamaños</span></a>
                                </li>   
                                <li>
                                    <a style="padding-top: 3px !important; padding-bottom: 3px !important" href="javascript:void(0);" onclick="return loadController('admin/Raza/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Razas</span></a>
                                </li>                                                

                                <li style="height:6px;">

     
                                <!-- DESCUENTOS -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Descuento/index');"><i class="si si-tag"></i>Descuentos</a>
                                </li> 


                                <li style="height:6px;">

                                <!-- NOTIFICACIONES y RECORDATORIOS -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Notificacion/index');"><i class="si si-bell"></i>Notificaciones</a>
                                </li>  

                                <!--
                                <li>
                                    <a style="padding-top: 3px !important; " href="javascript:void(0);" onclick="return loadController('admin/Recordatorio/index');"><i></i><span class="sidebar-mini-hide">&nbsp;&nbsp;&nbsp;&nbsp;Recordatorios</span></a>
                                </li>  
                                -->
                                
                                <!-- ADMINISTRADORES -->
                                <li>
                                    <a href="javascript:void(0);" onclick="return loadController('admin/Usuario/index');"><i class="si si-user"></i><span class="sidebar-mini-hide">Administradores</span></a>
                                </li>  
                            </ul>
                        </div>
                        <!-- END Side Content -->
                    </div>
                    <!-- Sidebar Content -->
                </div>
                <!-- END Sidebar Scroll Container -->
            </nav>
            <!-- END Menu Principal -->

            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">

                <!-- Nro de notificaciones nuevas -->
                <span class="badge badge-danger pull-right count-notificaciones" id="count_notificaciones"></span>

                <!-- Header Navigation Right -->
                <ul class="nav-header pull-right">

                    <!-- Usuario -->
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-image dropdown-toggle" data-toggle="dropdown" type="button">
                                <?php echo $usuario ?> 
                                <img src="<?php echo BASE_PATH ?>/assets/img/backend/admin_avatar.jpg" alt="Avatar">

                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <li>
                                    <a tabindex="-1" href="javascript:void(0);" onclick="return loadController('admin/Inicio/logout');">
                                        <i class="si si-logout pull-right"></i>Salir
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!-- FIN Usuario -->

                    <!-- Notificaciones -->
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button" id="btn_notificaciones">
                                <i class="fa fa-bell"></i>
                            </button>
                            <ul class="dropdown-menu dropdown-menu-right list list-activity box-notificaciones" id="lista_notificaciones">                                                                
                            </ul>
                        </div>
                    </li>
                    <!-- FIN Notificaciones -->
                </ul>
                <!-- END Header Navigation Right -->

                <!-- Header Navigation Left -->
                <ul class="nav-header pull-left">
                    <li class="hidden-md hidden-lg">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_toggle" type="button">
                            <i class="fa fa-navicon"></i>
                        </button>
                        <a class="text-black" href="index.php" style="float:right; padding-left: 6px; width: 50px">
                            <span >GeoTienda</span>
                        </a>                             
                    </li>                 
                    <li class="hidden-xs hidden-sm">
                        <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                        <button class="btn btn-default" data-toggle="layout" data-action="sidebar_mini_toggle" type="button">
                            <i class="fa fa-ellipsis-v"></i>
                        </button>
                    </li>




                </ul>
                <!-- END Header Navigation Left -->
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            <footer id="page-footer" class="content-mini content-mini-full font-s12 bg-gray-lighter clearfix">
                <div class="pull-right">
                    Desarrollado por <a class="font-w600" href="" target="_blank">Francisco Atencio</a>
                </div>
                <div class="pull-left">
                    <a class="font-w600" href="https://goo.gl/6LF10W" target="_blank">GeoTienda 1.0</a> &copy; <span class="js-year-copy"></span>
                </div>
            </footer>
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->

        <!-- Error no manejado Modal -->
        <div class="modal fade" id="modal_error_sistema" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="text-center text-naranja">Se ha producido un error</h3>
                            <br/>
                            <h4 class="font-w300 text-center">Por favor, intentá la operación nuevamente.</h4>
                            <br/><br/>

                            <div class="text-center">
                                <button class="btn btn-lg btn-geotienda" type="button" data-dismiss="modal">Aceptar</button>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- FIN Error no manejado Modal -->

        <!-- MODAL RESULTADO OK -->
        <div class="modal fade" id="modal_resultado_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="font-w300 text-center resaltado-naranja" id="resultado_ok_titulo1"></h3>
                            <br/><br/>
                            <h4 class="font-w300 text-center" id="resultado_ok_titulo2"></h4>
                            <br/><br/>
                            </hr>
                            <div class="text-center">
                                <button class="btn btn-lg btn-geotienda" type="button" data-dismiss="modal">Aceptar</button>
                            </div>
                            <br/>

                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- fin MODAL RESULTADO OK -->   

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.scrollLock.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.appear.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.countTo.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.placeholder.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/js.cookie.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/app.js"></script>

        <!-- Page Plugins -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/chartjs/Chart.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>
        <!--<script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.flash.min.js"></script>-->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/buttons.html5.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/dataTables.buttons.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/plugins/excel/jszip.min.js"></script>

        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>

        <!-- Custom Plugins -->
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.remote.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.cascadingdropdown.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/spectrum.js"></script>    
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/bootstrap-switch.js"></script>    
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyC0mza7rGNlOmcKg-5JF1NB3nUKqHFYMBk&sensor=false&v=3&libraries=geometry"></script>
        
        <!-- Page JS Code -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/pages/base_tables_datatables.js"></script> 

      
        <script>
            jQuery(function () {
                // Init page helpers
                // ckeditor --> Ritch text editor, usado en vista Articulos
                App.initHelpers('ckeditor');
                App.initHelpers('table-tools');
            });
        </script>

        <script type="text/javascript">

        var notificaciones_array;
        var table_detalle_venta;
        var table_comentarios_venta;
        var table_comentarios_turno;
        var venta;

        $(document).ready(function() {

            // Carga inicialmente lista de Locales
            loadController('admin/Local/index');

            // Verifica si hay nuevas notificaciones
            check_notificaciones();

            // Verifica si hay pagos por Mercadopago
        //    check_pagos_mp();

        });


        /* NOTIFICACIONES */

        // Verifica si hay nuevas notificaciones
        setInterval(check_notificaciones, <?php echo $this->config->item('intervalo_notificaciones'); ?>);


        function check_notificaciones(){

            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Notificacion/get_count_novistas_admin/",
              cache: false,
              success: function(data){
                if ($.isNumeric(data))
                    update_count_notificaciones(data);
                else
                    console.log('Error al actualizar notificaciones. ' + data);
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                console.log('Error al actualizar notificaciones. ' + textStatus);
              } 
            });
        }


        // Actualiza el contador de notificaciones
        function update_count_notificaciones(data){
            if (data != '0'){
                $("#count_notificaciones").show();
                $("#count_notificaciones").html(data);
                document.title =  '(' + data + ') GeoTienda';
            }
            else
            {
                $("#count_notificaciones").hide();
                document.title = 'GeoTienda'; 
            }
        }


        // Actualiza lista de notificaciones no vistas, y las marca como vistas
        $('#btn_notificaciones').click(function(e) {

            if (!$("#lista_notificaciones").is(":visible")) $("#count_notificaciones").hide();

            // Carga notificaciones recientes
            get_notificaciones_recientes();
        });


        function get_notificaciones_recientes(){
            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Notificacion/get_recientes/",
              cache: false,
              success: function(data){

                notificaciones_array = jQuery.parseJSON(data);

                muestra_notificaciones();

                // Marca notificaciones como vistas 
                set_notificaciones_vistas();                
              } 
            });
        }


        function set_notificaciones_vistas(){
            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Notificacion/set_vistas_admin/",
              cache: false,
              success: function(data){
                 update_count_notificaciones(data);
              } 
            });
        }


        function set_notificacion_leida(id){
            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Notificacion/set_leida_admin/" + id,
              cache: false,
              success: function(data){
               //  update_count_notificaciones(data);
              } 
            });
        }


        // Muestra lista de notificaciones no vistas
        function muestra_notificaciones()
        {
            var i;
            var clase_tipo = "";
            var clase_leida_admin = "";
            var link = "";

            $("#lista_notificaciones").empty();

            $("#lista_notificaciones").append('<li class="dropdown-header titulo-box-notificaciones">Notificaciones</li>');

            if (notificaciones_array.length > 0)
            {
                for (i = 0; i < notificaciones_array.length; ++i) {

                    // Estilo y link de las notificaciones segun tipo
                    switch (notificaciones_array[i][1]) { 
                        case 'calificación venta': 
                        case 'calificación turno': 
                            clase_tipo = "fa fa-paw text-warning";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;
                        case 'aviso venta': 
                        case 'venta': 
                            clase_tipo = "si si-handbag text-default";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                            break;
                        case 'mensaje venta': 
                            clase_tipo = "fa fa-comment-o text-default";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                            break;                            
                        case 'registro': 
                            clase_tipo = "glyphicon glyphicon-edit text-info";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;      
                        case 'aviso turno': 
                        case 'turno': 
                            clase_tipo = "si si-calendar text-success";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "')\";";
                            break;
                        case 'mensaje turno': 
                            clase_tipo = "fa fa-comment-o text-default";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "')\";";
                            break;                            
                        case 'cancela turno': 
                            clase_tipo = "si si-calendar text-warning";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "')\";";
                            break;    
                        case 'cancela venta': 
                            clase_tipo = "si si-handbag text-warning";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                            break;                                                      
                        case 'adhesión': 
                            clase_tipo = "si si-user-follow text-warning";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;       
                        case 'consulta': 
                            clase_tipo = "si si-envelope text-default";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;    
                        case 'mascota asignada':
                            clase_tipo = "si si-heart text-danger ";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;
                        case 'destacar':
                            clase_tipo = "si si-arrow-up text-success";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                            break;                             
                        default:
                            clase_tipo = "si si-calendar text-success ";
                            link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); return loadController(\'admin/Notificacion/index\')\";";
                    }


                    // Estilo de las notificaciones no leidas
                    if (notificaciones_array[i][3] == 0)
                        clase_leida_admin = "notificacion_no_leida";
                    else
                        clase_leida_admin = "notificacion_leida";

                    $("#lista_notificaciones").append(
                        '<li class="' + clase_leida_admin + '"><a href="javascript:void(0);" ' + link + ' ><i class="' + clase_tipo + ' detalle-notificacion"></i>' +
                        '<div class="font-w600" >' + notificaciones_array[i][2] + '</div>' +
                        '<div><small>' + notificaciones_array[i][0] + '</small></div></a></li>');

                }
            }
            else{
                $("#lista_notificaciones").append(
                    '<li class="no-hay-notificaciones"><span class="font-w200 font-s13 text-gray-darker">' + 
                     
                     'No hay notificaciones</span>' + 
                     '</li>');                   
            }
            
            $("#lista_notificaciones").append(
                '<li class="notificaciones-ver-todas"><span class="font-w600 font-s13 text-gray-darker">' + 
                 '<a href="javascript:void(0);" onclick="return loadController(\'admin/Notificacion/index\');" >' + 
                 'Ver Todas</a></span>' + 
                 '</li>');   
            
        }
        /* FIN NOTIFICACIONES */


        /* MERCADOPAGO */

        // Consulta si hay pagos realizados por Mercadopago
        function check_pagos_mp(){

            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Venta/check_pagos_mp/",
              cache: false,
              success: function(data){

                    console.log(data);
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                console.log('Error al buscar pagos por Mercadopago. ' + textStatus);
              } 
            });
        }

        /* FIN MERCADOPAGO */

        /* DATOS DE UNA VENTA (se llama desde Ventas y Notificaciones) */
        function detalle_venta(venta_id)
        {

            // Encabezado
            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Venta/get_venta_by_id/" + venta_id,
              cache: false,
              success: function(data){

                venta = jQuery.parseJSON(data);

                // Datos de la venta
                $('.modal-title-venta-nro').text('Venta ' + venta.numero + '  -  ' + venta.fecha  + ' (' + venta.estado + ')');
                $('#modal_detalle_venta').modal('show'); 
                $('#venta_condicion_venta').text(venta.condicion_venta);
                $('#venta_estado').text(venta.estado);
                
                // Datos del local
                $('#venta_local').text(venta.local);
                $('#venta_titular_local').text(venta.local_nombre_titular + ' ' + venta.local_apellido_titular);
                $('#venta_telefono_local').text(' ' + venta.local_telefono);
                $('#venta_email_local').text(' ' + venta.local_email);

                // Datos del cliente
                $('#venta_cliente').text(venta.cliente);
                $('#venta_telefono_cliente').text(' ' + venta.cliente_telefono);
                $('#venta_email_cliente').text(' ' + venta.cliente_email);

                // Datos adicionales
                $('#venta_tipo_envio').text(venta.tipo_envio);

                if (venta.tipo_envio == 'A domicilio')
                {
                    $('#div_venta_cliente_direccion').show();
                    $('#div_venta_cliente_ciudad').show();
                    $('#venta_cliente_direccion').text(venta.cliente_direccion);
                    $('#venta_cliente_ciudad').text(venta.cliente_localidad + ', ' +  venta.cliente_provincia);        
                }
                else
                {
                    $('#div_venta_cliente_direccion').hide();
                    $('#div_venta_cliente_ciudad').hide();
                }

                $('#venta_observaciones').text(venta.observaciones);


                // CALIFICACIONES
                $('#venta_calificacion_cliente').text("no ha calificado");
                $('#venta_valoracion_cliente').text("no ha calificado"); 
                $('#venta_calificacion_local').text("no ha notificado");
                $('#venta_valoracion_local').text("no ha notificado");    
                $('#venta_valoracion_aprobada').text(" (aprobada)");    

                $('#venta_puntaje_cliente').html('');              

                if (venta.concretado_cliente == '' && venta.concretado_local == '')
                {
                    $('#div_calificaciones_venta').hide();
                }
                else
                {
                    $('#div_calificaciones_venta').show();

                    // Calificación cliente
                    if (venta.concretado_cliente != '')
                    {
                        $('#venta_valoracion_aprobada').show();

                        if (venta.valoracion_aprobada)
                            $('#venta_valoracion_aprobada').text(" (aprobada)");    
                        else
                            $('#venta_valoracion_aprobada').text(" (no aprobada)"); 

                        if (venta.concretado_cliente == 'si')
                            $('#venta_calificacion_cliente').text("concretado");
                        else
                            $('#venta_calificacion_cliente').text("NO concretado");    

                        $('#venta_puntaje_cliente').html(venta.puntaje);                
                        $('#venta_valoracion_cliente').text(venta.valoracion);                
                    }
                    else
                    {
                        $('#venta_puntaje_cliente').html('');
                        $('#venta_calificacion_cliente').text("no ha calificado");
                        $('#venta_valoracion_cliente').text("no ha calificado");  
                        $('#venta_valoracion_aprobada').hide();
                    }
                     
                    // Notificación local
                    if (venta.concretado_local != '')
                    {
                        if (venta.concretado_local == 'si')
                            $('#venta_calificacion_local').text("concretado");
                        else
                            $('#venta_calificacion_local').text("NO concretado");     

                        $('#venta_valoracion_local').text(venta.valoracion_local);  
                    }
                    else
                    {
                        $('#venta_calificacion_local').text("no ha calificado");
                        $('#venta_valoracion_local').text("no ha calificado");  
                    }                     
                }

                // PEDIDO CANCELADO
                if (venta.estado == 'cancelado')
                {
                    $('#div_pedido_cancelado').show();
                    $('#venta_cancelado_por').text(venta.cancelado_por);  
                    $('#venta_cancelado_comentario').text(venta.cancelado_comentario);  
                }
                else
                {
                    $('#div_pedido_cancelado').hide();
                }
              } 
            });

            // Detalle
            table_detalle_venta = $('#table_detalle_venta').DataTable({ 

                "lengthChange": false, 
                "paging": false,
                "filter": false,
                "ordering": false,
                "info": false,

                "destroy": true,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_list_detalle/" + venta_id,
                    "type": "POST"
                },

                //Set column definition initialisation properties.
                "columnDefs": [
                { 
                    "targets": [ 0, -1 ], // first and last column
                    "orderable": false, //set not orderable
                },
                { 
                    "className": "text-right", "targets": [4] 
                }
                ],

                // Suma total del precio de cada detalle
                footerCallback: function ( row, data, start, end, display ) {
                    var api = this.api(), data;
         
                    // Remove the formatting to get integer data for summation
                    var intVal = function ( i ) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '')*1 :
                            typeof i === 'number' ?
                                i : 0;
                    };
            
                    // Total over all pages
                    total = api
                        .column( 4 )
                        .data()
                        .reduce( function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0 );
         
                    // Update footer
                    $( api.column( 4 ).footer() ).html('$'+ total);
                }

            });  

            // COMENTARIOS
            table_comentarios_venta = $('#table_comentarios_venta').DataTable({ 

                "lengthChange": false, 
                "paging": false,
                "filter": false,
                "ordering": false,
                "info": false,

                "destroy": true,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_comentarios_list/" + venta_id,
                    "type": "POST"
                },

            });            

        }
        /* FIN DETALLE DE UNA VENTA */


        /* DETALLE DE UN TURNO (se llama desde Turnos y Notificaciones) */
        function detalle_turno(turno_id)
        {

            $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/turno/get_by_id/" + turno_id,
              cache: false,
              success: function(data){

                turno = jQuery.parseJSON(data);

                // Datos del turno
                $('.modal-title-turno-nro').text('Turno ' + turno.numero + ' (' + turno.estado + ')');
                $('#modal_detalle_turno').modal('show'); 

                $('#turno_estado').text(turno.estado);

                $('#turno_servicio').text(turno.servicio  + ' - ' + turno.servicio_item  + ' ($' + turno.total + ')');
                $('#turno_fecha').text(turno.fecha + ' a las ' + turno.hora + ' hs');

                // Datos del local
                $('#turno_local').text(turno.local);
                $('#turno_titular_local').text(turno.local_nombre_titular + ' ' + turno.local_apellido_titular);
                $('#turno_telefono_local').text(' ' + turno.local_telefono);
                $('#turno_email_local').text(' ' + turno.local_email);

                // Datos del cliente
                $('#turno_cliente').text(turno.cliente);
                $('#turno_telefono_cliente').text(' ' + turno.cliente_telefono);
                $('#turno_email_cliente').text(' ' + turno.cliente_email);

                // Datos adicionales
                $('#turno_tipo_envio').text(turno.tipo_envio);
                $('#turno_cliente_direccion').text(turno.cliente_direccion);
                $('#turno_cliente_ciudad').text(turno.cliente_localidad + ', ' +  turno.cliente_provincia);
                $('#turno_observaciones').text(turno.observaciones);

                // CALIFICACIONES
                $('#turno_calificacion_cliente').text("no ha calificado");
                $('#turno_valoracion_cliente').text("no ha calificado"); 
                $('#turno_calificacion_local').text("no ha notificado");
                $('#turno_valoracion_local').text("no ha notificado");    
                $('#turno_valoracion_aprobada').text("aprobada");    

                $('#turno_puntaje_cliente').html('');              

                if (turno.concretado_cliente == '' && turno.concretado_local == '')
                {
                    $('#div_calificaciones_turno').hide();
                }
                else
                {
                    $('#div_calificaciones_turno').show();

                    // Calificación cliente
                    if (turno.concretado_cliente != '')
                    {
                        $('#turno_valoracion_aprobada').show();

                        if (turno.valoracion_aprobada)
                            $('#turno_valoracion_aprobada').text("aprobada");    
                        else
                            $('#turno_valoracion_aprobada').text("no aprobada");    

                        if (turno.concretado_cliente == 'si')
                            $('#turno_calificacion_cliente').text("concretado");
                        else
                            $('#turno_calificacion_cliente').text("NO concretado");    

                        $('#turno_puntaje_cliente').html(turno.puntaje);                
                        $('#turno_valoracion_cliente').text(turno.valoracion);                
                    }
                    else
                    {
                        $('#turno_puntaje_cliente').html('');
                        $('#turno_calificacion_cliente').text("no ha calificado");
                        $('#turno_valoracion_cliente').text("no ha calificado");  
                        $('#turno_valoracion_aprobada').hide();
                    }
                     
                    // Notificación local
                    if (turno.concretado_local != '')
                    {
                        if (turno.concretado_local == 'si')
                            $('#turno_calificacion_local').text("concretado");
                        else
                            $('#turno_calificacion_local').text("NO concretado");     

                        $('#turno_valoracion_local').text(turno.valoracion_local);  
                    }
                    else
                    {
                        $('#turno_calificacion_local').text("no ha calificado");
                        $('#turno_valoracion_local').text("no ha calificado");  
                    }                     
                }


                // Turno cancelado
                if (turno.estado == 'cancelado')
                {
                    $('#div_turno_cancelado_por').show();
                    $('#div_turno_cancelado_motivo').show();
                    $('#turno_cancelado_por').text(turno.cancelado_por);
                    $('#turno_cancelado_comentario').text(turno.cancelado_comentario);          
                }
                else
                {
                    $('#div_turno_cancelado_por').hide();
                    $('#div_turno_cancelado_motivo').hide();
                }  
              } 
            });

            // COMENTARIOS
            table_comentarios_turno = $('#table_comentarios_turno').DataTable({ 

                "lengthChange": false, 
                "paging": false,
                "filter": false,
                "ordering": false,
                "info": false,

                "destroy": true,
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo BASE_PATH ?>/admin/Turno/ajax_comentarios_list/" + turno_id,
                    "type": "POST"
                },

            });  
        }
        /* FIN DETALLE DE UN TURNO */


        /* AVISOS notify */
        function aviso(tipo, mensaje, titulo){

            var icono;

            switch(tipo){
                case 'success':
                    icono = 'fa fa-check';
                break;

               case 'danger':
                    icono = 'fa fa-times';
                break;                
            }

            $.notify({
                // options
                icon: icono,
                title: titulo,
                message: mensaje 
            },{
                // settings
                type: tipo,
                placement: {
                    from: "top",
                    align: "center"
                },    
                template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<span data-notify="title" style="margin-bottom:8px;" class="clearfix">{1}</span> ' +
                    '<span data-notify="icon"></span> ' +                    
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'                     
            });   
        }
        /* FIN AVISOS notify */

        /* Refresco invisible de la sesion */
        var refreshTime = 300000; // cada 5 minutos
        setInterval( function() {
            $.ajax({
                cache: false,
                type: "GET",
                url: "../refreshSession.php",
                success: function(data) {
                }
            });
        }, refreshTime );        
        </script>

        <!-- Bootstrap modal DETALLE VENTA (se llama desde Ventas y Notificaciones) -->
        <div class="modal fade" id="modal_detalle_venta" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content modal-grande">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="text-left">
                            <h3 class="modal-title-venta-nro"> Venta</h3>
                        </div>
                    </div>
                    <div class="modal-body">
                        <!-- ENCABEZADO VENTA -->

                        <div class="col-lg-6">
                            <!-- Datos Local -->
                            <div class="block-content detalle-venta">
                                <div class="h4 push-5">Local: <b id="venta_local"></b></div>
                                <address>
                                    Titular: <b id="venta_titular_local"></b>                             
                                    &nbsp;&nbsp;<i class="fa fa-phone"></i><span id="venta_telefono_local"></span><br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><span id="venta_email_local"></span></a>
                                </address>                                
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- Datos Cliente -->
                            <div class="block-content detalle-venta">
                                <div class="h4 push-5">Cliente: <b id="venta_cliente"></b></div>
                                <address>
                                    <i class="fa fa-phone"></i><span id="venta_telefono_cliente"></span><br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><span id="venta_email_cliente"></span></a>
                                </address>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="col-lg-6">
                            <!-- Forma de pago -->
                            <div class="block-content detalle-venta">
                                <div class="h5 push-5">Forma de pago: <b id="venta_condicion_venta"></b></div>
                                <div class="h5 push-5">Estado: <b id="venta_estado"></b></div>                             
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- Calificaciones cliente y local -->
                            <div class="block-content detalle-venta">
                                <div class="h5 push-5">Cliente calificó: <b id="venta_calificacion_cliente">no ha calificado</b></div>
                                <div class="h5 push-5">Local notificó: <b id="venta_calificacion_local">no ha notificado</b></div>                             
                            </div>
                        </div>

                        <!-- Comentarios por pedido cancelado -->
                        <div class="col-lg-12" id="div_pedido_cancelado">
                            <div class="block-content detalle-venta">                                
                                <div class="h5 espacio-8">Cancelado por: <b id="venta_cancelado_por"></b></div>
                                <div class="h5 espacio-8">Motivo: <b id="venta_cancelado_comentario"></b></div>
                            </div>
                        </div>  

                        <!-- DETALLES VENTA -->
                        <div class="block-content detalle-venta">
                            <table id="table_detalle_venta" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Artículo</th>
                                        <th>Presentación</th>
                                        <th>Cantidad</th>
                                        <th  style="min-width:100px;">Precio U.</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th class="text-right" colspan="4">Total</th>
                                        <th class="text-right"></th>
                                    </tr>
                                </tfoot>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>   

                        <!-- Datos adicionales -->
                        <div class="block">
                            <div class="block-content detalle-venta">
                                <div class="h4 push-5">Datos adicionales</div>                                   
                                <div class="h5 espacio-8">Envío: <b id="venta_tipo_envio"></b></div>
                                <div class="h5 espacio-8" id="div_venta_cliente_direccion">Dirección: <b id="venta_cliente_direccion"></b></div>
                                <div class="h5 espacio-8" id="div_venta_cliente_ciudad">Ciudad: <b id="venta_cliente_ciudad"></b></div>
                            </div>
                        </div>

                        <!-- Detalle calificaciones cliente y local -->
                        <div class="block" id="div_calificaciones_venta">
                            <div class="block-content detalle-venta">                              
                                <div class="h5 espacio-8">Valoración cliente<b id="venta_valoracion_aprobada"></b>: <b id="venta_valoracion_cliente"></b></div>
                                <span id="venta_puntaje_cliente"></span>
                                <div class="h5 espacio-8 push-20-t">Valoración local: <b id="venta_valoracion_local"></b></div>
                            </div>
                        </div>                      

                        <!-- COMENTARIOS -->
                        <div class="block-content detalle-venta">
                            <div class="h4 push-5">Comentarios</div>
                            <table id="table_comentarios_venta" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:100px">Fecha</th>
                                        <th>Cliente / Local</th>
                                        <th>Comentario</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End Bootstrap modal -->       


        <!-- Bootstrap modal DETALLE TURNO (se llama desde Turnos y Notificaciones) -->
        <div class="modal fade" id="modal_detalle_turno" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content modal-grande">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="text-left">
                            <h3 class="modal-title-turno-nro"> Turno</h3>
                        </div>
                    </div>
                    <div class="modal-body">

                        <!-- Servicio y Fecha -->
                        <div class="block-content detalle-turno">
                            <div class="h4 push-5" style="margin-bottom:3px"><i class="fa fa-calendar"></i> <b id="turno_fecha"></b></div>
                            <div class="h5 push-5"></i> <b id="turno_servicio"></b></div>   
                        </div>

                        <div class="col-lg-6">
                            <!-- Datos Local -->
                            <div class="block-content detalle-turno">
                                <div class="h4 push-5">Local: <b id="turno_local"></b></div>
                                <address>
                                    Titular: <b id="turno_titular_local"></b>                             
                                    &nbsp;&nbsp;<i class="fa fa-phone"></i><span id="turno_telefono_local"></span><br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><span id="turno_email_local"></span></a>
                                </address>                                
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- Datos Cliente -->
                            <div class="block-content detalle-turno">
                                <div class="h4 push-5">Cliente: <b id="turno_cliente"></b></div>
                                <address>
                                    <i class="fa fa-phone"></i><span id="turno_telefono_cliente"></span><br>
                                    <i class="fa fa-envelope-o"></i> <a href="javascript:void(0)"><span id="turno_email_cliente"></span></a>
                                </address>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="block-content detalle-turno">
                                <div class="h5 push-5">Estado: <b id="turno_estado"></b></div> 
                                <div class="h5 espacio-8" id="div_turno_cancelado_por">Cancelado por: <b id="turno_cancelado_por"></b></div>
                                <div class="h5 espacio-8" id="div_turno_cancelado_motivo">Motivo: <b id="turno_cancelado_comentario"></b></div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <!-- Calificaciones cliente y local -->
                            <div class="block-content detalle-turno">
                                <div class="h5 push-5">Cliente calificó: <b id="turno_calificacion_cliente">no ha calificado</b></div>
                                <div class="h5 push-5">Local notificó: <b id="turno_calificacion_local">no ha notificado</b></div>                             
                            </div>
                        </div>

                        <div class="clearfix push-20"></div>

                        <!-- Datos adicionales -->
                        <div class="block" id="div_calificaciones_turno">
                            <div class="block-content detalle-venta">
                                <div class="h4 push-5">Datos adicionales</div>                                   
                                <div class="h5 espacio-8">Valoración cliente (<b id="turno_valoracion_aprobada"></b>): <b id="turno_valoracion_cliente"></b></div>
                                <span id="turno_puntaje_cliente"></span>
                                <div class="h5 espacio-8 push-20-t">Valoración local: <b id="turno_valoracion_local"></b></div>
                            </div>
                        </div>

                        <!-- COMENTARIOS -->
                        <div class="block-content detalle-venta">
                            <div class="h4 push-5">Comentarios</div>
                            <table id="table_comentarios_turno" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:100px">Fecha</th>
                                        <th>Cliente / Local</th>
                                        <th>Comentario</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- End Bootstrap modal -->              
    </body>
</html>