<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_landing')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>
            

<!-- BUSCADOR PRINCIPAL  Version Tablet y Laptop -->
<section class="portada-title hidden-xs" style="top: <?php echo $portada_top; ?>px ; ">
    <!-- Buscador Principal -->
    <div class="push-20 text-center">
        <h1 class="push-50 font-w600 font-42" style="color: <?php echo $portada_color; ?>">La manera más fácil de cuidar a tu mascota</h1>
        <h2 class="push-10 font-w400 font-26" style="color: <?php echo $portada_color; ?>">Buscá la tienda de mascotas o veterinaria más cercana a tu hogar</h2>
    </div>

    <div class="row text-center" style="max-width: 800px; margin: auto">
        <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local" method="get">
        </form>
          <div class="col-sm-5 push-5" tab-index="-1" id="div_dir">
                <input class="form-control input-lg input-buscar-local dir font-w600 font-20" type="text" id="dir" name="dir" placeholder="MI DIRECCION" required value="" tabindex="1">
                <div class="animated fadeInDown valida-buscador" id="valida_dir" style="display:none">Ingresá tu dirección</div>
          </div>
          <div class="col-sm-4 push-5" id="div_localidad" tab-index="-1">
                <select id="loc" name="loc" class="js-select2 form-control input-lg input-buscar-local font-w600 font-20" required data-placeholder="MI LOCALIDAD" tabindex="2">
                    <option></option>
                    <?php 
                        if ($localidades)
                            foreach($localidades as  $localidad){?>
                                <option value="<?php echo $localidad->nombre ?>" ><?php echo $localidad->nombre ?></option> 
                    <?php   } ?>      
                </select>  
                <div class="animated fadeInDown valida-buscador" id="valida_loc" style="display:none">Seleccioná tu localidad</div>
          </div>

          <div class="col-sm-3 push-5">
                <a class="btn btn-block btn-lg btn-geotienda" id="btn_buscar" onclick="buscar_locales();" tabindex="3">Buscar</a>
          </div>                      

    </div>
    <!-- FIN Buscador Principal -->
</section>  
<!-- FIN BUSCADOR PRINCIPAL  Version Tablet y Laptop -->

<!-- BUSCADOR PRINCIPAL  Version Mobile -->
<section class="portada-title visible-xs" style="top: <?php echo $portada_top_mobile; ?>px;">
    <!-- Buscador Principal -->
    <div class="push-20 text-center">
        <h4 class="push-10 font-w400 font-28"  style="color: <?php echo $portada_color; ?> !important">Buscá la tienda de mascotas o veterinaria más cercana a tu hogar</h4>
    </div>

    <div class="row text-center push-50" style="max-width: 800px; margin: auto">
      <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_m" method="get">
      </form>

          <div class="col-sm-5 push-5">
                <input class="form-control input-lg input-buscar-local-m dir" type="text" id="dir_m" name="dir" placeholder="MI DIRECCION" required value="" tabindex="4">
                <div class="animated fadeInDown valida-buscador" id="valida_dir_m" style="display:none">Ingresá tu dirección</div>                            
          </div>
          <div class="col-sm-4 push-5" id="div_localidad_m">
                <select id="loc_m" name="loc" class="js-select2 form-control input-lg input-buscar-local-m" required data-placeholder="MI LOCALIDAD" tabindex="5">
                    <option></option>
                    <?php 
                        if ($localidades)
                            foreach($localidades as  $localidad){?>
                                <option value="<?php echo $localidad->nombre ?>" ><?php echo $localidad->nombre ?></option> 
                    <?php   } ?>    
                </select>  
                <div class="animated fadeInDown valida-buscador" id="valida_loc_m" style="display:none">Seleccioná tu localidad</div>                            
          </div>

          <div class="col-sm-3 push-5">
            <button class="btn btn-block btn-lg btn-geotienda" id="btn_buscar" onclick="buscar_locales_mobile();">Buscar</button>
          </div>                      

    </div>
    <!-- FIN Buscador Principal -->
</section>   
<!-- fin BUSCADOR PRINCIPAL  Version Mobile -->


<!-- IMAGEN DE FONDO - Version Tablet y Laptop -->
<section class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);"class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada" class="text-center">
            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">   
        </div> 
    </div>
</section>
<!-- fin IMAGEN DE FONDO - Version Tablet y Laptop -->


<!-- IMAGEN DE FONDO -  Version Mobile  -->
<section class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);"class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada" class="text-center">
            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">   
        </div> 
    </div>
</section>
<!-- fin IMAGEN DE FONDO -  Version Mobile  -->


<!-- Como Funciona-->
<div class="background-landing-abajo" id="como">
    <div class="como-funciona text-center">

        
        <a href="#como">
            <h2 class="push-10 font-w600" style="color:#89AEDC">
                ¿Cómo funciona?
            </h2>
            <h3 class="glyphicon glyphicon-chevron-down push-30" style="color:#89AEDC"></h3>
        </a>
        

        <!--
        <div class="row items-push-3x push-20-t nice-copy">
            <div class="col-sm-3">
                <div class="text-center push-20">
                    <span class="item item-2x item-circle border-naranja">
                        <i class="si si-pointer"></i>
                    </span>
                </div>
                <h3 style="color: #ED1C24;" class="h5 font-w600 text-uppercase text-center push-30">Escribís tu dirección</h3>
            </div>

            <div class="col-sm-3">
                <div class="text-center push-20">
                    <span class="item item-2x item-circle border-naranja">
                        <i class="si si-home"></i>
                    </span>
                </div>
                <h3 style="color: #ED1C24;"  class="h5 font-w600 text-uppercase text-center push-30">Elegís un petshop o veterinaria</h3>
            </div>

            <div class="col-sm-3">
                <div class="text-center push-20">
                    <span class="item item-2x item-circle border-naranja">
                        <i class="si si-basket"></i>
                    </span>
                </div>
                <h3 style="color: #ED1C24;" class="h5 font-w600 text-uppercase text-center push-30">Hacés tu pedido</h3>
            </div>

            <div class="col-sm-3">
                <div class="text-center push-20">
                    <span class="item item-2x item-circle border-naranja">
                        <i class="si si-like"></i>
                    </span>
                </div>
                <h3 style="color: #ED1C24;" class="h5 font-w600 text-uppercase text-center push-30">Recibís o retirás tu compra</h3>
            </div>            
        </div>
        -->

        <div class="row push-50-t" >
          <div class="col-sm-3 text-center">
            <div class="como-funciona-box">
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/como-funciona-img-1.png">
                <h4 class="text-white push-10 font-w400 font-20 text-center push-30 push-20-t">
                <strong style="font-size:26px">1</strong>&nbsp;&nbsp;Escribís tu dirección
                </h4>                    
            </div>
          </div>
          <div class="col-sm-3 text-center">
            <div class="como-funciona-box">
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/como-funciona-img-2.png">
                <h4 class="text-white push-10 font-w400 font-20 text-center push-30 push-20-t">
                <strong style="font-size:26px">2</strong>&nbsp;&nbsp;Elegís un petshop o veterinaria
                </h4>                        
            </div>
          </div>
          <div class="col-sm-3 text-center">
            <div class="como-funciona-box">
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/como-funciona-img-3.png">
                <h4 class="text-white push-10 font-w400 font-20 text-center push-30 push-20-t">
                <strong style="font-size:26px">3</strong>&nbsp;&nbsp;Hacés tu pedido
                </h4>                      
            </div>
          </div>
          <div class="col-sm-3 text-center">
            <div class="como-funciona-box">
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/como-funciona-img-4.png">
                <h4 class="text-white push-10 font-w400 font-20 text-center push-30 push-20-t">
                <strong style="font-size:26px">4</strong>&nbsp;&nbsp;Recibís o retirás tu compra
                </h4>                      
            </div>
          </div>              
        </div>         
    




        <a class="btn btn-rounded btn-noborder btn-lg btn-primary push-50-t push-50" href="#" onclick="comenzar_ahora()">Comenzá ahora</a>
    </div>
</div>
<!-- FIN Como Funciona -->

<!-- Registro local y Redes sociales -->
<div class="bg-gray-lighter">    
    <section class="content content-full content-boxed push-50">
        <div class="row">
            <div class="col-sm-6 text-center push-30-t">
                <div class="centrado">
                    <span class="h4 push-20" style="width: 100%;">
                        ¿Aún no te registraste?
                    </span>

                    <span class="pull-left push-20-r push-10-t" style="width: 100%;">    
                        <img class="push-20-l push--5-t" src="<?php echo BASE_PATH ?>/assets/img/frontend/local.png" alt="Registra tu local" height="51">   
                                                  
                        <span class="push-10-t h3 push-10-l push-10-r"><a  href="<?php echo site_url('/beneficios') ?>"> Beneficios para los usuarios</a></span>                   
                    </span>
                </div>
            </div>

            <div class="col-sm-6 text-center push-30-t">
                <div class="centrado">
                    <span class="h4 push-10-r push-20" style="width: 100%;">
                        Seguinos en
                    </span>

                    <span class="pull-left push-20-r push-10-t" style="width: 100%;">    
                        <a href="https://www.facebook.com/GeoTienda-1260448937411952/" target="_blank" ><img class="alignnone size-full wp-image-34" src="<?php echo BASE_PATH ?>/assets/img/frontend/facebook.png" alt="facebook" width="56" height="54" data-pin-nopin="true" title="Facebook"></a>   
                                                  
                        <a class="push-10-r" href="https://twitter.com/geo_tienda" target="_blank"><img class="alignnone size-full wp-image-33" src="<?php echo BASE_PATH ?>/assets/img/frontend/twitter.png" alt="twitter" width="56" height="54" data-pin-nopin="true" title="Twitter"></a> 

                        <a class="push-10-r" href="https://www.instagram.com/geotienda/?hl=es" target="_blank"><img class="alignnone size-full wp-image-33" src="<?php echo BASE_PATH ?>/assets/img/frontend/instagram.png" alt="instagram" width="56" height="54" data-pin-nopin="true" title="Instagram"></a>                         
                    </span>
                </div>
            </div>

        </div>      
    </section>
    <div class="clearfix"></div>
</div>    
<!-- FIN Registro local y Redes sociales -->

<!-- Quienes somos -->
<div class="background-landing-abajo">    
    <section class="content content-full content-boxed">  
        <div class="text-center push-50-t push-20">
            <div class="h3 font-w600">Quiénes somos</div>
            <br />
            <div class="h4 push-20-t text-muted">GeoTienda es una plataforma online pensada para las mascotas y sus dueños.</div>
            <br />
            <div class="h4 push-20-t text-muted">Con una plataforma simple, práctica y gratuita, nuestros usuarios pueden elegir de una manera fácil alimentos, accesorios, baños, peluquería canina, aplicaciones de vacunas y cualquier servicio brindado por los Pet Shop o Veterinarias cercanas a su hogar.</div>
            <br />
            <div class="h4 push-20-t text-muted">Nuestro servicio permite a los usuarios realizar pedidos de compra o reservas online a los Pet Shop o Veterinarias de la zona durante las 24hs., todos los días del año desde su computadora o smartphone sin necesidad de hacer una llamada telefónica. También disponen de una cartilla de vacunas digital donde podrá asignar a su veterinario de confianza para que controle sus vacunas.</div>
            <br />
            <div class="h3 push-20-t text-celeste font-w600">GeoTienda es la nueva plataforma gratuita para vos y tu mascota.</div>                                        
        </div>   
        <div class="text-center push-50-t push-50">
            <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
        </div>         
    </section>  
    <div class="clearfix"></div>
</div>
<!-- FIN Quienes somos -->

<script type="text/javascript">


    $(document).ready(function() {

        // Si el usuario activo es un local, redirecciona a su dashboard
        <?php if (isset($_SESSION['frontend_logged_in'])){ ?>  

             if('<?php echo $_SESSION['frontend_tipo']; ?>' == 'local') window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos';                           
          
        <?php } ?> 
        
        $('#loc').select2({
           "language": {
               "noResults": function(){
                   return "No se encontraron localidades.";
               }
           }
        });        

        $('#loc_m').select2({
           "language": {
               "noResults": function(){
                   return "No se encontraron localidades.";
               }
           }
        });        

        $('#dir').focus();

        <?php if (isset($login)){ ?>
            show_login();
        <?php } ?>    

        <?php if (isset($tab)){ ?>
            pantalla_retorno = '<?php echo $tab; ?>';
        <?php } ?>          
    });


    function comenzar_ahora(){
        $('.dir').focus();
    }

    /* FUNCIONES VARIAS */

    // Oculta cualquier modal al hacer 'back' en el browser o celular
    $(".modal").on("shown.bs.modal", function()  { 
        var urlReplace = "#" + $(this).attr('id'); 
        history.pushState(null, null, urlReplace); 
      });

      $(window).on('popstate', function() { 
        $(".modal").modal('hide');
      });

      /* fin FUNCIONES VARIAS */    

    // Tecla enter y tab en inputs de busqueda de locales
    $('#dir').keypress(function(event) 
    {
        if (event.which == 13) 
        {
            event.preventDefault();

            if($('#dir').val() != '') {

                $("#loc").select2('open');
            }
        }
    });   

    $("#div_dir").on('keydown', '#dir', function(e) { 
      var keyCode = e.keyCode || e.which; 

      if (keyCode == 9) { 
        e.preventDefault(); 

        if($('#dir').val() != '') {

                $("#loc").select2('open');
            }
      } 
    });  

      
      /*
    $('.select2-selection').on('keyup', function (e) {
        alert('select2');
      if (e.keyCode === 13) {
        $(this).closest('form').submit();
      }
    });
    */

    var current_select2 = null;
    $(document).on('keydown', '.select2-input', function (ev) {
        var me = $(this);
        if (me.data('listening') != 1)
        {
            me
                .data('listening', 1)
                .keydown(function(ev) {
                    if ((ev.keyCode ? ev.keyCode : ev.which) == 13)
                        onKeyEnterSelect2(me, ev);
                })
            ;
        }
    });

    function onKeyEnterSelect2(el, ev)
    {
        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
        console.log(keycode, el);
    }
</script>
