<!-- Footer -->
<footer id="page-footer" class="landing-footer">
    <div class="content content-boxed ancho-footer">
        <!-- Footer Navigation -->
        <div class="row push-30-t items-push-2x text-center">
            <div class="col-sm-3">
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/marcas.png" alt="GeoTienda">
            </div>

            <div class="col-sm-3">
                <div class="h4 font-w600 text-uppercase push-20">GeoTienda</div>
                <ul class="list list-simple-mini font-s13">
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/contacto_local') ?>">Sumá tu comercio a GeoTienda</a>
                    </li>
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/terminos') ?>">Términos y condiciones</a>
                    </li>
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/politicas') ?>">Políticas de privacidad</a>
                    </li>
                </ul>
            </div>

            <div class="col-sm-3">
                <div class="h4 font-w600 text-uppercase push-20">Ayuda</div>
                <ul class="list list-simple-mini font-s13">
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/faq') ?>">Preguntas frecuentes</a>
                    </li>
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/contacto') ?>">Contacto</a>
                    </li>
                    <li>
                        <a class="font-w600 links-footer" href="<?php echo site_url('/mapa') ?>">Mapa del sitio</a>
                    </li>
                </ul>
            </div>     

            <div class="col-sm-3">
                <div class="h4 font-w600 text-uppercase push-20">Formas de pago</div>
                <img src="<?php echo BASE_PATH ?>/assets/img/frontend/mediospago_efectivo_tarjetas.png" alt="GeoTienda">
            </div>                                           
        </div>
        <!-- END Footer Navigation -->

        <!-- Copyright Info -->
        <div class="row clearfix push-30">
            <div class="footfoot">
                <div class="icons">
                    <a href="https://www.facebook.com/GeoTienda-1260448937411952/" target="_blank"><i class="fa fa-facebook links-footer"></i></a>
                    <a href="https://twitter.com/geo_tienda" target="_blank"><i class="fa fa-twitter links-footer"></i></a><a href="https://www.instagram.com/geotienda/?hl=es" target="_blank"><i class="fa fa-instagram links-footer"></i></a>
                </div>
                <div class="raya"></div>
                <div >
                    GeoTienda 2017 &copy; | Buenos Aires, Argentina
                    <!--<a class="links-footer" href="#">T&eacute;rminos de uso</a> | <a class="links-footer" href="#">Cookies</a> | <a class="links-footer" href="#">Versi&oacute;n m&oacute;vil</a>-->
                </div>
            </div>       
        </div>
        <!-- END Copyright Info -->
    </div>
</footer>
<!-- END Footer -->



<script>

var latitud;
var longitud;  

/* BUSQUEDA DE LOCALES */
/*
// Tecla enter en inputs de busqueda de locales (DESKTOP y TABLET)
$('.input-buscar-local').keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        
        buscar_locales();
    }
});

// Tecla enter en inputs de busqueda de locales (MOBILE)
$('.input-buscar-local-m').keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        
        buscar_locales_mobile();
    }
});            

// Tecla enter en inputs de busqueda de locales (BUSCADOR MINI en el Header)
$('.input-buscar-local-mini').keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        
        buscar_locales_mini();
    }
});

// Tecla enter en inputs de busqueda de locales (BUSCADOR MINI en el Header - version MOBILE)
$('.input-buscar-local-mini-m').keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        
        buscar_locales_mini_m();
    }
});
*/

// Determina las coordenadas de la dirección ingresada
function geolocalizacion(form_buscar_local, form_buscar_local_input, input_dir, input_loc, valida_dir, valida_loc)
{
   valida_dir.hide();
   valida_loc.hide();

    // Geolocalizacion
    var geocoder = new google.maps.Geocoder();

    // TODO: agregar cuando se busque por provincia + ',' + $('#provincia option:selected').text() 
    var direccion = '' + input_dir.val() + ', ' + input_loc.val();  
    
    geocoder.geocode( { 'address': direccion}, function(results, status) {

        if (status == google.maps.GeocoderStatus.OK) {

            latitud = results[0].geometry.location.lat();
            longitud = results[0].geometry.location.lng();  

            // Limpia valores anteriores del form
            form_buscar_local_input.remove();

            // Agrega direccion a los datos enviados
            $('<input />').attr('type', 'hidden')
              .attr('name', "dir")
              .attr('value', input_dir.val())
              .appendTo(form_buscar_local);

            $('<input />').attr('type', 'hidden')
              .attr('name', "loc")
              .attr('value', input_loc.val())
              .appendTo(form_buscar_local); 

            // Agrega coordenadas a los datos enviados
            $('<input />').attr('type', 'hidden')
              .attr('name', "lat")
              .attr('value', latitud)
              .appendTo(form_buscar_local);

            $('<input />').attr('type', 'hidden')
              .attr('name', "long")
              .attr('value', longitud)
              .appendTo(form_buscar_local);        

            // Valida que se haya ingresado una direccion
            if(input_dir.val() == '') 
            {
                valida_dir.show();
                input_dir.focus();
            }
            else if(input_loc.val() == '') 
            {
                valida_loc.show();
                input_loc.focus();
            }

            // Si se pudo obtener la ubicación, envía el form
            else
                form_buscar_local.submit();    
        }
        // Se no pudo ubicar la direccion, ...
        else
        {
            if(input_dir.val() == '') 
            {
                valida_dir.show();
                input_dir.focus();
            }
            else if($('#loc').val() == '') 
            {
                valida_loc.show();
                input_loc.focus();
            }
            else
            {
                alert('No encontramos tu ubicación. Intentá ingresando una nueva dirección.');

             //   var input_dir = $('#dir');
                var strLength = input_dir.val().length * 2;
                input_dir.focus();
                input_dir[0].setSelectionRange(strLength, strLength);
            }
            return false;
        }


    }); 
}


// Buscador de locales (DESKTOP y TABLET)
function buscar_locales()
{
    geolocalizacion($('#form_buscar_local'), $('#form_buscar_local input'), $('#dir'), $('#loc'), $('#valida_dir'), $('#valida_loc'));
}      

// Buscador de locales  (MOBILE)
function buscar_locales_mobile()
{
    geolocalizacion($('#form_buscar_local_m'), $('#form_buscar_local_m input'), $('#dir_m'), $('#loc_m'), $('#valida_dir_m'), $('#valida_loc_m'));
}   

// Buscador de locales (BUSCADOR MINI en el Header)
function buscar_locales_mini()
{
    geolocalizacion($('#form_buscar_local_mini'), $('#form_buscar_local_mini input'), $('#dir_mini'), $('#loc_mini'), $('#valida_dir_mini'), $('#valida_loc_mini'));
}  

// Buscador de locales (BUSCADOR MINI en el Header - version MOBILE)
function buscar_locales_mini_m()
{
    geolocalizacion($('#form_buscar_local_mini_m'), $('#form_buscar_local_mini_m input'), $('#dir_mini_m'), $('#loc_mini_m'), $('#valida_dir_mini_m'), $('#valida_loc_mini_m'));
}  
/* fin BUSQUEDA DE LOCALES */  


/* Refresco invisible de la sesion */
var refreshTime = 3000; // cada 5 minutos
setInterval( function() 
{
    $.ajax({
        cache: false,
        type: "GET",
        url: "<?php echo site_url('/refreshSession.php') ?>",
        success: function(data) {
        }
    });
}, refreshTime );
 
</script>