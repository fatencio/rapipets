<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_terminos')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>


<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Términos y Condiciones</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Términos y Condiciones</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<section class="content content-full content-boxed">  
    <div class="push-50">
        <div class="h5 push-20-t text-gris">Este contrato describe los Términos y Condiciones Generales aplicados al uso de los servicios ofrecidos por GeoTienda.com, (los Servicios) dentro del sitio www.geotienda.com</div>

        <div class="h5 push-20-t text-gris">Cualquier persona (Usuario o Usuarios) que desee acceder y/o usar el sitio o los servicios podrá hacerlo sujetándose a los Términos y Condiciones Generales, junto con todas las demás políticas y principios que rigen GeoTienda y que son incorporados al presente por referencia.</div>

        <div class="h5 push-20-t text-gris">CUALQUIER PERSONA QUE NO ACEPTE ESTOS TÉRMINOS Y CONDICIONES GENERALES, LOS CUALES TIENEN UN CARÁCTER OBLIGATORIO Y VINCULANTE, DEBERÁ ABSTENERSE DE UTILIZAR EL SITIO Y/O LOS SERVICIOS.</div>

        <div class="h5 push-20-t text-gris">El Usuario debe leer, entender y aceptar todas las condiciones establecidas en los Términos y Condiciones Generales, en las Políticas de Privacidad y confidencialidad de la información Seguridad así como en los demás documentos incorporados a los mismos por referencia, previo a su registración como Usuario de GeoTienda.</div>

        <div class="h5 push-20-t text-gris">El registro del usuario para el uso del Servicio aquí especificado será considerado como expreso consentimiento a cumplir con este Contrato, incluido cualquier material disponible en el sitio Web de GeoTienda incorporado como referencia en este documento.</div>    

        <br />
        <div class="h4 push-20-t text-gris">1. Capacidad</div>                                      
        <div class="h5 push-20-t text-gris">Los Servicios sólo están disponibles para personas que tengan capacidad legal para contratar. No podrán utilizar los servicios las personas que no tengan esa capacidad, los menores de edad o Usuarios de GeoTienda que hayan sido suspendidos temporalmente o inhabilitados definitivamente. Si estás registrando un Usuario como Empresa, debes tener capacidad para contratar a nombre de tal entidad y de obligar a la misma en los términos de este Acuerdo.</div>        

        <br />
        <div class="h4 push-20-t text-gris">2. Registro</div>     
        <div class="h5 push-20-t text-gris">Es obligatorio completar el formulario de registración en todos sus campos con datos válidos para poder utilizar los servicios que brinda GeoTienda. El futuro Usuario deberá completarlo con sus Datos Personales de manera exacta, y verdadera y asume el compromiso de actualizar los Datos Personales conforme resulte necesario. GeoTienda podrá utilizar diversos medios para identificar a sus Usuarios, pero GeoTienda NO se responsabiliza por la certeza de los Datos Personales provistos por sus Usuarios. Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de los Datos personal elegida (Clave de Seguridad). El Usuario se obliga a mantener la confidencialidad de su Clave de Seguridad. La Cuenta es personal, única e intransferible, y está prohibido que un mismo Usuario registre o posea más de una Cuenta. En caso que GeoTienda detecte distintas Cuentas que contengan datos coincidentes o relacionados, podrá cancelar, suspender o inhabilitarlas. El Usuario será responsable por todas las operaciones efectuadas en su Cuenta, pues el acceso a la misma está restringido al ingreso y uso de su Clave de Seguridad, de conocimiento exclusivo del Usuario. El Usuario se compromete a notificar a GeoTienda en forma inmediata y por medio idóneo y fehaciente, cualquier uso no autorizado de su Cuenta, así como el ingreso por terceros no autorizados a la misma. Se aclara que está prohibida la venta, cesión o transferencia de la Cuenta. Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de los Información Personal ingresados.</div>    

        <br />
        <div class="h4 push-20-t text-gris">3. Modificaciones del acuerdo</div>

        <div class="h5 push-20-t text-gris">GeoTienda podrá modificar los Términos y Condiciones Generales en cualquier momento haciendo públicos en el Sitio los términos modificados. Todos los términos modificados entrarán en vigor a los 10 (diez) días de su publicación. Dentro de los 5 (cinco) días siguientes a la publicación de las modificaciones introducidas, el Usuario deberá comunicar por email a info@geotienda.com si no acepta las mismas; en ese caso será inhabilitado como Usuario siempre que no tenga deudas pendientes. Vencido este plazo, se considerará que el Usuario acepta los nuevos términos y el contrato continuará vinculando a ambas partes.</div>         

        <br />
        <div class="h4 push-20-t text-gris">4. Privacidad de la Información</div>                           
        <div class="h5 push-20-t text-gris">Para utilizar los Servicios ofrecidos por GeoTienda, los Usuarios deberán facilitar determinados datos de carácter personal o comercial. Esta Información se procesa y almacena en servidores o medios magnéticos que mantienen altos estándares de seguridad y protección tanto física como tecnológica. Para mayor información sobre la privacidad de la Información Personal o Comercial y casos en los que será revelada dicha Información, se puede consultar nuestra Política de privacidad y confidencialidad de la información.</div>                             

        <br />
        <div class="h4 push-20-t text-gris">5. Obligaciones de los Usuarios</div>         
        <div class="h5 push-20-t text-gris">GeoTienda sólo pone a disposición de los Usuarios un espacio virtual que les permite comunicarse através de Internet para encontrar una forma de comprar Alimentos para Mascotas, Accesorios, u otros servicios relacionados. GeoTienda no tiene participación alguna en el proceso de negociación y perfeccionamiento del contrato definitivo entre las partes. Por eso, GeoTienda no es responsable por el efectivo cumplimiento de las obligaciones fiscales o impositivas establecidas por la ley vigente. Dado que GeoTienda es un punto de encuentro entre comprador y Comercio y no participa de las operaciones que se realizan entre ellos, el Comercio será responsable por todas las obligaciones y cargas impositivas que correspondan por la venta de Alimentos para Mascotas, Accesorios, u otros servicios, sin que pudiera imputársele a GeoTienda algún tipo de responsabilidad por incumplimientos en tal sentido.</div>           
        <br />
        <div class="h4 push-20-t text-gris">6. Violaciones del Sistema o Bases de Datos</div>         
        <div class="h5 push-20-t text-gris">No está permitida ninguna acción o uso de dispositivo, software, u otro medio tendiente a interferir tanto en las actividades y operatoria de GeoTienda como en las ofertas, descripciones, cuentas o bases de datos de GeoTienda. Cualquier intromisión, tentativa o actividad violatoria o contraria a las leyes sobre derecho de propiedad intelectual y/o a las prohibiciones estipuladas en este contrato harán pasible a su responsable de las acciones legales pertinentes, y a las sanciones previstas por este acuerdo, así como lo hará responsable de indemnizar los daños ocasionados.</div>                                       

        <br />
        <div class="h4 push-20-t text-gris">7. Sanciones. Suspensión de operaciones</div>         
        <div class="h5 push-20-t text-gris">Sin perjuicio de otras medidas, GeoTienda advertirá, suspenderá temporal o inhabilitará definitivamente la Cuenta de un Usuario e iniciará las acciones que estime pertinentes, y no le prestará sus Servicios si (a) se quebrantara alguna ley, o cualquiera de las estipulaciones de los Términos y Condiciones Generales y demás políticas de GeoTienda; (b) si incumpliera sus compromisos como Usuario; (c) si se incurriera a criterio de GeoTienda en conductas o actos dolosos o fraudulentos; (d) no pudiera verificarse la identidad del Usuario o cualquier información proporcionada por el mismo fuere errónea. Asimismo GeoTienda podrá decidir a su exclusivo criterio no prestar el servicio, sin generar esto responsabilidad alguna.</div>    

        <br />
        <div class="h4 push-20-t text-gris">8. Responsabilidad</div>         
        <div class="h5 push-20-t text-gris">GeoTienda sólo pone a disposición de los Usuarios un espacio virtual que les permite ponerse en comunicación mediante Internet para encontrar una forma de comprar Alimentos para Mascotas, Accesorios u otros artículos, o servicios. GeoTienda no es el propietario de los artículos ofrecidos o servicios, no tiene posesión de ellos ni los ofrece en venta. GeoTienda no interviene en el perfeccionamiento de las operaciones realizadas entre los Usuarios ni en las condiciones por ellos estipuladas para las mismas, por ello no será responsable respecto de la existencia, calidad, estado, integridad o legitimidad de los bienes ofrecidos, adquiridos o enajenados por los Usuarios, así como de la capacidad para contratar de los Usuarios o de la veracidad de los Información Personal por ellos ingresados. Cada Comercio conoce y es el exclusivo responsable por las ofertas de productos y servicios que publica para su venta, por su existencia, calidad, precio, estado, integridad o legitimidad. Asimismo el Usuario o cliente asume y reconoce la anterior responsabilidad mencionada por las compras que realiza. GeoTienda sólo pone a disposición de los Usuarios un espacio virtual que les permite ponerse en comunicación mediante Internet para encontrar una forma de comprar Alimentos para Mascotas, Accesorios u otros artículos, o servicios. GeoTienda no es el propietario de los artículos ofrecidos o servicios, no tiene posesión de ellos ni los ofrece en venta. GeoTienda no interviene en el perfeccionamiento de las operaciones realizadas entre los Usuarios ni en las condiciones por ellos estipuladas para las mismas, por ello no será responsable respecto de la existencia, calidad, estado, integridad o legitimidad de los bienes ofrecidos, adquiridos o enajenados por los Usuarios, así como de la capacidad para contratar de los Usuarios o de la veracidad de los Información Personal por ellos ingresados. Cada Comercio conoce y es el exclusivo responsable por las ofertas de productos y servicios que publica para su venta, por su existencia, calidad, precio, estado, integridad o legitimidad. Asimismo el Usuario o cliente asume y reconoce la anterior responsabilidad mencionada por las compras que realiza.</div>    
        <div class="h5 push-20-t text-gris">En caso que uno o más Usuarios inicien cualquier tipo de reclamo o acciones legales contra un Comercio, todos y cada uno de los Usuarios involucrados en dichos reclamos o acciones eximen de toda responsabilidad a GeoTienda y a sus empleados, agentes, operarios, representantes y apoderados.</div> 


        <br />
        <div class="h4 push-20-t text-gris">9. Alcance de los servicios de GeoTienda</div>         
        <div class="h5 push-20-t text-gris">Este acuerdo no crea ningún contrato de sociedad, de mandato, de franquicia, o relación laboral entre GeoTienda y el Usuario. El Usuario reconoce y acepta que GeoTienda no es parte en ninguna operación, ni tiene control alguno sobre la calidad, seguridad o legalidad de productos o servicios adquiridos, o sobre la veracidad o exactitud de las ofertas.</div>   

        <br />
        <div class="h4 push-20-t text-gris">10. Fallas en el sistema</div>         
        <div class="h5 push-20-t text-gris">GeoTienda no se responsabiliza por cualquier daño, perjuicio o pérdida al Usuario causados por fallas en el sistema, en el servidor o en Internet. GeoTienda tampoco será responsable por cualquier virus que pudiera infectar el equipo del Usuario como consecuencia del acceso, uso o examen de su sitio web o a raíz de cualquier transferencia de datos, archivos, imágenes, textos, o audio contenidos en el mismo. Los Usuarios NO podrán imputarle responsabilidad alguna ni exigir pago por lucro cesante, en virtud de perjuicios resultantes de dificultades técnicas o fallas en los sistemas o en Internet. GeoTienda no garantiza el acceso y uso continuado o ininterrumpido de su sitio. El sistema puede eventualmente no estar disponible debido a dificultades técnicas o fallas de Internet, o por cualquier otra circunstancia ajena a GeoTienda; en tales casos se procurará restablecerlo con la mayor velocidad posible sin que por ello pueda imputársele algún tipo de responsabilidad. GeoTienda no será responsable por ningún error u omisión contenidos en su sitio web.</div>                                       

        <br />
        <div class="h4 push-20-t text-gris">11. Uso gratuito</div>         
        <div class="h5 push-20-t text-gris">La registración y el uso de GeoTienda son gratuitos para los Usuarios.</div>   
        <div class="h5 push-20-t text-gris">GeoTienda se reserva el derecho de modificar, cambiar, agregar, o eliminar las tarifas vigentes, en cualquier momento, lo cual será notificado a los Usuarios. Sin embargo, GeoTienda podrá modificar temporalmente la Política de Tarifas y las tarifas por sus servicios por razón de promociones, siendo efectivas estas modificaciones cuando se haga pública la promoción o se realice el anuncio.</div>    

        <br />
        <div class="h4 push-20-t text-gris">12. Propiedad intelectual. Enlaces</div>         
        <div class="h5 push-20-t text-gris">Los contenidos de las pantallas relativas a los servicios de GeoTienda como así también los programas, bases de datos, redes, archivos que permiten al Usuario acceder y usar su Cuenta, son de propiedad de GeoTienda y están protegidas por las leyes y los tratados internacionales de derecho de autor, marcas, patentes, modelos y diseños industriales. El uso indebido y la reproducción total o parcial de dichos contenidos quedan prohibidos, salvo autorización expresa y por escrito de GeoTienda.</div>    
        <div class="h5 push-20-t text-gris">El Sitio puede contener enlaces a otros sitios web lo cual no indica ni significa que sean propiedad u operados por GeoTienda. En virtud que GeoTienda no tiene control sobre tales sitios NO será responsable por los contenidos, materiales, acciones y/o servicios prestados por los mismos, ni por daños o pérdidas ocasionadas por la utilización de los mismos, sean causadas directa o indirectamente. La presencia de enlaces a otros sitios web no implica una sociedad, relación, aprobación, respaldo de GeoTienda a dichos sitios y sus contenidos.</div>    

        <br />
        <div class="h4 push-20-t text-gris">13. Jurisdicción y Ley Aplicable</div>         
        <div class="h5 push-20-t text-gris">Este acuerdo estará regido en todos sus puntos por las leyes vigentes en la República Argentina.</div>     
        <div class="h5 push-20-t text-gris">Cualquier controversia derivada del presente acuerdo, su existencia, validez, interpretación, alcance o cumplimiento, será sometida a los tribunales Ordinarios de la Ciudad Autónoma de Buenos Aires y los procedimientos se llevarán a cabo en idioma castellano.</div>                                          
    </div>   
    <div class="text-center push-50-t push-50">
        <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
    </div>         
</section>