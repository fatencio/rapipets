<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_restablecer')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>

<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $portada_transparencia?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada" class="text-center">
            
        <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">   
        </div>      
    </div>  
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $portada_transparencia?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada" class="text-center">
            
        <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">   
        </div>      
    </div>  
</span>

<section class="content content-full content-boxed form-reset">  
    <div class="push-20">
        <div class="h2 push-20-t text-gris text-center">¿Olvidaste tu contraseña?</div>
        <div class="h4 push-20-t text-gris text-center">Ingresá el email asociado a tu cuenta para generar una nueva contraseña</div>
    </div>   

    <!-- FORM reset -->
    <div>
        <form class="js-validation-login form-horizontal push-30-t push-30" action="#" id="form_solicitar_reset">      
            <!-- Reset Error -->
            <div class="alert alert-danger alert-dismissable" id="div_reset_error" style="display:none">
                <small id="reset_error_message"></small>                 
            </div>
            <!-- END Reset Error -->                                                                                  
            <div class="form-group form-group-login">
                <div class="col-xs-12">
                    <input class="form-control" type="text" id="reset-email" name="reset-email" placeholder="E-mail" value="<?php if (isset($reset_email)) echo $reset_email; ?>"  required>
                </div>
            </div>
        </form>   
        <div class="form-group form-group-login push-50-t push-50">
              <div class="text-center">
                <button class="btn btn-lg btn-geotienda push-10-r" id="btn_enviar_reset" onclick="enviar_reset()">&nbsp;&nbsp;Enviar&nbsp;&nbsp;</button>
            </div>                                                                    
        </div>
     </div>   
    <!-- fin FORM reset -->
      

    <!-- Confirmacion reset OK Modal -->
    <div class="modal fade" id="modal_reset_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-content geotienda-modal-content">
                        <ul class="block-options login-close" >
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>                        
                        <h2 class="text-center text-naranja">Solicitud enviada con éxito</h2>
                        <br/>
                        <h3 class="font-w300 text-center">Te enviamos un correo con las instrucciones para restablecer la contraseña de tu cuenta.</h3>
                        <h5 class="font-w300 text-center text-muted push-20-t">Si no te llegó un email al buzón de entrada, por favor revisá tu correo no deseado.</h5>
                        <br/><br/>

                        <div class="text-center">
                            <a href="<?php echo BASE_PATH ?>" id="btn_reset_ok" class="btn btn-lg btn-info btn-geotienda-celeste" >Aceptar</a>
                        </div>
                        <br/>


                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- FIN Confirmacion reset OK Modal -->

</section>

<script>

    $(document).ready(function() {

        $('#reset-email').focus();

    });

    /* FORM reset */

    // Al mostrar el modal de contacto exitoso, hace foco en el boton 'Aceptar'
    $('#modal_reset_ok').on('shown.bs.modal', function () {
        $('#btn_reset_ok').focus();
    })


    $('#reset-email').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#reset-email').val() != '') enviar_reset();
        }
    });    

    function enviar_reset()
    {
        var url = "<?php echo BASE_PATH ?>/Inicio/solicita_reset";

        $('#btn_enviar_reset').html('Enviando...');
        $('#btn_enviar_reset').attr('disabled', true); 

        $.ajax({
            url : url ,
            type: "POST",
            data: $('#form_solicitar_reset').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                // reset ok
                if(data.status) 
                {
                    $('#modal_reset_ok').modal('show');

                    $('#btn_enviar_reset').html('Enviar');
                    $('#btn_enviar_reset').attr('disabled', false);                          
                }
                // Fallo en reset
                else
                {
                    mensaje_error = '';

                    for (var i = 0; i < data.error.mensaje.length; i++) 
                    {
                        mensaje_error += data.error.mensaje[i] + '<br/>';
                    }

                    $('#reset_error_message').html(mensaje_error);
                    $('#div_reset_error').show();      

                    $('#btn_enviar_reset').html('Enviar');
                    $('#btn_enviar_reset').attr('disabled', false);                                               
                }

            },
            // Error no manejado
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_error_sistema').modal('show');
                console.log(errorThrown);

                $('#btn_enviar_reset').html('Enviar');
                $('#btn_enviar_reset').attr('disabled', false);                   
            }
        });
    }
/* fin FORM reset */
</script>