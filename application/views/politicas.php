<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_politicas')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>


<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Políticas de Privacidad</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Políticas de Privacidad</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span> 

<section class="content content-full content-boxed">  
    <div class="push-50">
        <div class="h5 push-20-t text-gris">Para GeoTienda es muy importante que usted lea detenidamente este documento. Estas Políticas de Seguridad y Privacidad, tienen como finalidad desarrollar algunas pautas que se relacionan con los datos personales que usted nos proporciona en forma consentida.</div>

            <div class="h5 push-20-t push-20-l text-gris">1 - Explicarle que medidas de seguridad adoptaremos sobre sus datos personales, siempre teniendo como parámetro los principios establecidos en la Ley N° 25.326 (Ley de Protección de Datos Personales), a la cuál adherimos.</div>

            <div class="h5 push-20-t push-20-l text-gris">2 - Determinar qué tratamiento le dará www.geotienda.com a sus datos personales.</div>

            <div class="h5 push-20-t push-20-l text-gris">3 - Darle a los usuarios el derecho de supresión, rectificación, confidencialidad o actualización de aquellos datos personales que nos proporciona, conforme a como lo menciona el art. 43 (párrafo tercero) de nuestra Constitución Nacional.</div>

        <div class="h5 push-20-t text-gris">GEOTIENDA no controla el contenido ni las políticas o prácticas de privacidad de sitios web de terceros, como así tampoco asume responsabilidad alguna por dichos contenidos, políticas o prácticas. El contenido de este documento de ninguna manera se aplicará a vínculos, links, conexiones, contenidos, términos y políticas de privacidad que no estén bajo la propiedad del titular explotador de GEOTIENDA.COM</div>

        <div class="h4 push-30-t push-20-l text-gris">1.1.- MEDIDAS DE SEGURIDAD Política y cultura de privacidad</div>

        <div class="h5 push-20-t push-20-l text-gris">GEOTIENDA se esfuerza por ofrecerle el más alto nivel de seguridad, con la intención de garantizarle el resguardo y respeto a los datos personales que usted nos brinda. Para lograr este objetivo no sólo cumplimos con las condiciones técnicas de integridad y seguridad sino que además, hemos adherido a los requerimientos que la Ley Nacional de Protección de Datos Personales N° 25.326 y sus normas complementarias, exigen para estar de acuerdo al ordenamiento legal.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">1.2.- Cookies</div>

        <div class="h5 push-20-t push-20-l text-gris">El Usuario conoce y acepta que GEOTIENDA podrá utilizar un sistema de seguimiento mediante la utilización de cookies. Las cookies son pequeños archivos que se instalan en el disco rígido, con una duración limitada en el tiempo que ayudan a personalizar los servicios. También ofrecemos ciertas funcionalidades que sólo están disponibles mediante el empleo de cookies. Utilizamos adicionalmente las cookies para que el Usuario no tenga que introducir su contraseña tan frecuentemente durante una sesión de navegación, también para contabilizar y corroborar las registraciones, la actividad del Usuario y otros acuerdos comerciales, siempre teniendo como objetivo de la instalación de las cookies, el beneficio del Usuario que la recibe, y no será usado con otros fines ajenos a GEOTIENDA.</div>

        <div class="h5 push-20-t push-20-l text-gris">Se establece que la instalación, permanencia y existencia de las cookies en el computador del Usuario depende de su exclusiva voluntad y puede ser eliminada de su computador cuando el Usuario así lo desee. Para saber como quitar las cookies del sistema es necesario revisar la sección Ayuda (Help) del navegador.</div>

        <div class="h4 push-30-t push-20-l text-gris">2.1.- Información que usted nos brinda</div>

        <div class="h5 push-20-t push-20-l text-gris">Para establecer un diálogo responsable y directo tanto con nuestros usuarios, como así también con usted, GEOTIENDA le solicitará información personal (Nombres, Apellidos, Tipo de Documento -DNI, LE, LC, Extranjero-, Número, Sexo, Fecha de nacimiento) con el objeto de que pueda acceder al servicio ofrecido. GEOTIENDA recoge y almacena automáticamente cierta Información Personal sobre la actividad de los Usuarios dentro de nuestro sitio web. Tal información puede incluir la URL de la que provienen (estén o no en nuestro sitio web), a qué URL acceden seguidamente (estén o no en nuestro sitio web), qué navegador están usando, y sus direcciones IP. Los Usuarios garantizan y responden, en cualquier caso, de la veracidad, exactitud, vigencia y autenticidad de los Datos Personales facilitados, y se comprometen a mantenerlos debidamente actualizados.</div>

        <div class="h5 push-20-t push-20-l text-gris">Una vez finalizada la primera compra utilizando GEOTIENDA, el Usuario podrá revisar y cambiar la información que nos ha enviado durante el proceso de compra incluyendo:</div>

        <div class="h5 push-10-t push-30-l text-gris">- El email. Sin perjuicio de los cambios que realice, GEOTIENDA conservará la Información Personal anterior por motivos de seguridad y control del fraude.</div>

        <div class="h5 push-10-t push-30-l text-gris">- La información de registro como: nombre y apellido, compañía, domicilio, ciudad, región, código postal, país, número principal de teléfono, número secundario de teléfono, número de fax, etc.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.2.- TRATAMIENTO DE LOS DATOS PERSONALES</div>

        <div class="h5 push-20-t push-20-l text-gris">Al proporcionarnos sus datos personales en forma expresa, libre y consentida, usted nos está autorizando automáticamente a que utilicemos esa información, conforme a como lo menciona esta Política de Seguridad y Privacidad.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.3.- ¿Cómo utilizamos su información?</div>

        <div class="h5 push-20-t push-20-l text-gris">GEOTIENDA utiliza la información que usted nos proporciona para: expandir ofertas de comercialización; para publicar productos y servicios que podrían ser de su interés; para la emisión de newsletters; para personalizar y mejorar nuestro sitio web, así como también para brindarle acceso a nuestro servicio o enviarle e-mails o correspondencia con fines promocionales.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.3.1 Información enviada</div>

        <div class="h5 push-20-t push-20-l text-gris">Si los Usuarios nos envían correspondencia, sean correos electrónicos o cartas, podemos recoger y almacenar tal Información Personal.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.3.2.- Confirmación de información</div>

        <div class="h5 push-20-t push-20-l text-gris">GEOTIENDA podrá confirmar los datos personales suministrados acudiendo a entidades públicas o compañías especializadas, para lo cual el Usuario nos autoriza expresamente. La información que GEOTIENDA obtenga de estas entidades será tratada en forma absolutamente confidencial.</div>  

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.3.3.- Uso que hacemos de la información</div>

        <div class="h5 push-20-t push-20-l text-gris">Para suministrar un excelente servicio y para que los Usuarios puedan realizar operaciones en forma ágil y segura, GEOTIENDA requiere ciertos datos de carácter personal. La recolección de Información Personal nos permite ofrecerles servicios y funcionalidades que se adecuan mejor a sus necesidades y personalizar nuestros servicios para hacer que sus experiencias sean lo más cómodas posible. La Información Personal que recabamos tiene las siguientes finalidades:</div>  

        <div class="h5 push-10-t push-30-l text-gris">- Poner en contacto directo a Comercios y al Usuario. En este caso, GEOTIENDA suministrará al Comercio sólo la información necesaria para transferir el Pedido (nombre, dirección y teléfonos), a través de Internet o telefónicamente sumado a un envió de correo electrónico O sms para control y archivo. La información así conocida por el Pet Shop o Veterinaria o el Usuario, sólo podrá ser utilizada a efectos de concluir la operación de compraventa y entrega a domicilio originada en GEOTIENDA y no podrá ser empleada por ninguno de ellos con fines publicitarios o promocionales u otras actividades no relacionadas con GEOTIENDA.</div>                              

        <div class="h5 push-20-t push-20-l text-gris">En resumen, la información podrá utilizarse para:</div>

        <div class="h5 push-10-t push-30-l text-gris">a.- Contactar al Usuario interesado en utilizar la plataforma brindada por GEOTIENDA.</div>

        <div class="h5 push-10-t push-30-l text-gris">b.- Desarrollar estudios internos sobre los intereses, el comportamiento y la demografía de nuestros Usuarios con el objetivo de comprender mejor sus necesidades y darle un mejor servicio.</div>

        <div class="h5 push-10-t push-30-l text-gris">c.- Mejorar nuestras iniciativas comerciales y promocionales para analizar las páginas más visitadas de nuestro sitio, perfeccionar nuestra oferta de contenidos y artículos, personalizar dichos contenidos, presentación y servicios.</div>

        <div class="h5 push-10-t push-30-l text-gris">d.- Enviar información o mensajes sobre nuevos servicios, promociones, banners, de interés para nuestros Usuarios, noticias sobre GEOTIENDA, además de la información expresamente autorizada en las preferencias. Si el Usuario lo prefiere, puede solicitar que lo excluyan de las listas para el envío de información promocional o publicitaria.</div>

        <div class="h5 push-10-t push-30-l text-gris">e.- En caso de existir algún tipo de promoción, concurso o sorteo, los ganadores de los mismos realizados por GEOTIENDA autorizan expresamente a difundir sus nombres, datos personales e imágenes y los de sus familias, por los medios y en las formas que se consideren convenientes, con fines publicitarios y/o promocionales, sin derecho a compensación alguna.</div>

        <div class="h5 push-10-t push-30-l text-gris">f.- Compartir los datos personales con los proveedores de servicios de valor agregado o las empresas de 'outsourcing' que contribuyan a mejorar o a facilitar las operaciones a través de GEOTIENDA, como servicios de transporte, medios de pago, seguros o intermediarios en la gestión de pagos, call centers o programas de fidelidad, entre otros. Estas compañías o sitios de Internet generalmente tienen políticas sobre confidencialidad de las informaciones similares a las nuestras. Sin embargo, GEOTIENDA velará porque se cumplan ciertos estándares, mediante la firma de acuerdos o convenios cuyo objeto sea la privacidad de los datos personales de nuestros Usuarios. No obstante, GEOTIENDA no se hace responsable por el uso indebido de la Información Personal del Usuario que hagan estas compañías o sitios de Internet. En algunos casos, estos proveedores de servicios serán quienes recojan información directamente del Usuario (por ejemplo si les solicitamos que realicen encuestas o estudios). En tales casos, recibirá una notificación acerca de la participación de un proveedor de servicios en tales actividades, y quedará a discreción del Usuario toda la información que quiera brindarle y los usos adicionales que los proveedores decidan hacer.</div>        

        <div class="h5 push-20-t push-20-l text-gris">En caso de que el Usuario facilite, por propia iniciativa información adicional a dichos prestadores de servicios directamente, tales prestadores usarán esta información conforme a sus propias políticas de privacidad.</div>        

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.4.- BAJA DE USUARIO</div>

        <div class="h5 push-20-t push-20-l text-gris">Se establece que en cualquier momento el Usuario registrado en GEOTIENDA podrá solicitar la baja de su registro y la eliminación de su cuenta e información de la base de datos de GEOTIENDA.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">2.5.- Destinatarios de la Información Personal</div>

        <div class="h5 push-20-t push-20-l text-gris">Sus datos personales constituyen un activo para nuestra empresa que valoramos y protegemos cuidadosamente. Por esto es que la información que usted nos proporciona la tratamos bajo los parámetros de la Ley N° 25.326 y sólo tienen acceso a estos datos, aquellas personas autorizadas en nuestra empresa para su utilización, conforme a la finalidad para la cual hayan sido brindadas. Con el objeto de otorgarle a usted un mejor servicio y asesoramiento, es que GEOTIENDA puede ceder estos datos a empresas asociadas en el servicio ofrecido.</div>        
        <div class="h4 push-30-t push-20-l text-gris">3.- DERECHOS Y OBLIGACIONES DEL USUARIO</div>

        <div class="h5 push-20-t push-20-l text-gris">De ninguna manera GEOTIENDA le solicitará para su base de datos, información personal que revele su origen racial y étnico, opiniones políticas, convicciones religiosas, filosóficas o morales, afiliación sindical e información referente a su salud o a su vida sexual (art. 2, “Datos sensibles” Ley N° 25.326). En caso de que usted revele esta información de su persona, se entiende enmarcada dentro de su libertad de expresión u opinión consagrada por nuestra Constitución Nacional (art. 14) y la misma se tendrá como libremente consentida. En caso de que un usuario manifieste datos sensibles de un tercero, GEOTIENDA no será responsable civil o penalmente por dicha revelación, y pondrá a disposición del o los lectores y/o interesados damnificados los datos personales del usuario registrado a fin de que se inicien las acciones legales correspondientes, además de eliminar en forma inmediata el mensaje, si el damnificado lo solicita.</div>

        <div class="h5 push-20-t push-20-l text-gris">Los datos personales que usted nos brinda deberán ser completos, ciertos y actualizados, no siendo responsable GEOTIENDA por su parcialidad, inexactitud o falta de actualización. El usuario podrá hacer uso del derecho de supresión, rectificación, confidencialidad o actualización de aquéllos, conforme al art. 43 (párrafo tercero) de nuestra Constitución Nacional, enviando un correo electrónico a <a href="mailto: info@geotienda.com">info@geotienda.com</a>.</div>        

        <div class="h5 push-20-t push-20-l text-gris font-w600">3.1.- Confidencialidad de la información</div>

        <div class="h5 push-20-t push-20-l text-gris">Posterior a la primera compra realizada en GEOTIENDA, la Información Personal será utilizada únicamente en las formas establecidas en estas Políticas. Haremos todo lo que esté a nuestro alcance para proteger la privacidad de la información. Puede suceder que en virtud de órdenes judiciales, o de regulaciones legales, debamos revelar información a las autoridades o terceras partes bajo ciertas circunstancias, o bien en casos que terceras partes puedan interceptar o acceder a cierta información o transmisiones de datos en cuyo caso GEOTIENDA no responderá por la información que sea revelada.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">3.2.- Contraseña Personal</div>

        <div class="h5 push-20-t push-20-l text-gris">Para acceder a los servicios, los Usuarios dispondrán de una contraseña personal. Con ella podrán comprar productos o servicios en los Pet shop o Veterinarias adheridas a GEOTIENDA. Esta contraseña debe ser mantenida bajo absoluta confidencialidad y, en ningún caso, debe ser revelada o compartida con otras personas. El Usuario será responsable de todos los actos que tengan lugar mediante el uso de su email y contraseña, lo que incluye hacerse cargo del pago de las tarifas que eventualmente se devenguen por los pedidos de compra efectuados.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">3.3.- Recomendaciones generales sobre la protección de sus datos personales</div>

        <div class="h5 push-20-t push-20-l text-gris">Si bien GEOTIENDA se esfuerza en garantizar el resguardo y respeto de los datos personales que nos brinda, es recomendable informarle que usted también tiene un importante papel que desempeñar en la protección de su privacidad.</div>

        <div class="h5 push-20-t push-20-l text-gris">Sin embargo, dadas las condiciones de seguridad que ofrece hoy Internet, usted debe tener presente que, al divulgar su información personal on line, al momento de utilizar nuestro servicio, contratar con alguno de los comercios publicados que ofrece nuestro sitio web, dicha información puede ser recogida y utilizada por otros. Ante esto GEOTIENDA no será responsable por la difusión de información personal que pudieren realizar otros visitantes del sitio web, ni será responsable por los daños y perjuicios que dicha divulgación genere.</div>

        <div class="h5 push-20-t push-20-l text-gris">Si usted es menor de edad, le pedimos que solicite autorización a sus padres o tutores antes de revelar sus datos personales a GEOTIENDA.</div>

        <div class="h5 push-20-t push-20-l text-gris font-w600">3.4.- Confidencialidad - Revelación de datos personales</div>

        <div class="h5 push-20-t push-20-l text-gris">Si bien GEOTIENDA se compromete con usted a darle el tratamiento adecuado a sus datos personales, esta información podrá ser relevada a terceros a fin de cumplir con procedimientos legales, autoridades judiciales o administrativas o bien, usuarios damnificados que necesiten dicha información para el ejercicio de funciones propias de los poderes del Estado o en virtud de una obligación legal que derive de la ley.</div>        

        <div class="h5 push-20-t push-20-l text-gris font-w600">3.5.- PROPIEDAD INTELECTUAL</div>

        <div class="h5 push-20-t push-20-l text-gris">Los contenidos desarrollados por GEOTIENDA (videos, notas, fotos de archivo, diseños, logos, etc.) no pueden ser reproducidos, usados, adaptados o comercializados sin la aprobación escrita de GEOTIENDA ya que son de su propiedad. De ninguna manera y bajo ninguna circunstancia GEOTIENDA autoriza la utilización sin el consentimiento expreso e informado de su representante legal el uso de su logo y marca GEOTIENDA. La misma se encuentra protegida por la Ley de Marcas N° 22.362.</div>

        <div class="h4 push-50-t text-gris">MODIFICACIÓN A NUESTRAS POLÍTICAS DE PRIVACIDAD</div>

        <div class="h5 push-20-t push-20-l text-gris">GEOTIENDA se reserva el derecho y su exclusiva discreción de modificar, alterar, agregar o eliminar partes de estas Políticas de Seguridad y Privacidad en cualquier momento. Recomendamos que examine esta política periódicamente.</div>        

    </div>   
    <div class="text-center push-50-t push-50">
        <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
    </div>         
</section>