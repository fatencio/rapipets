<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-focus"> <!--<![endif]-->
    <head>
        <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,500,700' rel='stylesheet' type='text/css'>
        <meta charset="utf-8">

        <title>GeoTienda</title>

        <meta name="description" content="GeoTienda es la primer plataforma online que conecta a dueños de mascotas con los petshops y veterinarias las 24hs los 365 días del año">
        <meta name="author" content="Francisco Atencio - fatencio@gmail.com">
        <meta name="robots" content="noindex, follow">
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1.0">

        <!-- Icons -->
        <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon.png">

        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-16x16.png" sizes="16x16">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-32x32.png" sizes="32x32">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-96x96.png" sizes="96x96">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-160x160.png" sizes="160x160">
        <link rel="icon" type="image/png" href="<?php echo BASE_PATH ?>/assets/img/favicons/favicon-192x192.png" sizes="192x192">

        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo BASE_PATH ?>/assets/img/favicons/apple-touch-icon-180x180.png">
        <!-- END Icons -->

        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.css">     


        <!-- Bootstrap and OneUI CSS framework -->
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/css/bootstrap.min.css">
        <link rel="stylesheet" id="css-main" href="<?php echo BASE_PATH ?>/assets/css/oneui.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker3.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/select2/select2.min.css">
        <link rel="stylesheet" href="<?php echo BASE_PATH ?>/assets/js/plugins/select2/select2-bootstrap.min.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" id="css-geotienda" href="<?php echo BASE_PATH ?>/assets/custom/css/geotienda.css">

        <!-- Custom plugins CSS -->
        <link rel="stylesheet" id="css-spectrum" href="<?php echo BASE_PATH ?>/assets/custom/css/spectrum.css">
        <link rel="stylesheet" id="css-bootstrap-multiselect" href="<?php echo BASE_PATH ?>/assets/custom/css/bootstrap-multiselect.css">

        <!-- END Stylesheets -->

        <!-- Librerias GeoTienda v3 -->    
        <?php include 'librerias_js.php'; ?>          


        <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>

        <script>
        var recaptcha1;
        var recaptcha2;

        var onloadCallback = function() {
           recaptcha1 = grecaptcha.render('div_recaptcha_crear_cuenta', {'sitekey' : '6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF'});
           recaptcha2 = grecaptcha.render('div_recaptcha_contacto', {'sitekey' : '6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF'});
     //     grecaptcha.render('div_recaptcha_contacto_local', {'sitekey' : '6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF'});
        };

        </script>

        
    </head>
    <body class="fondo-huesos">

<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas oculto" type="button" id="btn_portada_posicion" onclick="portada_posicion();" ><i class="fa fa-save text-white"></i>
    </button>   

    <button class="btn btn-semitransparente-naranja pull-right portadas oculto push-50-l text-white" type="button" id="btn_portada_cancelar" onclick="cancela_portada();" >cancelar 
    </button>   
<?php endif ?>
        
        <!--  <div id="page-loader"></div> TODO: consultar si lo dejamos -->
        <!-- Page Container -->
        <!--
            Available Classes:

            'enable-cookies'             Remembers active color theme between pages (when set through color theme list)

            'sidebar-l'                  Left Sidebar and right Side Overlay
            'sidebar-r'                  Right Sidebar and left Side Overlay
            'sidebar-mini'               Mini hoverable Sidebar (> 991px)
            'sidebar-o'                  Visible Sidebar by default (> 991px)
            'sidebar-o-xs'               Visible Sidebar by default (< 992px)

            'side-overlay-hover'         Hoverable Side Overlay (> 991px)
            'side-overlay-o'             Visible Side Overlay by default (> 991px)

            'side-scroll'                Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (> 991px)

            'header-navbar-fixed'        Enables fixed header
            'header-navbar-transparent'  Enables a transparent header (if also fixed, it will get a solid dark background color on scrolling)
        -->
        <div id="page-container" class="header-navbar-fixed header-navbar-transparent">
            <!-- Header -->
            <header id="header-navbar" class="content-mini content-mini-full">
                <div class="content-boxed">

                    <!-- Main Header Navigation -->
                        <!-- USUARIO NO LOGGEADO -->
                        <div class="buttons-not-logged-in" style="display:none">
                            <ul class="js-nav-main-header nav-main-header pull-right push-5-t">     
                                <!-- MOBILE -->
                                <li class="visible-xs ">
                                    <a href="javascript:void(0);" id="btn_register_m" class="btn btn-sm btn-geotienda btn-geotienda-transparent btn-geotienda-mini">Creá tu cuenta</a>
                                </li>                        
                                <li class="visible-xs">
                                    <a href="javascript:void(0);" id="btn_login_m" class="btn btn-sm btn-geotienda btn-geotienda-mini">Ingresar</a>
                                </li>

                                <!-- TABLET y LAPTOP -->
                                <li class="hidden-xs">
                                    <a href="javascript:void(0);" id="btn_register" class="btn btn-geotienda btn-geotienda-transparent">Creá tu cuenta</a>
                                </li>                        
                                <li class="hidden-xs">
                                    <a href="javascript:void(0);" id="btn_login" class="btn btn-geotienda">Ingresar</a>
                                </li>
                            </ul>
                        </div>

                         <!-- USUARIO LOGGEADO -->
                        <div class="buttons-logged-in" style="display:none">
                            
                            <!-- Notificaciones -->
                            <ul class="nav-header pull-right">
                                <span class="badge badge-danger pull-right count-notificaciones-frontend" id="count_notificaciones"></span>

                                <li class="notificaciones-frontend">    
                                    <div class="btn-group">
                                        <button class="btn dropdown-toggle btn-semitransparente-naranja" data-toggle="dropdown" type="button" id="btn_notificaciones">
                                            <i class="fa fa-bell text-white"></i>
                                        </button>
                                        <ul class="dropdown-menu dropdown-menu-right list list-activity box-notificaciones" id="lista_notificaciones">                                                                
                                        </ul>
                                    </div>
                                </li>                
                            </ul> 
                            <!-- FIN Notificaciones -->   

                            <ul class="js-nav-main-header nav-main-header pull-right push-5-t">
                                
                                <!-- Contacto / Ayuda -->            
                                <li class="hidden-xs">
                                    <div> 
                                        <a class="font-w600 text-blanco" href="<?php echo site_url('/contacto') ?>">Contacto / Ayuda</a>
                                    </div>
                                </li>
                                <!-- FIN Contacto / Ayuda -->  

                                <!-- Avatar y drop-down menu -->
                                <li> 
                                    <a class="active nav-submenu menu-header" href="javascript:void(0)">
                                        <img id="useravatar" src="" >
                                        <span id="username"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-right menu-header-item">

                                    <?php if(isset($_SESSION['frontend_tipo']) && $_SESSION['frontend_tipo'] == 'local'): ?>
                                        
                                        <!-- MENU DASHBOARD LOCAL -->     
                                                                                 
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/pedidos">
                                                <i class="si si-basket pull-right icono-menu-header"></i>Pedidos
                                            </a>
                                        </li>  

                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/turnos">
                                                <i class="si si-calendar pull-right icono-menu-header"></i>Turnos
                                            </a>
                                        </li>   

                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/ventas">
                                                <i class="si si-bar-chart pull-right icono-menu-header"></i>Ventas
                                            </a>
                                        </li>  

                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/vacunas">
                                                <i class="si si-notebook pull-right icono-menu-header"></i>Vacunas
                                            </a>
                                        </li>
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/productos">
                                                <i class="si si-tag pull-right icono-menu-header"></i>Productos
                                            </a>
                                        </li>                                                
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/notificaciones">
                                                <i class="si si-bell pull-right icono-menu-header"></i>Notificaciones
                                            </a>
                                        </li>                                                                          
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/datos">
                                                <i class="si si-user pull-right icono-menu-header"></i>Datos de la Tienda
                                            </a>
                                        </li>
                                    <?php else: ?>
                                        <!-- MENU DASHBOARD CLIENTE -->
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/pedidos">
                                                <i class="si si-basket pull-right icono-menu-header"></i>Mis Pedidos
                                            </a>
                                        </li>  

                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/turnos">
                                                <i class="si si-calendar pull-right icono-menu-header"></i>Mis Turnos
                                            </a>
                                        </li>    
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/mascotas">
                                                <i class="si si-heart pull-right icono-menu-header"></i>Mis Mascotas
                                            </a>
                                        </li>                                                                                 
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/historial">
                                                <i class="si si-clock pull-right icono-menu-header"></i>Historial
                                            </a>
                                        </li>                                                
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/notificaciones">
                                                <i class="si si-bell pull-right icono-menu-header"></i>Notificaciones
                                            </a>
                                        </li>                                                                          
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/dashboard/datos">
                                                <i class="si si-user pull-right icono-menu-header"></i>Mis Datos
                                            </a>
                                        </li>
                                    <?php endif ?>

                                        <li class="divider">
                                        </li>        
                                        <li class="visible-xs">
                                            <a tabindex="-1" href="<?php echo site_url('/contacto') ?>">
                                                <i class="si si-envelope pull-right icono-menu-header"></i>Contacto / Ayuda
                                            </a>
                                        </li>                                                                                       
                                        <li>
                                            <a tabindex="-1" href="<?php echo BASE_PATH ?>/Inicio/logout">
                                                
                                                <i class="si si-logout pull-right icono-menu-header"></i>Salir
                                            </a>
                                        </li>
                                    </ul>
                                </li>  
                                <!-- FIN Avatar y drop-down menu -->
                            </ul>                            
                        </div>                    
                                    
                    </ul>
                    <!-- END Main Header Navigation -->

                    <!-- LOGO -->
                    <ul class="nav-header pull-left">
                        <li class="header-content logo-geotienda">
                            <?php if(isset($_SESSION['frontend_tipo']) && $_SESSION['frontend_tipo'] == 'local'): ?>
                                <a href="<?php echo BASE_PATH ?>/dashboard/pedidos">
                            <?php else: ?>
                                <a href="<?php echo BASE_PATH ?>">
                            <?php endif ?>                                

                            <img src="<?php echo BASE_PATH ?>/assets/img/frontend/logo-geotienda.png" class="logo-geotienda-responsive"></a>
                        </li>
                    </ul>
                    <!-- fin LOGO -->
                </div>
            </header>
            <!-- END Header -->

        </div>
        <!-- END Page Container -->


        <!-- Modal Usuario (cliente o local) inhabilitado -->
        <div class="modal fade" id="modal_usuario_bloqueado" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h2 class="text-center text-naranja">Error de acceso</h2>
                            <br/>
                            <h3 class="font-w300 text-center">Tu cuenta en GeoTienda se encuentra momentáneamente suspendida.</h3>
                            <br/><br/>
                            <h4 class="font-w300 text-center">Por favor comunicate con el administrador del sitio por medio de nuestro formulario de contacto</h4>

                            <br/><br/>

                            <div class="text-center">
                                <a class="btn btn-lg btn-geotienda" href="<?php echo site_url('/contacto') ?>" > Contactar</a>&nbsp;&nbsp;
                                <button class="btn btn-lg btn-default" type="button" data-dismiss="modal"> Cancelar</button>                                
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- END Modal Usuario (cliente o local) inhabilitado -->


        <!-- Frontend Welcome Modal -->
        <div class="modal fade" id="modal_welcome" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h2 class="text-center text-naranja">¡Felicidades!</h2>
                            <br/>
                            <h3 class="font-w300 text-center">Tu cuenta fue creada con éxito.</h3>
                            <br/><br/>
                            <h4 class="font-w300 text-center">Te enviamos un correo con tus datos de acceso y un link para activar tu cuenta.</h4>
                            <!--
                            <br/>
                            <h4 class="font-w300 text-center">Desde tu cuenta vas a poder gestionar tus datos personales, los de tu mascota, valorar una tienda y mucho más.</h4>
                        -->
                            <h5 class="font-w300 text-center text-muted push-20-t">Si no te llegó un email al buzón de entrada, por favor revisá tu correo no deseado.</h5>                            
                            <br/><br/>

                            <div class="text-center">
                                <button class="btn btn-lg btn-info btn-geotienda-celeste" id="btn_welcome_comenzar" type="button" data-dismiss="modal">Aceptar</button>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- END Welcome Modal -->    


        <!-- Frontend Cuenta Activada OK Modal -->
        <div class="modal fade" id="modal_activacion" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h2 class="text-center text-naranja">¡Felicidades!</h2>
                            <br/>
                            <h3 class="font-w300 text-center">Tu cuenta fue activada con éxito.</h3>
                            <br/><br/>
                            <h4 class="font-w300 text-center">Accede a tu cuenta con los datos que enviamos a tu correo y comienza a utilizar los servicios de GeoTienda.</h4>
                            <br/><br/>

                            <div class="text-center">
                                <button class="btn btn-lg btn-info btn-geotienda-celeste" id="btn_activacion" type="button" data-dismiss="modal" onclick="return show_login();">Comenzar</button>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- END Cuenta Activada OK Modal -->          


        <!-- Error no manejado Modal -->
        <div class="modal fade" id="modal_error_sistema" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="text-center text-naranja">Se ha producido un error</h3>
                            <br/>
                            <h4 class="font-w300 text-center">Por favor, intentá la operación nuevamente<br /> o recargá la página.</h4>
                            <br/>
                            <h5 class="font-w300 text-center">Si el error persiste, avisanos por medio del formulario de contacto.</h5>
                            <br/><br/>

                            <div class="text-center">
                                <button class="btn btn-lg btn-geotienda" type="button" data-dismiss="modal" onclick="location.reload();">Aceptar</button>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- FIN Error no manejado Modal -->


        <!-- Error customizable Modal -->
        <div class="modal fade" id="modal_error" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="text-center text-naranja" id="titulo_error">Titulo</h3>
                            <br/>
                            <h4 class="font-w300 text-center" id="texto_error">Texto</h4>
                            <br/><br/>

                            <div class="text-center">
                                <a href="<?php echo BASE_PATH ?>" class="btn btn-lg btn-info btn-geotienda-celeste" >Aceptar</a>
                            </div>
                            <br/>


                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- FIN Error customizable Modal -->


        <!-- Frontend Login Modal -->
        <div class="modal fade" id="modal_login" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-sm login-modal">
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                        
                            <h3 class="text-center">Ingresar a mi cuenta</h3>
                            <!-- Login Form -->
                            <form class="js-validation-login form-horizontal push-30-t push-20" action="#" id="form_login">
                                <!-- Login Errors -->
                                <div class="alert alert-danger text-center" id="div_login_error">
                                    <small>E-mail o contraseña incorrectos</small>
                                </div>                           
                                <!-- END Login Errors -->                            
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="text" id="frontend-login-email" name="frontend-login-email" placeholder="E-mail" value="<?php if (isset($email)) echo $email; ?>" required>
                                    </div>
                                </div>
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="password" id="frontend-login-password" name="frontend-login-password" placeholder="Contraseña"  required>
                                    </div>
                                </div>
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <div class="pull-left">
                                            <label class="css-input css-checkbox css-checkbox-warning css-checkbox-sm">
                                                <input type="checkbox" id="recordar" name="recordar"  class=" back-naranja" checked><span></span> Recordarme
                                            </label>
                                        </div>
                                        <div class="pull-right">
                                            <small><a class="olvide" href="<?php echo site_url('/solicitar_restablecer') ?>">Olvidé mi contraseña</a></small>
                                        </div>
                                    </div>
                                </div>   
                            </form>

                            <div class="push-10">
                                <div class="col-xs-12 push-10">
                                    <button class="btn btn-block btn-lg btn-geotienda" onclick="login()" > Ingresar</button>
                                </div>      
                                 <div class="col-xs-12 visible-xs push-10">
                                    <button class="btn btn-block btn-lg btn-default btn-geotienda-default" type="button" data-dismiss="modal">Cancelar</button>
                                </div>                                                                                        
                            </div>

                            <div class="text-center sin-cuenta push-20">
                                ¿Todavía no estás registrado?<br/>
                                <h5 class="push-5-t"><a href="javascript:void(0);" onclick="return show_register();">Creá tu cuenta</a></h5>
                           </div>                                

                            <!-- END Login Form -->

                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- END Login Modal -->       

        <!-- MODAL INFORMACION TURNO CLIENTE -->
        <div class="modal fade" id="modal_detalle_turno_cliente" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content modal-grande">
                    <div class="modal-header bg-success text-white">
                        <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="text-left">
                            <h3 class="modal-title-turno-nro-cliente"> Turno</h3>
                        </div>
                    </div>

                    <div class="modal-body form">
                        <div class="block-content ">

                            <!-- Servicio y Fecha -->
                            <div class="detalle-turno">
                                <div class="h4 push-5" style="margin-bottom:3px"><i class="fa fa-calendar"></i> <b id="turno_fecha_cliente"></b></div>
                                <div class="h5 push-5"><b id="turno_servicio_cliente"></b></div>   
                            </div>

                            <div class="row">
                              <!-- Datos Local -->
                              <div class="col-sm-2 detalle-venta centrado">
                                  <div class="text-center pull-left push-10-r push-10" id="turno_local_avatar_cliente"></div>           
                              </div>

                              <div class="col-sm-8 detalle-venta">
                                  <div class="h4 push-5">Tienda: <b id="turno_local_cliente"></b></div>
                                  <div class="h5 push-5">Titular: <b id="turno_titular_local_cliente"></b></div>                    
                                  <span style="display: none"><i class="fa fa-phone"></i><span id="turno_telefono_local_cliente"></span></span>
                                  <div class="h5 push-5"><i class="fa fa-map-marker"></i><span id="turno_direccion_local_cliente"></span></div>
                              </div>
                            </div>

                            <div class="clearfix"></div>


                            <!-- TURNO CANCELADO -->
                            <div class="row">
                              <div class="col-sm-9 detalle-venta centrado" style="display:none" id="div_turno_cancelado_cliente">
                                  <div class="h4 push-5">Cancelado por el <b id="turno_cancelado_por_cliente"></b></div>  
                                  <div class="h5 push-5">Motivo: <b id="turno_cancelado_motivo_cliente"></b></div>              
                              </div>
                            </div>            
                            <!-- fin TURNO CANCELADO -->

                            <div class="clearfix"></div>

                            <!-- Valoración -->
                            <div class="block" id="turno_valoracion_bloque_cliente">
                                <div class="block-content detalle-venta">
                                    <div class="h4 push-5">Valoración</div>
                                        <div class="h5 espacio-8" id="turno_puntaje_cliente"></div>
                                        <div class="h5 espacio-8">Comentario: <b id="turno_valoracion_cliente"></b></div>
                                </div>
                            </div>

                            <!-- COMENTARIOS -->
                                
                                <!-- Nuevo mensaje -->
                                <div class="h4 text-center font-w600 push-20 push-20-t text-naranja" id="turno_titulo_comentarios_cliente"></div>
                                <span id="bloque_add_comentario_turno_cliente">
                                  <h5 class="push-10-t push-5" id="turno_titulo_nuevo_comentario_cliente">Enviale un mensaje a</h5>
                                  <h6 class="push-5">(Por favor no uses lenguaje grosero o preguntes por otros servicios que no tengan que ver con éste, o por otras cosas)</h6>   
                                  <form action="#" id="form_comentario_turno_cliente" class="form-horizontal">
                                      <input type="hidden" value="" name="hd_turno_turno_id" id="hd_turno_turno_id"/> 
                                      <input type="hidden" value="" name="hd_turno_cliente_id" id="hd_turno_cliente_id"/> 
                                      <div class="form-body">
                                          
                                          <div class="form-group">
                                              <div class="col-md-12">
                                                  <textarea name="turno_comentario_cliente" id="turno_comentario_cliente" placeholder="Ingresá mensaje" class="form-control" rows="3"></textarea>
                                              </div>                                  
                                          </div>
                                          <div class="pull-right">
                                            <button type="button" class="btn btn-primary back-naranja" id="btnSaveComentarioTurnoCliente" onclick="save_comentario_turno_cliente()" >Aceptar</button>  
                                          </div>
                                      </div>                    
                                  </form>  
                                </span>                   
                                <!-- fin Nuevo mensaje -->

                                <div class="clearfix"></div>

                                <!-- Lista de mensajes  -->
                                <h5 class="push-20-t push-5">Historial de mensajes</h5>
                        </div>

                        <div class="block-content detalle-venta table-responsive">
                            <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                            
                            <table id="table_comentarios_turno_cliente" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:100px">Fecha</th>
                                        <th>Cliente / Tienda</th>
                                        <th>Comentario</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">                  
                    </div>  
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- fin MODAL INFORMACION TURNO CLIENTE --> 


        <!-- MODAL INFORMACION VENTA - CLIENTE -->
        <div class="modal fade" id="modal_detalle_venta_cliente" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content modal-grande">
                    <div class="modal-header back-naranja text-white">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="text-left">
                            <h3 class="modal-title-venta-nro"> Compra</h3>
                        </div>
                    </div>

                    <div class="modal-body form">
                        <!-- ENCABEZADO VENTA -->
                        <div class="block-content detalle-venta">
                            <!-- Datos Local -->
                            <div class="col-sm-2 detalle-venta centrado">
                                <div class="text-center push-10" id="venta_local_cliente_avatar_cliente"></div>                            
                            </div>

                            <div class="col-sm-5 detalle-venta">               
                                <div class="h4 push-5">Tienda: <b id="venta_local_cliente"></b></div>
                                <div class="h5 push-5">Titular: <b id="venta_titular_local_cliente"></b></div>     
                                <span  style="display: none"><i class="fa fa-phone"></i><span id="venta_telefono_local_cliente"></span></span>
                                <div class="h5 push-5"><i class="fa fa-map-marker"></i><span id="venta_direccion_local_cliente"></span></div>         
                            </div>

                            <div class="col-sm-5 detalle-venta">
                                <div class="h5 push-5">Fecha del pedido: <b id="venta_fecha_cliente"></b></div>
                                <div class="h5 push-5">Forma de pago: <b id="venta_condicion_venta_cliente"></b></div>
                                <div class="h5 push-5">Estado: <b id="venta_estado_cliente"></b></div>              
                            </div>                    
                        </div>

                        <div class="clearfix push-10"></div>

                        <!-- DETALLES VENTA -->
                        <div class="block-content detalle-venta table-responsive">
                            <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>
                            <table id="table_detalle_venta_cliente" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Artículo</th>
                                        <th>Presentación</th>
                                        <th>Cantidad</th>
                                        <th  style="min-width:100px;">Precio U.</th>
                                        <th>Subtotal</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th class="text-right" colspan="4">Total</th>
                                        <th class="text-right"></th>
                                    </tr>
                                </tfoot>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>   
                        <!-- fin DETALLES VENTA -->

                        <!-- DATOS ADICIONALES -->
                        <div class="block-content detalle-venta">
                            <div class="h4 push-5">Datos adicionales</div>                                   
                            <div class="h5 espacio-8">Envío: <b id="venta_tipo_envio_cliente"></b></div>
                            <div class="h5 espacio-8" id="div_venta_cliente_direccion_cliente">Dirección: <b id="venta_cliente_direccion_cliente"></b></div>
                            <div class="h5 espacio-8" id="div_venta_cliente_ciudad_cliente">Ciudad: <b id="venta_cliente_ciudad_cliente"></b></div>
                        </div>

                        <!-- Pedido cancelado -->
                        <div class="block-content detalle-venta" id="div_venta_cancelado_cliente">
                              <div class="h5 push-5">Cancelado por el <b id="venta_cancelado_por_cliente"></b></div>  
                              <div class="h5 push-5">Motivo: <b id="venta_cancelado_motivo_cliente"></b></div>              
                        </div>     

                        <div class="clearfix"></div>                       

                        <!-- Valoración -->
                        <div class="block-content detalle-venta" id="venta_valoracion_cliente_bloque_cliente">
                            <div class="h4 push-5">Valoración</div>
                            <div class="h5 espacio-8" id="venta_puntaje_cliente"></div>
                            <div class="h5 espacio-8">Comentario: <b id="venta_valoracion_cliente"></b></div>
                        </div>
                        <!-- fin DATOS ADICIONALES -->

                        <!-- COMENTARIOS -->
                        <div class="block-content detalle-venta">
                            
                            <!-- Nuevo mensaje -->
                            <div class="h4 text-center font-w600 push-20 push-20-t text-naranja" id="venta_titulo_comentarios_cliente"></div>
                            <span id="bloque_add_comentario_venta_cliente">
                              <h5 class="push-10-t push-5" id="venta_titulo_nuevo_comentario_cliente">Enviale un mensaje a</h5>
                              <h6 class="push-5">(Por favor no uses lenguaje grosero o preguntes por otros artículos)</h6>   
                              <form action="#" id="form_comentario_venta_cliente" class="form-horizontal">
                                  <input type="hidden" value="" name="hd_cliente_venta_id" id="hd_cliente_venta_id"/> 
                                  <input type="hidden" value="" name="hd_cliente_id" id="hd_cliente_id"/> 
                                  <div class="form-body">
                                      
                                      <div class="form-group">
                                          <div class="col-md-12">
                                              <textarea name="venta_comentario_cliente" id="venta_comentario_cliente" placeholder="Ingresá mensaje" class="form-control" rows="3"></textarea>
                                          </div>                                  
                                      </div>
                                      <div class="pull-right">
                                        <button type="button" class="btn btn-primary back-naranja" id="btnSaveComentarioVentaCliente" onclick="save_comentario_venta_cliente()" >Aceptar</button> 
                                      </div>
                                  </div>                    
                              </form>  
                            </span>                   
                            <!-- fin Nuevo mensaje -->

                            <div class="clearfix"></div>                            

                            <!-- Lista de mensajes  -->
                            <h5 class="push-20-t push-5">Historial de mensajes</h5>
                        </div>

                        <div class="block-content detalle-venta table-responsive">
                            <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                            
                            <table id="table_comentarios_venta_cliente" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th style="width:100px">Fecha</th>
                                        <th>Cliente / Tienda</th>
                                        <th>Comentario</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- FIN Lista de mensajes  -->
                        <!-- fin COMENTARIOS -->
                    </div>

                    <div class="modal-footer">                    
                    </div>                    
                </div>
            </div><!-- /.modal-dialog -->
        </div>
        <!-- fin MODAL INFORMACION VENTA - CLIENTE -->


        <!-- MODAL PROXIMA VACUNA CLIENTE -->
        <div class="modal fade" id="modal_proxima_vacuna_cliente" role="dialog">
            <div class="modal-dialog" >
                <div class="modal-content modal-grande">
                    <div class="modal-header back-vacunas text-white">
                        <div class="text-left">
                            <h3>Vacuna agendada</h3>
                        </div>
                    </div>
                    <div class="block-content ">

                        <!-- Datos del Local -->
                        <div class="col-sm-2 detalle-venta centrado">
                            <div class="text-center pull-left push-10-r push-10" id="proxima_vacuna_local_avatar_cliente"></div>    
                        </div>

                        <div class="col-sm-8 detalle-venta centrado">
                            <div class="h4 push-5">Tienda: <b id="proxima_vacuna_local_cliente"></b></div>
                            <div class="h5 push-5">Titular: <b id="proxima_vacuna_titular_local_cliente"></b></div>                    
                            <span style="display: none"><i class="fa fa-phone"></i><span id="proxima_vacuna_telefono_local_cliente"></span></span>
                            <div class="h5 push-5"><i class="fa fa-map-marker"></i><span id="proxima_vacuna_direccion_local_cliente"></span></div>
                        </div>

                        <div class="clearfix"></div>

                        <!-- Nombre y Fecha -->
                        <div class="detalle-vacuna push-10-l push-10-t">
                            <div class="h4 push-5">Fecha agendada: <b id="proxima_vacuna_fecha_cliente"></b></div>
                            <div class="h4 push-5">Mascota: <b id="proxima_vacuna_mascota_cliente"></b></div>   
                            <div class="h4 push-5">Vacuna: <b id="proxima_vacuna_nombre_cliente"></b></div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="form-group form-group-login">
                            <div class="text-center">
                                <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>
                            </div>    
                        </div>  
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- fin MODAL PROXIMA VACUNA CLIENTE -->

        
        <!-- MODAL INFORMACION VACUNA CLIENTE -->
        <div class="modal fade" id="modal_detalle_vacuna_cliente" role="dialog">
            <div class="modal-dialog" >
                <div class="modal-content modal-grande">
                    <div class="modal-header back-vacunas text-white">
                        <div class="text-left">
                            <h3>Detalles vacuna</h3>
                        </div>
                    </div>
                    <div class="block-content ">

                        <!-- Datos del Local -->
                        <div class="col-sm-2 detalle-venta centrado">
                            <div class="text-center pull-left push-10-r push-10" id="vacuna_local_avatar_cliente"></div>    
                        </div>

                        <div class="col-sm-8 detalle-venta centrado">
                            <div class="h4 push-5">Tienda: <b id="vacuna_local_cliente"></b></div>
                            <div class="h5 push-5">Titular: <b id="vacuna_titular_local_cliente"></b></div>                    
                            <span style="display: none"><i class="fa fa-phone"></i><span id="vacuna_telefono_local_cliente"></span></span>
                            <div class="h5 push-5"><i class="fa fa-map-marker"></i><span id="vacuna_direccion_local_cliente"></span></div>
                        </div>

                        <div class="clearfix"></div>

                        <!-- Nombre y Fecha -->
                        <div class="detalle-vacuna push-10-l push-10-t">
                            <div class="h4 push-5" style="margin-bottom:3px">Fecha aplicación: <b id="vacuna_fecha_cliente"></b></div>
                            <div class="h4 push-5"></i>Mascota: <b id="vacuna_mascota_cliente"></b></div>   
                            <div class="h4 push-5"></i>Vacuna: <b id="vacuna_nombre_cliente"></b></div>
                        </div>

                        <div class="clearfix"></div>

                        <!-- Detalles -->
                        <div class="block">
                            <div class="block-content detalle-vacuna">
                                <div class="h5 push-5">Peso: <b id="vacuna_peso_cliente"></b></div>                              
                                <div class="h5 push-5">Aplicada: <b id="vacuna_aplicada_cliente"></b></div>
                                <div class="h5 push-5">Veterinario: <b id="vacuna_veterinario_cliente"></b></div>  
                                <div class="h5 push-5">Próxima vacuna: <b id="vacuna_proxima_vacuna_cliente"></b></div>  
                                <div class="h5 espacio-8">Observaciones: <b id="vacuna_observaciones_cliente"></b></div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr>
                        <div class="form-group form-group-login">
                            <div class="text-center">
                                <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>
                            </div>    
                        </div>  
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div>
        <!-- fin MODAL INFORMACION VACUNA CLIENTE -->        


        <!-- MODAL PORTADA -->
        <div class="modal fade" id="modal_portada" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title-add-edit-mascota">Seleccionar portada</h3>
                    </div>
                    <div class="form push-20-t">
                        <form action="#" id="form_portada" class="form-horizontal" enctype="multipart/form-data">
                            <!-- Div Error -->
                            <div class="alert alert-danger alert-dismissable push-10-l push-10-r" id="div_portada_error" style="display:none">
                                <small id="portada_error_message"></small>                 
                            </div>
                            <!-- END Div Error -->  

                            <input type="hidden" value="" name="item"/> 
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-2"></label>
                                    <div class="col-md-8">
          
                                        <input name="foto_portada" type="file" id="foto_portada" size="40" class="btn btn-default block-table" >  
                                        
                                        <button type="button" id="btn_mostrar_portadas" onclick="mostrar_portadas()" class="btn btn-default push-10-t block-table">o elegí una foto ya cargada</button>  
                                        <input type="hidden" id="foto_portada_anterior" name="foto_portada_anterior" />
                                        <input type="hidden" id="foto_portada_precargada" name="foto_portada_precargada" />
                                        <input type="hidden" id="foto_portada_posicion" name="foto_portada_posicion" />
                                        <input type="hidden" id="pagina_portada" name="pagina_portada" />
                                    </div>
                                    <label class="control-label col-md-2"></label>
                                </div>                                             
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="btn_add_edit_mascota" onclick="save_portada('<?php echo $rutaImagen; ?>')" class="btn btn-success push-10-r">Aceptar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- fin MODAL PORTADA -->

        <!-- MODAL PORTADAS -->
        <div class="modal fade" id="modal_portadas" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content modal-grande" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title-imagen">Portadas actuales</h3>
                    </div>
                    <div class="modal-body form">

                        <div class="row items-push js-gallery-advanced">
                            <?php
                        
                            $map = glob("assets/img/portadas/*");

                            foreach($map as $m)
                            {
                                $nombreArray=explode("/", $m);
                                $nombreImagen=$nombreArray[3];

                                $imagen = BASE_PATH . '/' . $m;
                            ?>
                                <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                                    <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                        <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                        <div class="img-options">
                                            <div class="img-options-content">
                                                <div class="btn-group push-50-t">
                                                    <a class="btn btn-sm btn-info push-10-r" href="javascript:void(0)" onclick="return elegir_portada('<?php echo $nombreImagen; ?>', '<?php echo $rutaImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                                    <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminarImagen('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php }  // end foreach
                            ?>
                            
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- fin MODAL PORTADAS --> 

        <!-- Frontend Register Modal -->
        <div class="modal fade" id="modal_register" role="dialog">
            <div class="modal-dialog modal-sm login-modal" id="modal_register_content" >
                <div class="modal-content">
                    <div class="block block-themed block-transparent remove-margin-b">
                        <div class="block-content geotienda-modal-content">
                            <ul class="block-options login-close" >
                                <li>
                                    <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                                </li>
                            </ul>                          
                            <h3 class="text-center">Creá tu cuenta</h3>                        

                            <!-- Register Form -->
                            <form class="js-validation-login form-horizontal push-30-t push-20" action="#" id="form_register">
                                <!-- Register Error -->
                                <div class="alert alert-danger alert-dismissable" id="div_register_error">
                                    <small id="register_error_message"></small>                 
                                </div>
                                <!-- END Register Error -->                            
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                            <input class="form-control" type="text" id="frontend-register-nombre" name="frontend-register-nombre" placeholder="Nombre" value="<?php if (isset($registro_nombre)) echo $registro_nombre; ?>"  required>
                                    </div>
                                </div>       
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="text" id="frontend-register-apellido" name="frontend-register-apellido" placeholder="Apellido" value="<?php if (isset($registro_apellido)) echo $registro_apellido; ?>"  required>
                                    </div>
                                </div>                                                       
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="text" id="frontend-register-email" name="frontend-register-email" placeholder="E-mail" value="<?php if (isset($registro_email)) echo $registro_email; ?>"  required>
                                    </div>
                                </div>
                                <div class="form-group form-group-login">
                                    <div class="col-xs-12">
                                        <input class="form-control" type="password" id="frontend-register-password" name="frontend-register-password" placeholder="Contraseña"  required>
                                    </div>
                                </div>


                                <div class="form-group form-group-login">                                    
                                    <div class="col-xs-12">
                                        <label class="css-input css-checkbox css-checkbox-warning css-checkbox-sm">
                                            <input type="checkbox" id="frontend-register-terms" name="frontend-register-terms" checked="checked" value="si" class="back-naranja"><span></span> Acepto los 
                                        </label><small><a href="<?php echo site_url('/terminos') ?>" target="_blank"> Términos y condiciones</a></small>
                                    </div>                                    
                                </div>

                                <div class="form-group form-group-login recaptcha" id="">
                                    <div id="div_recaptcha_crear_cuenta" class="g-recaptcha" data-sitekey="6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF"></div>
                                </div>                             
                            </form>

                            <div class="push-10 clearfix">
                                <div class="col-xs-12 push-10">
                                    <button type="button" id="btn_crear_cuenta" class="btn btn-block btn-lg btn-geotienda" onclick="register();" > Crear cuenta</button>
                                </div>    
                                <div class="col-xs-12 visible-xs push-10">
                                    <button class="btn btn-block btn-lg btn-default btn-geotienda-default" type="button" data-dismiss="modal">Cancelar</button>
                                </div>                                                                    
                            </div>

                            <div class="text-center sin-cuenta push-20">
                                ¿Ya tenés una cuenta?
                                <h5 class="push-5-t"><a href="javascript:void(0);" onclick="return show_login();"> Ingresar</a></h5>
                            </div>                                      

                            <!-- END Register Form -->
                            
                        </div>
                    </div>
                </div>
            </div>
        </div> 
        <!-- END Register Modal -->    

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.slimscroll.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.scrollLock.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.appear.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.countTo.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/jquery.placeholder.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/core/js.cookie.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/app.js"></script>

        <!-- Page Plugins -->
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/slick/slick.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/chartjs/Chart.min.js"></script>
        
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/ckeditor.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/ckeditor/adapters/jquery.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-notify/bootstrap-notify.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/datatables/jquery.dataTables.min.js"></script>  
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/select2/select2.full.min.js"></script>
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/plugins/jquery-ui/jquery-ui.js"></script>

        <!-- Custom Plugins -->
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.remote.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.chained.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/bootstrap-switch.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/jquery.sticky-kit.min.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/spectrum.js"></script>
        <script src="<?php echo BASE_PATH; ?>/assets/custom/js/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyC0mza7rGNlOmcKg-5JF1NB3nUKqHFYMBk&sensor=false&v=3&libraries=geometry"></script>
        
            
        <script type="text/javascript" src="<?php echo BASE_PATH ?>/assets/js/pages/base_tables_datatables.js"></script>

        <script>
            jQuery(function () {
                // Init page helpers
                // ckeditor --> Ritch text editor, usado en vista Articulos
                App.initHelpers('ckeditor');
                App.initHelpers('table-tools');
                App.initHelpers('datepicker');         
                App.initHelpers('select2');         
            });
        </script>

        <script>
            /* Variables globales */
            var direccion;
            var localidad;        

            var pantalla_retorno = '';
            var usuario_logged_in = false;
            var user_id;
            var tipo = 'cliente';  // cliente o local
            var tab; // tab activo 
            var notificaciones_actual; // cantidad de notificaciones no leidas
            var portada_anterior;
            var portada_actual;
            var posicion_actual;

            $(document).ready(function() {

                // Para manejo personalizado de errores en los datatables
                $.fn.dataTable.ext.errMode = 'none';

                $('.buttons-logged-in').hide();
                $('.buttons-not-logged-in').show();

                // Usuario con sesion iniciada   
                <?php if (isset($_SESSION['frontend_logged_in'])){ ?>             

                    $('.buttons-logged-in').show();
                    $('.buttons-not-logged-in').hide();

                    $('#username').html('&nbsp;<?php echo $_SESSION['frontend_nombre']; ?>&nbsp;&nbsp;');
                    $('#useravatar').attr('src', '<?php echo BASE_PATH ?>/assets/img/<?php echo $_SESSION['frontend_tipo']; ?>/miniaturas/<?php echo $_SESSION['frontend_avatar']; ?>');

                    usuario_logged_in = true;
                    user_id = <?php echo $_SESSION['frontend_user_id']; ?>        

                    // Verifica si hay nuevas notificaciones
                    check_notificaciones();                                                   

                <?php } ?>

                // Valida si hay algun error para mostrar
                <?php if (isset($show_error)){ ?>    
                    show_error('<?php echo $show_error ?>');
                <?php } ?>         

                // Valida si hay algun modal para mostrar
                <?php if (isset($show_modal)){ ?>    
                    show_modal('<?php echo $show_modal ?>');
                <?php } ?>         


                $('.portadas').hide();

                <?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
                    $('.portadas').show();

                    portada_actual = '<?php echo $portada; ?>';
                    posicion_actual = '<?php echo $portada_posicion; ?>';
                <?php endif ?>                     
            });


            /* LOGIN y REGISTRO */
            $('#btn_login_m').click(function(e) {
                show_login();
            });            

            $('#btn_login').click(function(e) {
                show_login();
            });  

            function show_login()
            {
                $('#modal_register').modal('hide');

                $('#frontend-login-email').val('');
                $('#frontend-login-password').val('');
                $('#div_login_error').hide();
                $('#div_login_error_sistema').hide();

                $('#modal_login').modal('show');
            }


            function show_error(error)
            {
                switch (error)
                {
                    case 'activar':
                         $('#titulo_error').html('Error al activar cuenta');   
                         $('#texto_error').html('El link no es válido o tu cuenta ya se encuentra activada.');                          
                    break;                    

                    case 'restablecer':
                         $('#titulo_error').html('Error al restablecer contraseña');   
                         $('#texto_error').html('El link no es válido o ha expirado.');                          
                    break;                    
                }

                $('#modal_error').modal({ backdrop: 'static', keyboard: false }) // Impide cerrar el modal haciendo clic fuera de él
                $('#modal_error').modal('show');
            }


            function show_modal(modal)
            {
                switch (modal)
                {
                    case 'activar':
                        $('#modal_activacion').modal({ backdrop: 'static', keyboard: false }) // Impide cerrar el modal haciendo clic fuera de él
                        $('#modal_activacion').modal('show');
                    break;                    

                }
            }


            // Al mostrar el modal del login, hace foco en el primer input
            $('#modal_login').on('shown.bs.modal', function () {
                $('#frontend-login-email').focus();
            })


            // Tecla enter en inputs del login
            $('#frontend-login-email').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-login-email').val() != '') $('#frontend-login-password').focus();
                }
            });


            $('#frontend-login-password').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-login-password').val() != '') login();
                }
            });



            $('#btn_register_m').click(function(e) {
                show_register();
            });            


            $('#btn_register').click(function(e) {
                show_register();
            });   


            function show_register(){

                $('#modal_login').modal('hide');

                $('#frontend-register-nombre').val('');
                $('#frontend-register-apellido').val('');
                $('#frontend-register-email').val('');
                $('#frontend-register-password').val('');
                $('#div_register_error').hide();

                $('#modal_register').modal('show');
            }            


            // Al mostrar el modal de registro, hace foco en el primer input
            $('#modal_register').on('shown.bs.modal', function () {
                $('#frontend-register-nombre').focus();
            })


            // Tecla enter en inputs del modal de registro
            $('#frontend-register-nombre').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-register-nombre').val() != '') $('#frontend-register-apellido').focus();
                }
            });


            $('#frontend-register-apellido').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-register-apellido').val() != '') $('#frontend-register-email').focus();
                }
            });


            $('#frontend-register-email').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-register-email').val() != '') $('#frontend-register-password').focus();
                }
            });


            $('#frontend-register-password').keypress(function(event) {
                if (event.which == 13) {
                    event.preventDefault();
                    
                    if($('#frontend-register-password').val() != '') register();
                }
            });


            function show_welcome(){

                $('#modal_welcome').modal('show');
            }            


            // Al mostrar el modal de registro exitoso, hace foco en el boton 'Aceptar'
            $('#modal_welcome').on('shown.bs.modal', function () {
                $('#btn_welcome_comenzar').focus();
            })


            // Al mostrar el modal de activación exitosa, hace foco en el boton 'Comenzar'
            $('#modal_activacion').on('shown.bs.modal', function () {
                $('#btn_activacion').focus();
            })


            // Al cerrar la pantalla de activación exitosa, valida si el cliente estaba finalizando un pedido
            /*
            $('#modal_activacion').on('hidden.bs.modal', function () {

                if (pantalla_retorno != ''){

                    switch (pantalla_retorno){

                        case 'pedido':
                            pantalla_retorno = '';                                    
                            finalizar_pedido();
                        break;
                   
                        case 'turno':
                            pantalla_retorno = '';
                            agendar_turno_desde_login();                                        
                        break;  
                    }
                }   
            })
            */


            function login()
            {
                var url = "<?php echo BASE_PATH ?>/Inicio/login";

                $.ajax({
                    url : url ,
                    type: "POST",
                    data: $('#form_login').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                        // Login ok
                        if(data.status) 
                        {
                            $('#modal_login').modal('hide');

                            $('.buttons-logged-in').show();
                            $('.buttons-not-logged-in').hide();

                            $('#username').html('&nbsp;&nbsp;' + data.username + '&nbsp;&nbsp;');
                            $('#useravatar').attr('src', '<?php echo BASE_PATH ?>/assets/img/' + data.tipo + '/miniaturas/' + data.avatar);

                            usuario_logged_in = true;
                            user_id = data.user_id;

                            // Verifica si hay nuevas notificaciones
                            check_notificaciones();  
                            setInterval(check_notificaciones, <?php echo $this->config->item('intervalo_notificaciones'); ?>);

                            // Si es login de un local, redirecciona a su dashboard
                            if (data.tipo == 'local') window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos'; 

                            // Redirecciona o abre modal si es necesario
                            if (pantalla_retorno != '')
                            {
                                switch (pantalla_retorno)
                                {
                                    case 'pedido':
                                        pantalla_retorno = '';
                                        finalizar_pedido();
                                    break;

                                    case 'turno':
                                        pantalla_retorno = '';
                                        agendar_turno_desde_login();                                        
                                    break;     

                                    case 'valoraciones':
                                        pantalla_retorno = '';
                                        location.reload();
                                    break;     

                                    case 'pedidos':
                                        pantalla_retorno = '';
                                        window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos';
                                    break;                                      

                                    case 'turnos':
                                        pantalla_retorno = '';
                                        window.location.href ='<?php echo BASE_PATH ?>/dashboard/turnos';
                                    break;                                                                          
                                }
                            }

                            // si es administrador, recarga pagina para validar variables de sesion
                            if (data.frontend_admin == 1) location.reload();
                        }
                        // Fallo en login
                        else
                        {
                            // Login correcto, pero el usuario se encuentra bloqueado
                            if (data.error == 'bloqueado'){
                                $('#modal_login').modal('hide');
                                $('#modal_usuario_bloqueado').modal('show');
                            }
                            // Login incorrecto
                            else{
                                $('#div_login_error').show();                            
                            }
                        }
                    },
                    // Error no manejado
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('#modal_login').modal('hide');
                        $('#modal_error_sistema').modal('show');
                        console.log(errorThrown);
                    }
                });
            }


            function register()
            {
                $('#btn_crear_cuenta').text('Procesando...');
                $('#btn_crear_cuenta').attr('disabled', true); 

                var url = "<?php echo BASE_PATH ?>/Inicio/cliente_registrar";

                $.ajax({
                    url : url ,
                    type: "POST",
                    data: $('#form_register').serialize(),
                    dataType: "JSON",
                    success: function(data)
                    {
                        // Register ok
                        if(data.status) 
                        {
                            usuario_logged_in = false;
                            user_id = data.user_id;

                            $('#modal_register').modal('hide');
                            $('#modal_welcome').modal('show');
                        }
                        // Fallo en registro
                        else
                        {
                            switch (data.error){

                                // Error en la transacción (proceso de registro y notificación)
                                case 'transaction':
                                    $('#modal_register').modal('hide');
                                    $('#modal_error_sistema').modal('show');
                                    console.log('Error en proceso de registro - Inicio->cliente_registrar');
                                break;

                                // Error en el envio del mail de bienvenida
                                case 'mail':
                                    $('#modal_register').modal('hide');
                                    $('#modal_error_sistema').modal('show');
                                    console.log('Error en envío de mail - Inicio->cliente_registrar');
                                break;

                                // Datos de registro incorrectos
                                default:
                                    mensaje_error = '';

                                    for (var i = 0; i < data.error.mensaje.length; i++) 
                                    {
                                        mensaje_error += data.error.mensaje[i] + '<br/>';
                                    }

                                    $('#register_error_message').html(mensaje_error);
                                    $('#div_register_error').show();     
                                break;                              
                            }
                        }

                        $('#btn_crear_cuenta').text('Crear cuenta');
                        $('#btn_crear_cuenta').attr('disabled', false);   
                    },
                    // Error no manejado
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('#modal_register').modal('hide');
                        $('#modal_error_sistema').modal('show');
                        console.log(errorThrown);

                        $('#btn_crear_cuenta').text('Crear cuenta');
                        $('#btn_crear_cuenta').attr('disabled', false);                           
                    }
                });

                $('#modal_login').modal('hide');

           
            }            
            /* fin LOGIN y REGISTRO */


            /* AVISOS notify */
            function aviso(tipo, mensaje, titulo, posicion = 'top')
            {
                // Para mobile muestra siempre las notificaciones abajo
                if ($(window).width() < 768) posicion = 'bottom';

                var icono;

                switch(tipo){
                    case 'success':
                        icono = 'fa fa-check';
                    break;

                   case 'danger':
                        icono = 'fa fa-times';
                    break;                
                }

                $.notify({
                    // options
                    icon: icono,
                    title: titulo,
                    message: mensaje 
                },{
                    // settings
                    type: tipo,
                    placement: {
                        from: posicion,
                        align: "center"
                    },    
                    template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                        '<span data-notify="title" style="margin-bottom:8px;" class="clearfix">{1}</span> ' +
                        '<span data-notify="icon"></span> ' +                    
                        '<span data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'                     
                });   
            }
            /* FIN AVISOS notify */        


            /* FUNCIONES VARIAS */

            // Oculta cualquier modal al hacer 'back' en el browser o celular
            $(".modal").on("shown.bs.modal", function()  { 
                var urlReplace = "#" + $(this).attr('id'); 
                history.pushState(null, null, urlReplace); 
              });

              $(window).on('popstate', function() { 
                $(".modal").modal('hide');
              });


              /* fin FUNCIONES VARIAS */    

            /* INFORMACION DE UNA VACUNA AGENDADA - CLIENTE*/
            function detalle_vacuna_agendada_cliente(vacuna_id)
            {
                $.ajax({
                  url : "<?php echo BASE_PATH ?>/vacuna/get_by_id/" + vacuna_id,
                  cache: false,
                  success: function(data){

                    vacuna = jQuery.parseJSON(data);

                    // Datos de la vacuna
                    $('#proxima_vacuna_nombre_cliente').text(vacuna.nombre);
                    $('#proxima_vacuna_fecha_cliente').text(vacuna.fecha_proxima);
                    $('#proxima_vacuna_mascota_cliente').text(vacuna.mascota);

                    // Datos del local
                    $("#proxima_vacuna_local_avatar_cliente").html('');
                    $("#proxima_vacuna_local_avatar_cliente").append('<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + vacuna.local_avatar + '" width="100%">');

                    $('#proxima_vacuna_local_cliente').text(vacuna.local);
                    $('#proxima_vacuna_titular_local_cliente').text(vacuna.local_nombre_titular + ' ' + vacuna.local_apellido_titular);
                    $('#proxima_vacuna_telefono_local_cliente').text(' ' + vacuna.local_telefono);
                    $('#proxima_vacuna_direccion_local_cliente').text(' ' + vacuna.local_direccion + ', ' + vacuna.local_localidad);
                           
                    $('#modal_proxima_vacuna_cliente').modal('show');                            

                  } 
                });

            }
            /* FIN INFORMACION DE UNA VACUNA - CLIENTE */


            /* INFORMACION DE UNA VACUNA - CLIENTE*/
            function detalle_vacuna_cliente(vacuna_id)
            {
                $.ajax({
                  url : "<?php echo BASE_PATH ?>/vacuna/get_by_id/" + vacuna_id,
                  cache: false,
                  success: function(data){

                    vacuna = jQuery.parseJSON(data);

                    if (vacuna)
                    {
                        // Datos de la vacuna
                        $('#vacuna_nombre_cliente').text(vacuna.nombre);
                        $('#vacuna_fecha_cliente').text(vacuna.fecha);
                        $('#vacuna_mascota_cliente').text(vacuna.mascota);

                        // Datos del local
                        $("#vacuna_local_avatar_cliente").html('');
                        $("#vacuna_local_avatar_cliente").append('<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + vacuna.local_avatar + '" width="100%">');

                        $('#vacuna_local_cliente').text(vacuna.local);
                        $('#vacuna_titular_local_cliente').text(vacuna.local_nombre_titular + ' ' + vacuna.local_apellido_titular);
                        $('#vacuna_telefono_local_cliente').text(' ' + vacuna.local_telefono);
                        $('#vacuna_direccion_local_cliente').text(' ' + vacuna.local_direccion + ', ' + vacuna.local_localidad);

                        // Datos adicionales               
                        $('#vacuna_aplicada_cliente').text(vacuna.aplicada);
                        $('#vacuna_veterinario_cliente').text(vacuna.veterinario);
                        $('#vacuna_proxima_vacuna_cliente').text(vacuna.fecha_proxima);

                        if (vacuna.peso != '0') $('#vacuna_peso_cliente').text(vacuna.peso);

                        $('#vacuna_observaciones_cliente').text(vacuna.observaciones);
                        
                        $('#modal_detalle_vacuna_cliente').modal('show');
                    }
                    else
                    {
                        alert('Mascota no encontrada');
                    }
                  } 
                });
            }


            // Permite mostrar un modal sobre otro
            // Es necesario que el modal que se muestra sobre otro modal esté ANTES en el html            
            $('#modal_detalle_vacuna_cliente').on('show.bs.modal', function () 
            {
              $('#modal_cartilla_mascota').removeClass('muestra-modal');
              $('#modal_cartilla_mascota').addClass('oculta-modal');
            })

            $('#modal_detalle_vacuna_cliente').on('hide.bs.modal', function () 
            {
              $('#modal_cartilla_mascota').removeClass('oculta-modal');
              $('#modal_cartilla_mascota').addClass('muestra-modal');
            })

            /* FIN INFORMACION DE UNA VACUNA - CLIENTE */


            /* INFORMACION DE UN TURNO - CLIENTE*/
            function detalle_turno_cliente(turno_id)
            {
                $("#turno_local_avatar_cliente").html('');
                $("#turno_comentario_cliente").val('');

                $.ajax({
                  url : "<?php echo BASE_PATH ?>/admin/turno/get_by_id/" + turno_id,
                  cache: false,
                  success: function(data){

                    turno = jQuery.parseJSON(data);

                    // Datos de la turno
                    $('.modal-title-turno-nro-cliente').text('Turno ' + turno.numero + ' (' + turno.estado + ')');
                    $('#modal_detalle_turno_cliente').modal('show'); 

                    $('#turno_servicio_cliente').text(turno.servicio  + ' - ' + turno.servicio_item  + ' ($' + turno.total + ')');
                    $('#turno_fecha_cliente').text(turno.fecha + ' a las ' + turno.hora + ' hs');

                    // Datos del local
                    $("#turno_local_avatar_cliente").append('<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + turno.local_avatar + '" width="100%">');

                    $('#turno_local_cliente').text(turno.local);
                    $('#turno_titulo_nuevo_comentario_cliente').text('Enviale un mensaje a ' + turno.local);
                    $('#turno_titular_local_cliente').text(turno.local_nombre_titular + ' ' + turno.local_apellido_titular);
                    $('#turno_telefono_local_cliente').text(' ' + turno.local_telefono);
                    $('#turno_direccion_local_cliente').text(' ' + turno.local_direccion + ', ' + turno.local_localidad);

                    // Datos adicionales
                    $('#turno_tipo_envio').text(turno.tipo_envio);
                    $('#turno_cliente_direccion').text(turno.cliente_direccion);
                    $('#turno_cliente_ciudad').text(turno.cliente_localidad + ', ' +  turno.cliente_provincia);

                    // Valoración
                    if (turno.puntaje)
                    {
                      $('#turno_puntaje_cliente').html(turno.puntaje);
                      $('#turno_valoracion_cliente').text(turno.valoracion);
                      $('#turno_valoracion_bloque_cliente').show();
                    }
                    else
                    {
                      $('#turno_valoracion_bloque_cliente').hide();
                    }

                    // Turno cancelado
                    if (turno.cancelado_por != '')
                    {
                        $('#div_turno_cancelado_cliente').show();
                        $('#turno_cancelado_por_cliente').text(turno.cancelado_por);
                        $('#turno_cancelado_motivo_cliente').text(turno.cancelado_comentario);          
                    }
                    else
                    {
                        $('#div_turno_cancelado_cliente').hide();
                    }        
                    

                    // Para turnos con estado 'reservado', permite ingresar comentarios
                    if (turno.estado == 'reservado')
                    {
                      $('#bloque_add_comentario_turno_cliente').show();
                      $('#turno_titulo_comentarios_cliente').text('¡Coordiná tu visita a la tienda desde acá!');
                    }
                    else
                    {
                      $('#bloque_add_comentario_turno_cliente').hide();       
                      $('#turno_titulo_comentarios_cliente').text('Mensajes');      
                    }   

                    // Datos para form de nuevo comentario
                    $("#hd_turno_turno_id").val(turno_id);
                    $("#hd_turno_cliente_id").val(turno.cliente_id);              

                  } 
                });


                // COMENTARIOS
                table_comentarios_turno_cliente = $('#table_comentarios_turno_cliente').DataTable({ 

                    "lengthChange": false, 
                    "paging": false,
                    "filter": false,
                    "ordering": false,
                    "info": false,

                    "destroy": true,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo BASE_PATH ?>/admin/turno/ajax_comentarios_list/" + turno_id,
                        "type": "POST"
                    },

                });                             

                $('#btnSaveComentarioturnoCliente').text('Aceptar');  

            }


            function save_comentario_turno_cliente()
            {
                // Si el cliente ingresó un nuevo comentario, se guarda.
                if ($('#turno_comentario_cliente').val().trim() != '')
                {
                    $('#btnSaveComentarioTurnoCliente').text('Guardando...'); 
                    $('#btnSaveComentarioTurnoCliente').attr('disabled',true); 


                    var url = "<?php echo BASE_PATH ?>/Turno/ajax_add_comentario_cliente";

                    $.ajax({
                        url : url ,
                        type: "POST",
                        data: $('#form_comentario_turno_cliente').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {
                            $('#btnSaveComentarioTurnoCliente').text('Aceptar');
                            $('#btnSaveComentarioTurnoCliente').attr('disabled',false);

                            $('#modal_detalle_turno_cliente').modal('hide');  

                            aviso('success', 'Mensaje enviado.');                   

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            $('#modal_detalle_turno_cliente').modal('hide');  
                            aviso('danger', textStatus, 'Error al ingresar mensaje: (' + errorThrown + ')'); 

                            $('#btnSaveComentarioTurnoCliente').text('Aceptar');
                            $('#btnSaveComentarioTurnoCliente').attr('disabled',false); 
                        }
                    });     
                }
                else
                {
                  $('#modal_detalle_turno_cliente').modal('hide');  
                }
            }

            // Si el cliente ingresó un nuevo comentario, cambia el botón 'Aceptar' por 'Enviar'
            $('#turno_comentario_cliente').on('input',function(e){
                
                if ($('#turno_comentario_cliente').val().trim() != '')
                    $('#btnSaveComentarioTurnoCliente').text('Enviar'); 
                else
                    $('#btnSaveComentarioTurnoCliente').text('Aceptar');    
            });            
            /* FIN INFORMACION DE UN TURNO - CLIENTE */


            /* INFORMACION DE UNA VENTA - CLIENTE */
            function detalle_venta_cliente(venta_id, comentarios = false)
            {
                $("#venta_local_cliente_avatar_cliente").html('');
                $("#venta_comentario_cliente").val('');

                // ENCABEZADO
                $.ajax({
                  url : "<?php echo BASE_PATH ?>/admin/Venta/get_venta_by_id/" + venta_id,
                  cache: false,
                  success: function(data){

                    venta = jQuery.parseJSON(data);

                    // Datos de la venta
                    $('.modal-title-venta-nro').text('Compra ' + venta.numero  + ' (' + venta.estado + ')');
                    $('#modal_detalle_venta_cliente').modal('show'); 

                    $('#venta_fecha_cliente').text(venta.fecha);
                    $('#venta_condicion_venta_cliente').text(venta.condicion_venta);
                    $('#venta_estado_cliente').text(venta.estado);
                    
                    // Datos del local
                    $("#venta_local_cliente_avatar_cliente").append('<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + venta.local_avatar + '" width="100%">');

                    $('#venta_local_cliente').text(venta.local);
                    $('#venta_titulo_nuevo_comentario_cliente').text('Enviale un mensaje a ' + venta.local);
                    $('#venta_titular_local_cliente').text(venta.local_nombre_titular + ' ' + venta.local_apellido_titular);
                    $('#venta_telefono_local_cliente').text(' ' + venta.local_telefono);
                    $('#venta_direccion_local_cliente').text(' ' + venta.local_direccion + ', ' + venta.local_localidad);

                    // Datos adicionales
                    $('#venta_tipo_envio_cliente').text(venta.tipo_envio);

                    if (venta.tipo_envio == 'A domicilio')
                    {
                        $('#div_venta_cliente_direccion_cliente').show();
                        $('#div_venta_cliente_ciudad_cliente').show();
                        $('#venta_cliente_direccion_cliente').text(venta.cliente_direccion);
                        $('#venta_cliente_ciudad_cliente').text(venta.cliente_localidad + ', ' +  venta.cliente_provincia);        
                    }
                    else
                    {
                        $('#div_venta_cliente_direccion_cliente').hide();
                        $('#div_venta_cliente_ciudad_cliente').hide();
                    }

                    // Valoración
                    if (venta.puntaje)
                    {
                      $('#venta_puntaje_cliente').html(venta.puntaje);
                      $('#venta_valoracion_cliente').text(venta.valoracion);
                      $('#venta_valoracion_cliente_bloque_cliente').show();
                    }
                    else
                    {
                      $('#venta_valoracion_cliente_bloque_cliente').hide();
                    }

                    // Pedido cancelado
                    if (venta.cancelado_por != '')
                    {
                        $('#div_venta_cancelado_cliente').show();
                        $('#venta_cancelado_por_cliente').text(venta.cancelado_por);
                        $('#venta_cancelado_motivo_cliente').text(venta.cancelado_comentario);          
                    }
                    else
                    {
                        $('#div_venta_cancelado_cliente').hide();
                    }  
                        
                    // Para pedidos pendiente, permite ingresar comentarios
                    if (venta.estado == 'pendiente')
                    {
                      $('#bloque_add_comentario_venta_cliente').show();
                      $('#venta_titulo_comentarios_cliente').text('¡Coordiná el envío o retiro del pedido desde acá!');
                    }
                    else
                    {
                      $('#bloque_add_comentario_venta_cliente').hide();       
                      $('#venta_titulo_comentarios_cliente').text('Mensajes');      
                    }     

                    // Datos para form de nuevo comentario
                    $("#hd_cliente_venta_id").val(venta_id);
                    $("#hd_cliente_id").val(venta.cliente_id);                    

                  } 
                });

                // DETALLE
                table_detalle_venta_cliente = $('#table_detalle_venta_cliente').DataTable({ 

                    "lengthChange": false, 
                    "paging": false,
                    "filter": false,
                    "ordering": false,
                    "info": false,

                    "destroy": true,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_list_detalle/" + venta_id,
                        "type": "POST"
                    },

                    //Set column definition initialisation properties.
                    "columnDefs": [
                    { 
                        "targets": [ 0, -1 ], // first and last column
                        "orderable": false, //set not orderable
                    },
                    { 
                        "className": "text-right", "targets": [4] 
                    }
                    ],

                    // Suma total del precio de cada detalle
                    footerCallback: function ( row, data, start, end, display ) {
                        var api = this.api(), data;
             
                        // Remove the formatting to get integer data for summation
                        var intVal = function ( i ) {
                            return typeof i === 'string' ?
                                i.replace(/[\$,]/g, '')*1 :
                                typeof i === 'number' ?
                                    i : 0;
                        };
                
                        // Total over all pages
                        total = api
                            .column( 4 )
                            .data()
                            .reduce( function (a, b) {
                                return intVal(a) + intVal(b);
                            }, 0 );
             
                        // Update footer
                        $( api.column( 4 ).footer() ).html('$'+ total);
                    }

                });


                // COMENTARIOS
                table_comentarios_venta_cliente = $('#table_comentarios_venta_cliente').DataTable({ 

                    "lengthChange": false, 
                    "paging": false,
                    "filter": false,
                    "ordering": false,
                    "info": false,

                    "destroy": true,
                    "processing": true, //Feature control the processing indicator.
                    "serverSide": true, //Feature control DataTables' server-side processing mode.

                    // Load data for the table's content from an Ajax source
                    "ajax": {
                        "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_comentarios_list/" + venta_id,
                        "type": "POST"
                    },

                });                             

                $('#btnSaveComentarioVentaCliente').text('Aceptar');   
            }


            function save_comentario_venta_cliente()
            {
                // Si el cliente ingresó un nuevo comentario, se guarda.
                if ($('#venta_comentario_cliente').val().trim() != '')
                {
                    $('#btnSaveComentarioVentaCliente').text('Guardando...'); 
                    $('#btnSaveComentarioVentaCliente').attr('disabled',true); 


                    var url = "<?php echo BASE_PATH ?>/Venta/ajax_add_comentario_cliente";

                    $.ajax({
                        url : url ,
                        type: "POST",
                        data: $('#form_comentario_venta_cliente').serialize(),
                        dataType: "JSON",
                        success: function(data)
                        {
                            $('#btnSaveComentarioVentaCliente').text('Aceptar');
                            $('#btnSaveComentarioVentaCliente').attr('disabled',false);

                            $('#modal_detalle_venta_cliente').modal('hide');  

                            aviso('success', 'Mensaje enviado.');                   

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            $('#modal_detalle_venta_cliente').modal('hide');  
                            aviso('danger', textStatus, 'Error al ingresar mensaje: (' + errorThrown + ')'); 

                            $('#btnSaveComentarioVentaCliente').text('Aceptar');
                            $('#btnSaveComentarioVentaCliente').attr('disabled',false); 
                        }
                    });     
                }
                else
                {
                  $('#modal_detalle_venta_cliente').modal('hide');  
                }
            }

            // Si el cliente ingresó un nuevo comentario, cambia el botón 'Aceptar' por 'Enviar'
            $('#venta_comentario_cliente').on('input',function(e){
                
                if ($('#venta_comentario_cliente').val().trim() != '')
                    $('#btnSaveComentarioVentaCliente').text('Enviar'); 
                else
                    $('#btnSaveComentarioVentaCliente').text('Aceptar');    
            });

            /* FIN INFORMACION DE UNA VENTA - CLIENTE */


            /* NOTIFICACIONES */
            <?php if(isset($_SESSION['frontend_tipo'])): ?>
              // Verifica si hay nuevas notificaciones
              setInterval(check_notificaciones, <?php echo $this->config->item('intervalo_notificaciones'); ?>);

              tipo = '<?php echo $_SESSION['frontend_tipo']; ?>';
            <?php endif ?>  

              function check_notificaciones()
              {
                  url = "<?php echo BASE_PATH ?>/Notificacion/get_count_novistas_" + tipo + "/";

                  $.ajax({
                    url : url,
                    cache: false,
                    success: function(data){
                      if ($.isNumeric(data))
                      {
                          update_count_notificaciones(data);

                      }
                      else
                          console.log('Error al actualizar notificaciones. ' + data);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                      console.log('Error al actualizar notificaciones. ' + textStatus);
                    } 
                  });
              }


              // Actualiza el contador de notificaciones
              function update_count_notificaciones(data){
                  if (data != '0'){
                      $("#count_notificaciones").show();
                      $("#count_notificaciones").html(data);
                      document.title =  '(' + data + ') GeoTienda';

                      // Si hubo actividad, actualiza la vista actual del local o cliente
                      if (data != notificaciones_actual)
                      {
                        actualiza_vista();
                        notificaciones_actual = data;                        
                      }

                  }
                  else
                  {
                      $("#count_notificaciones").hide();
                      document.title = 'GeoTienda'; 
                  }
              }

            
            // Actualiza lista de notificaciones no vistas, y las marca como vistas
            $('#btn_notificaciones').click(function(e) {

                if (!$("#lista_notificaciones").is(":visible")) $("#count_notificaciones").hide();

                // Carga notificaciones recientes
                get_notificaciones_recientes();
            });


            function get_notificaciones_recientes(){

                url = "<?php echo BASE_PATH ?>/Notificacion/get_recientes_" + tipo + "/";

                $.ajax({
                  url : url,
                  cache: false,
                  success: function(data)
                  {
                    notificaciones_array = jQuery.parseJSON(data);
                    muestra_notificaciones();

                    // Marca notificaciones como vistas 
                    set_notificaciones_vistas();     

                    // Actualiza contador de notificaciones no vistas del browser
                    notificaciones_actual = 0;       
                  } 
                });
            }


            // Actualiza la vista actual (cliente o local)
            function actualiza_vista()
            {
                if (tipo == 'local')
                {
                     switch (tab)
                     {
                        case 'pedidos':
                            mostrar_pedidos_local();                                      
                        break;  

                        case 'turnos':
                            mostrar_turnos_local();                                      
                        break;  

                        case 'ventas':
                            mostrar_ventas_local();                                      
                        break;                         

                        case 'vacunas':
                            mostrar_mascotas_local();                                      
                        break;  

                        case 'productos':                               
                            mostrar_productos_local();
                        break;

                        case 'notificaciones':                               
                            mostrar_notificaciones_local();
                        break;

                        case 'datos':
                            mostrar_datos_local();                                      
                        break;                                     
                    }    
                }
                else
                {
                    switch (tab){
                           
                        case 'pedidos':                               
                            mostrar_pedidos();
                        break;

                        case 'turnos':
                            mostrar_turnos();                                      
                        break;  

                        case 'mascotas':
                            mostrar_mascotas();                                      
                        break;  

                        case 'historial':
                            mostrar_historial();                                      
                        break;  


                        case 'notificaciones':                               
                            mostrar_notificaciones();
                        break;            

                        case 'datos':
                            mostrar_datos();                                      
                        break;                                     
                    } 
                }
            }


            function set_notificaciones_vistas(){

                url = "<?php echo BASE_PATH ?>/Notificacion/set_vistas_" + tipo + "/";

                $.ajax({
                  url : url,
                  cache: false,
                  success: function(data){
                     update_count_notificaciones(data);
                  } 
                });
            }


            function set_notificacion_leida(id){

                url = "<?php echo BASE_PATH ?>/Notificacion/set_leida_" + tipo + "/" + id;

                $.ajax({
                  url : url,
                  cache: false,
                  success: function(data){
                   //  update_count_notificaciones(data);
                  } 
                });
            }


            // Muestra lista de notificaciones no vistas
            function muestra_notificaciones(){

                var i;
                var clase_tipo = "";
                var clase_leida_admin = "";
                var link = "";

                $("#lista_notificaciones").empty();

                $("#lista_notificaciones").append('<li class="dropdown-header titulo-box-notificaciones">Notificaciones</li>');

                if (notificaciones_array.length > 0)
                {

                    for (i = 0; i < notificaciones_array.length; ++i) {

                        // Estilo y link de las notificaciones segun tipo (puede ser de cliente o local)
                        switch (notificaciones_array[i][1]) { 
                            case 'calificación venta': 
                                clase_tipo = "fa fa-paw text-warning";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta_cliente('" + notificaciones_array[i][4] + "')\";";
                                break;

                            case 'calificación turno': 
                                clase_tipo = "fa fa-paw text-warning";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "', false)\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno_cliente('" + notificaciones_array[i][5] + "', false)\";";                                    
                                break;

                            case 'aviso venta': 
                            case 'venta': 
                                clase_tipo = "si si-handbag text-default";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta_cliente('" + notificaciones_array[i][4] + "')\";";

                                break;   

                            case 'mensaje venta': 
                                clase_tipo = "fa fa-comment-o text-default";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta_cliente('" + notificaciones_array[i][4] + "')\";";

                                break;   

                            case 'mensaje turno': 
                                clase_tipo = "fa fa-comment-o text-default";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno_cliente('" + notificaciones_array[i][5] + "')\";";

                                break;                                 

                           case 'vacuna': 
                                clase_tipo = "si si-heart text-danger";

                                link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_vacuna_cliente('" + notificaciones_array[i][8] + "')\";";

                                break;                                 

                           case 'recordatorio vacuna': 
                                clase_tipo = "si si-heart text-danger";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_vacuna_agendada('" + notificaciones_array[i][8] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_vacuna_agendada_cliente('" + notificaciones_array[i][8] + "')\";";

                                break;    

                           case 'mascota asignada': 
                                clase_tipo = "si si-heart text-danger";

                                link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); window.location.href ='<?php echo BASE_PATH ?>/dashboard/vacunas'\";";

                                break;  

                            /*
                           case 'vacuna agendada': 
                                clase_tipo = "si si-heart text-default";

                                link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_vacuna_agendada_cliente('" + notificaciones_array[i][8] + "')\";";

                                break;
                            */                            


                            case 'aviso turno': 
                            case 'turno': 
                            case 'recordatorio turno':
                                clase_tipo = "si si-calendar text-success";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "', false)\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno_cliente('" + notificaciones_array[i][5] + "', false)\";";

                                break;

                            case 'cancela turno': 
                                clase_tipo = "si si-calendar text-warning";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno('" + notificaciones_array[i][5] + "', false)\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_turno_cliente('" + notificaciones_array[i][5] + "', false)\";";

                                break;    

                            case 'cancela venta': 
                                clase_tipo = "si si-handbag text-warning";

                                if (tipo == 'local')
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta('" + notificaciones_array[i][4] + "')\";";
                                else
                                    link = "onclick=\"set_notificacion_leida('" + notificaciones_array[i][7] + "'); detalle_venta_cliente('" + notificaciones_array[i][4] + "')\";";
                                
                                break;                                                                                 
                        }


                        // Estilo de las notificaciones no leidas
                        if (notificaciones_array[i][3] == 0)
                            clase_leida_admin = "notificacion_no_leida";
                        else
                            clase_leida_admin = "notificacion_leida";

                        $("#lista_notificaciones").append(
                            '<li class="' + clase_leida_admin + '"><a href="javascript:void(0);" ' + link + ' ><i class="' + clase_tipo + ' detalle-notificacion"></i>' +
                            '<div class="font-w600" >' + notificaciones_array[i][2] + '</div>' +
                            '<div><small>' + notificaciones_array[i][0] + '</small></div></a></li>');

                    }
                }
                else{
                    $("#lista_notificaciones").append(
                        '<li class="no-hay-notificaciones"><span class="font-w200 font-s13 text-gray-darker">' + 
                         
                         'No hay notificaciones</span>' + 
                         '</li>');                   
                }
                
                $("#lista_notificaciones").append(
                    '<li class="notificaciones-ver-todas"><span class="font-w600 font-s13 text-gray-darker">' + 

                     '<a href="<?php echo BASE_PATH ?>/dashboard/notificaciones">' + 
                     'Ver Todas</a></span>' + 
                     '</li>');   
                
            }            
            /* fin NOTIFICACIONES */          


            /* PORTADA */

            function portada(pagina)
            {
                $('#pagina_portada').val(pagina);
                $('#form_portada')[0].reset(); 
                $('#div_portada_error').hide();
                $('#modal_portada').modal('show'); 
            }   

            function mostrar_portadas()
            {
                $('#modal_portadas').modal('show');
            }


            function elegir_portada(foto, back)
            {
                $('#foto_portada_precargada').val(foto);

                if (back == 'false')
                    src = "./assets/img/portadas/" + foto;
                else
                    src = "../assets/img/portadas/" + foto;

                $('#div_portada_error').hide();   
                $('#modal_portada').modal('hide');                                 
                $('#modal_portadas').modal('hide');  

                $('#img_portada').addClass('headerimage ui-draggable');
                $('#img_portada').attr("src", src);

                $('.portada-layer').hide(); 
                $('.portada-title').hide(); 

                $('#btn_portada').hide(); 
                $('#btn_portada_posicion').removeClass('oculto');   
                $('#btn_portada_cancelar').removeClass('oculto');   
            }


            function eliminarImagen(nombre)
            {
                if(confirm('¿Eliminar imagen "' + nombre + '"?'))
                {
                    $.ajax({
                        url : "<?php echo BASE_PATH ?>/Configuracion/delete_imagen/" + nombre,
                        type: "POST",
                        dataType: "JSON",
                        success: function(data)
                        {
                            if(data.status)
                            {
                                 $('[id="' + nombre + '"]').hide(); 

                                 aviso('success', 'Imagen eliminada.');    
                            }
                            else
                            {
                                alert(data.mensaje);
                                //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                                
                                console.log(data.error);
                            }

                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {

                             aviso('danger', textStatus, "ERROR AL ELIMINAR IMAGEN"); 
                        }
                    });
                }
            }            


            function portada_posicion()
            {
                var p = $(".portada-posicion").attr("style");
                var Y =p.split("top:");
                var Z=Y[1].split(";");

                $('#foto_portada_posicion').val(Z[0]);

                var formdata = new FormData(document.getElementById('form_portada'));

                var url = "<?php echo BASE_PATH ?>/Configuracion/ajax_portada";

                $.ajax({
                    url : url ,
                    type: "POST",
                    data: formdata,
                    dataType: "JSON",
                    cache: false,
                    contentType: false,
                    processData: false,           
                    success: function(data)
                    {
                        // Actualizacion ok
                        if(data.status) 
                        {
                            location.reload();                               
                        }
                    },
                    // Error no manejado
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('#modal_portada').modal('hide');
                        $('#modal_portadas').modal('hide');
                        $('#modal_error_sistema').modal('show');
                        console.log(errorThrown);
                    }
                });
            }


            function resetFormElement(e) 
            {
                  e.wrap('<form>').closest('form').get(0).reset();
                  e.unwrap();
            }

            function cancela_portada()
            {
                $('#foto_portada_anterior').val(portada_anterior);
                $('#foto_portada_posicion').val(posicion_actual);
                $('#foto_portada_precargada').val(portada_actual);

                resetFormElement($('#foto_portada'));

                var formdata = new FormData(document.getElementById('form_portada'));

                var url = "<?php echo BASE_PATH ?>/Configuracion/ajax_portada";

                $.ajax({
                    url : url ,
                    type: "POST",
                    data: formdata,
                    dataType: "JSON",
                    cache: false,
                    contentType: false,
                    processData: false,           
                    success: function(data)
                    {
                        // Actualizacion ok
                        if(data.status) 
                        {
                            location.reload();                               
                        }
                    },
                    // Error no manejado
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('#modal_portada').modal('hide');
                        $('#modal_portadas').modal('hide');
                        $('#modal_error_sistema').modal('show');
                        console.log(errorThrown);
                    }
                });             
            }


            function save_portada(back)
            {
                $('#foto_portada_anterior').val();

                var url = "<?php echo BASE_PATH ?>/Configuracion/ajax_portada";

                var formdata = new FormData(document.getElementById('form_portada'));

                $.ajax({
                    url : url ,
                    type: "POST",
                    data: formdata,
                    dataType: "JSON",
                    cache: false,
                    contentType: false,
                    processData: false,           
                    success: function(data)
                    {
                        // Actualizacion ok
                        if(data.status) 
                        {
                            portada_anterior = data.portada;

                            if (back == 'false')
                                src = "./assets/img/portadas/" + data.portada;
                            else
                                src = "../assets/img/portadas/" + data.portada;

                            $('#div_portada_error').hide();   
                            $('#modal_portada').modal('hide');                                 
                            $('#modal_portadas').modal('hide');  

                            $('#img_portada').addClass('headerimage ui-draggable');
                            $('#img_portada').attr("src", src);

                            $('.portada-layer').hide(); 
                            $('.portada-title').hide(); 

                            $('#btn_portada').hide(); 
                            $('#btn_portada_posicion').removeClass('oculto');        
                            $('#btn_portada_cancelar').removeClass('oculto');                                                       
                        }
                        // Fallo en actualizacion
                        else
                        {
                            mensaje_error = '';

                            for (var i = 0; i < data.error.mensaje.length; i++) 
                            {
                                mensaje_error += data.error.mensaje[i] + '<br/>';
                            }

                            $('#portada_error_message').html(mensaje_error);
                            $('#div_portada_error').show();                                                   
                        }

                    },
                    // Error no manejado
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        $('#modal_portada').modal('hide');
                        $('#modal_error_sistema').modal('show');
                        console.log(errorThrown);
                    }
                });
            }


            $("body").on('mouseover','.headerimage',function()
            {
                var y1 = $('#div_portada').height();
                var y2 =  $('.headerimage').height();

                $(this).draggable({
                    scroll: false,
                    axis: "y",
                    drag: function(event, ui) 
                    {
                        
                        if(ui.position.top >= 0)
                        {
                            ui.position.top = 0;
                        }
                        else if(ui.position.top <= y1 - y2)
                        {
                            ui.position.top = y1 - y2;
                        }
                    },
                    stop: function(event, ui)
                    {
                    }
                });
            });            
            /* fin PORTADA */ 
        </script>
            
    </body>
</html>