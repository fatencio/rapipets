<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_contacto_local')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>

<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Sumá tu comercio a GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Sumá tu comercio a GeoTienda</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>


<section class="content content-full content-boxed "> 
    <div class="text-center push-30-t push-50 hidden-xs">
        <img class="img-responsive" src="<?php echo BASE_PATH ?>/assets/img/frontend/beneficios.jpg" alt="Beneficios de GeoTienda">
    </div> 

    <div class="push-30-t push-50 visible-xs">    
        <div class="push-20">

            <div class="text-center h3 font-w600">Beneficios para Pet Shops y Veterinarias</div>
            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-rocket"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Nuevo canal de ventas</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Ingresar a una nueva cartera de clientes que utilizan diariamente la plataforma.</div>
                    <div class="push-10">Encontrar nuevos clientes en tu zona de entrega que buscan comprar un producto o reservar un turno de un servicio online.</div>
                    <div class="push-10">Publicidad online gratuita: los Pet Shops o Veterinarias podrán subir fotos y descripciones de productos que vendan y servicios que brindan.</div>
                </div>
            </div>

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-star"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Compromiso GeoTienda</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Incrementar la publicidad de su negocio.</div>
                    <div class="push-10">Brindarle una plataforma para su Pet Shop o Veterinaria abierto las 24hs. los 365 días del año.</div>
                    <div class="push-10">Atención personalizada para cada uno de los negocios adheridos.</div>
                </div>
            </div>    

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-clock"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Gestión de entregas a domicilio</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Gestionar los pedidos recibidos de compra y los procesos logísticos de su negocio.</div>
                    <div class="push-10">Recibir pedidos simultáneos, sin perder a clientes por pérdida de número telefónico o lineas ocupadas.</div>
                </div>
            </div>      

            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-screen-smartphone"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Tu catálogo online</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Ofrecer al cliente un catálogo de productos y servicios completo.</div>
                    <div class="push-10">Diferenciar tu negocio destacando promociones y descuentos.</div>
                </div>
            </div>     


            <br />
            <div class="bg-white beneficios text-center ">
                <span class="item item-2x item-circle border">
                    <i class="si si-emoticon-smile"></i>
                </span>
                <div class="push-10-t h4 text-celeste">Sin costos fijos</div>
                <br />
                <div class="h5 text-muted">
                    <div class="push-10">Abonan cargos solamente por los pedidos recibidos de compra y reserva de turnos.</div>
                </div>
            </div>   
        </div>   
        <div class="text-center push-50-t push-50">
            <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
        </div>  
    </div>

</section>

<section class="content content-full content-boxed form-contacto">  
    <div class="push-30">
        <div class="h4 text-gris text-center">Completá tus datos y a la brevedad nos pondremos en contacto para finalizar el proceso.</div>
    </div>   

    <!-- CONSULTA LOCAL Form -->
    <div class="bg-white beneficios">
        <form class="js-validation-login form-horizontal push-30" action="#" id="form_registro_negocio">
            <!-- Registro Negocio Error -->
            <div class="alert alert-danger alert-dismissable" id="div_register_local_error" style="display:none">
                <small id="register_local_error_message"></small>                 
            </div>
            <!-- END Register Error -->                        
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                        <input class="form-control" type="text" id="consulta-negocio-nombre" name="consulta-negocio-nombre" placeholder="Tu Nombre y Apellido o Empresa" value="<?php if (isset($registro_negocio_nombre)) echo $registro_negocio_nombre; ?>"  required>
                </div>
            </div>                                                        
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                    <input class="form-control" type="text" id="consulta-negocio-email" name="consulta-negocio-email" placeholder="E-mail" value="<?php if (isset($registro_negocio_email)) echo $registro_negocio_email; ?>"  required>
                </div>
            </div>
            <div class="form-group form-group-login">
                <div class="col-xs-12 input-obligatorio">
                    <input class="form-control" type="telefono" id="consulta-negocio-telefono" name="consulta-negocio-telefono" placeholder="Teléfono" value="<?php if (isset($registro_negocio_telefono)) echo $registro_negocio_telefono; ?>"  required>
                </div>
            </div>
            <div class="form-group form-group-login">
                <div class="col-xs-12">
                    <textarea class="form-control" id="consulta-negocio-mensaje" name="consulta-negocio-mensaje" placeholder="Mensaje" value="<?php if (isset($consulta_mensaje)) echo $consulta_mensaje; ?>"  rows="4"></textarea>
                </div>
            </div>

            <div class="obligatorios"><span>*</span> Campos obligatorios</div>

            <div class="form-group form-group-login recaptcha">
                 <div id="div_recaptcha_contacto" class="g-recaptcha" data-sitekey="6LcjXCQTAAAAACqThe9TcP9eovw8fuZY1wj95QKF"></div>            
            </div>
        </form>                                                
        <div class="form-group form-group-login push-30-t">
              <div class="text-center">
                <button class="btn btn-lg btn-geotienda push-10-r" id="btn_enviar_consulta_local" onclick="register_local()">&nbsp;&nbsp;Enviar&nbsp;&nbsp;</button>
            </div>                                                                    
        </div>

     </div>   
    <!-- fin CONSULTA LOCAL Form -->
</section>             

    <!-- Confirmacion Pedido de Registro Local Modal -->
    <div class="modal fade" id="modal_registro_negocio_ok" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="block block-themed block-transparent remove-margin-b">
                    <div class="block-content geotienda-modal-content">
                        <ul class="block-options login-close" >
                            <li>
                                <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                            </li>
                        </ul>                        
                        <h2 class="text-center text-naranja">Solicitud enviada con éxito</h2>
                        <br/>
                        <h3 class="font-w300 text-center">Muy pronto nos pondremos en contacto para finalizar el proceso de alta.</h3>
                        <!--
                        <h5 class="font-w300 text-center text-muted push-20-t">Si no te llegó un email al buzón de entrada, por favor revisá tu correo no deseado.</h5>                        
                        -->
                        <br/><br/>

                        <div class="text-center">
                            <a href="<?php echo BASE_PATH ?>" id="btn_resultado_ok" class="btn btn-lg btn-info btn-geotienda-celeste" >Aceptar</a>
                        </div>
                        <br/>


                    </div>
                </div>
            </div>
        </div>
    </div> 
    <!-- FIN Confirmacion Pedido de Registro Local Ok Modal -->

<script>

    $(document).ready(function() {

        $('#consulta-negocio-nombre').focus();

    });

    /* FORM CONSULTA */


    // Al mostrar el modal de contacto exitoso, hace foco en el boton 'Aceptar'
    $('#modal_consulta_ok').on('shown.bs.modal', function () {
        $('#btn_consulta_ok').focus();
    })

   // Tecla enter en inputs de registro de nuevo local
    $('#consulta-negocio-nombre').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-negocio-nombre').val() != '') $('#consulta-negocio-email').focus();
        }
    });


    $('#consulta-negocio-email').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-negocio-email').val() != '') $('#consulta-negocio-telefono').focus();
        }
    });


    $('#consulta-negocio-telefono').keypress(function(event) {
        if (event.which == 13) {
            event.preventDefault();
            
            if($('#consulta-negocio-telefono').val() != '') register_local();
        }
    });    


    function register_local()
    {
        var url = "<?php echo BASE_PATH ?>/Inicio/local_pedido_registro";

        $('#btn_enviar_consulta_local').html('Enviando...');
        $('#btn_enviar_consulta_local').attr('disabled', true); 

        
        // Agrega manualmente recaptcha a los datos a enviar, 
        // ya que al haber 2 recaptchas en la misma pagina (crear cuenta y este) 
        // a los dos les asigna el mismo 'name' (g-recaptcha-response), por lo que no los lee correctamente
        // Al recaptcha duplicado le asingna 'id ' -> g-recaptcha-response-1

        var data_post = $('#form_registro_negocio').serializeArray();
        data_post.push({name: 'consulta-negocio-recaptcha', value: $('#g-recaptcha-response-1').val()});        

        $.ajax({
            url : url ,
            type: "POST",
            data: data_post,
            dataType: "JSON",
            success: function(data)
            {

                // Register ok
                if(data.status) 
                {
                    $('#modal_registro_negocio_ok').modal({ backdrop: 'static', keyboard: false }) // Impide cerrar el modal haciendo clic fuera de él
                    $('#modal_registro_negocio_ok').modal('show');                     
                }
                // Fallo en registro
                else
                {
                    mensaje_error = '';

                    for (var i = 0; i < data.error.mensaje.length; i++) 
                    {
                        mensaje_error += data.error.mensaje[i] + '<br/>';
                    }

                    $('#register_local_error_message').html(mensaje_error);
                    $('#div_register_local_error').show();                            
                }

                $('#btn_enviar_consulta_local').html('Enviar');
                $('#btn_enviar_consulta_local').attr('disabled', false);                   
            },
            // Error no manejado
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_error_sistema').modal('show');
                console.log(errorThrown);

                $('#btn_enviar_consulta_local').html('Enviar');
                $('#btn_enviar_consulta_local').attr('disabled', false);                  
            }
        });
    }
/* fin REGISTRO NUEVOS LOCALES */
</script>