<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_dashboard')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>


<!-- PORTADA version DESKTOP -->  
<div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer hidden-xs"></div>    

<div class="portada-wrapper hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px" class="hidden-xs">
      <div class="text-center">
          
          <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
      </div>
    </div>                    
</div>

<!-- PORTADA version MOBILE -->
<div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer visible-xs"></div>   

<div class="portada-wrapper visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px">
      <div class="text-center">
          
          <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">        
      </div>
    </div>                    
</div>

<!-- BUSCADOR MINI -->   
<!-- version MOBILE -->
<section class="content content-full content-boxed portada-title visible-xs" style="top: <?php echo $portada_top_mobile; ?>px;">
    <div class="row ">
        
        <!-- Volver al listado de locales -->                    
        <div class="col-xs-7" >
            <a class="btn btn-listado-locales-cliente push-20-t" onclick="buscar_locales_mini_m();" style="display:none"><i class="si si-arrow-left push-5-r" style="color:white"></i>Tiendas</a>  
        </div>                          

        <!-- Boton Nueva Busqueda -->   
        <div class="col-xs-5">
            <button class="btn btn-nueva-busqueda push-20-t" data-toggle="collapse" data-target="#campos_nueva_busqueda" id="btn_nueva_busqueda_m" >Nueva Búsqueda</button>
        </div>
    </div>              
</section>

<!-- version TABLET y LAPTOP -->
<section class="content content-full content-boxed hidden-xs portada-title" style="right: 1%; left: 1%; top: <?php echo $portada_top; ?>px;">
    <div class="row push-5">

        <div class="col-sm-4 push-20-t">
            <!-- Volver al listado de locales -->
              <a class="btn btn-listado-locales-cliente" style="max-width: 120px; display:none" onclick="buscar_locales_mini();" ><i class="si si-arrow-left push-5-r" style="color:white"></i>Locales</a>                        
        </div>


        <div class="col-sm-8 push-20-t push-5">    
            <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini" method="get">
            </form>                     
              <div class="col-sm-5">
                    <input class="form-control input-buscar-local-mini input-buscador-mini-dir-cliente font-w600 text-black" type="text" id="dir_mini" name="dir" placeholder="MI DIRECCION" required>
                    <div class="animated fadeInDown valida-buscador" id="valida_dir_mini" style="display:none">Ingresá tu dirección</div>                                
              </div>
              <div class="col-sm-4" id="div_localidad_mini">
                    <select id="loc_mini" name="loc" class="js-select2 form-control input-buscar-local-mini input-buscador-mini-loc font-w600 text-black" required data-placeholder="MI LOCALIDAD">
                        <option></option>
                        <?php 
                        foreach($localidades as  $localida_item){ ?>
                            <option value="<?php echo $localida_item->nombre ?>" ><?php echo $localida_item->nombre ?></option> 
                        <?php } ?>    
                    </select>                
                    <div class="animated fadeInDown valida-buscador" id="valida_loc_mini" style="display:none">Seleccioná tu localidad</div>                                
              </div>
            
              <div class="col-sm-3">
                    <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini" onclick="buscar_locales_mini();" >Buscar</a>
              </div>                         
        </div>                      
    </div>
</section>   

<div id="campos_nueva_busqueda" class="collapse push-10-t">    
    <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini_m" method="get">
    </form>                   
      <div class="col-sm-5 push-5">
            <input class="form-control input-buscar-local-mini-m input-buscador-mini-dir-cliente font-w600 text-black" type="text" id="dir_mini_m" name="dir" placeholder="MI DIRECCION" required>
            <div class="animated fadeInDown valida-buscador" id="valida_dir_mini_m" style="display:none">Ingresá tu dirección</div>
      </div>
      <div class="col-sm-4 push-5" id="div_localidad_mini_m">
            <select id="loc_mini_m" name="loc" class="js-select2" required data-placeholder="MI LOCALIDAD">
                <option></option>
                <?php 
                foreach($localidades as  $localidad_item){ ?>
                    <option value="<?php echo $localidad_item->nombre ?>" ><?php echo $localidad_item->nombre ?></option> 
                <?php } ?>    
            </select>      
            <div class="animated fadeInDown valida-buscador" id="valida_loc_mini_m" style="display:none">Seleccioná tu localidad</div>  
      </div>
      <div class="col-sm-3 push-5">
            <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini_m" onclick="buscar_locales_mini_m();" >Buscar</a>               
      </div>   

 </div>   

<!-- MI CUENTA -->
<section class="content content-full content-boxed">  
    <div class="row">
      <div class="col-sm-2 text-center ">
        <a href="javascript:void(0)" onclick="mostrar_avatares()"><img id="cliente_avatar" src="" height="100"><br /></a>
        <small><a href="javascript:void(0)" onclick="mostrar_avatares()">cambiar avatar</a></small>
      </div>

      <div class="col-sm-10">
        <div class="push-40 push-20-t">
            <div class="h3 font-w600 centrado-xs" id="cliente_nombre"></div>
        </div>  
        <div class="push-20">
            <div class="h4 push-10-t text-gris hidden-xs">Desde aquí podrás administrar o modificar tus datos y los de tu mascota, valorar una tienda o revisar tus pedidos y turnos.</div>
        </div>    
      </div>
    </div>


  <!-- TABS CON MENU DEL CLIENTE -->
  <div class="block push-20-t">
      <ul class="nav nav-tabs nav-justified text-gris text-center" data-toggle="tabs">    

          <li class="<?php echo ($tab == 'pedidos') ? 'active ' : 'hidden-xs'; ?> pedidos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos'"><i class="si si-basket font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Mis Pedidos</span></a>
          </li> 

          <li class="<?php echo ($tab == 'turnos') ? 'active ' : 'hidden-xs'; ?> turnos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/turnos'"><i class="si si-calendar font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Mis Turnos</span></a>
          </li>  

          <li class="<?php echo ($tab == 'mascotas') ? 'active ' : 'hidden-xs'; ?> mascotas">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/mascotas'"><i class="si si-heart font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Mis Mascotas</span></a>
          </li>

          <li class="<?php echo ($tab == 'historial') ? 'active ' : 'hidden-xs'; ?> historial">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/historial'"><i class="si si-clock font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Historial</span></a>
          </li> 

          <li class="<?php echo ($tab == 'notificaciones') ? 'active ' : 'hidden-xs'; ?> notificaciones">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/notificaciones'"><i class="si si-bell font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Notificaciones</span></a>
          </li>  

          <li class="<?php echo ($tab == 'datos') ? 'active ' : 'hidden-xs'; ?> datos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/datos'"><i class="si si-user font-tabs push-5-r"></i><br class="hidden-xs " /><span class="font-tabs-texto">Mis Datos</span></a>
          </li>                    
      </ul>

      <div class="block-content tab-content">

          <!-- PEDIDOS pendientes -->
          <div class="tables-frontend tab-pane <?php if($tab == 'pedidos') echo 'active'; ?>" id="tab-pedidos">
            <div class="block-content bg-blue-light">
                <table id="table_pedidos" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Fecha</th>       
                            <th>Tienda</th>                                              
                            <th>Forma Pago</th>                                                    
                            <th>Total</th>    
                            <th style="width:40px">Valoración</th>                                             
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>   
          </div>    

          <!-- TURNOS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'turnos') echo 'active'; ?>" id="tab-turnos">
            <div class="block-content bg-blue-light">
                <div class="h4 texto-turnos">Turnos Activos</div>
                <table id="table_turnos_activos" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Turno</th>
                            <th>Fecha</th>    
                            <th>Tienda</th>                                                                
                            <th>Servicio</th>                            
                            <th>Precio</th>                                                            
                            <th style="width:150px">Acciones</th>               
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>                          
                            <th></th>                          
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>   

            <div class="block-content push-20-t">
                <div class="h4 texto-turnos">Turnos Vencidos</div>
                <table id="table_turnos_vencidos" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Turno</th>
                            <th>Fecha</th>    
                            <th>Tienda</th>                                                                
                            <th>Servicio</th>                            
                            <th>Precio</th>                                                            
                            <th style="width:40px">Valoración</th>               
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>                          
                            <th></th>                          
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div>              
          </div> 

          <!-- MIS MASCOTAS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'mascotas') echo 'active'; ?>" id="tab-mascotas">
            <div class="block-content bg-blue-light"> 
                <div class="h4 pull-left push-5-t vacunas">Ingresá los datos de tu mascota y asignale una veterinaria de confianza para que le controle sus vacunas <i class="fa fa-hand-o-right"></i></div>
                <div class="h4 pull-right push-5"><button class="btn btn-vacunas" onclick="add_mascota()">Nueva mascota</button></div>    

                <table id="table_mascotas" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <!--<th>Código</th>-->
                            <th>Mascota</th>                                     
                            <th>Tienda Asignada</th>                             
                            <th>Próxima Vacuna</th>                             
                            <th>Acciones</th>                                                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>              
            </div>  
          </div>

          <!-- HISTORIAL DE PEDIDOS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'historial') echo 'active'; ?>" id="tab-historial">
            <div class="block-content bg-blue-light">
                <table id="table_historial" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Pedido / Turno</th>
                            <th>Fecha</th>          
                            <th>Tienda</th>          
                            <th>Estado</th>                                         
                            <th>Forma Pago</th>                                                      
                            <th>Total</th>    
                            <th>Comentarios</th>                                                              
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>                          
                            <th></th>                          
                        </tr>
                    </tfoot>                    
                    <tbody>
                    </tbody>
                </table>
            </div>   
          </div>

          <!-- NOTIFICACIONES -->
          <div class="tab-pane <?php if($tab == 'notificaciones') echo 'active'; ?>" id="tab-notificaciones">
              <div class="block-content detalle-venta bg-blue-light">
                <table id="table_notificaciones" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>                         
                            <th>Notificación</th>
                            <th style="width:70px;">Detalles</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th style="width:70px;"></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>              
              </div>
          </div>  

          <!-- DATOS PERSONALES -->
          <div class="tab-pane <?php if($tab == 'datos') echo 'active'; ?>" id="tab-datos">

            <!-- FORM DATOS CLIENTE -->
            <div class="block-content bg-blue-light">
                <form class="form-horizontal push-30-t push-30 text-flat" action="#" id="form_cliente">      
                    <!-- Div Error -->
                    <div class="alert alert-danger alert-dismissable" id="div_cliente_error" style="display:none">
                        <small id="cliente_error_message"></small>                 
                    </div>
                    <!-- END Div Error -->                              
                                                   
                    <div class="form-group form-group-login">
                        <label class="col-sm-3 control-label ">Nombre</label>
                        <div class="col-sm-5 input-obligatorio">
                            <input class="form-control" type="text" id="cliente-nombre" name="cliente-nombre" placeholder="Ingresá tu nombre" value="<?php if (isset($cliente_nombre)) echo $cliente_nombre; ?>"  required>
                        </div>
                    </div>

                    <div class="form-group form-group-login">
                        <label class="col-sm-3 control-label " >Apellido</label>
                        <div class="col-sm-5 input-obligatorio">
                            <input class="form-control" type="text" id="cliente-apellido" name="cliente-apellido" placeholder="Ingresá tu apellido" value="<?php if (isset($cliente_apellido)) echo $cliente_apellido; ?>"  required>
                        </div>
                    </div>

                    <div class="form-group form-group-login">
                        <label class="col-sm-3 control-label " >Teléfono</label>
                        <div class="col-sm-5 input-obligatorio">
                            <input class="form-control" type="text" id="cliente-telefono" name="cliente-telefono" placeholder="Ingresá tu teléfono" value="<?php if (isset($cliente_telefono)) echo $cliente_telefono; ?>"  required>
                        </div>
                    </div>

                    <div class="form-group form-group-login">
                        <label class="col-sm-3 control-label ">E-mail</label>
                        <div class="col-sm-5 input-obligatorio">
                            <input class="form-control" type="text" id="cliente-email" name="cliente-email" placeholder="Ingresá tu e-mail" value="<?php if (isset($cliente_email)) echo $cliente_email; ?>"  required>
                        </div>
                    </div>

                    <div class="form-group form-group-login">
                        <label class="col-sm-3 control-label"></label>
                        <div class="col-sm-7">
                            <span class="red">*</span> Campos obligatorios
                        </div>
                    </div>

                    <div class="form-group form-group-login push-30-t">
                        <label class="col-sm-3 control-label ">Modificar Contraseña</label>
                        <div class="col-sm-3">
                            <input class="form-control" type="password" id="cliente-password" name="cliente-password" placeholder="contraseña">
                        </div>
                        <div class="col-sm-2">
                            <input class="form-control" type="password" id="cliente-password2" name="cliente-password2" placeholder="repetí contraseña">
                        </div>                        
                    </div>


                </form>   
                <div class="form-group form-group-login push-50-t push-50">
                      <div class="text-center">
                        <button class="btn btn-lg btn-geotienda-flat push-10-r" id="btn_actualizar_cliente" onclick="actualizar_cliente()">Guardar Cambios</button>
                    </div>                                                                    
                </div>
             </div>   
            <!-- fin FORM DATOS CLIENTE -->
          </div>               
      </div>
  </div>
  <!-- fin TABS CON MENU DEL CLIENTE -->
</section>


<!-- MODAL DE AVATARES -->
<div class="modal fade" id="modal_avatares" role="dialog">
    <div class="modal-dialog modal-lg modal-grande-80 ">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Seleccioná tu avatar</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/cliente/miniaturas/*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-xs-6 col-sm-4 col-md-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 120px; height: 120px; margin: 6px">
                                <img height="150" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                        <br/><br/><br/>
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegir_avatar('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>                                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL DE AVATARES -->  


<!-- MODAL VALORAR PEDIDO -->
<div class="modal fade" id="modal_calificar" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content modal-grande">
            <div class="modal-header bg-info text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-title-pedido-nro"> Turno / Compra</h3>
                </div>
            </div>

              <div class="block-content"> 
                  <!-- 
                  <div class="row hidden-xs">
                    <div class="col-sm-2 detalle-venta centrado">
                        <div class="text-center push-10" id="pedido_local_avatar"></div>                            
                    </div>
                    
                    <div class="col-sm-5 detalle-venta centrado">               
                        <div class="h4 push-5">Local: <b id="pedido_local"></b></div>
                        <div class="h5 push-5">Titular: <b id="pedido_titular_local"></b></div>     
                        <span  style="display: none"><i class="fa fa-phone"></i><span id="pedido_telefono_local"></span></span>
                        <div class="h5 push-5"><i class="fa fa-map-marker"></i><span id="pedido_direccion_local"></span></div>         
                    </div>  

                    <div class="col-sm-5 detalle-venta centrado">
                          <div class="h5 push-5">Fecha: <b id="pedido_fecha"></b></div>
                          <div class="h5 push-5" id="div_pedido_servicio">Servicio: <b id="pedido_servicio"></b></div>
                          <div class="h5 push-5" id="div_pedido_condicion_venta">Forma de pago: <b id="pedido_condicion_venta"></b></div>
                          <div class="h5 push-5">Estado: <b id="pedido_estado"></b></div>              
                    </div>                     
                  </div>
                  -->
                  <form class="form-horizontal action="#" id="form_calificar">
                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_calificar_error" style="display:none">
                          <small id="calificar_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->     

                      <div class="block-content">                        
                          <div class="form-body">

                              <input type="hidden" value="" name="calificar_id" id="calificar_id"/> 
                              <input type="hidden" value="" name="local_id" id="local_id"/> 
                              <input type="hidden" value="" name="local" id="local"/> 

                              <!-- Concretado -->
                              <div class="h5 form-group">
                                  <label class="h5 control-label push-10" id="label_calificar_concretado">¿Pudiste concretar la compra?</label>
                                  <div class="puntos_califica">
                                    <label><input name="calificar_concretado" type="radio" id="calificar_concretado" value="si" /> Si</label> <label><input name="calificar_concretado" type="radio" id="calificar_concretado" value="no" /> No</label>
                                  </div>
                              </div>

                              <!-- Calificacion -->
                              <div class="h5 form-group">
                                  <label class="h5 control-label push-10">¿Qué te pareció el servicio? </label>

                                  <div class="puntos_califica">
                                    <label><input name="calificar_puntaje" type="radio" id="calificar_puntaje" value="5" /> <br />Excelente</label> 
                                    <label><input name="calificar_puntaje" type="radio" id="calificar_puntaje" value="4" /> <br />Muy bueno</label>   
                                    <label><input name="calificar_puntaje" type="radio" id="calificar_puntaje" value="3" /> <br />Bueno</label>                          
                                    <label><input name="calificar_puntaje" type="radio" id="calificar_puntaje" value="2" /> <br />Regular</label>                                                                                            
                                    <label><input name="calificar_puntaje" type="radio" id="calificar_puntaje" value="1" /> <br />Malo</label>
                                  </div>
                              </div>        

                              <!-- Comentarios -->
                              <div class="form-group">

                                  <label class="h5 control-label push-5">Dejanos tus comentarios para ayudarnos a mejorar</label>
                                  <div>
                                      <textarea class="form-control" name="calificar_valoracion" id="calificar_valoracion" rows="3"></textarea>
                                  </div>
                              </div>     

                          </div>
                      </div>
                  </form>
                  <div class="clearfix"></div>
                  <hr>
                  <div class="form-group form-group-login">
                      <div class="text-center">
                          <button class="btn btn-info push-10-r" id="btn_calificar" onclick="aceptar_calificar();">Calificar</button>
                          <button class="btn btn-default btn-geotienda-default" type="button" data-dismiss="modal">Cancelar</button>
                      </div>    
                  </div>
                </div>
        </div>
    </div>
</div> 
<!-- fin MODAL VALORAR PEDIDO -->  


<!-- MODAL NUEVA / EDITAR MASCOTA -->
<div class="modal fade" id="modal_add_edit_mascota" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-add-edit-mascota">Nueva Mascota</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add_edit_mascota" class="form-horizontal" enctype="multipart/form-data">
                    <!-- Div Error -->
                    <div class="alert alert-danger alert-dismissable push-10-l push-10-r" id="div_add_edit_mascota_error" style="display:none">
                        <small id="add_edit_mascota_error_message"></small>                 
                    </div>
                    <!-- END Div Error -->  

                    <input type="hidden" value="" name="id_mascota"/> 
                    <div class="form-body">

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Nombre</label>
                            <div class="col-md-8 input-obligatorio">
                                <input id="nombre_mascota" name="nombre_mascota" placeholder="Nombre" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- ANIMAL --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Tipo de mascota</label>
                            <div class="col-md-8 input-obligatorio">
                                <select id="animal_mascota" name="animal_mascota" class="form-control">
                                    <option selected value="">- seleccioná -</option>
                                    <?php foreach($animales as $animal){  ?>
                                        <option value="<?php echo $animal->id ?>"><?php echo $animal->nombre ?></option> 
                                    <?php } ?>
                                </select>  
                            </div>
                        </div>

                        <!-- RAZA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Raza</label>
                            <div class="col-md-8 input-obligatorio">
                                <input id="raza_mascota" name="raza_mascota" placeholder="Raza" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- SEXO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Sexo</label>
                            <div class="col-md-8 input-obligatorio">
                                <select id="sexo_mascota" name="sexo_mascota" class="form-control">
                                    <option selected value="">- seleccioná -</option>
                                    <option value="hembra">Hembra</option> 
                                    <option value="macho">Macho</option> 
                                </select>  
                            </div>
                        </div>                

                        <!-- FECHA DE NACIMIENTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Fecha de nacimiento</label>
                            <div class="col-md-8 input-obligatorio">
                              <input class="js-datepicker form-control" type="text" id="nacimiento_mascota" name="nacimiento_mascota" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            </div>
                        </div>

                        <!-- PELAJE --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Pelaje</label>
                            <div class="col-md-8 input-obligatorio">
                                <input id="pelaje_mascota" name="pelaje_mascota" placeholder="Pelaje" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- TALLA --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Talla</label>
                            <div class="col-md-8 input-obligatorio">
                                <select id="talla_mascota" name="talla_mascota" class="form-control">
                                    <option selected value="">- seleccioná -</option>
                                    <option value="peque">Pequeño</option> 
                                    <option value="mediano">Mediano</option> 
                                    <option value="grande">Grande</option> 
                                    <option value="gigante">Gigante</option> 
                                </select>  
                            </div>
                        </div>   

                         <!-- FOTO --> 
                        <div class="form-group">
                            <label class="control-label col-md-3">Foto</label>
                            <div class="col-md-8">
  
                                <input name="foto_mascota" type="file" id="foto_mascota" size="40" class="btn btn-default block-table" >  
                                
                                <button type="button" id="btn_mostrar_fotos_mascotas" onclick="mostrar_fotos_mascotas()" class="btn btn-default push-10-t block-table">o elegí una foto ya cargada</button>  
                                <input type="hidden" id="foto_mascota_precargada" name="foto_mascota_precargada" />
                            </div>
                        </div>

                      <div class="form-group push-20-t">
                          <label class="col-md-3 control-label"></label>
                          <div class="col-md-8">
                              <span class="red">*</span> Datos obligatorios
                          </div>
                      </div>                                               
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_add_edit_mascota" onclick="save_mascota()" class="btn btn-vacunas">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL NUEVA MASCOTA -->


<!-- MODAL CARTILLA MASCOTA -->
<div class="modal fade" id="modal_cartilla_mascota" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content modal-grande">
            <div class="modal-header back-vacunas text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-center">
                    <h3 class="modal-title-mascota"></h3>
                </div>
            </div>
            <div class="modal-body form">
              <div class="block-content ">

              <!-- Datos mascota -->
              <!-- local asignado -->
              <div class="col-sm-6">  
                <div class="h4 push-10-t push-15">Tienda asignada: <b id="mascota_local"></b></div>                
              </div>

              <!-- Asignar nuevo local -->
              <div class="col-sm-6">                         
                  <div class="push-10-t push-5">Asignar nueva tienda a esta mascota</div>      

                <!-- Div Error -->
                <div class="alert alert-danger alert-dismissable" id="div_asignar_mascota_error" style="display:none">
                    <small id="asignar_mascota_error_message"></small>                 
                </div>
                <!-- END Div Error -->  

                  <div class=" push-5 pull-left push-10-r">
                    <select class="js-select2 form-control" id="locales_mascota" data-placeholder="seleccioná una tienda" style="width: 100%;">
                      <option></option>
                        <?php foreach($locales as $local){ ?>
                          <option value="<?php echo $local->id ?>"><?php echo $local->nombre ?></option>
                        <?php } ?>                            
                    </select>

                  </div>  
                  <div class="pull-left">
                    <button type="button" class="btn btn-vacunas" id="btn_asignar_local_a_mascota" onclick="asignar_local_a_mascota()">Asignar Tienda</button>
                  </div>    
                </div>             
              </div>
              <!-- FIN Asignar nuevo local -->

              <div class="clearfix" style="height: 90px"></div>

              <div class="block-content ">
                <div class="col-sm-3 centrado">
                    <div class="text-center push-10" id="mascota_foto"></div>                            
                </div>

                <div class="col-sm-4 push-30-t">               
                    <div class="push-5">Nombre: <b id="mascota_nombre"></b></div>
                    <div class=" push-5">Tipo: <b id="mascota_tipo_sexo"></b></div>     
                    <div class=" push-5">Nacimiento: <b id="mascota_nacimiento"></b></div>                             
                </div>

                <div class="col-sm-5 push-30-t">
                    <div class=" push-5">Raza: <b id="mascota_raza"></b></div>
                    <div class=" push-5">Pelaje: <b id="mascota_pelaje"></b></div>
                    <div class=" push-5">Talla: <b id="mascota_talla"></b></div>            
                </div>                   

                  <div class="clearfix"></div>

                  <!-- Vacunas -->
                  <div class="h4 push-10-t push-5">Vacunas</div> 
                  <div class="h5 push-10-t text-success">Próxima Vacuna: <span id="mascota_proxima_vacuna"></span></div>   
              </div>             

              <div class="block-content detalle-venta table-responsive">
                <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>
                  <table id="table_vacunas" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Vacuna</th>
                            <th>Tienda</th>
                            <th>Aplicada</th>
                            <th>Próxima Vacuna</th>
                            <th>Ver detalles</th>
                        </tr>
                    </thead>                            
                    <tbody>
                    </tbody>
                </table>
              </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL CARTILLA MASCOTA -->   

<!-- MODAL FOTOS PRECARGADAS MASCOTA -->
<div class="modal fade" id="modal_fotos_mascota" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Fotos de tus mascotas</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced">
                    <?php
                
                    $map = glob("assets/img/mascotas/miniaturas/" . $_SESSION['frontend_user_id'] . "_*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[4];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn"  id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group push-100-t">
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegir_foto_mascota('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                            <br/><br/><br/>
                                            <a class="btn btn-sm btn-default" href="javascript:void(0)" onclick="return eliminar_foto_mascota('<?php echo $nombreImagen; ?>');"><i class="fa fa-remove"></i> Eliminar</a>                                                 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL FOTOS PRECARGADAS MASCOTA -->    


<!-- MODAL CANCELAR PEDIDO o TURNO -->
<div class="modal fade" id="modal_cancelar" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-cancelar-title-pedido-nro"> Turno / Compra</h3>
                </div>
            </div>

              <div class="block-content"> 

                  <form class="form-horizontal action="#" id="form_cancelar">
                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_cancelar_error" style="display:none">
                          <small id="cancelar_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->     

                      <input type="hidden" value="" name="hd_cancela_id" id="hd_cancela_id"/> 
                      <div class="block-content">                        
                          <div class="form-body">
  
                              <!-- Comentario -->
                              <div class="form-group">

                                  <label class="h5 control-label push-5">Ingresá un comentario para cancelar</label>
                                  <div>
                                      <textarea class="form-control" name="cancelar_comentario" id="cancelar_comentario" rows="3"></textarea>
                                  </div>
                              </div>     
                          </div>
                      </div>
                  </form>
                  <hr>
                  <div class="form-group form-group-login">
                      <div class="text-center">
                          <button class="btn btn-danger push-10-r" id="btn_cancelar" onclick="aceptar_cancelar();">Cancelar</button>
                          <button class="btn btn-default" type="button" data-dismiss="modal">Volver</button>
                      </div>    
                  </div>
                </div>
        </div>
    </div>
</div> 
<!-- fin MODAL CANCELAR PEDIDO o TURNO --> 


<script type="text/javascript">

var table_pedidos;
var table_turnos_activos;
var table_turnos_vencidos;
var table_mascotas;
var table_historial;
var table_notificaciones;
var table_vacunas;
var tipo_pedido;
var save_mascota_method; 

$(document).ready(function() {

    var mascota_actual;

    // Usuario con sesion iniciada   
    <?php if (isset($_SESSION['frontend_logged_in'])){ ?>             

        $('#cliente_nombre').html('¡Hola <?php echo $_SESSION['frontend_nombre']; ?>!');
        $('#cliente_avatar').attr('src', '<?php echo BASE_PATH ?>/assets/img/<?php echo $_SESSION['frontend_tipo']; ?>/miniaturas/<?php echo $_SESSION['frontend_avatar']; ?>');               

         // Recupera direccion ingresada
        <?php if (isset($_SESSION['frontend_direccion'])){ ?>   
         
          direccion = '<?php echo $_SESSION['frontend_direccion']; ?>';
          localidad = '<?php echo $_SESSION['frontend_localidad']; ?>';

          $('.input-buscador-mini-dir-cliente').val(direccion);
          $('.input-buscador-mini-loc-cliente').val(localidad);

          $('.btn-listado-locales-cliente').show();
          
        <?php } ?>

        // Muestra datos según tab activo
        tab = '<?php echo $tab; ?>';

        // TODO: validar que la venta corresponda al cliente (en el caso que modifiquen la url)
        //venta_id = '<?php echo $venta_id; ?>';
        venta_id = '';

        switch (tab){
       
            case 'pedidos':                               
                mostrar_pedidos(venta_id);
            break;

            case 'turnos':
                mostrar_turnos();                                      
            break;  

            case 'mascotas':
                mostrar_mascotas();                                      
            break;  

            case 'historial':
                mostrar_historial();                                      
            break;  


            case 'notificaciones':                               
                mostrar_notificaciones();
            break;            

            case 'datos':
                mostrar_datos();                                      
            break;                                     
        }   

      var $select = $('#loc_mini');
      $select.select2();
      $select.val(localidad).trigger('change');

      var $select_m = $('#loc_mini_m');
      $select_m.select2();
      $select_m.val(localidad).trigger('change');            
    <?php } ?>
});


function mostrar_pedidos(){

    tab = 'pedidos';

    //datatables
    table_pedidos = $('#table_pedidos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "&nbsp;Mostrar&nbsp; _MENU_", 
            "emptyTable": "No tenés pedidos pendientes.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_ventas_pendientes_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false
        },       
        {
            "className": "hidden-xs", 
            "targets": [1,2,3], 
        },       
        {
            "className": "text-right hidden-xs",
            "targets": [4], 
        }
        ],

    });

    if (venta_id != '') detalle_venta_cliente(venta_id);
}



function mostrar_turnos(){

    tab = 'turnos';

    // Turnos Activos (fecha >= hoy)
    table_turnos_activos = $('#table_turnos_activos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
        "paging": false,
        "filter": false,
        "info": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_",
            "emptyTable": "No tenés turnos activos.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_turnos_activos_list",
            "type": "POST"
        },

        "columnDefs": [    
          {
              "targets": [1,2,3,4], 
              "className": "hidden-xs" 
          },  
          { 
              "targets": [ 4 ], 
              "className": "text-right"
          },                  
          {
              "targets": [-1], 
              "orderable": false 
          }        
        ],

    });


    // Turnos Vencidos (fecha < hoy)
    table_turnos_vencidos = $('#table_turnos_vencidos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable(
      { 
        "language": 
        {
            "lengthMenu": "Mostrar _MENU_",
            "emptyTable": "No tenés turnos vencidos.",
        },        
        "paging": false,
        "filter": false,
        "info": false,
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_turnos_vencidos_list",
            "type": "POST"
        },

        "columnDefs": [    
          {
              "targets": [1,2,3,4], 
              "className": "hidden-xs" 
          },  
          { 
              "targets": [ 4 ], 
              "className": "text-right"
          },                  
          {
              "targets": [-1], 
              "orderable": false 
          }        
        ],

    });
}


function mostrar_cancelar_turno(id)
{
    tipo_pedido = 'turno';

    var str = "" + 1;
    var pad = "S0000";
    var numero = pad.substring(0, pad.length - id.length) + id;

    $('#cancelar_comentario').val('');
    $('#div_cancelar_error').hide();   
    $("#hd_cancela_id").val(id);
    $('.modal-cancelar-title-pedido-nro').text('Cancelar turno ' + numero);
    
    $('#modal_cancelar').modal('show'); 
}


function mostrar_cancelar_pedido(id)
{
    tipo_pedido = 'venta';

    var str = "" + 1;
    var pad = "P0000";
    var numero = pad.substring(0, pad.length - id.length) + id;

    $('#cancelar_comentario').val('');
    $('#div_cancelar_error').hide();   
    $("#hd_cancela_id").val(id);
    $('.modal-cancelar-title-pedido-nro').text('Cancelar pedido ' + numero);
    
    $('#modal_cancelar').modal('show'); 
}


function aceptar_cancelar()
{
    if (tipo_pedido == 'venta')
      var url = "<?php echo BASE_PATH ?>/Venta/ajax_cancelar/cliente";
    else
      var url = "<?php echo BASE_PATH ?>/Turno/ajax_cancelar/cliente";


    $('#btn_cancelar').html('Procesando...');
    $('#btn_cancelar').attr('disabled', true); 

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_cancelar').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_cancelar_error').hide();  
                $('#modal_cancelar').modal('hide');  

                if (tipo_pedido == 'venta')
                {
                  table_pedidos.ajax.reload(null,false); 
                  aviso('success', 'Pedido cancelado.');  
                }
                else
                {
                  table_turnos_activos.ajax.reload(null,false); 
                  table_turnos_vencidos.ajax.reload(null,false); 
                  aviso('success', 'Turno cancelado.');                       
                }
                              
            }
            // Fallo en actualizacion
            else
            {
                switch(data.error) 
                {
                    // Horario excedido    
                    case 'horario':
                      $('#modal_cancelar').modal('hide');  
                      aviso('danger', data.mensaje, "NO ES POSIBLE CANCELAR EL TURNO");    
                      break;

                    // Estado del pedido
                    case 'estado':
                      $('#modal_cancelar').modal('hide');  
                      aviso('danger', data.mensaje, "NO ES POSIBLE CANCELAR EL PEDIDO");    
                      break;

                    // No se ingreso comentario
                    case 'comentario':
                      $('#cancelar_error_message').html(data.mensaje);
                      $('#div_cancelar_error').show();      
                      break;  

                    default:    
                      $('#cancelar_error_message').html(data.mensaje);
                      $('#div_cancelar_error').show();      
                      break;                                      
                }                               
            }

            $('#btn_cancelar').html('Cancelar');
            $('#btn_cancelar').attr('disabled', false);              

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_cancelar').modal('hide');  
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_cancelar').html('Cancelar');
            $('#btn_cancelar').attr('disabled', false);                   
        }
    });
}


function mostrar_historial(){

    tab = 'historial';

    //datatables
    table_historial = $('#table_historial')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
            "emptyTable": "No tenés pedidos o turnos en tu historial.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_historial_list",
            "type": "POST"
        },  
        "columnDefs": [   
        {
            "targets": [1,2,3,4,5], 
            "className": "hidden-xs", 
        }
        ],

    });

}


function mostrar_mascotas(){

    tab = 'mascotas';

    //datatables
    table_mascotas = $('#table_mascotas')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 

        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés mascotas.",
        },

        "lengthChange": false, 
        "paging": false,
        "filter": false,
        "ordering": false,
        "info": false,

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.


        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Mascota/ajax_list",
            "type": "POST"
        },  
        "columnDefs": [   
        {
            "targets": [1,2], 
            "className": "hidden-xs", 
        }
        ],

    });

}


function add_mascota()
{
    save_mascota_method = 'add';

    $('#form_add_edit_mascota')[0].reset(); 
    $('#div_add_edit_mascota_error').hide();
    $('.modal-title-add-edit-mascota').text('Nueva mascota');
    $('#modal_add_edit_mascota').modal('show'); 
}


function reload_table_mascotas()
{
    table_mascotas.ajax.reload(null,false); 
}


function mostrar_avatares()
{
    $('#modal_avatares').modal('show');
}


function elegir_avatar(nombre)
{

      $.ajax({
        url : "<?php echo BASE_PATH ?>/Cliente/ajax_cambiar_avatar/" + nombre,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
          // Avatar del dashboard
          $('#cliente_avatar').attr('src', '<?php echo IMG_PATH . 'cliente/miniaturas/' ?>'  + nombre);

          // Avatar del header
          $('#useravatar').attr('src', '<?php echo IMG_PATH . 'cliente/miniaturas/' ?>'  + nombre);
        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });


    $('#modal_avatares').modal('hide');
}


function mostrar_datos(){

    tab = 'datos';

    $('#form_cliente')[0].reset(); 

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Cliente/ajax_cargar/",        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#cliente-nombre').val(data.nombre);
            $('#cliente-apellido').val(data.apellido);
            $('#cliente-telefono').val(data.telefono);
            $('#cliente-email').val(data.email);
        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });

}


function mostrar_calificar(tipo, id){

    $('#form_calificar')[0].reset(); 
    tipo_pedido = tipo;

    if (tipo == 'venta')
      urlPedido = "<?php echo BASE_PATH ?>/admin/venta/get_venta_by_id/" + id;
    else
      urlPedido = "<?php echo BASE_PATH ?>/admin/turno/get_by_id/" + id;

    //$("#pedido_local_avatar").html('');

    // Encabezado
    $.ajax({
      url : urlPedido,
      cache: false,
      success: function(data){

        pedido = jQuery.parseJSON(data);

        // Datos del pedido
        if (tipo == 'venta'){
            $('.modal-title-pedido-nro').text('Calificar compra ' + pedido.numero);
            $('#label_calificar_concretado').text('¿Pudiste concretar la compra?');

            /*
            $('#div_pedido_condicion_venta').show();
            $('#pedido_condicion_venta').text(pedido.condicion_venta);
            $('#div_pedido_servicio').hide();            
            */
        }
            
        else{
            $('.modal-title-pedido-nro').text('Calificar turno ' + pedido.numero);
            $('#label_calificar_concretado').text('¿Pudiste concretar el turno?');

            /*            
            $('#div_pedido_condicion_venta').hide();
            $('#div_pedido_servicio').show();
            $('#pedido_servicio').text(pedido.servicio + ' - ' + pedido.servicio_item);
            */
        }        

        $('#calificar_id').val(id);
        $('#local_id').val(pedido.local_id);
        $('#local').val(pedido.local);

        /*
        $('#pedido_fecha').text(pedido.fecha);
        $('#pedido_estado').text(pedido.estado);
        
        // Datos del local
        /$("#pedido_local_avatar").append('<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + pedido.local_avatar + '" width="80">');

        $('#pedido_local').text(pedido.local);
        $('#pedido_titular_local').text(pedido.local_nombre_titular + ' ' + pedido.local_apellido_titular);
        $('#pedido_telefono_local').text(' ' + pedido.local_telefono);
        $('#pedido_direccion_local').text(' ' + pedido.local_direccion + ', ' + pedido.local_localidad);
        */

        $('#modal_calificar').modal('show'); 

      } 
    });
}


function aceptar_calificar(){

    if (tipo_pedido == 'venta')
      urlPedido = "<?php echo BASE_PATH ?>/Venta/ajax_calificar_venta";
    else
      urlPedido = "<?php echo BASE_PATH ?>/Turno/ajax_calificar_turno";

    $('#btn_calificar').html('Guardando...');
    $('#btn_calificar').attr('disabled', true); 

    $.ajax({
        url : urlPedido,
        type: "POST",
        data: $('#form_calificar').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Calificacion ok
            if(data.status) 
            {
                $('#div_calificar_error').hide();      
                $('#btn_calificar').html('Calificar');
                $('#btn_calificar').attr('disabled', false); 

                $('#modal_calificar').modal('hide');

                if (tipo_pedido == 'venta')
                  table_pedidos.ajax.reload(null,false); 
                else
                {
                  table_turnos_activos.ajax.reload(null,false); 
                  table_turnos_vencidos.ajax.reload(null,false);                  
                }

                aviso('success', '¡Gracias! \n\n\nEn instantes aparecerá tu calificación en la tienda.');    
            }

            // Fallo en Calificacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#calificar_error_message').html(mensaje_error);
                $('#div_calificar_error').show();      

                $('#btn_calificar').html('Calificar');
                $('#btn_calificar').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_calificar').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_calificar').html('Calificar');
            $('#btn_calificar').attr('disabled', false);                   
        }
    });

}


function actualizar_cliente()
{
    var url = "<?php echo BASE_PATH ?>/Cliente/ajax_update";

    $('#btn_actualizar_cliente').html('Guardando...');
    $('#btn_actualizar_cliente').attr('disabled', true); 

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_cliente').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_cliente_error').hide();   
                       
                mostrar_datos(); 

                $('#btn_actualizar_cliente').html('Guardar Cambios');
                $('#btn_actualizar_cliente').attr('disabled', false);  

                aviso('success', 'Tus datos fueron correctamente actualizados.');                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#cliente_error_message').html(mensaje_error);
                $('#div_cliente_error').show();      

                $('#btn_actualizar_cliente').html('Guardar Cambios');
                $('#btn_actualizar_cliente').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_actualizar_cliente').html('Guardar Cambios');
            $('#btn_actualizar_cliente').attr('disabled', false);                   
        }
    });
}


function cartilla_mascota_vacuna(vacuna_id)
{
  // Recupera el id de la mascota correspondiente
// Datos mascota
    $.ajax({
      url : "<?php echo BASE_PATH ?>/Mascota/get_by_vacuna_id/" + vacuna_id,
      cache: false,
      success: function(data){

        mascota = jQuery.parseJSON(data);
    
        // Muestra cartilla 
        cartilla_mascota(mascota.id);

      } 
    });

}

function cartilla_mascota(mascota_id)
{
    $("#mascota_foto").html('');
    $('#div_asignar_mascota_error').hide();
    mascota_actual = mascota_id;
    var foto = '';

    // Datos mascota
    $.ajax({
      url : "<?php echo BASE_PATH ?>/Mascota/get_by_id/" + mascota_id,
      cache: false,
      success: function(data){

        mascota = jQuery.parseJSON(data);

        if (mascota.foto)
          foto = mascota.foto;
        else if (mascota.tipo.toLowerCase() == 'gato')
          foto = 'gato.png';
        else
          foto = 'perro.png';

        $("#mascota_foto").append('<img src="<?php echo BASE_PATH ?>/assets/img/mascotas/miniaturas/' + foto + '" style="max-width: 180px;">');

        $('.modal-title-mascota').html('<i class="si si-notebook"></i> Cartilla de vacunas de ' + mascota.nombre);
        $('#mascota_nombre').text(mascota.nombre);
        $('#mascota_tipo_sexo').text(mascota.tipo + ', ' + mascota.sexo);
        $('#mascota_nacimiento').text(mascota.nacimiento);
        $('#mascota_raza').text(mascota.raza);
        $('#mascota_pelaje').text(mascota.pelaje);
        $('#mascota_talla').text(mascota.talla);
        $('#mascota_proxima_vacuna').text(mascota.proxima_vacuna);

        // Local asignado
        if(mascota.local)
          $('#mascota_local').text(mascota.local);
        else
          $('#mascota_local').text('sin asignar');
        
        // Muestra vacunas 
        mostrar_vacunas(mascota_id);

      } 
    });


    $('#modal_cartilla_mascota').modal('show'); 

}


function mostrar_vacunas(mascota_id)
{

  table_vacunas = $('#table_vacunas')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
        "paging": false,
        "filter": false,
        "info": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés vacunas asignadas a esta mascota.",
        },

        "lengthChange": false, 
        "ordering": false,
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "pageLength": 25,        

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Vacuna/ajax_vacunas_mascota/" + mascota_id,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, 
            "className": "hidden-xs", "targets": [2],             
        },
        ], 

    });
}


function asignar_local_a_mascota(){

    $('#div_asignar_mascota_error').hide();   

    var url = "<?php echo BASE_PATH ?>/Mascota/ajax_asignar_local";

    local_id = $( "#locales_mascota" ).val();


    $('#btn_asignar_local_a_mascota').html('Guardando...');
    $('#btn_asignar_local_a_mascota').attr('disabled', true); 

    $.ajax({
        url : url ,
        type: "POST",
        data: {'mascota_id': mascota_actual, 'local_id': local_id},
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#mascota_local').text($('#locales_mascota option:selected').text());
                reload_table_mascotas();

                $('#btn_asignar_local_a_mascota').html('Asignar Local');
                $('#btn_asignar_local_a_mascota').attr('disabled', false);  

                aviso('success', 'Local correctamente asignado.', '', 'bottom');                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#asignar_mascota_error_message').html(mensaje_error);
                $('#div_asignar_mascota_error').show();      

                $('#btn_asignar_local_a_mascota').html('Asignar Local');
                $('#btn_asignar_local_a_mascota').attr('disabled', false);                                               
            }
        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_cartilla_mascota').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_asignar_local_a_mascota').html('Asignar Local');
            $('#btn_asignar_local_a_mascota').attr('disabled', false);                   
        }
    });

}


function save_mascota()
{
    var url;

    if(save_mascota_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Mascota/ajax_add";
        mensaje = 'Tu mascota fue correctamente agregada.';
    } else {
        url = "<?php echo BASE_PATH ?>/Mascota/ajax_update";
        mensaje = 'Los datos de tu mascota fueron correctamente modificados.';
    }

    $('#btn_add_edit_mascota').html('Guardando...');
    $('#btn_add_edit_mascota').attr('disabled', true); 

    var formdata = new FormData(document.getElementById('form_add_edit_mascota'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,           
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_add_edit_mascota_error').hide();   
                $('#modal_add_edit_mascota').modal('hide');
                table_mascotas.ajax.reload(null,false);         

                $('#btn_add_edit_mascota').html('Guardar');
                $('#btn_add_edit_mascota').attr('disabled', false);  

                aviso('success', mensaje);                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#add_edit_mascota_error_message').html(mensaje_error);
                $('#div_add_edit_mascota_error').show();      

                $('#btn_add_edit_mascota').html('Guardar');
                $('#btn_add_edit_mascota').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_add_edit_mascota').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_add_edit_mascota').html('Guardar');
            $('#btn_add_edit_mascota').attr('disabled', false);                   
        }
    });
}


function mostrar_fotos_mascotas()
{
    $('#modal_fotos_mascota').modal('show');
}


function elegir_foto_mascota(foto)
{
    $('#foto_mascota_precargada').val(foto);
    $('#modal_fotos_mascota').modal('hide');
}


function borrar_mascota(nombre, id)
{
    if(confirm('¿Borrar mascota "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Mascota/ajax_delete/" + id,
            type: "POST",
            dataType: "JSON",

            success: function(data)
            {
                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table_mascotas();
                    
                    aviso('success', 'Mascota borrada.');                 
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL BORRAR MASCOTA");  
                    console.log(data.error);
                }
            },

            error: function (jqXHR, textStatus, errorThrown)
            {
                
                aviso('danger', textStatus, 'Error al borrar mascota. ' + textStatus); 
            }
        });

    }
}

function modificar_mascota(id)
{
    save_mascota_method = 'edit';

    $('#form_add_edit_mascota')[0].reset(); 
    $('#div_add_edit_mascota_error').hide();
    $('.modal-title-add-edit-mascota').text('Modificar mascota');
    $('#modal_add_edit_mascota').modal('show'); 


    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo BASE_PATH ?>/Mascota/get_by_id/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_mascota"]').val(data.id);
            $('[name="nombre_mascota"]').val(data.nombre);
            $('[name="animal_mascota"]').val(data.tipo_id);
            $('[name="raza_mascota"]').val(data.raza);
            $('[name="sexo_mascota"]').val(data.sexo);
            $('[name="nacimiento_mascota"]').val(data.nacimiento);
            $('[name="pelaje_mascota"]').val(data.pelaje);
            $('[name="talla_mascota"]').val(data.talla);
            $('[name="foto_mascota_precargada"]').val(data.foto);

            $('.modal-title').text('Modificar Mascota');
            $('#modal_form').modal('show');

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS DE LA MASCOTA"); 
        }
    });
}


function eliminar_foto_mascota(nombre)
{
    if(confirm('¿Eliminar foto?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Mascota/delete_foto/" + nombre,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                if(data.status)
                {
                     $('[id="' + nombre + '"]').hide(); 

                     // aviso('success', 'Foto eliminada.');    
                     alert('Foto eliminada.');  
                }
                else
                {
                    alert(data.mensaje);
                    //aviso('danger', data.mensaje, "ERROR AL ELIMINAR IMAGEN");  
                    
                    console.log(data.error);
                }

            },
            error: function (jqXHR, textStatus, errorThrown)
            {

                 aviso('danger', textStatus, "ERROR AL ELIMINAR FOTO"); 
            }
        });
    }
}


function mostrar_notificaciones()
{
  tab = 'notificaciones';

  table_notificaciones = $('#table_notificaciones')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés notificaciones.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 25,        

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Cliente/ajax_notificaciones_cliente",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false
        },
        {
            "className": "hidden-xs",
            "targets": [0,1], 
        }
        ],

        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                // Habilita drop-down list de busqueda solo en segunda columna
                if (column.index() == 1)
                {
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 140px"><option value="">TIPO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                        } );
     
                      select.append( '<option value="mensaje venta">Mensajes de Pedidos</option>' );
                      select.append( '<option value="mensaje turno">Mensajes de Servicios</option>' );
                      select.append( '<option value="cancela venta">Pedido cancelado</option>' );
                      select.append( '<option value="cancela turno">Turno cancelado</option>' );
                      select.append( '<option value="recordatorio turno">Recordatorio turno</option>' );
                      select.append( '<option value="recordatorio vacuna">Recordatorio vacuna</option>' );
                      select.append( '<option value="vacuna">Nueva vacuna</option>' );
                }
            } );
        },  

    });
}




</script>