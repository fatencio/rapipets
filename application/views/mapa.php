<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_mapa')">
        <i class="si si-camera text-white"></i>
    </button> 
<?php endif ?>


<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Mapa del Sitio</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Mapa del Sitio</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>  

<section class="content content-full content-boxed ancho-footer">  
    <div class="push-20-t push-20">
        <div class="row push-20-t items-push-2x text-center">

            <div class="col-sm-6">
                <ul class="list list-simple-mini font-s13">
                    <li>
                        <a class="h4 " href="<?php echo BASE_PATH ?>">Inicio - Buscar tienda</a>
                    </li>                            
                    <li>
                        <a class="h4 " href="javascript:void(0);" onclick="show_register();">Crear cuenta</a>
                    </li> 
                    <li>
                        <a class="h4 " href="javascript:void(0);" onclick="return show_login();">Ingresar</a>
                    </li>                         
                    <li>
                        <a class="h4 " href="<?php echo site_url('/contacto_local') ?>">Sumá tu comercio a GeoTienda</a>
                    </li>                        
                </ul>
            </div>

            <div class="col-sm-6">
                <ul class="list list-simple-mini font-s13">
                    <li>
                        <a class="h4 " href="<?php echo site_url('/terminos') ?>">Términos y condiciones</a>
                    </li>      
                    <li>
                        <a class="h4 " href="<?php echo site_url('/politicas') ?>">Políticas de privacidad</a>
                    </li>                                      
                    <li>
                        <a class="h4 " href="<?php echo site_url('/faq') ?>">Preguntas frecuentes</a>
                    </li>
                    <li>
                        <a class="h4 " href="<?php echo site_url('/contacto') ?>">Contacto</a>
                    </li>
                </ul>
            </div>                                           
        </div>
    </div>   
    <div class="text-center push-50-t push-50">
        <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
    </div>         
</section>