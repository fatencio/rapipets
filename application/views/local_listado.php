<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_local_listado')">
        <i class="si si-camera text-white"></i>
    </button>  
<?php endif ?>
            
<!-- PORTADA version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" class="text-center">
            <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
        </div>
    </div>    
</span>

<!-- PORTADA version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" class="text-center">
            <img src="../assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">                     
        </div>
    </div>    
</span>


<!-- fin RESULTADOS DE BUSQUEDA Y BUSCADOR MINI -->

<!-- version MOBILE -->
<section class="content content-full content-boxed portada-title visible-xs" style="top: <?php echo $portada_top_mobile; ?>px;">
    <div class="row">
        
        <div class="col-xs-7">
            <!-- Cantidad de resultados -->
            <h5 class="text-white font-w300 push-30-t" id="resultado_cant_locales_m"></h5>                                                        
        </div>                          

        <!-- Boton Nueva Busqueda -->   
        <div class="col-xs-5">
            <button class="btn btn-nueva-busqueda push-20-t" data-toggle="collapse" data-target="#campos_nueva_busqueda" id="btn_nueva_busqueda_m" >Nueva Búsqueda</button>
        </div>
    </div>              
</section>

<!-- version TABLET y LAPTOP -->
<section class="hidden-xs portada-title" style="top: <?php echo $portada_top; ?>px;">
    <div class="row push-5">
        <div class="col-sm-4 push-20-t">

            <!-- Cantidad de resultados -->
            <h4 class="text-white font-w400 push-5-t pull-left" id="resultado_cant_locales" style="padding-left: 48px;"></h4>                                                                  
        </div>

        <div class="col-sm-8 push-20-t push-5" style="padding-right: 48px;">   
            <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini" method="get">
            </form>                     
              <div class="col-sm-5">
                    <input class="form-control input-buscar-local-mini input-buscador-mini-dir font-w600 text-black" type="text" id="dir_mini" name="dir" placeholder="MI DIRECCION" required style="color:black;">
                    <div class="animated fadeInDown valida-buscador" id="valida_dir_mini" style="display:none">Ingresá tu dirección</div>                                
              </div>
              <div class="col-sm-4" id="div_localidad_mini">
                    <select id="loc_mini" name="loc" class="js-select2 form-control input-buscar-local-mini input-buscador-mini-loc font-w600 text-black" required data-placeholder="MI LOCALIDAD">
                        <option></option>
                        <?php 
                        foreach($localidades as  $localida_item){ ?>
                            <option value="<?php echo $localida_item->nombre ?>" ><?php echo $localida_item->nombre ?></option> 
                        <?php } ?>    
                    </select>                
                    <div class="animated fadeInDown valida-buscador" id="valida_loc_mini" style="display:none">Seleccioná tu localidad</div>                                
              </div>
            
              <div class="col-sm-3">
                    <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini" onclick="buscar_locales_mini();" >Buscar</a>
              </div>                     
        </div>                      
    </div>           
</section>          

<div id="campos_nueva_busqueda" class="collapse push-10-t">    
    <form action="<?php echo BASE_PATH ?>/Local/" id="form_buscar_local_mini_m" method="get">
    </form>                   
      <div class="col-sm-5 push-5">
            <input class="form-control input-buscar-local-mini-m input-buscador-mini-dir font-w600 text-black" type="text" id="dir_mini_m" name="dir" placeholder="MI DIRECCION" required>
            <div class="animated fadeInDown valida-buscador" id="valida_dir_mini_m" style="display:none">Ingresá tu dirección</div>
      </div>
      <div class="col-sm-4 push-5" id="div_localidad_mini_m">
            <select id="loc_mini_m" name="loc" class="js-select2" required data-placeholder="MI LOCALIDAD">
                <option></option>
                <?php 
                foreach($localidades as  $localidad_item){ ?>
                    <option value="<?php echo $localidad_item->nombre ?>" ><?php echo $localidad_item->nombre ?></option> 
                <?php } ?>    
            </select>      
            <div class="animated fadeInDown valida-buscador" id="valida_loc_mini_m" style="display:none">Seleccioná tu localidad</div>  
      </div>
      <div class="col-sm-3 push-5">
            <a class="btn btn-block btn-lg btn-geotienda btn-buscador-mini" id="btn_buscar_mini_m" onclick="buscar_locales_mini_m();" >Buscar</a>               
      </div>                      
</div>  

<!-- lISTADO DE LOCALES y FILTROS -->
<section class="content content-boxed full-pantalla">
    <div class="row">

        <div class="col-sm-2">

            <!-- ORDEN y FILTROS  -->
            <div class="input-group push-15 pull-left">                        
                <select id="ddl_orden_locales" name="ddl_orden_locales" class="form-control" size="1" onchange="ordenarLocales(this.value)" style="padding: 6px 5px !important; font-size: 13px !important;">
                    <option value="destacado" selected>Tiendas Destacadas</option>
                    <option value="distancia">Menor Distancia</option>
                    <option value="puntaje">Mejor puntaje</option>
                    <option value="nombre">Alfabéticamente</option>
                </select>
               <div class="input-group-btn">
                    <span class="btn"><i class="fa fa-sort-amount-asc"></i></span>
                </div>                        
            </div>  
                     
            <div class="block filtros-box">
                <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                    <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters" data-class="hidden-xs">
                        <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                    </button>

                    <div class="js-search-filters hidden-xs text-center push-10-t ">
                        <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span> 

                        <div class="div_cargando text-center">
                           <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                        </div>                        

                        <!-- FILTROS APLICADOS --> 
                        <div id="div_filtros_locales_aplicados" class="filtro-tipo push-10-t push-10" style="display: none;">
                            Filtros aplicados

                            <div id="div_filtros_locales_aplicados_items" class="filtros-aplicados push-10-t push-10"></div>
                            <hr>
                        </div>

                        <!-- FILTROS DISPONIBLES --> 
                        <div id="div_filtros_locales_categoria" class="push-10-t push-10"></div>
                        <div id="div_filtros_locales_animal" class="push-10"></div>                        
                        <div id="div_filtros_locales_rubro" class="push-10"></div>
                        <div id="div_filtros_locales_marca" class="push-10"></div>
                        <div id="div_filtros_locales_edad" class="push-10"></div>
                        <div id="div_filtros_locales_tamanio" class="push-10"></div>
                        <div id="div_filtros_locales_raza" class="push-10"></div>
                        <div id="div_filtros_locales_presentacion" class="push-10"></div>                    
                        <div id="div_filtros_locales_medicados" class="push-10"></div>                    
                        <div id="div_filtros_locales_servicio" class="push-10 push-10-t"></div>                    
                        <div id="div_filtros_locales_servicio_item" class="push-10"></div>                    
                    </div>
                </div>
            </div> 
            <!-- fin FILTROS -->
        </div>


        <div class="col-sm-10">             

            <!-- LISTADO DE LOCALES -->
            <div id="div_sin_locales">
            </div>            
            <div id="div_locales">
                <table id="table_locales" class="table" cellspacing="0" width="100%">
                    <thead>
                        <tr>                                 
                            <th>Local</th>                                     
                            <th>Filtro Categoria</th>                                     
                            <th>Filtro Categoria</th>                                     
                            <th>Filtro Servicio</th>                                     
                            <th>Filtro Rubro</th>                                     
                            <th>Filtro Animal</th>                                     
                            <th>Filtro Marca</th>                                     
                            <th>Filtro Raza</th>                                     
                            <th>Filtro Tamanio</th>                                     
                            <th>Filtro Edad</th>                                     
                            <th>Filtro Presentacion</th>                                     
                            <th>Filtro</th>                                     
                            <th>Cuenta Destacados</th>                                     
                        </tr>
                    </thead>                     
                    <tbody>
                    </tbody>
                </table>    
            </div>
            <!-- fin LISTADO DE LOCALES -->

        </div>
    </div>
</section>
<!-- fin lISTADO DE LOCALES y FILTROS -->


<!-- MODAL COMENTARIOS LOCAL -->
<div class="modal fade" id="modal_comentarios_local" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div id="div_comentarios_local_header"></div>
                    <div class="clearfix"></div>
                    <div id="div_comentarios_local_list" class="modal-overflow-contenido"></div>
                     
                    <div class="clearfix"></div>
                    <div class="text-center">
                        <button class="btn btn-lg btn-info btn-geotienda-celeste" type="button" data-dismiss="modal"> Cerrar</button>
                    </div>  
                    <br/>

                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL COMENTARIOS LOCAL --> 


<script type="text/javascript">



var resultado;
var filtros_aplicados = [];
var locales_array;
var categoria_actual;
var rubro_actual;
var medicado_actual;
var marca_actual;
var presentacion_actual;
var servicio_actual;
var cantidad = 0;

// Filtros
var cuenta_filtros_aplicados = 0;

var filtros_aplicados_rubro = '';
var filtros_aplicados_rubro_id = -1;
var select_rubro;
var filtro_rubro;
var aplicado_rubro = '';
var cuenta_filtro_rubro = 0;

var filtros_aplicados_categoria = '';
var select_categoria;
var filtro_categoria;
var aplicado_categoria = '';
var cuenta_filtro_categoria = 0;

var filtros_aplicados_animal = '';
var filtros_aplicados_animal_id = -1;
var select_animal;
var filtro_animal;
var aplicado_animal = '';
var cuenta_filtro_animal = 0;

var filtros_aplicados_marca = '';
var filtros_aplicados_marca_id = -1;
var select_marca;
var filtro_marca;
var aplicado_marca = '';
var cuenta_filtro_marca = 0;

var filtros_aplicados_raza = '';
var filtros_aplicados_raza_id = -1;
var select_raza;
var filtro_raza;
var aplicado_raza = '';
var cuenta_filtro_raza = 0;

var filtros_aplicados_tamanio = '';
var filtros_aplicados_tamanio_id = -1;
var select_tamanio;
var filtro_tamanio;
var aplicado_tamanio = '';
var cuenta_filtro_tamanio = 0;

var filtros_aplicados_edad = '';
var filtros_aplicados_edad_id = -1;
var select_edad;
var filtro_edad;
var aplicado_edad = '';
var cuenta_filtro_edad = 0;

var filtros_aplicados_presentacion = '';
var filtros_aplicados_presentacion_id = -1;
var select_presentacion;
var filtro_presentacion;
var aplicado_presentacion = '';
var cuenta_filtro_presentacion = 0;

var filtros_aplicados_medicados = '';
var filtros_aplicados_medicados_id = -1;
var select_medicados;
var filtro_medicados;
var aplicado_medicados = '';
var cuenta_filtro_medicados = 0;

var filtros_aplicados_servicio = '';
var filtros_aplicados_servicio_id = -1;
var select_servicio;
var filtro_servicio;
var aplicado_servicio = '';
var cuenta_filtro_servicio = 0;

var filtros_aplicados_servicio_item = '';
var filtros_aplicados_servicio_item_id = -1;
var select_servicio_item;
var filtro_servicio_item;
var aplicado_servicio_item = '';
var cuenta_filtro_servicio_item = 0;

var locales_id = [];

$(document).ready(function() 
{
    <?php 
        // Si el usuario activo es un local, redirecciona a su dashboard
        if (isset($_SESSION['frontend_logged_in'])){ 
    ?>  

         if('<?php echo $_SESSION['frontend_tipo']; ?>' == 'local') window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos';                              
    <?php } ?>

    window.scrollTo(0, 0);

    direccion = '<?php echo $direccion; ?>';
    localidad = '<?php echo $localidad; ?>';

    $('.input-buscador-mini-dir').val(direccion);
    //$('.input-buscador-mini-loc').val(localidad);

    var $select = $('#loc_mini');
    $select.select2();
    $select.val(localidad).trigger('change');

    var $select_m = $('#loc_mini_m');
    $select_m.select2();
    $select_m.val(localidad).trigger('change');

    listar_locales('<?php echo $latitud; ?>', '<?php echo $longitud; ?>');  
});


function ordenarLocales(orden){

    var index = 1;
    var table_locales = $('#table_locales').DataTable();

    switch (orden)
    {
         case 'distancia':
            index = 1;
            orden = 'desc';
            break;

         case 'puntaje':
            index = 2;
            orden = 'desc';
            break;

         case 'nombre':
             index = 3;
             orden = 'asc';
             break;

         case 'destacado':
             index = 4;
             orden = 'desc';
             break;             
    }

    table_locales
        .columns(index)
        .order(orden)
        .draw();    
}


function listar_locales(latitud, longitud)
{

    //datatables
    table_locales = $('#table_locales')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable(
      { 
        "language": 
        {
            "lengthMenu": "&nbsp;Mostrar&nbsp; _MENU_", 
            "emptyTable": "No encontramos tiendas.",
        },
        "info": false,
        "order": [], //Initial no order.
        "lengthMenu": [[10, 25, 50, -1], ["10 tiendas" , "25 tiendas", "50 tiendas", "Todas"]],
        "pageLength": 10,
        //"lengthChange": false, 
        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        "ajax": 
        {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_locales/" + latitud + "/" + longitud,
            "type": "POST"
        },  
        "columnDefs": 
        [
            {
                "targets": [ 1,2,3,4,5,6,7,8,9,10,11,12 ],
                "visible": false
            }
        ],
        initComplete: function() 
        {
            // Filtro por CATEGORIA
            $('#div_filtros_locales_categoria').html('');

            filtro_categoria = $('<div><div class="filtro-tipo">CATEGORÍA</div></div>')
                .appendTo( $('#div_filtros_locales_categoria'));

            select_categoria= $('<ul id="ddl_categoria"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_categoria'));

            // Filtro por RUBRO
            $('#div_filtros_locales_rubro').html('');

            filtro_rubro = $('<div><div class="filtro-tipo">RUBRO</div></div>')
                .appendTo( $('#div_filtros_locales_rubro'));

            select_rubro = $('<ul id="ddl_rubro"  class="filtro-select push-15"></ul>')
                .appendTo( $('#div_filtros_locales_rubro'));

            // Filtro por ANIMAL
            $('#div_filtros_locales_animal').html('');

            filtro_animal = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
                .appendTo( $('#div_filtros_locales_animal'));

            select_animal= $('<ul id="ddl_animal"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_animal'));

            // Filtro por MARCA
            $('#div_filtros_locales_marca').html('');

            filtro_marca = $('<div><div class="filtro-tipo">MARCA</div></div>')
                .appendTo( $('#div_filtros_locales_marca'));

            select_marca= $('<ul id="ddl_marca"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_marca'));

            // Filtro por RAZA
            $('#div_filtros_locales_raza').html('');

            filtro_raza = $('<div><div class="filtro-tipo">RAZA</div></div>')
                .appendTo( $('#div_filtros_locales_raza'));

            select_raza= $('<ul id="ddl_raza"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_raza'));                

            // Filtro por TAMAÑO
            $('#div_filtros_locales_tamanio').html('');

            filtro_tamanio = $('<div><div class="filtro-tipo">TAMAÑO</div></div>')
                .appendTo( $('#div_filtros_locales_tamanio'));

            select_tamanio= $('<ul id="ddl_tamanio"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_tamanio'));    

            // Filtro por EDAD
            $('#div_filtros_locales_edad').html('');

            filtro_edad = $('<div><div class="filtro-tipo">EDAD</div></div>')
                .appendTo( $('#div_filtros_locales_edad'));

            select_edad= $('<ul id="ddl_edad"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_edad'));      

            // Filtro por PRESENTACION
            $('#div_filtros_locales_presentacion').html('');

            filtro_presentacion = $('<div><div class="filtro-tipo">PRESENTACIÓN</div></div>')
                .appendTo( $('#div_filtros_locales_presentacion'));

            select_presentacion= $('<ul id="ddl_presentacion"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_presentacion'));      

            // Filtro por MEDICADOS
            $('#div_filtros_locales_medicados').html('');

            filtro_medicados = $('<div><div class="filtro-tipo">MEDICADOS</div></div>')
                .appendTo( $('#div_filtros_locales_medicados'));

            select_medicados= $('<ul id="ddl_medicados"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_medicados'));    


            // Filtro por SERVICIO
            $('#div_filtros_locales_servicio').html('');

            filtro_servicio = $('<div><div class="filtro-tipo">SERVICIO</div></div>')
                .appendTo( $('#div_filtros_locales_servicio'));

            select_servicio= $('<ul id="ddl_servicio"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_servicio'));  

            // Filtro por SERVICIO ITEM
            $('#div_filtros_locales_servicio_item').html('');

            filtro_servicio_item = $('<div><div class="filtro-tipo">ITEM</div></div>')
                .appendTo( $('#div_filtros_locales_servicio_item'));

            select_servicio_item= $('<ul id="ddl_servicio_item"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_locales_servicio_item'));  


            // Oculta inicialmente filtros que dependen del Rubro y Animal seleccionado
            $('#div_filtros_locales_marca').hide();
            $('#div_filtros_locales_raza').hide();
            $('#div_filtros_locales_tamanio').hide();
            $('#div_filtros_locales_edad').hide();
            $('#div_filtros_locales_presentacion').hide();
            $('#div_filtros_locales_medicados').hide();                        
            filtro_marca.hide();
            filtro_raza.hide();
            filtro_tamanio.hide();
            filtro_edad.hide();
            filtro_presentacion.hide();
            filtro_medicados.hide();

            $('#div_filtros_locales_servicio_item').hide();
            $('#div_filtros_locales_servicio').hide();
            filtro_servicio_item.hide();
            filtro_servicio.hide();

        },        
        drawCallback: function() 
        {
            // Inicializa variables
            var count_destacados = 0;
            cuenta_filtro_categoria = 0;
            cuenta_filtro_rubro = 0;
            cuenta_filtro_animal = 0;
            cuenta_filtro_marca = 0;
            cuenta_filtro_raza = 0;
            cuenta_filtro_tamanio = 0;
            cuenta_filtro_edad = 0;
            cuenta_filtro_presentacion = 0;
            cuenta_filtro_medicados = 0;
            cuenta_filtro_servicio = 0;
            cuenta_filtro_servicio_item = 0;

            $('#div_filtros_locales_aplicados_items').html('');

            this.api().columns().every( function () 
            {                                                                                                 
                var column = this;

                // Cuenta locales destacados
                if (column.index() == 12)
                {
                    column.data().each( function ( d, j ) 
                    {
                        if (d == 1) count_destacados ++;
                    } );

                    // Si no hay locales destacados, oculta opcion 'Tiendas Destacadas' en select de orden
                    $("#ddl_orden_locales option[value='destacado']").remove();

                    if (count_destacados > 0)
                    {
                        $("#ddl_orden_locales").prepend("<option value='destacado' selected='selected'>Tiendas Destacadas</option>");
                    }

                }

                // Filtro por CATEGORIA
                if (column.index() == 1)
                {  
                    if (filtros_aplicados_categoria == '')
                    {                                
                        if (filtro_categoria) filtro_categoria.show();
                        if (select_categoria)
                        {
                            select_categoria.show();      
                            select_categoria.html('');
                        } 

                        // Agrega items del filtro
                        $.ajax({
                           type: "POST",
                           data: {'locales_id': locales_id},
                           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_categoria",
                           success: function(data)
                           {
                               data_categoria = jQuery.parseJSON(data);

                               $.each(data_categoria.categorias, function(indice, categoria)
                                {
                                    var option = '<li class="filtro-item" value="' + categoria.nombre + '">' + categoria.nombre_cuenta+ '</li>';
                                    var item = $(option)
                                        .appendTo(select_categoria)
                                        .on('click', function () 
                                        {
                                            $('.div_cargando').show();

                                            var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                            filtros_aplicados_categoria = categoria.nombre;
                 
                                            // Busca por el item seleccionado
                                            column
                                                .search( val ? val : '', true, false )
                                                .draw();

                                            cuenta_filtros_aplicados++;

                                            // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                            if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                        } )   

                                    cuenta_filtro_categoria++;
                                });   

                           }
                       });
                    }

                    // Filtro ya aplicado
                    else
                    {
                        // Oculta este filtro
                        filtro_categoria.hide();
                        select_categoria.hide();

                        // Agrega item seleccionado a filtros aplicados
                        aplicado_categoria = $('<div />')
                            .html(filtros_aplicados_categoria + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                            .appendTo( $('#div_filtros_locales_aplicados_items'))

                            // Funcion para quitar filtro aplicado
                            .on('click', function ()
                            { 
                                $('.div_cargando').show();

                                // Reset del filtro actual                                    
                                var val = '';

                                column
                                    .search( val ? val : '', true, false )
                                    .draw();                                    

                                cuenta_filtros_aplicados--;
                                filtros_aplicados_categoria = '';

                                //Quita el item
                                this.remove();

                                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                            }); 
                    }                              
                }
                   

                // Filtro por ANIMALES
                if (column.index() == 2)
                {
                    if (filtros_aplicados_animal == '')
                    {                                
                        if (filtro_animal) filtro_animal.show();

                        if (select_animal)
                        {
                            select_animal.show();      
                            select_animal.html('');
                        } 

                        // Agrega items del filtro
                        $.ajax({
                           type: "POST",
                           data: {'locales_id': locales_id},
                           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_animal/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_servicio_id + "/" + filtros_aplicados_servicio_item_id,
                           success: function(data)
                           {
                               data_animal = jQuery.parseJSON(data);

                               $.each(data_animal.animales, function(indice, animal)
                                {
                                    var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                                    var item = $(option)
                                        .appendTo(select_animal)
                                        .on('click', function () 
                                        {
                                            $('.div_cargando').show();
                                            var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                            filtros_aplicados_animal = animal.nombre;
                                            filtros_aplicados_animal_id = animal.id;
                 
                                            // Busca por el item seleccionado
                                            column
                                                .search( val ? val : '', true, false )
                                                .draw();

                                            cuenta_filtros_aplicados++;

                                            // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                            if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                        } )   

                                    cuenta_filtro_animal++;
                                });    

                           }
                       });

                        // Oculta filtros que dependen de Animal
                        if (filtro_marca)
                        {
                            filtro_marca.hide();
                            select_marca.hide();
                        } 

                        if (filtro_raza)
                        {
                            filtro_raza.hide();
                            select_raza.hide();
                        }    

                        if (filtro_tamanio)
                        {
                            filtro_tamanio.hide();
                            select_tamanio.hide();
                        }  

                        if (filtro_edad)
                        {
                            filtro_edad.hide();
                            select_edad.hide();
                        }  

                        if (filtro_presentacion)
                        {
                            filtro_presentacion.hide();
                            select_presentacion.hide();
                        }     

                        if (filtro_medicados)
                        {
                            filtro_medicados.hide();
                            select_medicados.hide();
                        }                                                                     
                    }

                    // Filtro ya aplicado
                    else
                    {
                        // Oculta este filtro
                        filtro_animal.hide();
                        select_animal.hide();

                        // Agrega item seleccionado a filtros aplicados
                        aplicado_animal = $('<div />')
                            .html(filtros_aplicados_animal + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                            .appendTo( $('#div_filtros_locales_aplicados_items'))

                            // Funcion para quitar filtro aplicado
                            .on('click', function ()
                            { 
                                var val = '';

                                $('.div_cargando').show();

                                // Quita filtros aplicados que dependen de Animal
                                var table_locales = $('#table_locales').DataTable();
                                table_locales.columns(4).search( val ? val : '', true, false );
                                table_locales.columns(5).search( val ? val : '', true, false );
                                table_locales.columns(6).search( val ? val : '', true, false );
                                table_locales.columns(7).search( val ? val : '', true, false );
                                table_locales.columns(8).search( val ? val : '', true, false );
                                table_locales.columns(9).search( val ? val : '', true, false );

                                // Reset del filtro actual                                    
                                column
                                    .search( val ? val : '', true, false )
                                    .draw();                                    

                                cuenta_filtros_aplicados--;
                                filtros_aplicados_animal = '';
                                filtros_aplicados_animal_id = -1;

                                //Quita el item
                                this.remove();


                                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                            }); 
                    }
                }


                // Filtro por RUBRO
                if (column.index() == 3)
                {
                    if (filtros_aplicados_rubro == '')
                    {                                
                        if (filtro_rubro) filtro_rubro.show();

                        if (select_rubro)
                        {
                            select_rubro.show();      
                            select_rubro.html('');
                        } 

                        // Agrega items del filtro
                        $.ajax({
                           type: "POST",
                           data: {'locales_id': locales_id},
                           url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_rubro/" + filtros_aplicados_animal_id,
                           success: function(data)
                           {
                               data_rubro = jQuery.parseJSON(data);

                               $.each(data_rubro.rubros, function(indice, rubro)
                                {
                                    var option = '<li class="filtro-item" value="' + rubro.id + '">' + rubro.nombre_cuenta + '</li>';
                                    var item = $(option)
                                        .appendTo(select_rubro)
                                        .on('click', function () 
                                        {
                                            $('.div_cargando').show();
                                            var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                            filtros_aplicados_rubro = rubro.nombre;
                                            filtros_aplicados_rubro_id = rubro.id;
                 
                                            // Busca por el item seleccionado
                                            column
                                                .search( val ? val : '', true, false )
                                                .draw();

                                            cuenta_filtros_aplicados++;

                                            // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                            if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                        } )   

                                    cuenta_filtro_rubro++;
                                }); 

                           }
                       });

                        // Oculta filtros que dependen de Rubro
                        if (filtro_marca)
                        {
                            filtro_marca.hide();
                            select_marca.hide();
                        } 

                        if (filtro_raza)
                        {
                            filtro_raza.hide();
                            select_raza.hide();
                        }    

                        if (filtro_tamanio)
                        {
                            filtro_tamanio.hide();
                            select_tamanio.hide();
                        }   

                        if (filtro_edad)
                        {
                            filtro_edad.hide();
                            select_edad.hide();
                        } 

                        if (filtro_presentacion)
                        {
                            filtro_presentacion.hide();
                            select_presentacion.hide();
                        } 

                        if (filtro_medicados)
                        {
                            filtro_medicados.hide();
                            select_medicados.hide();
                        } 

                    }

                    // Filtro ya aplicado
                    else
                    {
                        // Oculta filtros por servicios
                        if (filtro_servicio)
                        {
                            filtro_servicio.hide();
                            select_servicio.hide();
                        } 

                        // Oculta este filtro
                        filtro_rubro.hide();
                        select_rubro.hide();

                        // Agrega item seleccionado a filtros aplicados
                        aplicado_rubro = $('<div />')
                            .html(filtros_aplicados_rubro + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                            .appendTo( $('#div_filtros_locales_aplicados_items'))

                            // Funcion para quitar filtro aplicado
                            .on('click', function ()
                            { 
                                var val = '';

                                $('.div_cargando').show();

                                // Quita filtros aplicados que dependen de Rubro
                                var table_locales = $('#table_locales').DataTable();
                                table_locales.columns(4).search( val ? val : '', true, false );
                                table_locales.columns(5).search( val ? val : '', true, false );
                                table_locales.columns(6).search( val ? val : '', true, false );
                                table_locales.columns(7).search( val ? val : '', true, false );
                                table_locales.columns(8).search( val ? val : '', true, false );
                                table_locales.columns(9).search( val ? val : '', true, false );

                                // Reset del filtro actual                                    
                                filtros_aplicados_rubro = '';
                                filtros_aplicados_rubro_id = -1;

                                column
                                    .search( val ? val : '', true, false )
                                    .draw();                                    

                                cuenta_filtros_aplicados--;

                                //Quita el item
                                this.remove();

                                // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                            });

                    }
                }


                // Filtro por MARCA
                if (column.index() == 4)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'marca' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_marca == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_marca/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_marca = jQuery.parseJSON(data);

                                   if (data_marca.marcas)
                                   {
                                        $('#div_filtros_locales_marca').show();
                                        if (filtro_marca) filtro_marca.show();

                                        if (select_marca)
                                        {
                                            select_marca.show();      
                                            select_marca.html('');
                                        } 

                                       $.each(data_marca.marcas, function(indice, marca)
                                        {
                                            var option = '<li class="filtro-item" value="' + marca.id + '">' + marca.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_marca)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_marca = marca.nombre;
                                                    filtros_aplicados_marca_id = marca.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_marca++;   
                                        });   

                                   }

                                   if (data_marca.marcas.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_marca.hide();
                                        select_marca.hide();
                                   }                                      
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_marca.hide();
                            select_marca.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_marca = $('<div />')
                                .html(filtros_aplicados_marca + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_marca = '';
                                    filtros_aplicados_marca_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        if (filtros_aplicados_marca != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_marca = '';
                            filtros_aplicados_marca_id = -1;
                        }                                
                    }
                }


                // Filtro por EDAD
                if (column.index() == 5)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'edad' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_edad == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_edad/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_edad = jQuery.parseJSON(data);

                                   if (data_edad.edades)
                                   {
                                        $('#div_filtros_locales_edad').show();
                                        if (filtro_edad) filtro_edad.show();

                                        if (select_edad)
                                        {
                                            select_edad.show();      
                                            select_edad.html('');
                                        } 

                                       $.each(data_edad.edades, function(indice, edad)
                                        {
                                            var option = '<li class="filtro-item" value="' + edad.nombre + '">' + edad.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_edad)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_edad = edad.nombre;
                                                    filtros_aplicados_edad_id = edad.nombre;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_edad++;   
                                        });   

                                   }
                                   
                                   if (data_edad.edades.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_edad.hide();
                                        select_edad.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_edad.hide();
                            select_edad.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_edad = $('<div />')
                                .html(filtros_aplicados_edad + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_edad = '';
                                    filtros_aplicados_edad_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_edad != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_edad = '';
                            filtros_aplicados_edad_id = -1;
                        }
                    }                    
                }


                // Filtro por TAMAÑO
                if (column.index() == 6)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'tamanio' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_tamanio == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_tamanio/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_tamanio = jQuery.parseJSON(data);

                                   if (data_tamanio.tamanios)
                                   {
                                        $('#div_filtros_locales_tamanio').show();
                                        if (filtro_tamanio) filtro_tamanio.show();

                                        if (select_tamanio)
                                        {
                                            select_tamanio.show();      
                                            select_tamanio.html('');
                                        } 

                                       $.each(data_tamanio.tamanios, function(indice, tamanio)
                                        {
                                            var option = '<li class="filtro-item" value="' + tamanio.id + '">' + tamanio.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_tamanio)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_tamanio = tamanio.nombre;
                                                    filtros_aplicados_tamanio_id = tamanio.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_tamanio++;   
                                        });   

                                   }
                                   
                                   if (data_tamanio.tamanios.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_tamanio.hide();
                                        select_tamanio.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_tamanio.hide();
                            select_tamanio.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_tamanio = $('<div />')
                                .html(filtros_aplicados_tamanio + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_tamanio = '';
                                    filtros_aplicados_tamanio_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_tamanio != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_tamanio = '';
                            filtros_aplicados_tamanio_id = -1;
                        }
                    }                       
                }

                // Filtro por RAZA
                if (column.index() == 7)
                {
                    // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'raza' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_raza == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_raza/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_raza = jQuery.parseJSON(data);

                                   if (data_raza.razas)
                                   {
                                        $('#div_filtros_locales_raza').show();
                                        if (filtro_raza) filtro_raza.show();

                                        if (select_raza)
                                        {
                                            select_raza.show();      
                                            select_raza.html('');
                                        } 

                                       $.each(data_raza.razas, function(indice, raza)
                                        {
                                            var option = '<li class="filtro-item" value="' + raza.id + '">' + raza.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_raza)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_raza = raza.nombre;
                                                    filtros_aplicados_raza_id = raza.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_raza++;   
                                        });   

                                   }

                                   if (data_raza.razas.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_raza.hide();
                                        select_raza.hide();
                                   }                                   
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_raza.hide();
                            select_raza.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_raza = $('<div />')
                                .html(filtros_aplicados_raza + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_raza = '';
                                    filtros_aplicados_raza_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_raza != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_raza = '';
                            filtros_aplicados_raza_id = -1;
                        }
                    }                      
                }

                           
                // Filtro por PRESENTACION
                if (column.index() == 8)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'presentacion' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_presentacion == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_presentacion/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_medicados_id,
                               success: function(data)
                               {
                                   data_presentacion = jQuery.parseJSON(data);

                                   if (data_presentacion.presentaciones)
                                   {
                                        $('#div_filtros_locales_presentacion').show();
                                        if (filtro_presentacion) filtro_presentacion.show();

                                        if (select_presentacion)
                                        {
                                            select_presentacion.show();      
                                            select_presentacion.html('');
                                        } 

                                       $.each(data_presentacion.presentaciones, function(indice, presentacion)
                                        {
                                            var option = '<li class="filtro-item" value="' + presentacion.id + '">' + presentacion.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_presentacion)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_presentacion = presentacion.nombre;
                                                    filtros_aplicados_presentacion_id = presentacion.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_presentacion++;   
                                        });   

                                   }
                                   
                                   if (data_presentacion.presentaciones.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_presentacion.hide();
                                        select_presentacion.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_presentacion.hide();
                            select_presentacion.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_presentacion = $('<div />')
                                .html(filtros_aplicados_presentacion + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_presentacion = '';
                                    filtros_aplicados_presentacion_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_presentacion != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_presentacion = '';
                            filtros_aplicados_presentacion_id = -1;
                        }
                    }                      
                }


                // Filtro por MEDICADOS
                if (column.index() == 9)
                {
                    // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'medicados' (Alimentos por ej)
                    if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                    {
                        if (filtros_aplicados_medicados == '')
                        {                                
                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_medicados/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id,
                               success: function(data)
                               {
                                   data_medicados = jQuery.parseJSON(data);

                                   if (data_medicados.medicados)
                                   {
                                        $('#div_filtros_locales_medicados').show();
                                        if (filtro_medicados) filtro_medicados.show();

                                        if (select_medicados)
                                        {
                                            select_medicados.show();      
                                            select_medicados.html('');
                                        } 

                                       $.each(data_medicados.medicados, function(indice, medicados)
                                        {
                                            var option = '<li class="filtro-item" value="' + medicados.id + '">' + medicados.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_medicados)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();
                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_medicados = medicados.nombre;
                                                    filtros_aplicados_medicados_id = medicados.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )

                                                cuenta_filtro_medicados++;   
                                        });   

                                   }
                                   else
                                   {
                                         // Oculta este filtro
                                        filtro_medicados.hide();
                                        select_medicados.hide();                                   
                                   }
                                   
                                   if (data_medicados.medicados.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_medicados.hide();
                                        select_medicados.hide();
                                   }
                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_medicados.hide();
                            select_medicados.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_medicados = $('<div />')
                                .html('Medicados: ' + filtros_aplicados_medicados + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();
                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_medicados = '';
                                    filtros_aplicados_medicados_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                });
                        }                       
                    }
                    else
                    {
                        // Si el filtro ya estaba aplicado, lo quita
                        if (filtros_aplicados_medicados != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_medicados = '';
                            filtros_aplicados_medicados_id = -1;
                        }
                    }                      
                }   


                // Filtro por SERVICIO
                if (column.index() == 10)
                {  
                    // Solo se muestra si no se seleccionó un rubro
                    if (filtros_aplicados_rubro == '')
                    {        
                        if (filtros_aplicados_servicio == '')
                        {                                
                            if (filtro_servicio) filtro_servicio.show();
                            if (select_servicio)
                            {
                                select_servicio.show();      
                                select_servicio.html('');
                            } 

                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_servicio/" + filtros_aplicados_animal_id,
                               success: function(data)
                               {
                                   data_servicio = jQuery.parseJSON(data);

                                   if (data_servicio.servicios)
                                   {

                                        $('#div_filtros_locales_servicio').show();
                                        if (filtro_servicio) filtro_servicio.show();

                                        if (select_servicio)
                                        {
                                            select_servicio.show();      
                                            select_servicio.html('');
                                        } 

                                       $.each(data_servicio.servicios, function(indice, servicio)
                                        {
                                            var option = '<li class="filtro-item" value="' + servicio.id + '">' + servicio.nombre_cuenta+ '</li>';
                                            var item = $(option)
                                                .appendTo(select_servicio)
                                                .on('click', function () 
                                                {
                                                    $('.div_cargando').show();

                                                    var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                    filtros_aplicados_servicio = servicio.nombre;
                                                    filtros_aplicados_servicio_id = servicio.id;
                         
                                                    // Busca por el item seleccionado
                                                    column
                                                        .search( val ? val : '', true, false )
                                                        .draw();

                                                    cuenta_filtros_aplicados++;

                                                    // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                    if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                                } )   

                                            cuenta_filtro_servicio++;
                                        });   

                                    
                                   }

                                   if (data_servicio.servicios.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_servicio.hide();
                                        select_servicio.hide();
                                   }                                

                               }
                           });


                            if (filtro_servicio_item)
                            {
                                filtro_servicio_item.hide();
                                select_servicio_item.hide();
                            }                          
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta filtros por rubros
                            if (filtro_rubro)
                            {
                                filtro_rubro.hide();
                                select_rubro.hide();
                            }     

                            // Oculta este filtro
                            filtro_servicio.hide();
                            select_servicio.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_servicio = $('<div />')
                                .html(filtros_aplicados_servicio + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    var val = '';

                                    $('.div_cargando').show();

                                    // Quita filtros aplicados que dependen de Servicio
                                    var table_locales = $('#table_locales').DataTable();
                                    table_locales.columns(11).search( val ? val : '', true, false );

                                    // Reset del filtro actual                                    
                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_servicio = '';
                                    filtros_aplicados_servicio_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                }); 
                        }  
                    }                            
                }     


                // Filtro por SERVICIO ITEM
                if (column.index() == 11)
                {  
                    // Solo se muestra si se seleccionó un servicio
                    if (filtros_aplicados_servicio != '')
                    {
                        if (filtros_aplicados_servicio_item == '')
                        {         
                            $('#div_filtros_locales_servicio_item').show();                       

                            if (filtro_servicio_item) filtro_servicio_item.show();
                            if (select_servicio_item)
                            {
                                select_servicio_item.show();      
                                select_servicio_item.html('');
                            } 

                            // Agrega items del filtro
                            $.ajax({
                               type: "POST",
                               data: {'locales_id': locales_id},
                               url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_local_servicio_item/" + filtros_aplicados_servicio_id + "/" + filtros_aplicados_animal_id,
                               success: function(data)
                               {
                                   data_servicio_item = jQuery.parseJSON(data);

                                   $.each(data_servicio_item.servicio_items, function(indice, servicio_item)
                                    {
                                        var option = '<li class="filtro-item" value="' + servicio_item.id + '">' + servicio_item.nombre_cuenta+ '</li>';
                                        var item = $(option)
                                            .appendTo(select_servicio_item)
                                            .on('click', function () 
                                            {
                                                $('.div_cargando').show();

                                                var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                filtros_aplicados_servicio_item = servicio_item.nombre;
                                                filtros_aplicados_servicio_item_id = servicio_item.id;
                     
                                                // Busca por el item seleccionado
                                                column
                                                    .search( val ? val : '', true, false )
                                                    .draw();

                                                cuenta_filtros_aplicados++;

                                                // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                if (cuenta_filtros_aplicados == 1) $('#div_filtros_locales_aplicados').show();
                                            } )   

                                        cuenta_filtro_servicio_item++;
                                    });   


                                   if (data_servicio_item.servicio_items.length == 0)
                                   {
                                        // Oculta este filtro
                                        filtro_servicio_item.hide();
                                        select_servicio_item.hide();
                                   }                                       

                               }
                           });
                        }

                        // Filtro ya aplicado
                        else
                        {
                            // Oculta este filtro
                            filtro_servicio_item.hide();
                            select_servicio_item.hide();

                            // Agrega item seleccionado a filtros aplicados
                            aplicado_servicio_item = $('<div />')
                                .html(filtros_aplicados_servicio_item + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                .appendTo( $('#div_filtros_locales_aplicados_items'))

                                // Funcion para quitar filtro aplicado
                                .on('click', function ()
                                { 
                                    $('.div_cargando').show();

                                    // Reset del filtro actual                                    
                                    var val = '';

                                    column
                                        .search( val ? val : '', true, false )
                                        .draw();                                    

                                    cuenta_filtros_aplicados--;
                                    filtros_aplicados_servicio_item = '';
                                    filtros_aplicados_servicio_item_id = -1;

                                    //Quita el item
                                    this.remove();

                                    // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                    if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                                }); 
                        }  
                    }
                    else
                    {
                        if (filtros_aplicados_servicio_item != '')
                        {
                            cuenta_filtros_aplicados--;
                            filtros_aplicados_servicio_item = '';
                            filtros_aplicados_servicio_item_id = -1;

                            // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                            if (cuenta_filtros_aplicados == 0) $('#div_filtros_locales_aplicados').hide();

                        }                                
                    }                                                
                }                                          
            } );

            $('.div_cargando').hide();
        },

    });
    
    table_locales.on( 'xhr', function () 
    {
        var json = table_locales.ajax.json();
        locales_id = json.locales_id;

        muestra_cantidad_locales();

        if (locales_id.length == 0) locales_id.push(-1);
    } );
}


function muestra_cantidad_locales()
{
    if (locales_id.length > 0)
    {
        // N° de locales encontrados
        if (locales_id.length == 1)
        {
            $("#resultado_cant_locales").html("<strong>" + locales_id.length + " tienda</strong> en tu zona");
            $("#resultado_cant_locales_m").html("<strong>" + locales_id.length + " tienda</strong> en tu zona");
        }
        else
        {
            $("#resultado_cant_locales").html("<strong>" + locales_id.length + " tiendas</strong> en tu zona");
            $("#resultado_cant_locales_m").html("<strong>" + locales_id.length + " tiendas</strong> en tu zona");
        }

        $("#div_sin_locales").hide();
        $("#div_locales").show();
    }
    // No se encontraron locales
    else
    {
        // N° de locales encontrados
        $("#resultado_cant_locales").html("<strong>" + locales_id.length + " tiendas</strong> en tu zona");
        $("#resultado_cant_locales_m").html("<strong>" + locales_id.length + " tiendas</strong> en tu zona");

        $("#div_sin_locales").html('');
        $("#div_sin_locales").append(
            '<div class="div-centrado push-20-t">' + 
            ' <h4 class="push-10 centrado text-center"><i class="fa fa-search push-5-r"></i> No encontramos ninguna tienda</h4>' + 
            ' <h5 class="centrado text-center">Probá ingresando otra dirección o modificá los filtros seleccionados.</h5>' + 
            '</div>');

        $("#div_sin_locales").show();
        $("#div_locales").hide();
    }
}


function mostrar_comentarios_local(id, nombre, avatar)
{
    $("#div_comentarios_local_header").html('');
    $("#div_comentarios_local_list").html('');
    
    // Datos del local
    $("#div_comentarios_local_header").append(

        '<div class="col-sm-12 push-10-t">' + 
            '<div class="text-center push-10">' + 
                '<img src="<?php echo BASE_PATH ?>/assets/img/local/miniaturas/' + avatar + '" width="50">' +
                '<span class="h4 push-10-t push-10-l font-w600">' + nombre + '</span>' + 

            '</div>' +                                                                                                    
        '</div>' + 
        '<div class="col-sm-12 push-10">' + 
            '<div class="text-center push-10">' + 
                '<span class="h4 push-10-t push-10-l">Comentarios de los usuarios</span>' + 
            '</div>' +                                                                                                    
        '</div>');
    
        // Carga comentarios del local (aprobados)
        $.ajax({
          url : "<?php echo BASE_PATH ?>/Local/ajax_comentarios_local/" + id,
          cache: false,
          success: function(data)
          {
            data = jQuery.parseJSON(data);

            comentarios = data.comentarios;

                // Lista de comentarios del local
                for (i = 0; i < comentarios.length; ++i) 
                {

                    htmlPuntaje = '';
                    htmlComentario = '';

                    // Puntaje
                    starsi = comentarios[i].puntaje;
                    starno = 5 - starsi;

                    for (j = 0; j < starsi; ++j) {

                        htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starsi.png" width="20" height="20">';
                    };

                    for (j = 0; j < starno; ++j) {

                        htmlPuntaje += '<img class="push-5-r" src="<?php echo BASE_PATH ?>/assets/img/frontend/starno.png" width="20" height="20">';
                    };  

                    // Comentarios
                    if (comentarios[i].comentario != ''){

                        htmlComentario = '<div class="clearfix"></div>' + 
                            '<div class="pull-left push-10-t"><i class="fa fa-comment-o"></i> ' + 
                                 comentarios[i].comentario + 
                            '</div>';               
                    }

                    
                    $("#div_comentarios_local_list").append(

                    // Cliente
                    '<div class="col-sm-3">' + 
                        '<div class="text-center push-10">' + 
                             '<img src="<?php echo BASE_PATH ?>/assets/img/cliente/miniaturas/' + comentarios[i].cliente_avatar + '" width="50"></a>' + 
                        '</div>' +     
                        '<div class="text-center push-10">' + 
                             comentarios[i].cliente + 
                        '</div>' +                                                                                                            
                    '</div>' +

                    // Comentario
                    '<div class="col-sm-9 push-10-t">' +  
                        '<div class="pull-left">' + 
                             htmlPuntaje + 
                        '</div>' +    
                        '<div class="pull-right">' + 
                            comentarios[i].fecha_comentario + 
                        '</div>' +       
                        htmlComentario + 
                        '<div class="clearfix"></div>' + 
                        '<div class="pull-left push-10-t font-s12">' + 
                             comentarios[i].detalle + '...&nbsp;&nbsp;&nbsp;' + 
                        '</div>' +                                                                                           
                    '</div>'+ 
                    '<div class="clearfix"></div>' + 
                    '<hr />');

                }

            } 
        });    

    $('#modal_comentarios_local').modal('show');
}



function ver_local(id){

    var form_id = "#form_ver_local_" + id;
    $(form_id).submit();
}


// Submit del form de búsqueda de locales 
$('.buscar-local-filtro').submit(function() {

    // Agrega coordenadas a los datos enviados
    $('<input />').attr('type', 'hidden')
      .attr('name', "long")
      .attr('value', '<?php echo $longitud; ?>')
      .prependTo(this); 

    $('<input />').attr('type', 'hidden')
      .attr('name', "lat")
      .attr('value', '<?php echo $latitud; ?>')
      .prependTo(this);

    // Agrega direccion a los datos enviados
    $('<input />').attr('type', 'hidden')
      .attr('name', "loc")
      .attr('value', '<?php echo $localidad; ?>')
      .prependTo(this);    

    $('<input />').attr('type', 'hidden')
      .attr('name', "dir")
      .attr('value', '<?php echo $direccion; ?>')
      .prependTo(this);
});    
</script>