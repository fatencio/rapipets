<!-- PORTADA version DESKTOP -->     
<span class="hidden-xs">
  <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>    
  <div class="portada-wrapper">
      <div style="height: <?php echo $portada_altura; ?>px" class="text-center">

          <img src="../assets/img/portadas/<?php echo $portada; ?>" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">

      </div>
  </div>
</span>

<!-- PORTADA version MOBILE -->     
<span class="visible-xs">
  <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>    
  <div class="portada-wrapper">
      <div style="height: <?php echo $portada_altura_mobile; ?>px" class="text-center">

        <img src="../assets/img/portadas/<?php echo $portada; ?>" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion">

      </div>
  </div>
</span>        

<!-- fin ENCABEZADO -->

<!-- MI CUENTA -->
<section class="content content-full content-boxed">  
    <div class="row">
      <div class="col-sm-2 text-center">
          <img id="local_avatar" src="" height="100"><br />
      </div>

      <div class="col-sm-10">
        <div class="push-40 push-20-t">
            <div class="h3 font-w600 centrado-xs pull-left hidden-xs" id="local_nombre"></div>
            <div class="h3 font-w600 centrado-xs visible-xs" id="local_nombre2"></div>
        </div>  

        <!-- Aviso para destacar local -->
        <?php if ($destacado == '0'){ ?>
          <div class="pull-right hidden-xs push-10-r">
          <a class="btn btn-info" href="javascript:void(0)" title="Aumentar Exposición" onclick="return show_modal_aumentar_exposicion();">Aumentar Exposición</a></div>    
          <div class="clearfix"></div>    
          <div class="centrado-xs visible-xs push-20-t"><a class="btn btn-info" href="javascript:void(0)" title="Aumentar Exposición" onclick="return show_modal_aumentar_exposicion();">Aumentar Exposición</a>
          </div>  

                    
      <?php } else { ?>
          <div class="pull-right hidden-xs push-10-r">
            <div class="h4 font-w600 text-info push-10-r">Tienda Destacada</div>
          </div>
          <div class="clearfix"></div>    
          <div class="centrado-xs visible-xs push-20-t">
            <div class="h4 font-w600 text-info push-10-r">Tienda Destacada</div>
          </div>            
      <?php }  ?>
          
        <!-- FIN Aviso para destacar local -->

        <div class="clearfix"></div>  
        <div class="push-20">
            <div class="h4 push-10-t text-gris hidden-xs">Desde aquí podrás administrar o modificar tus datos, controlar cartillas de vacunas y revisar tus pedidos y turnos.</div>
        </div>  
      </div>
    </div>



  <!-- TABS CON MENU DEL LOCAL -->
  <div class="block push-20-t">
      <ul class="nav nav-tabs nav-justified text-gris text-center" data-toggle="tabs">
          <li class="<?php echo ($tab == 'pedidos') ? 'active ' : 'hidden-xs'; ?> pedidos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/pedidos'"><i class="si si-basket font-tabs push-5-r"></i><br class="hidden-xs " /><span class="font-tabs-texto">Pedidos</span></a>
          </li>

          <li class="<?php echo ($tab == 'turnos') ? 'active ' : 'hidden-xs'; ?> turnos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/turnos'"><i class="si si-calendar font-tabs push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Turnos</span></a>
          </li>  

          <li class="<?php echo ($tab == 'ventas') ? 'active ' : 'hidden-xs'; ?> ventas">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/ventas'"><i class="si si-bar-chart font-tabs push-5-r"></i><br class="hidden-xs " /><span class="font-tabs-texto">Ventas</span></a>
          </li>

          <li class="<?php echo ($tab == 'vacunas') ? 'active ' : 'hidden-xs'; ?> vacunas">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/vacunas'"><i class="si si-notebook font-tabs font-s48 push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Cartilla de Vacunas</span></a>
          </li>   

          <li class="<?php echo ($tab == 'productos') ? 'active ' : 'hidden-xs'; ?> productos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/productos'"><i class="si si-tag font-tabs font-s48 push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Productos</span></a>
          </li>  

          <li class="<?php echo ($tab == 'notificaciones') ? 'active ' : 'hidden-xs'; ?> notificaciones">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/notificaciones'"><i class="si si-bell font-tabs font-s48 push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Notificaciones</span></a>
          </li>   

          <li class="<?php echo ($tab == 'datos') ? 'active ' : 'hidden-xs'; ?> datos">
              <a href="" onclick="window.location.href ='<?php echo BASE_PATH ?>/dashboard/datos'"><i class="si si-user font-tabs font-s48 push-5-r"></i><br class="hidden-xs" /><span class="font-tabs-texto">Datos de la Tienda</span></a>
          </li>            
      </ul>

      <div class="block-content tab-content">

          <!-- PEDIDOS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'pedidos') echo 'active'; ?>" id="tab-pedidos">
              <div class="block-content bg-blue-light">
                <table id="table_pedidos" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Fecha</th>                     
                            <th>Forma Pago</th>                        
                            <th>Cliente</th>                                
                            <th>Total</th>                      
                            <th>Acciones</th>                      
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div> 
          </div>


          <!-- TURNOS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'turnos') echo 'active'; ?>" id="tab-turnos">
            <div class="block bg-gray-lighter">
              <ul class="nav nav-tabs nav-justified turnos-tabs" data-toggle="tabs">
                  <li class="active turnos-activos">
                      <a href="#tab-turnos-activos" onclick="return mostrar_turnos_activos();"><span>Turnos Activos</span></a>
                  </li>
                  <li class="turnos-activos">
                      <a href="#tab-turnos-servicios" onclick="return mostrar_servicios_local();"><span>Servicios Ofrecidos</span></a>
                  </li>
                  <li class="turnos-activos">
                      <a href="#tab-turnos-horarios" onclick="return mostrar_horarios_clinica();"><span>Horarios disponibles para Turnos</span></a>
                  </li>                  
              </ul>
              <div class="block-content tab-content bg-blue-light border-turnos">
                <!-- TURNOS ACTIVOS -->
                <div class="tables-frontend tab-pane active" id="tab-turnos-activos">
                    <div class="block-content">
                      <table id="table_turnos" class="table table-bordered table-striped bg-white" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Turno</th>
                                  <th>Fecha</th>                                     
                                  <th>Servicio</th>                            
                                  <th>Precio</th>                                                
                                  <th>Cliente</th>                        
                                  <th style="width:40px">Acciones</th>          
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                  <th></th>
                                  <th></th>                         
                                  <th></th>
                                  <th></th>                          
                                  <th></th>                          
                                  <th></th>                          
                              </tr>
                          </tfoot>
                          <tbody>
                          </tbody>
                      </table>
                  </div> 
                </div>

                <!-- SERVICIOS -->
                <div class="tab-pane" id="tab-turnos-servicios">
                    <form class="form-horizontal push-30-t push-30" action="#" id="form_servicios">
                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_servicios_error" style="display:none">
                          <small id="servicios_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->       

                      <ul class="list list-simple list-li-clearfix" id="lista_servicios">
                      </ul>
                    </form>   
                    <div class="form-group form-group-login push-50-t push-50">
                          <div class="text-center">
                            <button class="btn btn-lg btn-geotienda-turnos push-10-r" id="btn_actualizar_servicios" onclick="actualizar_servicios()">Guardar Cambios</button>
                        </div>                                                                    
                    </div>
                </div>

                <!-- HORARIOS PARA TURNOS -->
                <div class="block bg-gray-lighter" id="tab-turnos-horarios" style="display: none;">
                  <ul class="nav nav-tabs nav-justified horarios-tabs" data-toggle="tabs">
                      <li class="active horarios-activos">
                          <a href="#tab-horarios-clinica" onclick="return mostrar_horarios_clinica();"><span>Horarios Clínica</span></a>
                      </li>
                      <li class="horarios-activos">
                          <a href="#tab-horarios-peluqueria" onclick="return mostrar_horarios_peluqueria();"><span>Horarios Peluquería</span></a>
                      </li>
                  </ul>
                  <div class="block-content tab-content bg-blue-light border-horarios">
                    <!-- HORARIOS CLINICA -->
                    <div class="tables-frontend tab-pane active" id="tab-horarios-clinica">
                        <div class="block-content" style="display: none;" id="div_horarios_clinica">
                          <form action="#" id="form_horarios_clinica">
                            <!-- Div Error -->
                            <div class="alert alert-danger alert-dismissable" id="div_horarios_error_clinica" style="display:none">
                                <small id="horarios_error_message_clinica"></small>                 
                            </div>
                            <!-- END Div Error -->    

                            <div class="row push--30-t">
                              <div class="col-sm-6">
                                <ul class="list list-simple list-li-clearfix push-20-t">
                                  
                                  <!-- LUNES -->                            
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_lu_clinica' class="pull-left push-5-r" /> Atiende LUNES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="lu_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="lu_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="lu_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="lu_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="lu_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="lu_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="lu_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="lu_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- MARTES -->      
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_ma_clinica' class="pull-left push-5-r" /> Atiende MARTES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="ma_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ma_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ma_h_2_clinica" class="form-control select-horario">
                                         <option value="" selected>...</option>
                                         <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ma_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="ma_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ma_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ma_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ma_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- MIERCOLES -->    
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_mi_clinica' class="pull-left push-5-r" /> Atiende MIERCOLES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="mi_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="mi_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="mi_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="mi_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="mi_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="mi_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="mi_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="mi_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- JUEVES -->                                                                          
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_ju_clinica' class="pull-left push-5-r" /> Atiende JUEVES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="ju_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ju_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ju_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ju_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="ju_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ju_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ju_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ju_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- VIERNES -->  
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_vi_clinica' class="pull-left push-5-r" /> Atiende VIERNES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="vi_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="vi_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="vi_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="vi_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="vi_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="vi_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="vi_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="vi_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- SABADO -->                                                                          
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_sa_clinica' class="pull-left push-5-r" /> Atiende SABADO</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="sa_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="sa_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="sa_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="sa_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="sa_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="sa_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="sa_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="sa_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- DOMINGO -->
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_do_clinica' class="pull-left push-5-r" /> Atiende DOMINGO</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="do_h_1_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="do_m_1_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="do_h_2_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="do_m_2_clinica" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="do_h_3_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="do_m_3_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="do_h_4_clinica" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="do_m_4_clinica" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>
                                </ul>
                              </div>

                              <!-- Calendario -->
                              <div class="col-sm-6 push-30-t">
                                  <div id="div_calendario_clinica"></div>                       
                              </div>

                            </div>
                            <div class="form-group form-group-login push-50-t push-50">
                                  <div class="text-center">
                                    <button class="btn btn-lg btn-geotienda-turnos push-10-r" id="btn_actualizar_horarios_clinica" onclick="actualizar_horarios_clinica()">Guardar Cambios</button>
                                </div>                                                                    
                            </div>
                          </form>                        
                        </div>
                    </div>

                    <!-- HORARIOS PELUQUERIA -->
                    <div class="tables-frontend tab-pane active" id="tab-horarios-peluqueria">
                        <div class="block-content" style="display: none;" id="div_horarios_peluqueria">
                          <form action="#" id="form_horarios_peluqueria">
                            <!-- Div Error -->
                            <div class="alert alert-danger alert-dismissable" id="div_horarios_error_peluqueria" style="display:none">
                                <small id="horarios_error_message_peluqueria"></small>                 
                            </div>
                            <!-- END Div Error -->    

                            <div class="row push--30-t">
                              <div class="col-sm-6">
                                <ul class="list list-simple list-li-clearfix push-20-t">
                                  
                                  <!-- LUNES -->                            
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_lu_peluqueria' class="pull-left push-5-r" /> Atiende LUNES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="lu_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="lu_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="lu_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="lu_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="lu_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="lu_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="lu_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="lu_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- MARTES -->      
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_ma_peluqueria' class="pull-left push-5-r" /> Atiende MARTES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="ma_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ma_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ma_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ma_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="ma_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ma_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ma_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ma_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- MIERCOLES -->    
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_mi_peluqueria' class="pull-left push-5-r" /> Atiende MIERCOLES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="mi_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="mi_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="mi_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="mi_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="mi_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="mi_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="mi_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="mi_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- JUEVES -->                                                                          
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_ju_peluqueria' class="pull-left push-5-r" /> Atiende JUEVES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="ju_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ju_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ju_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ju_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="ju_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="ju_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="ju_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="ju_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- VIERNES -->  
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_vi_peluqueria' class="pull-left push-5-r" /> Atiende VIERNES</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="vi_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="vi_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="vi_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="vi_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="vi_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="vi_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="vi_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="vi_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- SABADO -->                                                                          
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_sa_peluqueria' class="pull-left push-5-r" /> Atiende SABADO</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="sa_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="sa_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="sa_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="sa_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="sa_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="sa_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="sa_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="sa_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>

                                  <!-- DOMINGO -->
                                  <li class="local-box">
                                    <div class="push-5-l">  
                                      <h5 class="push-10"><input type='checkbox' name='abre_do_peluqueria' class="pull-left push-5-r" /> Atiende DOMINGO</h5>
                                      <div class="push-20-l push-20-t">De &nbsp;&nbsp;
                                        <select name="do_h_1_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="do_m_1_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="do_h_2_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="do_m_2_peluqueria" class="form-control select-horario push-10">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.
                                        <br />y de
                                        <select name="do_h_3_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>                                      
                                        </select> :
                                        <select name="do_m_3_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs. &nbsp;&nbsp;&nbsp; a
                                        <select name="do_h_4_peluqueria" class="form-control select-horario">
                                          <option value="" selected>...</option>
                                          <option value="8">8</option>
                                          <option value="9">9</option>
                                          <option value="10">10</option>
                                          <option value="11">11</option>
                                          <option value="12">12</option>
                                          <option value="13">13</option>
                                          <option value="14">14</option>                                      
                                          <option value="15">15</option>
                                          <option value="16">16</option>
                                          <option value="17">17</option>
                                          <option value="18">18</option>
                                          <option value="19">19</option>
                                          <option value="20">20</option>                                      
                                          <option value="21">21</option>
                                        </select> :
                                        <select name="do_m_4_peluqueria" class="form-control select-horario">
                                          <option value="0" selected>00</option>
                                          <option value="30">30</option>
                                        </select> hs.                                                                       
                                      </div>
                                    </div>                                                                                   
                                  </li>
                                </ul>
                              </div>

                              <!-- Calendario -->
                              <div class="col-sm-6 push-30-t">
                                <div id="div_calendario_peluqueria"></div>            
                              </div>

                            </div>
                            <div class="form-group form-group-login push-50-t push-50">
                                  <div class="text-center">
                                    <button class="btn btn-lg btn-geotienda-turnos push-10-r" id="btn_actualizar_horarios_peluqueria" onclick="actualizar_horarios_peluqueria()">Guardar Cambios</button>
                                </div>                                                                    
                            </div>
                          </form>                        
                        </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>


          <!-- VENTAS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'ventas') echo 'active'; ?>" id="tab-ventas">
              <div class="block-content bg-blue-light">
                <table id="table_ventas" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Pedido</th>
                            <th>Fecha</th>                     
                            <th>Forma Pago</th>                        
                            <th>Estado</th>
                            <th>Cliente</th>                                
                            <th>Total</th>                        
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>
            </div> 
          </div>


          <!-- VACUNAS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'vacunas') echo 'active'; ?>" id="tab-vacunas">
            <div class="block-content bg-blue-light">
                <div class="h5 pull-left push-10 vacunas text-center">Aquí podrás ver la cartilla de vacunas de las mascotas de tus clientes e ingresar las nuevas, además de llevar un control más preciso que el papel.</div>
                <table id="table_mascotas" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                    <thead>
                        <tr>                                 
                            <th>Mascota</th>                                     
                            <th>Tipo</th>                                     
                            <th>Dueño</th>                             
                            <th>Próxima Vacuna</th>                             
                            <th>Acciones</th>                                                            
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>              
            </div>  
          </div>    


          <!-- PRODUCTOS -->
          <div class="tables-frontend tab-pane <?php if($tab == 'productos') echo 'active'; ?>" id="tab-productos">
            <div class="block-header block-header-productos">
                <div id="header_productos_local" class="text-center row" style="margin-top: -10px;">
                  <div class="h5 push-5 producto push-5-t col-sm-9" style="padding: 0 20px 10px 20px;">¡Agregá a tu tienda los productos que vendés, elegí presentación e ingresale el precio!</div>  
                  <div class="col-sm-3" style="padding: 0 20px 10px 20px;">
                    <button id="btn_add_producto" class="btn btn-info" onclick="return add_producto();"><i class="glyphicon glyphicon-plus"></i> Agregar Producto</button>
                  </div>
                </div>
                <span id="header_productos_geotienda" style="display: none">   

                <div class="row text-center push-20" style="margin-top: -10px;">
                  <div class="col-sm-4">
                    <div class="h4 push-10 productos push-5-t">Productos Geotienda</div> 
                  </div>

                  <div class="col-sm-8">
                    <button id="btn_volver_add_producto" class="btn btn-info push-20-r push-10" onclick="volver_add_producto()" ><i class="glyphicon glyphicon-arrow-left"></i> Volver</button>

                    <button id="btn_nuevo_producto" class="btn btn-primary push-10-r push-10" onclick="return nuevo_producto_local();"  data-toggle="modal" data-target="#modal_producto_local"><i class="glyphicon glyphicon-plus"></i>&nbsp;&nbsp;Crear Nuevo Producto</button>

                    <button id="btn_agregar_productos" class="btn btn-primary push-10-r push-10" onclick="return agregar_articulo_geotienda_masivo();"><i class="fa fa-list-ul"></i>&nbsp;&nbsp;Agregar Seleccionados</button>
                  </div>
                </div>


                  <div class="clearfix"></div>
                  
                  <div class="block-content text-center">
                    <div class="h5 push-5 push-10-t productos">Busca según el Rubro, Animal, etc... y presioná el botón <strong>+ Agregar a la tienda</strong></div>  
                    <div class="clearfix push-5"></div>                 
                    <div class="h5 push-5 productos">Si no encontrás el producto que buscás lo podés crear en la opción <strong>+ Crear Nuevo Producto</strong></div>    
                    <div class="clearfix"></div>                 
                  </div>
                </span>
            </div>     

            <div id="div_productos">
              
              <div class="block">
                <ul class="nav nav-tabs nav-justified productos-tabs" data-toggle="tabs">
                    <li class="active productos-activos">
                        <a href="#tab-productos-activos"><span style="font-size: 16px">Productos Activos</span></a>
                    </li>
                    <li class="productos-activos">
                        <a href="#tab-productos-inactivos"><span style="font-size: 16px">Productos Inactivos</span></a>
                    </li>                
                </ul>
                <div class="block-content tab-content bg-blue-light border-productos">

                    <!-- PRODUCTOS ACTIVOS -->
                    <div class="tables-frontend tab-pane active" id="tab-productos-activos">

                        <!-- FILTROS --> 
                        <div id="div_filtros_productos" class="col-sm-2">
                          <div class="block filtros-box">
                              <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                                  <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters" data-class="hidden-xs">
                                      <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                                  </button>

                                  <div class="js-search-filters hidden-xs text-center push-10-t">
                                      <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span>  

                                      <div class="div_cargando text-center">
                                         <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                                      </div>   

                                      <!-- FILTROS APLICADOS --> 
                                      <div id="div_filtros_articulos_aplicados" class="filtro-tipo push-10-t push-10" style="display: none;">
                                          Filtros aplicados

                                          <div id="div_filtros_articulos_aplicados_items" class="filtros-aplicados push-10-t push-10"></div>
                                          <hr>
                                      </div>

                                      <!-- FILTROS DISPONIBLES --> 
                                      <div id="div_filtros_articulos_creador" class="push-10 push-10-t"></div>
                                      <div id="div_filtros_articulos_animal" class="push-10"></div>
                                      <div id="div_filtros_articulos_rubro" class="push-10"></div>
                                      <div id="div_filtros_articulos_marca" class="push-10"></div>
                                      <div id="div_filtros_articulos_edad" class="push-10"></div>                            
                                      <div id="div_filtros_articulos_tamanio" class="push-10"></div>                            
                                      <div id="div_filtros_articulos_raza" class="push-10"></div>
                                      <div id="div_filtros_articulos_presentacion" class="push-10"></div>                    
                                      <div id="div_filtros_articulos_medicados" class="push-10"></div>                    
                                  </div>
                              </div>
                          </div>
                        </div>   

                        <!-- DATATABLE PRODUCTOS ACTIVOS --> 
                        <div class="div_table_productos col-sm-10">
                          <div class="block-content detalle-venta table-responsive">
                            <span class="visible-xs font-s13 text-muted push-10"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>

                            <table id="table_productos" class="table table-bordered table-striped bg-white" cellspacing="0" width="100%">
                              <thead>
                                  <tr>
                                      <th>Tipo</th>
                                      <th>Rubro</th>
                                      <th>Animal</th>                        
                                      <th>Marca</th>
                                      <th>Nombre</th>
                                      <th>Código</th>
                                      <th>Imagen</th>                        
                                      <th style="width:120px;">Acción</th>
                                      <th>Filtros</th>
                                      <th>Filtros</th>
                                      <th>Filtros</th>
                                      <th>Filtros</th>
                                  </tr>
                              </thead>
                              <tbody>
                              </tbody>
                            </table>     
                          </div>                 
                        </div>  
                    </div>

                    <!-- PRODUCTOS INACTIVOS -->
                    <div class="tables-frontend tab-pane" id="tab-productos-inactivos">

                        <!-- FILTROS --> 
                        <div id="div_filtros_productos_inactivos" class="col-sm-2">
                          <div class="block filtros-box">
                              <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                                  <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters-inactivos" data-class="hidden-xs">
                                      <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                                  </button>

                                  <div class="js-search-filters-inactivos hidden-xs text-center push-10-t">
                                      <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span>  

                                      <div class="div_cargando text-center">
                                         <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                                      </div>   

                                      <!-- FILTROS APLICADOS --> 
                                      <div id="div_filtros_articulos_aplicados_inactivos" class="filtro-tipo push-10-t push-10" style="display: none;">
                                          Filtros aplicados

                                          <div id="div_filtros_articulos_aplicados_items_inactivos" class="filtros-aplicados push-10-t push-10"></div>
                                          <hr>
                                      </div>

                                      <!-- FILTROS DISPONIBLES --> 
                                      <div id="div_filtros_articulos_creador_inactivos" class="push-10 push-10-t"></div>
                                      <div id="div_filtros_articulos_animal_inactivos" class="push-10"></div>
                                      <div id="div_filtros_articulos_rubro_inactivos" class="push-10"></div>
                                      <div id="div_filtros_articulos_marca_inactivos" class="push-10"></div>
                                      <div id="div_filtros_articulos_edad_inactivos" class="push-10"></div>                            
                                      <div id="div_filtros_articulos_tamanio_inactivos" class="push-10"></div>                            
                                      <div id="div_filtros_articulos_raza_inactivos" class="push-10"></div>
                                      <div id="div_filtros_articulos_presentacion_inactivos" class="push-10"></div>                    
                                      <div id="div_filtros_articulos_medicados_inactivos" class="push-10"></div>                    
                                  </div>
                              </div>
                          </div>
                        </div>   

                          <div class="div_table_productos col-sm-10">
                            <div class="block-content detalle-venta table-responsive">
                              <span class="visible-xs font-s13 text-muted push-10"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                          
                              <table id="table_productos_inactivos" class="table table-bordered table-striped bg-white" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Tipo</th>
                                        <th>Rubro</th>
                                        <th>Animal</th>                        
                                        <th>Marca</th>
                                        <th>Nombre</th>
                                        <th>Código</th>
                                        <th>Imagen</th>                        
                                        <th style="width:120px;">Acción</th>
                                        <th>Filtros</th>
                                        <th>Filtros</th>
                                        <th>Filtros</th>
                                        <th>Filtros</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>   
                            </div>
                        </div>                     
                    </div>
                </div>
              </div>
            </div> 


            <!-- PRODUCTOS GEOTIENDA -->
            <div id="div_agregar_producto" style="display: none">

              <div class="bg-blue-light">

                <!-- FILTROS --> 
                <div id="div_filtros_productos_geotienda" class="col-sm-2">
                  <div class="block filtros-box">
                      <div class="block-content form-horizontal block-content-geotienda filtros-box-content" style="position: relative;">

                          <button class="btn btn-sm btn-block btn-info visible-xs push-10" type="button" data-toggle="class-toggle" data-target=".js-search-filters-geotienda" data-class="hidden-xs">
                              <i class="fa fa-filter push-5-r"></i> ¡Buscá según tus necesidades!
                          </button>

                          <div class="js-search-filters-geotienda hidden-xs text-center push-10-t">
                              <span class="hidden-xs text-default push-10-t push-10 font-16"><strong>¡Buscá según tus necesidades!</strong></span>  

                              <div class="div_cargando text-center">
                                 <i class="fa fa-2x fa-cog fa-spin text-white" style="min-height: 20px;"></i>
                              </div>   

                              <!-- FILTROS APLICADOS --> 
                              <div id="div_filtros_articulos_aplicados_geotienda" class="filtro-tipo push-10-t push-10" style="display: none;">
                                  Filtros aplicados

                                  <div id="div_filtros_articulos_aplicados_items_geotienda" class="filtros-aplicados push-10-t push-10"></div>
                                  <hr>
                              </div>

                              <!-- FILTROS DISPONIBLES --> 
                              <div id="div_filtros_articulos_animal_geotienda" class="push-10 push-10-t"></div>
                              <div id="div_filtros_articulos_rubro_geotienda" class="push-10"></div>
                              <div id="div_filtros_articulos_marca_geotienda" class="push-10"></div>
                              <div id="div_filtros_articulos_edad_geotienda" class="push-10"></div>                            
                              <div id="div_filtros_articulos_tamanio_geotienda" class="push-10"></div>                            
                              <div id="div_filtros_articulos_raza_geotienda" class="push-10"></div>
                              <div id="div_filtros_articulos_presentacion_geotienda" class="push-10"></div>                    
                              <div id="div_filtros_articulos_medicados_geotienda" class="push-10"></div>                    
                          </div>
                      </div>
                  </div>
                </div>            

                <div class="div_cargando text-center" style="display: none;">
                  <!-- <i class="fa fa-2x fa-cog fa-spin text-warning"></i> --> 
                  <span class="cargando">Cargando...</span>
                </div>

                <div class="block-content detalle-venta table-responsive col-sm-10">
                  <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>
                  <table id="table_articulos_geotienda" class="table table-bordered table-striped js-dataTable-full bg-white"  cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th style="width:4px; padding-left:10px"><input type="checkbox" name="selectAll" id="selectAll" value=""></th>
                              <th>Rubro</th>
                              <th>Animal</th>                        
                              <th>Marca</th>
                              <th>Nombre</th>
                              <th>Código</th>
                              <th>Imagen</th>                        
                              <th>Acción</th>
                              <th>Filtros</th>
                              <th>Filtros</th>
                              <th>Filtros</th>
                              <th>Filtros</th>                              
                          </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                </div>
              </div>                
            </div>                       
          </div>
                    

          <!-- NOTIFICACIONES -->
          <div class="tables-frontend tab-pane <?php if($tab == 'notificaciones') echo 'active'; ?>" id="tab-notificaciones">
              <div class="block-content bg-blue-light">
                <table id="table_notificaciones" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Tipo</th>                         
                            <th>Notificación</th>
                            <th style="width:180px;">Detalles</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>                         
                            <th></th>
                            <th style="width:180px;"></th>
                        </tr>
                    </tfoot>
                    <tbody>
                    </tbody>
                </table>              
              </div>
          </div>  

          <!-- DATOS DEL LOCAL -->
          <div class="tab-pane <?php if($tab == 'datos') echo 'active'; ?>" id="tab-datos">
            
            <!-- FORM DATOS LOCAL -->
            <form class="form-horizontal" action="#" id="form_local">     


              <!-- TABS INFORMACION / MEDIOS DE MPAGO -->          
              <div class="block bg-gray-lighter">
                <ul class="nav nav-tabs nav-justified datos-tabs" data-toggle="tabs">
                    <li class="active datos-activos">
                        <a href="#tab-datos-informacion" ><span style="font-size: 16px">Información</span></a>
                    </li>
                    <li class=" datos-activos">
                        <a href="#tab-datos-mediospago" onclick="mostrar_medios_pago_local('true');" style="font-size: 16px"><span>Medios de Pago</span></a>
                    </li>                  
                </ul>
                <div class="block-content tab-content bg-blue-light border-datos">
                    <!-- TAB INFORMACION -->
                    <div class="tables-frontend tab-pane active push-20-t" id="tab-datos-informacion" style="margin: 20px;">

                      <input type="hidden" value="" name="local-texto-banner-hidden" id="local-texto-banner-hidden"/>    
                      <input type="hidden" value="" name="local-urgencias" id="local-urgencias"/>    

                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_local_error" style="display:none">
                          <small id="local_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->                              
                                      
                      <!-- DATOS DEL LOCAL -->               
                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label">Nombre de la Tienda</label>
                          <div class="col-sm-7 input-obligatorio">
                              <input class="form-control" type="text" id="local-nombre" name="local-nombre" placeholder="Nombre de la Tienda" value=""  required>
                          </div>
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label" >Titular</label>
                          <div class="col-sm-4 input-obligatorio push-5">
                              <input class="form-control" type="text" id="local-titular-nombre" name="local-titular-nombre" placeholder="Nombre del Titular" value=""  required>
                          </div>
                          <div class="col-sm-3 input-obligatorio">
                              <input class="form-control" type="text" id="local-titular-apellido" name="local-titular-apellido" placeholder="Apellido del Titular" value=""  required>
                          </div>                        
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label" >Teléfono</label>
                          <div class="col-sm-4 input-obligatorio">
                              <input class="form-control push-5" type="text" id="local-telefono" name="local-telefono" placeholder="Teléfono" value=""  required>
                          </div>
                          <div class="col-sm-3">
                              <input class="form-control" type="text" id="local-telefono2" name="local-telefono2" placeholder="Teléfono alternativo" value="">
                          </div>                          
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label">E-mail</label>
                          <div class="col-sm-4 input-obligatorio push-5">
                              <input class="form-control" type="text" id="local-email" name="local-email" placeholder="E-mail" value=""  required>
                          </div>
                          <div class="col-sm-3">
                              <input class="form-control" type="text" id="local-email2" name="local-email2" placeholder="E-mail alternativo" value="">
                          </div>                        
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label">Horarios de Atención</label>
                          <div class="col-sm-7 input-obligatorio">
                              <input class="form-control" type="text" id="local-horario" name="local-horario" placeholder="Lunes a Viernes" value=""  required>
                          </div>
                      </div>
                         
                      <div class="form-group form-group-login push--5-t">
                          <label class="col-sm-3 control-label"></label>
                          <div class="col-sm-4 push-5">
                              <input class="form-control" type="text" id="local-horario-sabados" name="local-horario-sabados" placeholder="Sábado" value="">
                          </div>
                          <div class="col-sm-3">
                              <input class="form-control" type="text" id="local-horario-domingos" name="local-horario-domingos" placeholder="Domingo" value="">
                          </div>                      
                      </div>
                           
                      <div class="form-group form-group-login push-30-t">
                          <label class="col-sm-3 control-label"></label>
                          <div class="col-sm-7">
                                <input type='hidden' value='1' name='envio-domicilio' id='envio-domicilio'>
                                <label class="css-input css-checkbox css-checkbox-flat">
                                    <input type="checkbox" name ="switch_envio_domicilio" id="switch_envio_domicilio" checked="checked"><span></span> ¿Hace envíos a domicilio?
                                </label>     
                          </div>
                      </div>

                      <div class="form-group form-group-login envio_domicilio">
                          <label class="col-sm-3 control-label">Costo de Envío</label>
                          <div class="col-sm-7 input-obligatorio">
                              <input class="form-control" type="text" id="local-envio" name="local-envio" placeholder="Costo de envío" value=""  required>
                          </div>
                      </div>

                      <div class="form-group form-group-login envio_domicilio">
                          <label class="col-sm-3 control-label">Zona de Entrega</label>
                          <div class="col-sm-7 input-obligatorio">
                              <input class="form-control" type="text" id="local-zona-entrega" name="local-zona-entrega" placeholder="Zona de Entrega" value=""  required>
                          </div>
                      </div>

                      <div class="form-group form-group-login envio_domicilio">
                          <label class="col-sm-3 control-label">Días de Entrega</label>
                          <div class="col-sm-7 input-obligatorio">
                              <input class="form-control" type="text" id="local-dias-entrega" name="local-dias-entrega" placeholder="Días de Entrega" value=""  required>
                          </div>
                      </div>

                      <div class="form-group form-group-login urgencias push-30-t">
                          <label class="col-sm-3 control-label" >Teléfono Urgencias</label>
                          <div class="col-sm-3 input-obligatorio">
                              <input class="form-control push-5" type="text" id="local-telefono-urgencias" name="local-telefono-urgencias" placeholder="Teléfono" value=""  required>
                          </div>
                          <div class="col-sm-4" style="text-align: right;">
                              <input type='hidden' value='0' name='local-urgencias-consultorio' id='local-urgencias-consultorio'>
                              <label class="css-input css-checkbox css-checkbox-flat">
                                  <input type="checkbox" name ="chk-local-urgencias-consultorio" id="chk-local-urgencias-consultorio" ><span></span> Atención en Consultorio
                              </label>  
                              <input type='hidden' value='0' name='local-urgencias-domicilio' id='local-urgencias-domicilio'>
                              <label class="css-input css-checkbox css-checkbox-flat push-10-l">
                                  <input type="checkbox" name ="chk-local-urgencias-domicilio" id="chk-local-urgencias-domicilio" ><span></span> Visita a Domicilio
                              </label>                              
                          </div>                          
                      </div>                    

                      <div class="form-group form-group-login push-30-t">
                          <label class="col-sm-3 control-label"></label>
                          <label class="col-sm-7 control-label">
                              <span class="red">*</span> Datos obligatorios
                          </label>
                      </div>                    
                      <!-- fin DATOS DEL LOCAL -->     


                      <!-- BANNER -->     
                      <div class="form-group form-group-login push-40-t">
                          <label class="col-sm-3 control-label"></label>
                          <label class="col-sm-7">CONFIGURAR BANNER</label>
                      </div>

                      <div class="form-group form-group-login" id="div_texto_banner">
                          <label class="col-sm-3 control-label">Texto</label>
                          <div class="col-sm-7">
                              <input class="form-control" type="text" id="local-texto-banner" name="local-texto-banner" placeholder="Texto Banner" value="">
                          </div>
                      </div>

                      <div class="form-group form-group-login" id="div_colores_banner">
                          <label class="col-sm-3 control-label">Color</label>
                          <div class="col-sm-9">
                              fondo:&nbsp;       
                              <input class="form-control" type="text" id="local-fondo-banner" name="local-fondo-banner" value=""> 
                              &nbsp;&nbsp;&nbsp;texto:&nbsp;          
                              <input class="form-control" type="text" id="local-color-banner" name="local-color-banner" value="">                    
                          </div>                     
                      </div>                    
                      
                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label">Vista Previa</label>
                          <div class="col-sm-7 vista-previa-banner text-center" id="div_vista_previa_banner">
                          </div>
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label"></label>
                          <div class="col-sm-7 alert alert-info alert-dismissable" id="div_aviso_banner" style="display: none">
                              <h3 class="font-w300 push-15">Atención</h3>
                              <p>Este banner no se verá en el local ya que estás adherido a una campaña de GeoTienda. Si no querés seguir con la campaña por favor mandanos un mail a <strong>soporte@geotienda.com</strong></p>
                          </div>
                      </div>                    
                      <!-- fin BANNER -->                       

                      <!-- MODIFICAR CONTRASEÑA -->           
                      <div class="form-group form-group-login push-80-t">
                          <label class="col-sm-3 control-label"></label>
                          <label class="col-sm-7">MODIFICAR CONTRASEÑA</label>
                      </div>

                      <div class="form-group form-group-login">
                          <label class="col-sm-3 control-label"></label>
                          <div class="col-sm-4 push-5">
                              <input class="form-control" type="password" id="local-password" name="local-password" placeholder="Nueva Contraseña">
                          </div>
                          <div class="col-sm-3">
                              <input class="form-control" type="password" id="local-password2" name="local-password2" placeholder="Repetí nueva Contraseña">
                          </div>                        
                      </div>
                      <!-- fin MODIFICAR CONTRASEÑA -->    
                    </div>

                    <!-- TAB MEDIOS DE PAGO -->
                    <div class="tab-pane active" id="tab-datos-mediospago">
                      <ul class="list list-li-clearfix push-20" id="lista_medios_pago"></ul>
                    </div>
                  </div>
                </form>   
                <div class="form-group form-group-login push-50-t push-50">
                      <div class="text-center">
                        <button class="btn btn-lg btn-geotienda-flat push-10-r" id="btn_actualizar_local" onclick="actualizar_local()">Guardar Cambios</button>
                    </div>                                                                    
                </div>
             </div>   
            <!-- fin FORM DATOS LOCAL -->
          </div>                     
      </div>
  </div>
  <!-- fin TABS CON MENU DEL LOCAL -->
</section>


<!-- MODAL INFORMACION VENTA-->
<div class="modal fade" id="modal_detalle_venta" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content modal-grande">
            <div class="modal-header back-naranja text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-title-venta-nro"> Venta</h3>
                </div>
            </div>

            <div class="modal-body form">
              <div class="block-content ">
                  <!-- ENCABEZADO VENTA -->

                  <!-- Datos Cliente -->
                  <div class="col-sm-2 detalle-venta centrado">
                      <div class="text-center push-10" id="venta_cliente_avatar"></div>                            
                  </div>

                  <div class="col-sm-5 detalle-venta">               
                      <div class="h5 push-5">Cliente: <b id="venta_cliente_nombre"></b></div>   
                      <div class="h5 push-5" style="display:none" id="div_venta_cliente_telefono">Teléfono: <b id="venta_cliente_telefono"></b></div>
                      <div class="h5 push-5">Fecha del pedido: <b id="venta_fecha"></b></div>       
                  </div>

                  <div class="col-sm-5 detalle-venta">
                      <div class="h5 push-5">Forma de pago: <b id="venta_condicion_venta"></b></div>
                      <div class="h5 push-5">Estado: <b id="venta_estado"></b></div>              
                  </div>                    

                  <div class="clearfix push-10"></div>
              </div>

              <!-- DETALLES VENTA -->
              <div class="block-content detalle-venta table-responsive">
                <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>
                  <table id="table_detalle_venta" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                      <thead>
                          <tr>
                              <th>Artículo</th>
                              <th>Presentación</th>
                              <th>Cantidad</th>
                              <th  style="min-width:100px;">Precio U.</th>
                              <th>Subtotal</th>
                          </tr>
                      </thead>
                      <tfoot>
                          <tr>
                              <th class="text-right" colspan="4">Total</th>
                              <th class="text-right"></th>
                          </tr>
                      </tfoot>                                
                      <tbody>
                      </tbody>
                  </table>
              </div>   

              <!-- Datos adicionales -->
              <div class="block">
                  <div class="block-content detalle-venta">
                      <div class="h4 push-5">Datos adicionales</div>                                   
                          <div class="h5 espacio-8">Envío: <b id="venta_tipo_envio"></b></div>
                          <div class="h5 espacio-8" id="div_venta_cliente_direccion">Dirección: <b id="venta_cliente_direccion"></b></div>
                          <div class="h5 espacio-8" id="div_venta_cliente_ciudad">Ciudad: <b id="venta_cliente_ciudad"></b></div>
                  </div>

                  <!-- Pedido cancelado -->
                  <div class="block-content detalle-venta" id="div_venta_cancelado">
                        <div class="h5 push-5">Cancelado por el <b id="venta_cancelado_por"></b></div>  
                        <div class="h5 push-5">Motivo: <b id="venta_cancelado_motivo"></b></div>              
                  </div>                      
              </div>

              <div class="clearfix"></div>

              <!-- Valoración -->
              <div class="block" id="venta_valoracion_bloque">
                  <div class="block-content detalle-venta">
                      <div class="h4 push-5">Valoración</div>
                      <div class="h5 espacio-8" id="venta_puntaje"></div>
                      <div class="h5 espacio-8">Comentario: <b id="venta_valoracion"></b></div>
                  </div>
              </div>

              <!-- COMENTARIOS -->
              <div class="block-content detalle-venta">

                  <!-- Nuevo mensaje -->
                  <div class="h4 push-5 text-center font-w600 push-20 text-naranja" id="venta_titulo_comentarios"></div>
                  <span id="bloque_add_comentario_venta">
                    <h5 class="push-10-t push-5" id="venta_titulo_nuevo_comentario">Enviale un mensaje a</h5>  
                    <h6 class="push-5">(por favor no uses lenguaje grosero)</h6>   
                    <form action="#" id="form_comentario_venta" class="form-horizontal">
                        <input type="hidden" value="" name="hd_venta_id" id="hd_venta_id"/> 
                        <input type="hidden" value="" name="hd_venta_local_id" id="hd_venta_local_id"/> 
                        <div class="form-body">
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea name="venta_comentario" id="venta_comentario" placeholder="Ingresá mensaje" class="form-control" rows="3"></textarea>
                                </div>                                  
                            </div>
                        </div>                    
                    </form>  
                  </span>         
                  <div class="pull-right">
                    <button type="button" class="btn btn-primary back-naranja" id="btnSaveComentarioVenta" onclick="save_comentario_venta()" >Aceptar</button>
                  </div>                              
                  <!-- fin Nuevo mensaje --> 

                  <div class="clearfix"></div>

                   <!-- Lista de mensajes  -->
                <h5 class="push-20-t push-5">Historial de mensajes</h5>
              </div>

              <div class="block-content detalle-venta table-responsive">
                <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                    
                    <table id="table_comentarios_venta" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:100px">Fecha</th>
                                <th>Cliente / Tienda</th>
                                <th>Comentario</th>
                            </tr>
                        </thead>                                
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- FIN Lista de mensajes  -->
            </div>

            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL INFORMACION VENTA -->   


<!-- MODAL INFORMACION TURNO  -->
<div class="modal fade" id="modal_detalle_turno" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content modal-grande">
            <div class="modal-header bg-success text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-title-turno-nro"> Turno</h3>
                </div>
            </div>

            <div class="modal-body form">
              <div class="block-content ">

                  <!-- Servicio y Fecha -->
                  <div class="detalle-turno">
                      <div class="h4 push-5" style="margin-bottom:3px"><i class="fa fa-calendar"></i> <b id="turno_fecha"></b></div>
                      
                  </div>

                  <div class="row">
                    <!-- Datos Local -->
                    <div class="col-sm-2 detalle-venta centrado">
                        <div class="text-center pull-left push-10-r push-10" id="turno_cliente_avatar"></div>           
                    </div>

                    <div class="col-sm-9 detalle-venta centrado push-10-t">
                        <div class="h4 push-5">Cliente: <b id="turno_cliente"></b></div>   
                        <div class="h5 push-5">Servicio: <b id="turno_servicio"></b></div>              
                    </div>
                  </div>

                  <!-- TURNO CANCELADO -->
                  <div class="row">
                    <div class="col-sm-9 detalle-venta centrado" style="display:none" id="div_turno_cancelado">
                        <div class="h4 push-5">Cancelado por el <b id="turno_cancelado_por"></b></div>   
                        <div class="h5 push-5">Motivo: <b id="turno_cancelado_motivo"></b></div>              
                    </div>
                  </div>            
                  <!-- fin TURNO CANCELADO -->

                  <div class="clearfix"></div>

                  <!-- Valoración -->
                  <div class="block" id="turno_valoracion_bloque">
                      <div class="detalle-venta">
                          <div class="h4 push-5">Valoración</div>
                              <div class="h5 espacio-8" id="turno_puntaje"></div>
                              <div class="h5 espacio-8">Comentario: <b id="turno_valoracion"></b></div>
                      </div>
                  </div>
              </div>

              <!-- COMENTARIOS -->
              <div class="block-content detalle-turno">

                  <!-- Nuevo mensaje -->
                  <div class="h4 push-5 text-center font-w600 push-20 text-naranja" id="turno_titulo_comentarios"></div>
                  <span id="bloque_add_comentario_turno">
                    <h5 class="push-10-t push-5" id="turno_titulo_nuevo_comentario">Enviale un mensaje a</h5>  
                    <h6 class="push-5">(por favor no uses lenguaje grosero)</h6>   
                    <form action="#" id="form_comentario_turno" class="form-horizontal">
                        <input type="hidden" value="" name="hd_turno_id" id="hd_turno_id"/> 
                        <input type="hidden" value="" name="hd_turno_local_id" id="hd_turno_local_id"/> 
                        <div class="form-body">
                            
                            <div class="form-group">
                                <div class="col-md-12">
                                    <textarea name="turno_comentario" id="turno_comentario" placeholder="Ingresá mensaje" class="form-control" rows="3"></textarea>
                                </div>                                  
                            </div>
                        </div>                    
                    </form>  
                  </span>       
                  <div class="pull-right">
                    <button type="button" class="btn btn-primary back-naranja" id="btnSaveComentarioTurno" onclick="save_comentario_turno()" >Aceptar</button>
                  </div>                                
                  <!-- fin Nuevo mensaje --> 

                  <div class="clearfix"></div>
                  
                   <!-- Lista de mensajes  -->
                  <h5 class="push-20-t push-5">Historial de mensajes</h5>
              </div>

              <div class="block-content detalle-venta table-responsive">
                <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                    
                    <table id="table_comentarios_turno" class="table table-bordered table-striped"  cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th style="width:100px">Fecha</th>
                                <th>Cliente / Tienda</th>
                                <th>Comentario</th>
                            </tr>
                        </thead>                                
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- FIN Lista de mensajes  -->
            </div>

            <div class="modal-footer">

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL INFORMACION TURNO -->     


<!-- MODAL INFORMACION VACUNA -->
<div class="modal fade" id="modal_detalle_vacuna" role="dialog">
    <div class="modal-dialog" >
        <div class="modal-content modal-grande">
            <div class="modal-header back-vacunas text-white">
                <div class="text-left">
                    <h3>Detalles vacuna</h3>
                </div>
            </div>
            <div class="block-content ">

                <div class="detalle-vacuna push-10-l push-10-t">
                    <div class="h5 push-5">Fecha aplicación: <b id="vacuna_fecha"></b></div>                
                    <div class="h5 push-5">Vacuna: <b id="vacuna_nombre"></b></div>                    
                    <div class="h5 push-5">Peso: <b id="vacuna_peso"></b></div>                              
                    <div class="h5 push-5">Aplicada: <b id="vacuna_aplicada"></b></div>
                    <div class="h5 push-5">Tienda: <b id="vacuna_local"></b></div>                    
                    <div class="h5 push-5">Veterinario: <b id="vacuna_veterinario"></b></div>                      
                    <div class="h5 push-5">Próxima vacuna: <b id="vacuna_proxima_vacuna"></b></div>  
                    <div class="h5 espacio-8">Observaciones: <b id="vacuna_observaciones"></b></div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="form-group form-group-login">
                    <div class="text-center">
                        <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>
                    </div>    
                </div> 
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- fin MODAL INFORMACION VACUNA -->


<!-- MODAL PROXIMA VACUNA LOCAL -->
<div class="modal fade" id="modal_proxima_vacuna" role="dialog">
    <div class="modal-dialog" >
        <div class="modal-content modal-grande">
            <div class="modal-header back-vacunas text-white">
                <div class="text-left">
                    <h3>Vacuna agendada</h3>
                </div>
            </div>
            <div class="block-content ">

                <!-- Datos del cliente -->
                <div class="col-sm-2 detalle-venta centrado">
                    <div class="text-center pull-left push-10-r push-10" id="proxima_vacuna_cliente_avatar"></div>    
                </div>

                <div class="col-sm-8 detalle-venta centrado">
                    <div class="h4 push-5">Cliente: <b id="proxima_vacuna_cliente_nombre"></b></div>
                </div>

                <div class="clearfix"></div>

                <!-- Nombre y Fecha -->
                <div class="detalle-vacuna push-10-l push-10-t">
                    <div class="h5 push-5">Fecha agendada: <b id="proxima_vacuna_fecha"></b></div>
                    <div class="h5 push-5">Mascota: <b id="proxima_vacuna_mascota"></b></div>   
                    <div class="h5 push-5">Vacuna: <b id="proxima_vacuna_nombre"></b></div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="form-group form-group-login">
                    <div class="text-center">
                      <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>
                    </div>    
                </div>  
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- fin MODAL PROXIMA VACUNA LOCAL -->


<!-- MODAL NUEVA / EDITAR VACUNA -->
<div class="modal fade" id="modal_add_edit_vacuna" role="dialog" data-focus-on="input:first">
    <div class="modal-dialog ">
        <div class="modal-content modal-grande">
            <div class="modal-header back-vacunas text-white">
                <h3 class="modal-title-add-edit-vacuna">Nueva Vacuna</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_add_edit_vacuna" class="form-horizontal">
                    <!-- Div Error -->
                    <div class="alert alert-danger alert-dismissable push-10-l push-10-r" id="div_add_edit_vacuna_error" style="display:none">
                        <small id="add_edit_vacuna_error_message"></small>                 
                    </div>
                    <!-- END Div Error -->  

                    <input type="hidden" value="" name="vacuna_mascota_id" id="vacuna_mascota_id"/> 
                    <input type="hidden" value="" name="id_vacuna" id="id_vacuna"/> 
                    <div class="form-body">

                        <!-- FECHA  --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Fecha vacunación</label>
                            <div class="col-md-6 input-obligatorio">
                              <input class="js-datepicker form-control" type="text" id="fecha_vacuna" name="fecha_vacuna" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            </div>
                        </div>

                        <!-- NOMBRE --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Nombre vacuna</label>
                            <div class="col-md-6 input-obligatorio">
                                <input id="nombre_vacuna" name="nombre_vacuna" placeholder="" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- PESO --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Peso del animal</label>
                            <div class="col-md-6">
                                <input id="peso_vacuna" name="peso_vacuna" placeholder="" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- APLICADA --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Vacuna aplicada</label>
                            <label class="push-10-l push-10-r css-input css-radio css-radio-warning push-10-r">
                                <input name="vacuna_aplicada" type="radio" id="vacuna_aplicada_si" value="si"><span></span> Sí
                            </label>
                            <label class="push-10-r css-input css-radio css-radio-warning">
                                <input name="vacuna_aplicada" type="radio" id="vacuna_aplicada_no" value="no"><span></span> No
                            </label>
                            <label class="css-input css-radio css-radio-warning">
                                <input name="vacuna_aplicada" type="radio" id="vacuna_aplicada_null" value="" checked><span></span> No definido
                            </label>                            
                        </div>

                        <!-- VETERINARIO --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Veterinario</label>
                            <div class="col-md-6 input-obligatorio">
                                <input id="veterinario_vacuna" name="veterinario_vacuna" placeholder="" class="form-control" type="text">
                            </div>
                        </div>

                        <!-- PROXIMA VACUNA  --> 
                        <div class="form-group">
                            <label class="control-label col-md-4">Próxima Vacuna</label>
                            <div class="col-md-6">
                              <input class="js-datepicker form-control" type="text" id="fecha_proxima_vacuna" name="fecha_proxima_vacuna" data-date-format="dd/mm/yyyy" placeholder="dd/mm/yyyy">
                            </div>
                        </div>

                      <!-- OBSERVACIONES -->
                      <div class="form-group">
                          <label class="control-label col-md-4">Observaciones</label>
                          <div class="col-md-6">
                              <textarea class="form-control" name="observaciones_vacuna" id="observaciones_vacuna" rows="4"></textarea>
                          </div>
                      </div>                          

                      <div class="form-group push-20-t">
                          <label class="col-md-4 control-label"></label>
                          <div class="col-md-6">
                              <span class="red">*</span> Datos obligatorios
                          </div>
                      </div>                                               
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btn_add_edit_vacuna" onclick="save_vacuna()" class="btn btn-vacunas push-10-r">Guardar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL NUEVA VACUNA -->


<!-- MODAL CARTILLA MASCOTA -->
<div class="modal fade" id="modal_cartilla_mascota" role="dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content modal-grande">
            <div class="modal-header back-vacunas text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-center">
                    <h3 class="modal-title-mascota"></h3>
                </div>
            </div>
            <div class="modal-body form">                    
              <div class="block-content ">

                  <!-- Datos mascota -->
                  <div class="col-sm-12">  
                    <div class="h4 push-10-t push-15">Dueño: <b id="mascota_duenio"></b></div>                
                  </div>

                  <div class="col-sm-3 centrado">
                      <div class="text-center push-10" id="mascota_foto"></div>                            
                  </div>

                  <div class="col-sm-4 push-30-t">               
                      <div class="push-5">Nombre: <b id="mascota_nombre"></b></div>
                      <div class=" push-5">Tipo: <b id="mascota_tipo_sexo"></b></div>     
                      <div class=" push-5">Nacimiento: <b id="mascota_nacimiento"></b></div>                             
                  </div>

                  <div class="col-sm-5 push-30-t">
                      <div class=" push-5">Raza: <b id="mascota_raza"></b></div>
                      <div class=" push-5">Pelaje: <b id="mascota_pelaje"></b></div>
                      <div class=" push-5">Talla: <b id="mascota_talla"></b></div>            
                  </div>                    

                  <div class="clearfix"></div>
              </div>

                <!-- Vacunas -->
                <div class="block-content detalle-venta">
                  <div class="h4 pull-left push-10-t push-10">Vacunas</div> 
                  <div class="h4 pull-right push-10"><button class="btn btn-vacunas" data-toggle="modal" href="#modal_add_edit_vacuna" onclick="add_vacuna()">Nueva vacuna</button></div> 

                  <div class="clearfix"></div>
                  <div class="h5 push-10-t text-success push-5">Próxima Vacuna: <span id="mascota_proxima_vacuna"></span></div>
                </div>

                <div class="block-content detalle-venta table-responsive">
                    <span class="visible-xs font-s13 text-muted"><i class="fa fa-arrows-h"></i>&nbsp;&nbsp;desliza para ver más datos</span>                    

                    <table id="table_vacunas" class="table table-bordered table-striped bg-white"  cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Fecha</th>
                                <th>Vacuna</th>
                                <th>Tienda</th>
                                <th>Aplicada</th>
                                <th>Próxima Vacuna</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>                            
                        <tbody>
                        </tbody>
                    </table>
                </div>   
            </div>              
            <div class="modal-footer">
              <button type="button" class="btn btn-vacunas" data-dismiss="modal">Aceptar</button>              
            </div>
        </div>
    </div>
</div>
<!-- fin MODAL CARTILLA MASCOTA --> 


<!-- MODAL AVISAR VENTA o TURNO (CONCERTADO / NO CONCRETADO) -->
<div class="modal fade" id="modal_avisar" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-grande">
            <div class="modal-header bg-info text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-title-pedido-nro"> Turno / Pedido</h3>
                </div>
            </div>

              <div class="block-content"> 
                  <form class="form-horizontal action="#" id="form_notificar">
                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_notificar_error" style="display:none">
                          <small id="notificar_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->     

                      <div class="block-content">                        
                          <div class="form-body">

                              <input type="hidden" value="" name="avisar_id" id="avisar_id"/> 
                              <input type="hidden" value="" name="avisar_cliente_id" id="avisar_cliente_id"/> 
                              <input type="hidden" value="" name="avisar_venta_nro" id="avisar_venta_nro"/> 

                              <!-- Concretado -->
                              <div class="h5 form-group">
                                  <label class="h5 control-label push-10" id="label_avisar_concretado">¿Pudiste concretar la venta?</label>
                                  <div class="puntos_califica">
                                    <label><input name="avisar_concretado" type="radio" id="avisar_concretado" value="si" /> Si</label> <label><input name="avisar_concretado" type="radio" id="avisar_concretado" value="no" /> No</label>
                                  </div>
                              </div>   

                              <!-- Comentarios -->
                              <div class="form-group col-sm-12">

                                  <label class="h5 control-label push-5">Dejanos tus comentarios para ayudarnos a mejorar</label>
                                  <div>
                                      <textarea class="form-control" name="avisar_valoracion" id="avisar_valoracion" rows="3"></textarea>
                                  </div>
                              </div>     

                          </div>
                      </div>
                  </form>
                  <div class="clearfix"></div>
                  <hr>
                  <div class="form-group form-group-login">
                      <div class="text-center">
                          <button class="btn btn-info push-10-r" id="btn_notificar" onclick="aceptar_avisar();">Notificar</button>
                          <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                      </div>    
                  </div>
              </div>
        </div>
    </div>
</div> 
<!-- fin MODAL VALORAR PEDIDO --> 


<!-- MODAL CANCELAR PEDIDO o TURNO -->
<div class="modal fade" id="modal_cancelar" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-cancelar-title-pedido-nro"> Turno / Pedido</h3>
                </div>
            </div>

              <div class="block-content"> 

                  <form class="form-horizontal action="#" id="form_cancelar">
                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_cancelar_error" style="display:none">
                          <small id="cancelar_error_message"></small>                 
                      </div>
                      <!-- END Div Error -->     

                      <input type="hidden" value="" name="hd_cancela_id" id="hd_cancela_id"/> 
                      <div class="block-content">                        
                          <div class="form-body">
  
                              <!-- Comentario -->
                              <div class="form-group">

                                  <label class="h5 control-label push-5">Ingresá un comentario para cancelar</label>
                                  <div>
                                      <textarea class="form-control" name="cancelar_comentario" id="cancelar_comentario" rows="3"></textarea>
                                  </div>
                              </div>     
                          </div>
                      </div>
                  </form>
                  <hr>
                  <div class="form-group form-group-login">
                      <div class="text-center">
                          <button class="btn btn-danger push-10-r" id="btn_cancelar" onclick="aceptar_cancelar();">Cancelar</button>
                          <button class="btn btn-default" type="button" data-dismiss="modal">Volver</button>
                      </div>    
                  </div>
              </div>
        </div>
    </div>
</div> 
<!-- fin MODAL CANCELAR PEDIDO o TURNO --> 


<!-- MODAL DETALLES ARTICULO -->
<div class="modal fade" id="modal_detalles_articulo" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="block">
                <div class="block-content">
                    <ul class="block-options login-close" >
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>                  
                    <div class="row items-push">
                        <div class="col-sm-12">
                            <!-- Nombre y Marca -->
                            <span class="h5">
                                <span class="font-w600 text-muted" id="modal_articulo_marca"></span>
                                <br>
                                <p class="h4 font-w600" id="modal_articulo_nombre"></p>
                            </span>
                            <!-- Nombre, Marca y Precio -->                         
                        </div>
                        <div class="col-sm-6">
                            <!-- Imagen -->
                            <div class="row" style="position: relative">
                                <div class="col-xs-12 push-10 push-10 img-container">
                                    <div class="div-img-articulos-detalle" id="modal_articulo_imagen">
                                    </div>
                                </div>
                            </div>
                            <!-- FIN Imagen -->
                        </div>
                        <div class="col-sm-6" >
                            
                            <!-- Detalles -->                            
                            <div  id="modal_articulo_detalle">
                            </div>      
                       
                            <!-- Presentaciones -->  
                            <span id="span_presentaciones_articulo_local">
                              <div class="h5 font-w600 push-5 push-5-t">Presentaciones Activas</div>                          
                              <div  id="modal_articulo_presentaciones"></div>                      

                              <div class="h5 font-w600 push-5 push-5-t">Presentaciones Inactivas</div>                          
                              <div  id="modal_articulo_presentaciones_inactivas"></div>  
                            </span>                            

                            <span id="span_presentaciones_articulo_geotienda">
                              <div class="h5 font-w600 push-5 push-5-t">Presentaciones</div>                          
                              <div  id="modal_articulo_presentaciones_geotienda"></div>   
                            </span>

                            <!-- Características -->
                            <!--
                            <div><strong>Animal</strong> <span id="modal_articulo_animal"></span></div> 
                            <div id="div_modal_articulo_raza" style="display:none"><strong>Raza</strong> <span id="modal_articulo_raza"></span></div>   
                            <div id="div_modal_articulo_tamanio" style="display:none"><strong>Tamaño</strong> <span id="modal_articulo_tamanio"></span></div> 
                            <div id="div_modal_articulo_edad" style="display:none"><strong>Edad</strong> <span id="modal_articulo_edad"></span></div> 
                            <div id="div_modal_articulo_medicados" style="display:none"><strong>Medicados</strong> <span id="modal_articulo_medicados"></span></div>     
                            -->                            
                            <!-- FIN Características -->
                        </div>

                    </div>
                        <hr>
                        <div class="text-center">
                            <button class="btn btn-info" type="button" data-dismiss="modal"> Aceptar</button>
                        </div>  
                        <br/>                   
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL DETALLES ARTICULO --> 


<!-- MODAL PRESENTACIONES ARTICULO AL LOCAL -->
<div class="modal fade" id="modal_presentaciones_articulo_local" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3>Presentaciones</h3>
            </div>

            <div class="block">
              <div class="block-content">                 
                  <div class="row items-push">

                      <!-- Div Error -->
                      <div class="alert alert-danger alert-dismissable" id="div_presentaciones_articulo_local_error" style="display:none">
                          <small id="presentaciones_articulo_local_error"></small>                 
                      </div>
                      <!-- END Div Error -->   

                      <div class="col-sm-12">
                          <!-- Nombre y Marca -->
                          <span class="h5">
                              <span class="font-w600 text-muted" id="modal_presentaciones_marca"></span>
                              <br>
                              <p class="h4 font-w600" id="modal_presentaciones_nombre"></p>
                          </span>
                          <!-- Nombre, Marca y Precio -->                         
                      </div>
                      <div class="col-sm-4">
                          <!-- Imagen -->
                          <div class="row" style="position: relative">
                              <div class="col-xs-12 push-10 push-10 img-container">
                                  <div class="div-img-articulos-detalle" id="modal_presentaciones_imagen">
                                  </div>
                              </div>
                          </div>
                          <!-- FIN Imagen -->
                      </div>
                      <div class="col-sm-8" >
                               
                          <!-- Presentaciones -->  
                          <div class="row">
                            <h4 class="font-w300 push-10 red col-xs-12 text-center" style="font-size: 18px">Seleccioná las presentaciones que vendas y luego ingresale el precio.</h4>
                            <div class="h6 font-w600 push-5 push-5-t col-xs-3">Presentac.</div>                          
                            <div class="h6 font-w600 push-5 push-5-t col-xs-3">Precio</div>                          
                            <div class="h6 font-w600 push-5 push-5-t col-xs-4">Descuento</div>                          
                            <div class="h6 font-w600 push-5 push-5-t col-xs-2">Stock</div>                          
                          </div>
                          <div class="row" id="modal_presentaciones_presentaciones">
                          </div>     

                          <div class="alert alert-danger push--10-l" id="div_advertencia_presentaciones" style="display: none">
                              <h3 class="font-w300 push-15">Atención</h3>
                              <p>El producto debe tener por lo menos una presentación activa para que esté disponible en la tienda.</p>
                          </div>                                                                     
                      </div>
                      <input name="presentaciones_articulo_id" id="presentaciones_articulo_id" type="hidden" value="" />    
                      <input name="presentaciones_articulo_tipo" id="presentaciones_articulo_tipo" type="hidden" value="" />    
                  </div>

                    <hr>
                    <div class="form-group form-group-login">
                        <div class="text-center">
                          <button class="btn btn-info push-10-r" id="btn_save_presentaciones_aticulo_local" onclick="save_presentaciones_articulo_local()" >Aceptar</button>
                          <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                        </div>    
                    </div>                  
                </div>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL PRESENTACIONES ARTICULO AL LOCAL -->


<!-- MODAL NUEVO / EDITAR PRODUCTO -->
<div class="modal fade" id="modal_producto_local" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande">
            <div class="modal-header bg-info text-white">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 id="modal_title_producto_local">Nuevo Producto</h3>
            </div>

            <div class="modal-body form">
              <div class="row items-push">

                  <!-- Div Error -->
                  <div class="alert alert-danger alert-dismissable" id="div_producto_local_error" style="display:none">
                      <small id="producto_local_error"></small>                 
                  </div>
                  <!-- END Div Error -->   

                  <div class="form">
                      <form action="#" id="form_producto_local" class="form-horizontal" enctype="multipart/form-data">
                          <input type="hidden" value="" name="id_producto_local" id="id_producto_local"/> 
                          <input type="hidden" value="0" id="con_marca_producto_local" name="con_marca_producto_local" /> 
                          <input type="hidden" value="0" id="con_presentacion_producto_local" name="con_presentacion_producto_local" /> 
                          <input type="hidden" value="" id="hd_rubro_producto_local" name="hd_rubro_producto_local" /> 
                          <div class="form-body">
                              <div class="col-md-11">

                                <!-- RUBRO --> 
                                <div class="form-group">
                                    <label class="control-label col-md-2">Rubro</label>
                                    <div class="col-md-10">
                                      <select id="rubro_producto_local" name="rubro_producto_local" class="form-control" onchange="cambioRubro_producto_local(this.value)">                       
                                          <option selected value="">Seleccioná...</option>
                                         
                                          <?php foreach($rubros as $rubro){ ?>
                                              <option value="<?php echo $rubro->id ?>"><?php echo $rubro->nombre ?></option> 
                                          <?php } ?> 
                                      </select>  
                                    </div>
                                </div>

                                <!-- ANIMAL --> 
                                <div class="form-group">
                                    <label class="control-label col-md-2">Animal</label>
                                    <div class="col-md-10">
                                        <select id="animal_producto_local" name="animal_producto_local" class="form-control" onchange="cambioAnimal_producto_local(this.value)">                       
                                            <option selected value="">Seleccioná...</option>
                                           
                                            <?php foreach($animales as $animal){ ?>
                                                <option value="<?php echo $animal->id ?>"><?php echo $animal->nombre ?></option> 
                                            <?php } ?>
                                        </select>  
                                    </div>
                                </div>

                                <!-- MARCA  --> 
                                <div class="form-group" id="divMarca_producto_local">
                                    <label class="control-label col-md-2">Marca</label>
                                    <div class="col-md-10">
                                        <select name="marca_producto_local" id="marca_producto_local"  class="form-control" id="marca" >
                                            <option selected value="1">Otras Marcas</option>
                                        </select>                                          
                                    </div>
                                </div>    

                                <!-- NOMBRE --> 
                                <div class="form-group">
                                    <label class="control-label col-md-2">Nombre</label>
                                    <div class="col-md-10">
                                        <input name="nombre_producto_local" placeholder="Nombre" class="form-control" type="text">
                                        
                                    </div>
                                </div>

                                 <!-- DETALLE --> 
                                <div class="form-group">
                                    <label class="control-label col-md-2">Detalle</label>
                                    <div class="col-md-10">
                                        <textarea id="detalle_producto_local" name="detalle_producto_local" maxlength="10"></textarea>       
                                        <input name="hdDetalle" placeholder="hdDetalle" class="form-control" type="text" style="display:none">                
                                        
                                    </div>
                                </div>

                                 <!-- IMAGEN --> 
                                <div class="form-group">
                                    <label class="control-label col-md-2">Imagen</label>
                                    <div class="col-md-10">
                                        <button type="button" id="btn_imagen_producto_local" data-toggle="modal" data-target="#modal_imagenes_producto_local" class="btn btn-default pull-right">o elegir una imagen del local</button>    

                                        <input name="foto_producto_local" type="file" id="foto_producto_local" size="40" class="btn btn-default pull-left" >  
                                        <div class="clearfix"></div>  
                                        <input type="hidden" value="noimage.gif" id="imagen_producto_local" name="imagen_producto_local" />
                                        
                                        
                                        <div class="clearfix"></div>    
                                        <br/>                                                       
                                        <img class="centrado" src="<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>" id="imagenPreview_producto_local" /> 
                                        

                                    </div>
                                </div>

                                <!-- RAZAS --> 
                                <div class="form-group" id="divRaza_producto_local">
                                    <label class="control-label col-md-3">Raza</label>
                                    <div class="col-md-8">
                                        <select name="raza_producto_local[]" id="raza_producto_local" class="form-control" multiple size="4">
                                            <?php foreach($razas as $raza){ ?>
                                                <option value="<?php echo $raza->id ?>" class="<?php echo $raza->animal_id ?>"><?php echo $raza->nombre ?></option> 
                                            <?php } ?>    
                                        </select>  
                                        
                                    </div>
                                </div>    

                                <!-- TAMAÑOS --> 
                                <div class="form-group" id="divTamanio_producto_local">
                                    <label class="control-label col-md-3">Tamaño</label>
                                    <div class="col-md-8">
                                        <select name="tamanio_producto_local[]" id="tamanio_producto_local" class="form-control" multiple size="4">       
                                            <?php foreach($tamanios as $tamanio){ ?>
                                                <option value="<?php echo $tamanio->id ?>" class="<?php echo $tamanio->animal_id ?>"><?php echo $tamanio->nombre ?></option> 
                                            <?php } ?>    
                                        </select>  
                                    </div>
                                </div>    
                                
                                <!-- MEDICADOS --> 
                                <div class="form-group" id="divMedicados_producto_local">
                                    <label class="control-label col-md-3">Medicados</label>
                                    <div class="col-md-8">
                                            <label class="push-10-r"><input name="medicados_producto_local" type="radio" id="medicados_producto_local_si" value="1" /> Si</label> 
                                            <label class="push-10-r"><input name="medicados_producto_local" type="radio" id="medicados_producto_local_no" value="0" /> No</label>
                                            <label><input name="medicados_producto_local" type="radio" id="medicados_producto_local_null" value="" checked="checked" /> No definido</label>
                                        
                                    </div>
                                </div>   

                                <!-- EDAD --> 
                                <div class="form-group" id="divEdad_producto_local">
                                    <label class="control-label col-md-3">Edad</label>
                                    <div class="col-md-8">
                                        <select name="edad_producto_local" class="form-control">                       
                                            <option selected value="">Seleccioná...</option>
                                            <option  value="Cachorros">Cachorros</option>
                                            <option  value="Adultos">Adultos</option>
                                            <option  value="Senior">Senior</option>
                                            <option  value="Todas">Todas las Edades</option>                                    
                                        </select>  
                                        
                                    </div>
                                </div>    

                                <!-- PRESENTACIONES --> 
                                <div class="form-group"  id="divPresentacion_producto_local">
                                    <label class="control-label col-md-3">Presentaciones</label>
                                    <div class="col-md-8">
                                        <select name="presentacion_producto_local[]" id="presentacion_producto_local" class="form-control" multiple size="10">
                                            <?php foreach($presentaciones as $presentacion){ ?>
                                                  <option value="<?php echo $presentacion->id ?>"><?php echo $presentacion->nombre ?></option> 
                                            <?php } ?>
                                        </select>  
                                        
                                    </div>
                                </div>                                                                                 
                              </div>
                            </div>
                      </form>
                  </div>
              </div>
            </div>
            <div class="modal-footer">
              <button class="btn btn-info push-10-r" id="btn_save_producto_local" onclick="save_producto_local()" >Aceptar</button>
              <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div> 
<!-- fin MODAL NUEVO PRODUCTO -->


<!-- MODAL AUMENTAR EXPOSICION (LOCAL DESTACADO) -->
<div class="modal fade" id="modal_aumentar_exposicion" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content modal-grande">
            <div class="modal-header bg-info text-white">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="text-left">
                    <h3 class="modal-title-pedido-nro">¡Aumentá tu Exposición!</h3>
                </div>
            </div>

              <div class="block-content"> 
                      <div class="block-content">                        
                          <div class="form-body">

                              <!-- Comentarios -->
                              <div class="form-group col-sm-12">
                                  <div class="h4 text-info"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Tienda destacada (ubicación en la primer pantalla)</div>
                                  <div class="h4 text-info push-5-t"><i class="fa fa-hand-o-right"></i>&nbsp;&nbsp;Publicidad extra y en redes sociales</div>
                                  <br />
                              </div>    
                              <div class="text-center push-20">                              
                                <span class="h3 control-label push-5 text-success font-w600" id="precio_local_destacado"></span>
                                <br />
                                <span class="h5 control-label push-5 text-success font-w600">Precio fijo mensual</span>
                              </div>
                          </div>
                      </div>
                  <div class="clearfix"></div>
                  <hr>
                  <div class="form-group form-group-login">
                      <div class="text-center">
                          <button class="btn btn-info push-10-r" id="btn_aumentar_exposicion" onclick="return aceptar_aumentar_exposicion();">Confirmar</button>
                          <button class="btn btn-default" type="button" data-dismiss="modal">Cancelar</button>
                      </div>    
                  </div>
              </div>
        </div>
    </div>
</div> 
<!-- fin MODAL AUMENTAR EXPOSICION (LOCAL DESTACADO) -->


<!-- MODAL CONFIGURAR DIA TURNOS -->
<div class="modal fade" id="modal_dia_turno" role="dialog" data-focus-on="input:first">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" id="modal_dia_turno_content">
            <div class="modal-header bg-success text-white sin_click">
                <h4 class="modal-title-dia-turnos">Horarios Disponibles</h4>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_dia_turno" class="form-horizontal">
                    <input type="hidden" value="" name="hd_dia_turno_servicio" id="hd_dia_turno_servicio"/> 
                    <input type="hidden" value="" name="hd_dia_turno_dia" id="hd_dia_turno_dia"/> 
                    
                    <div class="form-body">    
                      <!-- Local abierto / cerrado -->
                      <input type='hidden' value='1' name='hd_dia_turno_abierto' id='hd_dia_turno_abierto'>
                      <label id="lbl_tienda_abierta_siempre" class="sin_click css-input css-checkbox css-checkbox-success push-10-l font-16" style="z-index: 1;">
                          Tienda Abierta
                      </label> 
                      <label id="lbl_tienda_abierta" class="css-input css-checkbox css-checkbox-success push-10-l font-16" style="z-index: 1;">
                          <input type="checkbox" name ="chk_dia_turno_abierto" id="chk_dia_turno_abierto" checked="checked"><span></span> Tienda Abierta
                      </label>   
                      <div class="clearfix"></div>

                      <!-- Horarios del dia -->
                      <!-- Primer Turno -->
                      <div id="div_dia_turno_horarios1" class="col-sm-6" style="height: 400px" style="z-index: 1;"></div>
                      <!-- Segundo Turno -->
                      <div id="div_dia_turno_horarios2" class="col-sm-6" style="height: 400px" style="z-index: 1;"></div>
                    </div>
                </form>
                <div class="sin_click" style="background-color: transparent; position: absolute; top:0; left:0; width: 100%; height: 100%; z-index: 0;"></div>

            </div>
            <div class="modal-footer sin_click"> 
                <button type="button" id="btn_dia_turno" onclick="return actualizar_dia_turno();" class="btn btn-success push-10-r">Guardar</button>
                <button type="button" class="btn btn-default" onclick="return cancela_actualizar_dia_turno();" >Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL CONFIGURAR DIA TURNOS -->


<!-- MODAL IMAGEN PRODUCTO -->
<div class="modal fade" id="modal_imagenes_producto_local" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-grande" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title-imagen">Imágenes del local</h3>
            </div>
            <div class="modal-body form">

                <div class="row items-push js-gallery-advanced"
                    <?php
                
                    $map = glob("assets/img/articulos/locales/miniaturas/" . $_SESSION['frontend_user_id'] . "_*");

                    foreach($map as $m)
                    {
                        $nombreArray=explode("/", $m);
                        $nombreImagen=$nombreArray[5];

                        $imagen = BASE_PATH . '/' . $m;
                    ?>
                        <div class="col-sm-6 col-md-4 col-lg-3 animated fadeIn" id="<?php echo $nombreImagen ?>" >
                            <div class="img-container" style="width: 200px; height: 200px; margin: 6px">
                                <img height="200" class="img-responsive" src="<?php echo $imagen ?>" alt="" >
                                <div class="img-options">
                                    <div class="img-options-content">
                                        <h4 class="h6 font-w400 text-white-op push-15"><?php echo $nombreImagen ?></h4>
 
                                        <div class="btn-group">
                                            <br/><br/> 
                                            <a class="btn btn-info" href="javascript:void(0)" onclick="return elegirImagen_producto_local('<?php echo $nombreImagen; ?>');"><i class="fa fa-image"></i> Elegir</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php }  // end foreach
                    ?>
                    
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- fin MODAL IMAGEN PRODUCTO -->
<script type="text/javascript">

var local_id;
var table_ventas;
var table_turnos;
var table_pedidos;
var table_notificaciones;
var table_comentarios_venta;
var table_productos;
var table_productos_inactivos;
var tipo_pedido;
var mascota_actual;
var save_vacuna_method;
var save_method_producto_local; 
var animal_conraza = false;
var animal_contamanio = false;
var rubro_conraza = false;
var rubro_contamanio = false;

// Filtros productos activos
var cuenta_filtros_aplicados = 0;

var filtros_aplicados_creador = '';
var filtros_aplicados_creador_id = -1;
var select_creador;
var filtro_creador;
var aplicado_creador = '';
var cuenta_filtro_creador = 0;

var filtros_aplicados_rubro = '';
var filtros_aplicados_rubro_id = -1;
var select_rubro;
var filtro_rubro;
var aplicado_rubro = '';
var cuenta_filtro_rubro = 0;

var filtros_aplicados_animal = '';
var filtros_aplicados_animal_id = -1;
var select_animal;
var filtro_animal;
var aplicado_animal = '';
var cuenta_filtro_animal = 0;

var filtros_aplicados_marca = '';
var filtros_aplicados_marca_id = -1;
var select_marca;
var filtro_marca;
var aplicado_marca = '';
var cuenta_filtro_marca = 0;

var filtros_aplicados_raza = '';
var filtros_aplicados_raza_id = -1;
var select_raza;
var filtro_raza;
var aplicado_raza = '';
var cuenta_filtro_raza = 0;

var filtros_aplicados_tamanio = '';
var filtros_aplicados_tamanio_id = -1;
var select_tamanio;
var filtro_tamanio;
var aplicado_tamanio = '';
var cuenta_filtro_tamanio = 0;

var filtros_aplicados_edad = '';
var filtros_aplicados_edad_id = -1;
var select_edad;
var filtro_edad;
var aplicado_edad = '';
var cuenta_filtro_edad = 0;

var filtros_aplicados_presentacion = '';
var filtros_aplicados_presentacion_id = -1;
var select_presentacion;
var filtro_presentacion;
var aplicado_presentacion = '';
var cuenta_filtro_presentacion = 0;

var filtros_aplicados_medicados = '';
var filtros_aplicados_medicados_id = -1;
var select_medicados;
var filtro_medicados;
var aplicado_medicados = '';
var cuenta_filtro_medicados = 0;

var drawCallback = 0;

// Filtros productos inactivos
var cuenta_filtros_aplicados_inactivos = 0;

var filtros_aplicados_creador_inactivos = '';
var filtros_aplicados_creador_id_inactivos = -1;
var select_creador_inactivos;
var filtro_creador_inactivos;
var aplicado_creador_inactivos = '';
var cuenta_filtro_creador_inactivos = 0;

var filtros_aplicados_rubro_inactivos = '';
var filtros_aplicados_rubro_id_inactivos = -1;
var select_rubro_inactivos;
var filtro_rubro_inactivos;
var aplicado_rubro_inactivos = '';
var cuenta_filtro_rubro_inactivos = 0;

var filtros_aplicados_animal_inactivos = '';
var filtros_aplicados_animal_id_inactivos = -1;
var select_animal_inactivos;
var filtro_animal_inactivos;
var aplicado_animal_inactivos = '';
var cuenta_filtro_animal_inactivos = 0;

var filtros_aplicados_marca_inactivos = '';
var filtros_aplicados_marca_id_inactivos = -1;
var select_marca_inactivos;
var filtro_marca_inactivos;
var aplicado_marca_inactivos = '';
var cuenta_filtro_marca_inactivos = 0;

var filtros_aplicados_raza_inactivos = '';
var filtros_aplicados_raza_id_inactivos = -1;
var select_raza_inactivos;
var filtro_raza_inactivos;
var aplicado_raza_inactivos = '';
var cuenta_filtro_raza_inactivos = 0;

var filtros_aplicados_tamanio_inactivos = '';
var filtros_aplicados_tamanio_id_inactivos = -1;
var select_tamanio_inactivos;
var filtro_tamanio_inactivos;
var aplicado_tamanio_inactivos = '';
var cuenta_filtro_tamanio_inactivos = 0;

var filtros_aplicados_edad_inactivos = '';
var filtros_aplicados_edad_id_inactivos = -1;
var select_edad_inactivos;
var filtro_edad_inactivos;
var aplicado_edad_inactivos = '';
var cuenta_filtro_edad_inactivos = 0;

var filtros_aplicados_presentacion_inactivos = '';
var filtros_aplicados_presentacion_id_inactivos = -1;
var select_presentacion_inactivos;
var filtro_presentacion_inactivos;
var aplicado_presentacion_inactivos = '';
var cuenta_filtro_presentacion_inactivos = 0;

var filtros_aplicados_medicados_inactivos = '';
var filtros_aplicados_medicados_id_inactivos = -1;
var select_medicados_inactivos;
var filtro_medicados_inactivos;
var aplicado_medicados_inactivos = '';
var cuenta_filtro_medicados_inactivos = 0;

var drawCallback_inactivos = 0;


// Filtros productos geotienda
var cuenta_filtros_aplicados_geotienda = 0;

var filtros_aplicados_rubro_geotienda = '';
var filtros_aplicados_rubro_id_geotienda = -1;
var select_rubro_geotienda;
var filtro_rubro_geotienda;
var aplicado_rubro_geotienda = '';
var cuenta_filtro_rubro_geotienda = 0;

var filtros_aplicados_animal_geotienda = '';
var filtros_aplicados_animal_id_geotienda = -1;
var select_animal_geotienda;
var filtro_animal_geotienda;
var aplicado_animal_geotienda = '';
var cuenta_filtro_animal_geotienda = 0;

var filtros_aplicados_marca_geotienda = '';
var filtros_aplicados_marca_id_geotienda = -1;
var select_marca_geotienda;
var filtro_marca_geotienda;
var aplicado_marca_geotienda = '';
var cuenta_filtro_marca_geotienda = 0;

var filtros_aplicados_raza_geotienda = '';
var filtros_aplicados_raza_id_geotienda = -1;
var select_raza_geotienda;
var filtro_raza_geotienda;
var aplicado_raza_geotienda = '';
var cuenta_filtro_raza_geotienda = 0;

var filtros_aplicados_tamanio_geotienda = '';
var filtros_aplicados_tamanio_id_geotienda = -1;
var select_tamanio_geotienda;
var filtro_tamanio_geotienda;
var aplicado_tamanio_geotienda = '';
var cuenta_filtro_tamanio_geotienda = 0;

var filtros_aplicados_edad_geotienda = '';
var filtros_aplicados_edad_id_geotienda = -1;
var select_edad_geotienda;
var filtro_edad_geotienda;
var aplicado_edad_geotienda = '';
var cuenta_filtro_edad_geotienda = 0;

var filtros_aplicados_presentacion_geotienda = '';
var filtros_aplicados_presentacion_id_geotienda = -1;
var select_presentacion_geotienda;
var filtro_presentacion_geotienda;
var aplicado_presentacion_geotienda = '';
var cuenta_filtro_presentacion_geotienda = 0;

var filtros_aplicados_medicados_geotienda = '';
var filtros_aplicados_medicados_id_geotienda = -1;
var select_medicados_geotienda;
var filtro_medicados_geotienda;
var aplicado_medicados_geotienda = '';
var cuenta_filtro_medicados_geotienda = 0;

var drawCallback_geotienda = 0;

$(document).ready(function() 
{
    // Usuario con sesion iniciada   
    <?php if (isset($_SESSION['frontend_logged_in'])){ ?>             

        local_id = <?php echo $_SESSION['frontend_user_id']; ?>;

        $('#local_nombre').html('¡Hola <?php echo $_SESSION['frontend_nombre']; ?>!');
        $('#local_nombre2').html('¡Hola <?php echo $_SESSION['frontend_nombre']; ?>!');
        $('#local_avatar').attr('src', '<?php echo BASE_PATH ?>/assets/img/<?php echo $_SESSION['frontend_tipo']; ?>/miniaturas/<?php echo $_SESSION['frontend_avatar']; ?>');               

       // Muestra datos según tab seleccionado
        tab = '<?php echo $tab; ?>';

        switch (tab){
       
            case 'pedidos':
                mostrar_pedidos_local();                                      
            break;  

            case 'turnos':
                mostrar_turnos_local();                                      
            break;  

            case 'ventas':
                mostrar_ventas_local();                                      
            break;  

            case 'vacunas':
                mostrar_mascotas_local();                                      
            break;  

            case 'productos':                               
                mostrar_productos_local();
                mostrar_productos_geotienda();

                $("#raza_producto_local").chained("#animal_producto_local");
              //  $("#marca_producto_local").chained("#rubro_producto_local, #animal_producto_local"); 

            break;

            case 'notificaciones':                               
                mostrar_notificaciones_local();
            break;


            case 'datos':
                mostrar_datos_local();       
                mostrar_medios_pago_local('false');                               
            break;                                     
        }  

        // ckeditor para textarea Detalle de nuevo producto local
        CKEDITOR.replace('detalle_producto_local', 
        {
            toolbarGroups: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
                { name: 'colors', groups: [ 'colors' ] },            
                '/',            
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
                { name: 'styles', groups: [ 'styles' ] },
            ],

            removeButtons: 'Save,Language,BidiRtl,BidiLtr,Strike,Superscript,Subscript,Blockquote,CreateDiv',
            wordcount: { maxWordCount: <?php echo $palabras_descripcion_producto ?>, showParagraphs: false}
          });            

    <?php } ?>
});


function mostrar_pedidos_local(){

    tab = 'pedidos';

    //datatables
    table_pedidos = $('#table_pedidos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 25,

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_pedidos_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ 4 ], 
            "className": "text-right hidden-xs", 
        },       
        { 
            "targets": [ -1 ], 
            "orderable": false, 
        },          
        {
            "targets": [1,2,3], 
            "className": "hidden-xs", 
        }
        ],

        // Suma total del precio de cada pedido
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column(4)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column(4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 

            // Update footer
            if (pageTotal == total)
                $( api.column(4).footer() ).html('<b>$ '+ total + '</b>');            
            else
                $( api.column(4).footer() ).html('$ '+pageTotal +' ( $ '+ total +' total)');
        },

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // Ddrop-down lists de busqueda
                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 140px"><option value="">MES / AÑO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($fechas_pendientes as $fecha){ ?>
                        select.append( '<option value="<?php echo $fecha->mes_anio ?>"><?php echo $fecha->fecha_pedido ?></option>' )
                    <?php } ?>    
                }                                                        

               if (column.index() == 3){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 250px; width: 150px;"><option value="">CLIENTE</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($clientes_pendientes as $cliente){ ?>
                        select.append( '<option value="<?php echo $cliente->nombre ?>"><?php echo $cliente->nombre ?></option>' )
                    <?php } ?>    
                }  
        
            } );

        }, 
    });

}


function mostrar_ventas_local(){

    tab = 'ventas';

    //datatables
    table_ventas = $('#table_ventas')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 25,

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_ventas_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ 5 ], 
            "className": "text-right hidden-xs", 
        },       
        { 
            "targets": [ -1 ], 
            "orderable": false, 
        },          
        {
            "targets": [1,2,4], 
            "className": "hidden-xs", 
        }
        ],

        // Suma total del precio de cada venta
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column(5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 

            // Update footer
            if (pageTotal == total)
                $( api.column(5).footer() ).html('<b>$ '+ total + '</b>');            
            else
                $( api.column(5).footer() ).html('$ '+pageTotal +' ( $ '+ total +' total)');
        },

        initComplete: function () {
            this.api().columns().every( function () {

                var column = this;

                // Ddrop-down lists de busqueda
                if (column.index() == 0){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 140px;"><option value="">TIPO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                        select.append( '<option value="venta">venta</option>' );
                        select.append( '<option value="turno">turno</option>' );
                } 

                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 220px"><option value="">MES / AÑO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($fechas as $fecha){ ?>
                        select.append( '<option value="<?php echo $fecha->mes_anio ?>"><?php echo $fecha->fechas ?></option>' )
                    <?php } ?>    
                }                                                        

               if (column.index() == 3){
                    var select = $('<select class="form-control hidden-xs hidden-sm" style="padding: 6px !important; position: absolute; top: -39px; left: 492px;"><option value="">ESTADO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
                          
                        select.append( '<option value="concretado">concretado</option>' );
                        select.append( '<option value="no concretado">no concretado</option>' );
                        select.append( '<option value="cancelado">cancelado</option>' );
                } 

               if (column.index() == 4){
                    var select = $('<select id="ddl_clientes_ventas" class="form-control hidden-xs" style="padding: 6px !important; position: absolute; top: -39px; left: 330px; width: 150px;"><option value="">CLIENTE</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();
                        } );
     
                    <?php foreach($clientes as $cliente){ ?>
                        select.append( '<option value="<?php echo $cliente->nombre ?>"><?php echo $cliente->nombre ?></option>' )
                    <?php } ?>    
                }       
        
            } );

        }, 
    });

}


/* DATOS DE UNA VENTA */
function detalle_venta(venta_id)
{
    $("#venta_cliente_avatar").html('');
    $("#venta_comentario").val('');

    // ENCABEZADO
    $.ajax({
      url : "<?php echo BASE_PATH ?>/admin/Venta/get_venta_by_id/" + venta_id,
      cache: false,
      success: function(data){

        venta = jQuery.parseJSON(data);

        // Datos de la venta
        $('.modal-title-venta-nro').text('Venta ' + venta.numero  + ' (' + venta.estado + ')');
        $('#modal_detalle_venta').modal('show'); 

        $('#venta_fecha').text(venta.fecha);
        $('#venta_condicion_venta').text(venta.condicion_venta);
        $('#venta_estado').text(venta.estado);
        
        // Datos del cliente
        $("#venta_cliente_avatar").append('<img src="<?php echo BASE_PATH ?>/assets/img/cliente/miniaturas/' + venta.cliente_avatar + '" width="100%">');

        $('#venta_cliente_nombre').text(venta.cliente_nombre + ' ' + venta.cliente_apellido);
        $('#venta_titulo_nuevo_comentario').text('Enviale un mensaje a ' + venta.cliente_nombre + ' ' + venta.cliente_apellido);
        $('#venta_cliente_telefono').text(venta.cliente_telefono_venta);

        // Para locales que no hacen envio a domicilio
        /*
        if (venta.local_envio_domicilio == '0')
        {
            $('#div_venta_cliente_telefono').show();
        } 
        */       
        
        // Datos adicionales
        $('#venta_tipo_envio').text(venta.tipo_envio);

        if (venta.tipo_envio == 'A domicilio')
        {
            $('#div_venta_cliente_direccion').show();
            $('#div_venta_cliente_ciudad').show();
            $('#venta_cliente_direccion').text(venta.cliente_direccion);
            $('#venta_cliente_ciudad').text(venta.cliente_localidad + ', ' +  venta.cliente_provincia);          
        }
        else
        {
            $('#div_venta_cliente_direccion').hide();
            $('#div_venta_cliente_ciudad').hide();
        }

        // Pedido cancelado
        if (venta.cancelado_por != '')
        {
            $('#div_venta_cancelado').show();
            $('#venta_cancelado_por').text(venta.cancelado_por);
            $('#venta_cancelado_motivo').text(venta.cancelado_comentario);          
        }
        else
        {
            $('#div_venta_cancelado').hide();
        }  

        // Valoración
        if (venta.puntaje)
        {
          $('#venta_puntaje').html(venta.puntaje);
          $('#venta_valoracion').text(venta.valoracion);
          $('#venta_valoracion_bloque').show();
        }
        else
        {
          $('#venta_valoracion_bloque').hide();
        }

        // Para pedidos pendiente, permite ingresar comentarios
        if (venta.estado == 'pendiente')
        {
          $('#bloque_add_comentario_venta').show();
          $('#venta_titulo_comentarios').show();
          $('#venta_titulo_comentarios').text('¡Coordiná el envío o retiro del pedido desde acá!');
        }
        else
        {
          $('#bloque_add_comentario_venta').hide();       
          $('#venta_titulo_comentarios').hide();
        }

        // Datos para form de nuevo comentario
        $("#hd_venta_id").val(venta_id);
        $("#hd_venta_local_id").val(venta.local_id);          

      } 
    });

    // DETALLE
    table_detalle_venta = $('#table_detalle_venta').DataTable({ 

        "lengthChange": false, 
        "paging": false,
        "filter": false,
        "ordering": false,
        "info": false,

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_list_detalle/" + venta_id,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "className": "text-right", "targets": [4] 
        }
        ],

        // Suma total del precio de cada detalle
        footerCallback: function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
    
            // Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html('$'+ total);
        }

    });  

    // COMENTARIOS
    table_comentarios_venta = $('#table_comentarios_venta').DataTable({ 

        "language": {
            "emptyTable": "No hay mensajes para mostrar.",
        },
        "lengthChange": false, 
        "paging": false,
        "filter": false,
        "ordering": false,
        "info": false,

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Venta/ajax_comentarios_list/" + venta_id,
            "type": "POST"
        },
    });  
          

    $('#btnSaveComentarioVenta').text('Aceptar');    

}
/* FIN INFORMACION DE UNA VENTA */


/* INFORMACION DE UN TURNO */
function detalle_turno(turno_id)
{
    $("#turno_cliente_avatar").html('');
    $("#hd_turno_id").val(turno_id);
    $("#turno_comentario").val('');    

    $.ajax({
      url : "<?php echo BASE_PATH ?>/admin/turno/get_by_id/" + turno_id,
      cache: false,
      success: function(data){

        turno = jQuery.parseJSON(data);

        // Datos de la turno
        $('.modal-title-turno-nro').text('Turno ' + turno.numero + ' (' + turno.estado + ')');
        $('#modal_detalle_turno').modal('show'); 

        $('#turno_servicio').text(turno.servicio  + ' - ' + turno.servicio_item  + ' ($' + turno.total + ')');
        $('#turno_fecha').text(turno.fecha + ' a las ' + turno.hora + ' hs.');

        // Datos del cliente
        $("#turno_cliente_avatar").append('<img src="<?php echo BASE_PATH ?>/assets/img/cliente/miniaturas/' + turno.cliente_avatar + '" width="100%">');

        $('#turno_cliente').text(turno.cliente);
        $('#turno_titulo_nuevo_comentario').text('Enviale un mensaje a ' + turno.cliente);        

        // Datos adicionales
        $('#turno_tipo_envio').text(turno.tipo_envio);
        $('#turno_cliente_direccion').text(turno.cliente_direccion);
        $('#turno_cliente_ciudad').text(turno.cliente_localidad + ', ' +  turno.cliente_provincia);

        // Turno cancelado
        if (turno.cancelado_por != '')
        {
            $('#div_turno_cancelado').show();
            $('#turno_cancelado_por').text(turno.cancelado_por);
            $('#turno_cancelado_motivo').text(turno.cancelado_comentario);          
        }
        else
        {
            $('#div_turno_cancelado').hide();
        }

        // Valoración
        if (turno.puntaje)
        {
          $('#turno_puntaje').html(turno.puntaje);
          $('#turno_valoracion').text(turno.valoracion);
          $('#turno_valoracion_bloque').show();
        }
        else
        {
          $('#turno_valoracion_bloque').hide();
        }
        
        // Para turnos con estado 'reservado', permite ingresar comentarios
        if (turno.estado == 'reservado')
        {
          $('#bloque_add_comentario_turno').show();
          $('#turno_titulo_comentarios').show();
          $('#turno_titulo_comentarios').text('¡Coordiná la visita del cliente desde acá!');
        }
        else
        {
          $('#bloque_add_comentario_turno').hide();       
          $('#turno_titulo_comentarios').hide();
        }

        // Datos para form de nuevo comentario
        $("#hd_turno_id").val(turno_id);
        $("#hd_turno_local_id").val(turno.local_id);            
      } 
    });


    // COMENTARIOS
    table_comentarios_turno = $('#table_comentarios_turno').DataTable({ 

        "language": {
            "emptyTable": "No hay mensajes para mostrar.",
        },
        "lengthChange": false, 
        "paging": false,
        "filter": false,
        "ordering": false,
        "info": false,

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo BASE_PATH ?>/admin/Turno/ajax_comentarios_list/" + turno_id,
            "type": "POST"
        },
    });  
          

    $('#btnSaveComentarioTurno').text('Aceptar');       

}


function save_comentario_turno()
{
    // Si el local ingresó un nuevo comentario, se guarda.
    if ($('#turno_comentario').val().trim() != '')
    {
        $('#btnSaveComentarioTurno').text('Guardando...'); 
        $('#btnSaveComentarioTurno').attr('disabled',true); 


        var url = "<?php echo BASE_PATH ?>/Turno/ajax_add_comentario_local";

        $.ajax({
            url : url ,
            type: "POST",
            data: $('#form_comentario_turno').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#btnSaveComentarioTurno').text('Aceptar');
                $('#btnSaveComentarioTurno').attr('disabled',false);

                $('#modal_detalle_turno').modal('hide');  

                 aviso('success', 'Mensaje enviado.');                   

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_detalle_turno').modal('hide');  
                aviso('danger', textStatus, 'Error al ingresar mensaje: (' + errorThrown + ')'); 

                $('#btnSaveComentarioTurno').text('Aceptar');
                $('#btnSaveComentarioTurno').attr('disabled',false); 
            }
        });     
    }
    else
    {
      $('#modal_detalle_turno').modal('hide');  
    }
}


// Si el local ingresó un nuevo comentario, cambia el botón 'Aceptar' por 'Enviar'
$('#turno_comentario').on('input',function(e){
    
    if ($('#turno_comentario').val().trim() != '')
        $('#btnSaveComentarioTurno').text('Enviar'); 
    else
        $('#btnSaveComentarioTurno').text('Aceptar');    
});

/* FIN INFORMACION DE UN TURNO */


function mostrar_turnos_local(){

    tab = 'turnos';

    //datatables
    table_turnos = $('#table_turnos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_turnos_list",
            "type": "POST"
        },

        "columnDefs": [
        { 
            "targets": [ 3 ], 
            "className": "text-right hidden-xs", 
        },       
        {
            "targets": [1,2,4], 
            "className": "hidden-xs", 
        },
        {
            "targets": [-1], 
            "orderable": false, 
        }          
        ],

    });

}


function mostrar_cancelar_turno(id)
{
    tipo_pedido = 'turno';

    var str = "" + 1;
    var pad = "S0000";
    var numero = pad.substring(0, pad.length - id.length) + id;

    $('#cancelar_comentario').val('');
    $('#div_cancelar_error').hide();   
    $("#hd_cancela_id").val(id);
    $('.modal-cancelar-title-pedido-nro').text('Cancelar turno ' + numero);
    
    $('#modal_cancelar').modal('show'); 
}


function mostrar_cancelar_pedido(id)
{
    tipo_pedido = 'venta';

    var str = "" + 1;
    var pad = "P0000";
    var numero = pad.substring(0, pad.length - id.length) + id;

    $('#cancelar_comentario').val('');
    $('#div_cancelar_error').hide();   
    $("#hd_cancela_id").val(id);
    $('.modal-cancelar-title-pedido-nro').text('Cancelar pedido ' + numero);
    
    $('#modal_cancelar').modal('show'); 
}


function aceptar_cancelar()
{
    if (tipo_pedido == 'venta')
      var url = "<?php echo BASE_PATH ?>/Venta/ajax_cancelar/local";
    else
      var url = "<?php echo BASE_PATH ?>/Turno/ajax_cancelar/local";


    $('#btn_cancelar').html('Procesando...');
    $('#btn_cancelar').attr('disabled', true); 

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_cancelar').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_cancelar_error').hide();  
                $('#modal_cancelar').modal('hide');  

                if (tipo_pedido == 'venta')
                {
                  table_pedidos.ajax.reload(null,false); 
                  aviso('success', 'Pedido cancelado.');  
                }
                else
                {
                  table_turnos.ajax.reload(null,false); 
                  aviso('success', 'Turno cancelado.');                                    
                }
            }
            // Fallo en actualizacion
            else
            {
                switch(data.error) 
                {
                    // Horario excedido    
                    case 'horario':
                      $('#modal_cancelar').modal('hide');  
                      aviso('danger', data.mensaje, "NO ES POSIBLE CANCELAR EL TURNO");    
                      break;

                    // Estado del pedido
                    case 'estado':
                      $('#modal_cancelar').modal('hide');  
                      aviso('danger', data.mensaje, "NO ES POSIBLE CANCELAR EL PEDIDO");    
                      break;

                    // No se ingreso comentario
                    case 'comentario':
                      $('#cancelar_error_message').html(data.mensaje);
                      $('#div_cancelar_error').show();      
                      break;  

                    default:    
                      $('#cancelar_error_message').html(data.mensaje);
                      $('#div_cancelar_error').show();      
                      break;                                      
                }                                           
            }

            $('#btn_cancelar').html('Cancelar');
            $('#btn_cancelar').attr('disabled', false);              

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_cancelar').modal('hide');  
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_cancelar').html('Cancelar');
            $('#btn_cancelar').attr('disabled', false);                   
        }
    });
}


function save_observaciones_turno()
{
    $('#btnSaveObservaciones').text('Guardando...'); 
    $('#btnSaveObservaciones').attr('disabled',true); 


    var url = "<?php echo BASE_PATH ?>/Turno/ajax_update_observaciones";

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_observaciones_turno').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            $('#btnSaveObservaciones').text('Aceptar');
            $('#btnSaveObservaciones').attr('disabled',false);

            $('#modal_detalle_turno').modal('hide');  

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_detalle_turno').modal('hide');  
            aviso('danger', textStatus, 'Error al ingresar mensaje opcional: (' + errorThrown + ')'); 

            $('#btnSaveObservaciones').text('Aceptar');
            $('#btnSaveObservaciones').attr('disabled',false); 
        }
    });
}


function save_comentario_venta()
{
    // Si el local ingresó un nuevo comentario, se guarda.
    if ($('#venta_comentario').val().trim() != '')
    {
        $('#btnSaveComentarioVenta').text('Guardando...'); 
        $('#btnSaveComentarioVenta').attr('disabled',true); 


        var url = "<?php echo BASE_PATH ?>/Venta/ajax_add_comentario_local";

        $.ajax({
            url : url ,
            type: "POST",
            data: $('#form_comentario_venta').serialize(),
            dataType: "JSON",
            success: function(data)
            {
                $('#btnSaveComentarioVenta').text('Aceptar');
                $('#btnSaveComentarioVenta').attr('disabled',false);

                $('#modal_detalle_venta').modal('hide');  

                 aviso('success', 'Mensaje enviado.');                   

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                $('#modal_detalle_venta').modal('hide');  
                aviso('danger', textStatus, 'Error al ingresar mensaje: (' + errorThrown + ')'); 

                $('#btnSaveComentarioVenta').text('Aceptar');
                $('#btnSaveComentarioVenta').attr('disabled',false); 
            }
        });     
    }
    else
    {
      $('#modal_detalle_venta').modal('hide');  
    }
}


// Si el local ingresó un nuevo comentario, cambia el botón 'Aceptar' por 'Enviar'
$('#venta_comentario').on('input',function(e){
    
    if ($('#venta_comentario').val().trim() != '')
        $('#btnSaveComentarioVenta').text('Enviar'); 
    else
        $('#btnSaveComentarioVenta').text('Aceptar');    
});


function mostrar_horarios_clinica()
{
    $('#tab-turnos-horarios').show();

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Local/ajax_get_horario_servicio/clinica",        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            if(data)
            {
              // Lunes
              abre_lu = (data.lu == '1' ? true : false);
              $('[name="abre_lu_clinica"]').prop('checked', abre_lu);   
              if (data.lu_h_1 != 0) $('[name="lu_h_1_clinica"]').val(data.lu_h_1);
              if (data.lu_m_1 != 0) $('[name="lu_m_1_clinica"]').val(data.lu_m_1);
              if (data.lu_h_2 != 0) $('[name="lu_h_2_clinica"]').val(data.lu_h_2);
              if (data.lu_m_2 != 0) $('[name="lu_m_2_clinica"]').val(data.lu_m_2);
              if (data.lu_h_3 != 0) $('[name="lu_h_3_clinica"]').val(data.lu_h_3);
              if (data.lu_m_3 != 0) $('[name="lu_m_3_clinica"]').val(data.lu_m_3);
              if (data.lu_h_4 != 0) $('[name="lu_h_4_clinica"]').val(data.lu_h_4);
              if (data.lu_m_4 != 0) $('[name="lu_m_4_clinica"]').val(data.lu_m_4);

              // Martes
              abre_ma = (data.ma == '1' ? true : false);
              $('[name="abre_ma_clinica"]').prop('checked', abre_ma);   
              if (data.ma_h_1 != 0) $('[name="ma_h_1_clinica"]').val(data.ma_h_1);
              if (data.ma_m_1 != 0) $('[name="ma_m_1_clinica"]').val(data.ma_m_1);
              if (data.ma_h_2 != 0) $('[name="ma_h_2_clinica"]').val(data.ma_h_2);
              if (data.ma_m_2 != 0) $('[name="ma_m_2_clinica"]').val(data.ma_m_2);
              if (data.ma_h_3 != 0) $('[name="ma_h_3_clinica"]').val(data.ma_h_3);
              if (data.ma_m_3 != 0) $('[name="ma_m_3_clinica"]').val(data.ma_m_3);
              if (data.ma_h_4 != 0) $('[name="ma_h_4_clinica"]').val(data.ma_h_4);
              if (data.ma_m_4 != 0) $('[name="ma_m_4_clinica"]').val(data.ma_m_4);

              // Miercoles
              abre_mi = (data.mi == '1' ? true : false);
              $('[name="abre_mi_clinica"]').prop('checked', abre_mi);   
              if (data.mi_h_1 != 0) $('[name="mi_h_1_clinica"]').val(data.mi_h_1);
              if (data.mi_m_1 != 0) $('[name="mi_m_1_clinica"]').val(data.mi_m_1);
              if (data.mi_h_2 != 0) $('[name="mi_h_2_clinica"]').val(data.mi_h_2);
              if (data.mi_m_2 != 0) $('[name="mi_m_2_clinica"]').val(data.mi_m_2);
              if (data.mi_h_3 != 0) $('[name="mi_h_3_clinica"]').val(data.mi_h_3);
              if (data.mi_m_3 != 0) $('[name="mi_m_3_clinica"]').val(data.mi_m_3);
              if (data.mi_h_4 != 0) $('[name="mi_h_4_clinica"]').val(data.mi_h_4);
              if (data.mi_m_4 != 0) $('[name="mi_m_4_clinica"]').val(data.mi_m_4);     

              // Jueves
              abre_ju = (data.ju == '1' ? true : false);
              $('[name="abre_ju_clinica"]').prop('checked', abre_ju);   
              if (data.ju_h_1 != 0) $('[name="ju_h_1_clinica"]').val(data.ju_h_1);
              if (data.ju_m_1 != 0) $('[name="ju_m_1_clinica"]').val(data.ju_m_1);
              if (data.ju_h_2 != 0) $('[name="ju_h_2_clinica"]').val(data.ju_h_2);
              if (data.ju_m_2 != 0) $('[name="ju_m_2_clinica"]').val(data.ju_m_2);
              if (data.ju_h_3 != 0) $('[name="ju_h_3_clinica"]').val(data.ju_h_3);
              if (data.ju_m_3 != 0) $('[name="ju_m_3_clinica"]').val(data.ju_m_3);
              if (data.ju_h_4 != 0) $('[name="ju_h_4_clinica"]').val(data.ju_h_4);
              if (data.ju_m_4 != 0) $('[name="ju_m_4_clinica"]').val(data.ju_m_4);

              // Viernes
              abre_vi = (data.vi == '1' ? true : false);
              $('[name="abre_vi_clinica"]').prop('checked', abre_vi);   
              if (data.vi_h_1 != 0) $('[name="vi_h_1_clinica"]').val(data.vi_h_1);
              if (data.vi_m_1 != 0) $('[name="vi_m_1_clinica"]').val(data.vi_m_1);
              if (data.vi_h_2 != 0) $('[name="vi_h_2_clinica"]').val(data.vi_h_2);
              if (data.vi_m_2 != 0) $('[name="vi_m_2_clinica"]').val(data.vi_m_2);
              if (data.vi_h_3 != 0) $('[name="vi_h_3_clinica"]').val(data.vi_h_3);
              if (data.vi_m_3 != 0) $('[name="vi_m_3_clinica"]').val(data.vi_m_3);
              if (data.vi_h_4 != 0) $('[name="vi_h_4_clinica"]').val(data.vi_h_4);
              if (data.vi_m_4 != 0) $('[name="vi_m_4_clinica"]').val(data.vi_m_4);

              // Sabado        
              abre_sa = (data.sa == '1' ? true : false);
              $('[name="abre_sa_clinica"]').prop('checked', abre_sa);   
              if (data.sa_h_1 != 0) $('[name="sa_h_1_clinica"]').val(data.sa_h_1);
              if (data.sa_m_1 != 0) $('[name="sa_m_1_clinica"]').val(data.sa_m_1);
              if (data.sa_h_2 != 0) $('[name="sa_h_2_clinica"]').val(data.sa_h_2);
              if (data.sa_m_2 != 0) $('[name="sa_m_2_clinica"]').val(data.sa_m_2);
              if (data.sa_h_3 != 0) $('[name="sa_h_3_clinica"]').val(data.sa_h_3);
              if (data.sa_m_3 != 0) $('[name="sa_m_3_clinica"]').val(data.sa_m_3);
              if (data.sa_h_4 != 0) $('[name="sa_h_4_clinica"]').val(data.sa_h_4);
              if (data.sa_m_4 != 0) $('[name="sa_m_4_clinica"]').val(data.sa_m_4);            

              // Domingo
              abre_do = (data.do == '1' ? true : false);
              $('[name="abre_do_clinica"]').prop('checked', abre_do);   
              if (data.do_h_1 != 0) $('[name="do_h_1_clinica"]').val(data.do_h_1);
              if (data.do_m_1 != 0) $('[name="do_m_1_clinica"]').val(data.do_m_1);
              if (data.do_h_2 != 0) $('[name="do_h_2_clinica"]').val(data.do_h_2);
              if (data.do_m_2 != 0) $('[name="do_m_2_clinica"]').val(data.do_m_2);
              if (data.do_h_3 != 0) $('[name="do_h_3_clinica"]').val(data.do_h_3);
              if (data.do_m_3 != 0) $('[name="do_m_3_clinica"]').val(data.do_m_3);
              if (data.do_h_4 != 0) $('[name="do_h_4_clinica"]').val(data.do_h_4);
              if (data.do_m_4 != 0) $('[name="do_m_4_clinica"]').val(data.do_m_4);              
            }
  
            crear_calendario('clinica');

            $('#div_horarios_peluqueria').hide();
            $('#div_horarios_clinica').show();

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });
}


function mostrar_horarios_peluqueria()
{
    $.ajax({
        url : "<?php echo BASE_PATH ?>/Local/ajax_get_horario_servicio/peluqueria",        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            if(data)
            {
              // Lunes
              abre_lu = (data.lu == '1' ? true : false);
              $('[name="abre_lu_peluqueria"]').prop('checked', abre_lu);   
              if (data.lu_h_1 != 0) $('[name="lu_h_1_peluqueria"]').val(data.lu_h_1);
              if (data.lu_m_1 != 0) $('[name="lu_m_1_peluqueria"]').val(data.lu_m_1);
              if (data.lu_h_2 != 0) $('[name="lu_h_2_peluqueria"]').val(data.lu_h_2);
              if (data.lu_m_2 != 0) $('[name="lu_m_2_peluqueria"]').val(data.lu_m_2);
              if (data.lu_h_3 != 0) $('[name="lu_h_3_peluqueria"]').val(data.lu_h_3);
              if (data.lu_m_3 != 0) $('[name="lu_m_3_peluqueria"]').val(data.lu_m_3);
              if (data.lu_h_4 != 0) $('[name="lu_h_4_peluqueria"]').val(data.lu_h_4);
              if (data.lu_m_4 != 0) $('[name="lu_m_4_peluqueria"]').val(data.lu_m_4);

              // Martes
              abre_ma = (data.ma == '1' ? true : false);
              $('[name="abre_ma_peluqueria"]').prop('checked', abre_ma);   
              if (data.ma_h_1 != 0) $('[name="ma_h_1_peluqueria"]').val(data.ma_h_1);
              if (data.ma_m_1 != 0) $('[name="ma_m_1_peluqueria"]').val(data.ma_m_1);
              if (data.ma_h_2 != 0) $('[name="ma_h_2_peluqueria"]').val(data.ma_h_2);
              if (data.ma_m_2 != 0) $('[name="ma_m_2_peluqueria"]').val(data.ma_m_2);
              if (data.ma_h_3 != 0) $('[name="ma_h_3_peluqueria"]').val(data.ma_h_3);
              if (data.ma_m_3 != 0) $('[name="ma_m_3_peluqueria"]').val(data.ma_m_3);
              if (data.ma_h_4 != 0) $('[name="ma_h_4_peluqueria"]').val(data.ma_h_4);
              if (data.ma_m_4 != 0) $('[name="ma_m_4_peluqueria"]').val(data.ma_m_4);

              // Miercoles
              abre_mi = (data.mi == '1' ? true : false);
              $('[name="abre_mi_peluqueria"]').prop('checked', abre_mi);   
              if (data.mi_h_1 != 0) $('[name="mi_h_1_peluqueria"]').val(data.mi_h_1);
              if (data.mi_m_1 != 0) $('[name="mi_m_1_peluqueria"]').val(data.mi_m_1);
              if (data.mi_h_2 != 0) $('[name="mi_h_2_peluqueria"]').val(data.mi_h_2);
              if (data.mi_m_2 != 0) $('[name="mi_m_2_peluqueria"]').val(data.mi_m_2);
              if (data.mi_h_3 != 0) $('[name="mi_h_3_peluqueria"]').val(data.mi_h_3);
              if (data.mi_m_3 != 0) $('[name="mi_m_3_peluqueria"]').val(data.mi_m_3);
              if (data.mi_h_4 != 0) $('[name="mi_h_4_peluqueria"]').val(data.mi_h_4);
              if (data.mi_m_4 != 0) $('[name="mi_m_4_peluqueria"]').val(data.mi_m_4);     

              // Jueves
              abre_ju = (data.ju == '1' ? true : false);
              $('[name="abre_ju_peluqueria"]').prop('checked', abre_ju);   
              if (data.ju_h_1 != 0) $('[name="ju_h_1_peluqueria"]').val(data.ju_h_1);
              if (data.ju_m_1 != 0) $('[name="ju_m_1_peluqueria"]').val(data.ju_m_1);
              if (data.ju_h_2 != 0) $('[name="ju_h_2_peluqueria"]').val(data.ju_h_2);
              if (data.ju_m_2 != 0) $('[name="ju_m_2_peluqueria"]').val(data.ju_m_2);
              if (data.ju_h_3 != 0) $('[name="ju_h_3_peluqueria"]').val(data.ju_h_3);
              if (data.ju_m_3 != 0) $('[name="ju_m_3_peluqueria"]').val(data.ju_m_3);
              if (data.ju_h_4 != 0) $('[name="ju_h_4_peluqueria"]').val(data.ju_h_4);
              if (data.ju_m_4 != 0) $('[name="ju_m_4_peluqueria"]').val(data.ju_m_4);

              // Viernes
              abre_vi = (data.vi == '1' ? true : false);
              $('[name="abre_vi_peluqueria"]').prop('checked', abre_vi);   
              if (data.vi_h_1 != 0) $('[name="vi_h_1_peluqueria"]').val(data.vi_h_1);
              if (data.vi_m_1 != 0) $('[name="vi_m_1_peluqueria"]').val(data.vi_m_1);
              if (data.vi_h_2 != 0) $('[name="vi_h_2_peluqueria"]').val(data.vi_h_2);
              if (data.vi_m_2 != 0) $('[name="vi_m_2_peluqueria"]').val(data.vi_m_2);
              if (data.vi_h_3 != 0) $('[name="vi_h_3_peluqueria"]').val(data.vi_h_3);
              if (data.vi_m_3 != 0) $('[name="vi_m_3_peluqueria"]').val(data.vi_m_3);
              if (data.vi_h_4 != 0) $('[name="vi_h_4_peluqueria"]').val(data.vi_h_4);
              if (data.vi_m_4 != 0) $('[name="vi_m_4_peluqueria"]').val(data.vi_m_4);

              // Sabado        
              abre_sa = (data.sa == '1' ? true : false);
              $('[name="abre_sa_peluqueria"]').prop('checked', abre_sa);   
              if (data.sa_h_1 != 0) $('[name="sa_h_1_peluqueria"]').val(data.sa_h_1);
              if (data.sa_m_1 != 0) $('[name="sa_m_1_peluqueria"]').val(data.sa_m_1);
              if (data.sa_h_2 != 0) $('[name="sa_h_2_peluqueria"]').val(data.sa_h_2);
              if (data.sa_m_2 != 0) $('[name="sa_m_2_peluqueria"]').val(data.sa_m_2);
              if (data.sa_h_3 != 0) $('[name="sa_h_3_peluqueria"]').val(data.sa_h_3);
              if (data.sa_m_3 != 0) $('[name="sa_m_3_peluqueria"]').val(data.sa_m_3);
              if (data.sa_h_4 != 0) $('[name="sa_h_4_peluqueria"]').val(data.sa_h_4);
              if (data.sa_m_4 != 0) $('[name="sa_m_4_peluqueria"]').val(data.sa_m_4);            

              // Domingo
              abre_do = (data.do == '1' ? true : false);
              $('[name="abre_do_peluqueria"]').prop('checked', abre_do);   
              if (data.do_h_1 != 0) $('[name="do_h_1_peluqueria"]').val(data.do_h_1);
              if (data.do_m_1 != 0) $('[name="do_m_1_peluqueria"]').val(data.do_m_1);
              if (data.do_h_2 != 0) $('[name="do_h_2_peluqueria"]').val(data.do_h_2);
              if (data.do_m_2 != 0) $('[name="do_m_2_peluqueria"]').val(data.do_m_2);
              if (data.do_h_3 != 0) $('[name="do_h_3_peluqueria"]').val(data.do_h_3);
              if (data.do_m_3 != 0) $('[name="do_m_3_peluqueria"]').val(data.do_m_3);
              if (data.do_h_4 != 0) $('[name="do_h_4_peluqueria"]').val(data.do_h_4);
              if (data.do_m_4 != 0) $('[name="do_m_4_peluqueria"]').val(data.do_m_4);              
            }

            crear_calendario('peluqueria');

            $('#div_horarios_clinica').hide();
            $('#div_horarios_peluqueria').show();

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });
}


function mostrar_turnos_activos()
{
   $('#tab-turnos-horarios').hide(); 
}


function mostrar_servicios_local()
{
    $('#tab-turnos-horarios').hide();

    // Limpia lista anterior
    $("#lista_servicios").html('');

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Servicio/ajax_list_items/",        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            servicios = data.servicios;

            if (servicios.length > 0)
            {
                // Lista de servicios
                for (i = 0; i < servicios.length; ++i) {

                   // Items de cada servicio
                   htmlItems = '';

                   if (servicios[i].items){

                      for (j = 0; j < servicios[i].items.length; ++j) {

                        checked = '';
                        selected_30 = '';
                        selected_60 = '';
                        selected_90 = '';
                        selected_120 = '';

                        // Valida si el item está asociado al local
                        if (servicios[i].items[j].local_servicio_item_id){

                            checked = 'checked = "checked"';

                            if (servicios[i].items[j].duracion == '30') selected_30 = 'selected';
                            if (servicios[i].items[j].duracion == '60') selected_60 = 'selected';
                            if (servicios[i].items[j].duracion == '90') selected_90 = 'selected';
                            if (servicios[i].items[j].duracion == '120') selected_120 = 'selected';
                        }

                         htmlItems += 
                            '<div class="push-10-l push-10-t">' + 
                              '<div class="col-xs-12 col-sm-3">' + 
                                  '<input type="checkbox" ' + checked + '" name="chkServicioItem_' + servicios[i].items[j].id + '" id="chkServicioItem_' + servicios[i].items[j].id + '" class="chkServicioItem pull-left" duracion="15" precio="" item_id="' + servicios[i].items[j].id + '">' + 
                                  '<p class="pull-left push-10-l"><strong>' + servicios[i].items[j].nombre + '</strong></p>' + 
                              '</div>' + 
                              '<div class="col-xs-12 col-sm-5 push-10">Duración del turno&nbsp;&nbsp;' + 
                                '<select style="width:130px" name="duracion_' + servicios[i].items[j].id + '" id="duracion_' + servicios[i].items[j].id + '" item_id="' + servicios[i].items[j].id + '">' +
                                  '<option value="30" ' + selected_30 + '>30 minutos</option>' +
                                  '<option value="60" ' + selected_60 + '>60 minutos</option>' +
                                  '<option value="90" ' + selected_90 + '>90 minutos</option>' +
                                  '<option value="120" ' + selected_120 + '>120 minutos</option>' +
                                '</select>' +
                              '</div>' +     
                              '<div class="col-xs-12 col-sm-4 push-20">Precio $&nbsp;&nbsp;' + 
                                  '<input value="' + servicios[i].items[j].precio + '" name="precio_' + servicios[i].items[j].id + '" id="precio_' + servicios[i].items[j].id + '" style="width:80px" type="text" item_id="' + servicios[i].items[j].id + '">' +
                              '</div>' +                                 
                            '</div>' +     
                            '<div class="clearfix"></div>';                
                      }
                   }

                   $("#lista_servicios").append(
                      '<li class="local-box">' +
                          '<div class="push-5-l">' + 
                              '<h4>' + servicios[i].nombre + '</h4>' + 
                              htmlItems + 
                          '</div>' +                                                                                    
                      '</li>');
                }
            }



        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });
}


function mostrar_mascotas_local(){

    tab = 'vacunas';

    //datatables
    table_mascotas = $('#table_mascotas')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 

        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés mascotas asociadas al local.",
        },

        "lengthChange": false, 
        "paging": false,
        "filter": false,
        "ordering": false,
        "info": false,

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.


        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Mascota/ajax_list_local",
            "type": "POST"
        },  
        "columnDefs": [   
        {
            "targets": [1,2,3], 
            "className": "hidden-xs", 
        }
        ],

    });

}


function cartilla_mascota(mascota_id)
{
    $("#mascota_foto").html('');
    $('#div_asignar_mascota_error').hide();
    mascota_actual = mascota_id;
    var foto = '';

    // Datos mascota
    $.ajax({
      url : "<?php echo BASE_PATH ?>/Mascota/get_by_id/" + mascota_id,
      cache: false,
      success: function(data){

        mascota = jQuery.parseJSON(data);

        if (mascota.foto)
          foto = mascota.foto;
        else if (mascota.tipo.toLowerCase() == 'gato')
          foto = 'gato.png';
        else
          foto = 'perro.png';

        $("#mascota_foto").append('<img src="<?php echo BASE_PATH ?>/assets/img/mascotas/miniaturas/' + foto + '" style="max-width: 180px;">');

        $('.modal-title-mascota').html('<i class="si si-notebook"></i> Cartilla de vacunas de ' + mascota.nombre);
        $('#mascota_nombre').text(mascota.nombre);
        $('#mascota_duenio').text(mascota.cliente);
        $('#mascota_tipo_sexo').text(mascota.tipo + ', ' + mascota.sexo);
        $('#mascota_nacimiento').text(mascota.nacimiento);
        $('#mascota_raza').text(mascota.raza);
        $('#mascota_pelaje').text(mascota.pelaje);
        $('#mascota_talla').text(mascota.talla);
        $('#mascota_proxima_vacuna').text(mascota.proxima_vacuna);
        
        // Muestra vacunas 
        mostrar_vacunas(mascota_id);

      } 
    });
}



function mostrar_vacunas(mascota_id)
{

  table_vacunas = $('#table_vacunas')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
        "paging": false,
        "filter": false,
        "info": false,
        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés vacunas asignadas a esta mascota.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "ordering": false,
        "pageLength": 25,        

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Vacuna/ajax_vacunas_mascota_local/" + mascota_id,
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, 
            "className": "hidden-xs", "targets": [2],             
        },
        ], 

    });
}


function detalle_vacuna(vacuna_id)
{
    $.ajax({
      url : "<?php echo BASE_PATH ?>/vacuna/get_by_id/" + vacuna_id,
      cache: false,
      success: function(data){

        vacuna = jQuery.parseJSON(data);

        // Datos de la vacuna
        $('#vacuna_nombre').text(vacuna.nombre);
        $('#vacuna_mascota').text(vacuna.mascota);
        $('#vacuna_fecha').text(vacuna.fecha);
        $('#vacuna_local').text(vacuna.local);

        // Datos adicionales               
        $('#vacuna_aplicada').text(vacuna.aplicada);
        $('#vacuna_veterinario').text(vacuna.veterinario);
        $('#vacuna_proxima_vacuna').text(vacuna.fecha_proxima);

        if (vacuna.peso != '0') $('#vacuna_peso').text(vacuna.peso);
        
        $('#vacuna_observaciones').text(vacuna.observaciones); 
      
      } 
    });

}


// Permite mostrar un modal sobre otro
// Es necesario que el modal que se muestra sobre otro modal esté ANTES en el html
$('#modal_detalle_vacuna').on('show.bs.modal', function () 
{
  $('#modal_cartilla_mascota').removeClass('muestra-modal');
  $('#modal_cartilla_mascota').addClass('oculta-modal');

})

$('#modal_detalle_vacuna').on('hide.bs.modal', function () 
{
  $('#modal_cartilla_mascota').removeClass('oculta-modal');
  $('#modal_cartilla_mascota').addClass('muestra-modal');

})


$('#modal_add_edit_vacuna').on('show.bs.modal', function () 
{
  $('#modal_cartilla_mascota').removeClass('muestra-modal');
  $('#modal_cartilla_mascota').addClass('oculta-modal');

})


$('#modal_add_edit_vacuna').on('hidden.bs.modal', function () 
{
  $('#modal_cartilla_mascota').removeClass('oculta-modal');
  $('#modal_cartilla_mascota').addClass('muestra-modal');

})


// Modal de imagenes al crear / editar producto del local
$('#modal_imagenes_producto_local').on('show.bs.modal', function () 
{
  $('#modal_producto_local').removeClass('muestra-modal');
  $('#modal_producto_local').addClass('oculta-modal');

})


$('#modal_imagenes_producto_local').on('hidden.bs.modal', function () 
{
  $('#modal_producto_local').removeClass('oculta-modal');
  $('#modal_producto_local').addClass('muestra-modal');

})
// FIN Permite mostrar un modal sobre otro


function add_vacuna()
{
    save_vacuna_method = 'add';

    $('#form_add_edit_vacuna')[0].reset(); 
    $('#div_add_edit_vacuna_error').hide();
    $('.modal-title-add-edit-vacuna').text('Nueva vacuna');

    $('#vacuna_mascota_id').val(mascota_actual);

}


function modificar_vacuna(id)
{
    save_vacuna_method = 'edit';

    $('#form_add_edit_vacuna')[0].reset(); 
    $('#div_add_edit_vacuna_error').hide();
    $('.modal-title-add-edit-vacuna').text('Modificar vacuna');

    $('#vacuna_mascota_id').val(mascota_actual);

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Vacuna/get_by_id/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('[name="id_vacuna"]').val(data.id);
            $('[name="nombre_vacuna"]').val(data.nombre);
            $('[name="fecha_vacuna"]').val(data.fecha);
            $('[name="fecha_proxima_vacuna"]').val(data.fecha_proxima);
            $('[name="veterinario_vacuna"]').val(data.veterinario);
            $('[name="observaciones_vacuna"]').val(data.observaciones);

            if (data.peso != '0') $('[name="peso_vacuna"]').val(data.peso);

            // Aplicada
            $('#vacuna_aplicada_null').prop('checked', true);   

            if (data.aplicada == 'si') $('#vacuna_aplicada_si').prop('checked', true);    
            if (data.aplicada == 'no') $('#vacuna_aplicada_no').prop('checked', true);  

            $('.modal-title').text('Modificar Vacuna');
            $('#modal_add_edit_vacuna').modal('show');  

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS DE LA VACUNA"); 
        }
    });

   
}


function save_vacuna()
{
    var url;

    if(save_vacuna_method == 'add') {
        url = "<?php echo BASE_PATH ?>/Vacuna/ajax_add";
        mensaje = 'Vacuna correctamente agregada.';
    } else {
        url = "<?php echo BASE_PATH ?>/Vacuna/ajax_update";
        mensaje = 'Los datos de la vacuna fueron correctamente modificados.';
    }

    $('#btn_add_edit_vacuna').html('Guardando...');
    $('#btn_add_edit_vacuna').attr('disabled', true); 

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_add_edit_vacuna').serialize(),
        dataType: "JSON",          
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_add_edit_vacuna_error').hide();   
                $('#modal_add_edit_vacuna').modal('hide');

                // Recarga lista de mascotas y cartilla
                $('#mascota_proxima_vacuna').text(data.proxima_vacuna);
                table_mascotas.ajax.reload(null,false);   
                table_vacunas.ajax.reload(null,false);   

                aviso('success', mensaje);                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#add_edit_vacuna_error_message').html(mensaje_error);
                $('#div_add_edit_vacuna_error').show();                                                   
            }

            $('#btn_add_edit_vacuna').html('Guardar');
            $('#btn_add_edit_vacuna').attr('disabled', false);              

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_add_edit_vacuna').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_add_edit_vacuna').html('Guardar');
            $('#btn_add_edit_vacuna').attr('disabled', false);                   
        }
    });
}


function borrar_vacuna(nombre, id)
{
    if(confirm('¿Borrar vacuna "' + nombre + '"?'))
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/Vacuna/ajax_delete/" + id + "/" + mascota_actual,
            type: "POST",
            dataType: "JSON",

            success: function(data)
            {
                console.log('borrar: ' + data.proxima_vacuna);
                if(data.status) 
                {
                  // Recarga lista de mascotas y cartilla
                  $('#mascota_proxima_vacuna').text(data.proxima_vacuna);
                  table_mascotas.ajax.reload(null,false);   
                  table_vacunas.ajax.reload(null,false);   
                    
                    aviso('success', 'Vacuna borrada.');                 
                }
                else
                {
                    aviso('danger', data.mensaje, "ERROR AL BORRAR VACUNA");  
                    console.log(data.error);
                }
            },

            error: function (jqXHR, textStatus, errorThrown)
            {
                
                aviso('danger', textStatus, 'Error al borrar vacuna. ' + textStatus); 
            }
        });

    }
}


function detalle_vacuna_agendada(vacuna_id)
{
    $.ajax({
      url : "<?php echo BASE_PATH ?>/vacuna/get_by_id/" + vacuna_id,
      cache: false,
      success: function(data){

        vacuna = jQuery.parseJSON(data);

        // Datos de la vacuna
        $('#proxima_vacuna_nombre').text(vacuna.nombre);
        $('#proxima_vacuna_fecha').text(vacuna.fecha_proxima);
        $('#proxima_vacuna_mascota').text(vacuna.mascota);

        // Datos del cliente
        $("#proxima_vacuna_cliente_avatar").html('');
        $("#proxima_vacuna_cliente_avatar").append('<img src="<?php echo BASE_PATH ?>/assets/img/cliente/miniaturas/' + vacuna.cliente_avatar + '" width="100%">');

        $('#proxima_vacuna_cliente_nombre').text(vacuna.cliente);
               
        $('#modal_proxima_vacuna').modal('show');                            

      } 
    });
}


function mostrar_datos_local()
{
    tab = 'datos';

    $('#form_local')[0].reset();

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Local/ajax_cargar/",        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#local-nombre').val(data.nombre);
            $('#local-titular-nombre').val(data.nombre_titular);
            $('#local-titular-apellido').val(data.apellido_titular);
            $('#local-telefono').val(data.telefono);
            $('#local-telefono2').val(data.telefono2);
            $('#local-email').val(data.email);
            $('#local-email2').val(data.email2);
            $('#local-horario').val(data.horario_atencion);
            $('#local-horario-sabados').val(data.horario_sabados);
            $('#local-horario-domingos').val(data.horario_domingos);
            $('#local-envio').val(data.costo_envio);
            $('#local-zona-entrega').val(data.zona_entrega);
            $('#local-dias-entrega').val(data.dias_entrega);
            $('#local-urgencias').val(data.urgencias);
            $('#local-telefono-urgencias').val(data.telefono_urgencias);
            $('#local-urgencias-consultorio').val(data.urgencias_consultorio);
            $('#local-urgencias-domicilio').val(data.urgencias_domicilio);
            $('#local-texto-banner').val(data.texto_banner);
            $('#local-texto-banner-hidden').val(data.texto_banner);
            $('#local-fondo-banner').val(data.fondo_banner);
            $('#local-color-banner').val(data.color_banner);

            // Envios
            if (data.envio_domicilio == 1) 
            {
                $('#switch_envio_domicilio').prop('checked', true);
                $('[name="envio-domicilio"]').val('1'); 
                $('.envio_domicilio').show();
            }
            else
            {
                $('#switch_envio_domicilio').prop('checked', false);
                $('[name="envio-domicilio"]').val('0'); 
                $('.envio_domicilio').hide();
            }

            // Urgencias
            data.urgencias == 1 ? $('.urgencias').show() : $('.urgencias').hide();
            data.urgencias_consultorio == 1 ? $('#chk-local-urgencias-consultorio').prop('checked', true) : $('#chk-local-urgencias-consultorio').prop('checked', false);
            data.urgencias_domicilio == 1 ? $('#chk-local-urgencias-domicilio').prop('checked', true) : $('#chk-local-urgencias-domicilio').prop('checked', false);

            // Colores banner
            // --------------

            // Fondo
            $('#local-fondo-banner').spectrum({   
                preferredFormat: "hex",
                showPaletteOnly: true,
                hideAfterPaletteSelect: true,
                move: function(color) {
                    cambiaColorFondoBanner(color);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(102, 102, 102)","rgb(255, 255, 255)"],
                    ["rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(0, 0, 255)", "rgb(255, 0, 255)"]
                ]
            });            

            // Texto
            $('#local-color-banner').spectrum({   
                preferredFormat: "hex",
                showPaletteOnly: true,
                hideAfterPaletteSelect: true,
                move: function(color) {
                    cambiaColorTextoBanner(color);
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(102, 102, 102)","rgb(255, 255, 255)"],
                    ["rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                    "rgb(0, 255, 255)", "rgb(0, 0, 255)", "rgb(255, 0, 255)"]
                ]
            });

            // Banner editable por el local
            /*
            if (data.descuentos_editable == '0'){
                $('#div_texto_banner').hide();
                $('#div_colores_banner').hide();
            }
            */

            // Vista previa banner
            $('#div_vista_previa_banner').html($('#local-texto-banner').val()); 
            $('#div_vista_previa_banner').css('color',  $('#local-color-banner').val());    
            $('#div_vista_previa_banner').css('background-color',  $('#local-fondo-banner').val());    

            // Aviso banner global o grupal
            if (data.banner_global || data.banner_grupal) $('#div_aviso_banner').show();

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });

}


function mostrar_medios_pago_local(mostrar_lista)
{
    $("#lista_medios_pago").html('');

    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Mediopago/ajax_medios_pago_local/" + local_id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            medios_pago = data.medios_pago;
            
            if (medios_pago.length > 0)
            {
                // Lista de medios_pago
                for (i = 0; i < medios_pago.length; ++i) 
                {
                    checked = '';
                    disabled = '';
                    css_disabled = '';
                    tiene_items = false;

                    // Valida si el medio de pago está asociado al local
                    if (medios_pago[i].local_medio_pago_id)
                    {
                        checked = 'checked = "checked" ';
                    }    

                    // No permite desactivar Mercado Pago
                    if (medios_pago[i].id == 1)
                    {
                        disabled = ' onclick="return false;" ';
                        css_disabled = ' css-input-disabled ';
                    }                                    

                   // Items de cada medio de pago
                   htmlItems = '';

                   if (medios_pago[i].items)
                   {
                        if (medios_pago[i].items.length > 0) tiene_items = true;

                      for (j = 0; j < medios_pago[i].items.length; ++j) 
                      {
                        checked_item = '';

                        // Valida si el item está asociado al local
                        if (medios_pago[i].items[j].local_medio_pago_id)
                        {
                            checked_item = 'checked = "checked"';
                        }

                         htmlItems += 
                            '<div class="push-10-l push-10-t">' + 
                              '<div class="col-xs-12 col-sm-3" style="height: 18px;">' + 
                                  '<label class="css-input css-checkbox css-checkbox-datos-tab ">'+
                                    '<input type="checkbox" ' + checked_item + '" name="chkMediopago_' + medios_pago[i].id + '_' + medios_pago[i].items[j].id + '" >' + 
                                      '<span class="push-5-r  "></span> ' + medios_pago[i].items[j].nombre + 
                                    '</label>' +                                    
                              '</div>' +                                 
                            '</div>' +     
                            '<div class="clearfix"></div>';                
                      }
                   }

                   if (tiene_items)
                   {
                        $("#lista_medios_pago").append(
                          '<li class="">' +
                              '<div class="push-5-l push-10 push-30-t">' + 
                                  '<h5>' + medios_pago[i].nombre + '</h5>' + 
                                  htmlItems + 
                              '</div>' +                                                                                    
                          '</li>');
                   }
                   else
                   {
                        $("#lista_medios_pago").append(
                          '<li class="">' +
                              '<div class="push-5-l push-10 push-20-t">' + 
                                '<label class="h5 css-input css-checkbox css-checkbox-datos-tab ' + css_disabled + ' ">'+ 
                                '<input type="checkbox" ' + checked + disabled + '" name="chkMediopago_' + medios_pago[i].id + '_0" >' +
                                  '<span class="push-5-r  "></span> ' + medios_pago[i].nombre + 
                                  '</label>' + 
                                  htmlItems + 
                              '</div>' +                                                                                    
                          '</li>');
                   }
                }
            }
        
        if (mostrar_lista == 'true') 
          $("#lista_medios_pago").show();
        else
          $("#lista_medios_pago").hide();          

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);               
        }
    });
}


$("#switch_envio_domicilio").change(function() 
{
    if(this.checked) {
        $('[name="envio-domicilio"]').val('1'); 
        $('.envio_domicilio').show();
    }
    else {
        $('[name="envio-domicilio"]').val('0'); 
        $('.envio_domicilio').hide();
    }
});


$("#chk-local-urgencias-consultorio").change(function() 
{
    if(this.checked) {
        $('[name="local-urgencias-consultorio"]').val('1'); 
    }
    else {
        $('[name="local-urgencias-consultorio"]').val('0'); 
    }
});


$("#chk-local-urgencias-domicilio").change(function() 
{
    if(this.checked) {
        $('[name="local-urgencias-domicilio"]').val('1'); 
    }
    else {
        $('[name="local-urgencias-domicilio"]').val('0'); 
    }
});


$("#chk_dia_turno_abierto").change(function() 
{
    if(this.checked) 
    {
        $('[name="hd_dia_turno_abierto"]').val('1'); 
        $('#ddl_horario1').show(); 
        $('#ddl_horario2').show(); 

        $('#ddl_horario1').multiselect({
            includeSelectAllOption: false
        });     

        $('#ddl_horario2').multiselect({
            includeSelectAllOption: false
        });                   

        $('#ddl_horario1').next().addClass('open');         
        $('#ddl_horario2').next().addClass('open');         
        $('.multiselect').hide();        
    }
    else 
    {
        $('[name="hd_dia_turno_abierto"]').val('0'); 

        $('#ddl_horario1').hide(); 
        $('#ddl_horario2').hide();          
    }
});


function actualizar_local()
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_update";

    $('#btn_actualizar_local').html('Guardando...');
    $('#btn_actualizar_local').attr('disabled', true); 

    $('#local-texto-banner-hidden').val($('#local-texto-banner').val());

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_local').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_local_error').hide();   
                       
                $('#btn_actualizar_local').html('Guardar Cambios');
                $('#btn_actualizar_local').attr('disabled', false);  

                aviso('success', 'Los datos de la tienda fueron correctamente actualizados.');                                    
                window.location.href ='<?php echo BASE_PATH ?>/dashboard/datos'                
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#local_error_message').html(mensaje_error);
                $('#div_local_error').show();      

                $('#btn_actualizar_local').html('Guardar Cambios');
                $('#btn_actualizar_local').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_actualizar_local').html('Guardar Cambios');
            $('#btn_actualizar_local').attr('disabled', false);                   
        }
    });
}


// Vista previa del banner
// -----------------------
// Texto
$('#local-texto-banner').change(function() {
  $('#div_vista_previa_banner').html($('#local-texto-banner').val()); 
});


// Color texto
function cambiaColorTextoBanner(color) {
    $('#div_vista_previa_banner').css('color',  color.toHexString());    
}

// Color fondo
function cambiaColorFondoBanner(color) {
    $('#div_vista_previa_banner').css('background-color',  color.toHexString());    
}


function actualizar_servicios()
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_update_servicios";


    $('#btn_actualizar_servicios').html('Guardando...');
    $('#btn_actualizar_servicios').attr('disabled', true); 


    // Recupera los servicios seleccionados
    var data = { 'servicio[]' : [], 'duracion[]' : [], 'precio[]' : []};

    $(".chkServicioItem:checked").each(function() {

        duracion = '#duracion_' + $(this).attr('item_id');
        precio = '#precio_' + $(this).attr('item_id');

        data['servicio[]'].push($(this).attr('item_id'));
        data['duracion[]'].push($(duracion).val());
        data['precio[]'].push($(precio).val());

    });
    
    $.ajax({
        url : url ,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_servicios_error').hide();   
                       
                mostrar_servicios_local();

                $('#btn_actualizar_servicios').html('Guardar Cambios');
                $('#btn_actualizar_servicios').attr('disabled', false);  

                aviso('success', 'Servicios correctamente actualizados.');                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#servicios_error_message').html(mensaje_error);
                $('#div_servicios_error').show();      

                $('#btn_actualizar_servicios').html('Guardar Cambios');
                $('#btn_actualizar_servicios').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_actualizar_servicios').html('Guardar Cambios');
            $('#btn_actualizar_servicios').attr('disabled', false);                   
        }
    });

}


function actualizar_horarios_clinica()
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_update_horarios_clinica";

    $('#btn_actualizar_horarios_clinica').html('Guardando...');
    $('#btn_actualizar_horarios_clinica').attr('disabled', true); 
    
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_horarios_clinica').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_horarios_error').hide();   
                       
                mostrar_horarios_clinica();

                $('#btn_actualizar_horarios_clinica').html('Guardar Cambios');
                $('#btn_actualizar_horarios_clinica').attr('disabled', false);  

                crear_calendario('clinica');

                aviso('success', 'Horarios Clínica correctamente actualizados.');                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#horarios_error_message_clinica').html(mensaje_error);
                $('#div_horarios_error_clinica').show();      

                $('#btn_actualizar_horarios_clinica').html('Guardar Cambios');
                $('#btn_actualizar_horarios_clinica').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_actualizar_horarios_clinica').html('Guardar Cambios');
            $('#btn_actualizar_horarios_clinica').attr('disabled', false);                   
        }
    });

}


function actualizar_horarios_peluqueria()
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_update_horarios_peluqueria";

    $('#btn_actualizar_horarios_peluqueria').html('Guardando...');
    $('#btn_actualizar_horarios_peluqueria').attr('disabled', true); 
    
    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_horarios_peluqueria').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_horarios_error').hide();   
                       
                mostrar_horarios_peluqueria();

                $('#btn_actualizar_horarios_peluqueria').html('Guardar Cambios');
                $('#btn_actualizar_horarios_peluqueria').attr('disabled', false);  

                crear_calendario('peluqueria');

                aviso('success', 'Horarios Peluquería correctamente actualizados.');                                    
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#horarios_error_message_peluqueria').html(mensaje_error);
                $('#div_horarios_error_peluqueria').show();      

                $('#btn_actualizar_horarios_peluqueria').html('Guardar Cambios');
                $('#btn_actualizar_horarios_peluqueria').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_actualizar_horarios_peluqueria').html('Guardar Cambios');
            $('#btn_actualizar_horarios_peluqueria').attr('disabled', false);                   
        }
    });

}


function mostrar_notificaciones_local()
{
  tab = 'notificaciones';

  table_notificaciones = $('#table_notificaciones')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_", 
            "emptyTable": "No tenés notificaciones.",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 25,

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_notificaciones_local",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable    
        },
        {
            "targets": [ 0,1 ], 
            "className": "hidden-xs"             
        }
        ],

        initComplete: function () {
            this.api().columns().every( function () {
                var column = this;

                // Habilita drop-down list de busqueda solo en segunda columna
                if (column.index() == 1){
                    var select = $('<select class="form-control hidden-xs" style="position: absolute; top: -39px; left: 140px"><option value="">TIPO</option></select>')
                        .appendTo( $(column.footer()).empty() )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? val : '', true, false )
                                .draw();

                        } );
     
                        select.append( '<option value="venta">Pedido</option>' );
                        select.append( '<option value="turno">Solicitud de turno</option>' );
                        select.append( '<option value="calificación venta">Calificación Venta</option>' );
                        select.append( '<option value="calificación turno">Calificación Servicio</option>' );
                        select.append( '<option value="mensaje venta">Mensajes de Pedidos</option>' );
                        select.append( '<option value="mensaje turno">Mensajes de Servicios</option>' );
                        select.append( '<option value="cancela venta">Pedido cancelado</option>' );
                        select.append( '<option value="cancela turno">Turno cancelado</option>' );
                        select.append( '<option value="recordatorio vacuna">Recordatorio vacuna</option>' );
                        select.append( '<option value="mascota asignada">Mascota asignada</option>' );
                }
            } );
        },  

    });
}


function mostrar_notificar(tipo, id){


    $('#form_notificar')[0].reset(); 
    tipo_pedido = tipo;

    if (tipo == 'venta')
      urlPedido = "<?php echo BASE_PATH ?>/admin/venta/get_venta_by_id/" + id;
    else
      urlPedido = "<?php echo BASE_PATH ?>/admin/turno/get_by_id/" + id;

    // Encabezado
    $.ajax({
      url : urlPedido,
      cache: false,
      success: function(data){

        pedido = jQuery.parseJSON(data);

        // Datos del pedido
        if (tipo == 'venta')
        {
            $('.modal-title-pedido-nro').text('Notificar Venta ' + pedido.numero);
            $('#label_avisar_concretado').text('¿Pudiste concretar la venta?');
        }
            
        else
        {
            $('.modal-title-pedido-nro').text('Notificar Turno ' + pedido.numero);
            $('#label_avisar_concretado').text('¿Pudiste concretar el servicio?');
        }        


        $('#avisar_id').val(id);
        $('#avisar_venta_nro').val(pedido.numero);
        $('#avisar_cliente_id').val(pedido.cliente_id);

        $('#modal_avisar').modal('show'); 

      } 
    });
}


function aceptar_avisar()
{
    if (tipo_pedido == 'venta')
      urlPedido = "<?php echo BASE_PATH ?>/Venta/ajax_avisar_venta";
    else
      urlPedido = "<?php echo BASE_PATH ?>/Turno/ajax_avisar_turno";

    $('#btn_notificar').html('Guardando...');
    $('#btn_notificar').attr('disabled', true); 

    $.ajax({
        url : urlPedido,
        type: "POST",
        data: $('#form_notificar').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            // Notificación ok
            if(data.status) 
            {
                $('#div_notificar_error').hide();      
                $('#btn_notificar').html('Notificar');
                $('#btn_notificar').attr('disabled', false); 

                $('#modal_avisar').modal('hide');
                
                if (table_pedidos) table_pedidos.ajax.reload(null,false);        
                if (table_ventas) table_ventas.ajax.reload(null,false);        
                if (table_turnos) table_turnos.ajax.reload(null,false);                       

                aviso('success', '¡Gracias! \n\n\nHemos recibido tu notificación.');     
            }
            // Fallo en Notificación
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#notificar_error_message').html(mensaje_error);
                $('#div_notificar_error').show();      

                $('#btn_notificar').html('Notificar');
                $('#btn_notificar').attr('disabled', false);                                               
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_avisar').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_notificar').html('Notificar');
            $('#btn_notificar').attr('disabled', false);                   
        }
    });
}


function show_modal_aumentar_exposicion()
{
    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Local/ajax_get_precio_destacado/",
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            $('#precio_local_destacado').html('$' + data.precio);
            $('#modal_aumentar_exposicion').modal('show'); 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            aviso('danger', textStatus, 'Error al obtener precio local destacado. '); 
        }
    });
}


function aceptar_aumentar_exposicion()
{
    url = "<?php echo BASE_PATH ?>/Local/ajax_aumentar_exposicion/" + local_id;

    $('#btn_aumentar_exposicion').html('Guardando...');
    $('#btn_aumentar_exposicion').attr('disabled', true); 

    $.ajax({
        url : url,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            // Notificación ok
            if(data.status) 
            {
                $('#btn_aumentar_exposicion').html('Confirmar');
                $('#btn_aumentar_exposicion').attr('disabled', false); 

                $('#modal_aumentar_exposicion').modal('hide');
                
                aviso('success', '¡Gracias! \n\n\nHemos recibido tu aviso para destacar tu tienda.');     
            }
        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_aumentar_exposicion').modal('hide');
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_aumentar_exposicion').html('Confirmar');
            $('#btn_aumentar_exposicion').attr('disabled', false); 
        }
    });
}

/* PRODUCTOS DEL LOCAL */
function mostrar_productos_local()
{
    tab = 'productos';

    $('.div_table_productos').hide();
    $('.div_cargando').show();

    // Productos activos
    table_productos = $('#table_productos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 10,        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_productos_list/1",
            "type": "POST"
        },

          //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 7, -1 ], //last column
            "orderable": false, //set not orderable   
        },   
        {
            "targets": [ 5, 8, 9, 10, 11 ],
            "visible": false
        }     
        ],

        initComplete: function() 
        {
            // Filtro por CREADOR
            $('#div_filtros_articulos_creador').html('');

            filtro_creador = $('<div><div class="filtro-tipo">TIPO</div></div>')
                .appendTo( $('#div_filtros_articulos_creador'));

            select_creador = $('<ul id="ddl_creador"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_creador'));

            // Filtro por ANIMAL
            $('#div_filtros_articulos_animal').html('');

            filtro_animal = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
                .appendTo( $('#div_filtros_articulos_animal'));

            select_animal= $('<ul id="ddl_animal"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_animal'));

            // Filtro por RUBRO
            $('#div_filtros_articulos_rubro').html('');

            filtro_rubro = $('<div><div class="filtro-tipo">RUBRO</div></div>')
                .appendTo( $('#div_filtros_articulos_rubro'));

            select_rubro= $('<ul id="ddl_rubro"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_rubro'));

            // Filtro por MARCA
            $('#div_filtros_articulos_marca').html('');

            filtro_marca = $('<div><div class="filtro-tipo">MARCA</div></div>')
                .appendTo( $('#div_filtros_articulos_marca'));

            select_marca= $('<ul id="ddl_marca"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_marca'));    

            // Filtro por RAZA
            $('#div_filtros_articulos_raza').html('');

            filtro_raza = $('<div><div class="filtro-tipo">RAZA</div></div>')
                .appendTo( $('#div_filtros_articulos_raza'));

            select_raza= $('<ul id="ddl_raza"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_raza'));                       
                
            // Filtro por EDAD
            $('#div_filtros_articulos_edad').html('');

            filtro_edad = $('<div><div class="filtro-tipo">EDAD</div></div>')
                .appendTo( $('#div_filtros_articulos_edad'));

            select_edad= $('<ul id="ddl_edad"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_edad'));      

            // Filtro por TAMAÑO
            $('#div_filtros_articulos_tamanio').html('');

            filtro_tamanio = $('<div><div class="filtro-tipo">TAMAÑO</div></div>')
                .appendTo( $('#div_filtros_articulos_tamanio'));

            select_tamanio= $('<ul id="ddl_tamanio"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_tamanio'));    

            // Filtro por PRESENTACION
            $('#div_filtros_articulos_presentacion').html('');

            filtro_presentacion = $('<div><div class="filtro-tipo">PRESENTACIÓN</div></div>')
                .appendTo( $('#div_filtros_articulos_presentacion'));

            select_presentacion= $('<ul id="ddl_presentacion"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_presentacion'));     

            // Filtro por MEDICADOS
            $('#div_filtros_articulos_medicados').html('');

            filtro_medicados = $('<div><div class="filtro-tipo">MEDICADOS</div></div>')
                .appendTo( $('#div_filtros_articulos_medicados'));

            select_medicados= $('<ul id="ddl_medicados"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_medicados'));    


            // Oculta inicialmente filtros que dependen del Rubro y Animal seleccionado
            $('#div_filtros_articulos_marca').hide();
            $('#div_filtros_articulos_raza').hide();
            $('#div_filtros_articulos_tamanio').hide();
            $('#div_filtros_articulos_edad').hide();
            $('#div_filtros_articulos_presentacion').hide();
            $('#div_filtros_articulos_medicados').hide();                        
            filtro_marca.hide();
            filtro_edad.hide();
            filtro_tamanio.hide();
            filtro_raza.hide();
            filtro_presentacion.hide();
            filtro_medicados.hide();
        },        
        drawCallback: function() 
        {
            if (drawCallback == 0)
            {
              // Inicializa filtros
              cuenta_filtro_creador = 0;
              cuenta_filtro_rubro = 0;
              cuenta_filtro_animal = 0;
              cuenta_filtro_marca = 0;
              cuenta_filtro_raza = 0;
              cuenta_filtro_tamanio = 0;
              cuenta_filtro_edad = 0;
              cuenta_filtro_presentacion = 0;
              cuenta_filtro_medicados = 0;

              $('#div_filtros_articulos_aplicados_items').html('');

              this.api().columns().every( function () 
              {
                  var column = this;

                  // Filtro por Creador (Tipo)
                  if (column.index() == 0)
                  {
                      if (filtros_aplicados_creador == '')
                      {                                
                          if (filtro_creador) filtro_creador.show();

                          if (select_creador)
                          {
                              select_creador.show();      
                              select_creador.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_creador/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id + "/false/1",
                             success: function(data)
                             {
                                 data_creador = jQuery.parseJSON(data);

                                 $.each(data_creador.creadores, function(indice, creador)
                                  {
                                      var option = '<li class="filtro-item" value="' + creador.id + '">' + creador.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_creador)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback = 0;                                              
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_creador = creador.nombre;
                                              filtros_aplicados_creador_id = creador.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                          } )   

                                      cuenta_filtro_creador++;
                                  });    

                             }
                         });

                          // Oculta filtros que dependen de creador
                          if (filtro_marca)
                          {
                              filtro_marca.hide();
                              select_marca.hide();
                          } 

                          if (filtro_raza)
                          {
                              filtro_raza.hide();
                              select_raza.hide();
                          }    

                          if (filtro_tamanio)
                          {
                              filtro_tamanio.hide();
                              select_tamanio.hide();
                          }  

                          if (filtro_edad)
                          {
                              filtro_edad.hide();
                              select_edad.hide();
                          }  

                          if (filtro_presentacion)
                          {
                              filtro_presentacion.hide();
                              select_presentacion.hide();
                          }     

                          if (filtro_medicados)
                          {
                              filtro_medicados.hide();
                              select_medicados.hide();
                          }                                                                     
                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_creador.hide();
                          select_creador.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_creador = $('<div />')
                              .html(filtros_aplicados_creador + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  $('.div_cargando').show();
                                  drawCallback = 0;
                                  // Reset del filtro actual                                    
                                  var val = '';

                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados--;
                                  filtros_aplicados_creador = '';
                                  filtros_aplicados_creador_id = -1;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                              }); 
                      }
                  }

                  // Filtro por ANIMAL
                  if (column.index() == 1)
                  {
                      if (filtros_aplicados_animal == '')
                      {                                
                          if (filtro_animal) filtro_animal.show();

                          if (select_animal)
                          {
                              select_animal.show();      
                              select_animal.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_animal/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_creador_id + "/false/1",
                             success: function(data)
                             {
                                 data_animal = jQuery.parseJSON(data);

                                 $.each(data_animal.animales, function(indice, animal)
                                  {
                                      var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_animal)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback = 0;                                              
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_animal = animal.nombre;
                                              filtros_aplicados_animal_id = animal.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                          } )   

                                      cuenta_filtro_animal++;
                                  });    

                             }
                         });

                          // Oculta filtros que dependen de Animal
                          if (filtro_marca)
                          {
                              filtro_marca.hide();
                              select_marca.hide();
                          } 

                          if (filtro_raza)
                          {
                              filtro_raza.hide();
                              select_raza.hide();
                          }    

                          if (filtro_tamanio)
                          {
                              filtro_tamanio.hide();
                              select_tamanio.hide();
                          }  

                          if (filtro_edad)
                          {
                              filtro_edad.hide();
                              select_edad.hide();
                          }  

                          if (filtro_presentacion)
                          {
                              filtro_presentacion.hide();
                              select_presentacion.hide();
                          }     

                          if (filtro_medicados)
                          {
                              filtro_medicados.hide();
                              select_medicados.hide();
                          }                                                                     
                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_animal.hide();
                          select_animal.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_animal = $('<div />')
                              .html(filtros_aplicados_animal + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback = 0;                                

                                  // Quita filtros aplicados que dependen de Animal
                                  var table = $('#table_productos').DataTable();
                                  table.columns(3).search( val ? val : '', true, false );
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );
                                  table.columns(11).search( val ? val : '', true, false );

                                  // Reset del filtro actual                                    
                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados--;
                                  filtros_aplicados_animal = '';
                                  filtros_aplicados_animal_id = -1;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                              }); 
                      }
                  }


                  // Filtro por RUBRO
                  if (column.index() == 2)
                  {
                      if (filtros_aplicados_rubro == '')
                      {                                
                          if (filtro_rubro) filtro_rubro.show();

                          if (select_rubro)
                          {
                              select_rubro.show();      
                              select_rubro.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_rubro/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_creador_id + "/false/1",
                             success: function(data)
                             {
                                 data_rubro = jQuery.parseJSON(data);

                                 $.each(data_rubro.rubros, function(indice, rubro)
                                  {
                                      var option = '<li class="filtro-item" value="' + rubro.id + '">' + rubro.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_rubro)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback = 0;
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_rubro = rubro.nombre;
                                              filtros_aplicados_rubro_id = rubro.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                          } )   

                                      cuenta_filtro_rubro++;
                                  }); 

                             }
                         });

                          // Oculta filtros que dependen de Rubro
                          if (filtro_marca)
                          {
                              filtro_marca.hide();
                              select_marca.hide();
                          } 

                          if (filtro_raza)
                          {
                              filtro_raza.hide();
                              select_raza.hide();
                          }    

                          if (filtro_tamanio)
                          {
                              filtro_tamanio.hide();
                              select_tamanio.hide();
                          }   

                          if (filtro_edad)
                          {
                              filtro_edad.hide();
                              select_edad.hide();
                          } 

                          if (filtro_presentacion)
                          {
                              filtro_presentacion.hide();
                              select_presentacion.hide();
                          } 

                          if (filtro_medicados)
                          {
                              filtro_medicados.hide();
                              select_medicados.hide();
                          } 

                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_rubro.hide();
                          select_rubro.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_rubro = $('<div />')
                              .html(filtros_aplicados_rubro + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback = 0;                                

                                  // Quita filtros aplicados que dependen de Rubro
                                  var table = $('#table_productos').DataTable();
                                  table.columns(3).search( val ? val : '', true, false );
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );
                                  table.columns(11).search( val ? val : '', true, false );
                                  filtros_aplicados_rubro = '';
                                  filtros_aplicados_rubro_id = -1;

                                  // Reset del filtro actual
                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados--;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                              });

                      }
                  }


                  // Filtro por MARCA
                  if (column.index() == 3)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'marca' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_marca == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_marca/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_marca = jQuery.parseJSON(data);

                                     if (data_marca.marcas)
                                     {
                                          $('#div_filtros_articulos_marca').show();
                                          if (filtro_marca) filtro_marca.show();

                                          if (select_marca)
                                          {
                                              select_marca.show();      
                                              select_marca.html('');
                                          } 

                                         $.each(data_marca.marcas, function(indice, marca)
                                          {
                                              var option = '<li class="filtro-item" value="' + marca.id + '">' + marca.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_marca)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_marca = marca.nombre;
                                                      filtros_aplicados_marca_id = marca.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_marca++;   
                                          });   

                                     }

                                     if (data_marca.marcas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_marca.hide();
                                          select_marca.hide();
                                     }                                      
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_marca.hide();
                              select_marca.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_marca = $('<div />')
                                  .html(filtros_aplicados_marca + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_marca = '';
                                      filtros_aplicados_marca_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          if (filtros_aplicados_marca != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_marca = '';
                              filtros_aplicados_marca_id = -1;
                          }                                
                      }                         
                  }  


                  //  Filtro por EDAD
                  if (column.index() == 7)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'edad' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_edad == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_edad/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_edad = jQuery.parseJSON(data);

                                     if (data_edad.edades)
                                     {
                                          $('#div_filtros_articulos_edad').show();
                                          if (filtro_edad) filtro_edad.show();

                                          if (select_edad)
                                          {
                                              select_edad.show();      
                                              select_edad.html('');
                                          } 

                                         $.each(data_edad.edades, function(indice, edad)
                                          {
                                              var option = '<li class="filtro-item" value="' + edad.nombre + '">' + edad.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_edad)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_edad = edad.nombre;
                                                      filtros_aplicados_edad_id = edad.nombre;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_edad++;   
                                          });   

                                     }
                                     
                                     if (data_edad.edades.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_edad.hide();
                                          select_edad.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_edad.hide();
                              select_edad.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_edad = $('<div />')
                                  .html(filtros_aplicados_edad + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_edad = '';
                                      filtros_aplicados_edad_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          // Si el filtro ya estaba aplicado, lo quita
                          if (filtros_aplicados_edad != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_edad = '';
                              filtros_aplicados_edad_id = -1;
                          }
                      }                       
                  }  


                  //  Filtro por TAMAÑO
                  if (column.index() == 8)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'tamanio' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_tamanio == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_tamanio/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_tamanio = jQuery.parseJSON(data);

                                     if (data_tamanio.tamanios)
                                     {
                                          $('#div_filtros_articulos_tamanio').show();
                                          if (filtro_tamanio) filtro_tamanio.show();

                                          if (select_tamanio)
                                          {
                                              select_tamanio.show();      
                                              select_tamanio.html('');
                                          } 

                                         $.each(data_tamanio.tamanios, function(indice, tamanio)
                                          {
                                              var option = '<li class="filtro-item" value="' + tamanio.id + '">' + tamanio.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_tamanio)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_tamanio = tamanio.nombre;
                                                      filtros_aplicados_tamanio_id = tamanio.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_tamanio++;   
                                          });   

                                     }
                                     
                                     if (data_tamanio.tamanios.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_tamanio.hide();
                                          select_tamanio.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_tamanio.hide();
                              select_tamanio.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_tamanio = $('<div />')
                                  .html(filtros_aplicados_tamanio + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_tamanio = '';
                                      filtros_aplicados_tamanio_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      }  
                      else
                      {
                          // Si el filtro ya estaba aplicado, lo quita
                          if (filtros_aplicados_tamanio != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_tamanio = '';
                              filtros_aplicados_tamanio_id = -1;
                          }
                      }                                         
                  } 


                  // Filtro por RAZA
                  if (column.index() == 9)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'raza' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_raza == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_raza/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_medicados_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_raza = jQuery.parseJSON(data);

                                     if (data_raza.razas)
                                     {
                                          $('#div_filtros_articulos_raza').show();
                                          if (filtro_raza) filtro_raza.show();

                                          if (select_raza)
                                          {
                                              select_raza.show();      
                                              select_raza.html('');
                                          } 

                                         $.each(data_raza.razas, function(indice, raza)
                                          {
                                              var option = '<li class="filtro-item" value="' + raza.id + '">' + raza.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_raza)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_raza = raza.nombre;
                                                      filtros_aplicados_raza_id = raza.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_raza++;   
                                          });   

                                     }

                                     if (data_raza.razas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_raza.hide();
                                          select_raza.hide();
                                     }                                   
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_raza.hide();
                              select_raza.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_raza = $('<div />')
                                  .html(filtros_aplicados_raza + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_raza = '';
                                      filtros_aplicados_raza_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      }   
                      else
                      {
                          // Si el filtro ya estaba aplicado, lo quita
                          if (filtros_aplicados_raza != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_raza = '';
                              filtros_aplicados_raza_id = -1;
                          }
                      }                                        
                  }   


                  //  Filtro por PRESENTACION
                  if (column.index() == 10)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'presentacion' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_presentacion == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_presentacion/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_medicados_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_presentacion = jQuery.parseJSON(data);

                                     if (data_presentacion.presentaciones)
                                     {
                                          $('#div_filtros_articulos_presentacion').show();
                                          if (filtro_presentacion) filtro_presentacion.show();

                                          if (select_presentacion)
                                          {
                                              select_presentacion.show();      
                                              select_presentacion.html('');
                                          } 

                                         $.each(data_presentacion.presentaciones, function(indice, presentacion)
                                          {
                                              var option = '<li class="filtro-item" value="' + presentacion.id + '">' + presentacion.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_presentacion)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_presentacion = presentacion.nombre;
                                                      filtros_aplicados_presentacion_id = presentacion.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_presentacion++;   
                                          });   

                                     }
                                     
                                     if (data_presentacion.presentaciones.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_presentacion.hide();
                                          select_presentacion.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_presentacion.hide();
                              select_presentacion.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_presentacion = $('<div />')
                                  .html(filtros_aplicados_presentacion + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_presentacion = '';
                                      filtros_aplicados_presentacion_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      }   
                      else
                      {
                          // Si el filtro ya estaba aplicado, lo quita
                          if (filtros_aplicados_presentacion != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_presentacion = '';
                              filtros_aplicados_presentacion_id = -1;
                          }
                      }                                        
                  }


                  //  Filtro por MEDICADOS
                  if (column.index() == 11)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'medicados' (Alimentos por ej)
                      if (filtros_aplicados_rubro != '' && filtros_aplicados_animal != '')
                      {
                          if (filtros_aplicados_medicados == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_medicados/" + filtros_aplicados_rubro_id + "/" + filtros_aplicados_animal_id + "/" + filtros_aplicados_marca_id  + "/" + filtros_aplicados_raza_id  + "/" + filtros_aplicados_tamanio_id + "/" + filtros_aplicados_edad_id + "/" + filtros_aplicados_presentacion_id + "/" + filtros_aplicados_creador_id + "/false/1",
                                 success: function(data)
                                 {
                                     data_medicados = jQuery.parseJSON(data);

                                     if (data_medicados.medicados)
                                     {
                                          $('#div_filtros_articulos_medicados').show();
                                          if (filtro_medicados) filtro_medicados.show();

                                          if (select_medicados)
                                          {
                                              select_medicados.show();      
                                              select_medicados.html('');
                                          } 

                                         $.each(data_medicados.medicados, function(indice, medicados)
                                          {
                                              var option = '<li class="filtro-item" value="' + medicados.id + '">' + medicados.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_medicados)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_medicados = medicados.nombre;
                                                      filtros_aplicados_medicados_id = medicados.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados == 1) $('#div_filtros_articulos_aplicados').show();
                                                  } )

                                                  cuenta_filtro_medicados++;   
                                          });   

                                     }
                                     else
                                     {
                                           // Oculta este filtro
                                          filtro_medicados.hide();
                                          select_medicados.hide();                                   
                                     }
                                     
                                     if (data_medicados.medicados.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_medicados.hide();
                                          select_medicados.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_medicados.hide();
                              select_medicados.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_medicados = $('<div />')
                                  .html('Medicados: ' + filtros_aplicados_medicados + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados--;
                                      filtros_aplicados_medicados = '';
                                      filtros_aplicados_medicados_id = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados == 0) $('#div_filtros_articulos_aplicados').hide();

                                  });
                          }                       
                      } 
                      else
                      {
                          // Si el filtro ya estaba aplicado, lo quita
                          if (filtros_aplicados_medicados != '')
                          {
                              cuenta_filtros_aplicados--;
                              filtros_aplicados_medicados = '';
                              filtros_aplicados_medicados_id = -1;
                          }
                      }                                          
                  }

              } );

              $('.div_cargando').hide();     
              
              drawCallback = 1;         
            }

        },  
    });

    // Productos inactivos
    table_productos_inactivos = $('#table_productos_inactivos')
      .on( 'error.dt', function ( e, settings, techNote, message ) {
        $('#modal_error_sistema').modal('show');
        console.log( 'Error al procesar DataTables: ', message );
      } )
      .DataTable({ 
    
        "language": {
            "lengthMenu": "Mostrar _MENU_",
        },
        "destroy": true,
        "processing": true, 
        "serverSide": true,
        "order": [], 
        "pageLength": 10,

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Local/ajax_productos_list/0",
            "type": "POST"
        },

          //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 7, -1 ], //last column
            "orderable": false, //set not orderable   
        },   
        {
            "targets": [ 5, 8, 9, 10, 11 ],
            "visible": false
        }       

        ],

        initComplete: function () 
        {
            // Filtro por CREADOR
            $('#div_filtros_articulos_creador_inactivos').html('');

            filtro_creador_inactivos = $('<div><div class="filtro-tipo">TIPO</div></div>')
                .appendTo( $('#div_filtros_articulos_creador_inactivos'));

            select_creador_inactivos = $('<ul id="ddl_creador_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_creador_inactivos'));

            // Filtro por ANIMAL
            $('#div_filtros_articulos_animal_inactivos').html('');

            filtro_animal_inactivos = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
                .appendTo( $('#div_filtros_articulos_animal_inactivos'));

            select_animal_inactivos= $('<ul id="ddl_animal_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_animal_inactivos'));

            // Filtro por RUBRO
            $('#div_filtros_articulos_rubro_inactivos').html('');

            filtro_rubro_inactivos = $('<div><div class="filtro-tipo">RUBRO</div></div>')
                .appendTo( $('#div_filtros_articulos_rubro_inactivos'));

            select_rubro_inactivos= $('<ul id="ddl_rubro_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_rubro_inactivos'));

            // Filtro por MARCA
            $('#div_filtros_articulos_marca_inactivos').html('');

            filtro_marca_inactivos = $('<div><div class="filtro-tipo">MARCA</div></div>')
                .appendTo( $('#div_filtros_articulos_marca_inactivos'));

            select_marca_inactivos= $('<ul id="ddl_marca_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_marca_inactivos'));    

            // Filtro por RAZA
            $('#div_filtros_articulos_raza_inactivos').html('');

            filtro_raza_inactivos = $('<div><div class="filtro-tipo">RAZA</div></div>')
                .appendTo( $('#div_filtros_articulos_raza_inactivos'));

            select_raza_inactivos= $('<ul id="ddl_raza_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_raza_inactivos'));                       
                
            // Filtro por EDAD
            $('#div_filtros_articulos_edad_inactivos').html('');

            filtro_edad_inactivos = $('<div><div class="filtro-tipo">EDAD</div></div>')
                .appendTo( $('#div_filtros_articulos_edad_inactivos'));

            select_edad_inactivos= $('<ul id="ddl_edad_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_edad_inactivos'));      

            // Filtro por TAMAÑO
            $('#div_filtros_articulos_tamanio_inactivos').html('');

            filtro_tamanio_inactivos = $('<div><div class="filtro-tipo">TAMAÑO</div></div>')
                .appendTo( $('#div_filtros_articulos_tamanio_inactivos'));

            select_tamanio_inactivos= $('<ul id="ddl_tamanio_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_tamanio_inactivos'));    

            // Filtro por PRESENTACION
            $('#div_filtros_articulos_presentacion_inactivos').html('');

            filtro_presentacion_inactivos = $('<div><div class="filtro-tipo">PRESENTACIÓN</div></div>')
                .appendTo( $('#div_filtros_articulos_presentacion_inactivos'));

            select_presentacion_inactivos= $('<ul id="ddl_presentacion_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_presentacion_inactivos'));     

            // Filtro por MEDICADOS
            $('#div_filtros_articulos_medicados_inactivos').html('');

            filtro_medicados_inactivos = $('<div><div class="filtro-tipo">MEDICADOS</div></div>')
                .appendTo( $('#div_filtros_articulos_medicados_inactivos'));

            select_medicados_inactivos= $('<ul id="ddl_medicados_inactivos"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_medicados_inactivos'));    


            // Oculta inicialmente filtros que dependen del Rubro y Animal seleccionado
            $('#div_filtros_articulos_marca_inactivos').hide();
            $('#div_filtros_articulos_raza_inactivos').hide();
            $('#div_filtros_articulos_tamanio_inactivos').hide();
            $('#div_filtros_articulos_edad_inactivos').hide();
            $('#div_filtros_articulos_presentacion_inactivos').hide();
            $('#div_filtros_articulos_medicados_inactivos').hide();                        
            filtro_marca_inactivos.hide();
            filtro_edad_inactivos.hide();
            filtro_tamanio_inactivos.hide();
            filtro_raza_inactivos.hide();
            filtro_presentacion_inactivos.hide();
            filtro_medicados_inactivos.hide();

            $('.div_table_productos').show();
        },
        drawCallback: function() 
        {
            if (drawCallback_inactivos == 0)
            {
              // Inicializa filtros
              cuenta_filtro_creador_inactivos = 0;
              cuenta_filtro_rubro_inactivos = 0;
              cuenta_filtro_animal_inactivos = 0;
              cuenta_filtro_marca_inactivos = 0;
              cuenta_filtro_raza_inactivos = 0;
              cuenta_filtro_tamanio_inactivos = 0;
              cuenta_filtro_edad_inactivos = 0;
              cuenta_filtro_presentacion_inactivos = 0;
              cuenta_filtro_medicados_inactivos = 0;

              $('#div_filtros_articulos_aplicados_items_inactivos').html('');

              this.api().columns().every( function () 
              {

                  var column = this;

                  // Filtro por creador
                  if (column.index() == 0)
                  {
                      if (filtros_aplicados_creador_inactivos == '')
                      {                                
                          if (filtro_creador_inactivos) filtro_creador_inactivos.show();

                          if (select_creador_inactivos)
                          {
                              select_creador_inactivos.show();      
                              select_creador_inactivos.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_creador/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos + "/" + filtros_aplicados_raza_id_inactivos + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/false/0",
                             success: function(data)
                             {
                                 data_creador_inactivos = jQuery.parseJSON(data);

                                 $.each(data_creador_inactivos.creadores, function(indice, creador)
                                  {
                                      var option = '<li class="filtro-item" value="' + creador.id + '">' + creador.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_creador_inactivos)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback_inactivos = 0;                                              
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_creador_inactivos = creador.nombre;
                                              filtros_aplicados_creador_id_inactivos = creador.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados_inactivos++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                          } )   

                                      cuenta_filtro_creador_inactivos++;
                                  });    

                             }
                         });

                          // Oculta filtros que dependen de creador
                          if (filtro_marca_inactivos)
                          {
                              filtro_marca_inactivos.hide();
                              select_marca_inactivos.hide();
                          } 

                          if (filtro_raza_inactivos)
                          {
                              filtro_raza_inactivos.hide();
                              select_raza_inactivos.hide();
                          }    

                          if (filtro_tamanio_inactivos)
                          {
                              filtro_tamanio_inactivos.hide();
                              select_tamanio_inactivos.hide();
                          }  

                          if (filtro_edad_inactivos)
                          {
                              filtro_edad_inactivos.hide();
                              select_edad_inactivos.hide();
                          }  

                          if (filtro_presentacion_inactivos)
                          {
                              filtro_presentacion_inactivos.hide();
                              select_presentacion_inactivos.hide();
                          }     

                          if (filtro_medicados_inactivos)
                          {
                              filtro_medicados_inactivos.hide();
                              select_medicados_inactivos.hide();
                          }                                                                     
                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_creador_inactivos.hide();
                          select_creador_inactivos.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_creador_inactivos = $('<div />')
                              .html(filtros_aplicados_creador_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  $('.div_cargando').show();
                                  drawCallback_inactivos = 0;
                                  // Reset del filtro actual                                    
                                  var val = '';

                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados_inactivos--;
                                  filtros_aplicados_creador_inactivos = '';
                                  filtros_aplicados_creador_id_inactivos = -1;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                              }); 
                      }
                  }

                  // Filtro por ANIMAL
                  if (column.index() == 1)
                  {
                      if (filtros_aplicados_animal_inactivos == '')
                      {                                
                          if (filtro_animal_inactivos) filtro_animal_inactivos.show();

                          if (select_animal_inactivos)
                          {
                              select_animal_inactivos.show();      
                              select_animal_inactivos.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_animal/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                             success: function(data)
                             {
                                 data_animal_inactivos = jQuery.parseJSON(data);

                                 $.each(data_animal_inactivos.animales, function(indice, animal)
                                  {
                                      var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_animal_inactivos)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback_inactivos = 0;                                              
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_animal_inactivos = animal.nombre;
                                              filtros_aplicados_animal_id_inactivos = animal.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados_inactivos++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                          } )   

                                      cuenta_filtro_animal_inactivos++;
                                  });    

                             }
                         });

                          // Oculta filtros que dependen de Animal
                          if (filtro_marca_inactivos)
                          {
                              filtro_marca_inactivos.hide();
                              select_marca_inactivos.hide();
                          } 

                          if (filtro_raza_inactivos)
                          {
                              filtro_raza_inactivos.hide();
                              select_raza_inactivos.hide();
                          }    

                          if (filtro_tamanio_inactivos)
                          {
                              filtro_tamanio_inactivos.hide();
                              select_tamanio_inactivos.hide();
                          }  

                          if (filtro_edad_inactivos)
                          {
                              filtro_edad_inactivos.hide();
                              select_edad_inactivos.hide();
                          }  

                          if (filtro_presentacion_inactivos)
                          {
                              filtro_presentacion_inactivos.hide();
                              select_presentacion_inactivos.hide();
                          }     

                          if (filtro_medicados_inactivos)
                          {
                              filtro_medicados_inactivos.hide();
                              select_medicados_inactivos.hide();
                          }                                                                     
                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_animal_inactivos.hide();
                          select_animal_inactivos.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_animal_inactivos = $('<div />')
                              .html(filtros_aplicados_animal_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback_inactivos = 0;

                                  // Quita filtros aplicados que dependen de Animal
                                  var table = $('#table_productos_inactivos').DataTable();
                                  table.columns(3).search( val ? val : '', true, false );
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );
                                  table.columns(11).search( val ? val : '', true, false );

                                  // Reset del filtro actual                                    
                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados_inactivos--;
                                  filtros_aplicados_animal_inactivos = '';
                                  filtros_aplicados_animal_id_inactivos = -1;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                              }); 
                      }
                  }


                  // Filtro por RUBRO
                  if (column.index() == 2)
                  {
                      if (filtros_aplicados_rubro_inactivos == '')
                      {                                
                          if (filtro_rubro_inactivos) filtro_rubro_inactivos.show();

                          if (select_rubro_inactivos)
                          {
                              select_rubro_inactivos.show();      
                              select_rubro_inactivos.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_rubro/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                             success: function(data)
                             {
                                 data_rubro_inactivos = jQuery.parseJSON(data);

                                 $.each(data_rubro_inactivos.rubros, function(indice, rubro)
                                  {
                                      var option = '<li class="filtro-item" value="' + rubro.id + '">' + rubro.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_rubro_inactivos)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback_inactivos = 0;
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_rubro_inactivos = rubro.nombre;
                                              filtros_aplicados_rubro_id_inactivos = rubro.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados_inactivos++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                          } )   

                                      cuenta_filtro_rubro_inactivos++;
                                  }); 

                             }
                         });

                          // Oculta filtros que dependen de Rubro
                          if (filtro_marca_inactivos)
                          {
                              filtro_marca_inactivos.hide();
                              select_marca_inactivos.hide();
                          } 

                          if (filtro_raza_inactivos)
                          {
                              filtro_raza_inactivos.hide();
                              select_raza_inactivos.hide();
                          }    

                          if (filtro_tamanio_inactivos)
                          {
                              filtro_tamanio_inactivos.hide();
                              select_tamanio_inactivos.hide();
                          }   

                          if (filtro_edad_inactivos)
                          {
                              filtro_edad_inactivos.hide();
                              select_edad_inactivos.hide();
                          } 

                          if (filtro_presentacion_inactivos)
                          {
                              filtro_presentacion_inactivos.hide();
                              select_presentacion_inactivos.hide();
                          } 

                          if (filtro_medicados_inactivos)
                          {
                              filtro_medicados_inactivos.hide();
                              select_medicados_inactivos.hide();
                          } 

                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_rubro_inactivos.hide();
                          select_rubro_inactivos.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_rubro_inactivos = $('<div />')
                              .html(filtros_aplicados_rubro_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback_inactivos = 0;

                                  // Quita filtros aplicados que dependen de Animal
                                  var table = $('#table_productos_inactivos').DataTable();
                                  table.columns(3).search( val ? val : '', true, false );
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );
                                  table.columns(11).search( val ? val : '', true, false );

                                  // Reset del filtro actual                                    
                                  filtros_aplicados_rubro_inactivos = '';
                                  filtros_aplicados_rubro_id_inactivos = -1;

                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados_inactivos--;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                              });

                      }
                  }


                  // Filtro por MARCA
                  if (column.index() == 3)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'marca' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_marca_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_marca/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_raza_id_inactivos + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_marca_inactivos = jQuery.parseJSON(data);

                                     if (data_marca_inactivos.marcas)
                                     {
                                          $('#div_filtros_articulos_marca_inactivos').show();
                                          if (filtro_marca_inactivos) filtro_marca_inactivos.show();

                                          if (select_marca_inactivos)
                                          {
                                              select_marca_inactivos.show();      
                                              select_marca_inactivos.html('');
                                          } 

                                         $.each(data_marca_inactivos.marcas, function(indice, marca)
                                          {
                                              var option = '<li class="filtro-item" value="' + marca.id + '">' + marca.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_marca_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_marca_inactivos = marca.nombre;
                                                      filtros_aplicados_marca_id_inactivos = marca.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_marca_inactivos++;   
                                          });   

                                     }

                                     if (data_marca_inactivos.marcas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_marca_inactivos.hide();
                                          select_marca_inactivos.hide();
                                     }                                      
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_marca_inactivos.hide();
                              select_marca_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_marca_inactivos = $('<div />')
                                  .html(filtros_aplicados_marca_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_marca_inactivos = '';
                                      filtros_aplicados_marca_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          if (filtros_aplicados_marca_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_marca_inactivos = '';
                              filtros_aplicados_marca_id_inactivos = -1;
                          }                                
                      }                       
                  }  


                  //  Filtro por EDAD
                  if (column.index() == 7)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'edad' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_edad_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_edad/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos  + "/" + filtros_aplicados_raza_id_inactivos  + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_edad_inactivos = jQuery.parseJSON(data);

                                     if (data_edad_inactivos.edades)
                                     {
                                          $('#div_filtros_articulos_edad_inactivos').show();
                                          if (filtro_edad_inactivos) filtro_edad_inactivos.show();

                                          if (select_edad_inactivos)
                                          {
                                              select_edad_inactivos.show();      
                                              select_edad_inactivos.html('');
                                          } 

                                         $.each(data_edad_inactivos.edades, function(indice, edad)
                                          {
                                              var option = '<li class="filtro-item" value="' + edad.nombre + '">' + edad.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_edad_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_edad_inactivos = edad.nombre;
                                                      filtros_aplicados_edad_id_inactivos = edad.nombre;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_edad_inactivos++;   
                                          });   

                                     }
                                     
                                     if (data_edad_inactivos.edades.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_edad_inactivos.hide();
                                          select_edad_inactivos.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_edad_inactivos.hide();
                              select_edad_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_edad_inactivos = $('<div />')
                                  .html(filtros_aplicados_edad_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_edad_inactivos = '';
                                      filtros_aplicados_edad_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          if (filtros_aplicados_edad_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_edad_inactivos = '';
                              filtros_aplicados_edad_id_inactivos = -1;
                          }                                
                      }                        
                  }  


                  //  Filtro por TAMAÑO
                  if (column.index() == 8)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'tamanio' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_tamanio_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_tamanio/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos  + "/" + filtros_aplicados_raza_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_tamanio_inactivos = jQuery.parseJSON(data);

                                     if (data_tamanio_inactivos.tamanios)
                                     {
                                          $('#div_filtros_articulos_tamanio_inactivos').show();
                                          if (filtro_tamanio_inactivos) filtro_tamanio_inactivos.show();

                                          if (select_tamanio_inactivos)
                                          {
                                              select_tamanio_inactivos.show();      
                                              select_tamanio_inactivos.html('');
                                          } 

                                         $.each(data_tamanio_inactivos.tamanios, function(indice, tamanio)
                                          {
                                              var option = '<li class="filtro-item" value="' + tamanio.id + '">' + tamanio.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_tamanio_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_tamanio_inactivos = tamanio.nombre;
                                                      filtros_aplicados_tamanio_id_inactivos = tamanio.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_tamanio_inactivos++;   
                                          });   

                                     }
                                     
                                     if (data_tamanio_inactivos.tamanios.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_tamanio_inactivos.hide();
                                          select_tamanio_inactivos.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_tamanio_inactivos.hide();
                              select_tamanio_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_tamanio_inactivos = $('<div />')
                                  .html(filtros_aplicados_tamanio_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_tamanio_inactivos = '';
                                      filtros_aplicados_tamanio_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      }  
                      else
                      {
                          if (filtros_aplicados_tamanio_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_tamanio_inactivos = '';
                              filtros_aplicados_tamanio_id_inactivos = -1;
                          }                                
                      }                                          
                  } 


                  // Filtro por RAZA
                  if (column.index() == 9)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'raza' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_raza_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_raza/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_raza_inactivos = jQuery.parseJSON(data);

                                     if (data_raza_inactivos.razas)
                                     {
                                          $('#div_filtros_articulos_raza_inactivos').show();
                                          if (filtro_raza_inactivos) filtro_raza_inactivos.show();

                                          if (select_raza_inactivos)
                                          {
                                              select_raza_inactivos.show();      
                                              select_raza_inactivos.html('');
                                          } 

                                         $.each(data_raza_inactivos.razas, function(indice, raza)
                                          {
                                              var option = '<li class="filtro-item" value="' + raza.id + '">' + raza.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_raza_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_raza_inactivos = raza.nombre;
                                                      filtros_aplicados_raza_id_inactivos = raza.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_raza_inactivos++;   
                                          });   

                                     }

                                     if (data_raza_inactivos.razas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_raza_inactivos.hide();
                                          select_raza_inactivos.hide();
                                     }                                   
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_raza_inactivos.hide();
                              select_raza_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_raza_inactivos = $('<div />')
                                  .html(filtros_aplicados_raza_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_raza_inactivos = '';
                                      filtros_aplicados_raza_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      }  
                      else
                      {
                          if (filtros_aplicados_raza_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_raza_inactivos = '';
                              filtros_aplicados_raza_id_inactivos = -1;
                          }                                
                      }                                          
                  }   


                  //  Filtro por PRESENTACION
                  if (column.index() == 10)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'presentacion' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_presentacion_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_presentacion/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos  + "/" + filtros_aplicados_raza_id_inactivos  + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_medicados_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_presentacion_inactivos = jQuery.parseJSON(data);

                                     if (data_presentacion_inactivos.presentaciones)
                                     {
                                          $('#div_filtros_articulos_presentacion_inactivos').show();
                                          if (filtro_presentacion_inactivos) filtro_presentacion_inactivos.show();

                                          if (select_presentacion_inactivos)
                                          {
                                              select_presentacion_inactivos.show();      
                                              select_presentacion_inactivos.html('');
                                          } 

                                         $.each(data_presentacion_inactivos.presentaciones, function(indice, presentacion)
                                          {
                                              var option = '<li class="filtro-item" value="' + presentacion.id + '">' + presentacion.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_presentacion_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_presentacion_inactivos = presentacion.nombre;
                                                      filtros_aplicados_presentacion_id_inactivos = presentacion.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_presentacion_inactivos++;   
                                          });   

                                     }
                                     
                                     if (data_presentacion_inactivos.presentaciones.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_presentacion_inactivos.hide();
                                          select_presentacion_inactivos.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_presentacion_inactivos.hide();
                              select_presentacion_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_presentacion_inactivos = $('<div />')
                                  .html(filtros_aplicados_presentacion_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_presentacion_inactivos = '';
                                      filtros_aplicados_presentacion_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      }         
                      else
                      {
                          if (filtros_aplicados_presentacion_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_presentacion_inactivos = '';
                              filtros_aplicados_presentacion_id_inactivos = -1;
                          }                                
                      }                                   
                  }


                  //  Filtro por MEDICADOS
                  if (column.index() == 11)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'medicados' (Alimentos por ej)
                      if (filtros_aplicados_rubro_inactivos != '' && filtros_aplicados_animal_inactivos != '')
                      {
                          if (filtros_aplicados_medicados_inactivos == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_articulo_medicados/" + filtros_aplicados_rubro_id_inactivos + "/" + filtros_aplicados_animal_id_inactivos + "/" + filtros_aplicados_marca_id_inactivos  + "/" + filtros_aplicados_raza_id_inactivos  + "/" + filtros_aplicados_tamanio_id_inactivos + "/" + filtros_aplicados_edad_id_inactivos + "/" + filtros_aplicados_presentacion_id_inactivos + "/" + filtros_aplicados_creador_id_inactivos + "/false/0",
                                 success: function(data)
                                 {
                                     data_medicados_inactivos = jQuery.parseJSON(data);

                                     if (data_medicados_inactivos.medicados)
                                     {
                                          $('#div_filtros_articulos_medicados_inactivos').show();
                                          if (filtro_medicados_inactivos) filtro_medicados_inactivos.show();

                                          if (select_medicados_inactivos)
                                          {
                                              select_medicados_inactivos.show();      
                                              select_medicados_inactivos.html('');
                                          } 

                                         $.each(data_medicados_inactivos.medicados, function(indice, medicados)
                                          {
                                              var option = '<li class="filtro-item" value="' + medicados.id + '">' + medicados.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_medicados_inactivos)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_inactivos = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_medicados_inactivos = medicados.nombre;
                                                      filtros_aplicados_medicados_id_inactivos = medicados.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_inactivos++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_inactivos == 1) $('#div_filtros_articulos_aplicados_inactivos').show();
                                                  } )

                                                  cuenta_filtro_medicados_inactivos++;   
                                          });   

                                     }
                                     else
                                     {
                                           // Oculta este filtro
                                          filtro_medicados_inactivos.hide();
                                          select_medicados_inactivos.hide();                                   
                                     }
                                     
                                     if (data_medicados_inactivos.medicados.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_medicados_inactivos.hide();
                                          select_medicados_inactivos.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_medicados_inactivos.hide();
                              select_medicados_inactivos.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_medicados_inactivos = $('<div />')
                                  .html('Medicados: ' + filtros_aplicados_medicados_inactivos + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_inactivos'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_inactivos = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_inactivos--;
                                      filtros_aplicados_medicados_inactivos = '';
                                      filtros_aplicados_medicados_id_inactivos = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_inactivos == 0) $('#div_filtros_articulos_aplicados_inactivos').hide();

                                  });
                          }                       
                      } 
                      else
                      {
                          if (filtros_aplicados_medicados_inactivos != '')
                          {
                              cuenta_filtros_aplicados_inactivos--;
                              filtros_aplicados_medicados_inactivos = '';
                              filtros_aplicados_medicados_id_inactivos = -1;
                          }                                
                      }                                           
                  }

              } );

              $('.div_cargando').hide();     
              
              drawCallback_inactivos = 1;         
            }

        },         
    });
}


function ocultaSeccionesRubro()
{
    $('[id="ddl_marca_productos"]').hide();
    $('[id="ddl_raza_productos"]').hide();
    $('[id="ddl_tamanio_productos"]').hide();
    $('[id="ddl_edad_productos"]').hide();
    $('[id="ddl_medicados_productos"]').hide();
}


function ocultaSeccionesRubro_inactivos()
{
    $('[id="ddl_marca_productos_inactivos"]').hide();
    $('[id="ddl_raza_productos_inactivos"]').hide();
    $('[id="ddl_tamanio_productos_inactivos"]').hide();
    $('[id="ddl_edad_productos_inactivos"]').hide();
    $('[id="ddl_medicados_productos_inactivos"]').hide();
}


function ocultaSeccionesRubro_articulos_disponibles()
{
    $('[id="ddl_marca_articulos_disponibles"]').hide();
    $('[id="ddl_raza_articulos_disponibles"]').hide();
    $('[id="ddl_tamanio_articulos_disponibles"]').hide();
    $('[id="ddl_edad_articulos_disponibles"]').hide();
    $('[id="ddl_medicados_articulos_disponibles"]').hide();
}


function cambioRubro(rubro)
{
    // Oculta todas las secciones que dependen de Rubro
    ocultaSeccionesRubro();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (rubro !== "")
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_get_by_nombre/" + rubro,        
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                // Marca
                conmarca = (data.conmarca == '1' ? true : false);
                if (conmarca) $('[id="ddl_marca_productos"]').show();

                // Raza
                conraza = (data.conraza == '1' ? true : false);
                if (conraza) $('[id="ddl_raza_productos"]').show();

                // Tamaño
                contamanios = (data.contamanios == '1' ? true : false);
                if (contamanios) $('[id="ddl_tamanio_productos"]').show();

                // Edad
                conedad = (data.conedad == '1' ? true : false);
                if (conedad) $('[id="ddl_edad_productos"]').show();

                // Medicados
                conmedicados = (data.conmedicados == '1' ? true : false);
                if (conmedicados) $('[id="ddl_medicados_productos"]').show();    

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                
            }
        });
    }
}


function cambioRubro_inactivos(rubro)
{
    // Oculta todas las secciones que dependen de Rubro
    ocultaSeccionesRubro_inactivos();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (rubro !== "")
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_get_by_nombre/" + rubro,        
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                // Marca
                conmarca = (data.conmarca == '1' ? true : false);
                if (conmarca) $('[id="ddl_marca_productos_inactivos"]').show();

                // Raza
                conraza = (data.conraza == '1' ? true : false);
                if (conraza) $('[id="ddl_raza_productos_inactivos"]').show();

                // Tamaño
                contamanios = (data.contamanios == '1' ? true : false);
                if (contamanios) $('[id="ddl_tamanio_productos_inactivos"]').show();

                // Edad
                conedad = (data.conedad == '1' ? true : false);
                if (conedad) $('[id="ddl_edad_productos_inactivos"]').show();

                // Medicados
                conmedicados = (data.conmedicados == '1' ? true : false);
                if (conmedicados) $('[id="ddl_medicados_productos_inactivos"]').show();    

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                
            }
        });
    }
}


function cambioRubro_articulos_disponibles(rubro)
{
    // Oculta todas las secciones que dependen de Rubro
    ocultaSeccionesRubro_articulos_disponibles();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (rubro !== "")
    {
        $.ajax({
            url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_get_by_nombre/" + rubro,        
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {

                // Marca
                conmarca = (data.conmarca == '1' ? true : false);
                if (conmarca) $('[id="ddl_marca_articulos_disponibles"]').show();

                // Raza
                conraza = (data.conraza == '1' ? true : false);
                if (conraza) $('[id="ddl_raza_articulos_disponibles"]').show();

                // Tamaño
                contamanios = (data.contamanios == '1' ? true : false);
                if (contamanios) $('[id="ddl_tamanio_articulos_disponibles"]').show();

                // Edad
                conedad = (data.conedad == '1' ? true : false);
                if (conedad) $('[id="ddl_edad_articulos_disponibles"]').show();

                // Medicados
                conmedicados = (data.conmedicados == '1' ? true : false);
                if (conmedicados) $('[id="ddl_medicados_articulos_disponibles"]').show();    

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                
            }
        });
    }
}


// Activa o desactiva un producto del local
function switch_producto(articulo_id)
{

    $.ajax({
        url : "<?php echo BASE_PATH ?>/Local/ajax_activar_desactivar_producto/" + articulo_id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            // Recarga tablas de productos (sin modificar filtros)
            drawCallback = 0; 
            var table = $('#table_productos').DataTable();
            table.draw();            

            drawCallback_inactivos = 0; 
            var table_inactivos = $('#table_productos_inactivos').DataTable();
            table_inactivos.draw();         

            recargar_filtros();   

            if (data.nuevo_estado == 1)
                aviso('success', 'Producto activado.');  
            else
                aviso('success', 'Producto desactivado.');  

        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            aviso('danger', textStatus, 'Error al activar / desactivar.'); 
        }
    });
}


function recargar_filtros()
{
  $.ajax({
        url : "<?php echo BASE_PATH ?>/Local/ajax_filtros_productos/",
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            // PRODUCTOS ACTIVOS
            // Rubros
            seleccionado = $("#ddl_rubro_productos option:selected").text();

            $("#ddl_rubro_productos option").remove();

            $('#ddl_rubro_productos').append($('<option>', 
                { 
                    value: '',
                    text : 'RUBRO'
                }));

            $.each(data.rubros_activos, function (i, item) 
            {
                $('#ddl_rubro_productos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_rubro_productos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_rubro_productos").val(seleccionado);
            else
                $("#ddl_rubro_productos").val('');

            $("#ddl_rubro_productos").trigger('change'); 


            // Animales
            seleccionado = $("#ddl_animal_productos option:selected").text();

            $("#ddl_animal_productos option").remove();

            $('#ddl_animal_productos').append($('<option>', 
                { 
                    value: '',
                    text : 'ANIMAL'
                }));

            $.each(data.animales_activos, function (i, item) 
            {
                $('#ddl_animal_productos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_animal_productos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_animal_productos").val(seleccionado);
            else
                $("#ddl_animal_productos").val('');

            $("#ddl_animal_productos").trigger('change'); 


            // Marcas
            seleccionado = $("#ddl_marca_productos option:selected").text();

            $("#ddl_marca_productos option").remove();

            $('#ddl_marca_productos').append($('<option>', 
                { 
                    value: '',
                    text : 'MARCA'
                }));

            $.each(data.marcas_activos, function (i, item) 
            {
                $('#ddl_marca_productos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre,
                    class: item.rubro_animal_ajax
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_marca_productos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_marca_productos").val(seleccionado);
            else
                $("#ddl_marca_productos").val('');

            $("#ddl_marca_productos").chained("#ddl_rubro_productos, #ddl_animal_productos");
            $("#ddl_marca_productos").trigger('change'); 


            // PRODUCTOS INACTIVOS
            // Rubros
            seleccionado = $("#ddl_rubro_productos_inactivos option:selected").text();

            $("#ddl_rubro_productos_inactivos option").remove();

            $('#ddl_rubro_productos_inactivos').append($('<option>', 
                { 
                    value: '',
                    text : 'RUBRO'
                }));

            $.each(data.rubros_inactivos, function (i, item) 
            {
                $('#ddl_rubro_productos_inactivos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_rubro_productos_inactivos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_rubro_productos_inactivos").val(seleccionado);
            else
                $("#ddl_rubro_productos_inactivos").val('');

            $("#ddl_rubro_productos_inactivos").trigger('change'); 


            // Animales
            seleccionado = $("#ddl_animal_productos_inactivos option:selected").text();

            $("#ddl_animal_productos_inactivos option").remove();

            $('#ddl_animal_productos_inactivos').append($('<option>', 
                { 
                    value: '',
                    text : 'ANIMAL'
                }));

            $.each(data.animales_inactivos, function (i, item) 
            {
                $('#ddl_animal_productos_inactivos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_animal_productos_inactivos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_animal_productos_inactivos").val(seleccionado);
            else
                $("#ddl_animal_productos_inactivos").val('');

            $("#ddl_animal_productos_inactivos").trigger('change'); 


            // Marcas
            seleccionado = $("#ddl_marca_productos_inactivos option:selected").text();

            $("#ddl_marca_productos_inactivos option").remove();

            $('#ddl_marca_productos_inactivos').append($('<option>', 
                { 
                    value: '',
                    text : 'MARCA'
                }));

            $.each(data.marcas_inactivos, function (i, item) 
            {
                $('#ddl_marca_productos_inactivos').append($('<option>', 
                { 
                    value: item.nombre,
                    text : item.nombre,
                    class: item.rubro_animal_ajax
                }));
            });

            // Si todavía existe, vuelve a seleccionar el item que estaba seleccionado 
            if ($("#ddl_marca_productos_inactivos").find('option[value=' + seleccionado + ']').length)
                $("#ddl_marca_productos_inactivos").val(seleccionado);
            else
                $("#ddl_marca_productos_inactivos").val('');

            $("#ddl_marca_productos_inactivos").chained("#ddl_rubro_productos_inactivos, #ddl_animal_productos_inactivos");
            $("#ddl_marca_productos_inactivos").trigger('change'); 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {   
            aviso('danger', textStatus, 'Error al activar / desactivar.'); 
        }
    });
}


/* Detalles de un producto */
function ver_articulo(id)
{
    var marca_rubro = '';
    var presentacion = '';
    var sin_stock = '';
    var html_presentaciones = '';
    var html_presentaciones_inactivas = '';

    $("#span_presentaciones_articulo_local").show();
    $("#span_presentaciones_articulo_geotienda").hide();

    $.ajax({
      url : "<?php echo BASE_PATH ?>/local/get_articulo_by_id/" + id + '/<?php echo $_SESSION['frontend_user_id']; ?>',
      cache: false,
      success: function(data){

        articulo = jQuery.parseJSON(data);

        // Si el artículo no tiene marca, muestra el rubro en su lugar
        if (articulo.marca === null) 
            marca_rubro = articulo.rubro;
        else 
            marca_rubro = articulo.marca;

        $("#modal_articulo_marca").html(marca_rubro);
        $("#modal_articulo_nombre").html(articulo.nombre);

        // Productos creados por el Local
        if(articulo.tipo == 'Local')
          $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/locales/fotos/' +  articulo.imagen + '" alt="">');
        // Productos Geotienda
        else
          $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/fotos/' +  articulo.imagen + '" alt="">');

        $("#modal_articulo_detalle").html(articulo.detalle);


        // Presentaciones del artículo
        for (j = 0; j < articulo.presentacion.length; ++j) 
        {
            tachado = '';
            descuento_oculto = '';

            presentacion = articulo.presentacion[j].nombre;

            // Presentación con Descuento
            if (articulo.presentacion[j].precio != articulo.presentacion[j].precio_descuento){

                tachado = 'font-w300 tachado text-muted';
                ocultar_circulo = '';       
            }
            // Presentación sin Descuento
            else{

                tachado = 'font-w600 text-success';
                descuento_oculto = 'oculto';
                ocultar_circulo = 'style="display:none"';       
            }   

            // Presentación con Stock
            if (articulo.presentacion[j].stock == '1'){
     
                sin_stock = 'style="display:none"';       
            }
            // Presentación sin Stock
            else{
    
                sin_stock = '';      
            }    

            // Presentaciones activas
            if (articulo.presentacion[j].estado == '1')
            {
              html_presentaciones += 
                '<div class="h5 font-w600 text-muted pull-left">' + presentacion +'</div>' +

                // Stock
                '<div class="pull-right push-10-l push--5-t" ' + sin_stock + '>' + 
                '<div class="push-5-t" style="color: red;">SIN STOCK</div>' + 
                '</div>' + 

                '<div class="h5 pull-right push-10-l ' + tachado + '">$' +  articulo.presentacion[j].precio +'</div>' + 
                '<br />' + 
                '<div class="h5 font-w600 text-success pull-right push-10-l ' + descuento_oculto + '">' + 
                '<div class="circulo_descuento_mini"' + ocultar_circulo + '><span>-'+ articulo.presentacion[j].descuento +'%</span></div>' +                
                '&nbsp;&nbsp;$' +  articulo.presentacion[j].precio_descuento +'</div>' + 
                '<div style="height:10px; clear: both;"></div>';
            }
            // Presentaciones inactivas
            else
            {
              html_presentaciones_inactivas += 
                '<div class="h5 font-w600 text-muted pull-left">' + presentacion +'</div>' +

                // Stock
                '<div class="pull-right push-10-l push--5-t" ' + sin_stock + '>' + 
                '<div class="push-5-t" style="color: red;">SIN STOCK</div>' + 
                '</div>' + 

                '<div class="h5 pull-right push-10-l ' + tachado + '">$' +  articulo.presentacion[j].precio +'</div>' + 
                '<br />' + 
                '<div class="h5 font-w600 text-success pull-right push-10-l ' + descuento_oculto + '">' + 
                '<div class="circulo_descuento_mini"' + ocultar_circulo + '><span>-'+ articulo.presentacion[j].descuento +'%</span></div>' +                
                '&nbsp;&nbsp;$' +  articulo.presentacion[j].precio_descuento +'</div>' + 
                '<div style="height:10px; clear: both;"></div>';   
            }             
        }

        if(html_presentaciones == '') html_presentaciones = '<div class="h5 font-w600 text-muted pull-left push-20">No hay presentaciones activas</div>';

        $("#modal_articulo_presentaciones").html(html_presentaciones);
        $("#modal_articulo_presentaciones_inactivas").html(html_presentaciones_inactivas);

        $('#modal_detalles_articulo').modal('show'); 
      } 
    });

}
/* FIN Detalles de un producto */


/* Detalles de un articulo (no asociado al local) */
function ver_articulo_geotienda(id)
{
    var marca_rubro = '';
    var presentacion = '';
    var html_presentaciones = '';

    $("#span_presentaciones_articulo_local").hide();
    $("#span_presentaciones_articulo_geotienda").show();

    $.ajax({
      url : "<?php echo BASE_PATH ?>/articulo/get_by_id/" + id,
      cache: false,
      success: function(data){

        articulo = jQuery.parseJSON(data);

        // Si el artículo no tiene marca, muestra el rubro en su lugar
        if (articulo.marca === null) 
            marca_rubro = articulo.rubro;
        else 
            marca_rubro = articulo.marca;

        $("#modal_articulo_marca").html(marca_rubro);
        $("#modal_articulo_nombre").html(articulo.nombre);
        $("#modal_articulo_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/fotos/' +  articulo.imagen + '" alt="">');
        $("#modal_articulo_detalle").html(articulo.detalle);


        // Presentaciones del artículo
        for (j = 0; j < articulo.presentaciones.length; ++j) 
        {
            en_el_local = '';

            // Avisa si la presentación ya fue agregada al local
            if (articulo.presentaciones[j].precio) 
            {
                en_el_local = ' (ya agregada al local)';
            }

            presentacion = articulo.presentaciones[j].nombre;

            html_presentaciones += 
                '<div class="h5 font-w600 text-muted pull-left">' + presentacion + ' ' + en_el_local + '</div>' +
                '<div style="height:10px; clear: both;"></div>';
        }

        $("#modal_articulo_presentaciones_geotienda").html(html_presentaciones);

        $('#modal_detalles_articulo').modal('show'); 
      } 
    });

}
/* FIN Detalles de un articulo (no asociado al local) */


/* Agregar Producto (Geotienda o nuevo)*/
function add_producto()
{
    $('#div_productos').hide();
    $('#header_productos_local').hide();

    $('#div_agregar_producto').show();
    $('#header_productos_geotienda').show();    
}


function mostrar_productos_geotienda()
{
     table_articulos_geotienda = $('#table_articulos_geotienda').DataTable({ 

        "destroy": true,
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        "ajax": {
            "url": "<?php echo BASE_PATH ?>/Articulo/ajax_list_no_en_local/",
            "type": "POST"
        },
        "pageLength": 10,
        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ 0, -1 ], // first and last column
            "orderable": false, //set not orderable
        },
        {
            "targets": [ 5, 8, 9, 10, 11 ],
            "visible": false
        }           
        ],

        initComplete: function () 
        {
            // Filtro por ANIMAL
            $('#div_filtros_articulos_animal_geotienda').html('');

            filtro_animal_geotienda = $('<div><div class="filtro-tipo">ANIMAL</div></div>')
                .appendTo( $('#div_filtros_articulos_animal_geotienda'));

            select_animal_geotienda= $('<ul id="ddl_animal_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_animal_geotienda'));

            // Filtro por RUBRO
            $('#div_filtros_articulos_rubro_geotienda').html('');

            filtro_rubro_geotienda = $('<div><div class="filtro-tipo">RUBRO</div></div>')
                .appendTo( $('#div_filtros_articulos_rubro_geotienda'));

            select_rubro_geotienda= $('<ul id="ddl_rubro_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_rubro_geotienda'));

            // Filtro por MARCA
            $('#div_filtros_articulos_marca_geotienda').html('');

            filtro_marca_geotienda = $('<div><div class="filtro-tipo">MARCA</div></div>')
                .appendTo( $('#div_filtros_articulos_marca_geotienda'));

            select_marca_geotienda= $('<ul id="ddl_marca_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_marca_geotienda'));    

            // Filtro por RAZA
            $('#div_filtros_articulos_raza_geotienda').html('');

            filtro_raza_geotienda = $('<div><div class="filtro-tipo">RAZA</div></div>')
                .appendTo( $('#div_filtros_articulos_raza_geotienda'));

            select_raza_geotienda= $('<ul id="ddl_raza_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_raza_geotienda'));                       
                
            // Filtro por EDAD
            $('#div_filtros_articulos_edad_geotienda').html('');

            filtro_edad_geotienda = $('<div><div class="filtro-tipo">EDAD</div></div>')
                .appendTo( $('#div_filtros_articulos_edad_geotienda'));

            select_edad_geotienda= $('<ul id="ddl_edad_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_edad_geotienda'));      

            // Filtro por TAMAÑO
            $('#div_filtros_articulos_tamanio_geotienda').html('');

            filtro_tamanio_geotienda = $('<div><div class="filtro-tipo">TAMAÑO</div></div>')
                .appendTo( $('#div_filtros_articulos_tamanio_geotienda'));

            select_tamanio_geotienda= $('<ul id="ddl_tamanio_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_tamanio_geotienda'));    

            // Filtro por PRESENTACION
            $('#div_filtros_articulos_presentacion_geotienda').html('');

            filtro_presentacion_geotienda = $('<div><div class="filtro-tipo">PRESENTACIÓN</div></div>')
                .appendTo( $('#div_filtros_articulos_presentacion_geotienda'));

            select_presentacion_geotienda= $('<ul id="ddl_presentacion_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_presentacion_geotienda'));     

            // Filtro por MEDICADOS
            $('#div_filtros_articulos_medicados_geotienda').html('');

            filtro_medicados_geotienda = $('<div><div class="filtro-tipo">MEDICADOS</div></div>')
                .appendTo( $('#div_filtros_articulos_medicados_geotienda'));

            select_medicados_geotienda= $('<ul id="ddl_medicados_geotienda"  class="filtro-select push-15" ></ul>')
                .appendTo( $('#div_filtros_articulos_medicados_geotienda'));    


            // Oculta inicialmente filtros que dependen del Rubro y Animal seleccionado
            $('#div_filtros_articulos_marca_geotienda').hide();
            $('#div_filtros_articulos_raza_geotienda').hide();
            $('#div_filtros_articulos_tamanio_geotienda').hide();
            $('#div_filtros_articulos_edad_geotienda').hide();
            $('#div_filtros_articulos_presentacion_geotienda').hide();
            $('#div_filtros_articulos_medicados_geotienda').hide();                        
            filtro_marca_geotienda.hide();
            filtro_edad_geotienda.hide();
            filtro_tamanio_geotienda.hide();
            filtro_raza_geotienda.hide();
            filtro_presentacion_geotienda.hide();
            filtro_medicados_geotienda.hide();

            $('.div_table_productos').show();
        },
        drawCallback: function() 
        {
            if (drawCallback_geotienda == 0)
            {
              // Inicializa filtros
              cuenta_filtro_rubro_geotienda = 0;
              cuenta_filtro_animal_geotienda = 0;
              cuenta_filtro_marca_geotienda = 0;
              cuenta_filtro_raza_geotienda = 0;
              cuenta_filtro_tamanio_geotienda = 0;
              cuenta_filtro_edad_geotienda = 0;
              cuenta_filtro_presentacion_geotienda = 0;
              cuenta_filtro_medicados_geotienda = 0;

              $('#div_filtros_articulos_aplicados_items_geotienda').html('');

              this.api().columns().every( function () 
              {

                  var column = this;

                  // Filtro por ANIMAL
                  if (column.index() == 0)
                  {
                      if (filtros_aplicados_animal_geotienda == '')
                      {                                
                          if (filtro_animal_geotienda) filtro_animal_geotienda.show();

                          if (select_animal_geotienda)
                          {
                              select_animal_geotienda.show();      
                              select_animal_geotienda.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_animal/" + filtros_aplicados_rubro_id_geotienda,
                             success: function(data)
                             {
                                 data_animal_geotienda = jQuery.parseJSON(data);

                                 $.each(data_animal_geotienda.animales, function(indice, animal)
                                  {
                                      var option = '<li class="filtro-item" value="' + animal.id + '">' + animal.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_animal_geotienda)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback_geotienda = 0;                                              
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_animal_geotienda = animal.nombre;
                                              filtros_aplicados_animal_id_geotienda = animal.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados_geotienda++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                          } )   

                                      cuenta_filtro_animal_geotienda++;
                                  });    
                             }
                         });

                          // Oculta filtros que dependen de Animal
                          if (filtro_marca_geotienda)
                          {
                              filtro_marca_geotienda.hide();
                              select_marca_geotienda.hide();
                          } 

                          if (filtro_raza_geotienda)
                          {
                              filtro_raza_geotienda.hide();
                              select_raza_geotienda.hide();
                          }    

                          if (filtro_tamanio_geotienda)
                          {
                              filtro_tamanio_geotienda.hide();
                              select_tamanio_geotienda.hide();
                          }  

                          if (filtro_edad_geotienda)
                          {
                              filtro_edad_geotienda.hide();
                              select_edad_geotienda.hide();
                          }  

                          if (filtro_presentacion_geotienda)
                          {
                              filtro_presentacion_geotienda.hide();
                              select_presentacion_geotienda.hide();
                          }     

                          if (filtro_medicados_geotienda)
                          {
                              filtro_medicados_geotienda.hide();
                              select_medicados_geotienda.hide();
                          }                                                                     
                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_animal_geotienda.hide();
                          select_animal_geotienda.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_animal_geotienda = $('<div />')
                              .html(filtros_aplicados_animal_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback_geotienda = 0;

                                  // Quita filtros aplicados que dependen de Animal
                                  var table = $('#table_articulos_geotienda').DataTable();
                                  table.columns(2).search( val ? val : '', true, false );
                                  table.columns(6).search( val ? val : '', true, false );                                  
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );

                                  // Reset del filtro actual                                    
                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados_geotienda--;
                                  filtros_aplicados_animal_geotienda = '';
                                  filtros_aplicados_animal_id_geotienda = -1;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                              }); 
                      }
                  }


                  // Filtro por RUBRO
                  if (column.index() == 1)
                  {
                      if (filtros_aplicados_rubro_geotienda == '')
                      {                                
                          if (filtro_rubro_geotienda) filtro_rubro_geotienda.show();

                          if (select_rubro_geotienda)
                          {
                              select_rubro_geotienda.show();      
                              select_rubro_geotienda.html('');
                          } 

                          // Agrega items del filtro
                          $.ajax({
                             type: "POST",
                             data: {'local_id': local_id},
                             url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_rubro/" + filtros_aplicados_animal_id_geotienda,
                             success: function(data)
                             {
                                 data_rubro_geotienda = jQuery.parseJSON(data);

                                 $.each(data_rubro_geotienda.rubros, function(indice, rubro)
                                  {
                                      var option = '<li class="filtro-item" value="' + rubro.id + '">' + rubro.nombre_cuenta+ '</li>';
                                      var item = $(option)
                                          .appendTo(select_rubro_geotienda)
                                          .on('click', function () 
                                          {
                                              $('.div_cargando').show();
                                              drawCallback_geotienda = 0;
                                              var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                              filtros_aplicados_rubro_geotienda = rubro.nombre;
                                              filtros_aplicados_rubro_id_geotienda = rubro.id;
                   
                                              // Busca por el item seleccionado
                                              column
                                                  .search( val ? val : '', true, false )
                                                  .draw();

                                              cuenta_filtros_aplicados_geotienda++;

                                              // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                              if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                          } )   

                                      cuenta_filtro_rubro_geotienda++;
                                  }); 

                             }
                         });

                          // Oculta filtros que dependen de Rubro
                          if (filtro_marca_geotienda)
                          {
                              filtro_marca_geotienda.hide();
                              select_marca_geotienda.hide();
                          } 

                          if (filtro_raza_geotienda)
                          {
                              filtro_raza_geotienda.hide();
                              select_raza_geotienda.hide();
                          }    

                          if (filtro_tamanio_geotienda)
                          {
                              filtro_tamanio_geotienda.hide();
                              select_tamanio_geotienda.hide();
                          }   

                          if (filtro_edad_geotienda)
                          {
                              filtro_edad_geotienda.hide();
                              select_edad_geotienda.hide();
                          } 

                          if (filtro_presentacion_geotienda)
                          {
                              filtro_presentacion_geotienda.hide();
                              select_presentacion_geotienda.hide();
                          } 

                          if (filtro_medicados_geotienda)
                          {
                              filtro_medicados_geotienda.hide();
                              select_medicados_geotienda.hide();
                          } 

                      }

                      // Filtro ya aplicado
                      else
                      {
                          // Oculta este filtro
                          filtro_rubro_geotienda.hide();
                          select_rubro_geotienda.hide();

                          // Agrega item seleccionado a filtros aplicados
                          aplicado_rubro_geotienda = $('<div />')
                              .html(filtros_aplicados_rubro_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                              .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                              // Funcion para quitar filtro aplicado
                              .on('click', function ()
                              { 
                                  var val = '';

                                  $('.div_cargando').show();
                                  drawCallback_geotienda = 0;

                                  // Quita filtros aplicados que dependen de Rubro
                                  var table = $('#table_articulos_geotienda').DataTable();
                                  table.columns(2).search( val ? val : '', true, false );
                                  table.columns(6).search( val ? val : '', true, false );                                  
                                  table.columns(7).search( val ? val : '', true, false );
                                  table.columns(8).search( val ? val : '', true, false );
                                  table.columns(9).search( val ? val : '', true, false );
                                  table.columns(10).search( val ? val : '', true, false );

                                  // Reset del filtro actual                                    
                                  filtros_aplicados_rubro_geotienda = '';
                                  filtros_aplicados_rubro_id_geotienda = -1;

                                  column
                                      .search( val ? val : '', true, false )
                                      .draw();                                    

                                  cuenta_filtros_aplicados_geotienda--;

                                  //Quita el item
                                  this.remove();

                                  // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                  if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                              });

                      }
                  }


                  // Filtro por MARCA
                  if (column.index() == 2)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'marca' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_marca_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_marca/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_raza_id_geotienda + "/" + filtros_aplicados_tamanio_id_geotienda + "/" + filtros_aplicados_edad_id_geotienda + "/" + filtros_aplicados_presentacion_id_geotienda + "/" + filtros_aplicados_medicados_id_geotienda,
                                 success: function(data)
                                 {
                                     data_marca_geotienda = jQuery.parseJSON(data);

                                     if (data_marca_geotienda.marcas)
                                     {
                                          $('#div_filtros_articulos_marca_geotienda').show();
                                          if (filtro_marca_geotienda) filtro_marca_geotienda.show();

                                          if (select_marca_geotienda)
                                          {
                                              select_marca_geotienda.show();      
                                              select_marca_geotienda.html('');
                                          } 

                                         $.each(data_marca_geotienda.marcas, function(indice, marca)
                                          {
                                              var option = '<li class="filtro-item" value="' + marca.id + '">' + marca.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_marca_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_marca_geotienda = marca.nombre;
                                                      filtros_aplicados_marca_id_geotienda = marca.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_marca_geotienda++;   
                                          });   
                                     }

                                     if (data_marca_geotienda.marcas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_marca_geotienda.hide();
                                          select_marca_geotienda.hide();
                                     }                                      
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_marca_geotienda.hide();
                              select_marca_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_marca_geotienda = $('<div />')
                                  .html(filtros_aplicados_marca_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_marca_geotienda = '';
                                      filtros_aplicados_marca_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          if (filtros_aplicados_marca_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_marca_geotienda = '';
                              filtros_aplicados_marca_id_geotienda = -1;
                          }                                
                      }                        
                  }  


                  //  Filtro por EDAD
                  if (column.index() == 6)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'edad' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_edad_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_edad/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_marca_id_geotienda  + "/" + filtros_aplicados_raza_id_geotienda  + "/" + filtros_aplicados_tamanio_id_geotienda + "/" + filtros_aplicados_presentacion_id_geotienda + "/" + filtros_aplicados_medicados_id_geotienda,
                                 success: function(data)
                                 {
                                     data_edad_geotienda = jQuery.parseJSON(data);

                                     if (data_edad_geotienda.edades)
                                     {
                                          $('#div_filtros_articulos_edad_geotienda').show();
                                          if (filtro_edad_geotienda) filtro_edad_geotienda.show();

                                          if (select_edad_geotienda)
                                          {
                                              select_edad_geotienda.show();      
                                              select_edad_geotienda.html('');
                                          } 

                                         $.each(data_edad_geotienda.edades, function(indice, edad)
                                          {
                                              var option = '<li class="filtro-item" value="' + edad.nombre + '">' + edad.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_edad_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_edad_geotienda = edad.nombre;
                                                      filtros_aplicados_edad_id_geotienda = edad.nombre;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_edad_geotienda++;   
                                          });   

                                     }
                                     
                                     if (data_edad_geotienda.edades.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_edad_geotienda.hide();
                                          select_edad_geotienda.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_edad_geotienda.hide();
                              select_edad_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_edad_geotienda = $('<div />')
                                  .html(filtros_aplicados_edad_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_edad_geotienda = '';
                                      filtros_aplicados_edad_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }
                      else
                      {
                          if (filtros_aplicados_edad_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_edad_geotienda = '';
                              filtros_aplicados_edad_id_geotienda = -1;
                          }                                
                      }                       
                  }  


                  //  Filtro por TAMAÑO
                  if (column.index() == 7)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'tamanio' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_tamanio_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_tamanio/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_marca_id_geotienda  + "/" + filtros_aplicados_raza_id_geotienda + "/" + filtros_aplicados_edad_id_geotienda + "/" + filtros_aplicados_presentacion_id_geotienda + "/" + filtros_aplicados_medicados_id_geotienda,
                                 success: function(data)
                                 {
                                     data_tamanio_geotienda = jQuery.parseJSON(data);

                                     if (data_tamanio_geotienda.tamanios)
                                     {
                                          $('#div_filtros_articulos_tamanio_geotienda').show();
                                          if (filtro_tamanio_geotienda) filtro_tamanio_geotienda.show();

                                          if (select_tamanio_geotienda)
                                          {
                                              select_tamanio_geotienda.show();      
                                              select_tamanio_geotienda.html('');
                                          } 

                                         $.each(data_tamanio_geotienda.tamanios, function(indice, tamanio)
                                          {
                                              var option = '<li class="filtro-item" value="' + tamanio.id + '">' + tamanio.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_tamanio_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_tamanio_geotienda = tamanio.nombre;
                                                      filtros_aplicados_tamanio_id_geotienda = tamanio.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_tamanio_geotienda++;   
                                          });   

                                     }
                                     
                                     if (data_tamanio_geotienda.tamanios.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_tamanio_geotienda.hide();
                                          select_tamanio_geotienda.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_tamanio_geotienda.hide();
                              select_tamanio_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_tamanio_geotienda = $('<div />')
                                  .html(filtros_aplicados_tamanio_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_tamanio_geotienda = '';
                                      filtros_aplicados_tamanio_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }     
                      else
                      {
                          if (filtros_aplicados_tamanio_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_tamanio_geotienda = '';
                              filtros_aplicados_tamanio_id_geotienda = -1;
                          }                                
                      }                                      
                  } 


                  // Filtro por RAZA
                  if (column.index() == 8)
                  {
                      // Solo se muestra si se seleccionó un animal y un rubro que tenga el atributo 'raza' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_raza_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_raza/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_marca_id_geotienda + "/" + filtros_aplicados_tamanio_id_geotienda + "/" + filtros_aplicados_edad_id_geotienda + "/" + filtros_aplicados_presentacion_id_geotienda + "/" + filtros_aplicados_medicados_id_geotienda,
                                 success: function(data)
                                 {
                                     data_raza_geotienda = jQuery.parseJSON(data);

                                     if (data_raza_geotienda.razas)
                                     {
                                          $('#div_filtros_articulos_raza_geotienda').show();
                                          if (filtro_raza_geotienda) filtro_raza_geotienda.show();

                                          if (select_raza_geotienda)
                                          {
                                              select_raza_geotienda.show();      
                                              select_raza_geotienda.html('');
                                          } 

                                         $.each(data_raza_geotienda.razas, function(indice, raza)
                                          {
                                              var option = '<li class="filtro-item" value="' + raza.id + '">' + raza.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_raza_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_raza_geotienda = raza.nombre;
                                                      filtros_aplicados_raza_id_geotienda = raza.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_raza_geotienda++;   
                                          });   

                                     }

                                     if (data_raza_geotienda.razas.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_raza_geotienda.hide();
                                          select_raza_geotienda.hide();
                                     }                                   
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_raza_geotienda.hide();
                              select_raza_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_raza_geotienda = $('<div />')
                                  .html(filtros_aplicados_raza_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_raza_geotienda = '';
                                      filtros_aplicados_raza_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }   
                      else
                      {
                          if (filtros_aplicados_raza_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_raza_geotienda = '';
                              filtros_aplicados_raza_id_geotienda = -1;
                          }                                
                      }                                        
                  }   


                  //  Filtro por PRESENTACION
                  if (column.index() == 9)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'presentacion' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_presentacion_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_presentacion/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_marca_id_geotienda  + "/" + filtros_aplicados_raza_id_geotienda  + "/" + filtros_aplicados_tamanio_id_geotienda + "/" + filtros_aplicados_edad_id_geotienda + "/" + filtros_aplicados_medicados_id_geotienda,
                                 success: function(data)
                                 {
                                     data_presentacion_geotienda = jQuery.parseJSON(data);

                                     if (data_presentacion_geotienda.presentaciones)
                                     {
                                          $('#div_filtros_articulos_presentacion_geotienda').show();
                                          if (filtro_presentacion_geotienda) filtro_presentacion_geotienda.show();

                                          if (select_presentacion_geotienda)
                                          {
                                              select_presentacion_geotienda.show();      
                                              select_presentacion_geotienda.html('');
                                          } 

                                         $.each(data_presentacion_geotienda.presentaciones, function(indice, presentacion)
                                          {
                                              var option = '<li class="filtro-item" value="' + presentacion.id + '">' + presentacion.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_presentacion_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_presentacion_geotienda = presentacion.nombre;
                                                      filtros_aplicados_presentacion_id_geotienda = presentacion.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_presentacion_geotienda++;   
                                          });   

                                     }
                                     
                                     if (data_presentacion_geotienda.presentaciones.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_presentacion_geotienda.hide();
                                          select_presentacion_geotienda.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_presentacion_geotienda.hide();
                              select_presentacion_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_presentacion_geotienda = $('<div />')
                                  .html(filtros_aplicados_presentacion_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_presentacion_geotienda = '';
                                      filtros_aplicados_presentacion_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }  
                      else
                      {
                          if (filtros_aplicados_presentacion_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_presentacion_geotienda = '';
                              filtros_aplicados_presentacion_id_geotienda = -1;
                          }                                
                      }                                         
                  }

                  //  Filtro por MEDICADOS
                  if (column.index() == 10)
                  {
                      // Solo se muestra si se seleccionó un animal y rubro que tenga el atributo 'medicados' (Alimentos por ej)
                      if (filtros_aplicados_rubro_geotienda != '' && filtros_aplicados_animal_geotienda != '')
                      {
                          if (filtros_aplicados_medicados_geotienda == '')
                          {                                
                              // Agrega items del filtro
                              $.ajax({
                                 type: "POST",
                                 data: {'local_id': local_id},
                                 url : "<?php echo BASE_PATH ?>/Articulo/ajax_filtros_articulo_medicados/" + filtros_aplicados_rubro_id_geotienda + "/" + filtros_aplicados_animal_id_geotienda + "/" + filtros_aplicados_marca_id_geotienda  + "/" + filtros_aplicados_raza_id_geotienda  + "/" + filtros_aplicados_tamanio_id_geotienda + "/" + filtros_aplicados_edad_id_geotienda + "/" + filtros_aplicados_presentacion_id_geotienda,
                                 success: function(data)
                                 {
                                     data_medicados_geotienda = jQuery.parseJSON(data);

                                     if (data_medicados_geotienda.medicados)
                                     {
                                          $('#div_filtros_articulos_medicados_geotienda').show();
                                          if (filtro_medicados_geotienda) filtro_medicados_geotienda.show();

                                          if (select_medicados_geotienda)
                                          {
                                              select_medicados_geotienda.show();      
                                              select_medicados_geotienda.html('');
                                          } 

                                         $.each(data_medicados_geotienda.medicados, function(indice, medicados)
                                          {
                                              var option = '<li class="filtro-item" value="' + medicados.id + '">' + medicados.nombre_cuenta+ '</li>';
                                              var item = $(option)
                                                  .appendTo(select_medicados_geotienda)
                                                  .on('click', function () 
                                                  {
                                                      $('.div_cargando').show();
                                                      drawCallback_geotienda = 0;
                                                      var val = $.fn.dataTable.util.escapeRegex($(this).attr("value"));

                                                      filtros_aplicados_medicados_geotienda = medicados.nombre;
                                                      filtros_aplicados_medicados_id_geotienda = medicados.id;
                           
                                                      // Busca por el item seleccionado
                                                      column
                                                          .search( val ? val : '', true, false )
                                                          .draw();

                                                      cuenta_filtros_aplicados_geotienda++;

                                                      // Si es el primer filtro aplicado, muestra sección 'filtros aplicados'
                                                      if (cuenta_filtros_aplicados_geotienda == 1) $('#div_filtros_articulos_aplicados_geotienda').show();
                                                  } )

                                                  cuenta_filtro_medicados_geotienda++;   
                                          });   

                                     }
                                     else
                                     {
                                           // Oculta este filtro
                                          filtro_medicados_geotienda.hide();
                                          select_medicados_geotienda.hide();                                   
                                     }
                                     
                                     if (data_medicados_geotienda.medicados.length == 0)
                                     {
                                          // Oculta este filtro
                                          filtro_medicados_geotienda.hide();
                                          select_medicados_geotienda.hide();
                                     }
                                 }
                             });
                          }

                          // Filtro ya aplicado
                          else
                          {
                              // Oculta este filtro
                              filtro_medicados_geotienda.hide();
                              select_medicados_geotienda.hide();

                              // Agrega item seleccionado a filtros aplicados
                              aplicado_medicados_geotienda = $('<div />')
                                  .html('Medicados: ' + filtros_aplicados_medicados_geotienda + '<span style="float:right; font-size: 12px;"><i class="fa fa-remove"></i></span>')
                                  .appendTo( $('#div_filtros_articulos_aplicados_items_geotienda'))

                                  // Funcion para quitar filtro aplicado
                                  .on('click', function ()
                                  { 
                                      $('.div_cargando').show();
                                      drawCallback_geotienda = 0;
                                      // Reset del filtro actual                                    
                                      var val = '';

                                      column
                                          .search( val ? val : '', true, false )
                                          .draw();                                    

                                      cuenta_filtros_aplicados_geotienda--;
                                      filtros_aplicados_medicados_geotienda = '';
                                      filtros_aplicados_medicados_id_geotienda = -1;

                                      //Quita el item
                                      this.remove();

                                      // Si no quedan filtros aplicados, oculta sección 'filtros aplicados'
                                      if (cuenta_filtros_aplicados_geotienda == 0) $('#div_filtros_articulos_aplicados_geotienda').hide();

                                  });
                          }                       
                      }        
                      else
                      {
                          if (filtros_aplicados_medicados_geotienda != '')
                          {
                              cuenta_filtros_aplicados_geotienda--;
                              filtros_aplicados_medicados_geotienda = '';
                              filtros_aplicados_medicados_id_geotienda = -1;
                          }                                
                      }                                   
                  }

              } );

              $('.div_cargando').hide();     
              
              drawCallback_geotienda = 1;         
            }

        },   
    });    
 
}


function volver_add_producto()
{
    $('#div_agregar_producto').hide();
    $('#header_productos_geotienda').hide();     

    $('#div_productos').show();
    $('#header_productos_local').show();
}


$('#selectAll').change(function() 
{
    $(".chk_agregar_producto").prop('checked', $(this).prop("checked"));
});


function agregar_articulo_geotienda_masivo()
{
    // Recupera articulos seleccionados
    var data = { 'articulo[]' : []};

    $(".chk_agregar_producto:checked").each(function() 
    {
        data['articulo[]'].push($(this).val());
    });

    // Valida si se selecciono algun articulo
    if (data['articulo[]'].length > 0)
    {

        $.ajax({
            url : "<?php echo BASE_PATH ?>/Local/ajax_add_articulo_geotienda_masivo/",    
            type: "POST",
            data: data,
            dataType: "JSON",

            success: function(data)
            {
                $('[name="selectAll"]').prop('checked', false);

                // Recarga tablas (sin modificar filtros)
                drawCallback_geotienda = 0;
                var table = $('#table_articulos_geotienda').DataTable();
                table.draw();       

                drawCallback = 0;
                var table_productos = $('#table_productos').DataTable();
                table_productos.draw();        

                recargar_filtros();                      

                aviso('success', 'Productos correctamente agregados al local.');    

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                aviso('danger', textStatus, 'Error al aplicar descuento masivo  (' + errorThrown + ')'); 
            }
        });
    }
    else
    {
        aviso('danger', 'Seleccione por lo menos un producto para agregar al local.'); 
    }
}


function agregar_articulo_geotienda(id)
{
  var url = "<?php echo BASE_PATH ?>/Local/ajax_add_articulo_geotienda/" + id;

    $.ajax({
        url : url ,
        cache: false,
        success: function(data)
        {
              respuesta = jQuery.parseJSON(data);

              // Actualizacion ok
              if(respuesta.status) 
            {
                // Recarga tablas (sin modificar filtros)
                drawCallback_geotienda = 0;
                var table = $('#table_articulos_geotienda').DataTable();
                table.draw();       

                drawCallback = 0;
                var table_productos = $('#table_productos').DataTable();
                table_productos.draw();        

                recargar_filtros();                      

                aviso('success', 'Producto correctamente agregado a la tienda.');      
            }
        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);
        }
    });
}

/* FIN Agregar Producto */


/*  MODIFICAR PRESENTACIONES ARTICULO LOCAL  */
function modificar_presentaciones(id)
{
    var marca_rubro = '';
    var presentacion = '';
    var html_presentaciones = '';

    $.ajax({
      url : "<?php echo BASE_PATH ?>/local/get_articulo_by_id/" + id + '/' + <?php echo $_SESSION['frontend_user_id']; ?>,
      cache: false,
      success: function(data){

        articulo = jQuery.parseJSON(data);

        $("#presentaciones_articulo_id").val(articulo.id); 
        $("#presentaciones_articulo_tipo").val(articulo.tipo); 

        // Si el artículo no tiene marca, muestra el rubro en su lugar
        if (articulo.marca === null) 
            marca_rubro = articulo.rubro;
        else 
            marca_rubro = articulo.marca;

        $("#modal_presentaciones_marca").html(marca_rubro);
        $("#modal_presentaciones_nombre").html(articulo.nombre);

        // Productos creados por el Local
        if(articulo.tipo == 'Local')
          $("#modal_presentaciones_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/locales/fotos/' +  articulo.imagen + '" alt="">');
        // Productos Geotienda
        else
          $("#modal_presentaciones_imagen").html('<img class="img-articulos" src="<?php echo BASE_PATH ?>/assets/img/articulos/fotos/' +  articulo.imagen + '" alt="">');

        // Presentaciones del artículo

        // TODO: mostrar descuento
        for (j = 0; j < articulo.presentacion.length; ++j) 
        {
            // Stock          
            stock = '';
            checked = '';

            if (articulo.presentacion[j].stock == '1') stock = 'checked';

            id = articulo.presentacion[j].id;

            presentacion = articulo.presentacion[j].nombre;

            // Estado de la presentación (habilitada o no)
            if (articulo.presentacion[j].estado == 1) 
            {
                checked = ' checked ';
            }

            html_presentaciones += 
                '<div class="h5 font-w600 text-info col-xs-3">' + 
                '  <label class="css-input css-checkbox css-checkbox-info">' + 
                '      <input type="checkbox" ' + checked + ' articulo_presentacion_id="' + articulo.presentacion[j].articulo_presentacion_id + '" local_articulo_id="' + id + '" class="chk_presentacion_presentaciones" onchange="check_presentaciones();"><span></span> ' + presentacion + 
                '  </label>' +                
                '</div>' +
                '<div class="col-xs-3 push-10">' + 
                '   <input type="number" style="width: 66px; padding-left: 6px; padding-right: 6px" class="form-control" id="txt_precio_presentaciones_'  + id + '" pattern="^[0-9]" min="1" step="1" value ="' + articulo.presentacion[j].precio + '" />' + 
                '</div>' + 
                '<div class="col-xs-4">' + 
                ' <select id="ddl_descuento_presentaciones_'  + id + '" class="form-control">';

                    <?php foreach($descuentos as $descuento){ ?>
                        if (articulo.presentacion[j].descuento_id == <?php echo $descuento->id ?>) 
                          html_presentaciones += '<option selected porcentaje="<?php echo $descuento->porcentaje ?>" value="<?php echo $descuento->id ?>"><?php echo $descuento->nombre ?></option>';
                        else
                          html_presentaciones += '<option porcentaje="<?php echo $descuento->porcentaje ?>" value="<?php echo $descuento->id ?>"><?php echo $descuento->nombre ?></option>';
                    <?php } ?>

                html_presentaciones += ' </select>' + 
                '</div>' +
                '<div class="col-xs-2">' + 
                ' <label class="css-input switch switch-sm switch-info" title="Stock">' + 
                ' <input type="checkbox"' + stock + ' id="chk_stock_presentaciones_'  + id + '"><span></span>'+
                ' </label></div>' +
                '<div style="height:10px; clear: both;"></div>';
        }

        $("#modal_presentaciones_presentaciones").html(html_presentaciones);

        $('#div_presentaciones_articulo_local_error').hide();   

        check_presentaciones();
        $('#modal_presentaciones_articulo_local').modal('show'); 
      } 
    });

}


// Si no hay ninguna presentacion seleccionada avisa al usuario
function check_presentaciones()
{
    if ($(".chk_presentacion_presentaciones:checked").length == 0)
    {
        $('#div_advertencia_presentaciones').show();
    }
    else
    {
        $('#div_advertencia_presentaciones').hide();
    }
    
};


function quitar_producto_local(nombre, articulo_id)
{
    if(confirm('¿Quitar producto "' + nombre + '"?\n\n Podrás volver a agregar el producto cuando desees desde la opción "+ Agregar Producto."'))
    {
      
          $.ajax({
              url : "<?php echo BASE_PATH ?>/Local/ajax_quitar_producto_local/" + articulo_id,
              cache: false,
              success: function(data)
              {
                  respuesta = jQuery.parseJSON(data);

                  // Actualizacion ok
                  if(respuesta.status) 
                  {
                      // Recarga tablas de productos (sin modificar filtros)
                      drawCallback = 0;
                      var table = $('#table_productos').DataTable();
                      table.draw();            

                      recargar_filtros();

                      aviso('success', 'Producto quitado del local.');     
                  }
              },
              // Error no manejado
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $('#modal_error_sistema').modal('show');
                  console.log(errorThrown);               
              }
          });       
    }
}


function eliminar_producto_local(nombre, articulo_id)
{
    if(confirm('¿Eliminar producto "' + nombre + '"?\n\n Podrás volver a crear el producto cuando desees desde la opción "+ Crear Nuevo Producto."'))
    {
      
          $.ajax({
              url : "<?php echo BASE_PATH ?>/Local/ajax_eliminar_producto_local/" + articulo_id,
              cache: false,
              success: function(data)
              {
                  respuesta = jQuery.parseJSON(data);

                  // Actualizacion ok
                  if(respuesta.status) 
                  {
                      // Recarga tablas de productos (sin modificar filtros)
                      drawCallback = 0;
                      var table = $('#table_productos').DataTable();
                      table.draw();            

                      recargar_filtros();

                      aviso('success', 'Producto eliminado del local.');     
                  }
                  else
                  {
                      aviso('danger', respuesta.mensaje, "NO SE PUEDE ELIMINAR ESTE PRODUCTO");  
                      
                      console.log(respuesta.error);
                  }                  
              },
              // Error no manejado
              error: function (jqXHR, textStatus, errorThrown)
              {
                  $('#modal_error_sistema').modal('show');
                  console.log(errorThrown);               
              }
          });       
    }
}


function save_presentaciones_articulo_local()
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_save_presentaciones_articulo_local";

    $('#btn_save_presentaciones_aticulo_local').html('Guardando...');
    $('#btn_save_presentaciones_aticulo_local').attr('disabled', true); 

    // Recupera las presentaciones seleccionadas
    var data = { 'local_articulo_id[]' : [],  'presentacion[]' : [], 'estado[]' : [], 'precio[]' : [], 'porcentaje[]' : [], 'descuento[]' : [], 'stock[]' : [], 'tipo' : $('#presentaciones_articulo_tipo').val()};

    // Recupera todos los id para eliminar las presentaciones del articulo
    $(".chk_presentacion_presentaciones").each(function() 
    {
        precio = '#txt_precio_presentaciones_' + $(this).attr('local_articulo_id');
        descuento = '#ddl_descuento_presentaciones_' + $(this).attr('local_articulo_id');
        porcentaje = $('option:selected', $(descuento)).attr('porcentaje');
        stock = '#chk_stock_presentaciones_' + $(this).attr('local_articulo_id');
        stock_status = $(stock).prop('checked') ? 1:0;
        estado = $(this).prop('checked') ? 1:0;

        data['estado[]'].push(estado);
        data['local_articulo_id[]'].push($(this).attr('local_articulo_id'));
        data['presentacion[]'].push($(this).attr('articulo_presentacion_id'));
        data['precio[]'].push($(precio).val());
        data['descuento[]'].push($(descuento).val());
        data['porcentaje[]'].push(porcentaje);
        data['stock[]'].push(stock_status);
    });  

    $.ajax({
        url : url ,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(data)
        {
            // Actualizacion ok
            if(data.status) 
            {
                $('#div_presentaciones_articulo_local_error').hide();   
                $('#modal_presentaciones_articulo_local').modal('hide');

                $('#btn_save_presentaciones_aticulo_local').html('Aceptar');
                $('#btn_save_presentaciones_aticulo_local').attr('disabled', false);  

                // Recarga tablas de productos (sin modificar filtros)
                drawCallback = 0;
                var table = $('#table_productos').DataTable();
                table.draw();            

                drawCallback_inactivos = 0; 
                var table_inactivos = $('#table_productos_inactivos').DataTable();
                table_inactivos.draw();           

                recargar_filtros();                

                aviso('success', 'Presentaciones correctamente modificadas.');     
            }
            // Fallo en actualizacion
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#presentaciones_articulo_local_error').html(mensaje_error);
                $('#div_presentaciones_articulo_local_error').show();      

                $('#btn_save_presentaciones_aticulo_local').html('Aceptar');
                $('#btn_save_presentaciones_aticulo_local').attr('disabled', false);                                       
            }

        },
        // Error no manejado
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_error_sistema').modal('show');
            console.log(errorThrown);

            $('#btn_save_presentaciones_aticulo_local').html('Aceptar');
            $('#btn_save_presentaciones_aticulo_local').attr('disabled', false);                   
        }
    }); 
}


/* fin MODIFICAR PRESENTACIONES ARTICULO LOCAL  */


/* NUEVO / EDITAR PRODUCTO LOCAL  */
function nuevo_producto_local()
{
    save_method_producto_local = 'add';

    // Reset del formulario
    $('#producto_local_error').html('');
    $('#div_producto_local_error').hide();     
    ocultaSeccionesRubro_producto_local();
    ocultaSeccionesAnimal_producto_local();
    $('[name="rubro_producto_local"]').attr('disabled',false); 

    CKEDITOR.instances['detalle_producto_local'].setData('');

    $('#imagenPreview_producto_local').attr('src', '<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>');  
    $('#imagen_producto_local').val('noimage.gif');
    $('#form_producto_local')[0].reset(); 
    $('#modal_title_producto_local').text('Nuevo Producto'); 
}


function modificar_producto_local(id)
{
    save_method_producto_local = 'update';

    // Reset del formulario
    $('#producto_local_error').html('');
    $('#div_producto_local_error').hide();     
    ocultaSeccionesRubro_producto_local();
    ocultaSeccionesAnimal_producto_local();

   // CKEDITOR.instances['detalle_producto_local'].setData('');

    $('#imagenPreview_producto_local').attr('src', '<?php echo IMG_PATH . 'articulos/miniaturas/noimage.gif' ?>');  
    $('#imagen_producto_local').val('noimage.gif');
    $('#form_producto_local')[0].reset(); 

    $('#modal_title_producto_local').text('Modificar Producto'); 


    $.ajax({
        url : "<?php echo BASE_PATH ?>/admin/Articulo/ajax_edit/" + id,        
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {   
            $('[name="id_producto_local"]').val(data.id);
            $('[name="nombre_producto_local"]').val(data.nombre);
            //$('[name="codigo"]').val(data.codigo);
            $('[name="detalle_producto_local"]').val(data.detalle);
            $('[name="rubro_producto_local"]').val(data.id_rubro);
            $('[name="hd_rubro_producto_local"]').val(data.id_rubro);
            $('[name="rubro_producto_local"]').attr('disabled',true); 
            $('[name="animal_producto_local"]').val();
            $('[name="animal_producto_local"]').val(data.id_animal).change();
            $('[name="marca_producto_local"]').val(data.id_marca);                                 
            $('[name="edad_producto_local"]').val(data.edad);
            $('[name="imagen_producto_local"]').val(data.imagen);  

            $('#imagenPreview_producto_local').attr('src', '<?php echo IMG_PATH . 'articulos/locales/fotos/' ?>'  + data.imagen);      

            CKEDITOR.instances.detalle_producto_local.setData($('[name="detalle_producto_local"]').val());

            medicados = (data.medicados == '1' ? true : false);

            $('#medicados_producto_local_null').prop('checked', true);   

            if (data.medicados == '1') $('#medicados_producto_local_si').prop('checked', true);    
            if (data.medicados == '0') $('#medicados_producto_local_no').prop('checked', true);    

            $.each(data.presentaciones_view.split(","), function(i,e){
                $("#presentacion_producto_local option[value='" + e + "']").prop("selected", true);
            });      

            $.each(data.razas_view.split(","), function(i,e){
                $("#raza_producto_local option[value='" + e + "']").prop("selected", true);
            });    

            $.each(data.tamanios_view.split(","), function(i,e){
                $("#tamanio_producto_local option[value='" + e + "']").prop("selected", true);
            });   

            cambioRubro_producto_local(data.id_rubro);
            cambioAnimal_producto_local(data.id_animal);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           
            aviso('danger', textStatus, "ERROR AL CARGAR DATOS (' + errorThrown + ')"); 
        }
    });
}


function elegirImagen_producto_local(nombreImagen)
{
    $('#imagenPreview_producto_local').attr('src', '<?php echo IMG_PATH . 'articulos/locales/miniaturas/' ?>'  + nombreImagen);   
    $('#imagen_producto_local').val(nombreImagen);

    $('#modal_imagenes_producto_local').modal('hide');
}


function cambioRubro_producto_local(id)
{
    // Inicialmente oculta todas las secciones que dependen del Rubro seleccionado
    ocultaSeccionesRubro_producto_local();

    // Muestra las secciones habilitadas para el Rubro seleccionado
    if (typeof id !== "undefined")
    {
         $("#hd_rubro_producto_local").val(id);

        if (id != "")
        {
          $.ajax({
              url : "<?php echo BASE_PATH ?>/admin/Rubro/ajax_edit/" + id,        
              type: "GET",
              dataType: "JSON",
              success: function(data)
              {
                  // Raza
                  conraza = (data.conraza == '1' ? true : false);
                  if (conraza && animal_conraza) $('[id="divRaza_producto_local"]').show();

                  rubro_conraza = conraza;

                  // Tamaño
                  contamanios = (data.contamanios == '1' ? true : false);
                  if (contamanios && animal_contamanio) $('[id="divTamanio_producto_local"]').show();

                  rubro_contamanio = contamanios;

                  // Marca
                  conmarca = (data.conmarca == '1' ? true : false);

                  // Para productos creados por el local habilita solo la marca 'Otras Marcas'
                  if (conmarca)
                  {
                      $('[id="divMarca_producto_local"]').show();
                      $("#con_marca_producto_local").val("1");
                  }

                  // Presentación
                  // Para productos creados por el local Presentación no se utiliza en ningun rubro
                  conpresentacion = false; 

                  // Edad
                  conedad = (data.conedad == '1' ? true : false);
                  if (conedad) $('[id="divEdad_producto_local"]').show();

                  // Medicados
                  conmedicados = (data.conmedicados == '1' ? true : false);
                  if (conmedicados) $('[id="divMedicados_producto_local"]').show();    

                  cambioAnimal_producto_local($('[id="animal_producto_local"]').val());
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                  
              }
          });
        }
    }
}


function ocultaSeccionesRubro_producto_local()
{
    $('[id="divRaza_producto_local"]').hide(); 
    $('[id="divTamanio_producto_local"]').hide(); 
    $('[id="divPresentacion_producto_local"]').hide(); 
    $('[id="divMarca_producto_local"]').hide(); 
    $('[id="divEdad_producto_local"]').hide(); 
    $('[id="divMedicados_producto_local"]').hide(); 

    $("#con_marca_producto_local").val("0");
    $("#con_presentacion_producto_local").val("0");
}


function ocultaSeccionesAnimal_producto_local()
{
    $('[id="divRaza_producto_local"]').hide(); 
    $('[id="divTamanio_producto_local"]').hide(); 
}


function cambioAnimal_producto_local(id)
{
    // Inicialmente oculta todas las secciones que dependen del Animal seleccionado
    ocultaSeccionesAnimal_producto_local();

    // Muestra las secciones habilitadas para el Animal seleccionado
    if (typeof id !== "undefined")
    {
        if (id != "")
        {
          $.ajax(
          {
              url : "<?php echo BASE_PATH ?>/admin/Animal/ajax_edit/" + id,        
              type: "GET",
              dataType: "JSON",
              success: function(data)
              {
                  // Raza
                  conraza = (data.conraza == '1' ? true : false);
                  if (conraza && rubro_conraza) $('[id="divRaza_producto_local"]').show();

                  animal_conraza = conraza;

                  // Tamaño
                  contamanios = (data.contamanios == '1' ? true : false);
                  if (contamanios && rubro_contamanio) $('[id="divTamanio_producto_local"]').show();   

                  animal_contamanio = contamanios;
              },
              error: function (jqXHR, textStatus, errorThrown)
              {
                  aviso('danger', textStatus, 'Error al cargar datos (' + errorThrown + ')'); 
                  
              }
          });
        }
    }
}


function save_producto_local()
{
    var url;
    var mensaje;

    for (instance in CKEDITOR.instances) {
        CKEDITOR.instances[instance].updateElement();
    }

    $('#btn_save_producto_local').text('Guardando...'); 
    $('#btn_save_producto_local').attr('disabled',true); 

    if(save_method_producto_local == 'add') 
    {
        url = "<?php echo BASE_PATH ?>/Articulo/ajax_add_producto_local";
        mensaje = 'Producto creado.';
    } 
    else 
    {
        url = "<?php echo BASE_PATH ?>/Articulo/ajax_update_producto_local";
        mensaje = 'Producto modificado.';
    }

    // ajax adding data to database
    var formdata = new FormData(document.getElementById('form_producto_local'));

    $.ajax({
        url : url ,
        type: "POST",
        data: formdata,
        dataType: "JSON",
        //Options to tell jQuery not to process data or worry about content-type.
        cache: false,
        contentType: false,
        processData: false,        
        success: function(data)
        {
            if(data.status)
            {
                $('#modal_producto_local').modal('hide');

                // Recarga tabla de productos (sin modificar filtros)
                drawCallback = 0;
                var table = $('#table_productos').DataTable();
                table.draw();            

                recargar_filtros();

                aviso('success', mensaje);  

                // Avisa que hay presentaciones que no se eliminaron porque están siendo utilizadas
                if (save_method_producto_local == 'update' && data.aviso_borrar_presentaciones != '')
                {
                    aviso('danger', 'Hay presentaciones que no se quitaron porque están activas en el local.');                      
                }                
            }
            else
            {
                mensaje_error = '';

                for (var i = 0; i < data.error.mensaje.length; i++) 
                {
                    mensaje_error += data.error.mensaje[i] + '<br/>';
                }

                $('#producto_local_error').html(mensaje_error);
                $('#div_producto_local_error').show(); 
            }

            $('#btn_save_producto_local').text('Aceptar'); 
            $('#btn_save_producto_local').attr('disabled',false); 

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error al crear o modificar Producto (' + errorThrown + ')');
            $('#btn_save_producto_local').text('Aceptar'); 
            $('#btn_save_producto_local').attr('disabled',false); 

        }
    });
}
/* fin NUEVO PRODUCTO LOCAL  */

/* fin PRODUCTOS DEL LOCAL */


/* CALENDARIO LOCAL */
function crear_calendario(servicio)
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_crear_calendario_local";

    var data_post = 
    {
        'servicio': servicio
    };

    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            switch (servicio)
            {
                 case 'clinica':
                    $('#div_calendario_clinica').html(data.calendario);
                    break;

                 case 'peluqueria':
                    $('#div_calendario_peluqueria').html(data.calendario);
                    break;
            }           
             
        },
        // Error al cargar calendario
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_error_sistema').modal('show');
        }
    });
}


// dia: ej 20170819
// dia_mostrar: 19/08/2017
// abierto: abierto (1) o cerrado (0) segun tabla local_horario_excepcion_dia (prioridad) o local_horario (si no hay excepcion para el dia)
function ver_dia_turno(dia, dia_mostrar, abierto, servicio, hay_turnos)
{
    $('#form_dia_turno')[0].reset(); 

    var url = "<?php echo BASE_PATH ?>/Local/ajax_ver_dia_turno";

    var data_post = 
    {
        'fecha': dia,
        'fecha_mostrar' : dia_mostrar,
        'servicio': servicio
    };


    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            $('.modal-title-dia-turnos').text(servicio.toUpperCase() + '  - Agenda ' + dia_mostrar);

            $("#hd_dia_turno_servicio").val(servicio);          
            $("#hd_dia_turno_dia").val(dia_mostrar);          
            
            $('#div_dia_turno_horarios1').html(data.horarios1);
            $('#div_dia_turno_horarios2').html(data.horarios2);

            // Local abierto
            if (abierto == 1) 
            {
                $('#chk_dia_turno_abierto').prop('checked', true);
                $('[name="hd_dia_turno_abierto"]').val('1'); 
                $('#ddl_horario1').show(); 
                $('#ddl_horario2').show(); 

                $('#ddl_horario1').multiselect({
                    includeSelectAllOption: false
                });     

                $('#ddl_horario2').multiselect({
                    includeSelectAllOption: false
                });                   

                $('#ddl_horario1').next().addClass('open');         
                $('#ddl_horario2').next().addClass('open');         
                $('.multiselect').hide();                
            }
            else
            {
                $('#chk_dia_turno_abierto').prop('checked', false);
                $('[name="hd_dia_turno_abierto"]').val('0');
                $('#ddl_horario1').hide();
                $('#ddl_horario2').hide();
            }     

            // hay_turnos: > 0 significa que hay turnos reservados para ese dia => no permite 'cerrar' el local para ese dia
            if (hay_turnos > 0)
            {
                $('#lbl_tienda_abierta_siempre').show();
                $('#lbl_tienda_abierta').hide();
            }
            else
            {
                $('#lbl_tienda_abierta_siempre').hide();
                $('#lbl_tienda_abierta').show();
            }


            // No permite hacer clic en el modal, ya que oculta la lista de horarios del dia
            $('.sin_click').click(false);
            
            $('#modal_dia_turno').modal('show');              
             
        },
        // Error al cargar calendario
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_dia_turno').modal('hide');
            $('#modal_error_sistema').modal('show');
        }
    });

}


function cancela_actualizar_dia_turno()
{
   $('#modal_dia_turno').modal('hide'); 
}

function actualizar_dia_turno()
{
    $('#btn_dia_turno').text('Guardando...'); 
    $('#btn_dia_turno').attr('disabled',true); 

    var url = "<?php echo BASE_PATH ?>/Local/ajax_actualizar_dia_turno";

    // Permite que los horarios de turnos ya reservados se graben en la tabla de excepciones
    $("#ddl_horario1 option:selected").removeAttr('disabled');
    $("#ddl_horario2 option:selected").removeAttr('disabled');

    $.ajax({
        url : url ,
        type: "POST",
        data: $('#form_dia_turno').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            $('#btn_dia_turno').text('Guardar');
            $('#btn_dia_turno').attr('disabled',false);

            $('#modal_dia_turno').modal('hide');  

            fecha = $("#hd_dia_turno_dia").val().replace("/", "-");
            fecha = fecha.replace("/", "-");

            calendario_cambia_mes_local(fecha, $("#hd_dia_turno_servicio").val())

            aviso('success', 'Horario Actualizado.');                   

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            $('#modal_detalle_turno').modal('hide');  
            aviso('danger', textStatus, 'Error al actualizar horario: (' + errorThrown + ')'); 

            $('#btn_dia_turno').text('Guardar');
            $('#btn_dia_turno').attr('disabled',false); 
        }
    });     
}


function calendario_cambia_mes_local(fecha, servicio)
{
    var url = "<?php echo BASE_PATH ?>/Local/ajax_calendario_cambia_mes_local";

    var data_post = 
    {
        'fecha': fecha,
        'servicio': servicio
    };

    $.ajax({
        url : url ,
        type: "POST",
        data: data_post, 
        dataType: "JSON",
        success: function(data)
        {
            switch (servicio)
            {
                 case 'clinica':
                    $('#div_calendario_clinica').html(data.calendario);
                    break;

                 case 'peluqueria':
                    $('#div_calendario_peluqueria').html(data.calendario);
                    break;
            }           
             
        },
        // Error al cargar calendario
        error: function (jqXHR, textStatus, errorThrown)
        {
            console.log(errorThrown);
            $('#modal_detalle_turno').modal('hide'); 
            $('#modal_error_sistema').modal('show');
        }
    });
}
/* fin CALENDARIO LOCAL */

</script>