<?php if(isset($_SESSION['frontend_admin']) && $_SESSION['frontend_admin'] == 1): ?>
    <button class="btn btn-semitransparente-naranja pull-right portadas" type="button" id="btn_portada" onclick="portada('portada_faq')">
        <i class="si si-camera text-white"></i>
    </button>
   
<?php endif ?>

<!-- version DESKTOP -->
<span class="hidden-xs">
    <div style="height: <?php echo $portada_altura; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: <?php echo $portada_posicion; ?>" class="portada-posicion">
            
            <div class="portada-title" style="top: <?php echo $portada_top; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Preguntas Frecuentes</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<!-- version MOBILE -->
<span class="visible-xs">
    <div style="height: <?php echo $portada_altura_mobile; ?>px; background-color: rgba(0, 0, 0, <?php echo $transparencia_portada ?>);" class="portada-layer"></div>
    <div class="portada-wrapper">
        <div style="height: <?php echo $portada_altura_mobile; ?>px" id="div_portada">

            <img src="./assets/img/portadas/<?php echo $portada; ?>" id="img_portada" style="top: 0px; width: 100%; margin-top: 0;" class="portada-posicion visible-xs">
            
            <div class="portada-title" style="top: <?php echo $portada_top_mobile; ?>px;">
                <div class="centrado-h" >
                    <h2 class="font-w600" style="color: <?php echo $portada_color; ?>">Preguntas Frecuentes</h2>
                </div>  
            </div>        
        </div>  
    </div>
</span>

<section class="content content-full content-boxed">  
    <div class="push-20">

        <div class="h4 push-30-t push-20-l text-gris">¿Cómo hago un pedido? </div>
        <div class="h5 push-20-t push-20-l text-gris"><b>¡Es realmente muy fácil!</b> Sólo sigue las instrucciones que se describen a continuación: </div>

        <div class="h5 push-20-t push-10 text-gris">
            <ol>
              <li class="push-10">Ingresa tu ubicación y presiona el botón "Buscar". Serás redirigido al listado que muestra todos los Pet Shop o Veterinarias disponibles en tu área, allí puedes filtrar por: Categoría, Servicios, Rubros, según tus necesidades.</li>
              <li class="push-10">Elige el negocio al que le quieras realizar el pedido de compra o reserva de turno de algún servicio que brinde.</li>
              <li class="push-10">Una vez dentro del negocio puedes filtrar según tus necesidades ya sea en el área de Productos o Servicios. En el caso de compras de productos selecciona los productos que deseas comprar presionando el botón del carrito (Agregar al pedido) luego si quieres comprar más de uno de los productos seleccionados, puedes ingresar la cantidad que deseas en el espacio para la cantidad en la ventana (Mi Pedido). Cuando hayas terminado presiona el botón (Finalizar) completa los datos necesarios para que el Pet Shop o Veterinaria pueda realizar el delivery o si deseas pasar a retirarlo tu mismo el pedido que realizaste. Luego te llegara un email a tu casilla de correo con la (Confirmación del pedido) y la copia de tu pedido. En el caso de reserva de un turno selecciona el servicio que necesita tu mascota desde los filtros, una vez elegido presiona el botón Agendar (Agendar Turno) y allí selecciona el día que este libre y el horario, y por ultimo presiona el botón Agendar Turno…  Automáticamente enviaras la solicitud de reserva al negocio. Luego te llegara un email a tu casilla de correo con la (Confirmación de turno).</li>
            </ol> 
        </div>

        <div class="h4 push-50-t push-20-l text-gris">¿Cómo contacto a un negocio por mi pedido de compra o reserva de un turno?</div>
        <div class="h5 push-15-t push-20-l text-gris">Una vez que hayas enviado el pedido de compra o reserva de turno, desde tu cuenta en la pestaña ‘‘Mis Pedidos’’ vas a poder mandarle mensajes sobre tu pedido de compra. Y en la pestaña de ‘‘Mis Turnos’’ podrás hacer lo mismo sobre el turno que reservaste además vas a recibir un email con una copia del pedido que realizaste (Confirmación de Pedido) y en el caso de una reserva de turno vas a recibir un email con una copia del turno reservado (Confirmación de Turno) allí estará los datos del negocio.</div>
 
        <div class="h4 push-50-t push-20-l text-gris">¿Cómo sé si el Pet Shop o Veterinaria ha recibido mi pedido de compra?</div>
        <div class="h5 push-15-t push-20-l text-gris">Cuando realizas un pedido de compra o reserva de turno por un servicio en GeoTienda , el mismo se envía automáticamente al negocio y una copia de tu pedido es enviado a tu casilla de correo. GeoTienda a través de su proceso de seguimiento de pedidos, asegura que el pedido haya llegado al negocio.</div>

        <div class="h4 push-50-t push-20-l text-gris">Hice un pedido, pero me di cuenta que quiero cambiar, agregar o sacar algo, ¿cómo hago? </div>
        <div class="h5 push-15-t push-20-l text-gris">Desde tu cuenta en la pestaña ‘‘Mis Pedidos’’ dirígete al pedido que realizaste y cancélalo luego vuelve a realizarlo ya que una vez procesado, el pedido es enviado automáticamente. Y sino llama rápidamente al Pet Shop o Veterinaria y comunícale lo sucedido.</div>   

        <div class="h4 push-50-t push-20-l text-gris">Hice una reserva de turno para un servicio, pero me di cuenta que no puedo asistir ¿cómo hago? </div>
        <div class="h5 push-15-t push-20-l text-gris">Desde tu cuenta en la pestaña ‘‘Mis Turnos’’ dirígete al turno que realizaste y cancélalo luego vuelve a realizarlo ya que una vez procesado, la reserva es enviada automáticamente. Y sino llama rápidamente al Pet Shop o Veterinaria y comunícale lo sucedido.</div>      

        <div class="h4 push-50-t push-20-l text-gris">¿Qué pasa si no asisto o cancelo con tiempo una reserva de turno que había hecho?</div>
        <div class="h5 push-15-t push-20-l text-gris">Si no asistís al negocio y nos has cancelado la reserva con anterioridad, te bloquearemos el acceso futuro a la plataforma, ya que perjudicas gravemente al negocio y a los demás usuarios de GeoTienda.</div>    

        <div class="h4 push-50-t push-20-l text-gris">Tengo quejas con respecto a mi pedido o reserva de turno  – ¿A quién debo dirigirlas?</div>
        <div class="h5 push-15-t push-20-l text-gris">A GeoTienda le importa la satisfacción de los usuarios con respecto a los Pet Shop o Veterinarias adheridos. Si tuviste algún problema con respecto a tu pedido de compra o reserva y no fue solucionado por el negocio, por favor contáctanos a <a href="mailto: contacto@geotienda.com">contacto@geotienda.com</a>.</div>                             
        <div class="h4 push-50-t push-20-l text-gris">¿Qué pasa si el Pet Shop o Veterinaria no puede completar el pedido de compra, reserva, o delivery?</div>
        <div class="h5 push-15-t push-20-l text-gris">Si el negocio cuenta con algún inconveniente para completar el pedido de compra, delivery, o reserva de algún servicio, se comunicará con usted a la brevedad. Por eso es muy importante que su teléfono de contacto sea real y actualizado y este ingresado en la pestaña ‘‘Mis Datos’’ de su cuenta. Al mismo tiempo, GeoTienda realizará el seguimiento del evento para que el problema sea resuelto cuanto antes.</div> 

        <div class="h4 push-50-t push-20-l text-gris">¿Realizar pedidos o reservas de turno es gratis?</div>
        <div class="h5 push-15-t push-20-l text-gris">Si es gratis.</div>          

        <div class="h4 push-50-t push-20-l text-gris">¿A quién le pago mi pedido de compra o reserva de turno?
</div>
        <div class="h5 push-15-t push-20-l text-gris">El pago será recibido por el negocio en la entrega del delivery o en la tienda al momento de retirarlo. Lo mismo si fue por un servicio que contrato.</div>          

        <div class="h4 push-50-t push-20-l text-gris">¿Cuáles son las formas de pagos?</div>
        <div class="h5 push-15-t push-20-l text-gris">El método de pago es depende de cada negocio, por lo que podrás chequear en sus respectivos perfiles. Ya sea en efectivo o tarjeta de crédito.</div>          

        <div class="h4 push-50-t push-20-l text-gris">¿Qué es la cartilla de vacunas?</div>
        <div class="h5 push-15-t push-20-l text-gris">La cartilla de vacunas es la misma que te da tu veterinario para el control de las vacunas de tu mascota. Allí estarán todas las vacunas aplicadas y las próximas que le tocan.</div>          

        <div class="h4 push-50-t push-20-l text-gris">¿Si tengo más de una mascota y la ingreso a la cartilla de vacunas tengo que pagar?</div>
        <div class="h5 push-15-t push-20-l text-gris">No. La cartilla de vacunas es totalmente gratuita, y puedes ingresar todas las mascotas que tengas.</div>

        <div class="h4 push-50-t push-20-l text-gris">¿Cómo me entero que el veterinario que atiende a mi mascota esta en GeoTienda?</div>
        <div class="h5 push-15-t push-20-l text-gris">Una vez que ingresaste a tu mascota en la cartilla, entra en el botón ‘‘Ver Vacunas’’ y allí podrás asignarle una veterinaria nueva o buscar a tu veterinario de confianza.</div>

        <div class="h4 push-50-t push-20-l text-gris">¿Qué hago si el veterinario que conozco no aparece en la lista?</div>
        <div class="h5 push-15-t push-20-l text-gris">Nos puedes avisar a <a href="mailto: contacto@geotienda.com">contacto@geotienda.com</a> y en ‘‘Asunto’’ poner Sugerir negocio, déjanos los datos del comercio así un represéntate se comunica con él. </div>

        <div class="h4 push-50-t push-20-l text-gris">¿Las vacunas aplicadas las tengo que ingresar yo en la cartilla de vacunas?</div>
        <div class="h5 push-15-t push-20-l text-gris">No. Las vacunas las ingresa el veterinario.</div>        
    </div>

    </div>   
    <div class="text-center push-50-t push-50">
        <img src="<?php echo BASE_PATH ?>/assets/img/frontend/patita.png" alt="GeoTienda">
    </div>         
</section>