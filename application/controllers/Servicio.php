<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/servicio_model','servicio');
		$this->load->model('ABM/servicioitem_model','servicio_item');	
		$this->load->model('Usuarios/localservicioitem_model','local_servicio_item');					
	}


	public function ajax_list_items()
	{
		$data = new stdClass();

		$data->servicios = $this->servicio->get_all();

		// Carga items de cada servicio
		foreach ($data->servicios as $servicio) 
		{
			$servicio->items = $this->servicio_item->get_by_servicio($servicio->id);

			// Si el local activo tiene un item asociado, carga los datos propios del local
			foreach ($servicio->items as $item) 
			{
				$item_local = $this->local_servicio_item->get_by_servicio_item_local($item->id, $_SESSION['frontend_user_id']);

				if ($item_local)
				{
					$item->local_servicio_item_id = $item_local->id;
					$item->duracion = $item_local->duracion;
					$item->precio = $item_local->precio;
				}
				else
				{
					$item->precio = '';					
				}
			}			
		}

		echo json_encode($data);	
	}
}