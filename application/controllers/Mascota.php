<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mascota extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/mascota_model','mascota');
		$this->load->model('ABM/vacuna_model','vacuna');
		$this->load->model('Tools/media_model','media');
		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');	
		$this->load->model('Actividad/notificacion_model','notificacion');	
		$this->load->model('Usuarios/frontend_model','frontend_user');		

		$this->load->library('validations');
		$this->load->library('email_geotienda');		
		$this->load->helper('url');
		$this->load->helper('cookie');				
	}


	// DASHBOARD CLIENTE - TAB 'MASCOTAS'
	public function ajax_list()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->mascota->get_datatables($_SESSION['frontend_user_id']);

			$datatable = array();
			$no = $_POST['start'];

			foreach ($list as $mascota) {
				$no++;
				$row = array();
				$acciones = '';
				$nombre_foto = '';
				$foto = '';
				
				$nombre_foto = $mascota->nombre;

				if ($mascota->foto != '')
					$foto = $mascota->foto;
				elseif (strtolower($mascota->tipo) == 'gato')
					$foto = 'gato.png';
				else
					$foto = 'perro.png';

				$nombre_foto .= '<br /><br /><img src="'. IMG_PATH . 'mascotas/miniaturas/'. $foto .'" width="100%" style="max-width:160px">';

				$row[] = $nombre_foto;

				if ($mascota->local != '')
					$row[] = $mascota->local;
				else 
					$row[] = 'No asignado';


		        // Resalta vacunas del dia
		        $proxima_vacuna = $this->get_proxima_vacuna($mascota->id);
		        $hoy = $proxima_vacuna == date('d/m/Y') ? "&nbsp;&nbsp;&nbsp;<div class='label label-warning'>HOY</div>": "";

		        $fecha = $proxima_vacuna . $hoy;

		        $row[] = $fecha;	

				$acciones = '<a class="btn btn-vacunas push-10-l push-5" href="javascript:void(0)" title="Ver Vacunas" onclick="cartilla_mascota('."'".$mascota->id."'".')"><i class="si si-notebook"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Vacunas&nbsp</a>'; 

				$acciones .= '<br /><a class="btn btn-success push-10-l push-5 w-150" href="javascript:void(0)" title="Modificar Datos" onclick="modificar_mascota('."'".$mascota->id."'".')"><i class="fa fa-edit"></i>&nbsp;&nbsp;Modificar Datos</a>'; 

				$acciones .= '<br /><a class="btn btn-danger push-10-l w-150" href="javascript:void(0)" title="Borrar Mascota" onclick="borrar_mascota('."'".$mascota->nombre."'".',' .$mascota->id. ')"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;&nbsp;Borrar Mascota</a>'; 

				$row[] = $acciones;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	// DASHBOARD LOCAL - TAB 'VACUNAS'
	public function ajax_list_local()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		 // Valida que el local tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->mascota->get_datatables_local($_SESSION['frontend_user_id']);

			$datatable = array();
			$no = $_POST['start'];

			foreach ($list as $mascota) {
				$no++;
				$row = array();
				$acciones = '';

				$row[] = $mascota->nombre;
				$row[] = $mascota->tipo;
				$row[] = $mascota->cliente;

		        // Resalta vacunas del dia
		        $proxima_vacuna = $this->get_proxima_vacuna($mascota->id);
		        $hoy = $proxima_vacuna == date('d/m/Y') ? "&nbsp;&nbsp;&nbsp;<div class='label label-warning'>HOY</div>": "";
		        $fecha = $proxima_vacuna . $hoy;

		        $row[] = $fecha;

				$acciones = '<a class="btn btn-vacunas push-10-l push-5" href="javascript:void(0)" title="Ver Cartilla" onclick="cartilla_mascota('."'".$mascota->id."'".')" data-toggle="modal" data-target="#modal_cartilla_mascota"><i class="si si-notebook"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ver Cartilla&nbsp</a>'; 

				$row[] = $acciones;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	public function ajax_add()
	{
		$data = array();
		$data['status'] = TRUE;
		$foto = '';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		// Valida datos ingresados
		$error = $this->_validate();

		// Validación OK
		if ($error->status){

			$fecha = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('nacimiento_mascota')))));

			// Valida si se seleccionó una foto y si la foto es precargada o nueva
			// Foto nueva
			if(is_uploaded_file($_FILES['foto_mascota']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$foto = $this->media->resize_image_big_thumb($_FILES['foto_mascota'], $thumb, $big, 'mascotas', $_SESSION['frontend_user_id']);
			}
			// Foto precargada
			else{
			
				$foto = $this->input->post('foto_mascota_precargada');		
			}

			// Crea mascota
			$data_insert = array(
				'mascota_nombre' => trim($this->input->post('nombre_mascota')),
				'mascota_animal_id' => trim($this->input->post('animal_mascota')),
				'mascota_raza' => trim($this->input->post('raza_mascota')),
				'mascota_sexo' => trim($this->input->post('sexo_mascota')),
				'mascota_nacimiento' => $fecha,
				'mascota_pelaje' => trim($this->input->post('pelaje_mascota')),
				'mascota_talla' => trim($this->input->post('talla_mascota')),
				'mascota_cliente_id' => $_SESSION['frontend_user_id'],
				'mascota_foto' => $foto
			);

			$this->mascota->save($data_insert);

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_update()
	{
		$data = array();
		$data['status'] = TRUE;
		$foto = '';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		// Valida datos ingresados
		$error = $this->_validate();

		// Validación OK
		if ($error->status){

			$fecha = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('nacimiento_mascota')))));

			// Valida si se seleccionó una foto y si la foto es precargada o nueva
			// Foto nueva
			if(is_uploaded_file($_FILES['foto_mascota']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$foto = $this->media->resize_image_big_thumb($_FILES['foto_mascota'], $thumb, $big, 'mascotas', $_SESSION['frontend_user_id']);
			}
			// Foto precargada
			else{
			
				$foto = $this->input->post('foto_mascota_precargada');		
			}

			
			$data_update = array(
				'mascota_nombre' => trim($this->input->post('nombre_mascota')),
				'mascota_animal_id' => trim($this->input->post('animal_mascota')),
				'mascota_raza' => trim($this->input->post('raza_mascota')),
				'mascota_sexo' => trim($this->input->post('sexo_mascota')),
				'mascota_nacimiento' => $fecha,
				'mascota_pelaje' => trim($this->input->post('pelaje_mascota')),
				'mascota_talla' => trim($this->input->post('talla_mascota')),
				'mascota_cliente_id' => $_SESSION['frontend_user_id'],
				'mascota_foto' => $foto
			);

			$this->mascota->update(array('mascota_id' => $this->input->post('id_mascota')), $data_update);

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}

	public function ajax_asignar_local()
	{
		$data = array();
		$data['status'] = TRUE;

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				
		
		// Valida datos ingresados
		$error = $this->valida_asignar_local();

		// Validación OK
		if ($error->status){

			// Fecha y hora local
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			$data_update = array('mascota_local_id' => $this->input->post('local_id'));

			$this->mascota->update(array('mascota_id' => $this->input->post('mascota_id')), $data_update);	

			// Recupera datos del local
			$local = $this->local->get_by_id($this->input->post('local_id'));

			// Recupera datos del cliente
			$cliente = $this->cliente->get_by_id($_SESSION['frontend_user_id']);	

			// Recupera datos de la mascota
			$mascota = $this->mascota->get_by_id($this->input->post('mascota_id'));

			// Envía mail de aviso al local
			$msg_mail = $this->email_geotienda->local_nueva_mascota(		
				$local->email,
				$cliente->nombre,
				$local->nombre, 
				$mascota->nombre);


			// NOTIFICACIONES
			// al Administrador
			$data_notificacion = array(
				'notificacion_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_cliente_id' => $cliente->id,
				'notificacion_local_id' => $local->id,
				'notificacion_texto' => '<b>' . $cliente->nombre . ' ' . $cliente->apellido . '</b> asignó su mascota ' . $mascota->nombre . ' al local <b>' . $local->nombre. '</b>',
				'notificacion_tipo' => 'mascota asignada'
			);	

			$this->notificacion->save($data_notificacion);			

			// al Local
			$data_notificacion_local = array(
				'notificacion_local_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_local_cliente_id' => $cliente->id,			
				'notificacion_local_local_id' => $local->id,
				'notificacion_local_texto' => '<b>' . $cliente->nombre . ' ' . $cliente->apellido . '</b> asignó su mascota ' . $mascota->nombre . ' a tu local',
				'notificacion_local_tipo' => 'mascota asignada'
			);		

			$this->notificacion_local->save($data_notificacion_local);	

			if ($msg_mail == '')							
			{				
				$data['email'] = TRUE;
			}
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	private function _validate()
	{

		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('nombre_mascota')) == '')
		{
			$error->mensaje[] = 'Ingresá un Nombre';
			$error->status = FALSE;
		}		

		if(trim($this->input->post('animal_mascota')) == '')
		{
			$error->mensaje[] = 'Seleccioná un Tipo de Mascota';
			$error->status = FALSE;
		}

		if(trim($this->input->post('raza_mascota')) == '')
		{
			$error->mensaje[] = 'Ingresá una Raza';
			$error->status = FALSE;
		}

		if(trim($this->input->post('sexo_mascota')) == '')
		{
			$error->mensaje[] = 'Seleccioná un Sexo';
			$error->status = FALSE;
		}

		if(trim($this->input->post('nacimiento_mascota')) == '')
		{
			$error->mensaje[] = 'Ingresá una Fecha de Nacimiento';
			$error->status = FALSE;
		}


		if(trim($this->input->post('pelaje_mascota')) == '')
		{
			$error->mensaje[] = 'Ingresá un Pelaje';
			$error->status = FALSE;
		}


		if(trim($this->input->post('talla_mascota')) == '')
		{
			$error->mensaje[] = 'Seleccioná una Talla';
			$error->status = FALSE;
		}		
		return $error;		
	}
	

	private function valida_asignar_local()
	{

		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('local_id')) == '')
		{
			$error->mensaje[] = 'Seleccioná un local';
			$error->status = FALSE;
		}		

		return $error;		
	}

	public function get_by_id($id){

		$data = $this->mascota->get_by_id($id);

		$nacimiento = new DateTime($data->nacimiento);
		$data->nacimiento = $nacimiento->format('d/m/Y');

		// Fecha próxima vacuna
		$data->proxima_vacuna = $this->get_proxima_vacuna($id);

		echo json_encode($data);	
	}	


	public function get_by_vacuna_id($vacuna_id){

		$data = $this->mascota->get_by_vacuna_id($vacuna_id);

		/*
		$nacimiento = new DateTime($data->nacimiento);
		$data->nacimiento = $nacimiento->format('d/m/Y');

		// Fecha próxima vacuna
		$data->proxima_vacuna = $this->get_proxima_vacuna($id);
		*/
		
		echo json_encode($data);	
	}	

	

	public function get_proxima_vacuna($id)
	{
		// Fecha próxima vacuna
		$data_vacuna = $this->vacuna->get_proxima_por_mascota($id);

		if ($data_vacuna->fecha)
		{
			$fecha = new DateTime($data_vacuna->fecha);
			return $fecha->format('d/m/Y');				
		}
		else
		{
			return 'no definida';
		}
	}


	public function ajax_delete($id)
	{
        $data = array();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		        

		$resultado = $this->mascota->delete_by_id($id);

		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	public function delete_foto($name)
	{
		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		// Valida si la imagen $name está siendo utilizada en alguna mascota
		$count = $this->mascota->check_foto($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta foto ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'mascotas')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar foto.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);

	}	


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}	
}