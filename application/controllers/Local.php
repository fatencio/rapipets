<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Local extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('Usuarios/localarticulo_model','local_articulo');	
		$this->load->model('Usuarios/localservicioitem_model','local_servicio_item');
		$this->load->model('Usuarios/localhorario_model','local_horario');
		$this->load->model('Usuarios/localmediopago_model','local_medio_pago');				
		$this->load->model('ABM/animal_model','animal');
		$this->load->model('ABM/marca_model','marca');					
		$this->load->model('ABM/rubro_model','rubro');				
		$this->load->model('ABM/tamanio_model','tamanio');
		$this->load->model('ABM/raza_model','raza');			
		$this->load->model('ABM/presentacion_model','presentacion');			
		$this->load->model('ABM/articulo_model','articulo');	
		$this->load->model('ABM/banner_model','banner');		
		$this->load->model('ABM/servicio_model','servicio');
		$this->load->model('ABM/servicioitem_model','servicio_item');
		$this->load->model('ABM/mediopago_model','medio_pago');
		$this->load->model('ABM/mediopagoitem_model','medio_pago_item');
		$this->load->model('Actividad/turno_model','turno');	
		$this->load->model('Actividad/venta_model','venta');	
		$this->load->model('Tools/provincia_model','provincia');	
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');
		$this->load->model('Tools/configuracion_model','configuracion');
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('Usuarios/frontend_model','frontend_user');

		$this->load->helper('url');	
		$this->load->helper('cookie');
		$this->load->library('tools');
		$this->load->library('validations');		
	}


	public function index()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		$direccion = $this->input->get('dir');
		$localidad = $this->input->get('loc');
		
		$data->latitud = $this->input->get('lat');	
		$data->longitud = $this->input->get('long');	
		$data->direccion = str_replace("%20", " ", $direccion);
		$data->localidad = str_replace("%20", " ", $localidad);
		$data->busqueda = $this->input->get('busco'); 

		$_SESSION['frontend_direccion'] = $data->direccion;
		$_SESSION['frontend_localidad'] = $data->localidad;

		//print_r($data);
		// TODO: ver por que no muestra direccion ingresada en el buscador mini (local_listado y local_articulos)

		// Filtros para listado de locales
		//$data->rubros = $this->rubro->get_by_articulos_activos(1);
		//$data->categorias = array("Petshop", "Veterinaria");
		$data->animales = $this->animal->get_by_articulos_activos(1);
		$data->marcas_select = $this->marca->get_by_articulos_activos_by_animal(1);
		$data->razas = $this->raza->get_by_articulos_activos_by_animal(1);
		$data->tamanios = $this->tamanio->get_by_articulos_activos_by_animal(1);
		$data->presentaciones = $this->presentacion->get_by_articulos_activos(1);
		$data->servicios = $this->servicio->get_all();
		$data->servicio_items = $this->servicio_item->get_all_servicio();		

		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_local_listado');
		$data->portada_posicion = $this->configuracion->get_value2('portada_local_listado');
		$data->portada_altura = $this->config->item('altura_portada_local_listado');
		$data->portada_altura_mobile = $this->config->item('altura_portada_local_listado_mobile');
		$data->portada_top = $this->config->item('texto_portada_top_local_listado');	
		$data->portada_top_mobile = $this->config->item('texto_portada_top_local_listado_mobile');	
 		$data->transparencia_portada = $this->config->item('transparencia_portada_local_listado');		

		$data->rutaImagen = 'true';

		// Listado de localidades
		$data->localidades = $this->local->get_all_localidades();		

		$this->load->template('local_listado', $data);
	}


	// Listado de locales (activos)
	// Se llama cuando un cliente ingresa una direccion y obtiene un listado de locales
	public function ajax_locales($latitud, $longitud)
	{
		$list = $this->local->get_datatables_frontend();
		$data = array();
		$locales_id = array();
		$no = $_POST['start'];

		// Datos adicionales de cada local
		foreach ($list as $local) 
		{
			// Calcula la distancia entre la ubicación del local y la dirección ingresada
			$local->distancia = $this->tools->distancia($latitud, $longitud, $local->latitud, $local->longitud, "K");

			// Adapta la distancia a km o metros
			$local->distancia_mostrar = $this->tools->format_distancia($local->distancia);

			// Carga puntaje promedio de los comentarios aprobados del local
			$local->puntaje = $this->local->get_puntaje($local->id, 'si');			

			// Cantidad de comentarios del local (aprobados)
			$local->comentarios = $this->local->get_comentarios($local->id, 'si', TRUE);				
		}

		// Limita la cantidad de locales a mostrar segun la distancia
		$list = array_filter($list, function($obj){
		    if (isset($obj->distancia)) {
		        if ((int)$obj->distancia >= (int)$this->config->item('geolocalizacion_max_distancia')) return false;
		    }
		    return true;
		});

		// Ordena locales segun criterio seleccionado (locales destacados por defecto)
		function destacado($a, $b)
		{
		    if ($a->destacado == $b->destacado) 
		    {
		        // Si los dos locales son destacados, prioriza el de menor distancia
		        return ($a->distancia < $b->distancia) ? -1 : 1;
		    }
		    return ($a->destacado > $b->destacado) ? -1 : 1;
		}

		function distancia($a, $b)
		{
		    if ($a->distancia == $b->distancia) {
		        return 0;
		    }
		    return ($a->distancia < $b->distancia) ? -1 : 1;
		}	

		function puntaje($a, $b)
		{
		    if ($a->puntaje == $b->puntaje) {
		        return 0;
		    }
		    return ($a->puntaje > $b->puntaje) ? -1 : 1;
		}	

		function nombre($a, $b)
		{
		    if ($a->nombre == $b->nombre) {
		        return 0;
		    }
		    return ($a->nombre < $b->nombre) ? -1 : 1;
		}

		if(isset($_POST['order'])) 
		{
			switch ($_POST['order']['0']['column']) {
				case 1:
					usort($list, "distancia");	
					break;

				case 2:
					usort($list, "puntaje");	
					break;

				case 3:
					usort($list, "nombre");	
					break;	

				case 4:
					usort($list, "destacado");	
					break;	

				default:
					usort($list, "destacado");	
					break;																		
			}
		} 
		else
		{
			usort($list, "destacado");	
		}	

		// Limita la cantidad máxima de locales que devuelve el buscador
		$list = array_slice($list, 0, $this->config->item('geolocalizacion_max_locales'));

		// Cantidad de locales antes de paginar
		$nro_locales = count($list);

		// Locales id antes de paginar (para los filtros)
		foreach ($list as $local) 
		{
			$locales_id[] = $local->id;
		}

		// Paginado
		if($_POST['length'] != -1) $list = array_slice($list, $_POST['start'], $_POST['length']);


		foreach ($list as $local) 
		{
			$no++;
			$row = array();
			$i = 0;
            $icono = 'fa fa-shopping-bag';
            $color = '#89AEDC';
            $clase = '';
            $oculto = '';
            $comentarios = '';
            $htmlPuntaje = '';            
            $htmlComentarios = '';
            $htmlEntregaDomicilio = '';
            $htmlDestacado = '';
            $cssDestacado = '';
            $htmlUrgencias = '';
            $htmlUrgenciasModalidad = '';
            $htmlUrgenciasDomicilio = '';
            $htmlMediosPago = '';
            $htmlMediosPagoItems = '';

            // Categoria
            if ($local->categoria == 'Veterinaria'){
                $icono = 'fa-plus';
                $color = 'violet'; 
                $clase = 'btn-veterinaria';
            } 			 

			// Carga el mayor descuento de cada local
			$local->mayor_descuento = $this->local->get_mayor_descuento($local->id);

            if ($local->mayor_descuento === null || $local->mayor_descuento == '0'){
                $oculto = 'oculto';
            } 

            // Local con Entrega a Domicilio
            if ($local->envio_domicilio == '1')
            {
                $htmlEntregaDomicilio = '<b><div style="clear: both;"></div><div class="pull-left push-5-t  push-20-l"><i class="fa fa-truck" style="color:gray"></i> Zona de entrega: ' . $local->zona_entrega . '</div>' .     
                            '<div style="clear: both;"></div>' . 
                            '<div class="pull-left push-5-t push-20-l"><i class="fa fa-calendar" style="color:gray"></i> Días de entrega: ' . $local->dias_entrega . '</div>' .  
                            '<div style="clear: both;"></div>' . 
                            '<div class="pull-left push-5-t push-20-l"><i class="fa fa-money" style="color:gray"></i> Costo de envío: ' . $local->costo_envio . '</div></b>';
            }
            else
            {
                $htmlEntregaDomicilio = '<b><div style="clear: both;"></div><div class="pull-left push-5-t push-20-l"><i class="fa fa-truck" style="color:gray"></i> No hace envíos a domicilio</div></b>' ;
            }

			// Puntaje
            if ($local->puntaje){
                $starsi = round($local->puntaje);
                $starno = 5 - $starsi;

                $htmlPuntaje = '<div class="pull-left push-5-t push-20-l" style="margin-right:10px">';    
                
                for ($j = 0; $j < $starsi; ++$j) {

                    $htmlPuntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
                };

                for ($j = 0; $j < $starno; ++$j) {

                    $htmlPuntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
                };

                $htmlPuntaje .= '</div>';           
            }

            // Comentarios
            if ($local->comentarios > 0){
                if ($local->comentarios == 1) $comentarios = '1 comentario';
                else $comentarios =  $local->comentarios . ' comentarios';

                $htmlComentarios = '<div class="pull-left push-5-t">' . 
                                '<a href="javascript:void(0)" onclick="mostrar_comentarios_local(' . $local->id . ', \'' . $local->nombre . '\', \'' . $local->avatar . '\')">' . $comentarios . '</a>' . 
                            '</div>' .   
                            '<div style="height:10px; clear: both;"></div>';
            }

            // local destacado
            if ($local->destacado == 1)
            {
            	$htmlDestacado = '<div class="centrado push-10"><img class="" src="' . BASE_PATH . '/assets/img/frontend/tienda_destacada.jpg" alt=""></div>';
            	$cssDestacado = ' destacado ';
            }

            // Urgencias
            if ($local->urgencias == 1)
            {
            	$htmlUrgenciasModalidad = '<span class="push-5-l" style="font-size: 13px;">(';

            	if ($local->urgencias_consultorio == 1) $htmlUrgenciasModalidad .= 'Atención en Consultorio';
            	if ($local->urgencias_consultorio  == 1 && $local->urgencias_domicilio == 1) $htmlUrgenciasModalidad .= ' y ';
            	if ($local->urgencias_domicilio == 1) $htmlUrgenciasModalidad .= 'Visita a Domicilio';

            	$htmlUrgenciasModalidad .= ')</span>';

            	$htmlUrgencias = '<div class="clearfix"></div><div class="pull-left push-10-t text-danger push-10 push-20-l"><i class="fa fa-plus-square"></i> <strong>Urgencias 24hs.:</strong> ' . $local->telefono_urgencias . $htmlUrgenciasModalidad . '</div>' ;
            }       

			
			// Medios de Pago	
			$medios_pago = $this->medio_pago->get_by_local($local->id);

        	$htmlMediosPago = '<div class="clearfix"></div><div class="pull-left push-10-t push-10 push-20-l"><i class="fa fa-dollar" style="color:gray"></i> <b>Medios de Pago:</b><ul class="push-5-t push--60-l">';

			foreach ($medios_pago as $medio_pago) 
			{
				$htmlMediosPagoItems = '';

				$htmlMediosPago .= '<li class="medio-pago push-25-l">' .
										'<img src="' . BASE_PATH . '/assets/img/medios_pago/miniaturas/' . $medio_pago->imagen . '" alt="' . $medio_pago->nombre . '" title="' . $medio_pago->nombre . '" data-pin-nopin="true" height="30">';

				$medios_pago_items = $this->medio_pago_item->get_by_local_by_medio_pago($local->id, $medio_pago->id);

				if ($medios_pago_items)
				{	        		
					foreach ($medios_pago_items as $medios_pago_item) 
					{							        
						$htmlMediosPagoItems .= '<li class="medio-pago push-5-t push-8-l">' .
												'<img src="' . BASE_PATH . '/assets/img/medios_pago/miniaturas/' . $medios_pago_item->imagen . '" alt="' . $medios_pago_item->nombre . '" title="' . $medios_pago_item->nombre . '" data-pin-nopin="true" height="22">' .
								        		'</li>';        								
					}
					
					$htmlMediosPago .= $htmlMediosPagoItems . '</li>';
				}				
			}				 

			$htmlMediosPago .= '</ul></div>';

			$local_html =    				
 				'<ul class="list list-simple list-li-clearfix" id="lista_locales">' .
					'<li class="local-box ' . $cssDestacado . '">' .
	                 '<div class="col-sm-3 centrado">' . 
	                        '<div class="local-avatar text-center">' . 
	                            '<a href="javascript:void(0)" onclick="ver_local(' . $local->id . ')" class="local-avatar-img">' . 
	                            '<div class="circulo_descuento ' . $oculto . '"><span>'. $local->mayor_descuento .'%</span><b></b>OFF</div>' . 
	                            '<img src="' . BASE_PATH . '/assets/img/local/miniaturas/' . $local->avatar . '" width="100%" style="min-width: 120px; max-width: 140px;"></a>' . 
	                        '</div>' .
	                    '</div>' .
	                    '<div class="col-sm-7 centrado">' . 

	                    	'<div class="pull-left" style="min-width:320px;">' . 
		                        '<h4 class="pull-left push-5 push-20-r push-20-l"><a href="javascript:void(0)" onclick="ver_local(' . $local->id . ')"><i class="' . $clase . ' fa ' . $icono . '" style="color:' . $color .'"></i>&nbsp;&nbsp;' . $local->nombre . '</a></h4>' . 
		                        '<div class="pull-left push-5-t"><i class="si si-map" style="color:gray"></i>  ' . $local->distancia_mostrar . '</div>' .  
		                        '<div style="height:10px; clear: both;"></div>' . 

		                        $htmlPuntaje .
		                        $htmlComentarios . 

		                        '<b><div class="pull-left push-20-l" >' .  
		                            '<i class="fa fa-map-marker" style="color:gray"></i> ' . $local->direccion . 
		                        ', ' . $local->localidad . 
		                        '</div></b>' .  
		                        '<div style="height:8px; clear: both;"></div>' . 
		                    '</div>' .

		                    $htmlDestacado .

	                        $htmlEntregaDomicilio . 
	                        $htmlUrgencias .
	                        $htmlMediosPago .
	                    '</div>' .                             
	                    '<div class="col-sm-2 hidden-xs push--30-l push-10-t">' .  
	                        '<div class="center-block ">' .  
	                            '<form  action="' . BASE_PATH . '/Local/local_articulos" id="form_ver_local_' . $local->id . '" method="get">' .
	                                '<input type="hidden" name="id" value="' . $local->id . '"  />' .
	                                '<input type="hidden" name="cat" value="' . $local->categoria . '"  />' .
	                                '<input type="hidden" name="distancia" value="' . $local->distancia_mostrar . '"  />' .
	                                '<button class="btn btn-geotienda-celeste hidden-860" type="submit" id="btn_buscar" style="position:absolute; right:0px" >Ver Tienda</button>' .  
	                            '</form>' .
	                        '</div>' .  
	                    '</div>' .   
	                '</li>' . 

                '</ul>';

            $row[] = $local_html;

            // Columnas auxiliares para filtros y orden
            $row[] = $local->id;            
            $row[] = $local->distancia;            
            $row[] = $local->puntaje;            
            $row[] = $local->nombre;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->id;
            $row[] = $local->destacado;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $nro_locales,
						"recordsFiltered" => $nro_locales,
						"data" => $data,
						"locales_id" => $locales_id,
				);

		echo json_encode($output);
	}


	public function local_articulos(){

		$data = new stdClass();

		$data->id = $this->input->get('id');
		$data->categoria = $this->input->get('cat');		
		$data->distancia = $this->input->get('distancia');		
		$data->busqueda = $this->input->get('busco'); 
		$data->turno = $this->input->get('turno'); 
		$data->venta = $this->input->get('venta'); 

		$data->direccion = "";
		$data->localidad = "";
		
		// Recupera direccion ingresada
		if (isset($_SESSION['frontend_direccion']))
		{
			$data->direccion = $_SESSION['frontend_direccion'];
			$data->localidad = $_SESSION['frontend_localidad'];			
		}

		// Carga calendario de cada Servicio del local para reserva de turnos
		$data->calendario_clinica = $this->crearCalendario(null, $data->id, null, 'clinica');
		$data->calendario_peluqueria = $this->crearCalendario(null, $data->id, null, 'peluqueria');

		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_local_articulos');	
		$data->portada_posicion = $this->configuracion->get_value2('portada_local_articulos');
		$data->portada_altura = $this->config->item('altura_portada_local_articulos');
		$data->portada_altura_mobile = $this->config->item('altura_portada_local_articulos_mobile');
		$data->portada_top = $this->config->item('texto_portada_top_local_articulos');
		$data->portada_top_mobile = $this->config->item('texto_portada_top_local_articulos_mobile');
		$data->transparencia_portada = $this->config->item('transparencia_portada_local_articulos');	

		$data->rutaImagen = 'true';			

		// Filtros para artículos del local
		$data->rubros = $this->rubro->get_by_local($data->id, 1, 1);
		$data->animales = $this->animal->get_by_local($data->id, 1, 1);
		$data->marcas_select = $this->marca->get_by_local_by_animal($data->id, 1, 1);
		$data->razas = $this->raza->get_by_local_by_animal($data->id, 1);
		$data->tamanios = $this->tamanio->get_by_local_by_animal($data->id, 1);
		$data->presentaciones = $this->presentacion->get_by_local($data->id, 1);

		// Filtros para servicios del local
		$data->servicios = $this->servicio->get_by_local($data->id);
		$data->servicio_items = $this->servicio_item->get_by_local_by_servicio($data->id);
		$data->servicio_animales = $this->servicio_item->get_animales_by_local($data->id);

		// Opciones para finalizar pedido
		$data->provincias = $this->provincia->get_all();
		$data->medios_pago = $this->medio_pago->get_by_local($data->id);

		// Listado de localidades
		$data->localidades = $this->local->get_all_localidades();				

		$this->load->template('local_articulos', $data);
	}


	// Carga artículos de un local
	// Se llama cuando un cliente selecciona un local del listado de locales
	public function ajax_local_articulos($id)
	{
		$data = new stdClass();
		$articulos_array = array();

		$data->local = $this->local->get_by_id($id);

		// Carga el mayor descuento del local
		$data->local->mayor_descuento = $this->local->get_mayor_descuento($id);	 

		// Servicios del local
		$data->servicios_item = $this->local_servicio_item->get_servicios_item_local($id, 1);  // Trae lista de servicios habilitados

		// Puntaje promedio del local
		$data->local->puntaje = $this->local->get_puntaje($id, 'si');

		// Cantidad de comentarios del local (aprobados)
		$data->local->comentarios = $this->local->get_comentarios($id, 'si', TRUE);	

		// Carga banner activo del local
		$data->local->banner = $this->get_banner_activo($id);

		// Carga cantidad de articulos del local
		$data->local->count_articulos = $this->local_articulo->count_all($id);		

		// Medios de pago del local
		$data->local->medios_pago = $this->medio_pago->get_by_local($id);	

		foreach ($data->local->medios_pago as $medio_pago) 
		{
			$medio_pago->items = $this->medio_pago_item->get_by_local_by_medio_pago($id, $medio_pago->id);
		}

		echo json_encode($data);	
	}


	// Listado de Artículos asociados a un local (activos)
	// Se llama cuando un cliente ingresa a un local 
	public function ajax_articulos_local($local_id)
	{
		$list = $this->local_articulo->get_datatables_local($local_id);
		$data = array();
		$no = $_POST['start'];
        $marca_rubro = '';	

		foreach ($list as $articulo) {
			$no++;
			$row = array();
			$articulo_html = '';
            $presentaciones_html = '';		
            $oculto = '';
			$i = 0;
			$imagen_locales = '';
			
			// Recupera descuentos, presentaciones, tamanios y razas asociadas a cada artículo
			$presentaciones = $this->local_articulo->get_presentaciones_activas_articulo_local($local_id , $articulo->id);	
			$mayor_descuento = $this->local_articulo->get_mayor_descuento_articulo_local($local_id, $articulo->id);		

            // Si el artículo no tiene marca, muestra el rubro en su lugar
            if ($articulo->marca === null) 
                $marca_rubro = $articulo->rubro;
            else 
                $marca_rubro = $articulo->marca;
			
            // Mayor Descuento
            if ($mayor_descuento === null || $mayor_descuento == '0'){
                $oculto = 'oculto';
            } 

            // Presentaciones de cada artículo
            foreach ($presentaciones as $presentacion) 
            {            	
                $tachado = '';
                $descuento_oculto = '';
                $nombre = '';
                $en_pedido = FALSE;

                // Si está en el pedido, muestra botón 'Quitar del pedido'
                if ($en_pedido){
                    $oculta_agregar = 'style="display:none"';
                    $oculta_quitar = '';
                }
                // Si no está en el pedido, muestra botón 'Agregar al pedido'                
                else{
                    $oculta_agregar = '';
                    $oculta_quitar = 'style="display:none"';                   
                }

                // Si el artículo tiene asignada la presentación 'Sin Presentación' (presentacion_id = 1), no muestra el nombre
                if ($presentacion->presentacion_id != 1) $nombre = $presentacion->nombre;
              

                // Presentación con Descuento
                if ($presentacion->precio != $presentacion->precio_descuento){

                    $tachado = 'font-w300 tachado text-muted';
                    $ocultar_circulo = '';
                }
                // Presentación sin Descuento
                else{

                    $tachado = 'font-w600 text-success';
                    $descuento_oculto = 'oculto';
                    $ocultar_circulo = 'style="display:none"';       
                }   

                // Presentación con Stock
                if ($presentacion->stock == '1'){

                    $ocultar_carrito = '';       
                    $sin_stock = 'style="display:none"';       
                }
                // Presentación sin Stock
                else{

                    $ocultar_carrito = 'style="display:none"';       
                    $sin_stock = '';      
                }                      

                $presentaciones_html .=
                    
                    '<div class="h5 font-w400 text-muted pull-left push-10-top">' .  $nombre .'</div>' .

                    // Agregar al carrito
                    '<div class="pull-right push-10-l push--5-t" ' . $ocultar_carrito . '>' . 
                        '<a class="btn btn-redondo btn-redondo-mini btn-celeste" title="Agregar al pedido" id="btn_agrega_pedido_muted_' . $articulo->id . '_' . $i . '" href="javascript:void(0)" onclick="agregar_al_pedido(' . $articulo->id . ', ' . $i .', ' . $presentacion->presentacion_id . ');"' . $oculta_agregar . '>' . 
                            '<i class="fa fa-shopping-cart"></i> ' . 
                        '</a>' . 
                        '<a class="btn btn-redondo btn-redondo-mini btn-celeste" title="Quitar del pedido" id="btn_quita_pedido_muted_' . $articulo->id . '_' . $i . '" href="javascript:void(0)" onclick="quitar_del_pedido(' . $articulo->id . ', ' . $i .');" ' .  $oculta_quitar . '>' . 
                            '<i class="fa fa-minus"></i>' . 
                        '</a>' .     
                    '</div>' . 

                    // Stock
                    '<div class="pull-right push-10-l push--5-t" ' . $sin_stock . '>' . 
                    '<div class="push-5-t" style="color: red;">SIN STOCK</div>' . 
                    '</div>' . 

                    '<div class="h5 pull-right push-10-l ' . $tachado . '">$' .  $presentacion->precio .'</div>' . 
                    '<div class="h5 font-w600 text-success pull-right push-10-l ' . $descuento_oculto . '">' . 
                    '<div class="circulo_descuento_mini"' . $ocultar_circulo . '><span>-'. $presentacion->descuento .'%</span></div>' . 
                    '$' .  $presentacion->precio_descuento .'</div>' . 
                    '<div style="height:10px; clear: both;"></div>';

                    $i++;
            }		

        // Las imágenes de un articulo creado por el Local están en el subdirectorio 'locales'
        if($articulo->tipo == 'Local') $imagen_locales = 'locales/';

            $articulo_html =    
            	'<div class="block">' . 
                    '<div class="block-content">' . 
                        '<div class="row push-5" style="position: relative">' . 

                            // Ver detalles articulo (mobile)
                            '<div class="ver-detalle-articulo visible-xs">' . 
                                '<a class="h4 btn btn-redondo btn-muted btn-semitransparente" title="Ver detalle" href="javascript:void(0)" onclick="mostrar_detalles_articulo(' . $articulo->id  . ');"><i class="fa fa-search-plus"></i></a><br/><br/>' . 
                            '</div>' .   

                            // Imagen
                            '<div class="col-sm-5 push-10 img-container">' . 
                                '<div class="div-img-articulos">' . 
                                    '<img class="img-articulos" src="' . BASE_PATH . '/assets/img/articulos/' . $imagen_locales . 'miniaturas/' .  $articulo->imagen .'" alt="">' . 
                                '</div>' .              
                                '<div class="circulo_descuento ' . $oculto . '"><span>'. $mayor_descuento .'%</span><b></b>OFF</div>' . 

                                // Ver detalles articulo (desktop)    
                                '<div class="img-options">' . 
                                    '<div class="img-options-content">' . 
                                        '<div class="btn-group push-150-t">' . 
                                            '<a class="btn btn-info" href="javascript:void(0)" onclick="mostrar_detalles_articulo(' . $articulo->id  . ');"><i class="fa fa-search-plus"></i> Ver detalles</a>' .
                                        '</div>' . 
                                    '</div>' . 
                                '</div>' .                                                                         
                            '</div>' . 

                            // Descripcion
                            '<div class="col-sm-7">' . 
                                '<div class="push-10">' . 
                                    '<span class="h6 font-w600 text-muted" href="javascript:void(0)">' .  $marca_rubro . '</span>' . 
                                    '<p class="h5 font-w600"> ' .  $articulo->nombre .'</p>' .
                                    $presentaciones_html .
                                '</div>' .                                 
                            '</div>' . 
                        '</div>' . 
                    '</div>' . 
                '</div>';	

            $row[] = $articulo_html;

            // Columnas auxiliares para filtros y orden
            $row[] = $articulo->nombre;
            $row[] = $articulo->marca;
            $row[] = $articulo->id;
            $row[] = $articulo->id;
            $row[] = $articulo->id;
            $row[] = $articulo->id;
            $row[] = $articulo->id;
            $row[] = $articulo->id;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->local_articulo->count_all_local($local_id),
						"recordsFiltered" => $this->local_articulo->count_filtered_local($local_id),
						"data" => $data,
				);
		

		echo json_encode($output);
	}


	// Dashboard Local - Recarga dinamicamente filtros de productos
	public function ajax_filtros_productos()
	{
		$data = new stdClass();

		// Filtros para tabla de productos activos del local
		$data->rubros_activos = $this->rubro->get_by_local($_SESSION['frontend_user_id'], 1, -1);
		$data->animales_activos = $this->animal->get_by_local($_SESSION['frontend_user_id'], 1, -1);
		$data->marcas_activos = $this->marca->get_by_local_animal_rubro($_SESSION['frontend_user_id'], 1, -1);
		
		// Filtros para tabla de productos inactivos del local
		$data->rubros_inactivos = $this->rubro->get_by_local($_SESSION['frontend_user_id'], 0, -1);
		$data->animales_inactivos = $this->animal->get_by_local($_SESSION['frontend_user_id'], 0, -1);
		$data->marcas_inactivos = $this->marca->get_by_local_animal_rubro($_SESSION['frontend_user_id'], 0, -1);

		echo json_encode($data);	
	}


	public function ajax_comentarios_local($id)
	{
		$data = new stdClass();

		// Carga comentarios aprobados del local
		$data->comentarios = $this->local->get_comentarios($id, 'si');	

		// Carga datos de la venta o del servicio de cada comentario
		if (count($data->comentarios) > 0){

			foreach ($data->comentarios as $comentario) {

				// Venta
				if ($comentario->tipo == 'venta'){

					$detalle_venta = $this->venta->get_detalle_venta_by_id($comentario->venta_turno_id);

					if (count($detalle_venta) > 0){

						// Recupera datos del primer detalle de la venta
						$comentario->detalle = '<i class="fa fa-shopping-cart"></i> ' . $detalle_venta[0]->marca . ' ' . $detalle_venta[0]->articulo  . ' ' . $detalle_venta[0]->presentacion;
					}
				}

				// Turno
				if ($comentario->tipo == 'turno'){

					$detalle_turno = $this->turno->get_by_id($comentario->venta_turno_id);

					// Recupera datos del turno
					$comentario->detalle = '<i class="si si-notebook"></i> ' . $detalle_turno->servicio . ' - ' . $detalle_turno->servicio_item;
				}

			}				
		}	

		echo json_encode($data);	
	}


	public function get_banner_activo($id){

		$banner = new stdClass();

		// Recupera banner global
		$banner_global = $this->banner->get_global();

		if ($banner_global) $banner = $banner_global;
		else
		{
			// Recupera banner grupal asignado al local
			$banner_grupal = $this->banner->get_by_local($id);
			
			if ($banner_grupal) $banner = $banner_grupal;
			else
			{
				// Recupera banner del local
				$banner_local = $this->local->get_banner_local($id);
				$banner = $banner_local;
			}
		}

		return $banner;
	}

	// Filtros listado de locales
	public function ajax_filtros_local_categoria()
	{
		$data = array();

		$data['categorias'] = $this->local->get_categorias_locales($this->input->post('locales_id'));

		echo json_encode($data);	
	}


	public function ajax_filtros_local_rubro($animal = -1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = -1;
		$filtros->animal = $animal;
		$filtros->marca = -1;
		$filtros->raza = -1;
		$filtros->tamanio = -1;
		$filtros->edad = -1;
		$filtros->presentacion = -1;
		$filtros->medicados = -1;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		$data['rubros'] = $this->local->get_items_filtro_locales('rubros', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_animal($rubro = -1, $servicio = -1, $servicio_item = -1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = -1;
		$filtros->marca = -1;
		$filtros->raza = -1;
		$filtros->tamanio = -1;
		$filtros->edad = -1;
		$filtros->presentacion = -1;
		$filtros->medicados = -1;
		$filtros->servicio = $servicio;
		$filtros->servicio_item = $servicio_item;

		$data['animales'] = $this->local->get_items_filtro_locales('animales', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_marca($rubro, $animal, $raza, $tamanio, $edad, $presentacion, $medicados)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = -1;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'marca'
		$con_marca = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmarca');

		if ($con_marca > 0) $data['marcas'] = $this->local->get_items_filtro_locales('marcas', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_raza($rubro, $animal, $marca, $tamanio, $edad, $presentacion, $medicados)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = -1;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'raza'
		$con_raza = $this->rubro->get_by_id_atributo($rubro, 'rubro_conraza');

		if ($con_raza > 0) $data['razas'] = $this->local->get_items_filtro_locales('razas', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_tamanio($rubro, $animal, $marca, $raza, $edad, $presentacion, $medicados)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = -1;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'tamanio'
		$con_tamanio = $this->rubro->get_by_id_atributo($rubro, 'rubro_contamanios');

		if ($con_tamanio > 0) $data['tamanios'] = $this->local->get_items_filtro_locales('tamanios', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_edad($rubro, $animal, $marca, $raza, $tamanio, $presentacion, $medicados)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = -1;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'edad'
		$con_edad = $this->rubro->get_by_id_atributo($rubro, 'rubro_conedad');

		if ($con_edad > 0) $data['edades'] = $this->local->get_items_filtro_locales('edades', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_presentacion($rubro, $animal, $marca, $raza, $tamanio, $edad, $medicados)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = -1;
		$filtros->medicados = $medicados;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'presentacion'
		$con_presentacion = $this->rubro->get_by_id_atributo($rubro, 'rubro_conpresentacion');

		if ($con_presentacion > 0) $data['presentaciones'] = $this->local->get_items_filtro_locales('presentaciones', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_medicados($rubro, $animal, $marca, $raza, $tamanio, $edad, $presentacion)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = -1;
		$filtros->servicio = -1;
		$filtros->servicio_item = -1;

		// Valida si el rubro actual tiene el atributo 'medicados'
		$con_medicados = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmedicados');

		if ($con_medicados > 0) $data['medicados'] = $this->local->get_items_filtro_locales('medicados', $this->input->post('locales_id'), $filtros);

		echo json_encode($data);	
	}


	public function ajax_filtros_local_servicio($animal = -1)
	{
		$data = array();

		$data['servicios'] = $this->local->get_servicios_locales($this->input->post('locales_id'), $animal);

		echo json_encode($data);	
	}

	
	public function ajax_filtros_local_servicio_item($servicio, $animal = -1)
	{
		$data = array();

		$data['servicio_items'] = $this->local->get_servicio_items_locales($this->input->post('locales_id'), $servicio, $animal);

		echo json_encode($data);	
	}
	// FIN Filtros listado de locales


	// Filtros en articulos de un local
	public function ajax_filtros_articulo_creador($rubro, $animal, $marca, $raza, $tamanio, $edad, $presentacion, $medicados, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->creador = -1;
		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;

		$data['creadores'] = $this->local_articulo->get_items_filtro_articulos_local('creadores', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}	


	public function ajax_filtros_articulo_animal($rubro = -1, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = -1;
		$filtros->marca = -1;
		$filtros->raza = -1;
		$filtros->tamanio = -1;
		$filtros->edad = -1;
		$filtros->presentacion = -1;
		$filtros->medicados = -1;
		$filtros->creador = $creador;

		$data['animales'] = $this->local_articulo->get_items_filtro_articulos_local('animales', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}	


	public function ajax_filtros_articulo_rubro($animal = -1, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = -1;
		$filtros->animal = $animal;
		$filtros->marca = -1;
		$filtros->raza = -1;
		$filtros->tamanio = -1;
		$filtros->edad = -1;
		$filtros->presentacion = -1;
		$filtros->medicados = -1;
		$filtros->creador = $creador;

		$data['rubros'] = $this->local_articulo->get_items_filtro_articulos_local('rubros', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_marca($rubro, $animal, $raza, $tamanio, $edad, $presentacion, $medicados, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = -1;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->creador = $creador;

		// Valida si el rubro actual tiene el atributo 'marca'
		$con_marca = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmarca');

		if ($con_marca > 0) $data['marcas'] = $this->local_articulo->get_items_filtro_articulos_local('marcas', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}	


	public function ajax_filtros_articulo_edad($rubro, $animal, $marca, $raza, $tamanio, $presentacion, $medicados, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = -1;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->creador = $creador;
		$filtros->servicio = -1;

		// Valida si el rubro actual tiene el atributo 'edad'
		$con_edad = $this->rubro->get_by_id_atributo($rubro, 'rubro_conedad');

		if ($con_edad > 0) $data['edades'] = $this->local_articulo->get_items_filtro_articulos_local('edades', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_tamanio($rubro, $animal, $marca, $raza, $edad, $presentacion, $medicados, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = -1;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->creador = $creador;
		$filtros->servicio = -1;

		// Valida si el rubro actual tiene el atributo 'tamanio'
		$con_tamanio = $this->rubro->get_by_id_atributo($rubro, 'rubro_contamanios');

		if ($con_tamanio > 0) $data['tamanios'] = $this->local_articulo->get_items_filtro_articulos_local('tamanios', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_raza($rubro, $animal, $marca, $tamanio, $edad, $presentacion, $medicados, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = -1;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->medicados = $medicados;
		$filtros->creador = $creador;

		// Valida si el rubro actual tiene el atributo 'raza'
		$con_raza = $this->rubro->get_by_id_atributo($rubro, 'rubro_conraza');

		if ($con_raza > 0) $data['razas'] = $this->local_articulo->get_items_filtro_articulos_local('razas', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}	


	public function ajax_filtros_articulo_presentacion($rubro, $animal, $marca, $raza, $tamanio, $edad, $medicados, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = -1;
		$filtros->medicados = $medicados;
		$filtros->creador = $creador;
		$filtros->servicio = -1;

		// Valida si el rubro actual tiene el atributo 'presentacion'
		$con_presentacion = $this->rubro->get_by_id_atributo($rubro, 'rubro_conpresentacion');

		if ($con_presentacion > 0) $data['presentaciones'] = $this->local_articulo->get_items_filtro_articulos_local('presentaciones', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_medicados($rubro, $animal, $marca, $raza, $tamanio, $edad, $presentacion, $creador = -1, $filtrar_inactivos = "true", $filtro_articulos_inactivos = 1)
	{
		$data = array();
		$filtros = new stdClass();

		$filtros->rubro = $rubro;
		$filtros->animal = $animal;
		$filtros->marca = $marca;
		$filtros->raza = $raza;
		$filtros->tamanio = $tamanio;
		$filtros->edad = $edad;
		$filtros->presentacion = $presentacion;
		$filtros->creador = $creador;
		$filtros->medicados = -1;
		$filtros->servicio = -1;

		// Valida si el rubro actual tiene el atributo 'medicados'
		$con_medicados = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmedicados');

		if ($con_medicados > 0) $data['medicados'] = $this->local_articulo->get_items_filtro_articulos_local('medicados', $this->input->post('local_id'), $filtros, $filtrar_inactivos, $filtro_articulos_inactivos);

		echo json_encode($data);	
	}	


	public function ajax_filtros_articulo_animal_servicio($servicio = -1, $servicio_item = -1)
	{
		$data = array();

		$data['animales'] = $this->local_articulo->get_animales_servicio_local($this->input->post('local_id'), $servicio, $servicio_item);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_servicio($animal = -1)
	{
		$data = array();

		$data['servicios'] = $this->local_articulo->get_servicios_local($this->input->post('local_id'), $animal);

		echo json_encode($data);	
	}


	public function ajax_filtros_articulo_servicio_item($servicio, $animal = -1)
	{
		$data = array();

		$data['servicio_items'] = $this->local_articulo->get_servicio_items_local($this->input->post('local_id'), $servicio, $animal);

		echo json_encode($data);	
	}	
	// FIN Filtros en articulos de un local


	public function ajax_filtros_articulo(){

		$data = array();

		$data['marcas'] = $this->local_articulo->get_count_marcas($this->input->post('local_id'), $this->input->post('articulos_id'));
		$data['presentaciones'] = $this->local_articulo->get_count_presentaciones($this->input->post('local_id'), $this->input->post('articulos_id'));		
		$data['rubros'] = $this->local_articulo->get_count_rubros($this->input->post('local_id'), $this->input->post('articulos_id'));
		$data['animales'] = $this->local_articulo->get_count_animales($this->input->post('local_id'), $this->input->post('articulos_id'));
		$data['tamanios'] = $this->local_articulo->get_count_tamanios($this->input->post('local_id'), $this->input->post('articulos_id'));		
		$data['razas'] = $this->local_articulo->get_count_razas($this->input->post('local_id'), $this->input->post('articulos_id'));		
		$data['medicados'] = $this->local_articulo->get_count_medicados($this->input->post('local_id'), $this->input->post('articulos_id'));	
		$data['edades'] = $this->local_articulo->get_count_edades($this->input->post('local_id'), $this->input->post('articulos_id'));			
		
		echo json_encode($data);	
	}


	public function ajax_filtros_servicio(){

		$data = array();

		$data['servicios'] = $this->local_servicio_item->get_count_servicios($this->input->post('local_id'), $this->input->post('servicio_items_id'));		
		$data['servicio_items'] = $this->local_servicio_item->get_count_servicio_items($this->input->post('local_id'), $this->input->post('servicio'), $this->input->post('servicio_items_id'));	
		$data['animales'] = $this->local_servicio_item->get_count_animales($this->input->post('local_id'), $this->input->post('servicio_items_id'));		

		echo json_encode($data);	
	}


	public function finalizar_pedido(){

		$data = new stdClass();

		$this->load->view('finalizar_pedido', $data);
	}


	public function ajax_calendario_cambia_mes(){

		$data = array();
		$horarios=array();

		$fecha = $this->input->post('fecha');
		$local_id = $this->input->post('local_id');
		$servicio = $this->input->post('servicio');

		$data['calendario'] = $this->crearCalendario($fecha, $local_id, null, $servicio);

		echo json_encode($data);		
	}


	private function crearCalendario($siguiente, $id, $vete, $servicio)
	{
		$calendario = '';

		// Recupera horarios del local
		$horario = $this->local_horario->get_horario_servicio($id, $servicio);

		// Valida que el local tenga configurado los horarios
		if ($horario === null)
		{
			$calendario = '<div style="color: red">Calendario no configurado</div>';
		}
		else
		{
			$trabaja_lu = false;
			$trabaja_ma = false;
			$trabaja_mi = false;
			$trabaja_ju = false;
			$trabaja_vi = false;
			$trabaja_sa = false;
			$trabaja_do = false;

			if($horario->lu == 1) $trabaja_lu = true;
			if($horario->ma == 1) $trabaja_ma = true;
			if($horario->mi == 1) $trabaja_mi = true;
			if($horario->ju == 1) $trabaja_ju = true;
			if($horario->vi == 1) $trabaja_vi = true;
			if($horario->sa == 1) $trabaja_sa = true;
			if($horario->do == 1) $trabaja_do = true;
			
			if(isset($siguiente))
				$fechahoy=$siguiente;
			else
				$fechahoy=date('d-m-Y'); //fecha a tomar
			
			$fechahoyConvertida=date('Ymd');
			
			$idMesSiguiente = $this->tools->mesSiguiente($fechahoy);
			$idMesAnterior = $this->tools->mesAnterior($fechahoy);
			
			$nroMes = $this->tools->idddeMes($fechahoy);
			$nroAnio = $this->tools->iddeAnio($fechahoy);

			$eventos_del_mes=array();
			$dias_del_mes=array();

			for($x = 1; $x <= 31; $x ++)
				$dias_del_mes[$x]=array();
			

		   $calendario = "
		   <div id='vp_eventos'>
		   <h3 class='mesactual'>" . $this->tools->nombreMes($fechahoy)." $nroAnio</h3>
		   <div class='clear'></div>";
			
		  if($this->tools->idddeMes($fechahoy) != $this->tools->idddeMes(date('d-m-Y')) || $vete=='vete')
		  {
		  		$calendario .="<div class='prevmonth'><a href='#' onclick='calendario_cambia_mes(\"$idMesAnterior\",\"$servicio\"); return false'><</a></div>";
		  }
		  
		  $diassemana=array(
			1 =>"LUN",
			2 =>"MAR",
			3 =>"MIE",
			4 =>"JUE",
			5 =>"VIE",
			6 =>"SAB",
			7 =>"DOM",
			);
		   
		   $calendario.="
		   <div class='nextmonth'><a href='#' onclick='calendario_cambia_mes(\"$idMesSiguiente\",\"$servicio\"); return false'>></a></div>
		   <div class='clear'></div>
		   <div class='tabla'>";

		   foreach($diassemana as $ds=>$s)
		   {
			   $calendario .="<div class='columna titulos'>$s</div>";
		   }
		   
		   	$totalDias = $this->tools->cantidadDiasdelMes($fechahoy); //dias totales del mes
		   	$primerDiaSemana = $this->tools->diaSemana($this->tools->_data_first_month_day($fechahoy)); //donde cae el 1
			$comenzoMes=false;
			$dsemana=0;

		   for($x=1;$x<=$totalDias; $x++)
		   {
			   $dsemana++;	   

			   if($x <= 9)
				   $r = "0". $x;
			   else
				   $r = $x;

				$diaConvertido = $nroAnio.$nroMes.$r;
				$diaMostrar = $r . '/' . $nroMes . '/' . $nroAnio;

			   if($comenzoMes==false)
			   {		   
				   if($dsemana==$primerDiaSemana)
				   {
					 $comenzoMes = true; 
					 $diaquees = $this->tools->diaSemanaOF($diaConvertido);
					 $linkPrev="";
					 $linkNext="";
					 
					 if($diaquees == 1)
					 {
						 if($trabaja_lu == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diainactivo";
					 }
					 if($diaquees == 2)
					 {
						 if($trabaja_ma == true)
							 $estilo="diaactivo";
						 else
							  $estilo="diainactivo";
					 }
					 if($diaquees == 3)
					 {
						 if($trabaja_mi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diainactivo";
					 }
					 if($diaquees == 4)
					 {
						 if($trabaja_ju == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diainactivo";
					 }					 					 
					 if($diaquees == 5)
					 {
						 if($trabaja_vi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diainactivo";
					 }
					 else if($diaquees == 6)
					 {
						  if($trabaja_sa == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diainactivo";
					 }
					 else if($diaquees == 7)
					 {
						  if($trabaja_do == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diainactivo";
					 }

					if($vete != 'vete')
					 {
						 if($diaConvertido <= $fechahoyConvertida)	
							$estilo = "diainactivo";
					 }
					
					// Valida si hay una excepcion a los horarios para el dia y servicio actual
					$excepcion = $this->local_horario->get_excepcion_dia($id, $servicio, $diaMostrar);
					$abierto = 0;

					if(!is_null($excepcion) && $diaConvertido > $fechahoyConvertida)
					{
						if($excepcion->abierto == 1)
						{
							$estilo = "diaactivo";	
							$abierto = 1;
						} 

						if($excepcion->abierto == 0)
						{
							$estilo = "diainactivo";	
						} 
					}

					if($estilo == 'diaactivo')
					{
						$linkPrev = "onclick='ver_horarios(\"$diaConvertido\", \"$diaMostrar\", \"$servicio\")'";
					}
					 
					 $calendario .="<div class='columna dia $estilo' $linkPrev><span id='a$diaConvertido' class='a$diaConvertido'>$x</span>";

					 if(isset($dias_del_mes[$diaConvertido]))
					 {
						 foreach($dias_del_mes[$diaConvertido] as $m)
						 {
							$calendario .="$m";
						 }
					 }
					
					 $calendario .="</div>";		
				   }
				   else
				   {
					   $x=0;
					   $calendario .="<div class='columna othermonth dia'></div>";
				   }			   
			   }
			 else
			 {
				 $diaquees = $this->tools->diaSemanaOF($diaConvertido);
				 $linkPrev="";
				 $linkNext="";

				 if($diaquees == 1)
				 {
					 if($trabaja_lu == true)
						 $estilo = "diaactivo";
					 else
						  $estilo = "diainactivo";
				 }
				 if($diaquees == 2)
				 {
					 if($trabaja_ma == true)
						 $estilo="diaactivo";
					 else
						  $estilo="diainactivo";
				 }
				 if($diaquees == 3)
				 {
					 if($trabaja_mi == true)
						 $estilo = "diaactivo";
					 else
						  $estilo = "diainactivo";
				 }
				 if($diaquees == 4)
				 {
					 if($trabaja_ju == true)
						 $estilo = "diaactivo";
					 else
						  $estilo = "diainactivo";
				 }					 					 
				 if($diaquees == 5)
				 {
					 if($trabaja_vi == true)
						 $estilo = "diaactivo";
					 else
						  $estilo = "diainactivo";
				 }
				 else if($diaquees == 6)
				 {
					  if($trabaja_sa == true)
						   $estilo = "diaactivo";
					   else
						    $estilo = "diainactivo";
				 }
				 else if($diaquees == 7)
				 {
					  if($trabaja_do == true)
						   $estilo = "diaactivo";
					   else
						    $estilo = "diainactivo";
				 }

				 if($vete != 'vete')
				 {
					 if($diaConvertido <= $fechahoyConvertida)	
						$estilo = "diainactivo";
				 }
				
				// Valida si hay una excepcion a los horarios para el dia y servicio actual
				$excepcion = $this->local_horario->get_excepcion_dia($id, $servicio, $diaMostrar);
				$abierto = 0;

				if(!is_null($excepcion) && $diaConvertido > $fechahoyConvertida)
				{
					if($excepcion->abierto == 1)
					{
						$estilo = "diaactivo";	
						$abierto = 1;
					} 

					if($excepcion->abierto == 0)
					{
						$estilo = "diainactivo";	
					} 
				}

				if($estilo == 'diaactivo')
				{
					$linkPrev = "onclick='ver_horarios(\"$diaConvertido\", \"$diaMostrar\", \"$servicio\")'";
				}
				 
				 $calendario .="<div class='columna dia $estilo' $linkPrev><span id='a$diaConvertido' class='a$diaConvertido'>$x</span>";

				 if(isset($dias_del_mes[$diaConvertido]))
				 {
					 foreach($dias_del_mes[$diaConvertido] as $m)
					 {
						$calendario .="$m";
					 }
				 }
				
				 $calendario .="</div>";
			 }

			   if($dsemana == 7)
				   $dsemana = 0;
		   }
		   if($dsemana!=0)
		   {
			   for($x=$dsemana;$x<=6;$x++)
				  $calendario .="<div class='columna othermonth dia'></div>";
		   }
		   
		   $calendario .= "</div>
		   </div>   
		   ";
		}
	   
	    return $calendario;
	}	


	public function ajax_ver_horarios($servicio)
	{
		$data = array();
		$horarios_turno1 = array();
		$horarios_turno2 = array();
		$disponibles_turno1 = array();
		$disponibles_turno2 = array();

		$fecha = $this->input->post('fecha');
		$fecha_mostrar = $this->input->post('fecha_mostrar');
		$local_id = $this->input->post('local_id');
		$local_servicio_item_id = $this->input->post('local_servicio_item_id');


		// Recupera datos del servicio item del local
		$servicio_item = $this->local_servicio_item->get_by_id($local_servicio_item_id);

		// Duracion del servicio item
		$duracionturno = $servicio_item->duracion;

		// Cuantos intervalos de 30 minutos abarca el item 
		// Ej, un item de 60 minutos son 2 intervalos de 30 minutos
		$intervalos30turno = $duracionturno / 30;


		// Excepciones al horario del local para el dia actual
		$excepciones = $this->local_horario->get_excepcion_turno($local_id, $servicio, $fecha_mostrar);

		if ($excepciones)
		{
			foreach ($excepciones as $excepcion) 
			{
				$horarios_turno1[] = $excepcion->turno;
				$disponibles_turno1[] = TRUE;
			}
		}
		else
		{
			// Recupera horarios del local
			$horario = $this->local_horario->get_horario_servicio($local_id, $servicio);

			$trabaja_lu = false;
			$trabaja_ma = false;
			$trabaja_mi = false;
			$trabaja_ju = false;
			$trabaja_vi = false;
			$trabaja_sa = false;
			$trabaja_do = false;

			if($horario->lu == 1) $trabaja_lu = true;
			if($horario->ma == 1) $trabaja_ma = true;
			if($horario->mi == 1) $trabaja_mi = true;
			if($horario->ju == 1) $trabaja_ju = true;
			if($horario->vi == 1) $trabaja_vi = true;
			if($horario->sa == 1) $trabaja_sa = true;
			if($horario->do == 1) $trabaja_do = true;
			
			$diaquees = $this->tools->diaSemanaOF($fecha);

		 	 // Lunes
			 if($diaquees == 1)
			 {
				 if($trabaja_lu)
				 {
					// Primer turno
					$horainicio = $horario->lu_h_1;	
					$horafin = $horario->lu_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->lu_m_1 + $horario->lu_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->lu_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->lu_h_3;	
					$horafin = $horario->lu_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->lu_m_3 + $horario->lu_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->lu_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }

			 // Martes
			 if($diaquees == 2)
			 {
				 if($trabaja_ma)
				 {
					// Primer turno
					$horainicio = $horario->ma_h_1;	
					$horafin = $horario->ma_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ma_m_1 + $horario->ma_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->ma_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->ma_h_3;	
					$horafin = $horario->ma_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ma_m_3 + $horario->ma_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->ma_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }

			 // Miercoles
			 if($diaquees == 3)
			 {
				 if($trabaja_mi)
				 {
					// Primer turno
					$horainicio = $horario->mi_h_1;	
					$horafin = $horario->mi_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->mi_m_1 + $horario->mi_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->mi_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->mi_h_3;	
					$horafin = $horario->mi_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->mi_m_3 + $horario->mi_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->mi_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }

			 // Jueves
			 if($diaquees == 4)
			 {
				 if($trabaja_ju)
				 {
					// Primer turno
					$horainicio = $horario->ju_h_1;	
					$horafin = $horario->ju_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ju_m_1 + $horario->ju_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->ju_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->ju_h_3;	
					$horafin = $horario->ju_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ju_m_3 + $horario->ju_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->ju_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }		 

			 // Viernes
			 if($diaquees == 5)
			 {
				 if($trabaja_vi)
				 {
					// Primer turno
					$horainicio = $horario->vi_h_1;	
					$horafin = $horario->vi_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->vi_m_1 + $horario->vi_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->vi_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->vi_h_3;	
					$horafin = $horario->vi_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->vi_m_3 + $horario->vi_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->vi_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }		 

			 // Sabado
			 if($diaquees == 6)
			 {
				 if($trabaja_sa)
				 {
					// Primer turno
					$horainicio = $horario->sa_h_1;	
					$horafin = $horario->sa_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->sa_m_1 + $horario->sa_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->sa_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->sa_h_3;	
					$horafin = $horario->sa_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->sa_m_3 + $horario->sa_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->sa_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }		 

			 // Domingo
			 if($diaquees == 7)
			 {
				 if($trabaja_do)
				 {
					// Primer turno
					$horainicio = $horario->do_h_1;	
					$horafin = $horario->do_h_2;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->do_m_1 + $horario->do_m_2;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->do_m_1;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno1[] = $hora.":".$minutos;
						$disponibles_turno1[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}		

					// Segundo turno
					$horainicio = $horario->do_h_3;	
					$horafin = $horario->do_h_4;			 	

					$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->do_m_3 + $horario->do_m_4;

					$intervalos30 = $minutosDisponibles / 30;

					$minutosSiguienteTurno = $horainicio * 60 + $horario->do_m_3;

					// Arma array de horarios (en intervalos de 30 minutos)
					for($i = 1; $i <= $intervalos30; $i++)
					{
					 	$hora = floor($minutosSiguienteTurno / 60);
					 	$minutosFraccion = $minutosSiguienteTurno / 60;
						$minutos = ($minutosFraccion - $hora) * 60;

						if($minutos == 0) $minutos="00";

						$horarios_turno2[] = $hora.":".$minutos;
						$disponibles_turno2[] = TRUE;

					 	$minutosSiguienteTurno += 30;					
					}							 
				 }	
			 }			
		}
	
		$i = 0;

		// Primer turno
		foreach($horarios_turno1 as $h)
		{
			$count_turno = 0;

			// Valida si el turno esta disponible
			for ($j = 0; $j < $intervalos30turno; $j++) 
			{ 
				if ($i+$j < count($horarios_turno1))
				{
					$count_turno = $this->turno->get_count_by_local_horario($local_id, $servicio, $fecha, $horarios_turno1[$i+$j]);

					if ($count_turno > 0) $disponibles_turno1[$i] = FALSE;
				}
			}

			// Turnos del horario actual que abarcan mas de un intervalo de 30 minutos
			$turnoHorarioActual = $this->turno->get_by_local_horario($local_id, $servicio, $fecha, $horarios_turno1[$i]);

			$turnoHorarioActual ? $duracionTurnoHorarioActual = $turnoHorarioActual->duracion : $duracionTurnoHorarioActual = 0;
			$intervalosTurnoHorarioActual = $duracionTurnoHorarioActual / 30;

			for ($j = 0; $j < $intervalosTurnoHorarioActual; $j++) 
			{ 
				$disponibles_turno1[$i+$j] = FALSE;
			}			

			// Item actual abarca mas de un intervalo de 30 minutos
			if (count($horarios_turno1) - $i < $intervalos30turno) $disponibles_turno1[$i] = FALSE;

			// Cuando el horario es una excepcion, los turnos disponibles pueden no ser contiguos
			if ($excepciones && $intervalos30turno > 1)
			{
				for ($j = 0; $j < $intervalos30turno - 1; $j++) 
				{
					if ($i < count($horarios_turno1) - $j)
					{
						$horario_contiguo_hora = substr($horarios_turno1[$i + $j], 0, -3);
						$horario_contiguo_minutos = substr($horarios_turno1[$i+ $j], -2);

						if ($horario_contiguo_minutos == '00') 
						{
							$horario_contiguo_minutos = '30';
						}
						else
						{
							$horario_contiguo_hora += 1;
							$horario_contiguo_minutos = '00';
						}

						$horario_contiguo = $horario_contiguo_hora . ':' . $horario_contiguo_minutos;

						if ($i < count($horarios_turno1) - $j - 1)
						{
							if ($horarios_turno1[$i + $j + 1] != $horario_contiguo) $disponibles_turno1[$i] = FALSE;
						}						
					}
				}
			}

			$i++;
		}

		 $ddl_horarios = '<select name="ddl_horario" id="ddl_horario" class="form-control" required>';
		 $ddl_horarios .= '<option value="">Horarios disponibles...</option>';

		$i = 0;

		foreach($horarios_turno1 as $h)
		{
			if($disponibles_turno1[$i])
			{
				$ddl_horarios .= '<option value="' . $h . '">' . $h . ' hs</option>';
			}	
			else
			{
				$ddl_horarios .= '<option style="color: red" disabled value="' . $h . '">' . $h . ' hs (no disponible)</option>';
			}

			$i++;
		}

		$i = 0;

		// Segundo turno
		foreach($horarios_turno2 as $h)
		{
			$count_turno = 0;

			// Valida que el turno esté disponible
			for ($j = 0; $j < $intervalos30turno; $j++) 
			{ 
				if ($i+$j < count($horarios_turno2))
				{
					$count_turno = $this->turno->get_count_by_local_horario($local_id, $servicio, $fecha, $horarios_turno2[$i+$j]);

					if ($count_turno > 0) $disponibles_turno2[$i] = FALSE;
				}
			}

			// Turnos del horario actual que abarcan mas de un intervalo de 30 minutos
			$turnoHorarioActual = $this->turno->get_by_local_horario($local_id, $servicio, $fecha, $horarios_turno2[$i]);

			$turnoHorarioActual ? $duracionTurnoHorarioActual = $turnoHorarioActual->duracion : $duracionTurnoHorarioActual = 0;
			$intervalosTurnoHorarioActual = $duracionTurnoHorarioActual / 30;

			for ($j = 0; $j < $intervalosTurnoHorarioActual; $j++) 
			{ 
				$disponibles_turno2[$i+$j] = FALSE;
			}			

			// Item actual abarca mas de un intervalo de 30 minutos
			if (count($horarios_turno2) - $i < $intervalos30turno)
			{
				$disponibles_turno2[$i] = FALSE;
			}

			$i++;
		}

		$i = 0;

		foreach($horarios_turno2 as $h)
		{
			if($disponibles_turno2[$i])
			{
				$ddl_horarios .= '<option value="' . $h . '">' . $h . ' hs</option>';
			}	
			else
			{
				$ddl_horarios .= '<option style="color: red" disabled value="' . $h . '">' . $h . ' hs (no disponible)</option>';
			}

			$i++;
		}

		$ddl_horarios .= '</select>';

		$data['status'] = TRUE;
		$data['horarios'] = $ddl_horarios;

		echo json_encode($data);
	}


	public function ajax_crear_calendario_local(){

		$data = array();

		$servicio = $this->input->post('servicio');

		$data['calendario'] = $this->crearCalendarioLocal(null, $servicio);

		echo json_encode($data);		
	}


	public function ajax_calendario_cambia_mes_local(){

		$data = array();

		$fecha = $this->input->post('fecha');
		$servicio = $this->input->post('servicio');

		$data['calendario'] = $this->crearCalendarioLocal($fecha, $servicio);

		echo json_encode($data);		
	}


	private function crearCalendarioLocal($siguiente, $servicio)
	{
		$calendario = '';

		// Recupera horarios del local
		$horario = $this->local_horario->get_horario_servicio($_SESSION['frontend_user_id'], $servicio);

		// Valida que el local tenga configurado los horarios
		if ($horario === null){
			$calendario = '<div style="color: red">Calendario no configurado</div>';
		}
		else
		{
			$trabaja_lu = false;
			$trabaja_ma = false;
			$trabaja_mi = false;
			$trabaja_ju = false;
			$trabaja_vi = false;
			$trabaja_sa = false;
			$trabaja_do = false;

			if($horario->lu == 1) $trabaja_lu = true;
			if($horario->ma == 1) $trabaja_ma = true;
			if($horario->mi == 1) $trabaja_mi = true;
			if($horario->ju == 1) $trabaja_ju = true;
			if($horario->vi == 1) $trabaja_vi = true;
			if($horario->sa == 1) $trabaja_sa = true;
			if($horario->do == 1) $trabaja_do = true;
			
			if(isset($siguiente))
				$fechahoy = $siguiente;
			else
				$fechahoy = date('d-m-Y'); //fecha a tomar
			
			$fechahoyConvertida = date('Ymd');
			
			$idMesSiguiente = $this->tools->mesSiguiente($fechahoy);
			$idMesAnterior = $this->tools->mesAnterior($fechahoy);
			
			$nroMes = $this->tools->idddeMes($fechahoy);
			$nroAnio = $this->tools->iddeAnio($fechahoy);

			$eventos_del_mes=array();
			$dias_del_mes=array();

			for($x = 1; $x <= 31; $x ++)
				$dias_del_mes[$x]=array();
			

		   $calendario = "
		   <div id='vp_eventos'>
		   <h3 class='mesactual'>" . $this->tools->nombreMes($fechahoy)." $nroAnio</h3>
		   <div class='clear'></div>";
			
		  if($this->tools->idddeMes($fechahoy) != $this->tools->idddeMes(date('d-m-Y')))
		  {
		  		$calendario .="<div class='prevmonth'><a href='#' onclick='calendario_cambia_mes_local(\"$idMesAnterior\",\"$servicio\"); return false'><</a></div>";
		  }
		  
		  $diassemana=array(
			1 =>"LUN",
			2 =>"MAR",
			3 =>"MIE",
			4 =>"JUE",
			5 =>"VIE",
			6 =>"SAB",
			7 =>"DOM",
			);
		   
		   $calendario.="
		   <div class='nextmonth'><a href='#' onclick='calendario_cambia_mes_local(\"$idMesSiguiente\",\"$servicio\"); return false'>></a></div>
		   <div class='clear'></div>
		   <div class='tabla'>";

		   foreach($diassemana as $ds=>$s)
		   {
			   $calendario .="<div class='columna titulos'>$s</div>";
		   }
		   
		   	$totalDias = $this->tools->cantidadDiasdelMes($fechahoy); //dias totales del mes
		   	$primerDiaSemana = $this->tools->diaSemana($this->tools->_data_first_month_day($fechahoy)); //donde cae el 1
			$comenzoMes=false;
			$dsemana=0;

		   for($x=1;$x<=$totalDias; $x++)
		   {
			   $dsemana++;	   
			   if($x<=9)
				   $r="0".$x;
			   else
				   $r=$x;

				$diaConvertido=$nroAnio.$nroMes.$r;
				$diaMostrar = $r . '/' . $nroMes . '/' . $nroAnio;
				$diaConsulta = $nroAnio . '/' . $nroMes . '/' . $r;

				// Valida si hay una excepcion a los horarios para el dia y servicio actual
				$excepcion = $this->local_horario->get_excepcion_dia($_SESSION['frontend_user_id'], $servicio, $diaMostrar);

			   if($comenzoMes == false)
			   {		   
				   if($dsemana==$primerDiaSemana)
				   {
					 $comenzoMes = true; 
					 $diaquees = $this->tools->diaSemanaOF($diaConvertido);
					 $linkPrev="";
					 $linkNext="";
					 
					 if($diaquees == 1)
					 {
						 if($trabaja_lu == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 if($diaquees == 2)
					 {
						 if($trabaja_ma == true)
							 $estilo="diaactivo";
						 else
							  $estilo="diacerrado";
					 }
					 if($diaquees == 3)
					 {
						 if($trabaja_mi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 if($diaquees == 4)
					 {
						 if($trabaja_ju == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }					 					 
					 if($diaquees == 5)
					 {
						 if($trabaja_vi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 else if($diaquees == 6)
					 {
						  if($trabaja_sa == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diacerrado";
					 }
					 else if($diaquees == 7)
					 {
						  if($trabaja_do == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diacerrado";
					 }

					if(!is_null($excepcion))
					{
						if($excepcion->abierto == 1) $estilo = "diaactivo";
						if($excepcion->abierto == 0) $estilo = "diacerrado";
					}

					// Dias anteriores a fecha actual
					if($diaConvertido <= $fechahoyConvertida) $estilo = "diainactivo";

					// Valida si hay turnos reservados para el dia actual
					$count_turno = $this->turno->get_count_by_local_fecha($_SESSION['frontend_user_id'], $servicio, $diaConsulta);
					
					if(!is_null($excepcion) && $estilo != "diainactivo")
					{
						$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", $excepcion->abierto, \"$servicio\", $count_turno)'";
					}
					else
					{
						if($estilo == 'diaactivo')
						{
							$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", 1,  \"$servicio\", $count_turno)'";
						}

						if($estilo == 'diacerrado')
						{
							$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", 0, \"$servicio\", $count_turno)'";
						}
					}
					 
					 $calendario .="<div class='columna dia $estilo' $linkPrev><span id='a$diaConvertido'>$x</span>";
					 if(isset($dias_del_mes[$diaConvertido]))
					 {
						  foreach($dias_del_mes[$diaConvertido] as $m)
						 {
							$my_postid = $m;//This is page id or post i

							//$calendario .="<div class='contenido'><div class='imagen'><a href='$link'><img src='$imagen' /></a></div><a href='$link'>".$content."</a></div>";
							$calendario .="$m";
						 }
					 }
					
					 $calendario .="</div>";		
				   }
				   else
				   {
					   $x=0;
					   $calendario .="<div class='columna othermonth dia'></div>";
				   }			   
			   }
				 else
				 {
					 $diaquees = $this->tools->diaSemanaOF($diaConvertido);
					 $linkPrev="";
					 $linkNext="";

					 if($diaquees == 1)
					 {
						 if($trabaja_lu == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 if($diaquees == 2)
					 {
						 if($trabaja_ma == true)
							 $estilo="diaactivo";
						 else
							  $estilo="diacerrado";
					 }
					 if($diaquees == 3)
					 {
						 if($trabaja_mi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 if($diaquees == 4)
					 {
						 if($trabaja_ju == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }					 					 
					 if($diaquees == 5)
					 {
						 if($trabaja_vi == true)
							 $estilo = "diaactivo";
						 else
							  $estilo = "diacerrado";
					 }
					 else if($diaquees == 6)
					 {
						  if($trabaja_sa == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diacerrado";
					 }
					 else if($diaquees == 7)
					 {
						  if($trabaja_do == true)
							   $estilo = "diaactivo";
						   else
							    $estilo = "diacerrado";
					 }

					if(!is_null($excepcion))
					{
						if($excepcion->abierto == 1) $estilo = "diaactivo";
						if($excepcion->abierto == 0) $estilo = "diacerrado";
					}

					// Dias anteriores a fecha actual
					if($diaConvertido <= $fechahoyConvertida) $estilo = "diainactivo";
					
					// Valida si hay turnos reservados para el dia actual
					$count_turno = $this->turno->get_count_by_local_fecha($_SESSION['frontend_user_id'], $servicio, $diaConsulta);
					
					if(!is_null($excepcion) && $estilo != "diainactivo")
					{
						$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", $excepcion->abierto, \"$servicio\", $count_turno)'";
					}
					else
					{
						if($estilo == 'diaactivo')
						{
							$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", 1,  \"$servicio\", $count_turno)'";
						}

						if($estilo == 'diacerrado')
						{
							$linkPrev="onclick='ver_dia_turno(\"$diaConvertido\", \"$diaMostrar\", 0, \"$servicio\", $count_turno)'";
						}
					}
					 
					 $calendario .="<div class='columna dia $estilo' $linkPrev><span id='a$diaConvertido'>$x</span>";
					 if(isset($dias_del_mes[$diaConvertido]))
					 {
						  foreach($dias_del_mes[$diaConvertido] as $m)
						 {
							$my_postid = $m;//This is page id or post i
							$calendario .="$m";
						 }
					 }
					
					 $calendario .="</div>";
				 }
			   if($dsemana==7)
				   $dsemana=0;
		   }
		   if($dsemana!=0)
		   {
			   for($x=$dsemana;$x<=6;$x++)
				  $calendario .="<div class='columna othermonth dia'></div>";
		   }
		   
		   $calendario .= "</div>
		   </div>   
		   ";
		}
	   
	    return $calendario;
	}		


	public function ajax_ver_dia_turno()
	{
		$data = array();
		$horarios_turno = array();
		$disponibles_turno = array();

		$fecha = $this->input->post('fecha');
		$fecha_mostrar = $this->input->post('fecha_mostrar');
		$servicio = $this->input->post('servicio');


		// Intervalos de 30 minutos que hay entre las 8:00 y las 21:30 (horario maximo disponible)
		// TODO: hacer estos rangos maximos configurable
		$intervalos30turno1 = 14;  // de 8:00 a 14:30
		$intervalos30turno2 = 13;  // de 15:00 a 21:00

		// Primer Turno
		$minutosSiguienteTurno = 8 * 60; // (8:00 hs * 60 minutos)

		// Arma array de horarios (en intervalos de 30 minutos)
		for($i = 1; $i <= $intervalos30turno1; $i++)
		{
		 	$hora = floor($minutosSiguienteTurno / 60);
		 	$minutosFraccion = $minutosSiguienteTurno / 60;
			$minutos = ($minutosFraccion - $hora) * 60;

			if($minutos == 0) $minutos="00";

			$horarios_turno[] = $hora.":".$minutos;
			$disponibles_turno[] = TRUE;
			$reservados_turno[] = '';

		 	$minutosSiguienteTurno += 30;					
		}		

		// Segundo Turno
		$minutosSiguienteTurno = 15 * 60; // (15:00 hs * 60 minutos)

		// Arma array de horarios (en intervalos de 30 minutos)
		for($i = 1; $i <= $intervalos30turno2; $i++)
		{
		 	$hora = floor($minutosSiguienteTurno / 60);
		 	$minutosFraccion = $minutosSiguienteTurno / 60;
			$minutos = ($minutosFraccion - $hora) * 60;

			if($minutos == 0) $minutos="00";

			$horarios_turno[] = $hora.":".$minutos;
			$disponibles_turno[] = TRUE;
			$reservados_turno[] = '';

		 	$minutosSiguienteTurno += 30;					
		}

		// Excepciones al horario del local para el dia actual
		$excepciones = $this->local_horario->get_excepcion_turno($_SESSION['frontend_user_id'], $servicio, $fecha_mostrar);

		if ($excepciones)
		{
			// Primer Turno
			for($i = 0; $i < $intervalos30turno1; $i++)
			{
				foreach ($excepciones as $excepcion) 
				{
					if ($horarios_turno[$i] == $excepcion->turno) $disponibles_turno[$i] = FALSE;
				}
			}	

			// Segundo Turno
			for($i = $intervalos30turno1; $i < $intervalos30turno1 + $intervalos30turno2; $i++)
			{
				foreach ($excepciones as $excepcion) 
				{
					if ($horarios_turno[$i] == $excepcion->turno) $disponibles_turno[$i] = FALSE;
				}
			}				
		}
		else
		{
			// Recupera horarios del local
			$horario = $this->local_horario->get_horario_servicio($_SESSION['frontend_user_id'], $servicio);

			$diaquees = $this->tools->diaSemanaOF($fecha);

			// Lunes
			if($diaquees == 1)
			{
				// Primer turno				
				$horainicio = $horario->lu_h_1;	
				$horafin = $horario->lu_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->lu_m_1 + $horario->lu_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->lu_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->lu_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->lu_h_3;	
				$horafin = $horario->lu_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->lu_m_3 + $horario->lu_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->lu_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->lu_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}

			// Martes
			if($diaquees == 2)
			{
				// Primer turno				
				$horainicio = $horario->ma_h_1;	
				$horafin = $horario->ma_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ma_m_1 + $horario->ma_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->ma_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->ma_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->ma_h_3;	
				$horafin = $horario->ma_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ma_m_3 + $horario->ma_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->ma_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->ma_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}

			// Miercoles
			if($diaquees == 3)
			{
				// Primer turno				
				$horainicio = $horario->mi_h_1;	
				$horafin = $horario->mi_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->mi_m_1 + $horario->mi_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->mi_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->mi_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->mi_h_3;	
				$horafin = $horario->mi_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->mi_m_3 + $horario->mi_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->mi_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->mi_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}

			// Jueves
			if($diaquees == 4)
			{
				// Primer turno				
				$horainicio = $horario->ju_h_1;	
				$horafin = $horario->ju_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ju_m_1 + $horario->ju_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->ju_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->ju_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->ju_h_3;	
				$horafin = $horario->ju_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->ju_m_3 + $horario->ju_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->ju_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->ju_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}

			// Viernes
			if($diaquees == 5)
			{
				// Primer turno				
				$horainicio = $horario->vi_h_1;	
				$horafin = $horario->vi_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->vi_m_1 + $horario->vi_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->vi_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->vi_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->vi_h_3;	
				$horafin = $horario->vi_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->vi_m_3 + $horario->vi_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->vi_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->vi_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}	

			// Sabado
			if($diaquees == 6)
			{
				// Primer turno				
				$horainicio = $horario->sa_h_1;	
				$horafin = $horario->sa_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->sa_m_1 + $horario->sa_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->sa_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->sa_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->sa_h_3;	
				$horafin = $horario->sa_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->sa_m_3 + $horario->sa_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->sa_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->sa_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}		
			
			// Domingo
			if($diaquees == 7)
			{
				// Primer turno				
				$horainicio = $horario->do_h_1;	
				$horafin = $horario->do_h_2;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->do_m_1 + $horario->do_m_2;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->do_m_1;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->do_m_1; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}

				// Segundo turno				
				$horainicio = $horario->do_h_3;	
				$horafin = $horario->do_h_4;			 	

				$minutosDisponibles = ($horafin - $horainicio) * 60 - $horario->do_m_3 + $horario->do_m_4;
				$intervalos30 = $minutosDisponibles / 30;
				$minutosSiguienteTurno = $horainicio * 60 + $horario->do_m_3;

				// Diferencia en el inicio del horario del local y del horario maximo disponible
				$minutosPrimerTurnoDisponible = 8 * 60; // (8:00 hs * 60 minutos)
				$minutosPrimerTurnoLocal = $horainicio * 60 + $horario->do_m_3; 

				$diferencia = ($minutosPrimerTurnoLocal - $minutosPrimerTurnoDisponible) / 30;

				for($i = 0; $i < $intervalos30; $i++)
				{
					$disponibles_turno[$i+$diferencia] = FALSE;					
				}
			}						
		}

		 		
		// Carga turnos agendados para el dia actual
		for($i = 0; $i < count($horarios_turno); $i++)
		{
			// Turnos del horario actual que abarcan mas de un intervalo de 30 minutos
			$turnoHorarioActual = $this->turno->get_by_local_horario($_SESSION['frontend_user_id'], $servicio, $fecha, $horarios_turno[$i]);

			$turnoHorarioActual ? $duracionTurnoHorarioActual = $turnoHorarioActual->duracion : $duracionTurnoHorarioActual = 0;
			$intervalosTurnoHorarioActual = $duracionTurnoHorarioActual / 30;

			for ($j = 0; $j < $intervalosTurnoHorarioActual; $j++) 
			{ 
				$reservados_turno[$i+$j] = $turnoHorarioActual->numero . ' - ' . $turnoHorarioActual->item;
			}
		}

						 		 
		$ddl_horarios1 = '<select name="ddl_horario1[]" id="ddl_horario1" class="form-control" required multiple size="14" style="overflow:hidden;">';
		$ddl_horarios2 = '<select name="ddl_horario2[]" id="ddl_horario2" class="form-control" required multiple size="14" style="overflow:hidden;">';

		$i = 0;

		// Primer Turno
		for($i = 0; $i < $intervalos30turno1; $i++)
		{

			$horario_mostrar = str_pad($horarios_turno[$i], 5, "0", STR_PAD_LEFT);

			if($reservados_turno[$i] != '')
			{
				$ddl_horarios1 .= '<option selected disabled value="' . $horarios_turno[$i] . '">' . $horario_mostrar . ' hs &nbsp;&nbsp;' .  $reservados_turno[$i] . '</option>';

				// Para no repetir el mismo turno para cada intervalo de 30 minutos
				/*
				if ($i > 0 && $reservados_turno[$i] == $reservados_turno[$i-1])
				{
					$ddl_horarios1 .= '<option selected disabled value="">' . $horarios_turno[$i] . ' hs</option>';
				}
				else
				{
					$ddl_horarios1 .= '<option selected disabled value="">' . $horarios_turno[$i] . ' hs &nbsp;&nbsp;' .  $reservados_turno[$i] . '</option>';
				}
				*/
			}			
			else
			{
				if($disponibles_turno[$i])
				{
					$ddl_horarios1 .= '<option value="' . $horarios_turno[$i] . '">' . $horario_mostrar . ' hs</option>';
				}	
				else
				{
					$ddl_horarios1 .= '<option selected value="' . $horarios_turno[$i] . '">' . $horario_mostrar . ' hs</option>';
				}				
			}
		}

		// Segundo Turno
		for($i = $intervalos30turno1; $i < $intervalos30turno1 + $intervalos30turno2; $i++)
		{
			$horario_mostrar = str_pad($horarios_turno[$i], 5, "0", STR_PAD_LEFT);

			if($reservados_turno[$i] != '')
			{
				$ddl_horarios2 .= '<option selected disabled value="">' . $horario_mostrar . ' hs &nbsp;&nbsp;' .  $reservados_turno[$i] . '</option>';
			}			
			else
			{
				if($disponibles_turno[$i])
				{
					$ddl_horarios2 .= '<option value="' . $horarios_turno[$i] . '">' . $horario_mostrar . ' hs</option>';
				}	
				else
				{
					$ddl_horarios2 .= '<option selected value="' . $horarios_turno[$i] . '">' . $horario_mostrar . ' hs</option>';
				}				
			}
		}

		$ddl_horarios1 .= '</select>';
		$ddl_horarios2 .= '</select>';

		$data['status'] = TRUE;
		$data['horarios1'] = $ddl_horarios1;
		$data['horarios2'] = $ddl_horarios2;


		echo json_encode($data);		
	}


	// Actualiza horarios de turnos para un dia en particular
	public function ajax_actualizar_dia_turno()
	{
		$data = array();
		$data['status'] = TRUE;

		// Valida datos ingresados
        $servicio = $this->input->post('hd_dia_turno_servicio');
        $dia = $this->input->post('hd_dia_turno_dia');
        $abierto = $this->input->post('hd_dia_turno_abierto');

        // DIA ABIERTO / CERRADO
        // Elimina excepcion actual para ese dia
        $this->local_horario->delete_excepcion_dia($_SESSION['frontend_user_id'], $servicio, $dia);

        // Inserta excepcion
		$data_insert = array(
			'local_horario_excepcion_dia_local_id' => $_SESSION['frontend_user_id'],
			'local_horario_excepcion_dia_servicio' => $servicio,
			'local_horario_excepcion_dia_dia' => $dia,
			'local_horario_excepcion_dia_abierto' => $abierto
		);

		$this->local_horario->save_excepcion_dia($data_insert);		

		// HORARIOS DEL DIA
        // Elimina excepciones actuales para ese dia
        $this->local_horario->delete_excepcion_turno($_SESSION['frontend_user_id'], $servicio, $dia);

		// Primer Turno
        $data_horario1 = $this->input->post('ddl_horario1');
         
        for($i = 0; $i < count($data_horario1); $i++) 
        {
			$data_insert = array(
				'local_horario_excepcion_turno_local_id' => $_SESSION['frontend_user_id'],
				'local_horario_excepcion_turno_servicio' => $servicio,
				'local_horario_excepcion_turno_dia' => $dia,
				'local_horario_excepcion_turno_turno' => $data_horario1[$i]
			);

			$this->local_horario->save_excepcion_turno($data_insert);		
	    }		

		// Segundo Turno
        $data_horario2 = $this->input->post('ddl_horario2');
         
        for($i = 0; $i < count($data_horario2); $i++) 
        {
			$data_insert = array(
				'local_horario_excepcion_turno_local_id' => $_SESSION['frontend_user_id'],
				'local_horario_excepcion_turno_servicio' => $servicio,
				'local_horario_excepcion_turno_dia' => $dia,
				'local_horario_excepcion_turno_turno' => $data_horario2[$i]
			);

			$this->local_horario->save_excepcion_turno($data_insert);		
	    }		    	

		echo json_encode($data);		
	}


	public function ajax_get_horario_servicio($servicio){

		// Recupera horarios del local
		$horario = $this->local_horario->get_horario_servicio($_SESSION['frontend_user_id'], $servicio);

		echo json_encode($horario);
	}


	public function ajax_update_horarios_clinica()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_update_horarios_clinica();

		// Validación OK
		if ($error->status){

			$abre_lu = 0;
			$abre_ma = 0;
			$abre_mi = 0;
			$abre_ju = 0;
			$abre_vi = 0;
			$abre_sa = 0;
			$abre_do = 0;

			if (isset($_POST['abre_lu_clinica'])) $abre_lu = 1;
			if (isset($_POST['abre_ma_clinica'])) $abre_ma = 1;
			if (isset($_POST['abre_mi_clinica'])) $abre_mi = 1;
			if (isset($_POST['abre_ju_clinica'])) $abre_ju = 1;
			if (isset($_POST['abre_vi_clinica'])) $abre_vi = 1;
			if (isset($_POST['abre_sa_clinica'])) $abre_sa = 1;
			if (isset($_POST['abre_do_clinica'])) $abre_do = 1;

			// Actualiza datos del local
			$data_update = array(
				
				'local_horario_local_id' => $_SESSION['frontend_user_id'],
				'local_horario_servicio' => 'clinica',
				'local_horario_lu' => $abre_lu,
				'local_horario_lu_h_1' => trim($this->input->post('lu_h_1_clinica')),
				'local_horario_lu_m_1' => trim($this->input->post('lu_m_1_clinica')),
				'local_horario_lu_h_2' => trim($this->input->post('lu_h_2_clinica')),
				'local_horario_lu_m_2' => trim($this->input->post('lu_m_2_clinica')),
				'local_horario_lu_h_3' => trim($this->input->post('lu_h_3_clinica')),
				'local_horario_lu_m_3' => trim($this->input->post('lu_m_3_clinica')),
				'local_horario_lu_h_4' => trim($this->input->post('lu_h_4_clinica')),
				'local_horario_lu_m_4' => trim($this->input->post('lu_m_4_clinica')),

				'local_horario_ma' => $abre_ma,
				'local_horario_ma_h_1' => trim($this->input->post('ma_h_1_clinica')),
				'local_horario_ma_m_1' => trim($this->input->post('ma_m_1_clinica')),
				'local_horario_ma_h_2' => trim($this->input->post('ma_h_2_clinica')),
				'local_horario_ma_m_2' => trim($this->input->post('ma_m_2_clinica')),
				'local_horario_ma_h_3' => trim($this->input->post('ma_h_3_clinica')),
				'local_horario_ma_m_3' => trim($this->input->post('ma_m_3_clinica')),
				'local_horario_ma_h_4' => trim($this->input->post('ma_h_4_clinica')),
				'local_horario_ma_m_4' => trim($this->input->post('ma_m_4_clinica')),

				'local_horario_mi' => $abre_mi,
				'local_horario_mi_h_1' => trim($this->input->post('mi_h_1_clinica')),
				'local_horario_mi_m_1' => trim($this->input->post('mi_m_1_clinica')),
				'local_horario_mi_h_2' => trim($this->input->post('mi_h_2_clinica')),
				'local_horario_mi_m_2' => trim($this->input->post('mi_m_2_clinica')),
				'local_horario_mi_h_3' => trim($this->input->post('mi_h_3_clinica')),
				'local_horario_mi_m_3' => trim($this->input->post('mi_m_3_clinica')),
				'local_horario_mi_h_4' => trim($this->input->post('mi_h_4_clinica')),
				'local_horario_mi_m_4' => trim($this->input->post('mi_m_4_clinica')),		
				
				'local_horario_ju' => $abre_ju,
				'local_horario_ju_h_1' => trim($this->input->post('ju_h_1_clinica')),
				'local_horario_ju_m_1' => trim($this->input->post('ju_m_1_clinica')),
				'local_horario_ju_h_2' => trim($this->input->post('ju_h_2_clinica')),
				'local_horario_ju_m_2' => trim($this->input->post('ju_m_2_clinica')),
				'local_horario_ju_h_3' => trim($this->input->post('ju_h_3_clinica')),
				'local_horario_ju_m_3' => trim($this->input->post('ju_m_3_clinica')),
				'local_horario_ju_h_4' => trim($this->input->post('ju_h_4_clinica')),
				'local_horario_ju_m_4' => trim($this->input->post('ju_m_4_clinica')),

				'local_horario_vi' => $abre_vi,
				'local_horario_vi_h_1' => trim($this->input->post('vi_h_1_clinica')),
				'local_horario_vi_m_1' => trim($this->input->post('vi_m_1_clinica')),
				'local_horario_vi_h_2' => trim($this->input->post('vi_h_2_clinica')),
				'local_horario_vi_m_2' => trim($this->input->post('vi_m_2_clinica')),
				'local_horario_vi_h_3' => trim($this->input->post('vi_h_3_clinica')),
				'local_horario_vi_m_3' => trim($this->input->post('vi_m_3_clinica')),
				'local_horario_vi_h_4' => trim($this->input->post('vi_h_4_clinica')),
				'local_horario_vi_m_4' => trim($this->input->post('vi_m_4_clinica')),	
				
				'local_horario_sa' => $abre_sa,
				'local_horario_sa_h_1' => trim($this->input->post('sa_h_1_clinica')),
				'local_horario_sa_m_1' => trim($this->input->post('sa_m_1_clinica')),
				'local_horario_sa_h_2' => trim($this->input->post('sa_h_2_clinica')),
				'local_horario_sa_m_2' => trim($this->input->post('sa_m_2_clinica')),
				'local_horario_sa_h_3' => trim($this->input->post('sa_h_3_clinica')),
				'local_horario_sa_m_3' => trim($this->input->post('sa_m_3_clinica')),
				'local_horario_sa_h_4' => trim($this->input->post('sa_h_4_clinica')),
				'local_horario_sa_m_4' => trim($this->input->post('sa_m_4_clinica')),

				'local_horario_do' => $abre_do,
				'local_horario_do_h_1' => trim($this->input->post('do_h_1_clinica')),
				'local_horario_do_m_1' => trim($this->input->post('do_m_1_clinica')),
				'local_horario_do_h_2' => trim($this->input->post('do_h_2_clinica')),
				'local_horario_do_m_2' => trim($this->input->post('do_m_2_clinica')),
				'local_horario_do_h_3' => trim($this->input->post('do_h_3_clinica')),
				'local_horario_do_m_3' => trim($this->input->post('do_m_3_clinica')),
				'local_horario_do_h_4' => trim($this->input->post('do_h_4_clinica')),
				'local_horario_do_m_4' => trim($this->input->post('do_m_4_clinica'))
			);

			// Recupera horarios del local
			$horario = $this->local_horario->get_horario_servicio($_SESSION['frontend_user_id'], 'clinica');

			// Valida que el local tenga configurado los horarios
			if ($horario === null){
				$this->local_horario->save($data_update);
			}	
			else
			{
				$this->local_horario->update(array('local_horario_local_id' => $_SESSION['frontend_user_id'], 'local_horario_servicio' => 'clinica'), $data_update);
			}		
		
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_update_horarios_peluqueria()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_update_horarios_peluqueria();

		// Validación OK
		if ($error->status){

			$abre_lu = 0;
			$abre_ma = 0;
			$abre_mi = 0;
			$abre_ju = 0;
			$abre_vi = 0;
			$abre_sa = 0;
			$abre_do = 0;

			if (isset($_POST['abre_lu_peluqueria'])) $abre_lu = 1;
			if (isset($_POST['abre_ma_peluqueria'])) $abre_ma = 1;
			if (isset($_POST['abre_mi_peluqueria'])) $abre_mi = 1;
			if (isset($_POST['abre_ju_peluqueria'])) $abre_ju = 1;
			if (isset($_POST['abre_vi_peluqueria'])) $abre_vi = 1;
			if (isset($_POST['abre_sa_peluqueria'])) $abre_sa = 1;
			if (isset($_POST['abre_do_peluqueria'])) $abre_do = 1;

			// Actualiza datos del local
			$data_update = array(
				
				'local_horario_local_id' => $_SESSION['frontend_user_id'],
				'local_horario_servicio' => 'peluqueria',
				'local_horario_lu' => $abre_lu,
				'local_horario_lu_h_1' => trim($this->input->post('lu_h_1_peluqueria')),
				'local_horario_lu_m_1' => trim($this->input->post('lu_m_1_peluqueria')),
				'local_horario_lu_h_2' => trim($this->input->post('lu_h_2_peluqueria')),
				'local_horario_lu_m_2' => trim($this->input->post('lu_m_2_peluqueria')),
				'local_horario_lu_h_3' => trim($this->input->post('lu_h_3_peluqueria')),
				'local_horario_lu_m_3' => trim($this->input->post('lu_m_3_peluqueria')),
				'local_horario_lu_h_4' => trim($this->input->post('lu_h_4_peluqueria')),
				'local_horario_lu_m_4' => trim($this->input->post('lu_m_4_peluqueria')),

				'local_horario_ma' => $abre_ma,
				'local_horario_ma_h_1' => trim($this->input->post('ma_h_1_peluqueria')),
				'local_horario_ma_m_1' => trim($this->input->post('ma_m_1_peluqueria')),
				'local_horario_ma_h_2' => trim($this->input->post('ma_h_2_peluqueria')),
				'local_horario_ma_m_2' => trim($this->input->post('ma_m_2_peluqueria')),
				'local_horario_ma_h_3' => trim($this->input->post('ma_h_3_peluqueria')),
				'local_horario_ma_m_3' => trim($this->input->post('ma_m_3_peluqueria')),
				'local_horario_ma_h_4' => trim($this->input->post('ma_h_4_peluqueria')),
				'local_horario_ma_m_4' => trim($this->input->post('ma_m_4_peluqueria')),

				'local_horario_mi' => $abre_mi,
				'local_horario_mi_h_1' => trim($this->input->post('mi_h_1_peluqueria')),
				'local_horario_mi_m_1' => trim($this->input->post('mi_m_1_peluqueria')),
				'local_horario_mi_h_2' => trim($this->input->post('mi_h_2_peluqueria')),
				'local_horario_mi_m_2' => trim($this->input->post('mi_m_2_peluqueria')),
				'local_horario_mi_h_3' => trim($this->input->post('mi_h_3_peluqueria')),
				'local_horario_mi_m_3' => trim($this->input->post('mi_m_3_peluqueria')),
				'local_horario_mi_h_4' => trim($this->input->post('mi_h_4_peluqueria')),
				'local_horario_mi_m_4' => trim($this->input->post('mi_m_4_peluqueria')),		
				
				'local_horario_ju' => $abre_ju,
				'local_horario_ju_h_1' => trim($this->input->post('ju_h_1_peluqueria')),
				'local_horario_ju_m_1' => trim($this->input->post('ju_m_1_peluqueria')),
				'local_horario_ju_h_2' => trim($this->input->post('ju_h_2_peluqueria')),
				'local_horario_ju_m_2' => trim($this->input->post('ju_m_2_peluqueria')),
				'local_horario_ju_h_3' => trim($this->input->post('ju_h_3_peluqueria')),
				'local_horario_ju_m_3' => trim($this->input->post('ju_m_3_peluqueria')),
				'local_horario_ju_h_4' => trim($this->input->post('ju_h_4_peluqueria')),
				'local_horario_ju_m_4' => trim($this->input->post('ju_m_4_peluqueria')),

				'local_horario_vi' => $abre_vi,
				'local_horario_vi_h_1' => trim($this->input->post('vi_h_1_peluqueria')),
				'local_horario_vi_m_1' => trim($this->input->post('vi_m_1_peluqueria')),
				'local_horario_vi_h_2' => trim($this->input->post('vi_h_2_peluqueria')),
				'local_horario_vi_m_2' => trim($this->input->post('vi_m_2_peluqueria')),
				'local_horario_vi_h_3' => trim($this->input->post('vi_h_3_peluqueria')),
				'local_horario_vi_m_3' => trim($this->input->post('vi_m_3_peluqueria')),
				'local_horario_vi_h_4' => trim($this->input->post('vi_h_4_peluqueria')),
				'local_horario_vi_m_4' => trim($this->input->post('vi_m_4_peluqueria')),	
				
				'local_horario_sa' => $abre_sa,
				'local_horario_sa_h_1' => trim($this->input->post('sa_h_1_peluqueria')),
				'local_horario_sa_m_1' => trim($this->input->post('sa_m_1_peluqueria')),
				'local_horario_sa_h_2' => trim($this->input->post('sa_h_2_peluqueria')),
				'local_horario_sa_m_2' => trim($this->input->post('sa_m_2_peluqueria')),
				'local_horario_sa_h_3' => trim($this->input->post('sa_h_3_peluqueria')),
				'local_horario_sa_m_3' => trim($this->input->post('sa_m_3_peluqueria')),
				'local_horario_sa_h_4' => trim($this->input->post('sa_h_4_peluqueria')),
				'local_horario_sa_m_4' => trim($this->input->post('sa_m_4_peluqueria')),

				'local_horario_do' => $abre_do,
				'local_horario_do_h_1' => trim($this->input->post('do_h_1_peluqueria')),
				'local_horario_do_m_1' => trim($this->input->post('do_m_1_peluqueria')),
				'local_horario_do_h_2' => trim($this->input->post('do_h_2_peluqueria')),
				'local_horario_do_m_2' => trim($this->input->post('do_m_2_peluqueria')),
				'local_horario_do_h_3' => trim($this->input->post('do_h_3_peluqueria')),
				'local_horario_do_m_3' => trim($this->input->post('do_m_3_peluqueria')),
				'local_horario_do_h_4' => trim($this->input->post('do_h_4_peluqueria')),
				'local_horario_do_m_4' => trim($this->input->post('do_m_4_peluqueria'))
			);

			// Recupera horarios del local
			$horario = $this->local_horario->get_horario_servicio($_SESSION['frontend_user_id'], 'peluqueria');

			// Valida que el local tenga configurado los horarios
			if ($horario === null){
				$this->local_horario->save($data_update);
			}	
			else
			{
				$this->local_horario->update(array('local_horario_local_id' => $_SESSION['frontend_user_id'], 'local_horario_servicio' => 'peluqueria'), $data_update);
			}		
		
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_cargar(){

		$data = $this->local->get_by_id($_SESSION['frontend_user_id']);

		// Recupera banner global
		$data->banner_global = $this->banner->get_global();

		// Recupera banner grupal asociado al local
		$data->banner_grupal = $this->banner->get_by_local($_SESSION['frontend_user_id']);

		echo json_encode($data);
	}	


	// Listado de Pedidos local (ventas con estado 'pendiente')
	// Se llama desde el dashboard del local - Tab 'Pedidos'
	public function ajax_pedidos_list()
	{
		$data = new stdClass();

		// Valida que el local tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->venta->get_datatables_pedidos_local(0, $_SESSION['frontend_user_id']);

			$datatable = array();
			$no = $_POST['start'];

			foreach ($list as $pedido) {
				$no++;
				$row = array();
				$row_acciones = '';

				$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Venta" onclick="detalle_venta('."'".$pedido->id."'".')">&nbsp;&nbsp;Venta ' . $pedido->numero . '&nbsp;</a>';

				$fecha = new DateTime($pedido->fecha);
				$row[] = $fecha->format('d/m/Y');

				$row[] = $pedido->forma_pago;	

				$row[] = $pedido->cliente;

				$row[] = "$ " . $pedido->total;

				// ACCIONES
				// Para pedidos con estado 'pendiente' pero ya notificados por el local, muestra 'ya notificaste'
				// se da en casos que el local notificó como 'no concretada' y el cliente todavía no notifica
				if ($pedido->concretado_local != '')
				{
					$row_acciones = '<td class="hidden-xs"><span class="text-success font-w600">Ya notificaste</span></td>';
				}

				// Si el cliente ya notificó, no permite cancelar el pedido	(solo permite notificar)	
				// se da en casos que el cliente notificó como 'no concretada' y el local todavía no notifica
				elseif ($pedido->concretado_cliente != '')
				{
					$row_acciones = '<a class="btn btn-xs btn-info push-5" href="javascript:void(0)" title="Notificar Venta" onclick="mostrar_notificar('. "'" . $pedido->tipo . "'" . ", '" . $pedido->id . "'" . ')">Notificar Venta</a>&nbsp;&nbsp;';
				}

				// Si no notificó el cliente ni el local, permite notificar y cancelar
				else
				{
					$row_acciones = '<a class="btn btn-xs btn-info push-5" href="javascript:void(0)" title="Notificar Venta" onclick="mostrar_notificar('. "'" . $pedido->tipo . "'" . ", '" . $pedido->id . "'" . ')">Notificar Venta</a>&nbsp;&nbsp;';

					$row_acciones .= '<a class="btn btn-xs btn-danger push-5" href="javascript:void(0)" title="Cancelar Pedido" onclick="mostrar_cancelar_pedido('."'".$pedido->id."'".')">Cancelar Pedido</a></div>';								
				}				

				$row[] = $row_acciones;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->venta->count_all_pedidos_local(0, $_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->venta->count_filtered_pedidos_local(0, $_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	// Listado de Turnos activos del local (turnos con estado 'reservado')
	// Se llama desde el dashboard del local - Tab 'Turnos'
	public function ajax_turnos_list()
	{
		$data = new stdClass();

		 // Valida que el local tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->turno->get_datatables(0, $_SESSION['frontend_user_id']);
			$datatable = array();
			$no = $_POST['start'];

			foreach ($list as $turno) {
				$no++;
				$row = array();
				$row_acciones = '';

				$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno('."'".$turno->id."'".')">&nbsp;Turno&nbsp;' . $turno->numero . '&nbsp;</a>'; 

				$fecha = new DateTime($turno->fecha);
	            $hora = new DateTime($turno->hora);  

		        // Resalta turnos del dia
		        $hoy = $fecha->format('d/m/Y') == date('d/m/Y') ? "<div class='label label-warning'>HOY</div>": "";

		        $fecha = $fecha->format('d/m/Y') . ' ' . $hora->format('H:i') . ' hs&nbsp;&nbsp;&nbsp;' . $hoy;

		        $row[] = $fecha;	
				$row[] = $turno->servicio . ' - ' . $turno->servicio_item;
				$row[] = "$ " . number_format($turno->total, 2, ',', '.');        
				$row[] = $turno->cliente;	


				// Para turnos con estado 'reservado' pero ya notificados por el local, muestra 'ya notificaste'
				// se da en casos que el local notificó 'no concretado' y el cliente todavía no notifica
				if ($turno->concretado_local != '')
				{
					$row_acciones = '<td class="hidden-xs"><span class="text-success font-w600">Ya notificaste</span></td>';
				}
				else
				{
					$row_acciones = '<a class="btn btn-xs btn-info push-5" href="javascript:void(0)" title="Notificar Turno" onclick="mostrar_notificar('. "'" . 'turno' . "'" . ", '" . $turno->id . "'" . ')">Notificar Turno</a>&nbsp;&nbsp;';		
					
					$row_acciones .= '<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Cancelar Turno" onclick="mostrar_cancelar_turno('."'".$turno->id."'".')">Cancelar Turno</a>';
				}

				$row[] = $row_acciones;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->turno->count_all(0, $_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->turno->count_filtered(0, $_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}			
	}


	// Listado de Ventas del local (turnos o pedidos con estado 'cancelado', 'concretado' o 'no concretado')
	// Se llama desde el dashboard del local - Tab 'Ventas'
	public function ajax_ventas_list()
	{
		$data = new stdClass();

		 // Valida que el local tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->venta->get_datatables_ventas_local(0, $_SESSION['frontend_user_id']);

			$datatable = array();
			$no = $_POST['start'];

			foreach ($list as $pedido) {
				$no++;
				$row = array();

				if ($pedido->tipo == 'venta')
				{
					$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Venta" onclick="detalle_venta('."'".$pedido->id."'".')">&nbsp;&nbsp;Venta ' . $pedido->numero . '&nbsp;</a>'; 						
				}
				else
				{
					$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno('."'".$pedido->id."'".')">&nbsp;Turno&nbsp;&nbsp;' . $pedido->numero . '&nbsp;</a>'; 
				}


				$fecha = new DateTime($pedido->fecha);
				$row[] = $fecha->format('d/m/Y');

				$row[] = $pedido->forma_pago;

				switch ($pedido->estado) {

					case 'concretado':
						$row[] = '<td class="hidden-xs"><span class="text-success font-w600">Concretado</span></td>';
						break;

					case 'cancelado':
						$row[] = '<td class="hidden-xs"><span class="text-danger font-w600">Cancelado</span></td>';
						break;	

					case 'no concretado':
						$row[] = '<td class="hidden-xs"><span class="text-danger font-w600">No Concretado</span></td>';
						break;												
				}		

				$row[] = $pedido->cliente;

				$row[] = "$ " . $pedido->total;


				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->venta->count_all_ventas_local(0, $_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->venta->count_filtered_ventas_local(0, $_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	// Listado de Productos del local (activos o inactivos)
	// Se llama desde el dashboard del local - Tab 'Productos'
	public function ajax_productos_list($estado)
	{
		$data = new stdClass();

		 // Valida que el local tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in']))
		{
			$list = $this->local_articulo->get_datatables_productos($_SESSION['frontend_user_id'], $estado);

			$data = array();
			$no = $_POST['start'];

			foreach ($list as $articulo) 
			{
				$acciones = '';
			
				$no++;
				$row = array();

				$row[] = $articulo->tipo;
				$row[] = $articulo->rubro;
				$row[] = $articulo->animal;			
				$row[] = $articulo->marca;
				$row[] = $articulo->nombre;
				$row[] = $articulo->codigo; 

				// Productos creados por el Local
				if($articulo->tipo == 'Local')
					$row[] = '<img width="100%" src="'. IMG_PATH . 'articulos/locales/miniaturas/'. $articulo->imagen .'" >';
				// Productos Geotienda
				else
					$row[] = '<img width="100%" src="'. IMG_PATH . 'articulos/miniaturas/'. $articulo->imagen .'" >';

				// Ver detalles
				$acciones = '<a class="btn btn-success w-150 push-10" href="javascript:void(0)" title="Ver Detalles" onclick="ver_articulo(' . $articulo->id. ')"><i class="fa fa-eye"></i>&nbsp;&nbsp;Ver Detalles</a>';	

				// Modificar precio y stock
				if ($articulo->estado == 1)
				{				
					$acciones .= '<br/><a class="btn btn-info w-150 push-10" href="javascript:void(0)" title="Modificar precio, descuento y stock" onclick="modificar_presentaciones(' . $articulo->id. ')"><i class="fa fa-sliders"></i>&nbsp;&nbsp;Presentaciones</a>';	
				}


				// Activar / Desactivar
				if ($articulo->estado == 1)
				{
					$acciones .= '<br/><a class="btn btn-warning w-150 push-10" href="javascript:void(0)" title="Desactivar" onclick="switch_producto(' . $articulo->id. ')"><i class="si si-arrow-down"></i>&nbsp;&nbsp;&nbsp;&nbsp;Desactivar</a>';
				}
				else
				{
					$acciones .= '<br/><a class="btn btn-warning w-150 push-10" href="javascript:void(0)" title="Activar" onclick="switch_producto(' . $articulo->id. ')"><i class="si si-arrow-up"></i>&nbsp;&nbsp;&nbsp;&nbsp;Activar</a>';
				}
					
				// Para artículos creados por el local permite editar y eliminar
				if ($articulo->estado == 1)
				{				
					if($articulo->tipo == 'Local')
					{
						$acciones .= '<br/><a class="btn btn-primary w-150 push-10" href="javascript:void(0)" title="Editar" data-toggle="modal" data-target="#modal_producto_local" onclick="modificar_producto_local('."'".$articulo->id."'".')"><i class="glyphicon glyphicon-pencil"></i>&nbsp;&nbsp;&nbsp;Modificar</a>
						  <br/><a class="btn btn-danger w-150" href="javascript:void(0)" title="Eliminar Producto" onclick="eliminar_producto_local('."'".$articulo->nombre."'".',' .$articulo->id. ')"><i class="glyphicon glyphicon-trash"></i>&nbsp;&nbsp;&nbsp;&nbsp;Eliminar</a>';				
					}
					// Para artículos 'GeoTienda' permita quitar el producto del local (no elimina el artículo)
					else
					{
						// Quitar producto del local
						$acciones .= '<br/><a class="btn btn-danger w-150" href="javascript:void(0)" title="Quitar Producto" onclick="quitar_producto_local('."'".$articulo->nombre."'".',' .$articulo->id. ')"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;&nbsp;&nbsp;&nbsp;Quitar</a>';	
					}
				}

				$row[] = $acciones;

				// Columnas auxiliares para filtros por edad, tamaño, raza y medicados
				$row[] = $articulo->id;				
				$row[] = $articulo->id;
				$row[] = $articulo->id;
				$row[] = $articulo->id;

				$data[] = $row;
			}

			$output = array(
							"draw" => $_POST['draw'],
							"recordsTotal" => $this->local_articulo->count_all_productos($_SESSION['frontend_user_id'], $estado),
							"recordsFiltered" => $this->local_articulo->count_filtered_productos($_SESSION['frontend_user_id'], $estado),
							"data" => $data,
					);
			
	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	public function ajax_activar_desactivar_producto($articulo_id)
	{
		$nuevo_estado = 0;

		// Recupera estado actual del producto
		$articulo = $this->local_articulo->get_articulo_local($_SESSION['frontend_user_id'], $articulo_id);

		if($articulo->estado == 0) $nuevo_estado = 1;

		// Recupera filas a actualizar		
		$productos = $this->local_articulo->get_presentaciones_articulo_local($_SESSION['frontend_user_id'], $articulo_id);

		foreach ($productos as $producto) 
		{
            $data_where = array('local_articulo_id' => $producto->id);            
			$data = array('local_articulo_estado' => $nuevo_estado);

			$this->local_articulo->update($data_where, $data);			
		}		

		echo json_encode(array("status" => TRUE, "nuevo_estado" => $nuevo_estado));
	}


	// Detalles de un articulo de un local
	public function get_articulo_by_id($articulo_id, $id)
	{
		$data = $this->local_articulo->get_by_articulo_id($articulo_id);

		$data->presentacion = $this->local_articulo->get_presentaciones_articulo_local($id, $articulo_id);

		echo json_encode($data);	
	}


	// Detalles de un articulo de un local
	public function get_articulo_by_id_presentaciones_activas($articulo_id, $id)
	{
		$data = $this->local_articulo->get_by_articulo_id($articulo_id);

		$data->presentacion = $this->local_articulo->get_presentaciones_activas_articulo_local($id, $articulo_id);

		echo json_encode($data);	
	}


	// Tab 'Notificaciones' del dashboard del Local
	public function ajax_notificaciones_local()
	{
		$list = $this->notificacion_local->get_datatables($_SESSION['frontend_user_id']);

		$data = array();
		$no = $_POST['start'];
		$es_venta = true;

		foreach ($list as $notificacion) {

			// Recupera datos de la venta o turno
			if ($notificacion->turno_id != 0)
			{
				$venta_turno = $this->turno->get_by_id($notificacion->turno_id);
				$es_venta = false;
			}
			else
			{
				$venta_turno = $this->venta->get_venta_by_id($notificacion->venta_id);
				$es_venta = true;
			}

			$row_puntaje = '';
			$no++;
			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;

			// tipo
			switch ($notificacion->tipo) {
				case 'venta':
					$row[] = 'Pedido';
					break;

				case 'turno':
					$row[] = 'Solicitud de turno';	
					break;				

				case 'cancela turno':
					$row[] = 'Turno cancelado';	
					break;	

				case 'cancela venta':
					$row[] = 'Pedido cancelado';	
					break;	
					
				case 'calificación venta':
				case 'calificación turno':
					$row[] = 'Calificación';	
					break;	

				case 'mensaje venta':
				case 'mensaje turno':
					$row[] = 'Mensajes';	
					break;	

				case 'recordatorio turno':
					$row[] = 'Recordatorio turno';	
					break;		

				case 'mascota asignada':
					$row[] = 'Mascota asignada';	
					break;	

				case 'recordatorio vacuna':
					$row[] = 'Recordatorio vacuna';	
					break;															
			}


			$row[] = $notificacion->texto;

			$numero_venta = str_pad($notificacion->venta_id, 5, "P0000", STR_PAD_LEFT);
			$numero_turno = str_pad($notificacion->turno_id, 5, "S0000", STR_PAD_LEFT);

			// ver detalles 
			switch ($notificacion->tipo) {
				case 'venta':	
				case 'cancela venta':
				case 'mensaje venta':
					$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Venta" onclick="detalle_venta(' . "'" . $notificacion->venta_id . "'" . ')">&nbsp;&nbsp;Venta ' . $numero_venta . '&nbsp;</a>';
					break;

				case 'turno':
				case 'cancela turno':
				case 'recordatorio turno':
				case 'mensaje turno':
					$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno('."'".$notificacion->turno_id."'".')">&nbsp;Turno&nbsp;&nbsp;' . $numero_turno . '&nbsp;</a>'; 	
					break;				
					
				case 'calificación venta':
				case 'calificación turno':

					$puntaje = $venta_turno->puntaje;

					if ($es_venta)
					{
						$row_puntaje = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Venta" onclick="detalle_venta(' . "'" . $notificacion->venta_id . "'" . ')">&nbsp;&nbsp;Venta ' . $numero_venta . '&nbsp;</a><br /><br />';
					}
					else
					{
						$row_puntaje = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno('."'".$notificacion->turno_id."'".')">&nbsp;Turno&nbsp;&nbsp;' . $numero_turno . '&nbsp;</a><br /><br />';
					}
					

					for($x=1; $x<=5; $x++)
					{					
					if($x <= $puntaje)
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
					else
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
					}					
			
					$row_puntaje .= '<br /><div  class="push-10-t">"' . $venta_turno->valoracion . '"</div>';
					
					$row[] = $row_puntaje; 

					break;	
					
				case 'recordatorio vacuna':  
					$row[] = '<a class="btn btn-xs btn-vacunas" href="javascript:void(0)" title="ver vacuna" onclick="detalle_vacuna_agendada('."'".$notificacion->vacuna_id."'".')">&nbsp;ver vacuna&nbsp;</a>';
					break;

				default:
					$row[] = '';
					break;
			}

			$data[] = $row;				
			
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->notificacion_local->count_all($_SESSION['frontend_user_id']),
						"recordsFiltered" => $this->notificacion_local->count_filtered($_SESSION['frontend_user_id']),
						"data" => $data,
				);

		echo json_encode($output);
	}


    public function ajax_update()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_update();

		// Validación OK
		if ($error->status)
		{
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			// Actualiza datos del local
			$data_update = array(
				'local_nombre' => trim($this->input->post('local-nombre')),
				'local_nombre_titular' => trim($this->input->post('local-titular-nombre')),
				'local_apellido_titular' => trim($this->input->post('local-titular-apellido')),
				'local_telefono' => trim($this->input->post('local-telefono')),
				'local_telefono2' => trim($this->input->post('local-telefono2')),
				'local_email' => trim($this->input->post('local-email')),
				'local_email2' => trim($this->input->post('local-email2')),
				'local_horario_atencion' => trim($this->input->post('local-horario')),
				'local_horario_sabados' => trim($this->input->post('local-horario-sabados')),
				'local_horario_domingos' => trim($this->input->post('local-horario-domingos')),
				'local_envio_domicilio' => trim($this->input->post('envio-domicilio')),
				'local_costo_envio' => trim($this->input->post('local-envio')),
				'local_zona_entrega' => trim($this->input->post('local-zona-entrega')),
				'local_dias_entrega' => trim($this->input->post('local-dias-entrega')),
				'local_telefono_urgencias' => trim($this->input->post('local-telefono-urgencias')),
				'local_urgencias_consultorio' => trim($this->input->post('local-urgencias-consultorio')),
				'local_urgencias_domicilio' => trim($this->input->post('local-urgencias-domicilio')),
				'local_texto_banner' => trim($this->input->post('local-texto-banner-hidden')),
				'local_color_banner' => trim($this->input->post('local-color-banner')),
				'local_fondo_banner' => trim($this->input->post('local-fondo-banner')),

				// Audit trail
				'local_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
				'local_usuario_modificacion' => trim($this->input->post('local-email'))									
			);

			$this->local->update(array('local_id' => $_SESSION['frontend_user_id']), $data_update);


			// Actualiza contraseña
			if(trim($this->input->post('local-password')) != ''){

				$data_update = array('local_password' => $this->input->post('local-password'));

				$this->local->update_password(array('local_id' => $_SESSION['frontend_user_id']), $data_update);		
			}

			// Medios de Pago del local
	        $this->local_medio_pago->delete_by_local($_SESSION['frontend_user_id']);

			foreach ($_POST as $name => $value) 
			{
				if (substr($name, 0, 12) == 'chkMediopago')
				{
					$medios_pago_array = explode('_', $name);

					$data_insert = array(
						'local_medio_pago_medio_pago_id' => $medios_pago_array[1],
						'local_medio_pago_medio_pago_item_id' => $medios_pago_array[2],
						'local_medio_pago_local_id' => $_SESSION['frontend_user_id']
					);

					$this->local_medio_pago->save($data_insert);							
				}
			}				

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}	


	// Actualiza servicios item asociados al local
	public function ajax_update_servicios()
	{
		$data = array();
		$data['status'] = TRUE;
		$i = 0;

		// Valida datos ingresados
		$error = $this->valida_update_servicios();

		// Validación OK
		if ($error->status){

	        // Elimina servicios item asociados al local
	        $this->local_servicio_item->delete_by_local($_SESSION['frontend_user_id']);

	        // Inserta nuevos servicios item asociados al local
	        $precio = $this->input->post('precio');
	        $duracion = $this->input->post('duracion');

	        if (isset($_POST['servicio']))
	        {	        
				foreach ($this->input->post('servicio') as $servicio_item_id) {

					$data_insert = array(
						'local_servicio_item_local_id' => $_SESSION['frontend_user_id'],
						'local_servicio_item_servicio_item_id' => $servicio_item_id,
						'local_servicio_item_duracion' => $duracion[$i],
						'local_servicio_item_precio' => $precio[$i],
						'local_servicio_item_descuento_id' => 5,  // TODO: implementar descuentos?
						'local_servicio_item_precio_descuento' => $precio[$i],
						'local_servicio_item_status' => 1
					);

					$this->local_servicio_item->save($data_insert);		

					$i++;
				}
			}

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	// Agrega un articulo Geotienda al local (agrega todas las presentaciones, pero inactivas)
	public function ajax_add_articulo_geotienda($articulo_id)
	{
		$data['status'] = TRUE;

		// Recupera presentaciones del articulo
		$presentaciones = $this->articulo->get_articulo_presentaciones($articulo_id);		

		foreach ($presentaciones as $presentacion) 
		{
			$data_insert = array(
				'local_articulo_local_id' => $_SESSION['frontend_user_id'],
				'local_articulo_articulo_presentacion_id' => $presentacion->articulo_presentacion_id,
				'local_articulo_precio' => 0,
				'local_articulo_precio_descuento' => 0,
				'local_articulo_descuento_id' => 5, // TODO: recuperar id sin descuento desde el modelo
				'local_articulo_creador' => 'GeoTienda'
			);

			$this->local_articulo->save($data_insert);		
		}	
		
		echo json_encode($data);	
	}


	// Agrega varios articulos Geotienda al local (agrega todas las presentaciones, pero inactivas)
	public function ajax_add_articulo_geotienda_masivo()
	{
		$data['status'] = TRUE;

		// Recorre articulos seleccionados
		foreach ($this->input->post('articulo') as $articulo_id) 
		{
			// Recupera presentaciones del articulo actual
			$presentaciones = $this->articulo->get_articulo_presentaciones($articulo_id);		

			foreach ($presentaciones as $presentacion) 
			{
				$data_insert = array(
					'local_articulo_local_id' => $_SESSION['frontend_user_id'],
					'local_articulo_articulo_presentacion_id' => $presentacion->articulo_presentacion_id,
					'local_articulo_precio' => 0,
					'local_articulo_precio_descuento' => 0,
					'local_articulo_descuento_id' => 5, // TODO: recuperar id sin descuento desde el modelo
					'local_articulo_creador' => 'GeoTienda'
				);

				$this->local_articulo->save($data_insert);		
			}
		}
		
		echo json_encode($data);	
	}
	

	public function ajax_add_articulo_geotienda_back()
	{
		$data = array();
		$data['status'] = TRUE;
		$i = 0;

		// Valida datos ingresados
		$error = $this->valida_add_articulo_geotienda();

		// Validación OK
		if ($error->status)
		{
	        $precio = $this->input->post('precio');

	        if (isset($_POST['presentacion']))
	        {	        
				foreach ($this->input->post('presentacion') as $presentacion) 
				{
					$data_insert = array(
						'local_articulo_local_id' => $_SESSION['frontend_user_id'],
						'local_articulo_articulo_presentacion_id' => $presentacion,
						'local_articulo_precio' => $precio[$i],
						'local_articulo_precio_descuento' => $precio[$i],
						'local_articulo_descuento_id' => 5, // TODO: recuperar id sin descuento desde el modelo
						'local_articulo_creador' => 'GeoTienda'
					);

					$this->local_articulo->save($data_insert);		

					$i++;
				}
			}

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	// Actualiza presentaciones de un articulo del local 
	public function ajax_save_presentaciones_articulo_local()
	{
		$data = array();
		$local_articulo_ids = array();
		$data['status'] = TRUE;
		$i = 0;
		$desactivar_articulo = TRUE;

		// Valida datos ingresados
		$error = $this->valida_save_presentaciones_aticulo_local();

		// Validación OK
		if ($error->status)
		{
			$this->db->trans_begin();

	        // Elimina presentaciones actuales
			foreach ($this->input->post('local_articulo_id') as $local_articulo_id) 
			{
				$this->local_articulo->delete_by_id($local_articulo_id);	
			}

	        $precio = $this->input->post('precio');
	        $descuento = $this->input->post('descuento');
	        $porcentaje = $this->input->post('porcentaje');
	        $stock = $this->input->post('stock');
	        $estado = $this->input->post('estado');
	        $tipo = $this->input->post('tipo');

	        // Crea las presentaciones actualizadas
	        if (isset($_POST['presentacion']))
	        {	        
				foreach ($this->input->post('presentacion') as $presentacion) 
				{
					$descuento_precio = ($precio[$i] * $porcentaje[$i]) / 100;    

					$data_insert = array(
						'local_articulo_local_id' => $_SESSION['frontend_user_id'],
						'local_articulo_articulo_presentacion_id' => $presentacion,
						'local_articulo_precio' => $precio[$i],
						'local_articulo_precio_descuento' => round($precio[$i] - $descuento_precio), 
						'local_articulo_descuento_id' => $descuento[$i],
						'local_articulo_stock' => $stock[$i],
						'local_articulo_estado_presentacion' => $estado[$i],
						'local_articulo_creador' => $tipo
					);

					$local_articulo_ids[] = $this->local_articulo->save($data_insert);		

					if ($estado[$i] == 1) $desactivar_articulo = FALSE;

					$i++;
				}
			}


			// Si todas las presentaciones estan inactivas, desactiva el producto
			if($desactivar_articulo)
			{
				foreach ($local_articulo_ids as $local_articulo_id) 
				{
		            $data_where = array('local_articulo_id' => $local_articulo_id);            
					$data_update = array('local_articulo_estado' => 0);

					$this->local_articulo->update($data_where, $data_update);			
				}				
			}

			$this->db->trans_commit();
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_quitar_producto_local($articulo_id)
	{
		$data = array();
		$data['status'] = TRUE;

        // Elimina todas las presentaciones del articulo en el local
		$this->local_articulo->delete_by_local_articulo($_SESSION['frontend_user_id'], $articulo_id);	

		echo json_encode($data);			
	}


	public function ajax_eliminar_producto_local($articulo_id)
	{
		$data = array();
		$data['status'] = TRUE;

		$this->db->trans_begin();

        // Elimina todas las presentaciones del articulo en el local
		$this->local_articulo->delete_by_local_articulo($_SESSION['frontend_user_id'], $articulo_id);	

	    // Elimina presentaciones, razas y tamanios asignadas al articulo
	    $this->articulo->delete_articulo_presentacion_by_id($articulo_id, []);	
	    $this->articulo->delete_articulo_raza_by_id($articulo_id);	   
	    $this->articulo->delete_articulo_tamanio_by_id($articulo_id);	      			

		// Elimina el articulo
		$resultado = $this->articulo->delete_by_id($articulo_id);
		$data = $this->validations->valida_db_error($resultado);

		// Si no hubo error, confirma los cambios
		if ($resultado == '')
			$this->db->trans_commit();	
		else
			$this->db->trans_rollback();

		echo json_encode($data);			
	}


	// Notifica a los administradores que el local quiere ser destacado
	public function ajax_aumentar_exposicion($local_id)
	{
		$data = array();
		$data['status'] = TRUE;

		// Fecha y hora local
		$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_local_id' => $local_id,
				'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> ha solicitado ser tienda destacada.<b>',
				'notificacion_tipo' => 'destacar',
			);

		$this->notificacion->save($data_notificacion);			

		echo json_encode($data);			
	}
				

	private function valida_update()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('local-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá un nombre';
			$error->status = FALSE;
		}
	
		if(trim($this->input->post('local-titular-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá nombre del titular';
			$error->status = FALSE;
		}

		if(trim($this->input->post('local-titular-apellido')) == '')
		{
			$error->mensaje[] = 'Ingresá apellido del titular';
			$error->status = FALSE;
		}

		if(trim($this->input->post('local-telefono')) == '')
		{
			$error->mensaje[] = 'Ingresá un teléfono';
			$error->status = FALSE;
		}		

		if(trim($this->input->post('local-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('local-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}	
		}								

		if(trim($this->input->post('local-email2')) != '')
		{
			// Valida formato email alternativo
			if(!filter_var(trim($this->input->post('local-email2')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail alternativo válido';
				$error->status = FALSE;
			}	
		}

		if(trim($this->input->post('local-horario')) == '')
		{
			$error->mensaje[] = 'Ingresá un horario de atención';
			$error->status = FALSE;
		}

		
		// Locales con envio a domicilio
		if($this->input->post('envio-domicilio') == '1' && trim($this->input->post('local-envio')) == '')
		{
			$error->mensaje[] = 'Ingresá un costo de envío';
			$error->status = FALSE;
		}

		if($this->input->post('envio-domicilio') == '1' && trim($this->input->post('local-zona-entrega')) == '')
		{
			$error->mensaje[] = 'Ingresá zona de entrega';
			$error->status = FALSE;
		}

		if($this->input->post('envio-domicilio') == '1' && trim($this->input->post('local-dias-entrega')) == '')
		{
			$error->mensaje[] = 'Ingresá días de entrega';
			$error->status = FALSE;
		}

		// Para locales con servicio de urgecias, valida telefono de urgencias
		if ($this->input->post('local-urgencias') == '1' && trim($this->input->post('local-telefono-urgencias')) == '')
		{
			$error->mensaje[] = 'Ingresá un Teléfono para Urgencias';
			$error->status = FALSE;
		}

		// Valida longitud de la contraseña
		if(trim($this->input->post('local-password')) != '' && strlen(trim($this->input->post('local-password'))) < 6)
		{
			$error->mensaje[] = 'Longitud mínima de la contraseña: 6 caracteres';
			$error->status = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('local-password')) != '' || trim($this->input->post('local-password2')) != '')
		{
			if(trim($this->input->post('local-password')) != trim($this->input->post('local-password2')))
			{
				$error->mensaje[] = 'Las contraseñas no coinciden';
				$error->status = FALSE;
			}
		}	

		return $error;
	} 	


	private function valida_update_servicios()
	{
		$error = new stdClass();
		$i = 0;

		$error->status = TRUE;
		$error->mensaje = array();

        $precio = $this->input->post('precio');

        if (isset($_POST['servicio']))
        {
			foreach ($this->input->post('servicio') as $servicio_item_id) {

				if(trim($precio[$i]) == '')
				{
					$error->mensaje[] = 'Ingresá el precio del servicio.';
					$error->status = FALSE;
					break;
				}	

				// Valida que precio sea un número
				if(!is_numeric($precio[$i]))
				{
					$error->mensaje[] = 'El precio del servicio debe ser un número.';
					$error->status = FALSE;
					break;
				}	

				$i++;
			}
		}

		return $error;
	} 		


	private function valida_add_articulo_geotienda()
	{
		$error = new stdClass();
		$i = 0;

		$error->status = TRUE;
		$error->mensaje = array();

        $precio = $this->input->post('precio');

        if (isset($_POST['presentacion']))
        {
			foreach ($this->input->post('presentacion') as $presentacion) {

				if(trim($precio[$i]) == '')
				{
					$error->mensaje[] = 'Ingresá el precio de las presentaciones seleccionadas.';
					$error->status = FALSE;
					break;
				}	

				// Valida que precio sea un numero entero positivo
				if(!$this->tools->is_natural($precio[$i]))
				{
					$error->mensaje[] = 'El precio de la presentación debe ser un número entero mayor o igual que 0.';
					$error->status = FALSE;
					break;
				}	

				$i++;
			}
		}
		else
		{
			$error->mensaje[] = 'Seleccioná al menos una presentación.';
			$error->status = FALSE;
		}				

		return $error;
	}

	
	private function valida_save_presentaciones_aticulo_local()
	{
		$error = new stdClass();
		$i = 0;

		$error->status = TRUE;
		$error->mensaje = array();

        $precio = $this->input->post('precio');

        if (isset($_POST['presentacion']))
        {
			foreach ($this->input->post('presentacion') as $presentacion) {

				if(trim($precio[$i]) == '')
				{
					$error->mensaje[] = 'Ingresá un precio a la presentación.';
					$error->status = FALSE;
					break;
				}	

				// Valida que precio sea un numero entero positivo
				if(!$this->tools->is_natural($precio[$i]))
				{
					$error->mensaje[] = 'El precio de la presentación debe ser un número entero mayor o igual que 0.';
					$error->status = FALSE;
					break;
				}	

				$i++;
			}
		}			

		return $error;
	}


	private function valida_update_horarios_clinica()
	{
		$error = new stdClass();
		$i = 0;

		$error->status = TRUE;
		$error->mensaje = array();


		// LUNES
		if(trim($this->input->post('abre_lu_clinica')) != '')
		{
			if(trim($this->input->post('lu_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día LUNES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('lu_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día LUNES';
				$error->status = FALSE;
			}
		}


		// MARTES
		if(trim($this->input->post('abre_ma_clinica')) != '')
		{
			if(trim($this->input->post('ma_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día MARTES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('ma_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día MARTES';
				$error->status = FALSE;
			}
		}


		// MIERCOLES
		if(trim($this->input->post('abre_mi_clinica')) != '')
		{
			if(trim($this->input->post('mi_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día MIERCOLES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('mi_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día MIERCOLES';
				$error->status = FALSE;
			}
		}


		// JUEVES
		if(trim($this->input->post('abre_ju_clinica')) != '')
		{
			if(trim($this->input->post('ju_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día JUEVES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('ju_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día JUEVES';
				$error->status = FALSE;
			}
		}


		// VIERNES
		if(trim($this->input->post('abre_vi_clinica')) != '')
		{
			if(trim($this->input->post('vi_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día VIERNES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('vi_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día VIERNES';
				$error->status = FALSE;
			}
		}



        // SABADO
		if(trim($this->input->post('abre_sa_clinica')) != '')
		{
			if(trim($this->input->post('sa_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día SABADO';
				$error->status = FALSE;
			}

			if(trim($this->input->post('sa_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día SABADO';
				$error->status = FALSE;
			}
		}



        // DOMINGO
		if(trim($this->input->post('abre_do_clinica')) != '')
		{
			if(trim($this->input->post('do_h_1_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día DOMINGO';
				$error->status = FALSE;
			}

			if(trim($this->input->post('do_h_2_clinica')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día DOMINGO';
				$error->status = FALSE;
			}
		}


		return $error;
	} 		


	private function valida_update_horarios_peluqueria()
	{
		$error = new stdClass();
		$i = 0;

		$error->status = TRUE;
		$error->mensaje = array();


		// LUNES
		if(trim($this->input->post('abre_lu_peluqueria')) != '')
		{
			if(trim($this->input->post('lu_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día LUNES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('lu_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día LUNES';
				$error->status = FALSE;
			}
		}


		// MARTES
		if(trim($this->input->post('abre_ma_peluqueria')) != '')
		{
			if(trim($this->input->post('ma_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día MARTES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('ma_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día MARTES';
				$error->status = FALSE;
			}
		}


		// MIERCOLES
		if(trim($this->input->post('abre_mi_peluqueria')) != '')
		{
			if(trim($this->input->post('mi_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día MIERCOLES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('mi_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día MIERCOLES';
				$error->status = FALSE;
			}
		}


		// JUEVES
		if(trim($this->input->post('abre_ju_peluqueria')) != '')
		{
			if(trim($this->input->post('ju_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día JUEVES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('ju_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día JUEVES';
				$error->status = FALSE;
			}
		}


		// VIERNES
		if(trim($this->input->post('abre_vi_peluqueria')) != '')
		{
			if(trim($this->input->post('vi_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día VIERNES';
				$error->status = FALSE;
			}

			if(trim($this->input->post('vi_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día VIERNES';
				$error->status = FALSE;
			}
		}

        // SABADO
		if(trim($this->input->post('abre_sa_peluqueria')) != '')
		{
			if(trim($this->input->post('sa_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día SABADO';
				$error->status = FALSE;
			}

			if(trim($this->input->post('sa_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día SABADO';
				$error->status = FALSE;
			}
		}


        // DOMINGO
		if(trim($this->input->post('abre_do_peluqueria')) != '')
		{
			if(trim($this->input->post('do_h_1_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario DESDE para día DOMINGO';
				$error->status = FALSE;
			}

			if(trim($this->input->post('do_h_2_peluqueria')) == '')
			{
				$error->mensaje[] = 'Ingresá un horario HASTA para día DOMINGO';
				$error->status = FALSE;
			}
		}


		return $error;
	}	


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}		
}