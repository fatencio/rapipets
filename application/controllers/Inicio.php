<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		
		$this->load->model('Usuarios/frontend_model','frontend_user');
		$this->load->model('Usuarios/usuario_model','admin');
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('ABM/animal_model','animal');
		$this->load->model('ABM/rubro_model','rubro');		
		$this->load->model('ABM/marca_model','marca');			
		$this->load->model('ABM/tamanio_model','tamanio');
		$this->load->model('ABM/raza_model','raza');		
		$this->load->model('ABM/descuento_model','descuento');		
		$this->load->model('ABM/presentacion_model','presentacion');		
		$this->load->model('ABM/servicio_model','servicio');		
		$this->load->model('Tools/configuracion_model','configuracion');
		$this->load->model('Usuarios/localhorario_model','local_horario');		
		$this->load->helper('url');			
		$this->load->helper('form');
		$this->load->helper('cookie');
		$this->load->library('form_validation');	
		$this->load->library('email_geotienda');	
		$this->load->library('tools');		
	}


	public function index()
	{
		$data = array();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}

		// Valida si el usuario tiene la opción 'recordar' activada.
		if($this->input->cookie('id_extreme') != '')
		{
			$cookie = htmlentities($this->input->cookie('id_extreme'));
			
			$cookie = explode("$", $cookie);
			$email = $cookie[0];
			$cookie = $cookie[1];


				if ($this->frontend_user->resolve_user_cookie($email, $cookie)) 
				{
					$user = $this->frontend_user->get_user_from_email($email);

					// Valida que el usuario esté habilitado
					if ($this->frontend_user->resolve_user_status($email)) {

						// inicializa sesion del usuario
						$_SESSION['frontend_logged_in'] = (bool)true;	
						$_SESSION['frontend_user_id'] = $user->id;			
						$_SESSION['frontend_email'] = $user->email;
						$_SESSION['frontend_nombre'] = $user->nombre;
						$_SESSION['frontend_apellido'] = $user->apellido;
						$_SESSION['frontend_avatar'] = $user->avatar;
						$_SESSION['frontend_tipo'] = $user->tipo;  // Local o Cliente
						$_SESSION['frontend_admin'] = (bool)$user->admin;  // 1 = admin / 0 = cliente normal
					}
		   		}
		}

		// Recupera imagen de portada
		$data['portada'] = $this->configuracion->get_value('portada_landing');

		$data['portada_posicion'] = $this->configuracion->get_value2('portada_landing');
		$data['portada_altura'] = $this->config->item('altura_portada_landing');
		$data['portada_altura_mobile'] = $this->config->item('altura_portada_landing_mobile');
		$data['portada_color'] = $this->config->item('color_portada_landing');	
		$data['portada_top'] = $this->config->item('texto_portada_top_landing');	
		$data['portada_top_mobile'] = $this->config->item('texto_portada_top_landing_mobile');	
		$data['transparencia_portada'] = $this->config->item('transparencia_portada_landing');
		$data['rutaImagen'] = 'false';	

		// Listado de localidades
		$data['localidades'] = $this->local->get_all_localidades();

		$this->load->template('landing', $data);
	}


	public function dashboard($tab = '')
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		// Valida que el usuario tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in']))
		{
			$data->tab = $tab ;
			$data->venta_id = $this->input->get('vid');

			// Valida tipo de usuario (Local o Cliente)
			if ($_SESSION['frontend_tipo'] == 'local')
			{
				// Fechas y clientes de los pedidos pendientes del local (tab Pedidos)
				$data->fechas_pendientes = $this->venta->get_fechas_pedidos_local($_SESSION['frontend_user_id']);
				$data->clientes_pendientes = $this->venta->get_clientes_pedidos_local($_SESSION['frontend_user_id']);

				// Fechas y clientes del historial de ventas del local (tab Ventas)
				$data->fechas = $this->venta->get_fechas_local($_SESSION['frontend_user_id']);
				$data->clientes = $this->venta->get_clientes_ventas_local($_SESSION['frontend_user_id']);

				// Filtros para tabla de productos activos del local
				$data->rubros_activos = $this->rubro->get_by_local($_SESSION['frontend_user_id'], 1, -1);
				$data->animales_activos = $this->animal->get_by_local($_SESSION['frontend_user_id'], 1, -1);
				$data->marcas_activos = $this->marca->get_by_local_animal_rubro($_SESSION['frontend_user_id'], 1, -1);

				// Filtros para tabla de productos inactivos del local
				$data->rubros_inactivos = $this->rubro->get_by_local($_SESSION['frontend_user_id'], 0, -1);
				$data->animales_inactivos = $this->animal->get_by_local($_SESSION['frontend_user_id'], 0, -1);
				$data->marcas_inactivos = $this->marca->get_by_local_animal_rubro($_SESSION['frontend_user_id'], 0, -1);

				// Listados para ABM de productos
				$data->rubros = $this->rubro->get_all();				
				$data->animales = $this->animal->get_all();
				$data->marcas = $this->marca->get_all_by_animal_rubro_by_id();	// Modal nuevo / editar Producto
				$data->marcas_select = $this->marca->get_all_by_animal_rubro();	// Lista de productos GeoTienda			
				$data->razas = $this->raza->get_all_by_animal();
				$data->tamanios = $this->tamanio->get_all_by_animal();
				$data->presentaciones = $this->presentacion->get_all();		

				// Cantidad máxima de palabras que puede contener la descripción del producto
				$data->palabras_descripcion_producto = $this->config->item('palabras_descripcion_producto');		

				// Datos para ABM de articulos del local
				$data->descuentos = $this->descuento->get_all();

                // Recupera imagen de portada
                $data->portada = $this->configuracion->get_value('portada_dashboard');		
				$data->portada_posicion = $this->configuracion->get_value2('portada_dashboard');
				$data->portada_altura = $this->config->item('altura_portada_dashboard_local');
				$data->portada_altura_mobile = $this->config->item('altura_portada_dashboard_local_mobile');
                $data->transparencia_portada = $this->config->item('transparencia_portada_dashboard_local');

				$data->rutaImagen = 'true';	           

				// Local destacado
				$data->destacado = $this->local->get_destacado($_SESSION['frontend_user_id']);


				$this->load->template('local_dashboard', $data);
			}
			else
			{
				$data->animales = $this->animal->get_all();
				$data->locales = $this->local->get_all();
				
                // Recupera imagen de portada
                $data->portada = $this->configuracion->get_value('portada_dashboard');		
				$data->portada_posicion = $this->configuracion->get_value2('portada_dashboard');
				$data->portada_altura = $this->config->item('altura_portada_dashboard_cliente');
				$data->portada_altura_mobile = $this->config->item('altura_portada_dashboard_cliente_mobile');
				$data->portada_top = $this->config->item('texto_portada_top_dashboard_cliente');	
				$data->portada_top_mobile = $this->config->item('texto_portada_top_dashboard_cliente_mobile');	
				$data->transparencia_portada = $this->config->item('transparencia_portada_dashboard_cliente');

				$data->rutaImagen = 'true';	   

				// Listado de localidades
				$data->localidades = $this->local->get_all_localidades();					             			
				
				$this->load->template('cliente_dashboard', $data);
			}
		}
		else
		{
			$data->login = true;
			$data->tab = $tab;

			// Recupera imagen de portada
			$data->portada = $this->configuracion->get_value('portada_landing');

			$data->portada_posicion = $this->configuracion->get_value2('portada_landing');
			$data->portada_altura = $this->config->item('altura_portada_landing');
			$data->portada_altura_mobile = $this->config->item('altura_portada_landing_mobile');
			$data->portada_color = $this->config->item('color_portada_landing');	
			$data->portada_top = $this->config->item('texto_portada_top_landing');	
			$data->portada_top_mobile = $this->config->item('texto_portada_top_landing_mobile');	
			$data->transparencia_portada = $this->config->item('transparencia_portada_landing');
			$data->rutaImagen = 'false';	

			$this->load->template('landing', $data);
		}
	}


	function login(){

		$data = array();
		$data['status'] = TRUE;

		$email = $this->input->post('frontend-login-email');
		$password = $this->input->post('frontend-login-password');

		// No ingresó e-mail o password
		if($email == "" || $password == ""){

			$data['status'] = FALSE;
		}
		else{

			// Valida login	
			if ($this->frontend_user->resolve_user_login($email, $password)) {

				$user = $this->frontend_user->get_user_from_email($email);

				// Valida que el usuario esté habilitado
				if ($this->frontend_user->resolve_user_status($email)) {
				
					// inicializa sesion del usuario
					$_SESSION['frontend_logged_in'] = (bool)true;	
					$_SESSION['frontend_user_id'] = $user->id;			
					$_SESSION['frontend_email'] = $user->email;
					$_SESSION['frontend_nombre'] = $user->nombre;
					$_SESSION['frontend_apellido'] = $user->apellido;
					$_SESSION['frontend_avatar'] = $user->avatar;
					$_SESSION['frontend_tipo'] = $user->tipo;  // Local o Cliente
					$_SESSION['frontend_admin'] = 0;  // 1 = admin / 0 = cliente normal

					$data['user_id'] = $user->id;
					$data['username'] = $user->nombre;
					$data['avatar'] = $user->avatar;
					$data['tipo'] = $user->tipo;
					$data['frontend_admin'] = 0;

					// Recordar login
					if($this->input->post('recordar')){

						$ip = urldecode($this->input->ip_address());

						$id_extreme = md5(uniqid(rand(), true));
						$id_extreme2 = $user->email."$".$id_extreme."$".$ip;

						$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

						setcookie('id_extreme', $id_extreme2, time()+7776000, $domain); //, '/', '', false, false);

						// Cliente
						if ($user->tipo == 'cliente')
						{
							$data_update = array('cliente_cookie' => $id_extreme);
							$this->cliente->update(array('cliente_id' => $user->id), $data_update);
						}
						// Local
						else
						{
							$data_update = array('local_cookie' => $id_extreme);
							$this->local->update(array('local_id' => $user->id), $data_update);
						}	
					}			
				}
				else
				{
					$data['status'] = FALSE;
					$data['error'] = 'bloqueado';
				}

			}
			else
			{
				// Valida si es un administrador (tabla 'user')
		
				// login ok				
				if ($this->admin->resolve_user_login($email, $password)) {
					
					$user_id = $this->admin->get_user_id_from_username($email);
					$user    = $this->admin->get_user($user_id);
					
					// inicializa sesion del usuario administrador
					$_SESSION['frontend_logged_in'] = (bool)true;	
					$_SESSION['frontend_user_id'] = $user->id;			
					$_SESSION['frontend_email'] = $user->email;
					$_SESSION['frontend_nombre'] = $user->nombre;
					$_SESSION['frontend_apellido'] = $user->apellido;
					$_SESSION['frontend_avatar'] = $user->avatar;
					$_SESSION['frontend_tipo'] = 'cliente';  // Local o Cliente
					$_SESSION['frontend_admin'] = 1;  // 1 = admin / 0 = cliente normal

					$data['user_id'] = $user->id;
					$data['username'] = $user->nombre;
					$data['avatar'] = $user->avatar;
					$data['tipo'] = 'cliente';
					$data['frontend_admin'] = 1;
				}
				else
				{
					$data['status'] = FALSE;					
				}
			}				
		}

		echo json_encode($data);		
	}


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}	


	public function cliente_registrar()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_registro();

		// Validación OK
		if ($error->status)
		{
			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));			

			$cliente = array(
					'cliente_nombre' => $this->input->post('frontend-register-nombre'),
					'cliente_apellido' => $this->input->post('frontend-register-apellido'),
					'cliente_email' => $this->input->post('frontend-register-email'),
					'cliente_password' => $this->input->post('frontend-register-password'),

					// Un cliente nuevo está deshabilitado hasta que confirme por mail
					'cliente_status' => 0,

					// Audit trail
					'cliente_fecha_alta' => $fecha->format('Y-m-d H:i:s'),
					'cliente_usuario_alta' => $this->input->post('frontend-register-email')					
				);

			$this->db->trans_begin();

			// Agrega el cliente a la base de datos
			$id = $this->cliente->save($cliente);

			// Notificacion
			$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_cliente_id' => $id,
				'notificacion_texto' => '<b>' . $this->input->post('frontend-register-nombre') . ' ' . $this->input->post('frontend-register-apellido') . '</b> se registró en GeoTienda',
				'notificacion_tipo' => 'registro',
			);

			$this->notificacion->save($data_notificacion);		

			// Valida error en el registro
			if ($this->db->trans_status() === FALSE)
			{
				$this->db->trans_rollback();

				$data['status'] = FALSE;
				$data['error'] = 'transaction';					
			}
			else
			{
				// Genera link para activar cuenta creada
				$linkActivacion = $this->generar_link_activacion($id, $this->input->post('frontend-register-nombre') . ' ' . $this->input->post('frontend-register-apellido'));

				// Envía mail de bienvenida con el link de activacion
				$msg_mail = $this->email_geotienda->nuevo_cliente(
					$this->input->post('frontend-register-email'), 					   
					$this->input->post('frontend-register-password'),
					$linkActivacion);

				if ($msg_mail == '')							
				{
					$this->db->trans_commit();
				}
				else
				{
					$data['status'] = FALSE;
					$data['error'] = 'mail';			
					$this->db->trans_rollback();
				}				
			}
		} 

		// Error en los datos ingresados
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


    public function local_pedido_registro()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_consulta_negocio();

		// Validación OK
		if ($error->status)
		{
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Envía mail de pedido de registro
			$msg_mail = $this->email_geotienda->pedido_registro(
				$this->input->post('consulta-negocio-nombre'),
				$this->input->post('consulta-negocio-email'),
				$this->input->post('consulta-negocio-telefono'),
				$this->input->post('consulta-negocio-mensaje'));

			if ($msg_mail != '')							
			{
				$data['status'] = FALSE;
				$data['error'] = 'mail';	
			}
			else
			{
				// Notificacion
				$data_notificacion = array(
					'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_texto' => '<b>' . $this->input->post('consulta-negocio-nombre') . ' - ' . $this->input->post('consulta-negocio-email') . '</b> solicitó sumarse a GeoTienda',
					'notificacion_tipo' => 'adhesión',
				);

				$this->notificacion->save($data_notificacion);					
			}
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}		


	public function consulta()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_consulta();

		// Validación OK
		if ($error->status)
		{
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Envía mail de pedido de registro
			$msg_mail = $this->email_geotienda->consulta(
				$this->input->post('consulta-motivo'),
				$this->input->post('consulta-nombre'),
				$this->input->post('consulta-email'),
				$this->input->post('consulta-telefono'),
				$this->input->post('consulta-mensaje'));

			if ($msg_mail != '')							
			{
				$data['status'] = FALSE;
				$data['error'] = 'mail';	
			}
			else
			{
				// Notificacion
				$data_notificacion = array(
					'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_texto' => '<b>' . $this->input->post('consulta-nombre') . ' - ' . $this->input->post('consulta-email') . '</b> envió una consulta',
					'notificacion_tipo' => 'consulta',
				);

				$this->notificacion->save($data_notificacion);					
			}
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}	

    private function valida_consulta_negocio()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('consulta-negocio-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá tu nombre';
			$error->status = FALSE;
		}
		
		if(trim($this->input->post('consulta-negocio-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('consulta-negocio-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}
			else{
				// Valida que no exista un Cliente o Local con el mismo email
				$duplicated = $this->frontend_user->check_duplicated(trim($this->input->post('consulta-negocio-email')));

				if ($duplicated > 0)
				{
					$error->mensaje[] = 'Ya existe un usuario con ese e-mail';
					$error->status = FALSE;
				}					
			}		
		}								

		 if(trim($this->input->post('consulta-negocio-telefono')) == '')
		{
			$error->mensaje[] = 'Ingresá un teléfono';
			$error->status = FALSE;
		}
	
		// En desarrollo (localhost) no valida CAPTCHA
		if ($this->config->item('ambiente') != 'desarrollo')
		{
			if($this->input->post('consulta-negocio-recaptcha') == '')
			{
				$error->mensaje[] = 'Falta resolver el captcha';
				$error->status = FALSE;
			}				
		}


		return $error;
	} 	


	private function valida_registro()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();

		if(trim($this->input->post('frontend-register-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá tu nombre';
			$error->status = FALSE;
		}

	    if(trim($this->input->post('frontend-register-apellido')) == '')
		{
			$error->mensaje[] = 'Ingresá tu apellido';
			$error->status = FALSE;
		}
		
		if(trim($this->input->post('frontend-register-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('frontend-register-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}
			else{
				// Valida que no exista un Cliente o Local con el mismo email
				$duplicated = $this->frontend_user->check_duplicated(trim($this->input->post('frontend-register-email')));

				if ($duplicated > 0)
				{
					$error->mensaje[] = 'Ya existe un usuario con ese e-mail';
					$error->status = FALSE;
				}					
			}		
		}								

		if(trim($this->input->post('frontend-register-password')) == '')
		{
			$error->mensaje[] = 'Tu contraseña tiene menos de 5 caracteres';
			$error->status = FALSE;
		}	
		else{
			// Valida longitud de la contraseña
			if(trim($this->input->post('frontend-register-password')) != '' && strlen(trim($this->input->post('frontend-register-password'))) < 6)
			{
				$error->mensaje[] = 'Tu contraseña tiene menos de 5 caracteres';
				$error->status = FALSE;
			}			
		}
	
		if(!isset($_POST['frontend-register-terms']))
		{
			$error->mensaje[] = 'Debes aceptar los Términos y condiciones';
			$error->status = FALSE;
		}

		// En desarrollo (localhost) no valida CAPTCHA
		if ($this->config->item('ambiente') != 'desarrollo')
		{
		    if($_POST['g-recaptcha-response'] == '')
			{
				$error->mensaje[] = 'Falta resolver el captcha';
				$error->status = FALSE;
			}
		}

		return $error;
	} 	


	private function valida_consulta()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('consulta-motivo')) == '')
		{
			$error->mensaje[] = 'Ingresá el motivo de tu consulta';
			$error->status = FALSE;
		}

		if(trim($this->input->post('consulta-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá tu nombre';
			$error->status = FALSE;
		}
		
		if(trim($this->input->post('consulta-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('consulta-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}	
		}								

		if(trim($this->input->post('consulta-mensaje')) == '')
		{
			$error->mensaje[] = 'Ingresá un mensaje';
			$error->status = FALSE;
		}
	
		// En desarrollo (localhost) no valida CAPTCHA
		if ($this->config->item('ambiente') != 'desarrollo')
		{
		    if($_POST['g-recaptcha-response'] == '')
			{
				$error->mensaje[] = 'Falta resolver el captcha';
				$error->status = FALSE;
			}
		}
		
		return $error;
	} 	


    public function solicita_reset()
	{
		$data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_reset();

		// Validación OK
		if ($error->status){

			$user = $this->frontend_user->get_user_from_email($this->input->post('reset-email'));

			// Valida que exista un usuario con el mail ingresado
			if(isset($user->id)) {

		      $linkTemporal = $this->generar_link_temporal($user->id, $user->nombre . ' ' .  $user->apellido, $user->tipo);

		      if($linkTemporal){

				// Envía mail de reset
				$msg_mail = $this->email_geotienda->reset(
					$this->input->post('reset-email'),
					$linkTemporal);

				if ($msg_mail != '')							
				{
					$data['status'] = FALSE;
					$data['error'] = 'mail';	
				}

				// TODO: enviar notificacion?	
		        
		      }
		   }
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	private function valida_reset()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();
		
		if(trim($this->input->post('reset-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('reset-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}	
		}								

		return $error;
	}

	public function restablecer(){

		$idusuario = $this->input->get('idusuario');
		$token = $this->input->get('token');

		$datos_vista = array();

		$reset_data = $this->frontend_user->get_reset_data($token);
		
		// Valida que existan datos de reset
		if(isset($reset_data->reset_id)) {

		   if( sha1($reset_data->reset_usuario_id) == $idusuario ){

		   		$datos_vista["idreset"] = $reset_data->reset_id;
		   		$datos_vista["idusuario"] = $reset_data->reset_usuario_id;
		   		$datos_vista["tipo"] = $reset_data->reset_usuario_tipo;  // cliente o local

				// Recupera imagen de portada
				$datos_vista["portada"] = $this->configuracion->get_value('portada_restablecer');
				$datos_vista["portada_posicion"] = $this->configuracion->get_value2('portada_restablecer');
				$datos_vista["portada_altura"] = $this->config->item('altura_portada_restablecer');
				$datos_vista["portada_altura_mobile"] = $this->config->item('altura_portada_restablecer_mobile');
				$datos_vista["portada_color"] = $this->config->item('color_portada_restablecer');	
                $datos_vista["portada_transparencia"] = $this->config->item('transparencia_portada_restablecer');	

				$datos_vista['rutaImagen'] = 'false';	

				$this->load->template('restablecer', $datos_vista);
		   }
		   else{
		   		$datos_vista["show_error"] = "restablecer";

				// Recupera imagen de portada
				$data['portada'] = $this->configuracion->get_value('portada_landing');

				$datos_vista['portada_posicion'] = $this->configuracion->get_value2('portada_landing');
				$datos_vista['portada_altura'] = $this->config->item('altura_portada_landing');
				$datos_vista['portada_altura_mobile'] = $this->config->item('altura_portada_landing_mobile');
				$datos_vista['portada_color'] = $this->config->item('color_portada_landing');	
				$datos_vista['portada_top'] = $this->config->item('texto_portada_top_landing');
				$datos_vista['portada_top_mobile'] = $this->config->item('texto_portada_top_landing_mobile');	
				$datos_vista['transparencia_portada'] = $this->config->item('transparencia_portada_landing');

				$datos_vista['rutaImagen'] = 'false';			   		

		   		$this->load->template('landing', $datos_vista);
		   }
		}
		else
		{
			$datos_vista["show_error"] = "restablecer";

			// Recupera imagen de portada
			$datos_vista['portada'] = $this->configuracion->get_value('portada_landing');

			$datos_vista['portada_posicion'] = $this->configuracion->get_value2('portada_landing');
			$datos_vista['portada_altura'] = $this->config->item('altura_portada_landing');
			$datos_vista['portada_altura_mobile'] = $this->config->item('altura_portada_landing_mobile');
			$datos_vista['portada_color'] = $this->config->item('color_portada_landing');	
			$datos_vista['portada_top'] = $this->config->item('texto_portada_top_landing');	
			$datos_vista['portada_top_mobile'] = $this->config->item('texto_portada_top_landing_mobile');	
			$datos_vista['transparencia_portada'] = $this->config->item('transparencia_portada_landing');

			$datos_vista['rutaImagen'] = 'false';	

			$this->load->template('landing', $datos_vista);
		}
	}	


	private function generar_link_temporal($idusuario, $username, $tipo){

	   // Se genera una cadena para validar el cambio de contraseña
	   $cadena = $idusuario.$username.rand(1,9999999).date('Y-m-d');
	   $token = sha1($cadena);

	   // Si existe, elimina registro para el mismo usuario
	   $this->frontend_user->delete_reset_by_usuario_id($idusuario);

	   // Se inserta el registro en la tabla reset
		$data = array(
				'reset_usuario_id' => $idusuario,
				'reset_usuario_nombre' => $username,
				'reset_usuario_tipo' => $tipo,
				'reset_token' => $token
			);

		$insert = $this->frontend_user->save_reset($data);

	   if($insert > 0){
	      // Se devuelve el link que se enviara al usuario
	      $enlace = BASE_PATH . '/restablecer?idusuario='.sha1($idusuario).'&token='.$token;
	      return $enlace;
	   }
	   else
	      return FALSE;
	}


	private function generar_link_activacion($idusuario, $username)
	{
	    // Se genera una cadena para validar el pedido de activacion
	    $cadena = $idusuario.$username.rand(1,9999999).date('Y-m-d');
	    $token = sha1($cadena);

	    // Se asigna el token al nuevo cliente
		$data_update = array('cliente_activacion' => $token);
		$update = $this->cliente->update(array('cliente_id' => $idusuario), $data_update);				   

	   if($update > 0)
	   {
	      // Se devuelve el link que se enviara al usuario
	      $enlace = BASE_PATH . '/activar?idusuario='.sha1($idusuario).'&token='.$token;
	      return $enlace;
	   }
	   else
	      return FALSE;
	}


	public function activar()
	{
		$idusuario = $this->input->get('idusuario');
		$token = $this->input->get('token');

		$datos_vista = array();

		$activacion_data = $this->frontend_user->get_activacion_data($token);
		
		// Valida que existan datos de activacion
		if(isset($activacion_data->cliente_id)) 
		{			
		   if(sha1($activacion_data->cliente_id) == $idusuario)
		   {
				// Fecha y hora local
				$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));			

		   		// Activa al cliente
				$data_update = array(
					'cliente_activacion' => '',
					'cliente_status' => 1,
					'cliente_fecha_activacion' => $fecha->format('Y-m-d H:i:s'));

				$update = $this->cliente->update(array('cliente_id' => $activacion_data->cliente_id), $data_update);	   

				if($update > 0)
	   			{
	   				// Activacion OK
	   				$datos_vista["show_modal"] = "activar";
	   			}
	   			else
	   			{
		   			$datos_vista["show_error"] = "activar";		   	   				
	   			}
		   }
		   else
		   {
		   		$datos_vista["show_error"] = "activar";
		   }
		}
	   else
	   {
	   		$datos_vista["show_error"] = "activar";
	   }

		// Recupera imagen de portada
		$datos_vista['portada'] = $this->configuracion->get_value('portada_landing');

		$datos_vista['portada_posicion'] = $this->configuracion->get_value2('portada_landing');
		$datos_vista['portada_altura'] = $this->config->item('altura_portada_landing');
		$datos_vista['portada_altura_mobile'] = $this->config->item('altura_portada_landing_mobile');
		$datos_vista['portada_color'] = $this->config->item('color_portada_landing');	
		$datos_vista['portada_top'] = $this->config->item('texto_portada_top_landing');	
		$datos_vista['portada_top_mobile'] = $this->config->item('texto_portada_top_landing_mobile');	
		$datos_vista['transparencia_portada'] = $this->config->item('transparencia_portada_landing');

		$datos_vista['rutaImagen'] = 'false';	

		$this->load->template('landing', $datos_vista);
	}	


	public function reset_password()
	{
	 
	    $data = array();
		$data['status'] = TRUE;
		
		// Valida datos ingresados
		$error = $this->valida_reset_password();

		// Validación OK
		if ($error->status){

			// Actualiza password del usuario (local o cliente)
			if($this->input->post('tipo') == 'cliente'){

				$data_update = array('cliente_password' => $this->input->post('password'));
				$this->cliente->update_password(array('cliente_id' => $this->input->post('idusuario')), $data_update);				
			}
			else{

				$data_update = array('local_password' => $this->input->post('password'));
				$this->local->update_password(array('local_id' => $this->input->post('idusuario')), $data_update);	
			}

			// Elimina los datos de reset
			$this->frontend_user->delete_reset_by_id($this->input->post('idreset'));
		} 

		// Error en los datos ingresados
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);
	}


	private function valida_reset_password()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();
		
		if(trim($this->input->post('password')) == '')
		{
			$error->mensaje[] = 'Ingresá una contraseña';
			$error->status = FALSE;
		}	

		if(trim($this->input->post('password2')) == '')
		{
			$error->mensaje[] = 'Ingresá nuevamente la contraseña';
			$error->status = FALSE;
		}	

		// Valida longitud de la contraseña
		if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
		{
			$error->mensaje[] = 'Longitud mínima de la contraseña: 6 caracteres';
			$error->status = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
		{
			if(trim($this->input->post('password')) != trim($this->input->post('password2')))
			{
				$error->mensaje[] = 'Las contraseñas no coinciden';
				$error->status = FALSE;
			}
		}								

		return $error;
	}


	public function beneficios()
	{
		 $data = new stdClass();
      
		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_beneficios');
        $data->portada_posicion = $this->configuracion->get_value2('portada_beneficios');
		$data->portada_altura = $this->config->item('altura_portada_beneficios');	
		$data->portada_altura_mobile = $this->config->item('altura_portada_beneficios_mobile');	
		$data->portada_color = $this->config->item('color_portada_beneficios');	
		$data->portada_top = $this->config->item('texto_portada_top_beneficios');
		$data->portada_top_mobile = $this->config->item('texto_portada_top_beneficios_mobile');
		$data->transparencia_portada = $this->config->item('transparencia_portada_beneficios');	
			
		$data->rutaImagen = 'false';	

		$this->load->template('beneficios', $data);		
	}


	public function terminos()
	{	
		 $data = new stdClass();
      
		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_terminos');
		$data->portada_posicion = $this->configuracion->get_value2('portada_terminos');
        $data->portada_altura = $this->config->item('altura_portada_terminos'); 
        $data->portada_altura_mobile = $this->config->item('altura_portada_terminos_mobile'); 
		$data->portada_color = $this->config->item('color_portada_terminos');	
		$data->portada_top = $this->config->item('texto_portada_top_terminos');
		$data->portada_top_mobile = $this->config->item('texto_portada_top_terminos_mobile');
		$data->transparencia_portada = $this->config->item('transparencia_portada_terminos');		
		$data->rutaImagen = 'false';			

        $this->load->template('terminos', $data);		
	}


	public function politicas(){
		
		 $data = new stdClass();
      
		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_politicas');
		$data->portada_posicion = $this->configuracion->get_value2('portada_politicas');
        $data->portada_altura = $this->config->item('altura_portada_politicas'); 
        $data->portada_altura_mobile = $this->config->item('altura_portada_politicas_mobile'); 
		$data->portada_color = $this->config->item('color_portada_politicas');   
		$data->portada_top = $this->config->item('texto_portada_top_politicas');	
		$data->portada_top_mobile = $this->config->item('texto_portada_top_politicas_mobile');	
		$data->transparencia_portada = $this->config->item('transparencia_portada_politicas');	
		$data->rutaImagen = 'false';	

        $this->load->template('politicas', $data);
	}	


	public function faq()
	{
        $data = new stdClass();
      
        // Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_faq');
		$data->portada_posicion = $this->configuracion->get_value2('portada_faq');
        $data->portada_altura = $this->config->item('altura_portada_faq'); 
        $data->portada_altura_mobile = $this->config->item('altura_portada_faq_mobile'); 
		$data->portada_color = $this->config->item('color_portada_faq');	
		$data->portada_top = $this->config->item('texto_portada_top_faq');
		$data->portada_top_mobile = $this->config->item('texto_portada_top_faq_mobile');
		$data->transparencia_portada = $this->config->item('transparencia_portada_faq');		
		$data->rutaImagen = 'false';			        

		$this->load->template('faq',  $data);
	}	


	public function mapa()
	{
        $data = new stdClass();
      
        // Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_mapa');
		$data->portada_posicion = $this->configuracion->get_value2('portada_mapa');
        $data->portada_altura = $this->config->item('altura_portada_mapa'); 
        $data->portada_altura_mobile = $this->config->item('altura_portada_mapa_mobile'); 
		$data->portada_color = $this->config->item('color_portada_mapa');
		$data->portada_top = $this->config->item('texto_portada_top_mapa');	
		$data->portada_top_mobile = $this->config->item('texto_portada_top_mapa_mobile');	
		$data->transparencia_portada = $this->config->item('transparencia_portada_mapa');	
		$data->rutaImagen = 'false';			

		$this->load->template('mapa',  $data);

	}		


	public function contacto()
	{	
        $data = new stdClass();
      
        // Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_contacto');
		$data->portada_posicion = $this->configuracion->get_value2('portada_contacto');
		$data->portada_altura = $this->config->item('altura_portada_contacto');
		$data->portada_altura_mobile = $this->config->item('altura_portada_contacto_mobile');
		$data->portada_color = $this->config->item('color_portada_contacto');	
		$data->portada_top = $this->config->item('texto_portada_top_contacto');	
		$data->portada_top_mobile = $this->config->item('texto_portada_top_contacto_mobile');	
		$data->transparencia_portada = $this->config->item('transparencia_portada_contacto');	
		$data->rutaImagen = 'false';					

		$this->load->template('contacto',  $data);

	}		


 	public function contacto_local()
 	{
        $data = new stdClass();
      
		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_contacto_local');
		$data->portada_posicion = $this->configuracion->get_value2('portada_contacto_local');
		$data->portada_altura = $this->config->item('altura_portada_contacto_local');
		$data->portada_altura_mobile = $this->config->item('altura_portada_contacto_local_mobile');
		$data->portada_color = $this->config->item('color_portada_contacto_local');		
		$data->portada_top = $this->config->item('texto_portada_top_contacto_local');	
		$data->portada_top_mobile = $this->config->item('texto_portada_top_contacto_local_mobile');	
		$data->transparencia_portada = $this->config->item('transparencia_portada_contacto_local');	
		$data->rutaImagen = 'false';				

        $this->load->template('contacto_local', $data);
	}		


	public function solicitar_restablecer()
	{
        $data = new stdClass();
      
		// Recupera imagen de portada
		$data->portada = $this->configuracion->get_value('portada_restablecer');
		$data->portada_posicion = $this->configuracion->get_value2('portada_restablecer');
		$data->portada_altura = $this->config->item('altura_portada_restablecer');
		$data->portada_altura_mobile = $this->config->item('altura_portada_restablecer_mobile');
		$data->portada_color = $this->config->item('color_portada_restablecer');	
	    $data->portada_transparencia = $this->config->item('transparencia_portada_restablecer');	

		$data->rutaImagen = 'false';				

        $this->load->template('solicitar_restablecer', $data);
    }
}
?>