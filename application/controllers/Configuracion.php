<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Tools/media_model','media');	
		$this->load->model('Tools/configuracion_model','configuracion');		
		$this->load->helper('url');		
	}


	// Actualiza la portada de una página del frontend
	public function ajax_portada()
	{
		$data = array();
		$data['status'] = TRUE;
		$foto = '';
		$pagina = '';

		// Valida si la foto es precargada o nueva

		// Foto nueva
		if(is_uploaded_file($_FILES['foto_portada']['tmp_name']))
		{
			$alto_imagen = $this->config->item('img_portada_width');
            $foto = $this->media->resize_imagen($_FILES['foto_portada'], $alto_imagen , 'portadas');
		}
		// Foto precargada
		else{
		
			$foto = $this->input->post('foto_portada_precargada');		
		}

		$data['portada'] = $foto;

		$pagina = trim($this->input->post('pagina_portada'));
		$posicion = trim($this->input->post('foto_portada_posicion'));

		// Graba la nueva portada
		$data_update = array(
				'configuracion_valor' => $foto,
				'configuracion_valor2' => $posicion,
			);

		$this->configuracion->update(array('configuracion_item' => $pagina), $data_update);

		// Elimina la portada anterior (cuando se llama desde método 'cancela_portada()')
		if (trim($this->input->post('foto_portada_anterior') != ''))
		{
			$this->media->delete_image(trim($this->input->post('foto_portada_anterior')), 'portadas');
		}		

		echo json_encode($data);		
	}
	

	// Actualiza la posición de la portada de una página del frontend
	public function ajax_portada_posicion()
	{
		$data = array();
		$data['status'] = TRUE;

		$pagina = trim($this->input->post('pagina_portada'));
		$posicion = trim($this->input->post('posicion'));

		// Graba la nueva posición
		$data_update = array(
			'configuracion_valor2' => $posicion,
		);

		$this->configuracion->update(array('configuracion_item' => $pagina), $data_update);			


		echo json_encode($data);		
	}		
		

	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en alguna portada
		$count = $this->configuracion->check_imagen_portada($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image($name, 'portadas')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen de articulo.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);
	}		
}