<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notificacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');
		$this->load->model('Actividad/venta_model','venta');	
		$this->load->model('Actividad/turno_model','turno');	
		
		$this->load->library('validations');
		$this->load->library('tools');
	}


	public function get_count_novistas_admin(){

		$data = $this->notificacion->get_count_novistas();
		echo json_encode($data);	
	}


	public function get_novistas_admin(){

		$data = array();

		$notificaciones = $this->notificacion->get_novistas();

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;

			$data[] = $row;
		}


		echo json_encode($data);	
	}


	public function get_recientes()
	{
		$data = array();

		$notificaciones = $this->notificacion->get_recientes();

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;
			$row[] = $notificacion->leida;
			$row[] = $notificacion->venta_id;
			$row[] = $notificacion->turno_id;
			$row[] = $notificacion->vista;
			$row[] = $notificacion->id;

			$data[] = $row;
		}

		echo json_encode($data);	
	}


	public function set_leida($id){

		$data = array(
				'notificacion_leida' => '1',
			);

		$this->notificacion->update(array('notificacion_id' => $id), $data);

		echo json_encode(0);	
	}


	public function set_vistas(){

		$data = array(
				'notificacion_vista' => '1',
			);

		$this->notificacion->update(array('notificacion_vista' => '0'), $data);

		echo json_encode(0);	
	}


	// Notificaciones LOCAL 
	public function get_count_novistas_local()
	{
		$data = array();

		if (isset($_SESSION['frontend_user_id'])) 
			$data = $this->notificacion_local->get_count_novistas($_SESSION['frontend_user_id']);

		echo json_encode($data);	
	}


	public function get_recientes_local(){

		$data = array();

		$notificaciones = $this->notificacion_local->get_recientes($_SESSION['frontend_user_id']);

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;
			$row[] = $notificacion->leida;
			$row[] = $notificacion->venta_id;
			$row[] = $notificacion->turno_id;
			$row[] = $notificacion->vista;
			$row[] = $notificacion->id;
			$row[] = $notificacion->vacuna_id;
			
			$data[] = $row;
		}

		echo json_encode($data);	
	}


	public function set_vistas_local(){

		$where = array(
				'notificacion_local_vista' => '0',
				'notificacion_local_local_id' => $_SESSION['frontend_user_id'],
			);

		$data = array(
				'notificacion_local_vista' => '1',
			);

		$this->notificacion_local->update($where, $data);

		echo json_encode(0);	
	}


	public function set_leida_local($id){

		$data = array(
				'notificacion_local_leida' => '1',
			);

		$this->notificacion_local->update(array('notificacion_local_id' => $id), $data);

		echo json_encode(0);	
	}


	// Notificaciones CLIENTE
	public function get_count_novistas_cliente()
	{
		$data = array();

		if (isset($_SESSION['frontend_user_id'])) 
			$data = $this->notificacion_cliente->get_count_novistas($_SESSION['frontend_user_id']);

		echo json_encode($data);	
	}	


	public function get_recientes_cliente(){

		$data = array();

		$notificaciones = $this->notificacion_cliente->get_recientes($_SESSION['frontend_user_id']);

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;
			$row[] = $notificacion->leida;
			$row[] = $notificacion->venta_id;
			$row[] = $notificacion->turno_id;
			$row[] = $notificacion->vista;
			$row[] = $notificacion->id;
			$row[] = $notificacion->vacuna_id;

			$data[] = $row;
		}

		echo json_encode($data);	
	}


	public function set_vistas_cliente(){

		$where = array(
				'notificacion_cliente_vista' => '0',
				'notificacion_cliente_cliente_id' => $_SESSION['frontend_user_id'],
			);

		$data = array(
				'notificacion_cliente_vista' => '1',
			);

		$this->notificacion_cliente->update($where, $data);

		echo json_encode(0);	
	}


	public function set_leida_cliente($id){

		$data = array(
				'notificacion_cliente_leida' => '1',
			);

		$this->notificacion_cliente->update(array('notificacion_cliente_id' => $id), $data);

		echo json_encode(0);	
	}


	public function test()
	{
		$this->load->helper('url');
		$this->load->view('admin/Actividad/notificacion_test');
	}
	
}