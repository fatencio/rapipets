<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turno extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/turnocomentario_model','turno_comentario');
		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');
		$this->load->model('Actividad/recordatorio_model','recordatorio');
		$this->load->model('Usuarios/frontend_model','frontend_user');		

		$this->load->library('email_geotienda');
		$this->load->helper('cookie');
	}


	public function ajax_procesar_turno()
	{
		$data = array();
		$detalle = new stdClass();	

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		$data = $this->_validate();

		// Datos del turno correctos
		if($data['status'])
		{
			$data['email'] = FALSE;
			$data['turno'] = FALSE;

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));			

			// Datos del turno
			$data_insert = array(
					'turno_fecha_creacion' => $fecha->format('Y-m-d H:i:s'),
					'turno_local_id' => $this->input->post('fin_turno_local_id'),
					'turno_cliente_id' => $this->input->post('fin_turno_cliente_id'),
					'turno_cliente_telefono' => $this->input->post('fin_turno_telefono'),
					'turno_servicio_item_id' => $this->input->post('fin_turno_servicio_item_id'),
					'turno_fecha' => $this->input->post('fin_turno_fecha'),
					'turno_hora' => $this->input->post('fin_turno_hora'),
					'turno_precio' => $this->input->post('fin_turno_precio'),
					'turno_mes' => substr($this->input->post('fin_turno_fecha'), 0, 6)
				);


			// Actualiza telefono del cliente
			$data_update = array(
				'cliente_telefono' => trim($this->input->post('fin_turno_telefono'))
			);

			$this->cliente->update(array('cliente_id' => $this->input->post('fin_turno_cliente_id')), $data_update);	

			// Recupera datos del cliente
			$cliente = $this->cliente->get_by_id($this->input->post('fin_turno_cliente_id'));		

			$this->db->trans_start();

				// Inserta el turno
				$turno_id = $this->turno->save($data_insert);

				// Comentarios del turno
				if(trim($this->input->post('fin_turno_observaciones')) != '')
				{
					$data_comentario = array(					
						'turno_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),
						'turno_comentario_turno_id' => $turno_id,
						'turno_comentario_cliente_id' => $this->input->post('fin_turno_cliente_id'),
						'turno_comentario_comentario' => trim($this->input->post('fin_turno_observaciones'))
					);

					$this->turno_comentario->save($data_comentario);					
				}				

				// Notificaciones
				// --------------

				// al Administrador
				$data_notificacion = array(
					'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_turno_id' => $turno_id,
					'notificacion_cliente_id' => $this->input->post('fin_turno_cliente_id'),
					'notificacion_local_id' => $this->input->post('fin_turno_local_id'),
					'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . ' ' . $_SESSION['frontend_apellido'] . '</b> solicitó un turno en <b>' . $this->input->post('fin_turno_local_nombre') . '</b>',
					'notificacion_tipo' => 'turno',
				);

				$this->notificacion->save($data_notificacion);		

				// al Local
				$data_notificacion_local = array(
					'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_local_turno_id' => $turno_id,
					'notificacion_local_cliente_id' => $this->input->post('fin_turno_cliente_id'),
					'notificacion_local_local_id' => $this->input->post('fin_turno_local_id'),
					'notificacion_local_texto' => '<b>' . $_SESSION['frontend_nombre'] . ' ' . $_SESSION['frontend_apellido'] . '</b> solicitó un turno',
					'notificacion_local_tipo' => 'turno',
				);	

				$this->notificacion_local->save($data_notificacion_local);				

				// recordatorio
				$data_recordatorio = array(
					'recordatorio_turno_id' => $turno_id
				);

				$this->recordatorio->save($data_recordatorio);						
				

			$this->db->trans_complete();

			/*  TODO: si la reserva de turno fue exitosa, pero no pudo enviarse el mail, avisar al usuario */
			
			// Valida error en la reserva de turno
			if ($this->db->trans_status() === TRUE)
			{
				$data['turno'] = TRUE;

				// Recupera datos del local
				$local = $this->local->get_by_id($this->input->post('fin_turno_local_id'));

				// Recupera datos del turno
				$turno = $this->turno->get_by_id($turno_id);

				// Formato fecha del turno
				$fecha = substr($this->input->post('fin_turno_fecha'), 6, 2) . '/' . substr($this->input->post('fin_turno_fecha'), 4, 2) . '/' . substr($this->input->post('fin_turno_fecha'), 0, 4);

				// Envía mail al cliente con los datos del turno
				$msg_mail = $this->email_geotienda->turno(
					$_SESSION['frontend_email'], 
					$local->nombre, 
					$local->direccion . ', ' . $local->localidad, 
					$local->telefono, 
					$fecha,
					$this->input->post('fin_turno_hora'),
					$turno->servicio,
					$turno->servicio_item,
					$turno->total,
					trim($this->input->post('fin_turno_observaciones')),
					$this->config->item('hs_cancela_turno_cliente')
				);

				if ($msg_mail == '')							
				{				
					$data['email'] = TRUE;
				}		

				
				// Envía mail al local con los datos del turno
				$msg_mail = $this->email_geotienda->turno_aviso_local(
					$local->email, 
					$fecha,
					$this->input->post('fin_turno_hora'),
					$cliente->nombre . ' ' . $cliente->apellido,
					$cliente->nombre,
					$cliente->telefono,
					$turno->servicio,
					$turno->servicio_item,
					$turno->total,
					trim($this->input->post('fin_turno_observaciones')),
					$this->config->item('hs_cancela_turno_local')
				);

				if ($msg_mail == '')							
				{				
					$data['email'] = TRUE;
				}		
			}	

		}

		echo json_encode($data);
	}


	public function ajax_add_comentario_local()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		$data_comentario = array
		(
			'turno_comentario_turno_id' => $this->input->post('hd_turno_id'),
			'turno_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),
			'turno_comentario_local_id' => $this->input->post('hd_turno_local_id'),
			'turno_comentario_comentario' => $this->input->post('turno_comentario')
		);

		$this->turno_comentario->save($data_comentario);	

		// Recupera datos del turno y del cliente
		$turno = $this->turno->get_by_id($this->input->post('hd_turno_id'));
		$cliente = $this->cliente->get_by_id($turno->cliente_id);

		// NOTIFICACIONES
		// al Administrador
		$data_notificacion = array(
			'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_turno_id' => $turno->id,
			'notificacion_cliente_id' => $turno->cliente_id,
			'notificacion_local_id' => $turno->local_id,
			'notificacion_texto' => '<b>' . $turno->local . '</b> ingresó un mensaje al servicio <b>' . $turno->numero. '</b>',
			'notificacion_tipo' => 'mensaje turno',
		);	

		$this->notificacion->save($data_notificacion);			

		// al Cliente
		$data_notificacion_cliente = array(
			'notificacion_cliente_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_cliente_turno_id' => $turno->id,
			'notificacion_cliente_cliente_id' => $turno->cliente_id,			
			'notificacion_cliente_local_id' => $turno->local_id,
			'notificacion_cliente_texto' => '<b>' . $turno->local . '</b> te envió un mensaje por el turno <b>' . $turno->numero. '</b>',
			'notificacion_cliente_tipo' => 'mensaje turno',
		);		

		$this->notificacion_cliente->save($data_notificacion_cliente);		

		// Mail de aviso al cliente
		$link = BASE_PATH . '/dashboard/turnos';

		$msg_mail = $this->email_geotienda->comentario_turno_aviso_cliente(
			$cliente->email,
			$cliente->nombre,
			$_SESSION['frontend_nombre'],
			$turno->numero, 
			$this->input->post('turno_comentario'),
			$link
		);

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}		

		echo json_encode(array("status" => TRUE));	
	}


	public function ajax_add_comentario_cliente()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		$data_comentario = array
		(
			'turno_comentario_turno_id' => $this->input->post('hd_turno_turno_id'),
			'turno_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),			
			'turno_comentario_cliente_id' => $this->input->post('hd_turno_cliente_id'),
			'turno_comentario_comentario' => $this->input->post('turno_comentario_cliente')
		);

		$this->turno_comentario->save($data_comentario);	

		// Recupera datos del turno y del local
		$turno = $this->turno->get_by_id($this->input->post('hd_turno_turno_id'));
		$local = $this->local->get_by_id($turno->local_id); 

		// NOTIFICACIONES
		// al Administrador
		$data_notificacion = array(
			'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_turno_id' => $turno->id,
			'notificacion_cliente_id' => $turno->cliente_id,
			'notificacion_local_id' => $turno->local_id,
			'notificacion_texto' => '<b>' . $turno->cliente . '</b> ingresó un mensaje al servicio <b>' . $turno->numero. '</b>',
			'notificacion_tipo' => 'mensaje turno',
		);	

		$this->notificacion->save($data_notificacion);			

		// al Local
		$data_notificacion_local = array(
			'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_local_turno_id' => $turno->id,
			'notificacion_local_cliente_id' => $turno->cliente_id,			
			'notificacion_local_local_id' => $turno->local_id,
			'notificacion_local_texto' => '<b>' . $turno->cliente . '</b> te envió un mensaje por el turno <b>' . $turno->numero. '</b>',
			'notificacion_local_tipo' => 'mensaje turno',
		);		

		$this->notificacion_local->save($data_notificacion_local);

		// Mail de aviso al local
		$link = BASE_PATH . '/dashboard/turnos';

		$msg_mail = $this->email_geotienda->comentario_turno_aviso_local(
			$local->email,
			$_SESSION['frontend_nombre'],
			$local->nombre,
			$turno->numero, 
			$this->input->post('turno_comentario_cliente'),
			$link
		);

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}

		echo json_encode(array("status" => TRUE));	
	}


	// El Cliente notifica el resultado de un turno
	// Concretado: SI / NO
	// Puntaje: 5 4 3 2 1 
	// Comentario (obligatorio)
	public function ajax_calificar_turno()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$data = array();
		$data['status'] = TRUE;
		$estado = 'reservado';

		// Valida datos ingresados
		$error = $this->valida_calificar();

		// Validación OK
		if ($error->status){

			// Recupera estado actual del Turno
			$turno = $this->turno->get_by_id($this->input->post('calificar_id'));

			// Estado del Turno
			// Si el cliente avisa que el turno se concretó o el estado actual es 'concretado', el turno pasa a 'concretado'
			if ($turno->estado == 'concretado' || trim($this->input->post('calificar_concretado') == 'si')) $estado = 'concretado';

			// Si el cliente avisa que el turno no se concretó y el local ya notificó como 'no concretado', el turno pasa a 'no concretado'
			if ($turno->concretado_local == 'no' && trim($this->input->post('calificar_concretado') == 'no')) $estado = 'no concretado';

			// Graba calificacion
			$data_update = array(
				'turno_concretado_cliente' => trim($this->input->post('calificar_concretado')),
				'turno_puntaje_cliente' => trim($this->input->post('calificar_puntaje')),
				'turno_valoracion_cliente' => trim($this->input->post('calificar_valoracion')),
				'turno_estado' => $estado
			);

			$this->turno->update(array('turno_id' => $this->input->post('calificar_id')), $data_update);

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// notificacion
			$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_turno_id' => $this->input->post('calificar_id'),
				'notificacion_cliente_id' => $_SESSION['frontend_user_id'],
				'notificacion_local_id' => $this->input->post('local_id'),
				'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . ' ' . $_SESSION['frontend_apellido'] . '</b> calificó al local <b>' . $this->input->post('local') . '</b> con ' . trim($this->input->post('calificar_puntaje')) . ' estrellas' ,
				'notificacion_tipo' => 'calificación turno',
			);		

			$this->notificacion->save($data_notificacion);	
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_update_observaciones()
	{	
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$data_update = array(
			'turno_observaciones'=>$this->input->post('turno_observaciones')				
		);

		$this->turno->update(array('turno_id' => $this->input->post('hd_turno_id')), $data_update);

		echo json_encode(array("status" => TRUE));
	}


	// El cliente o el local cancela un turno
	public function ajax_cancelar($cliente_local)
	{	
		$data = array();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		// Recupera datos del turno
		$turno = $this->turno->get_by_id($this->input->post('hd_cancela_id'));

		// Calcula horas faltantes para el turno
		$fecha = new DateTime($turno->fecha . ' ' . $turno->hora);
		$hoy = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));
		$interval = $fecha->diff($hoy);
		$horas = $interval->m * 720 + $interval->d * 24 + $interval->h;

		// Valida que el turno pueda ser cancelado por el cliente o por el local
		if ($cliente_local == 'cliente')
			$limite_cancela = $this->config->item('hs_cancela_turno_cliente');
		else
			$limite_cancela = $this->config->item('hs_cancela_turno_local');

		// No se ingreso comentario
		if(trim($this->input->post('cancelar_comentario')) == '')
		{
			$data['status'] = FALSE;			
			$data['error'] = 'comentario';		
			$data['mensaje'] = 'Ingresá un comentario para cancelar.';	
		}
		else
		{
			// Cancelación ok
			if ($horas >= $limite_cancela)
			{

				// Fecha y hora local
				$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

				$this->db->trans_start();

					$data_update = array(
						'turno_estado'=> 'cancelado',
						'turno_cancelado_por'=>	$cliente_local,		
						'turno_cancelado_comentario'=> $this->input->post('cancelar_comentario')		
					);

					$this->turno->update(array('turno_id' => $turno->id), $data_update);	

					// Notificaciones
					// --------------

					// Si canceló el local
					if ($cliente_local == 'local')
					{
						// al Administrador
						$data_notificacion = array(
							'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_turno_id' => $turno->id,
							'notificacion_cliente_id' => $turno->cliente_id,
							'notificacion_local_id' => $turno->local_id,
							'notificacion_texto' => '<b>' . $turno->local . '</b> canceló un turno de <b>' . $turno->cliente . '</b>',
							'notificacion_tipo' => 'cancela turno',
						);	

						// al Cliente
						$data_notificacion_cliente = array(
							'notificacion_cliente_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_cliente_turno_id' => $turno->id,
							'notificacion_cliente_cliente_id' => $turno->cliente_id,			
							'notificacion_cliente_local_id' => $turno->local_id,
							'notificacion_cliente_texto' => '<b>' . $turno->local . '</b> canceló tu turno',
							'notificacion_cliente_tipo' => 'cancela turno',
						);		

						$this->notificacion_cliente->save($data_notificacion_cliente);									
					}		

					// Si canceló el cliente
					if ($cliente_local == 'cliente')
					{
						// al Administrador
						$data_notificacion = array(
							'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_turno_id' => $turno->id,
							'notificacion_cliente_id' => $turno->cliente_id,
							'notificacion_local_id' => $turno->local_id,
							'notificacion_texto' => '<b>' . $turno->cliente . '</b> canceló un turno en <b>' . $turno->local . '</b>',
							'notificacion_tipo' => 'cancela turno',
						);	

						// al Local
						$data_notificacion_local = array(
							'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_local_turno_id' => $turno->id,
							'notificacion_local_cliente_id' => $turno->cliente_id,			
							'notificacion_local_local_id' => $turno->local_id,
							'notificacion_local_texto' => '<b>' . $turno->cliente . '</b> canceló un turno',
							'notificacion_local_tipo' => 'cancela turno',
						);		

						$this->notificacion_local->save($data_notificacion_local);							
					}	

					$this->notificacion->save($data_notificacion);				


				$this->db->trans_complete();

				/*  TODO: si la reserva de turno fue exitosa, pero no pudo enviarse el mail, avisar al usuario */
				
				// Valida error en la cancelación de turno
				if ($this->db->trans_status() === TRUE)
				{
					$data['turno'] = TRUE;

					// Recupera datos del local
					$local = $this->local->get_by_id($turno->local_id);

					// Formato fecha del turno
					$fecha = substr($turno->fecha, 8, 2) . '/' . substr($turno->fecha, 5, 2) . '/' . substr($turno->fecha, 0, 4);


					// E-mails de aviso de cancelacion (al que canceló y a la otra parte)
					// -------------------------------

					$link = BASE_PATH . '/Local/local_articulos?id=' . $local->id . '&cat=' . $local->categoria . '&turno=1';
					
					// Si canceló el local
					if ($cliente_local == 'local'){

						// Mail de aviso al cliente
						$msg_mail = $this->email_geotienda->cancela_turno_aviso_cliente(
							$turno->cliente_email, 
							$local->nombre, 
							$local->direccion . ', ' . $local->localidad, 
							$local->telefono, 
							$fecha,
							$turno->hora,
							$turno->servicio,
							$turno->servicio_item,
							$turno->total,
							$this->input->post('cancelar_comentario'),
							$link
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}		

						// Mail de confirmación al local
						$msg_mail = $this->email_geotienda->cancela_turno_confirmacion_local(
							$local->email, 
							$turno->cliente, 
							$turno->cliente_email, 
							$turno->cliente_telefono, 
							$fecha,
							$turno->hora,
							$turno->servicio,
							$turno->servicio_item,
							$turno->total,
							$this->input->post('cancelar_comentario')
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}												
					}


					// Si canceló el cliente
					if ($cliente_local == 'cliente'){

						// Mail de aviso al local
						$msg_mail = $this->email_geotienda->cancela_turno_aviso_local(
							$local->email, 
							$turno->cliente, 
							$turno->cliente_email, 
							$turno->cliente_telefono, 
							$fecha,
							$turno->hora,
							$turno->servicio,
							$turno->servicio_item,
							$turno->total,
							$this->input->post('cancelar_comentario')
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}	

						// Mail de confirmación al cliente
						$msg_mail = $this->email_geotienda->cancela_turno_confirmacion_cliente(
							$turno->cliente_email, 
							$local->nombre, 
							$local->direccion . ', ' . $local->localidad, 
							$local->telefono, 
							$fecha,
							$turno->hora,
							$turno->servicio,
							$turno->servicio_item,
							$turno->total,
							$this->input->post('cancelar_comentario'),
							$link
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}							
					}					

					$data['status'] = TRUE;			
				}
					
			}

			// Horario excedido
			else
			{
				$data['status'] = FALSE;		
				$data['interval'] = $interval;		
				$data['horas'] = $horas;		
				$data['error'] = 'horario';		
				$data['mensaje'] = 'El horario límite para cancelar el turno ha expirado.';		
			}			
		}

		echo json_encode($data);
	}


	// El local notifica el resultado de un turno
	// Concretado: SI / NO
	// Comentario (obligatorio)
	public function ajax_avisar_turno()
	{
		$data = array();
		$data['status'] = TRUE;
		$estado = 'reservado';
		
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				
		
		// Valida datos ingresados
		$error = $this->valida_avisar();

		// Validación OK
		if ($error->status){

			// Recupera estado actual del Turno
			$turno = $this->turno->get_by_id($this->input->post('avisar_id'));

			// Estado del Turno
			// Si el local avisa que el turno se concretó o el estado actual es 'concretado',  el turno pasa a 'concretado'
			if ($turno->estado == 'concretado' || trim($this->input->post('avisar_concretado') == 'si')) $estado = 'concretado';

			// Si el local avisa que el turno no se concretó y el cliente ya notificó como 'no concretado', el turno pasa a 'no concretado'
			if ($turno->concretado_cliente == 'no' && trim($this->input->post('avisar_concretado') == 'no')) $estado = 'no concretado';


			// Graba aviso
			$data_update = array(
				'turno_concretado_local' => trim($this->input->post('avisar_concretado')),
				'turno_valoracion_local' => trim($this->input->post('avisar_valoracion')),
				'turno_estado' => $estado
			);

			$this->turno->update(array('turno_id' => $this->input->post('avisar_id')), $data_update);

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Notificacion
			$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_turno_id' => $this->input->post('avisar_id'),
				'notificacion_cliente_id' => $this->input->post('avisar_cliente_id'),
				'notificacion_local_id' => $_SESSION['frontend_user_id'],
				'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> avisó que el servicio <b>' . $this->input->post('avisar_venta_nro') . '</b> ' . trim($this->input->post('avisar_concretado')) .' fue concretado.' ,
				'notificacion_tipo' => 'aviso turno',
			);

			$this->notificacion->save($data_notificacion);			


			// Recordatorio
			// TODO: elimina recordatorio de notificación
			// elimina solo si el cliente y el local ya notificaron la venta
			//$this->recordatorio->delete_by_venta('recordatorio_venta_id' => $this->input->post('avisar_id'));	
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	private function valida_avisar()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('avisar_concretado')) == '')
		{
			$error->mensaje[] = 'Seleccioná concretado SI o NO';
			$error->status = FALSE;
		}

		if(trim($this->input->post('avisar_valoracion')) == '')
		{
			$error->mensaje[] = 'Ingresá un comentario';
			$error->status = FALSE;
		}

		return $error;
	}


	private function valida_calificar()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('calificar_concretado')) == '')
		{
			$error->mensaje[] = 'Seleccioná concretado SI o NO';
			$error->status = FALSE;
		}
	
		if(trim($this->input->post('calificar_puntaje')) == '')
		{
			$error->mensaje[] = 'Calificá el servicio';
			$error->status = FALSE;
		}

		if(trim($this->input->post('calificar_valoracion')) == '')
		{
			$error->mensaje[] = 'Ingresá un comentario';
			$error->status = FALSE;
		}

		return $error;
	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fin_turno_dia') == '-1')
		{
			$data['inputerror'][] = 'hd_fin_turno_dia';
			$data['error_string'][] = 'Seleccioná un día';
			$data['status'] = FALSE;
		}

		if(trim($this->input->post('fin_turno_hora')) == '')
		{
			$data['inputerror'][] = 'hd_fin_turno_hora';
			$data['error_string'][] = 'Seleccioná un horario';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('fin_turno_telefono')) == '')
		{
			$data['inputerror'][] = 'fin_turno_telefono';
			$data['error_string'][] = 'Ingresá un teléfono';
			$data['status'] = FALSE;
		}	

		return $data;
	}


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}		
}