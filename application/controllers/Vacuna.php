<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vacuna extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/vacuna_model','vacuna');
		$this->load->model('ABM/mascota_model','mascota');
		$this->load->model('Usuarios/cliente_model','cliente');				
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');		
		$this->load->model('Usuarios/frontend_model','frontend_user');		

		$this->load->library('validations');
		$this->load->library('email_geotienda');
		$this->load->helper('url');		
		$this->load->helper('cookie');		
	}


	// DASHBOARD CLIENTE - TAB 'MIS MASCOTAS'
	// Listado de vacunas de una mascota
	public function ajax_vacunas_mascota($mascota_id)
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		$list = $this->vacuna->get_datatables($mascota_id);

		$datatable = array();
		$no = $_POST['start'];

		foreach ($list as $vacuna) {
			$no++;
			$row = array();
			$acciones = '';
			
	        $fecha = new DateTime($vacuna->fecha);
	        $fecha = $fecha->format('d/m/Y');

	        $row[] = $fecha;				

			$row[] = $vacuna->nombre;
			$row[] = $vacuna->local;
			$row[] = $vacuna->aplicada;

			// Fecha próxima vacuna
			if ($vacuna->fecha_proxima != '')
			{
		        $fecha_proxima = new DateTime($vacuna->fecha_proxima);

		        // Oculta fecha de aviso de vacunas anteriores a hoy
		        $ahora = new DateTime("today");

		        if ($fecha_proxima < $ahora)
		        {
		        	$row[] = 'aplicada';
		        }
		        else
		        {
			        $fecha_proxima = $fecha_proxima->format('d/m/Y');

			       	// Resalta vacunas del dia
			        $hoy = $fecha_proxima == date('d/m/Y') ? "&nbsp;&nbsp;&nbsp;<div class='label label-warning'>HOY</div>": "";
			        $fecha_proxima = $fecha_proxima . $hoy;	    
			        
			        $row[] = $fecha_proxima;	    	
		        }				
			}
			else
			{
				$row[] = 'no definida';
			}


			$row[] = '<button class="btn btn-xs btn-venta push-5" data-toggle="modal" href="#modal_detalle_vacuna_cliente" title="Ver detalles" onclick="detalle_vacuna_cliente('."'".$vacuna->id."'".')">Ver detalles</button>';

			$datatable[] = $row;
		}

		$output = array(

			"draw" => $_POST['draw'],
			"data" => $datatable
		);


		echo json_encode($output);
	}


	// DASHBOARD LOCAL - TAB 'VACUNAS'
	// Listado de vacunas de una mascota
	public function ajax_vacunas_mascota_local($mascota_id)
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$list = $this->vacuna->get_datatables_local($mascota_id);

		$datatable = array();
		$no = $_POST['start'];

		foreach ($list as $vacuna) {
			$no++;
			$row = array();
			$acciones = '';
			
	        $fecha = new DateTime($vacuna->fecha);
	        $fecha = $fecha->format('d/m/Y');

			$row[] = $fecha;

			$row[] = $vacuna->nombre;
			$row[] = $vacuna->local;
			$row[] = $vacuna->aplicada;


			// Fecha próxima vacuna
			if ($vacuna->fecha_proxima != '')
			{
		        $fecha_proxima = new DateTime($vacuna->fecha_proxima);

		        // Oculta fecha de aviso de vacunas anteriores a hoy
		        $ahora = new DateTime("today");

		        if ($fecha_proxima < $ahora)
		        {
		        	$row[] = 'aplicada';
		        }
		        else
		        {
			        $fecha_proxima = $fecha_proxima->format('d/m/Y');

			       	// Resalta vacunas del dia
			        $hoy = $fecha_proxima == date('d/m/Y') ? "&nbsp;&nbsp;&nbsp;<div class='label label-warning'>HOY</div>": "";
			        $fecha_proxima = $fecha_proxima . $hoy;	    
			        
			        $row[] = $fecha_proxima;	    	
		        }				
			}
			else
			{
				$row[] = 'no definida';
			}


			$acciones = '<a class="btn btn-xs btn-venta push-5" href="javascript:void(0)" title="Ver detalles" onclick="detalle_vacuna('."'".$vacuna->id."'".')" data-toggle="modal" data-target="#modal_detalle_vacuna">Ver detalles</a>';

			// Si la vacuna pertenece al local, permite modificarla
			if($vacuna->local == $_SESSION['frontend_nombre'])
			{
				$acciones .= '<a class="btn btn-xs btn-success push-5-l push-5" href="javascript:void(0)" title="Modificar" onclick="modificar_vacuna('."'".$vacuna->id."'".')" >Modificar</a>'; 
			}

			// Si no se ha indicado si la vacuna se aplicó o no, permite eliminar vacuna
			if(!$vacuna->aplicada)
			{
				$acciones .= '<a class="btn btn-xs btn-danger push-5-l" href="javascript:void(0)" title="Borrar" onclick="borrar_vacuna('."'".$vacuna->nombre."'".',' .$vacuna->id. ')">Borrar</a>'; 
			}

			$row[] = $acciones;

			$datatable[] = $row;
		}

		$output = array(

			"draw" => $_POST['draw'],
			"data" => $datatable
		);


		echo json_encode($output);
	}


	public function get_by_id($id)
	{
		$data = $this->vacuna->get_by_id($id);
		
		if($data)
		{
			$fecha = new DateTime($data->fecha);
			$data->fecha = $fecha->format('d/m/Y');
			
			if (!is_null($data->fecha_proxima))
			{
				$fecha_proxima = new DateTime($data->fecha_proxima);
				$data->fecha_proxima = $fecha_proxima->format('d/m/Y');			
			}
		}

		echo json_encode($data);	
	}	


	// DASHBOARD LOCAL - TAB 'VACUNAS'
	// Un local crea una nueva vacuna
	public function ajax_add()
	{
		$data = array();
		$data['status'] = TRUE;
		$foto = '';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		// Valida datos ingresados
		$error = $this->_validate();

		// Validación OK
		if ($error->status){

			$fecha = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fecha_vacuna')))));

			// Fecha próxima vacuna
			if (trim($this->input->post('fecha_proxima_vacuna')) != '')
				$fecha_proxima = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fecha_proxima_vacuna')))));
			else
				$fecha_proxima = null;

			// Fecha y hora local
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));			

			// Crea vacuna
			$data_insert = array(
				'vacuna_fecha_creacion' => $ahora->format('Y-m-d H:i:s'),
				'vacuna_mascota_id' => trim($this->input->post('vacuna_mascota_id')),
				'vacuna_local_id' => $_SESSION['frontend_user_id'],
				'vacuna_fecha' => $fecha,
				'vacuna_fecha_proxima' => $fecha_proxima,
				'vacuna_nombre' => trim($this->input->post('nombre_vacuna')),
				'vacuna_peso' => trim($this->input->post('peso_vacuna')),
				'vacuna_veterinario' => trim($this->input->post('veterinario_vacuna')),
				'vacuna_aplicada' => trim($this->input->post('vacuna_aplicada')),
				'vacuna_observaciones' => trim($this->input->post('observaciones_vacuna'))
			);

			$vacuna_id = $this->vacuna->save($data_insert);

			// Recupera datos del cliente
			$cliente = $this->cliente->get_by_mascota($this->input->post('vacuna_mascota_id'));
			$cliente = $this->cliente->get_by_id($cliente->id);

			// Recupera datos de la mascota
			$mascota = $this->mascota->get_by_id($this->input->post('vacuna_mascota_id'));

			// Fecha vacuna
			//$fecha_vacuna = date("d/m/Y",strtotime(trim($this->input->post('fecha_vacuna'))));
			$fecha_vacuna = date("d-m-Y",strtotime(str_replace('/', '-', trim($this->input->post('fecha_vacuna')))));

			// Fecha próxima vacuna
			if (trim($this->input->post('fecha_proxima_vacuna')) != '')
				$fecha_proxima = date("d-m-Y",strtotime(str_replace('/', '-', trim($this->input->post('fecha_proxima_vacuna')))));
			else
				$fecha_proxima = 'no definida';

			// Envía mail al cliente con los datos de la vacuna 
			$msg_mail = $this->email_geotienda->cliente_nueva_vacuna(
				$cliente->email,
				$cliente->nombre,
				$_SESSION['frontend_nombre'],
				$mascota->nombre,
				$fecha_vacuna,
				trim($this->input->post('nombre_vacuna')),
				$fecha_proxima);

			if ($msg_mail == '')							
			{				
				$data['email'] = TRUE;
			}				

			// Notifica al cliente
			$data_notificacion_cliente = array(
				'notificacion_cliente_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_cliente_vacuna_id' => $vacuna_id,
				'notificacion_cliente_cliente_id' => $cliente->id,			
				'notificacion_cliente_local_id' => $_SESSION['frontend_user_id'],
				'notificacion_cliente_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> actualizó la cartilla de vacunas de ' . $mascota->nombre,
				'notificacion_cliente_tipo' => 'vacuna',
			);		
	
			$this->notificacion_cliente->save($data_notificacion_cliente);			

			// Si el local agendó una próxima vacuna, notifica al Cliente
			/*
			if ($fecha_proxima != '')
				{
				// Recupera id del cliente
				$cliente = $this->cliente->get_by_mascota($this->input->post('vacuna_mascota_id'));

				$data_notificacion_cliente = array(
					'notificacion_cliente_fecha' => $ahora->format('Y-m-d H:i:s'),
					'notificacion_cliente_vacuna_id' => $vacuna_id,
					'notificacion_cliente_cliente_id' => $cliente->id,			
					'notificacion_cliente_local_id' => $_SESSION['frontend_user_id'],
					'notificacion_cliente_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> agendó una vacuna para tu mascota',
					'notificacion_cliente_tipo' => 'vacuna agendada',
				);		
		
				$this->notificacion_cliente->save($data_notificacion_cliente);				
			}
			*/

			$data['proxima_vacuna'] = $this->get_proxima_vacuna_mascota(trim($this->input->post('vacuna_mascota_id')));

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function ajax_update()
	{
		$data = array();
		$data['status'] = TRUE;
		$foto = '';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		

		// Valida datos ingresados
		$error = $this->_validate();

		// Validación OK
		if ($error->status){

			// Fecha y hora local
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			$fecha = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fecha_vacuna')))));

			// Fecha próxima vacuna
			if (trim($this->input->post('fecha_proxima_vacuna')) != '')
				$fecha_proxima = date("Y-m-d",strtotime(str_replace('/', '-', trim($this->input->post('fecha_proxima_vacuna')))));
			else
				$fecha_proxima = null;			
			
			$data_update = array(
				'vacuna_mascota_id' => trim($this->input->post('vacuna_mascota_id')),
				'vacuna_local_id' => $_SESSION['frontend_user_id'],
				'vacuna_fecha' => $fecha,
				'vacuna_fecha_proxima' => $fecha_proxima,				
				'vacuna_nombre' => trim($this->input->post('nombre_vacuna')),
				'vacuna_peso' => trim($this->input->post('peso_vacuna')),
				'vacuna_veterinario' => trim($this->input->post('veterinario_vacuna')),
				'vacuna_aplicada' => trim($this->input->post('vacuna_aplicada')),
				'vacuna_observaciones' => trim($this->input->post('observaciones_vacuna'))
			);

			$this->vacuna->update(array('vacuna_id' => $this->input->post('id_vacuna')), $data_update);

			/*
			// Si el local agendó una próxima vacuna, notifica al Cliente
			if ($fecha_proxima != '')
				{
				// Recupera id del cliente
				$cliente = $this->cliente->get_by_mascota($this->input->post('vacuna_mascota_id'));

				$data_notificacion_cliente = array(
					'notificacion_cliente_fecha' => $ahora->format('Y-m-d H:i:s'),
					'notificacion_cliente_vacuna_id' => $this->input->post('id_vacuna'),
					'notificacion_cliente_cliente_id' => $cliente->id,			
					'notificacion_cliente_local_id' => $_SESSION['frontend_user_id'],
					'notificacion_cliente_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> agendó una vacuna para tu mascota',
					'notificacion_cliente_tipo' => 'vacuna agendada',
				);		
		
				$this->notificacion_cliente->save($data_notificacion_cliente);				
			}
			*/

			$data['proxima_vacuna'] = $this->get_proxima_vacuna_mascota(trim($this->input->post('vacuna_mascota_id')));

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	public function get_proxima_vacuna($id)
	{
		// Fecha próxima vacuna
		$data_vacuna = $this->vacuna->get_proxima($id);

		if ($data_vacuna->fecha)
		{
			$fecha = new DateTime($data_vacuna->fecha);
			return $fecha->format('d/m/Y');				
		}
		else
		{
			return 'no definida';
		}
	}


	public function get_proxima_vacuna_mascota($mascota_id)
	{
		// Fecha próxima vacuna
		$data_vacuna = $this->vacuna->get_proxima_por_mascota($mascota_id);

		if ($data_vacuna->fecha)
		{
			$fecha = new DateTime($data_vacuna->fecha);
			return $fecha->format('d/m/Y');				
		}
		else
		{
			return 'no definida';
		}
	}


	private function _validate()
	{

		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('nombre_vacuna')) == '')
		{
			$error->mensaje[] = 'Ingresá un Nombre de vacuna';
			$error->status = FALSE;
		}		

		if(trim($this->input->post('fecha_vacuna')) == '')
		{
			$error->mensaje[] = 'Ingresá una Fecha de vacunación';
			$error->status = FALSE;
		}

		if(trim($this->input->post('veterinario_vacuna')) == '')
		{
			$error->mensaje[] = 'Ingresá un Veterinario';
			$error->status = FALSE;
		}

		/*
		if(trim($this->input->post('peso_vacuna')) == '')
		{
			$error->mensaje[] = 'Ingresá el Peso del animal';
			$error->status = FALSE;
		}
		*/

		return $error;		
	}
	

	public function ajax_delete($id, $mascota_id)
	{
        $data = array();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}		


		$resultado = $this->vacuna->delete_by_id($id);

		$data = $this->validations->valida_db_error($resultado);

		/*
		$data['proxima_vacuna'] = $this->get_proxima_vacuna($id);
		$data['proxima_vacuna_mascota'] = $this->get_proxima_vacuna($mascota_id);
		*/

		echo json_encode($data);
	}


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}
}