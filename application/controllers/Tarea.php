<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarea extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/recordatorio_model','recordatorio');
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Usuarios/local_model','local');	
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Actividad/notificacion_model','notificacion');				
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');
		$this->load->model('ABM/vacuna_model','vacuna');

		$this->load->library('email_geotienda');
		$this->load->library('validations');
	}


	// Envía mails recordatorio de calificación a Clientes y Locales
	// a partir de N horas despues de realizada la Compra o Turno
	public function enviar_recordatorio_calificacion()
	{

		$cant_recordatorios_cliente = 0;
		$cant_recordatorios_local = 0;

		$data = new stdClass();
		$data->status = TRUE;

		// CLIENTE
		// Ventas
		$recordatorios_venta = $this->recordatorio->get_recordatorios_cliente_calificacion_venta();

		$link = BASE_PATH . '/dashboard/pedidos';

		foreach ($recordatorios_venta as $recordatorio) {		

			// Recupera datos del pedido
			$venta = $this->venta->get_venta_by_id($recordatorio->venta_id);
			$detalle = $this->venta->get_detalle_venta_by_id($recordatorio->venta_id);
			$local = $this->local->get_by_id($venta->local_id);

			$fecha = new DateTime($venta->fecha);

			// Envía mail al cliente con los datos del pedido
			$msg_mail = $this->email_geotienda->recordatorio_cliente_calificacion_venta(
				$venta->cliente_email, 
				$local->nombre, 
				$local->direccion . ', ' . $local->localidad, 
				$local->telefono, 
				$venta->cliente,
				$venta->cliente_direccion . ', ' . $venta->cliente_localidad,
				$venta->cliente_telefono,				
				$venta->condicion_venta,
				$venta->tipo_envio,
				$venta->observaciones,
				$link,
				$venta->numero, 
				$fecha->format('d/m/Y'), 
				$venta->total,
				$detalle);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// registra envio del aviso
				$cliente_enviados = $recordatorio->cliente_enviados;
				$cliente_enviados++;
				$data_update = array(
					'recordatorio_cliente_enviados' => $cliente_enviados,
					'recordatorio_cliente_ultimo_enviado' => date('Y-m-d H:i:s'));

				$this->recordatorio->update(array('recordatorio_id' => $recordatorio->id), $data_update);	

				// cantidad de recordatorios enviados
				$cant_recordatorios_cliente ++;		
			}	

		}

		// Turnos
		$recordatorios_turno = $this->recordatorio->get_recordatorios_cliente_calificacion_turno();

		$link = BASE_PATH . '/dashboard/turnos';

		foreach ($recordatorios_turno as $recordatorio) {
	
			// Recupera datos del turno
			$turno = $this->turno->get_by_id($recordatorio->turno_id);

			$fecha = new DateTime($turno->fecha);
			$fecha = $fecha->format('d/m/Y');

			// Recupera datos del local
			$local = $this->local->get_by_id($turno->local_id);

			// Recupera datos del cliente
			$cliente = $this->cliente->get_by_id($turno->cliente_id);			

			
			// Envía mail al cliente con los datos del turno
			$msg_mail = $this->email_geotienda->recordatorio_cliente_calificacion_turno(
				$cliente->email, 
				$local->nombre, 
				$local->direccion . ', ' . $local->localidad, 
				$local->telefono, 
				$link,				
				$fecha,
				$turno->servicio,
				$turno->servicio_item,
				$turno->total,
				$turno->observaciones
			);


			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// registra envio del aviso
				$cliente_enviados = $recordatorio->cliente_enviados;
				$cliente_enviados++;
				$data_update = array(
					'recordatorio_cliente_enviados' => $cliente_enviados, 
					'recordatorio_cliente_ultimo_enviado' => date('Y-m-d H:i:s'));

				$this->recordatorio->update(array('recordatorio_id' => $recordatorio->id), $data_update);		

				// cantidad de recordatorios enviados
				$cant_recordatorios_cliente ++;					
			}	
			
		}
	

		// LOCAL
		// Ventas
		$recordatorios_venta_local = $this->recordatorio->get_recordatorios_local_calificacion_venta();

		$link = BASE_PATH . '/dashboard/pedidos';

		foreach ($recordatorios_venta_local as $recordatorio) {	
	
			// Recupera datos del pedido
			$venta = $this->venta->get_venta_by_id($recordatorio->venta_id);
			$detalle = $this->venta->get_detalle_venta_by_id($recordatorio->venta_id);
			$local = $this->local->get_by_id($venta->local_id);

			$fecha = new DateTime($venta->fecha);

			// Envía mail al local con los datos del pedido
			$msg_mail = $this->email_geotienda->recordatorio_local_calificacion_venta(
				$local->email, 
				$local->nombre, 
				$local->direccion . ', ' . $local->localidad, 
				$local->telefono, 
				$venta->cliente,
				$venta->cliente_direccion . ', ' . $venta->cliente_localidad,
				$venta->cliente_telefono,				
				$venta->condicion_venta,
				$venta->tipo_envio,
				$venta->observaciones,
				$link,
				$venta->numero, 
				$fecha->format('d/m/Y'), 
				$venta->total,
				$detalle);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// registra envio del aviso
				$local_enviados = $recordatorio->local_enviados;
				$local_enviados++;
				$data_update = array(
					'recordatorio_local_enviados' => $local_enviados,
					'recordatorio_local_ultimo_enviado' => date('Y-m-d H:i:s'));

				$this->recordatorio->update(array('recordatorio_id' => $recordatorio->id), $data_update);	

				// cantidad de recordatorios enviados
				$cant_recordatorios_local ++;		
			}	

		}

		// Turnos
		$recordatorios_turno_local = $this->recordatorio->get_recordatorios_local_calificacion_turno();

		$link = BASE_PATH . '/dashboard/turnos';

		foreach ($recordatorios_turno_local as $recordatorio) {

			// Recupera datos del turno
			$turno = $this->turno->get_by_id($recordatorio->turno_id);

			$fecha = new DateTime($turno->fecha);
			$fecha = $fecha->format('d/m/Y');

			// Recupera datos del local
			$local = $this->local->get_by_id($turno->local_id);

			// Recupera datos del cliente
			$cliente = $this->cliente->get_by_id($turno->cliente_id);			

			// Envía mail al local con los datos del turno
			$msg_mail = $this->email_geotienda->recordatorio_local_calificacion_turno(
				$local->email, 
				$turno->cliente, 
				$link,				
				$fecha,
				$turno->servicio,
				$turno->servicio_item,
				$turno->total,
				$turno->observaciones
			);


			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// registra envio del aviso
				$local_enviados = $recordatorio->local_enviados;
				$local_enviados++;
				$data_update = array(
					'recordatorio_local_enviados' => $local_enviados, 
					'recordatorio_local_ultimo_enviado' => date('Y-m-d H:i:s'));

				$this->recordatorio->update(array('recordatorio_id' => $recordatorio->id), $data_update);		

				// cantidad de recordatorios enviados
				$cant_recordatorios_local ++;					
			}	

		}

		$data->cant_recordatorios_cliente = $cant_recordatorios_cliente;
		$data->cant_recordatorios_local = $cant_recordatorios_local;

		log_message('debug', 'Mails enviados a clientes: ' . $cant_recordatorios_cliente);
		log_message('debug', 'Mails enviados a locales: ' . $cant_recordatorios_local);

		echo json_encode($data);	
	}


	// Califica pedidos o servicios luego de N dias sin notificación del cliente o local
	public function calificar_no_notificados()
	{
		$estado = 'concretado';

		$cant_calificaciones_pedidos = 0;
		$cant_calificaciones_servicios = 0;

		$data = new stdClass();
		$data->status = TRUE;

		// VENTAS
		$ventas_pendientes = $this->venta->get_ventas_pendientes();

		foreach ($ventas_pendientes as $venta) {		

			// Actualiza el estado de la venta
			// Si el cliente o el local avisaron como 'no concretado', pasa a 'no concretado'
			// Si no hay aviso del cliente ni del local, pasa a 'concretado'
			if ($venta->concretado_cliente == 'no' || $venta->concretado_local == 'no' ) 
				$estado = 'no concretado';
			else
				$estado = 'concretado';

			$data_update = array(
					'venta_estado' => $estado,
				);

			$data->resultado =  $this->venta->update(array('venta_id' => $venta->id), $data_update);

			$cant_calificaciones_pedidos ++;		
		}
		
		// TURNOS
		$turnos_reservados = $this->turno->get_turnos_reservados();

		foreach ($turnos_reservados as $turno) {		

			// Actualiza el estado del turno
			// Si el cliente o el local avisaron como 'no concretado', pasa a 'no concretado'
			// Si no hay aviso del cliente ni del local, pasa a 'concretado'
			if ($turno->concretado_cliente == 'no' || $turno->concretado_local == 'no' ) 
				$estado = 'no concretado';
			else
				$estado = 'concretado';

			$data_update = array(
					'turno_estado' => $estado,
				);

			$data->resultado =  $this->turno->update(array('turno_id' => $turno->id), $data_update);

			$cant_calificaciones_servicios ++;		
		}

		$data->cant_calificaciones_pedidos = $cant_calificaciones_pedidos;
		$data->cant_calificaciones_servicios = $cant_calificaciones_servicios;

		log_message('debug', 'Pedidos calificados: ' . $cant_calificaciones_pedidos);
		log_message('debug', 'Servicios calificados: ' . $cant_calificaciones_servicios);

		echo json_encode($data);	
	}	


	// Envía mails recordatorio de turno a Clientes y Locales
	public function enviar_recordatorio_turno()
	{
		$cant_recordatorios_cliente = 0;
		$cant_recordatorios_local = 0;

		$data = new stdClass();
		$data->status = TRUE;

		$recordatorios_turno = $this->turno->get_turnos_cercanos();

		foreach ($recordatorios_turno as $turno) {	

			$fecha = new DateTime($turno->fecha);
			$fecha_mail = $fecha->format('d/m/Y');

			// Mails
			// -----
			// Envía mail al cliente con los datos del turno
			$msg_mail = $this->email_geotienda->recordatorio_cliente_turno(
				$turno->cliente_email, 
				$turno->cliente_nombre,
				$turno->local, 
				$turno->local_direccion . ', ' . $turno->local_localidad, 
				$turno->local_telefono, 
				$fecha_mail,
				$turno->hora,
				$turno->servicio,
				$turno->servicio_item,
				$turno->total,
				$turno->observaciones,
				$this->config->item('hs_cancela_turno_cliente')
			);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// cantidad de recordatorios enviados
				$cant_recordatorios_cliente ++;		
			}

			// Envía mail al local con los datos del turno
			$msg_mail = $this->email_geotienda->recordatorio_local_turno(
				$turno->local_email, 
				$fecha_mail,
				$turno->hora,
				$turno->local,
				$turno->cliente,
				$turno->cliente_nombre,
				$turno->cliente_telefono,
				$turno->servicio,
				$turno->servicio_item,
				$turno->total,
				$turno->observaciones,
				$this->config->item('hs_cancela_turno_local')
			);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// cantidad de recordatorios enviados
				$cant_recordatorios_local ++;		
			}

			// Notificaciones
			// --------------

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));
			
			// al Cliente
			$data_notificacion_cliente = array(
				'notificacion_cliente_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_cliente_turno_id' => $turno->id,
				'notificacion_cliente_cliente_id' => $turno->cliente_id,			
				'notificacion_cliente_local_id' => $turno->local_id,
				'notificacion_cliente_texto' => 'El <b>'. $fecha->format('d/m/Y') . '</b> a las <b>' . $turno->hora . ' hs.</b> tenés un turno en <b>' . $turno->local. '</b>',
				'notificacion_cliente_tipo' => 'recordatorio turno'
			);		

			$this->notificacion_cliente->save($data_notificacion_cliente);		

			// al Local
			$data_notificacion_local = array(
				'notificacion_local_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_local_turno_id' => $turno->id,
				'notificacion_local_cliente_id' => $turno->cliente_id,			
				'notificacion_local_local_id' => $turno->local_id,
				'notificacion_local_texto' => 'El <b>'. $fecha->format('d/m/Y') . '</b> a las <b>' . $turno->hora . ' hs.</b> tenés un turno con <b>' . $turno->cliente . '</b>',
				'notificacion_local_tipo' => 'recordatorio turno'
			);		

			$this->notificacion_local->save($data_notificacion_local);													
		}

		$data->cant_recordatorios_cliente = $cant_recordatorios_cliente;
		$data->cant_recordatorios_local = $cant_recordatorios_local;

		log_message('debug', 'Recordatorio de turno enviados a clientes: ' . $cant_recordatorios_cliente);
		log_message('debug', 'Recordatorio de turno enviados a locales: ' . $cant_recordatorios_local);

		echo json_encode($data);	
	}


	// Envía mails recordatorio de vacuna a Clientes y Locales
	public function enviar_recordatorio_vacuna()
	{
		$cant_recordatorios_cliente = 0;
		$cant_recordatorios_local = 0;

		$data = new stdClass();
		$data->status = TRUE;

		$recordatorios_vacuna = $this->vacuna->get_vacunas_cercanas();

		foreach ($recordatorios_vacuna as $vacuna) {	

			$fecha_proxima = new DateTime($vacuna->fecha_proxima);
			$fecha_proxima = $fecha_proxima->format('d/m/Y');

			$fecha = new DateTime($vacuna->fecha);
			$fecha_mail = $fecha->format('d/m/Y');

			// Mails
			// -----
			// al Cliente
			$msg_mail = $this->email_geotienda->recordatorio_cliente_vacuna(
				$vacuna->cliente_email, 
				$vacuna->cliente_nombre,
				$vacuna->local, 
				$vacuna->mascota,
				$fecha_mail,
				$fecha_proxima,
		        $vacuna->nombre);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// cantidad de recordatorios enviados
				$cant_recordatorios_cliente ++;		
			}

			// al Local
			$msg_mail = $this->email_geotienda->recordatorio_local_vacuna(
				$vacuna->local_email, 
				$vacuna->cliente,
				$vacuna->local, 
				$vacuna->mascota,
				$fecha_mail,
				$fecha_proxima,
		        $vacuna->nombre);

			if ($msg_mail != '')							
			{			
				$data->status = FALSE;	
				$data->error = $msg_mail;
			}
			else
			{
				// cantidad de recordatorios enviados
				$cant_recordatorios_local ++;		
			}			

			// Notificaciones
			// --------------

			// Fecha y hora local
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// al Cliente
			$data_notificacion_cliente = array(
				'notificacion_cliente_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_cliente_vacuna_id' => $vacuna->id,
				'notificacion_cliente_cliente_id' => $vacuna->cliente_id,			
				'notificacion_cliente_local_id' => $vacuna->local_id,
				'notificacion_cliente_texto' => 'El <b>'. $fecha_proxima . '</b> tenés agendada una vacuna para <b>' . $vacuna->mascota. '</b>',
				'notificacion_cliente_tipo' => 'recordatorio vacuna'
			);		

			$this->notificacion_cliente->save($data_notificacion_cliente);		

			// al Local
			$data_notificacion_local = array(
				'notificacion_local_fecha' => $ahora->format('Y-m-d H:i:s'),
				'notificacion_local_vacuna_id' => $vacuna->id,
				'notificacion_local_cliente_id' => $vacuna->cliente_id,			
				'notificacion_local_local_id' => $vacuna->local_id,
				'notificacion_local_texto' => 'El <b>'. $fecha_proxima . '</b> tenés agendada una vacuna con <b>' . $vacuna->cliente_nombre . '</b> para su mascota <b>' . $vacuna->mascota. '</b>',
				'notificacion_local_tipo' => 'recordatorio vacuna'
			);		

			$this->notificacion_local->save($data_notificacion_local);												
		}

		$data->cant_recordatorios_cliente = $cant_recordatorios_cliente;
		$data->cant_recordatorios_local = $cant_recordatorios_local;

		log_message('debug', 'Recordatorio de vacuna enviados a clientes: ' . $cant_recordatorios_cliente);
		log_message('debug', 'Recordatorio de vacuna enviados a locales: ' . $cant_recordatorios_local);

		echo json_encode($data);	
	}


	// Elimina notificaciones y archivos de logs antiguos
	public function eliminar_antiguos()
	{
		$data = array();

		// Elimina notificaciones
		$resultado = $this->notificacion->delete_old();
		$resultado = $this->notificacion_cliente->delete_old();
		$resultado = $this->notificacion_local->delete_old();

		$data = $this->validations->valida_db_error($resultado);

		// Elimina logs (logs y queryLogs)
		$dias = $this->config->item('dias_log');
		$files = glob("application/logs/*");

 		$now   = time();

		foreach ($files as $file)
		    if (is_file($file))
		      	if ($now - filemtime($file) >= 60 * 60 * 24 * $dias) 
		        	unlink($file);


		echo json_encode($data);
	}


	public function test()
	{
		$data = array();
		$data['status'] = TRUE;

		// Envía mail de prueba
		$msg_mail = $this->email_geotienda->test('fatencio@gmail.com');

		if ($msg_mail != '')							
		{
			$data['status'] = FALSE;
			$data['error'] = 'mail';	
		}

		echo json_encode($data);		
	}


}