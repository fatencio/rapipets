<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/articulo_model','articulo');
		$this->load->model('Usuarios/localarticulo_model','local_articulo');	
		$this->load->model('ABM/animal_model','animal');		
		$this->load->model('ABM/rubro_model','rubro');		
		$this->load->model('ABM/marca_model','marca');		
		$this->load->model('ABM/presentacion_model','presentacion');		
		$this->load->model('ABM/tamanio_model','tamanio');
		$this->load->model('ABM/raza_model','raza');		
		$this->load->model('Tools/media_model','media');		
		$this->load->model('Usuarios/frontend_model','frontend_user');		

		$this->load->library('validations');
		$this->load->helper('cookie');				
	}


	// Listado de Artículos Geotienda que no están asociados a un local
	// Se llama desde el Dashboard de un local -> Tab Productos -> Agregar Producto
	public function ajax_list_no_en_local()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		$list = $this->articulo->get_datatables_no_en_local($_SESSION['frontend_user_id']);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $articulo) 
		{
			$no++;
			$row = array();

			$row[] = '<input type="checkbox" name="chk_agregar_producto[]" id="chk_agregar_producto" class="chk_agregar_producto" value=" ' . $articulo->id . '">';

			$row[] = $articulo->rubro;
			$row[] = $articulo->animal;								
			$row[] = $articulo->marca;
			$row[] = $articulo->nombre;
			$row[] = $articulo->codigo;

			$row[] = '<img width="100%" src="'. IMG_PATH . 'articulos/miniaturas/'. $articulo->imagen .'" >';

			//add html for action
			$row[] = '<a class="btn btn-success w-166 push-10" href="javascript:void(0)" title="Ver Detalles" onclick="ver_articulo_geotienda(' . $articulo->id. ')"><i class="fa fa-eye"></i>&nbsp;&nbsp;Ver Detalles</a>
				<br/>
				<a class="btn btn-info w-166" href="javascript:void(0)" title="Agregar a la tienda"  onclick="agregar_articulo_geotienda('."'".$articulo->id."'".')"><i class="glyphicon glyphicon-plus"></i> Agregar a la tienda </a>';

			$data[] = $row;				
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->articulo->count_all_no_en_local($_SESSION['frontend_user_id']),
						"recordsFiltered" => $this->articulo->count_filtered_no_en_local($_SESSION['frontend_user_id']),
						"data" => $data,
				);
		

		echo json_encode($output);
	}
	

	// Filtros de Artículos Geotienda que no están asociados a un local
  public function ajax_filtros_articulo_animal($rubro = -1)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = -1;
    $filtros->marca = -1;
    $filtros->raza = -1;
    $filtros->tamanio = -1;
    $filtros->edad = -1;
    $filtros->presentacion = -1;
    $filtros->medicados = -1;

    $data['animales'] = $this->articulo->get_items_filtro_articulos('animales', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  } 


  public function ajax_filtros_articulo_rubro($animal = -1)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = -1;
    $filtros->animal = $animal;
    $filtros->marca = -1;
    $filtros->raza = -1;
    $filtros->tamanio = -1;
    $filtros->edad = -1;
    $filtros->presentacion = -1;
    $filtros->medicados = -1;
    

    $data['rubros'] = $this->articulo->get_items_filtro_articulos('rubros', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  }


  public function ajax_filtros_articulo_marca($rubro, $animal, $raza, $tamanio, $edad, $presentacion, $medicados)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = -1;
    $filtros->raza = $raza;
    $filtros->tamanio = $tamanio;
    $filtros->edad = $edad;
    $filtros->presentacion = $presentacion;
    $filtros->medicados = $medicados;
    

    // Valida si el rubro actual tiene el atributo 'marca'
    $con_marca = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmarca');

    if ($con_marca > 0) $data['marcas'] = $this->articulo->get_items_filtro_articulos('marcas', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  } 


  public function ajax_filtros_articulo_edad($rubro, $animal, $marca, $raza, $tamanio, $presentacion, $medicados)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = $marca;
    $filtros->raza = $raza;
    $filtros->tamanio = $tamanio;
    $filtros->edad = -1;
    $filtros->presentacion = $presentacion;
    $filtros->medicados = $medicados;
    
    $filtros->servicio = -1;

    // Valida si el rubro actual tiene el atributo 'edad'
    $con_edad = $this->rubro->get_by_id_atributo($rubro, 'rubro_conedad');

    if ($con_edad > 0) $data['edades'] = $this->articulo->get_items_filtro_articulos('edades', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  }


  public function ajax_filtros_articulo_tamanio($rubro, $animal, $marca, $raza, $edad, $presentacion, $medicados)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = $marca;
    $filtros->raza = $raza;
    $filtros->tamanio = -1;
    $filtros->edad = $edad;
    $filtros->presentacion = $presentacion;
    $filtros->medicados = $medicados;
    
    $filtros->servicio = -1;

    // Valida si el rubro actual tiene el atributo 'tamanio'
    $con_tamanio = $this->rubro->get_by_id_atributo($rubro, 'rubro_contamanios');

    if ($con_tamanio > 0) $data['tamanios'] = $this->articulo->get_items_filtro_articulos('tamanios', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  }


  public function ajax_filtros_articulo_raza($rubro, $animal, $marca, $tamanio, $edad, $presentacion, $medicados)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = $marca;
    $filtros->raza = -1;
    $filtros->tamanio = $tamanio;
    $filtros->edad = $edad;
    $filtros->presentacion = $presentacion;
    $filtros->medicados = $medicados;
    

    // Valida si el rubro actual tiene el atributo 'raza'
    $con_raza = $this->rubro->get_by_id_atributo($rubro, 'rubro_conraza');

    if ($con_raza > 0) $data['razas'] = $this->articulo->get_items_filtro_articulos('razas', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  } 


  public function ajax_filtros_articulo_presentacion($rubro, $animal, $marca, $raza, $tamanio, $edad, $medicados)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = $marca;
    $filtros->raza = $raza;
    $filtros->tamanio = $tamanio;
    $filtros->edad = $edad;
    $filtros->presentacion = -1;
    $filtros->medicados = $medicados;
    
    $filtros->servicio = -1;

    // Valida si el rubro actual tiene el atributo 'presentacion'
    $con_presentacion = $this->rubro->get_by_id_atributo($rubro, 'rubro_conpresentacion');

    if ($con_presentacion > 0) $data['presentaciones'] = $this->articulo->get_items_filtro_articulos('presentaciones', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  }


  public function ajax_filtros_articulo_medicados($rubro, $animal, $marca, $raza, $tamanio, $edad, $presentacion)
  {
    $data = array();
    $filtros = new stdClass();

    $filtros->rubro = $rubro;
    $filtros->animal = $animal;
    $filtros->marca = $marca;
    $filtros->raza = $raza;
    $filtros->tamanio = $tamanio;
    $filtros->edad = $edad;
    $filtros->presentacion = $presentacion;
    
    $filtros->medicados = -1;
    $filtros->servicio = -1;

    // Valida si el rubro actual tiene el atributo 'medicados'
    $con_medicados = $this->rubro->get_by_id_atributo($rubro, 'rubro_conmedicados');

    if ($con_medicados > 0) $data['medicados'] = $this->articulo->get_items_filtro_articulos('medicados', $this->input->post('local_id'), $filtros);

    echo json_encode($data);  
  } 

  	 
	// Detalles de un articulo (no asociado a un local)
	public function get_by_id($id)
	{
		$data = $this->articulo->get_by_id($id, $_SESSION['frontend_user_id']);

		echo json_encode($data);	
	}	


	public function ajax_add_producto_local()
	{	
		$data = array();
		$data['status'] = TRUE;
		$imagen = '';
		$marca_id = null;
		$medicados = null;		

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		// Valida datos ingresados
		$error = $this->valida_producto_local(true);

		// Validación OK
		if ($error->status)
		{
			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto_producto_local']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$imagen = $this->media->resize_image_big_thumb($_FILES['foto_producto_local'], $thumb, $big, 'articulos/locales', $_SESSION['frontend_user_id']);
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen_producto_local');		
			}

			// Valida si es un artículo con marca
			if ($this->input->post('con_marca_producto_local') == '1') $marca_id = $this->input->post('marca_producto_local');

			// Medicados
			if ($this->input->post('medicados_producto_local') != '') $medicados = $this->input->post('medicados_producto_local');

			$data_insert = array
			(
				'articulo_nombre' => $this->input->post('nombre_producto_local'),
				//'articulo_codigo' => $this->input->post('codigo'),
				'articulo_detalle' => $this->input->post('detalle_producto_local'),
				'articulo_animal_id' => $this->input->post('animal_producto_local'),	
				'articulo_rubro_id' => $this->input->post('hd_rubro_producto_local'),
				'articulo_imagen' => $imagen,
				'articulo_medicados' => $medicados,	
				'articulo_edad' => $this->input->post('edad_producto_local'),	
				'articulo_marca_id' => $marca_id,
				'articulo_creador' => 'Local'
			);

			$id_insert = $this->articulo->save($data_insert);
	         
	         
		    // Para artículos sin presentación, asigna la presentación 'Sin Presentación' (presentacion_id = 1)
			if ($this->input->post('con_presentacion_producto_local') == '0')
			{
		        $data_presentacion = array
		        (
		     	    'articulo_presentacion_articulo_id' => $id_insert,
					'articulo_presentacion_presentacion_id'=> 1,
		         );

		     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);	    	
		    }
		    else
		    {
		        // Inserta presentaciones asignadas al articulo
		        $presentacion_producto_local = $this->input->post('presentacion_producto_local');
		         
		        for($i = 0; $i < count($presentacion_producto_local); $i++) 
		        {	        
			        $data_presentacion = array
			        (
			     	    'articulo_presentacion_articulo_id' => $id_insert,
						'articulo_presentacion_presentacion_id'=> (int)$presentacion_producto_local[$i],
			         );

			     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);
			    }
			}
			

	        // Inserta razas asignadas al articulo
	        $data_razas = $this->input->post('raza_producto_local');
	         
	        for($i = 0; $i < count($data_razas); $i++) 
	        {
		        $data_raza = array
		        (
		     	    'articulo_raza_articulo_id' => $id_insert,
					'articulo_raza_raza_id'=> (int)$data_razas[$i],
		         );

		     	$insert_raza =  $this->articulo->save_raza($data_raza);
		    }	    

	     	// Inserta tamaños asignados al articulo
	        $data_tamanios = $this->input->post('tamanio_producto_local');
	         
	        for($i = 0; $i < count($data_tamanios); $i++) 
	        {
		        $data_tamanio= array
		        (
		     	    'articulo_tamanio_articulo_id' => $id_insert,
					'articulo_tamanio_tamanio_id'=> (int)$data_tamanios[$i],
		         );

		     	$insert_tamanio =  $this->articulo->save_tamanio($data_tamanio);
		    }	


		    // AGREGA ARTICULO AL LOCAL (agrega todas las presentaciones, pero inactivas)
			// Recupera presentaciones del articulo creado
			$presentaciones = $this->articulo->get_articulo_presentaciones($id_insert);		

			foreach ($presentaciones as $presentacion) 
			{
				$data_insert = array(
					'local_articulo_local_id' => $_SESSION['frontend_user_id'],
					'local_articulo_articulo_presentacion_id' => $presentacion->articulo_presentacion_id,
					'local_articulo_precio' => 0,
					'local_articulo_precio_descuento' => 0,
					'local_articulo_descuento_id' => 5, // TODO: recuperar id sin descuento desde el modelo
					'local_articulo_creador' => 'Local'
				);

				$this->local_articulo->save($data_insert);		
			}	    

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);
	}	


	public function ajax_update_producto_local()
	{
		$data = array();
		$data['status'] = TRUE;
		$imagen = '';
		$marca_id = null;
		$medicados = null;
		$presentaciones_actuales = array();
		$presentaciones_nuevas = array();
		$presentaciones_no_borrar = array();
		$presentaciones_insertar = array();
		$presentaciones_utilizadas = array();
		$aviso_borrar_presentaciones = '';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	
		
		// Valida datos ingresados
		$error = $this->valida_producto_local();

		// Validación OK
		if ($error->status)
		{
			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto_producto_local']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');

				$imagen = $this->media->resize_image_big_thumb($_FILES['foto_producto_local'], $thumb, $big, 'articulos/locales', $_SESSION['frontend_user_id']);
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen_producto_local');		
			}

			// Valida si es un artículo con marca
			if ($this->input->post('con_marca_producto_local') == '1') $marca_id = $this->input->post('marca_producto_local');

			// Medicados
			if ($this->input->post('medicados_producto_local') != '') $medicados = $this->input->post('medicados_producto_local');

			$data_update = array(
					'articulo_nombre' => $this->input->post('nombre_producto_local'),
					//'articulo_codigo' => $this->input->post('codigo'),
					'articulo_detalle' => $this->input->post('detalle_producto_local'),
					'articulo_animal_id' => $this->input->post('animal_producto_local'),		
					'articulo_rubro_id' => $this->input->post('hd_rubro_producto_local'),	
					'articulo_medicados' => $medicados,	
					'articulo_edad' => $this->input->post('edad_producto_local'),				
					'articulo_marca_id' => $marca_id,	
					'articulo_imagen' => $imagen,			
				);

			$this->articulo->update(array('articulo_id' => $this->input->post('id_producto_local')), $data_update);

			// Presentaciones
			if ($this->input->post('con_presentacion_producto_local') == '1')
		    {
				$presentaciones_nuevas = $this->input->post('presentacion_producto_local');

				// Recupera presentaciones actuales del articulo
				$presentaciones_actuales = $this->articulo->get_presentaciones_by_id($this->input->post('id_producto_local'));

				// Presentaciones que no se modifican
				$presentaciones_no_borrar = array_intersect($presentaciones_actuales, $presentaciones_nuevas);   

				// Busca presentaciones activas en el local y que están siendo quitadas del articulo
				$presentaciones_utilizadas = $this->local_articulo->get_presentaciones_id_activas_articulo_local($_SESSION['frontend_user_id'], $this->input->post('id_producto_local'));
				$presentaciones_utilizadas = array_diff($presentaciones_utilizadas, $presentaciones_nuevas); 

		 		// Si existen, las agrega a $presentaciones_no_borrar y avisa
				if (count($presentaciones_utilizadas) > 0)
				{
					$aviso_borrar_presentaciones = 'true';
					$presentaciones_no_borrar = array_merge($presentaciones_no_borrar, $presentaciones_utilizadas);
				}

		        // Elimina presentaciones asignadas al producto correspondiente en el local, excepto las no modificadas y las que estan activas en el local
		        $this->local_articulo->delete_by_local_articulo_filtrado($_SESSION['frontend_user_id'], $this->input->post('id_producto_local'), $presentaciones_no_borrar);

		        // Elimina presentaciones asignadas al articulo, excepto las no modificadas y las que estan activas en el local
		        $this->articulo->delete_articulo_presentacion_by_id($this->input->post('id_producto_local'), $presentaciones_no_borrar);

				// Inserta presentaciones asignadas al articulo, excepto las no modificadas y las que no pueden eliminarse
		        for($i = 0; $i < count($presentaciones_nuevas); $i++) 
		        {
		        	if (!in_array($presentaciones_nuevas[$i], $presentaciones_no_borrar))
		        	{
				        $data_presentacion = array
				        (
				     	    'articulo_presentacion_articulo_id' => $this->input->post('id_producto_local'),
							'articulo_presentacion_presentacion_id'=> (int)$presentaciones_nuevas[$i],
				         );

				     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);    

				     	// Inserta presentaciones en el producto del local correspondiente
						$data_presentacion_local = array
						(
							'local_articulo_local_id' => $_SESSION['frontend_user_id'],
							'local_articulo_articulo_presentacion_id' => $insert_presentacion,
							'local_articulo_precio' => 0,
							'local_articulo_precio_descuento' => 0,
							'local_articulo_descuento_id' => 5, // TODO: recuperar id sin descuento desde el modelo
							'local_articulo_creador' => 'Local'
						);

						$this->local_articulo->save($data_presentacion_local);					     	 		
		        	}
			    }
			}

	        // Elimina razas asignadas al articulo
	        $this->articulo->delete_articulo_raza_by_id($this->input->post('id_producto_local'));

	        // Inserta razas asignadas al articulo
	        $data_razas = $this->input->post('raza_producto_local');
	         
	        for($i = 0; $i < count($data_razas); $i++) {
	           
		        $data_raza = array(
		     	    'articulo_raza_articulo_id' => $this->input->post('id_producto_local'),
					'articulo_raza_raza_id'=> (int)$data_razas[$i],
						
		         );

		     	$insert_raza =  $this->articulo->save_raza($data_raza);
		    }

	        // Elimina tamanios asignadas al articulo
	        $this->articulo->delete_articulo_tamanio_by_id($this->input->post('id_producto_local'));

		      // Inserta tamaños asignadas al articulo
	        $data_tamanios= $this->input->post('tamanio_producto_local');
	         
	        for($i = 0; $i < count($data_tamanios); $i++) {
	           
		        $data_tamanio = array
		        (
		     	    'articulo_tamanio_articulo_id' => $this->input->post('id_producto_local'),
					'articulo_tamanio_tamanio_id'=> (int)$data_tamanios[$i],
		         );

		     	$insert_tamanio =  $this->articulo->save_tamanio($data_tamanio);
		    }
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		$data['aviso_borrar_presentaciones'] = $aviso_borrar_presentaciones;

		echo json_encode($data);		    
	}


	private function valida_producto_local($add = false)
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


        if($this->input->post('hd_rubro_producto_local') == '')
		{
			$error->mensaje[] = 'Seleccioná una rubro.';
			$error->status = FALSE;
		}
		
        if($this->input->post('animal_producto_local') == '')
		{
			$error->mensaje[] = 'Seleccioná una animal.';
			$error->status = FALSE;
		}	

		if(trim($this->input->post('nombre_producto_local')) == '')
		{
			$error->mensaje[] = 'Ingresá un nombre.';
			$error->status = FALSE;
		}	

		// Valida marca si es un artículo con marca
		if ($this->input->post('con_marca_producto_local') == '1')
		{
			if(trim($this->input->post('marca_producto_local')) == '')
			{
				$error->mensaje[] = 'Seleccioná una marca.';
				$error->status = FALSE;
			}			
		} 

		// Valida presentacion si es un artículo con presentacion
		if ($this->input->post('con_presentacion_producto_local') == '1')
		{
			if($this->input->post('presentacion_producto_local') == '')
			{
				$error->mensaje[] = 'Seleccioná al menos una presentación.';
				$error->status = FALSE;
			}			
		} 

		// Valida que no exista un registro con el mismo nombre
		if ($add){

			// Valida si es un artículo con marca
			if ($this->input->post('con_marca_producto_local') == '1')
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('nombre_producto_local')), $this->input->post('animal_producto_local'), $this->input->post('marca_producto_local'));
			else
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('nombre_producto_local')), $this->input->post('animal_producto_local'), null);				
		}
		else
			// Valida si es un artículo con marca
			if ($this->input->post('con_marca_producto_local') == '1')
				$duplicated = $this->articulo->check_duplicated_edit($this->input->post('id_producto_local'), trim($this->input->post('nombre_producto_local')), $this->input->post('animal_producto_local'), $this->input->post('marca_producto_local'));
			else
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('id_producto_local'), $this->input->post('nombre_producto_local')), $this->input->post('animal_producto_local'), null);	

			
		if ($duplicated > 0)
		{
			if ($this->input->post('con_marca_producto_local') == '1')
				$error->mensaje[] = 'Ya existe un Producto con ese nombre, marca y animal';
			else
				$error->mensaje[] = 'Ya existe un Producto con ese nombre y animal';

			$error->status = FALSE;
		}					

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto_producto_local']['tmp_name']))
		{
			if(!$this->media->check_image_extension($_FILES['foto_producto_local']['name']))
			{
				$error->mensaje[] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$error->status = FALSE;
			}
		}
		
		return $error;
	}	


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}	
}