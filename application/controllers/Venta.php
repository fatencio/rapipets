<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/ventacomentario_model','venta_comentario');
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');
		$this->load->model('Actividad/recordatorio_model','recordatorio');
		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Usuarios/frontend_model','frontend_user');

		$this->load->library('email_geotienda');
		$this->load->helper('cookie');		
	
  		$CI = &get_instance();
    	$CI->config->load("mercadopago", TRUE);
    	$config = $CI->config->item('mercadopago');
    	$this->load->library('mercadopago', $config); 
	}


	public function ajax_procesar_pedido()
	{
		$data = array();
		$detalle = new stdClass();	

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$data = $this->_validate();

		// Datos del pedido correctos
		if($data['status'])
		{
			$venta_estado = 'pendiente';
			$data['email'] = FALSE;
			$data['venta'] = FALSE;
			$data['pago_con_mp'] = FALSE;

			// Para una venta por Mercadopago, el estado inicial de la venta es 'cancelado'
			// para evitar que una venta quede pendiente si el usuario cerró la aplicación 
			// mientras se mostraba el checkout de mercadopago
			if ($this->input->post('fin_pedido_forma_pago') == "Mercado Pago") $venta_estado = 'cancelado';

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Encabezado de la venta
			$data_insert = array(
					'venta_fecha' => $fecha->format('Y-m-d H:i:s'),
					'venta_tipo_envio' => $this->input->post('fin_pedido_envio'),
					'venta_cliente_nombre' => $this->input->post('fin_pedido_nombre'),
					'venta_cliente_direccion' => $this->input->post('fin_pedido_direccion'),
					'venta_cliente_localidad' => $this->input->post('fin_pedido_localidad'),
					'venta_cliente_provincia' => $this->input->post('fin_pedido_provincia'),
					'venta_cliente_telefono' => $this->input->post('fin_pedido_telefono'),
					'venta_local_id' => $this->input->post('fin_pedido_local_id'),
					'venta_cliente_id' => $this->input->post('fin_pedido_cliente_id'),
					'venta_total' => $this->input->post('fin_pedido_total'),
					'venta_condicion_venta' => $this->input->post('fin_pedido_forma_pago'),
					'venta_estado' => $venta_estado,
					'venta_mes' => $fecha->format('Ym')
				);


			$this->db->trans_start();

				// Actualiza telefono y domicilio del cliente
				if(trim($this->input->post('fin_pedido_direccion')) != '') 
				{				
					$data_update = array(
					'cliente_telefono' => trim($this->input->post('fin_pedido_telefono')),
					'cliente_direccion' => trim($this->input->post('fin_pedido_direccion')),
					'cliente_localidad' => trim($this->input->post('fin_pedido_localidad')),
					'cliente_provincia_id' => trim($this->input->post('fin_pedido_provincia_id'))
					);
				}
				else
				{
					$data_update = array(
						'cliente_telefono' => trim($this->input->post('fin_pedido_telefono'))
					);					
				}

				$this->cliente->update(array('cliente_id' => $this->input->post('fin_pedido_cliente_id')), $data_update);	

				$nro_lineas = $this->input->post('nro_lineas');			

				// Inserta la venta
				$venta_id = $this->venta->save($data_insert);

				// Comentarios de la venta
				if(trim($this->input->post('fin_pedido_observaciones')) != '')
				{
					$data_comentario = array(					
						'venta_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),
						'venta_comentario_venta_id' => $venta_id,
						'venta_comentario_cliente_id' => $this->input->post('fin_pedido_cliente_id'),
						'venta_comentario_comentario' => trim($this->input->post('fin_pedido_observaciones'))
					);

					$this->venta_comentario->save($data_comentario);					
				}

				// Detalles de la venta
				for ($i=0; $i < $nro_lineas; $i++) { 

					$articulo_id = "detalle_articulo_id_" . $i;
					$presentacion_id = "detalle_presentacion_id_" . $i;
					$precio = "detalle_precio_" . $i;
					$cantidad = "detalle_cantidad_" . $i;

					$data_detalle = array(
						'venta_detalle_venta_id' => $venta_id,
						'venta_detalle_articulo_id' => $this->input->post($articulo_id),
						'venta_detalle_presentacion_id'  => $this->input->post($presentacion_id),
						'venta_detalle_precio' => $this->input->post($precio),
						'venta_detalle_descuento' => 0,  // TODO: se usa? en el campo 'venta_detalle_precio' ya se graba el precio con descuento, si existe el descuento
						'venta_detalle_cantidad' => $this->input->post($cantidad),
					);
					
					$this->venta->save_detalle($data_detalle);
				}

				// Notificaciones
				// --------------

				// al Administrador
				$data_notificacion = array(
					'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_venta_id' => $venta_id,
					'notificacion_cliente_id' => $this->input->post('fin_pedido_cliente_id'),
					'notificacion_local_id' => $this->input->post('fin_pedido_local_id'),
					'notificacion_texto' => '<b>' . $this->input->post('fin_pedido_nombre') . '</b> realizó una compra en <b>' . $this->input->post('local_nombre') . '</b>',
					'notificacion_tipo' => 'venta',
				);

				$this->notificacion->save($data_notificacion);			

				// al Local
				$data_notificacion_local = array(
					'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
					'notificacion_local_venta_id' => $venta_id,
					'notificacion_local_cliente_id' => $this->input->post('fin_pedido_cliente_id'),			
					'notificacion_local_local_id' => $this->input->post('fin_pedido_local_id'),
					'notificacion_local_texto' => '<b>' . $this->input->post('fin_pedido_nombre') . '</b> realizó un pedido',
					'notificacion_local_tipo' => 'venta',
				);		

				$this->notificacion_local->save($data_notificacion_local);					

				// recordatorio
				$data_recordatorio = array(
					'recordatorio_venta_id' => $venta_id
				);

				$this->recordatorio->save($data_recordatorio);	


			$this->db->trans_complete();

			/*  TODO: si el pedido fue exitoso, pero no pudo enviarse el mail, avisar al usuario? */
			
			// Valida error en la venta
			if ($this->db->trans_status() === TRUE)
			{
				$data['venta'] = TRUE;
				$data['venta_id'] = $venta_id;

				// Recupera datos del pedido
				$local = $this->local->get_by_id($this->input->post('fin_pedido_local_id'));
				$venta = $this->venta->get_venta_by_id($venta_id);
				$detalle = $this->venta->get_detalle_venta_by_id($venta_id);

				// Procesa pedido por Mercado Pago
				if ($this->input->post('fin_pedido_forma_pago') == "Mercado Pago"){

				  $preference_data = array(
						"items" => array(				
						   array(
								"id" => "$venta_id",
							    "title" => "GeoTienda",
							    "quantity" => 1,
							    "currency_id" => "ARS",
							    "unit_price" => floatval($this->input->post('fin_pedido_total'))
						   )
						),
						"external_reference" => $venta_id,

						"back_urls"=>  array(
							"success" => BASE_PATH . "/Local/local_articulos?id=" . $local->id . "&cat=" . $local->categoria . "&venta_id=" . $venta_id,
							"pending" => BASE_PATH . "/Local/local_articulos?id=" . $local->id . "&cat=" . $local->categoria . "&venta_id=" . $venta_id,							
							"failure" => BASE_PATH . "/Local/local_articulos?id=" . $local->id . "&cat=" . $local->categoria . "&venta_id=" . $venta_id
						),
					);

					$data['mercadopago'] = $this->mercadopago->create_preference ($preference_data);
					$data['pago_con_mp'] = TRUE;

					// TODO: enviar mail una vez recibido el aviso ok de mp
					// TODO: meter proceso de mp dentro de la transaccion??

				}

				// Si la venta es en Efectivo o con tarjetas, envía mail con los datos al cliente y al local
				else{

					$fecha = new DateTime($venta->fecha);

					// Envía mail al cliente con los datos del pedido
					$msg_mail = $this->email_geotienda->venta(
						$venta->cliente_email, 
						$local->nombre, 
						$local->direccion . ', ' . $local->localidad, 
						$local->telefono, 
						$this->input->post('fin_pedido_nombre'),
						$this->input->post('fin_pedido_direccion') . ', ' . $this->input->post('fin_pedido_localidad'),
						$this->input->post('fin_pedido_telefono'),				
						$this->input->post('fin_pedido_forma_pago'),
						$this->input->post('fin_pedido_envio'),
						$this->input->post('fin_pedido_observaciones'),
						$venta->numero, 
						$fecha->format('d/m/Y'), 
						$venta->total,
						$detalle);

					if ($msg_mail == '')							
					{				
						$data['email'] = TRUE;
					}	

					// Recupera solo el nombre del cliente
					$arr = explode(' ',trim($this->input->post('fin_pedido_nombre')));
					$nombre = $arr[0]; 

					// Envía mail al local con los datos del pedido
					$msg_mail = $this->email_geotienda->venta_aviso_local(
						$local->email, 
						$this->input->post('fin_pedido_nombre'),
						$nombre,
						$this->input->post('fin_pedido_direccion') . ', ' . $this->input->post('fin_pedido_localidad'),
						$this->input->post('fin_pedido_telefono'),				
						$this->input->post('fin_pedido_forma_pago'),
						$this->input->post('fin_pedido_envio'),
						$this->input->post('fin_pedido_observaciones'),
						$venta->numero, 
						$fecha->format('d/m/Y'), 
						$venta->total,
						$detalle);					

					if ($msg_mail == '')							
					{				
						$data['email'] = TRUE;
					}	
				}
			}			
		}

		echo json_encode($data);
	}


	public function ajax_add_comentario_local()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		$data_comentario = array
		(
			'venta_comentario_venta_id' => $this->input->post('hd_venta_id'),
			'venta_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),
			'venta_comentario_local_id' => $this->input->post('hd_venta_local_id'),
			'venta_comentario_comentario' => $this->input->post('venta_comentario')
		);

		$this->venta_comentario->save($data_comentario);	

		// Recupera datos de la venta y el cliente
		$venta = $this->venta->get_venta_by_id($this->input->post('hd_venta_id'));
		$cliente = $this->cliente->get_by_id($venta->cliente_id);

		// NOTIFICACIONES
		// al Administrador
		$data_notificacion = array(
			'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_venta_id' => $venta->id,
			'notificacion_cliente_id' => $venta->cliente_id,
			'notificacion_local_id' => $venta->local_id,
			'notificacion_texto' => '<b>' . $venta->local . '</b> ingresó un mensaje al pedido <b>' . $venta->numero. '</b>',
			'notificacion_tipo' => 'mensaje venta',
		);	

		$this->notificacion->save($data_notificacion);			

		// al Cliente
		$data_notificacion_cliente = array(
			'notificacion_cliente_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_cliente_venta_id' => $venta->id,
			'notificacion_cliente_cliente_id' => $venta->cliente_id,			
			'notificacion_cliente_local_id' => $venta->local_id,
			'notificacion_cliente_texto' => '<b>' . $venta->local . '</b> te envió un mensaje por el pedido <b>' . $venta->numero. '</b>',
			'notificacion_cliente_tipo' => 'mensaje venta',
		);		

		$this->notificacion_cliente->save($data_notificacion_cliente);								

		// Mail de aviso al cliente
		// TODO: validar que la venta corresponda al cliente (en el caso que modifiquen la url)
		//$link = BASE_PATH . '/dashboard/pedidos?vid=' . $venta->id;
		$link = BASE_PATH . '/dashboard/pedidos';

		$msg_mail = $this->email_geotienda->comentario_venta_aviso_cliente(
			$cliente->email,
			$cliente->nombre,
			$_SESSION['frontend_nombre'],
			$venta->numero, 
			$this->input->post('venta_comentario'),
			$link
		);

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}

		echo json_encode(array("status" => TRUE));	
	}


	public function ajax_add_comentario_cliente()
	{
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		$data_comentario = array
		(
			'venta_comentario_venta_id' => $this->input->post('hd_cliente_venta_id'),
			'venta_comentario_fecha' => $fecha->format('Y-m-d H:i:s'),			
			'venta_comentario_cliente_id' => $this->input->post('hd_cliente_id'),
			'venta_comentario_comentario' => $this->input->post('venta_comentario_cliente')
		);

		$this->venta_comentario->save($data_comentario);

		// Recupera datos de la venta y del local
		$venta = $this->venta->get_venta_by_id($this->input->post('hd_cliente_venta_id'));
		$local = $this->local->get_by_id($venta->local_id); 

		// NOTIFICACIONES
		// al Administrador
		$data_notificacion = array(
			'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_venta_id' => $venta->id,
			'notificacion_cliente_id' => $venta->cliente_id,
			'notificacion_local_id' => $venta->local_id,
			'notificacion_texto' => '<b>' . $venta->cliente . '</b> ingresó un mensaje al pedido <b>' . $venta->numero. '</b>',
			'notificacion_tipo' => 'mensaje venta',
		);	

		$this->notificacion->save($data_notificacion);			

		// al Local
		$data_notificacion_local = array(
			'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
			'notificacion_local_venta_id' => $venta->id,
			'notificacion_local_cliente_id' => $venta->cliente_id,			
			'notificacion_local_local_id' => $venta->local_id,
			'notificacion_local_texto' => '<b>' . $venta->cliente . '</b> te envió un mensaje por el pedido <b>' . $venta->numero. '</b>',
			'notificacion_local_tipo' => 'mensaje venta',
		);		

		$this->notificacion_local->save($data_notificacion_local);

		// Mail de aviso al local
		$link = BASE_PATH . '/dashboard/pedidos';

		$msg_mail = $this->email_geotienda->comentario_venta_aviso_local(
			$local->email,
			$_SESSION['frontend_nombre'],
			$local->nombre,
			$venta->numero, 
			$this->input->post('venta_comentario_cliente'),
			$link
		);

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}

		echo json_encode(array("status" => TRUE));	
	}


	// Para ventas hechas con Mercadopago (pendiente o concretado), envía mail al cliente con los datos de su compra
	public function ajax_enviar_mail_venta()
	{

		$venta_id = $this->input->post('venta_id');

		// Recupera datos del pedido
		$venta = $this->venta->get_venta_by_id($venta_id);
		$detalle = $this->venta->get_detalle_venta_by_id($venta_id);
		$local = $this->local->get_by_id($venta->local_id);

		$fecha = new DateTime($venta->fecha);

		// Envía mail al cliente con los datos del pedido
		// TODO: recuperar comentarios a la venta, ya no se usa $venta->observaciones
		$msg_mail = $this->email_geotienda->venta(
			$venta->cliente_email, 
			$local->nombre, 
			$local->direccion . ' ' . $local->localidad, 
			$local->telefono, 
			$venta->cliente,
			$venta->cliente_direccion . ' ' . $venta->cliente_localidad,
			$venta->cliente_telefono,				
			$venta->condicion_venta,
			$venta->tipo_envio,
			$venta->observaciones,
			$venta->numero, 
			$fecha->format('d/m/Y'), 
			$venta->total,
			$detalle);

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}

		// Recupera solo el nombre del cliente
		$arr = explode(' ',trim($this->input->post('fin_pedido_nombre')));
		$nombre = $arr[0]; 

		// Envía mail al local con los datos del pedido
		$msg_mail = $this->email_geotienda->venta_aviso_local(
			$venta->cliente_email, 
			$this->input->post('fin_pedido_nombre'),
			$nombre,
			$this->input->post('fin_pedido_direccion') . ', ' . $this->input->post('fin_pedido_localidad'),
			$this->input->post('fin_pedido_telefono'),				
			$this->input->post('fin_pedido_forma_pago'),
			$this->input->post('fin_pedido_envio'),
			$this->input->post('fin_pedido_observaciones'),
			$venta->numero, 
			$fecha->format('d/m/Y'), 
			$venta->total,
			$detalle);					

		if ($msg_mail == '')							
		{				
			$data['email'] = TRUE;
		}

		echo json_encode($data);
	}


	public function ajax_actualizar_estado_venta()
	{
		$venta_id = $this->input->post('venta_id');
		$estado = $this->input->post('estado');

		$data = array(
				'venta_estado' => $estado,
			);

		$data['resultado'] =  $this->venta->update(array('venta_id' => $venta_id), $data);

		echo json_encode($data);
	}



	// El Cliente notifica el resultado de una venta
	// Concretado: SI / NO
	// Puntaje: 5 4 3 2 1 
	// Comentario (obligatorio)
	public function ajax_calificar_venta()
	{
		$data = array();
		$data['status'] = TRUE;
		$estado = 'pendiente';

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		// Valida datos ingresados
		$error = $this->valida_calificar();

		// Validación OK
		if ($error->status){

			// Recupera estado actual del Pedido
			$venta = $this->venta->get_venta_by_id($this->input->post('calificar_id'));
			$concretado_local = $venta->concretado_local;

			// Estado del Pedido
			// Si el cliente avisa que la venta se concretó o el estado actual es 'concretado', el pedido pasa a 'concretado'
			if ($venta->estado == 'concretado' || trim($this->input->post('calificar_concretado') == 'si')) $estado = 'concretado';

			// Si el cliente avisa que la venta no se concretó y el local ya notificó como 'no concretado', el pedido pasa a 'no concretado'
			if ($venta->concretado_local == 'no' && trim($this->input->post('calificar_concretado') == 'no')) $estado = 'no concretado';

			// Graba calificacion
			$data_update = array(
				'venta_concretado_cliente' => trim($this->input->post('calificar_concretado')),
				'venta_puntaje_cliente' => trim($this->input->post('calificar_puntaje')),
				'venta_valoracion_cliente' => trim($this->input->post('calificar_valoracion')),
				'venta_estado' => $estado
			);

			$this->venta->update(array('venta_id' => $this->input->post('calificar_id')), $data_update);

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// notificacion
			$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_venta_id' => $this->input->post('calificar_id'),
				'notificacion_cliente_id' => $_SESSION['frontend_user_id'],
				'notificacion_local_id' => $this->input->post('local_id'),
				'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . ' ' . $_SESSION['frontend_apellido'] . '</b> calificó al local <b>' . $this->input->post('local') . '</b> con ' . trim($this->input->post('calificar_puntaje')) . ' estrellas' ,
				'notificacion_tipo' => 'calificación venta',
			);

			$this->notificacion->save($data_notificacion);			

			// TODO: elimina recordatorio de calificacion
			// elimina solo si el cliente y el local ya calificaron la venta
			//$this->recordatorio->delete_by_venta('recordatorio_venta_id' => $this->input->post('calificar_id'));	
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	private function valida_calificar()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('calificar_concretado')) == '')
		{
			$error->mensaje[] = 'Seleccioná concretado SI o NO';
			$error->status = FALSE;
		}
	
		if(trim($this->input->post('calificar_puntaje')) == '')
		{
			$error->mensaje[] = 'Calificá el servicio';
			$error->status = FALSE;
		}

		if(trim($this->input->post('calificar_valoracion')) == '')
		{
			$error->mensaje[] = 'Ingresá un comentario';
			$error->status = FALSE;
		}

		return $error;
	}


	// El local notifica el resultado de una venta
	// Concretado: SI / NO
	// Comentario (obligatorio)
	public function ajax_avisar_venta()
	{
		$data = array();
		$data['status'] = TRUE;
		$estado = 'pendiente';
		
		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}				

		// Valida datos ingresados
		$error = $this->valida_avisar();

		// Validación OK
		if ($error->status){

			// Recupera estado actual del Pedido
			$venta = $this->venta->get_venta_by_id($this->input->post('avisar_id'));

			// Estado del Pedido
			// Si el local avisa que la venta se concretó o el estado actual es 'concretado', el pedido pasa a 'concretado'
			if ($venta->estado == 'concretado' || trim($this->input->post('avisar_concretado') == 'si')) 
				$estado = 'concretado';

			// Si el local avisa que la venta no se concretó y el cliente ya notificó como 'no concretado', el pedido pasa a 'no concretado'
			if ($venta->concretado_cliente == 'no' && trim($this->input->post('avisar_concretado') == 'no')) 
				$estado = 'no concretado';

			// Graba aviso
			$data_update = array(
				'venta_concretado_local' => trim($this->input->post('avisar_concretado')),
				'venta_valoracion_local' => trim($this->input->post('avisar_valoracion')),
				'venta_estado' => $estado
			);

			$this->venta->update(array('venta_id' => $this->input->post('avisar_id')), $data_update);

			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Notificacion
			$data_notificacion = array(
				'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
				'notificacion_venta_id' => $this->input->post('avisar_id'),
				'notificacion_cliente_id' => $this->input->post('avisar_cliente_id'),
				'notificacion_local_id' => $_SESSION['frontend_user_id'],
				'notificacion_texto' => '<b>' . $_SESSION['frontend_nombre'] . '</b> avisó que la venta <b>' . $this->input->post('avisar_venta_nro') . '</b> ' . trim($this->input->post('avisar_concretado')) .' fue concretada.' ,
				'notificacion_tipo' => 'aviso venta',
			);

			$this->notificacion->save($data_notificacion);			


			// Recordatorio
			// TODO: elimina recordatorio de notificación
			// elimina solo si el cliente y el local ya notificaron la venta
			//$this->recordatorio->delete_by_venta('recordatorio_venta_id' => $this->input->post('avisar_id'));	
		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}


	private function valida_avisar()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('avisar_concretado')) == '')
		{
			$error->mensaje[] = 'Seleccioná concretado SI o NO';
			$error->status = FALSE;
		}

		if(trim($this->input->post('avisar_valoracion')) == '')
		{
			$error->mensaje[] = 'Ingresá un comentario';
			$error->status = FALSE;
		}

		return $error;
	}


	// El cliente o el local cancela un pedido
	public function ajax_cancelar($cliente_local)
	{	
		$data = array();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}						

		// Recupera datos de la venta
		$venta = $this->venta->get_venta_by_id($this->input->post('hd_cancela_id'));

		// No se ingreso comentario
		if(trim($this->input->post('cancelar_comentario')) == '')
		{
			$data['status'] = FALSE;			
			$data['error'] = 'comentario';		
			$data['mensaje'] = 'Ingresá un comentario para cancelar.';	
		}
		else
		{
			// Fecha y hora local
			$fecha = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			// Valida que el venta pueda ser cancelado (estado 'pendiente')
			if ($venta->estado == 'pendiente') 
			{
				$this->db->trans_start();

					$data_update = array(
						'venta_estado'=> 'cancelado',
						'venta_cancelado_por'=>	$cliente_local,		
						'venta_cancelado_comentario'=> $this->input->post('cancelar_comentario')		
					);

					$this->venta->update(array('venta_id' => $venta->id), $data_update);	

					// Notificaciones
					// --------------

					// Si canceló el local
					if ($cliente_local == 'local')
					{
						// al Administrador
						$data_notificacion = array(
							'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_venta_id' => $venta->id,
							'notificacion_cliente_id' => $venta->cliente_id,
							'notificacion_local_id' => $venta->local_id,
							'notificacion_texto' => '<b>' . $venta->local . '</b> canceló un pedido de <b>' . $venta->cliente. '</b>',
							'notificacion_tipo' => 'cancela venta',
						);	

						// al Cliente
						$data_notificacion_cliente = array(
							'notificacion_cliente_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_cliente_venta_id' => $venta->id,
							'notificacion_cliente_cliente_id' => $venta->cliente_id,			
							'notificacion_cliente_local_id' => $venta->local_id,
							'notificacion_cliente_texto' => '<b>' . $venta->local . '</b> canceló tu pedido',
							'notificacion_cliente_tipo' => 'cancela venta',
						);		

						$this->notificacion_cliente->save($data_notificacion_cliente);							
					}		

					// Si canceló el cliente
					if ($cliente_local == 'cliente')
					{
						// al Administrador
						$data_notificacion = array(
							'notificacion_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_venta_id' => $venta->id,
							'notificacion_cliente_id' => $venta->cliente_id,
							'notificacion_local_id' => $venta->local_id,
							'notificacion_texto' => '<b>' . $venta->cliente . '</b> canceló un pedido en <b>' . $venta->local . '</b>',
							'notificacion_tipo' => 'cancela venta',
						);	

						// al Local
						$data_notificacion_local = array(
							'notificacion_local_fecha' => $fecha->format('Y-m-d H:i:s'),
							'notificacion_local_venta_id' => $venta->id,
							'notificacion_local_cliente_id' => $venta->cliente_id,			
							'notificacion_local_local_id' => $venta->local_id,
							'notificacion_local_texto' => '<b>' . $venta->cliente . '</b> canceló un pedido',
							'notificacion_local_tipo' => 'cancela venta',
						);		

						$this->notificacion_local->save($data_notificacion_local);								
					}	

					$this->notificacion->save($data_notificacion);				

				$this->db->trans_complete();

				/*  TODO: si la reserva de venta fue exitosa, pero no pudo enviarse el mail, avisar al usuario */
				
				// Valida error en la cancelación de venta
				if ($this->db->trans_status() === TRUE)
				{
					$data['venta'] = TRUE;

					// Recupera datos del local
					$local = $this->local->get_by_id($venta->local_id); 

					// Recupera detalles del pedido
					$detalle = $this->venta->get_detalle_venta_by_id($venta->id);

					// Formato fecha del venta
					$fecha = substr($venta->fecha, 8, 2) . '/' . substr($venta->fecha, 5, 2) . '/' . substr($venta->fecha, 0, 4);


					// E-mails de aviso de cancelacion (al que canceló y a la otra parte)
					// -------------------------------

					$link = BASE_PATH . '/Local/local_articulos?id=' . $local->id . '&cat=' . $local->categoria . '&venta=1';
					
					// Si canceló el local
					if ($cliente_local == 'local'){

						// Mail de aviso al cliente
						$msg_mail = $this->email_geotienda->cancela_venta_aviso_cliente(
							$venta->cliente_email, 
							$local->nombre, 
							$local->direccion . ', ' . $local->localidad, 
							$local->telefono, 
							$venta->numero, 
							$detalle,
							$venta->total,
							$fecha,
							$this->input->post('cancelar_comentario'),
							$link
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}		

						// Mail de confirmación al local
						$msg_mail = $this->email_geotienda->cancela_venta_confirmacion_local(
							$local->email, 
							$venta->cliente, 
							$venta->cliente_email, 
							$venta->cliente_telefono, 
							$venta->numero, 
							$detalle,
							$venta->total,
							$fecha,
							$this->input->post('cancelar_comentario')
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}												
					}


					// Si canceló el cliente
					if ($cliente_local == 'cliente'){

						// Mail de aviso al local
						$msg_mail = $this->email_geotienda->cancela_venta_aviso_local(
							$local->email, 
							$venta->cliente, 
							$venta->cliente_email, 
							$venta->cliente_telefono, 
							$venta->numero, 
							$detalle,
							$venta->total,								
							$fecha,
							$this->input->post('cancelar_comentario')
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}	

						// Mail de confirmación al cliente
						$msg_mail = $this->email_geotienda->cancela_venta_confirmacion_cliente(
							$venta->cliente_email, 
							$local->nombre, 
							$local->direccion . ', ' . $local->localidad, 
							$local->telefono, 
							$venta->numero, 
							$detalle,
							$venta->total,							
							$fecha,
							$this->input->post('cancelar_comentario'),
							$link
						);

						if ($msg_mail == '')							
						{				
							$data['email'] = TRUE;
						}							
					}					

					$data['status'] = TRUE;			
				}
			}

			// Venta con estaddo <> a 'pendiente'
			else
			{
				$data['status'] = FALSE;		
				$data['error'] = 'estado';		
				$data['mensaje'] = 'El pedido ya ha sido procesado.';		
			}			
		}

		echo json_encode($data);
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('fin_pedido_envio') == '')
		{
			$data['inputerror'][] = 'ddl_fin_pedido_envio';
			$data['error_string'][] = 'Elegí una forma de envío';
			$data['status'] = FALSE;
		}

		if(trim($this->input->post('fin_pedido_nombre')) == '')
		{
			$data['inputerror'][] = 'fin_pedido_nombre';
			$data['error_string'][] = 'Ingresá tu nombre';
			$data['status'] = FALSE;
		}

		if($this->input->post('fin_pedido_forma_pago') == '-1')
		{
			$data['inputerror'][] = 'fin_pedido_forma_pago';
			$data['error_string'][] = 'Elegí una forma de pago';
			$data['status'] = FALSE;
		}

		// Para envios a domicilio
		if($this->input->post('fin_pedido_envio') == 'A domicilio')
		{
			if(trim($this->input->post('fin_pedido_direccion')) == '')
			{
				$data['inputerror'][] = 'fin_pedido_direccion';
				$data['error_string'][] = 'Ingresá tu dirección';
				$data['status'] = FALSE;
			}

			if(trim($this->input->post('fin_pedido_localidad')) == '')
			{
				$data['inputerror'][] = 'fin_pedido_localidad';
				$data['error_string'][] = 'Ingresá tu localidad';
				$data['status'] = FALSE;
			}

			if(trim($this->input->post('fin_pedido_provincia_id')) == '')
			{
				$data['inputerror'][] = 'fin_pedido_provincia_id';
				$data['error_string'][] = 'Seleccioná tu provincia';
				$data['status'] = FALSE;
			}

			if(trim($this->input->post('fin_pedido_telefono')) == '')
			{
				$data['inputerror'][] = 'fin_pedido_telefono';
				$data['error_string'][] = 'Ingresá un teléfono';
				$data['status'] = FALSE;
			}			
		}	

		// Para locales que no hace envios a domicilio
		if($this->input->post('fin_pedido_envio_domicilio') == 'no')
		{
			if(trim($this->input->post('fin_pedido_telefono')) == '')
			{
				$data['inputerror'][] = 'fin_pedido_telefono';
				$data['error_string'][] = 'Ingresá un teléfono';
				$data['status'] = FALSE;
			}			
		}	

		return $data;
	}


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}		
}