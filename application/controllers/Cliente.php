<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios/cliente_model','cliente');		
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/valoracion_model','valoracion');
		$this->load->model('Actividad/notificacioncliente_model','notificacion_cliente');		
		$this->load->model('Usuarios/frontend_model','frontend_user');		

		$this->load->library('tools');
		$this->load->helper('url');		
		$this->load->helper('cookie');		
	}

	public function ajax_cargar_datos($id)
	{
		$data = new stdClass();

		$data->cliente = $this->cliente->get_by_id($id);

		echo json_encode($data);	
	}


	// Tab 'Mis Pedidos' del dashboard del Cliente 
	public function ajax_ventas_pendientes_list()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->venta->get_datatables_pendientes($_SESSION['frontend_user_id']);

			$datatable = array();

			foreach ($list as $venta) {

				$row_puntaje = '';
				$row_acciones = '';
				$row = array();

				// Para pedidos pendientes permite ingresar comentarios
				if ($venta->estado == 'pendiente')
				{
					$row[] = '<div class="pull-left"><a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Compra" onclick="detalle_venta_cliente('."'".$venta->id."'".', true)">Compra ' . $venta->numero . '&nbsp;</a>';
				}
				else
				{
					$row[] = '<div class="pull-left"><a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Compra" onclick="detalle_venta_cliente('."'".$venta->id."'".')">Compra ' . $venta->numero . '&nbsp;</a>';					
				}

				$fecha = new DateTime($venta->fecha);
				$row[] = $fecha->format('d/m/Y');
				$row[] = $venta->local;
				$row[] = $venta->forma_pago;

				$row[] = "$ " . number_format($venta->total, 2, ',', '.');

				$puntaje = $venta->puntaje_cliente;

				// Si ya tiene califiacion la muestra
				if (!is_null($puntaje)){

					for($x=1; $x<=5; $x++)
					{					
					if($x <= $puntaje)
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
					else
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
					}					

					if ($venta->valoracion_aprobada == 'no')
						$row_puntaje .= '<div class="alert alert-danger push-10-t"><p class="font-13">Comentario Rechazado porque viola los <a  class="alert-link" target="_blank" href="' . site_url("/terminos") . '">Términos y Condiciones</a> y las <a class="alert-link" target="_blank" href="' . site_url("/politicas") . '">Políticas de Privacidad</a> de GeoTienda.</p></div>';
				
					$row_puntaje .= '<br /><div  class="push-10-t">"' . $venta->valoracion_cliente . '"</div>';
				}
				// Si no tiene calificación, muestra los botones 'calificar' y 'cancelar'
				else
				{
					$row_puntaje .= '<a class="btn btn-xs btn-info push-5" href="javascript:void(0)" title="Calificar Compra" onclick="mostrar_calificar('. "'" . $venta->tipo . "'" . ", '" . $venta->id . "'" . ')">Calificar Compra</a>&nbsp;&nbsp;';

					// Solo permite cancelar pedidos que no hayan sido notificados o concretados
					if ($venta->concretado_local == '' && $venta->concretado_cliente == '' && $venta->estado != 'concretado')
					{
						$row_puntaje .= '<a class="btn btn-xs btn-danger push-5" href="javascript:void(0)" title="Cancelar Pedido" onclick="mostrar_cancelar_pedido('."'".$venta->id."'".')">Cancelar Pedido</a></div>';	
					}
				}

				$row_puntaje .= '</div>';

				$row[] = $row_puntaje; 

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->venta->count_all_pendientes($_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->venta->count_filtered_pendientes($_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	// Tab 'Mis Turnos' del dashboard del Cliente - Turnos Activos
	public function ajax_turnos_activos_list()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->turno->get_datatables_activos($_SESSION['frontend_user_id']);
			$datatable = array();


			foreach ($list as $turno) {

				$row = array();
				$acciones = '';

				$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno_cliente('."'".$turno->id."'".')">&nbsp;Turno&nbsp;' . $turno->numero . '&nbsp;</a>'; 

				$fecha = new DateTime($turno->fecha);
	            $hora = new DateTime($turno->hora);  

		        // Resalta turnos del dia
		        $hoy = $fecha->format('d/m/Y') == date('d/m/Y') ? "<div class='label label-warning'>HOY</div>": "";

		        $fecha = $fecha->format('d/m/Y') . ' ' . $hora->format('H:i') . ' hs&nbsp;&nbsp;&nbsp;' . $hoy;

		        $row[] = $fecha;	
		        $row[] = $turno->local;	
				$row[] = $turno->servicio . ' - ' . $turno->servicio_item;
				$row[] = "$ " . number_format($turno->total, 2, ',', '.');        
			
				
				$puntaje = $turno->puntaje_cliente;

				// Si ya tiene califiacion la muestra
				if (!is_null($puntaje)){

					$acciones .= '<div class="pull-left push-5-t force-left-60 force-top-10">';

					for($x=1; $x<=5; $x++)
					{					
					if($x <= $puntaje)
						$acciones .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
					else
						$acciones .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
					}					

					if ($turno->valoracion_aprobada == 'no')
						$acciones .= '<div class="alert alert-danger push-10-t"><p class="font-13">Comentario Rechazado porque viola los <a  class="alert-link" target="_blank" href="' . site_url("/terminos") . '">Términos y Condiciones</a> y las <a class="alert-link" target="_blank" href="' . site_url("/politicas") . '">Políticas de Privacidad</a> de GeoTienda.</p></div>';
				

					$acciones .= '<br /><div  class="push-10-t">"' . $turno->valoracion_cliente . '"</div>';
				}
				// Si no tiene calificación, muestra los botones 'calificar' y 'cancelar'
				else
				{
					// Calcula dias transcurridos del turno					
					$fecha = new DateTime($turno->fecha);
					$hoy = new DateTime("today");

					$diasTranscurridos = $this->tools->cuentaDias($hoy, $fecha);

					// Valida si ya puede calificar
					$dias = $this->config->item('dias_calificacion_turno');

					if ($diasTranscurridos >= $dias)
					{
						$acciones .= '<a class="btn btn-xs btn-info" href="javascript:void(0)" title="Calificar Turno" onclick="mostrar_calificar('. "'turno'" . ", '" . $turno->id . "'" . ')">&nbsp;Calificar Turno&nbsp;</a>';	
					}

					// Valida que el turno este en estado 'reservado' para poder cancelar
					if ($turno->estado == 'reservado')
					{
						$acciones .= '<a class="btn btn-xs btn-danger push-5-t" href="javascript:void(0)" title="Cancelar Turno" onclick="mostrar_cancelar_turno('."'".$turno->id."'".')">Cancelar Turno</a>';					
					}					
				}

				$row[] = $acciones;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->turno->count_all_activos($_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->turno->count_filtered_activos($_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}			
	}


	// Tab 'Mis Turnos' del dashboard del Cliente - Turnos Vencidos
	public function ajax_turnos_vencidos_list()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}			

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->turno->get_datatables_vencidos($_SESSION['frontend_user_id']);
			$datatable = array();


			foreach ($list as $turno) {

				$row_puntaje = '';
				$row = array();

				$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno_cliente('."'".$turno->id."'".')">&nbsp;Turno&nbsp;' . $turno->numero . '&nbsp;</a>'; 

				$fecha = new DateTime($turno->fecha);
	            $hora = new DateTime($turno->hora);  

		        // Resalta turnos del dia
		        $hoy = $fecha->format('d/m/Y') == date('d/m/Y') ? "<div class='label label-warning'>HOY</div>": "";

		        $fecha = $fecha->format('d/m/Y') . ' ' . $hora->format('H:i') . ' hs&nbsp;&nbsp;&nbsp;' . $hoy;

		        $row[] = $fecha;	
		        $row[] = $turno->local;	
				$row[] = $turno->servicio . ' - ' . $turno->servicio_item;
				$row[] = "$ " . number_format($turno->total, 2, ',', '.');        

				$puntaje = $turno->puntaje_cliente;

				// Si ya tiene califiacion la muestra
				if (!is_null($puntaje)){

					$row_puntaje = '<div class="pull-left push-5-t force-left-60 force-top-10">';

					for($x=1; $x<=5; $x++)
					{					
					if($x <= $puntaje)
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
					else
						$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
					}					

					if ($turno->valoracion_aprobada == 'no')
						$row_puntaje .= '<div class="alert alert-danger push-10-t"><p class="font-13">Comentario Rechazado porque viola los <a  class="alert-link" target="_blank" href="' . site_url("/terminos") . '">Términos y Condiciones</a> y las <a class="alert-link" target="_blank" href="' . site_url("/politicas") . '">Políticas de Privacidad</a> de GeoTienda.</p></div>';
				

					$row_puntaje .= '<br /><div  class="push-10-t">"' . $turno->valoracion_cliente . '"</div>';
				}
				// Si no tiene calificación, muestra el botón 'calificar'
				else
				{
					// Calcula dias transcurridos del turno					
					$fecha = new DateTime($turno->fecha);
					$hoy = new DateTime("today");

					$diasTranscurridos = $this->tools->cuentaDias($hoy, $fecha);

					// Valida si ya puede calificar
					$dias = $this->config->item('dias_calificacion_turno');

					if ($diasTranscurridos >= $dias)
					{
						$row_puntaje .= '<a class="btn btn-xs btn-info push-5" href="javascript:void(0)" title="Calificar Turno" onclick="mostrar_calificar('. "'turno'" . ", '" . $turno->id . "'" . ')">Calificar Turno</a>';	
					}

				}

				$row[] = $row_puntaje;

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->turno->count_all_vencidos($_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->turno->count_filtered_vencidos($_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}			
	}


	// Tab 'Historial' del dashboard del Cliente 
	public function ajax_historial_list()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}			

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$list = $this->venta->get_datatables_historial($_SESSION['frontend_user_id']);

			$datatable = array();

			foreach ($list as $venta) {

				$row_puntaje = '';
				$row = array();

				if ($venta->tipo == 'venta')
				{
					$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Compra" onclick="detalle_venta_cliente('."'".$venta->id."'".')">Compra ' . $venta->numero . '&nbsp;</a>'; 
				}
				else
				{
					$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno_cliente('."'".$venta->id."'".')">&nbsp;Turno&nbsp;' . $venta->numero . '&nbsp;</a>'; 
				}

				$fecha = new DateTime($venta->fecha);
				$row[] = $fecha->format('d/m/Y');
				
				$row[] = $venta->local;

				switch ($venta->estado) {

					case 'concretado':
						$row[] = '<td class="hidden-xs"><span class="text-success font-w600">Concretado</span></td>';
						break;

					case 'cancelado':
						$row[] = '<td class="hidden-xs"><span class="text-danger font-w600">Cancelado</span></td>';
						break;		

					case 'no concretado':
						$row[] = '<td class="hidden-xs"><span class="text-danger font-w600">No Concretado</span></td>';
						break;											
				}

				$row[] = $venta->forma_pago;

				$row[] = "$ " . number_format($venta->total, 2, ',', '.');

				$puntaje = $venta->puntaje_cliente;

				switch ($venta->estado) {

					// Para pedidos o servicios concretados o no concretados muestra la valoración
					case 'concretado':
					case 'no concretado':

						// Si ya tiene califiacion la muestra
						if (!is_null($puntaje)){

							for($x=1; $x<=5; $x++)
							{					
							if($x <= $puntaje)
								$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
							else
								$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
							}					

							if ($venta->valoracion_aprobada == 'no')
								$row_puntaje .= '<div class="alert alert-danger push-10-t"><p class="font-13">Comentario Rechazado porque viola los <a  class="alert-link" target="_blank" href="' . site_url("/terminos") . '">Términos y Condiciones</a>&nbsp;&nbsp;<br /> y las <a class="alert-link" target="_blank" href="' . site_url("/politicas") . '">Políticas de Privacidad</a> de GeoTienda.</p></div>';
						

							$row_puntaje .= '<br /><div  class="push-10-t">"' . $venta->valoracion_cliente . '"</div>';
						}
						
					break;

					// Para pedidos o servicios cancelados, muestra la valoración o el comentario de cancelación
					case 'cancelado':

						// Si ya tiene califiacion la muestra
						if (!is_null($puntaje))
						{
							for($x=1; $x<=5; $x++)
							{					
							if($x <= $puntaje)
								$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
							else
								$row_puntaje .= '<img class="push-4-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
							}					

							if ($venta->valoracion_aprobada == 'no')
								$row_puntaje .= '<div class="alert alert-danger push-10-t"><p class="font-13">Comentario Rechazado porque viola los <a  class="alert-link" target="_blank" href="' . site_url("/terminos") . '">Términos y Condiciones</a> y las <a class="alert-link" target="_blank" href="' . site_url("/politicas") . '">Políticas de Privacidad</a> de GeoTienda.</p></div>';
						

							$row_puntaje .= '<br /><div  class="push-10-t">"' . $venta->valoracion_cliente . '"</div>';
						}
						else
						{
							$row_puntaje = '"' . $venta->cancelado_comentario . '"';
						}

					break;
				}	

				$row[] = $row_puntaje; 

				$datatable[] = $row;
			}

			$output = array(

				"draw" => $_POST['draw'],
			
				"recordsTotal" => $this->venta->count_all_historial($_SESSION['frontend_user_id']),
				"recordsFiltered" => $this->venta->count_filtered_historial($_SESSION['frontend_user_id']),
			
				"data" => $datatable
			);

	
			echo json_encode($output);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


	// Tab 'Notificaciones' del dashboard del Cliente
	public function ajax_notificaciones_cliente()
	{

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		$list = $this->notificacion_cliente->get_datatables($_SESSION['frontend_user_id']);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $notificacion) {
			$no++;
			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;

			// tipo
			switch ($notificacion->tipo) {			

				case 'mensaje venta':
				case 'mensaje turno':
					$row[] = 'Mensaje';	
					break;	

				case 'cancela turno':
					$row[] = 'Turno cancelado';	
					break;	

				case 'cancela venta':
					$row[] = 'Pedido cancelado';	
					break;	

				case 'recordatorio turno':
					$row[] = 'Recordatorio turno';	
					break;	

				case 'vacuna':
					$row[] = 'Nueva vacuna';	
					break;	

				case 'recordatorio vacuna':
					$row[] = 'Recordatorio vacuna';	
					break;										

			}

			$row[] = $notificacion->texto;

			// ver detalles 
			switch ($notificacion->tipo) {			

				case 'cancela turno':
				case 'recordatorio turno':
				case 'mensaje turno':
					$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="ver turno" onclick="detalle_turno_cliente('."'".$notificacion->turno_id."'".')">&nbsp;&nbsp;&nbsp;ver turno&nbsp;&nbsp;</a>';	
					break;	

				case 'cancela venta':
				case 'mensaje venta':
					$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="ver pedido" onclick="detalle_venta_cliente('."'".$notificacion->venta_id."'".')">&nbsp;ver pedido&nbsp;</a>';	
					break;						

				case 'vacuna': 
					$row[] = '<a class="btn btn-xs btn-vacunas" href="javascript:void(0)" title="ver vacuna" onclick="detalle_vacuna_cliente('."'".$notificacion->vacuna_id."'".')">&nbsp;ver vacuna&nbsp;</a>';
					break;	

				case 'recordatorio vacuna':  
					$row[] = '<a class="btn btn-xs btn-vacunas" href="javascript:void(0)" title="ver vacuna" onclick="detalle_vacuna_agendada_cliente('."'".$notificacion->vacuna_id."'".')">&nbsp;ver vacuna&nbsp;</a>';
					break;

				default:
					$row[] = '';
					break;
			}

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->notificacion_cliente->count_all($_SESSION['frontend_user_id']),
						"recordsFiltered" => $this->notificacion_cliente->count_filtered($_SESSION['frontend_user_id']),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_cambiar_avatar($nombre)
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}			

		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$data_update = array('cliente_avatar' => $nombre);

			$this->cliente->update(array('cliente_id' => $_SESSION['frontend_user_id']), $data_update);

			$_SESSION['frontend_avatar'] = $nombre;

			echo json_encode($data);	
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}			

	}


	public function ajax_cargar()
	{
		$data = new stdClass();

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}			
		
		 // Valida que el cliente tenga su sesion iniciada   
		if (isset($_SESSION['frontend_logged_in'])){

			$data = $this->cliente->get_by_id($_SESSION['frontend_user_id']);
			echo json_encode($data);
		}
		else
		{
			$data->login = true;
			$this->load->template('landing', $data);
		}
	}


    public function ajax_update()
	{
		$data = array();
		$data['status'] = TRUE;
		

		// Valida que el usuario este habilitado
		if (isset($_SESSION['frontend_email']))
		{
			if (!$this->frontend_user->resolve_user_status($_SESSION['frontend_email'])) $this->logout();			
		}	

		// Valida datos ingresados
		$error = $this->valida_update();

		// Validación OK
		if ($error->status)
		{
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			// Actualiza datos del cliente
			$data_update = array(
				'cliente_nombre' => trim($this->input->post('cliente-nombre')),
				'cliente_apellido' => trim($this->input->post('cliente-apellido')),
				'cliente_telefono' => trim($this->input->post('cliente-telefono')),
				'cliente_email' => trim($this->input->post('cliente-email')),

				// Audit trail
				'cliente_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
				'cliente_usuario_modificacion' => trim($this->input->post('cliente-email'))						
			);

			$this->cliente->update(array('cliente_id' => $_SESSION['frontend_user_id']), $data_update);

			// Actualiza contraseña
			if(trim($this->input->post('cliente-password')) != ''){

				$data_update = array('cliente_password' => $this->input->post('cliente-password'));

				$this->cliente->update_password(array('cliente_id' => $_SESSION['frontend_user_id']), $data_update);		
			}

		} 
		else
		{
			$data['status'] = FALSE;
			$data['error'] = $error;
		}

		echo json_encode($data);		
	}	


	private function valida_update()
	{
		$error = new stdClass();

		$error->status = TRUE;
		$error->mensaje = array();


		if(trim($this->input->post('cliente-nombre')) == '')
		{
			$error->mensaje[] = 'Ingresá tu nombre';
			$error->status = FALSE;
		}
	
		if(trim($this->input->post('cliente-apellido')) == '')
		{
			$error->mensaje[] = 'Ingresá tu apellido';
			$error->status = FALSE;
		}

		if(trim($this->input->post('cliente-telefono')) == '')
		{
			$error->mensaje[] = 'Ingresá tu teléfono';
			$error->status = FALSE;
		}		

		if(trim($this->input->post('cliente-email')) == '')
		{
			$error->mensaje[] = 'Ingresá un e-mail válido';
			$error->status = FALSE;
		}
		else{
			// Valida formato email
			if(!filter_var(trim($this->input->post('cliente-email')), FILTER_VALIDATE_EMAIL))
			{
				$error->mensaje[] = 'Ingresá un e-mail válido';
				$error->status = FALSE;
			}	
		}								
		

		// Valida longitud de la contraseña
		if(trim($this->input->post('cliente-password')) != '' && strlen(trim($this->input->post('cliente-password'))) < 6)
		{
			$error->mensaje[] = 'Longitud mínima de la contraseña: 6 caracteres';
			$error->status = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('cliente-password')) != '' || trim($this->input->post('cliente-password2')) != '')
		{
			if(trim($this->input->post('cliente-password')) != trim($this->input->post('cliente-password2')))
			{
				$error->mensaje[] = 'Las contraseñas no coinciden';
				$error->status = FALSE;
			}
		}	

		return $error;
	} 		


	public function logout()
	{	
		$data = new stdClass();
      
		$data->rutaImagen = 'false';	

		// quita cookie en caso que el usuario haya seleccionado la opción 'recordarme'
		$domain = ($_SERVER['HTTP_HOST'] != 'localhost') ? $_SERVER['HTTP_HOST'] : false;

		delete_cookie('id_extreme', $domain);

		$_SESSION = array();  
        session_unset();
        session_destroy();  

		$this->load->template('redirect', $data);
	}		

}