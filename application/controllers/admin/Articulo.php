<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Articulo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/articulo_model','articulo');
		$this->load->model('Usuarios/localarticulo_model','local_articulo');	
		$this->load->model('Usuarios/local_model','local');		
		$this->load->model('ABM/animal_model','animal');		
		$this->load->model('ABM/rubro_model','rubro');		
		$this->load->model('ABM/marca_model','marca');		
		$this->load->model('ABM/presentacion_model','presentacion');		
		$this->load->model('ABM/tamanio_model','tamanio');
		$this->load->model('ABM/raza_model','raza');		
		$this->load->model('Tools/media_model','media');		
		$this->load->library('validations');
		$this->load->library('tools');		
		$this->load->helper('url');
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["animales"] = $this->animal->get_all();
		$datos_vista["rubros"] = $this->rubro->get_all();
		$datos_vista["marcas"] = $this->marca->get_all_by_animal_rubro_by_id();
		$datos_vista["marcas_select"] = $this->marca->get_all_by_animal_rubro();
		$datos_vista["presentaciones"] = $this->presentacion->get_all_by_rubro();
		$datos_vista["razas"] = $this->raza->get_all();
		$datos_vista["tamanios"] = $this->tamanio->get_all();

		$this->load->view('admin/ABM/articulo_view', $datos_vista);
	}


	// Listado de Artículos GeoTienda
	// Se llama desde Backend - Articulos GeoTienda
	public function ajax_list()
	{
		$list = $this->articulo->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $articulo) {
			$no++;
			$row = array();

			$row[] = $articulo->rubro;
			$row[] = $articulo->animal;			
			$row[] = $articulo->marca;
			$row[] = $articulo->nombre;
			$row[] = $articulo->codigo;

			$row[] = '<img src="'. IMG_PATH . 'articulos/miniaturas/'. $articulo->imagen .'" >';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar"    onclick="edit_articulo('."'".$articulo->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_articulo('."'".$articulo->nombre."'".',' .$articulo->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->articulo->count_all(),
						"recordsFiltered" => $this->articulo->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->articulo->get_by_id($id);

		// Convierte lista de presentaciones a string para select multiple de la vista
		$presentaciones_view = '';

		foreach ($data->presentaciones as $presentacion) {
			$presentaciones_view .= $presentacion->presentacion_id . ',';
		}

		$data->presentaciones_view = rtrim($presentaciones_view, ",");


		// Convierte lista de razas a string para select multiple de la vista
		$razas_view = '';

		foreach ($data->razas as $raza) {
			$razas_view .= $raza->raza_id . ',';
		}

		$data->razas_view = rtrim($razas_view, ",");	


        // Convierte lista de tamanios a string para select multiple de la vista
		$tamanios_view = '';

		foreach ($data->tamanios as $tamanio) {
			$tamanios_view .= $tamanio->tamanio_id . ',';
		}

		$data->tamanios_view = rtrim($tamanios_view, ",");		


        echo json_encode($data);
	}


	public function ajax_add()
	{	
		$imagen = '';
		$marca_id = null;
		$medicados = null;

		$this->_validate(true);
		

		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width');
			$big = $this->config->item('img_big_width');
			
			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'articulos');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		// Valida si es un artículo con marca
		if ($this->input->post('con_marca') == '1') $marca_id = $this->input->post('marca');

		// Medicados
		if ($this->input->post('medicados') != '') $medicados = $this->input->post('medicados');

		$data = array(
			'articulo_nombre' => $this->input->post('nombre'),
			'articulo_codigo' => $this->input->post('codigo'),
			'articulo_detalle' => $this->input->post('detalle'),
			'articulo_animal_id' => $this->input->post('animal'),	
			'articulo_rubro_id' => $this->input->post('rubro'),
			'articulo_imagen' => $imagen,
			'articulo_medicados' => $medicados,	
			'articulo_edad' => $this->input->post('edad'),	
			'articulo_marca_id' => $marca_id
				
		);

		$id_insert = $this->articulo->save($data);
         
	    // Para artículos sin presentación, asigna la presentación 'Sin Presentación' (presentacion_id = 1)
	    if ($this->input->post('con_presentacion') == '0')
	    {
	        $data_presentacion = array
	        (
	     	    'articulo_presentacion_articulo_id' => $id_insert,
				'articulo_presentacion_presentacion_id'=> 1,
	         );

	     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);	    	
	    }
	    else
	    {
	        // Inserta presentaciones asignadas al articulo
	        $data_presentaciones = $this->input->post('presentacion');
	         
	        for($i = 0; $i < count($data_presentaciones); $i++) {
	           
		        $data_presentacion = array
		        (
		     	    'articulo_presentacion_articulo_id' => $id_insert,
					'articulo_presentacion_presentacion_id'=> (int)$data_presentaciones[$i],
		         );

		     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);
		    }	    	
	    }         

        // Inserta razas asignadas al articulo
        $data_razas = $this->input->post('raza');
         
        for($i = 0; $i < count($data_razas); $i++) {
           
	        $data_raza = array(
	     	    'articulo_raza_articulo_id' => $id_insert,
				'articulo_raza_raza_id'=> (int)$data_razas[$i],
					
	         );

	     	$insert_raza =  $this->articulo->save_raza($data_raza);
	    }	    


     	// Inserta tamaños asignados al articulo
        $data_tamanios = $this->input->post('tamanio');
         
        for($i = 0; $i < count($data_tamanios); $i++) {
           
	        $data_tamanio= array(
	     	    'articulo_tamanio_articulo_id' => $id_insert,
				'articulo_tamanio_tamanio_id'=> (int)$data_tamanios[$i],
					
	         );

	     	$insert_tamanio =  $this->articulo->save_tamanio($data_tamanio);
	    }	    

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$marca_id = null;
		$medicados = null;

		$presentaciones_actuales = array();
		$presentaciones_nuevas = array();
		$presentaciones_agregadas = array();
		$presentaciones_no_borrar = array();
		$presentaciones_insertar = array();
		$presentaciones_utilizadas = array();
		$articulo_presentacion_list = array();
		$locales_update = array();
		$data_insert = array();
		$aviso_borrar_presentaciones = '';

		$this->_validate();

		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width');
			$big = $this->config->item('img_big_width');

			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'articulos');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		// Medicados
		if ($this->input->post('medicados') != '') $medicados = $this->input->post('medicados');

		// Valida si es un artículo con marca
		if ($this->input->post('con_marca') == '1') $marca_id = $this->input->post('marca');
		

		$data = array(
				'articulo_nombre' => $this->input->post('nombre'),
				'articulo_codigo' => $this->input->post('codigo'),
				'articulo_detalle' => $this->input->post('detalle'),
				'articulo_animal_id' => $this->input->post('animal'),		
				'articulo_rubro_id' => $this->input->post('rubro'),	
				'articulo_medicados' => $medicados,	
				'articulo_edad' => $this->input->post('edad'),				
				'articulo_marca_id' => $marca_id,	
				'articulo_imagen' => $imagen,			
			);

		$this->articulo->update(array('articulo_id' => $this->input->post('id')), $data);

		// Presentaciones
		if ($this->input->post('con_presentacion') == '1')
	    {
			$presentaciones_nuevas = $this->input->post('presentacion');

			// Recupera presentaciones actuales del articulo
			$presentaciones_actuales = $this->articulo->get_presentaciones_by_id($this->input->post('id'));

			// Presentaciones que no se modifican
			$presentaciones_no_borrar = array_intersect($presentaciones_actuales, $presentaciones_nuevas);   


			// Presentaciones agregadas
			$presentaciones_agregadas = array_diff($presentaciones_nuevas, $presentaciones_actuales); 

			// Busca presentaciones ya utilizadas (activas) en algun local y que están siendo quitadas del articulo
			$presentaciones_utilizadas = $this->local_articulo->get_presentaciones_articulo_cualquier_local($this->input->post('id'));
			$presentaciones_utilizadas = array_diff($presentaciones_utilizadas, $presentaciones_nuevas); 

	 		// Si existen, las agrega a $presentaciones_no_borrar y avisa
			if (count($presentaciones_utilizadas) > 0)
			{
				$aviso_borrar_presentaciones = 'true';
				$presentaciones_no_borrar = array_merge($presentaciones_no_borrar, $presentaciones_utilizadas);
			}

	        // Elimina presentaciones a borrar asignadas (pero no activas) en cualquier local
	        $this->local_articulo->delete_local_articulo_presentacion_by_id($this->input->post('id'), $presentaciones_no_borrar);

	        // Elimina presentaciones asignadas al articulo, excepto las no modificadas y las que estan asignadas a algun local
	        $this->articulo->delete_articulo_presentacion_by_id($this->input->post('id'), $presentaciones_no_borrar);

			// Inserta presentaciones asignadas al articulo, excepto las no modificadas y las que no pueden eliminarse
	        for($i = 0; $i < count($presentaciones_nuevas); $i++) 
	        {
	        	if (!in_array($presentaciones_nuevas[$i], $presentaciones_no_borrar))
	        	{
			        $data_presentacion = array(
			     	    'articulo_presentacion_articulo_id' => $this->input->post('id'),
						'articulo_presentacion_presentacion_id'=> (int)$presentaciones_nuevas[$i],
							
			         );

			     	$insert_presentacion =  $this->articulo->save_presentacion($data_presentacion);        		
	        	}
		    }


			if (count($presentaciones_agregadas) > 0)
			{
				// Recupera articulo_presentacion_id de las presentaciones agregadas
				$articulo_presentacion_list = $this->articulo->get_articulo_presentaciones_by_presentacion($this->input->post('id'), $presentaciones_agregadas);			

				// Busca locales que esten utilizando cualquier presentación del articulo, para asignarles las nuevas presentaciones (inactivas)
				$locales_update = $this->local_articulo->get_locales_presentaciones_articulo($this->input->post('id'));

				// Asigna presentaciones nuevas a los locales encontrados
				foreach ($locales_update as $local) 
				{
					foreach ($articulo_presentacion_list as $presentacion) 
					{
						$data_insert = array(
							'local_articulo_local_id' => $local,
							'local_articulo_articulo_presentacion_id' => $presentacion->articulo_presentacion_id,
							'local_articulo_precio' => 0,
							'local_articulo_precio_descuento' => 0,
							'local_articulo_descuento_id' => 5, // Sin Descuento
							'local_articulo_creador' => 'Local'
						);

						$this->local_articulo->save($data_insert);		
					}	    
				}					
			}
		}

        // Elimina razas asignadas al articulo
        $this->articulo->delete_articulo_raza_by_id($this->input->post('id'));

        // Inserta razas asignadas al articulo
        $data_razas = $this->input->post('raza');
         
        for($i = 0; $i < count($data_razas); $i++) {
           
	        $data_raza = array(
	     	    'articulo_raza_articulo_id' => $this->input->post('id'),
				'articulo_raza_raza_id'=> (int)$data_razas[$i],
					
	         );

	     	$insert_raza =  $this->articulo->save_raza($data_raza);
	    }

        // Elimina tamanios asignadas al articulo
        $this->articulo->delete_articulo_tamanio_by_id($this->input->post('id'));

	      // Inserta tamaños asignadas al articulo
        $data_tamanios= $this->input->post('tamanio');
         
        for($i = 0; $i < count($data_tamanios); $i++) {
           
	        $data_tamanio = array(
	     	    'articulo_tamanio_articulo_id' => $this->input->post('id'),
				'articulo_tamanio_tamanio_id'=> (int)$data_tamanios[$i],
					
	         );

	     	$insert_tamanio =  $this->articulo->save_tamanio($data_tamanio);
	    }

		echo json_encode(array(
			"status" => TRUE,
			"presentaciones_actuales" => $presentaciones_actuales,
			"presentaciones_no_borrar" => $presentaciones_no_borrar,
			"presentaciones_nuevas" => $presentaciones_nuevas,
			"presentaciones_insertar" => $presentaciones_insertar,
			"presentaciones_utilizadas" => $presentaciones_utilizadas,
			"presentaciones_agregadas" => $presentaciones_agregadas,
			"aviso_borrar_presentaciones" => $aviso_borrar_presentaciones,
			"locales_update" => $locales_update,
			"data_insert" => $data_insert,
			));
	}


	public function ajax_delete($id)
	{
		$data = array();
		$presentaciones_no_borrar= array();

		$resultado = $this->articulo->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		// Si no hubo error, sigue eliminando
		if (sizeof($presentaciones_no_borrar) == 0)
		{
	         // Elimina presentaciones asignadas al articulo
	         $this->articulo->delete_articulo_presentacion_by_id($id, []);	

	         // Elimina razas asignadas al articulo
	         $this->articulo->delete_articulo_raza_by_id($id);	   

	         // Elimina tamanios asignadas al articulo
	         $this->articulo->delete_articulo_tamanio_by_id($id);	      			
	     }

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('codigo')) == '')
		{
			$data['inputerror'][] = 'codigo';
			$data['error_string'][] = 'Ingrese un Código';
			$data['status'] = FALSE;
		}

		// Valida marca si es un artículo con marca
		if ($this->input->post('con_marca') == '1')
		{
			if(trim($this->input->post('marca')) == '')
			{
				$data['inputerror'][] = 'marca';
				$data['error_string'][] = 'Seleccione una marca';
				$data['status'] = FALSE;
			}			
		} 

		// Valida presentacion si es un artículo con presentacion
		if ($this->input->post('con_presentacion') == '1')
		{
			if($this->input->post('presentacion') == '')
			{
				$data['inputerror'][] = 'presentacion[]';
				$data['error_string'][] = 'Seleccione una presentacion';
				$data['status'] = FALSE;
			}			
		}

		// Valida que no exista un registro con el mismo nombre
		if ($add){

			// Valida si es un artículo con marca
			if ($this->input->post('con_marca') == '1')
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('nombre')), $this->input->post('animal'), $this->input->post('marca'));
			else
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('nombre')), $this->input->post('animal'), null);				
		}
		else
			// Valida si es un artículo con marca
			if ($this->input->post('con_marca') == '1')
				$duplicated = $this->articulo->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')), $this->input->post('animal'), $this->input->post('marca'));
			else
				$duplicated = $this->articulo->check_duplicated(trim($this->input->post('id'), $this->input->post('nombre')), $this->input->post('animal'), null);	

			
		if ($duplicated > 0)
		{
			if ($this->input->post('con_marca') == '1')
				$data['error_string'][] = 'Ya existe un Artículo con ese nombre, marca y animal';
			else
				$data['error_string'][] = 'Ya existe un Artículo con ese nombre y animal';

			$data['inputerror'][] = 'nombre';

			$data['status'] = FALSE;
		}

        if($this->input->post('animal') == '')
		{
			$data['inputerror'][] = 'animal';
			$data['error_string'][] = 'Seleccione un animal';
			$data['status'] = FALSE;
		}	

        if($this->input->post('rubro') == '')
		{
			$data['inputerror'][] = 'rubro';
			$data['error_string'][] = 'Seleccione un rubro';
			$data['status'] = FALSE;
		}					

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún artíulo
		$count = $this->articulo->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'articulos')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen de articulo.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);
	}


	/* ARTICULOS CREADOS POR LOS LOCALES */

	public function index_locales()
	{
		$datos_vista = array();

		$datos_vista["animales"] = $this->animal->get_all();
		$datos_vista["rubros"] = $this->rubro->get_all();
		$datos_vista["marcas"] = $this->marca->get_all_by_animal_rubro_by_id();
		$datos_vista["marcas_select"] = $this->marca->get_all_by_animal_rubro();
		$datos_vista["presentaciones"] = $this->presentacion->get_all();
		$datos_vista["razas"] = $this->raza->get_all();
		$datos_vista["tamanios"] = $this->tamanio->get_all();
		$datos_vista["locales"] = $this->local->get_all();

		$this->load->view('admin/ABM/articulo_locales_view', $datos_vista);
	}


	// Listado de Artículos creados por los locales
	// Se llama desde Backend - Articulos Locales
	public function ajax_list_locales()
	{
		$list = $this->articulo->get_datatables_locales();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $articulo) 
		{
			$no++;
			$row = array();

			$row[] = $articulo->local;
			$row[] = $articulo->rubro;
			$row[] = $articulo->animal;			
			$row[] = $articulo->marca;
			$row[] = $articulo->nombre;

			$row[] = '<img src="'. IMG_PATH . 'articulos/locales/miniaturas/'. $articulo->imagen .'" >';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Ver Detalles" onclick="ver_articulo_local('. $articulo->id .', ' . $articulo->local_id . ');">Detalles</a>';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->articulo->count_all_locales(),
						"recordsFiltered" => $this->articulo->count_filtered_locales(),
						"data" => $data,
				);

		echo json_encode($output);
	}

	/* fin ARTICULOS CREADOS POR LOS LOCALES */
}