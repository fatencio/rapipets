<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Valoracion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/valoracion_model','valoracion');
		$this->load->model('Actividad/notificacionlocal_model','notificacion_local');		
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');
	}


	public function index()
	{
		$this->load->helper('url');
		$this->load->view('admin/Actividad/valoracion_view');
	}


	public function ajax_list_pendientes()
	{

		$list = $this->valoracion->get_datatables_pendientes();

		$datatable = array();
		$no = $_POST['start'];

		foreach ($list as $valoracion) {
			$no++;
			$row = array();
			$row_valoracion = '';


			if ($valoracion->tipo == 'venta'){

				$row[] = '<a class="btn btn-xs btn-venta" href="javascript:void(0)" title="Detalles Venta" onclick="detalle_venta('."'".$valoracion->id."'".')">&nbsp;&nbsp;Venta&nbsp;&nbsp;' . $valoracion->numero . '&nbsp;</a>'; 
			}
			else{

				$row[] = '<a class="btn btn-xs btn-success" href="javascript:void(0)" title="Detalles Turno" onclick="detalle_turno('."'".$valoracion->id."'".')">&nbsp;&nbsp;Turno&nbsp;&nbsp;' . $valoracion->numero . '&nbsp;</a>'; 
			}

			$row[] = $valoracion->cliente;			
			$row[] = $valoracion->local;

			$row_puntaje = '';
			$puntaje = $valoracion->puntaje_cliente;

			$row_puntaje = '<div class="pull-left push-5-t force-left-60 force-top-10" style="width: 125px">';

			for($x=1; $x<=5; $x++)
			{					
				if($x <= $puntaje)
					$row_puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
				else
					$row_puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
			}				

			$row_puntaje .= '</div>';


			$row[] = $row_puntaje;

			$row[] = $valoracion->valoracion_cliente;

			//add html for action
			$row[] = '<a class="btn btn-success" href="javascript:void(0)" title="Aprobar" onclick="aprobar_comentario('."'".$valoracion->id."'". ",'".$valoracion->tipo."'". ",'si'". ')"><i class="si si-check"></i></a>
				<a class="btn btn-danger" href="javascript:void(0)" title="Rechazar" onclick="aprobar_comentario('."'".$valoracion->id."'". ",'".$valoracion->tipo."'". ",'no'". ')"><i class="si si-close"></i></a>';	
			$datatable[] = $row;
		}

		$output = array(

			"draw" => $_POST['draw'],
		
			"recordsTotal" => $this->valoracion->count_all_pendientes(),
			"recordsFiltered" => $this->valoracion->count_filtered_pendientes(),
		
			"data" => $datatable
		);


		echo json_encode($output);
	}


	public function ajax_aprobar_comentario($id, $tipo, $aprobada)
	{
		// Fecha y hora local
		$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

		// Venta
		if ($tipo == 'venta')
		{
			$data = array(
					'venta_valoracion_aprobada' => $aprobada,
				);

			$this->venta->update(array('venta_id' => $id), $data);

			// Si el comentario fue aprobado, notifica al local
			if ($aprobada == 'si')
			{
				// Recupera datos de la venta
				$venta = $this->venta->get_venta_by_id($id);

				// Notifica al Local
				$data_notificacion_local = array(
					'notificacion_local_fecha' => $ahora->format('Y-m-d H:i:s'),
					'notificacion_local_venta_id' => $venta->id,
					'notificacion_local_cliente_id' => $venta->cliente_id,			
					'notificacion_local_local_id' => $venta->local_id,
					'notificacion_local_texto' => '<b>' . $venta->cliente . '</b> calificó una venta',
					'notificacion_local_tipo' => 'calificación venta'
				);		

				$this->notificacion_local->save($data_notificacion_local);					
			}
		}

		// Turno
		else
		{
			$data = array(
					'turno_valoracion_aprobada' => $aprobada,
				);

			$this->turno->update(array('turno_id' => $id), $data);

			// Si el comentario fue aprobado, notifica al local
			if ($aprobada == 'si')
			{
				// Recupera datos del turno
				$turno = $this->turno->get_by_id($id);

				// Notifica al Local
				$data_notificacion_local = array(
					'notificacion_local_fecha' => $ahora->format('Y-m-d H:i:s'),
					'notificacion_local_turno_id' => $id,
					'notificacion_local_cliente_id' => $turno->cliente_id,			
					'notificacion_local_local_id' => $turno->local_id,
					'notificacion_local_texto' => '<b>' . $turno->cliente . '</b> calificó un servicio',
					'notificacion_local_tipo' => 'calificación turno'
				);		

				$this->notificacion_local->save($data_notificacion_local);	
			}
						
		}

		echo json_encode(array("status" => TRUE));
	}

}