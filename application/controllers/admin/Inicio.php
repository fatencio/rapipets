<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		
		$this->load->model('Usuarios/usuario_model','usuario');
		$this->load->helper('url');			
		$this->load->helper('form');
		$this->load->library('form_validation');		
	}


	function index(){

		$data = new stdClass();

		// Valida que el usuario este loggeado
		if(!isset($_SESSION['logged_in'])){			
			
			if(!isset($_POST['login-username'])){
				
				$data->login_error = false;

				$this->load->view('admin/login', $data);
					
			}else{
					
				$username = $this->input->post('login-username');
				$password = $this->input->post('login-password');
				
				// login ok				
				if ($this->usuario->resolve_user_login($username, $password)) {
					
					$user_id = $this->usuario->get_user_id_from_username($username);
					$user    = $this->usuario->get_user($user_id);
					
					// inicializa sesion del usuario
					$_SESSION['user_id']      = (int)$user->id;
					$_SESSION['username']     = (string)$user->username;
					$_SESSION['logged_in']    = (bool)true;
					$_SESSION['is_confirmed'] = (bool)$user->is_confirmed;
					$_SESSION['is_admin']     = (bool)$user->is_admin;
					
					$this->inicio();
				
				// fallo el login	
				} else {
					
					$data->login_error = true;
					
					$this->load->view('admin/login', $data);
					
				}
			}	
		}
		else{
		
			$this->inicio();
		}		
	}


	function inicio(){
	
		$data = new stdClass();
		
		//Asigno el nombre de usuario para mostrarlo en pantalla
		$data->usuario = $_SESSION['username'];

		//Vista (container principal)
		$this->load->view('admin/index', $data);
	}	


	function logout(){
		
		$data = new stdClass();

		$_SESSION = array();  
        session_unset();
        session_destroy();  

        $this->load->view('admin/redirect', $data);
	}	

}
?>