<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Marca extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/marca_model','marca');
		$this->load->model('ABM/animal_model','animal');
		$this->load->model('ABM/rubro_model','rubro');
		$this->load->library('validations');

		$this->load->helper('url');		
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["animales"] = $this->animal->get_all();
		$datos_vista["rubros"] = $this->rubro->get_all();

		$this->load->view('admin/ABM/marca_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->marca->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $marca) {
			$no++;
			$row = array();

			$row[] = $marca->nombre;
			$row[] = $marca->rubros;
			$row[] = $marca->animales;
			

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_marca('."'".$marca->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_marca('."'".$marca->nombre."'".',' .$marca->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->marca->count_all(),
						"recordsFiltered" => $this->marca->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->marca->get_by_id($id);

		// Convierte lista de animales a string para select multiple de la vista
		$animales_list = '';

		foreach ($data->animales as $animal) {
			$animales_list .= $animal->animal_id . ',';
		}

		$data->animales_list = rtrim($animales_list, ",");

		// Convierte lista de rubros a string para select multiple de la vista
		$rubros_list = '';

		foreach ($data->rubros as $rubro) {
			$rubros_list .= $rubro->rubro_id . ',';
		}

		$data->rubros_list = rtrim($rubros_list, ",");		

		echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		 $data = array(
				'marca_nombre' => $this->input->post('nombre'),
		 	);

         $id_insert = $this->marca->save($data);
         
         $data_animales = $this->input->post('animal_asignado');
         $data_rubros = $this->input->post('rubro_asignado');
         
         // Rubros y animales asociados a la marca
         for($i = 0; $i < count($data_animales); $i++) 
         {
            $data_animal = array(
         	    'marca_animal_marca_id' => $id_insert,
				'marca_animal_animal_id'=> (int)$data_animales[$i],
					
             );

         	$insert_animal =  $this->marca->save_animal($data_animal);     	        
         }

         for($i = 0; $i < count($data_rubros); $i++) 
         {
           $data_rubro = array(
         	    'marca_rubro_marca_id' => $id_insert,
				'marca_rubro_rubro_id'=> (int)$data_rubros[$i],
					
             );

         	$insert_rubro =  $this->marca->save_rubro($data_rubro);             
         }

      
         echo json_encode(array("status" => TRUE));
	
	}


	public function ajax_update()
	{
		 $this->_validate();

		 $data = array(
				'marca_nombre' => $this->input->post('nombre'),
		 	);

         $this->marca->update(array('marca_id' => $this->input->post('id')), $data);
         
         // Elimina animales y rubros asignados a la marca
         $this->marca->delete_marca_animal_by_id($this->input->post('id'));
         $this->marca->delete_marca_rubro_by_id($this->input->post('id'));


         // Inserta animales y rubros asignados a la marca 
         $data_animales = $this->input->post('animal_asignado');
         $data_rubros = $this->input->post('rubro_asignado');
         
         for($i = 0; $i < count($data_animales); $i++) 
         {
            $data_animal = array(
     	        'marca_animal_marca_id' => $this->input->post('id'),
				'marca_animal_animal_id'=> (int)$data_animales[$i],
				
            );

            $insert_animal =  $this->marca->save_animal($data_animal);        
         }
      
		for($i = 0; $i < count($data_rubros); $i++) 
         {
           $data_rubro = array(
         	    'marca_rubro_marca_id' => $this->input->post('id'),
				'marca_rubro_rubro_id'=> (int)$data_rubros[$i],
					
             );

         	$insert_rubro =  $this->marca->save_rubro($data_rubro);             
         }

         echo json_encode(array("status" => TRUE));

	}


	public function ajax_delete($id)
	{
		$data = array();
		$resultado = "";

		$resultado = $this->marca->delete_by_id($id);

        $data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}		

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->marca->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->marca->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe una Marca con ese nombre';
			$data['status'] = FALSE;
		}	


		// TODO: averiguar si valida animal 

	//	$data_animales = $this->input->post('animal_asignado');
		/*
        if(true) //!isset($data_animales[1]))
		{
			$data['inputerror'][] = 'animal_asignado';
			$data['error_string'][] = 'Seleccione un animal';
			$data['status'] = FALSE;
		}		
		*/

      
        //if (empty($_POST["animal_asignado"])) {

      
		if($this->input->post('animal_asignado') == ''){
		
			$data['inputerror'][] = 'animal_asignado[]';
			$data['error_string'][] = 'Seleccione un animal';
			$data['status'] = FALSE;
		}		

		if($this->input->post('rubro_asignado') == ''){
		
			$data['inputerror'][] = 'rubro_asignado[]';
			$data['error_string'][] = 'Seleccione un rubro';
			$data['status'] = FALSE;
		}
       

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}