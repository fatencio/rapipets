<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Usuarios/local_model','local');
		$this->load->model('Usuarios/frontend_model','frontend_user');				
		$this->load->model('Usuarios/localarticulo_model','local_articulo');	
		$this->load->model('Usuarios/localservicioitem_model','local_servicio_item');
		$this->load->model('Usuarios/localhorario_model','local_horario');
		$this->load->model('Usuarios/localmediopago_model','local_medio_pago');				
		$this->load->model('Tools/provincia_model','provincia');
		$this->load->model('Tools/configuracion_model','configuracion');		
		$this->load->model('Tools/media_model','media');			
		$this->load->model('ABM/descuento_model','descuento');
		$this->load->model('ABM/banner_model','banner');
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/ventacomentario_model','venta_comentario');		
		$this->load->model('Actividad/turnocomentario_model','turno_comentario');		

		$this->load->library('validations');
		$this->load->library('email_geotienda');		
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista["provincias"] = $this->provincia->get_all();
		$datos_vista["descuentos"] = $this->descuento->get_all();
		$datos_vista["banners"] = $this->banner->get_all();
		$datos_vista["categorias"] = $this->local->get_all_categorias();

		$this->load->view('admin/Usuarios/local_view', $datos_vista);
	}


	public function ajax_list()
	{
		$destacado = '';

		$list = $this->local->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $local) {

			$checked = $local->status == 1 ? "checked": "";
			$status = $local->status == 1 ? 0:1;
			$destacado = $local->destacado == 1 ? '<i class="si si-arrow-up text-success"></i>' : '';

			$no++;
			$row = array();

			$row[] = '<input type="checkbox" name="chk_locales[]" id="chk_locales" class="chk_locales" value=" ' . $local->id . '">';

			$row[] = $local->categoria;
			$row[] = $destacado . ' <b>' . $local->nombre . '</b>';
            $row[] = $local->titular;
            $row[] = $local->telefono;
            $row[] = $local->email;
            $row[] = '<img src="'. IMG_PATH . 'local/miniaturas/'. $local->avatar .'" width="120">';

			
			//add html for action
			$row[] = '<a class="btn btn-md btn-primary btn-tabla" href="javascript:void(0)" title="Ver Artículos"    onclick="articulos_local('."'".$local->nombre."'".',' .$local->id. ')"><i class="si si-handbag"></i> &nbsp;&nbsp;artículos&nbsp;&nbsp;</a>
				<br/>

				<a class="btn btn-md btn-primary btn-tabla" href="javascript:void(0)" title="Configurar banner"    onclick="banner_local(' .$local->id. ')"><i class="si si-flag"></i> &nbsp;&nbsp;banner&nbsp;&nbsp;</a>
				<br/>

				<a class="btn btn-md btn-primary btn-tabla" target="_blank" href="https://www.google.com.ar/maps/place/' . $local->latitud . ',' . $local->longitud . '" title="Mapa"> <i class="glyphicon glyphicon-map-marker"></i> &nbsp;&nbsp;&nbsp;ver mapa&nbsp;&nbsp;</a>

				<a class="btn btn-md btn-primary btn-tabla" id="btnMail_bienvenida_' .$local->id. '" href="javascript:void(0)" title="Enviar mail de bienvenida" onclick="mail_bienvenida('."'".$local->id."'".')"> <i class="si si-envelope"></i> &nbsp;bienvenida&nbsp;&nbsp;</a>	

				<br/>
				<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Modificar Password"    onclick="edit_password('."'".$local->id."'".')"><i class="fa fa-key"></i></a>
				
				<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_local('."'".$local->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>

				<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_local('."'".$local->nombre."'".',' .$local->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;

			 	<label class="css-input switch switch-sm switch-info" title="Activar / Desactivar">
			 		<input type="checkbox" name="switch_local"' . $checked . ' onclick="switch_local('. $local->id .')"><span></span>
             	</label>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->local->count_all(),
						"recordsFiltered" => $this->local->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = new stdClass();

		$data = $this->local->get_by_id($id);

	//	$data->medios_pago = $this->local_mediopago_item->get_medio_pago_items_local($id);

		echo json_encode($data);
	}


	public function ajax_banner_activo($id){

		$data = new stdClass();

		// Recupera banner del local
		$banner_local = $this->local->get_banner_local($id);
		$data->local = $banner_local;

		// Recupera banner grupal asignado al local
		$banner_grupal = $this->banner->get_by_local($id);
		$data->grupal = $banner_grupal;		

		// Recupera banner global
		$banner_global = $this->banner->get_global();
		$data->global = $banner_global;	

		echo json_encode($data);
	}


	public function ajax_nivel_banner(){

		$data = new stdClass();	

		// Recupera banner global
		$banner_global = $this->banner->get_global();

		if ($banner_global) 
			$data->nivel = 'global';
		else
			$data->nivel = 'grupal';

		echo json_encode($data);
	}


	public function ajax_activar_desactivar($id)
	{
		if (isset($_SESSION['username']))
		{	
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$nuevo_status = 0;
			$local = $this->local->get_by_id($id);

			if($local->status == 0) $nuevo_status = 1;

			if ($nuevo_status == 0)
			{
				$data = array(
					'local_status' => $nuevo_status,

					// Audit trail
					'local_fecha_suspension' => $ahora->format('Y-m-d H:i:s'),
					'local_usuario_suspension' => $_SESSION['username']			
				);	
			}
			else
			{
				$data = array(
					'local_status' => $nuevo_status,

					// Audit trail
					'local_fecha_habilitacion' => $ahora->format('Y-m-d H:i:s'),
					'local_usuario_habilitacion' => $_SESSION['username']			
				);		
			}

			$this->local->update(array('local_id' => $id), $data);

			echo json_encode(array("status" => TRUE, "nuevo_status" => $nuevo_status));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	public function ajax_add()
	{
		if (isset($_SESSION['username']))
		{		
			$data = array();
			$data['status'] = TRUE;
			$imagen = '';
			
			$this->_validate(true);
			
			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'local');
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));

			$data_insert = array(
				'local_categoria' => $this->input->post('categoria'),
				'local_nombre' => $this->input->post('nombre'),
				'local_nombre_titular' => $this->input->post('nombre_titular'),
				'local_apellido_titular' => $this->input->post('apellido_titular'),
				'local_telefono' => $this->input->post('telefono'),
				'local_email' => $this->input->post('email'),
				'local_telefono2' => $this->input->post('telefono2'),
				'local_email2' => $this->input->post('email2'),				
				'local_direccion' => $this->input->post('direccion'),
				'local_localidad' => $this->input->post('localidad'),
	            'local_horario_atencion' => $this->input->post('horario_atencion'),
				'local_horario_sabados'=> $this->input->post('horario_sabados'),
				'local_horario_domingos'=>$this->input->post('horario_domingos'),
				'local_envio_domicilio'=>$this->input->post('envio_domicilio'),		
				'local_costo_envio'=>$this->input->post('costo_envio'),
				'local_zona_entrega'=>$this->input->post('zona_entrega'),
				'local_dias_entrega'=> $this->input->post('dias_entrega'),
				'local_provincia_id' => $this->input->post('provincia'),
				'local_avatar' => $imagen,
				'local_latitud' => $this->input->post('latitud'),
				'local_longitud' => $this->input->post('longitud'),
				'local_destacado' => $this->input->post('destacado'),			
				'local_urgencias' => $this->input->post('urgencias'),			
				'local_telefono_urgencias' => $this->input->post('telefono_urgencias'),			
				'local_urgencias_consultorio' => $this->input->post('urgencias_consultorio'),			
				'local_urgencias_domicilio' => $this->input->post('urgencias_domicilio'),	

				// Audit trail
				'local_fecha_alta' => $ahora->format('Y-m-d H:i:s'),
				'local_usuario_alta' => $_SESSION['username']					
			);

			$local_id = $this->local->save($data_insert);

			// Password inicial es el timestap de creacion del local
			$local = $this->local->get_by_id($local_id);
			$data_update = array('local_password' => strtotime($local->fecha_creacion));

			$this->local->update_password(array('local_id' => $local_id), $data_update);

			// Medios de Pago del local
			foreach ($_POST as $name => $value) 
			{
				if (substr($name, 0, 12) == 'chkMediopago')
				{
					$medios_pago_array = explode('_', $name);

					$data_insert = array(
						'local_medio_pago_medio_pago_id' => $medios_pago_array[1],
						'local_medio_pago_medio_pago_item_id' => $medios_pago_array[2],
						'local_medio_pago_local_id' => $local_id
					);

					$this->local_medio_pago->save($data_insert);							
				}
			}

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	// Envía mail de bienvenida 
	public function ajax_mail_bienvenida($id){

		$data = array();
		$data['status'] = TRUE;

		// Recupera datos del local
		$local = $this->local->get_by_id($id);

		$msg_mail = $this->email_geotienda->nuevo_local(
			$local->email, 					   
			strtotime($local->fecha_creacion));

		if ($msg_mail != '')							
		{
			$data['status'] = FALSE;
		}

		echo json_encode($data);		
	}


	public function ajax_get_precio_destacado()
	{
		$data = new stdClass();	

		$data->precio = $this->configuracion->get_value('local_precio_destacado');

		echo json_encode($data);
	}


	public function ajax_actualizar_precio_destacado($precio = "")
	{
		$this->_validate_precio_destacado($precio);		

		$data_update = array(
				'configuracion_valor' => $precio
			);

		$this->configuracion->update(array('configuracion_item' => 'local_precio_destacado'), $data_update);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		if (isset($_SESSION['username']))
		{		
			$this->_validate();

			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');

				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'local');
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}		

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$data = array(
				'local_categoria' => $this->input->post('categoria'),
				'local_nombre' => $this->input->post('nombre'),
				'local_nombre_titular' => $this->input->post('nombre_titular'),
				'local_apellido_titular' => $this->input->post('apellido_titular'),
				'local_telefono' => $this->input->post('telefono'),
				'local_email' => $this->input->post('email'),
				'local_telefono2' => $this->input->post('telefono2'),
				'local_email2' => $this->input->post('email2'),						
				'local_direccion' => $this->input->post('direccion'),
				'local_localidad' => $this->input->post('localidad'),
				'local_horario_atencion' => $this->input->post('horario_atencion'),
				'local_horario_sabados'=> $this->input->post('horario_sabados'),
				'local_horario_domingos'=>$this->input->post('horario_domingos'),
				'local_envio_domicilio'=>$this->input->post('envio_domicilio'),			
				'local_costo_envio'=>$this->input->post('costo_envio'),			
				'local_zona_entrega'=>$this->input->post('zona_entrega'),
				'local_dias_entrega'=> $this->input->post('dias_entrega'),
				'local_provincia_id' => $this->input->post('provincia'),
				'local_avatar' => $imagen,
				'local_latitud' => $this->input->post('latitud'),
				'local_longitud' => $this->input->post('longitud'),
				'local_destacado' => $this->input->post('destacado'),
				'local_urgencias' => $this->input->post('urgencias'),
				'local_telefono_urgencias' => $this->input->post('telefono_urgencias'),
				'local_urgencias_consultorio' => $this->input->post('urgencias_consultorio'),			
				'local_urgencias_domicilio' => $this->input->post('urgencias_domicilio'),	

				// Audit trail
				'local_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
				'local_usuario_modificacion' => $_SESSION['username']										
			);

			$this->local->update(array('local_id' => $this->input->post('id')), $data);

			// Medios de Pago del local
	        $this->local_medio_pago->delete_by_local($this->input->post('id'));

			foreach ($_POST as $name => $value) 
			{
				if (substr($name, 0, 12) == 'chkMediopago')
				{
					$medios_pago_array = explode('_', $name);

					$data_insert = array(
						'local_medio_pago_medio_pago_id' => $medios_pago_array[1],
						'local_medio_pago_medio_pago_item_id' => $medios_pago_array[2],
						'local_medio_pago_local_id' => $this->input->post('id')
					);

					$this->local_medio_pago->save($data_insert);							
				}
			}		

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	public function ajax_update_banner()
	{
		$this->_validate_banner();		

		$data = array(
			'local_texto_banner'=>$this->input->post('texto_banner'),
			'local_fondo_banner'=>$this->input->post('fondo_banner'),
			'local_color_banner'=>$this->input->post('color_banner'),				

		);

		$this->local->update(array('local_id' => $this->input->post('banner_local_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_banner_grupal()
	{
		$i = 0;

		foreach ($this->input->post('locales') as $local_id) {

			$data = array(
				'local_banner_id' => $this->input->post('banner_id'),
			);

			$this->local->update(array('local_id' => $local_id), $data);		
			$i++;
		}

		// Recupera datos del banner asignado
		$data = $this->banner->get_by_id($this->input->post('banner_id'));

		echo json_encode($data);
	}


	public function ajax_quitar_banner_grupal()
	{
		$data = array("status" => TRUE);

		$i = 0;

		foreach ($this->input->post('locales') as $local_id) {

			$data = array(
				'local_banner_id' => null,
			);

			$this->local->update(array('local_id' => $local_id), $data);	

			// Recupera datos del banner del local 
			// (para el caso de quitar la asignación a un local en particular)
			if ($i == 0)
				$data = $this->local->get_banner_local($local_id);

			$i++;
		}

		echo json_encode($data);
	}


	public function ajax_update_password()
	{
		$this->_validate_password();

		$data = array(
				'local_password' => $this->input->post('password'),

			);

		$this->local->update_password(array('local_id' => $this->input->post('local_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	// Elimina Local solo si no tiene Ventas o Turnos asociados
	public function ajax_delete($id)
	{
		if (isset($_SESSION['username']))
		{
	        $data = array();

	        // Valida si hay VENTAS del local
	        $ventas = $this->local->count_ventas($id);

	        // Valida si hay TURNOS del local
	        $turnos = $this->local->count_turnos($id);

	        if ($ventas == 0 && $turnos == 0)
	        {
				$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

				$dataDelete = array(

						// Audit trail
						'local_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
						'local_usuario_baja' => $_SESSION['username']				
					);

				$this->db->trans_begin();

				$this->local->update(array('local_id' => $id), $dataDelete);

				// Elimina entidades asociadas (horarios, articulos, medios de pago, items de servicios)
				$this->local_horario->delete_by_local($id);
				$this->local_articulo->delete_by_local($id);
				$this->local_medio_pago->delete_by_local($id);
				$this->local_servicio_item->delete_by_local($id);

				$this->db->trans_commit();

				$data['status'] = true;
	        }
	        else
	        {
	        	$data['status'] = false;
	        	$data['mensaje'] = "No es posible eliminar este elemento ya que está siendo utilizado.";
	        }

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	// Elimina Local junto con Ventas o Turnos asociados
	public function ajax_delete_full($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

	        $ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$dataDelete = array(
					// Audit trail
					'local_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
					'local_usuario_baja' => $_SESSION['username']				
				);

	        $this->db->trans_begin();

	        // Elimina VENTAS y TURNOS del local
	      	$this->venta_comentario->delete_by_local($id);
	      	$this->venta->delete_detalle_by_local($id);
	        $this->venta->delete_by_local($id);
	        
	      	$this->turno_comentario->delete_by_local($id);
	        $this->turno->delete_by_local($id);

	        // Elimina local
			$this->local->update(array('local_id' => $id), $dataDelete);

			$this->db->trans_commit();

			$data['status'] = true;

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('categoria')) == '')
		{
			$data['inputerror'][] = 'categoria';
			$data['error_string'][] = 'Seleccione una Categoría';
			$data['status'] = FALSE;
		}

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre de Local';
			$data['status'] = FALSE;
		}

		if(trim($this->input->post('nombre_titular')) == '')
		{
			$data['inputerror'][] = 'nombre_titular';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

	    if(trim($this->input->post('apellido_titular')) == '')
		{
			$data['inputerror'][] = 'apellido_titular';
			$data['error_string'][] = 'Ingrese un Apellido';
			$data['status'] = FALSE;
		}
		
		if(trim($this->input->post('email')) == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail';
			$data['status'] = FALSE;
		}		

		if(trim($this->input->post('telefono')) == '')
		{
			$data['inputerror'][] = 'telefono';
			$data['error_string'][] = 'Ingrese un Teléfono';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('direccion')) == '')
		{
			$data['inputerror'][] = 'direccion';
			$data['error_string'][] = 'Ingrese una Dirección';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('localidad')) == '')
		{
			$data['inputerror'][] = 'localidad';
			$data['error_string'][] = 'Ingrese una Localidad';
			$data['status'] = FALSE;
		}													

		if(trim($this->input->post('provincia')) == '0')
		{
			$data['inputerror'][] = 'provincia';
			$data['error_string'][] = 'Seleccione una Provincia';
			$data['status'] = FALSE;
		}	

		// Valida formato email
		if(!filter_var(trim($this->input->post('email')), FILTER_VALIDATE_EMAIL))
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail válido';
			$data['status'] = FALSE;
		}	

		// Para un usuario nuevo, valida contraseña
		/*
		if ($add)
		{
			if(trim($this->input->post('password')) == '')
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Ingrese una contraseña';
				$data['status'] = FALSE;
			}	

			if(trim($this->input->post('password2')) == '')
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Ingrese nuevamente la contraseña';
				$data['status'] = FALSE;
			}	

			// Valida longitud de la contraseña
			if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
				$data['status'] = FALSE;
			}

			// Valida contraseñas iguales
			if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
			{
				if(trim($this->input->post('password')) != trim($this->input->post('password2')))
				{
					$data['inputerror'][] = 'password2';
					$data['error_string'][] = 'Las contraseñas no coinciden';
					$data['status'] = FALSE;
				}
			}	
		}
		*/

		// Valida que no exista una entidad con el mismo email (Cliente o Local)
		if ($add)
			$duplicated = $this->frontend_user->check_duplicated(trim($this->input->post('email')));
		else
			$duplicated = $this->frontend_user->check_duplicated_edit($this->input->post('id'), trim($this->input->post('email')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ya existe un usuario con ese e-mail';
			$data['status'] = FALSE;
		}	


        if(trim($this->input->post('horario_atencion')) == '')
		{
		
			$data['inputerror'][] = 'horario_atencion';
			$data['error_string'][] = 'Ingrese un Horario de Atención';
			$data['status'] = FALSE;
		}

		// Para locales con envio a domicilio, valida datos de envio
		if ($this->input->post('envio_domicilio') == '1')
		{
	        if(trim($this->input->post('costo_envio')) == '')
			{
				$data['inputerror'][] = 'costo_envio';
				$data['error_string'][] = 'Ingrese un Costo de Envío';
				$data['status'] = FALSE;
			}

	        if(trim($this->input->post('zona_entrega')) == '')
			{
				$data['inputerror'][] = 'zona_entrega';
				$data['error_string'][] = 'Ingrese una Zona de Entrega';
				$data['status'] = FALSE;
			}

	        if(trim($this->input->post('dias_entrega')) == '')
			{
				$data['inputerror'][] = 'dias_entrega';
				$data['error_string'][] = 'Ingrese Días de Entrega';
				$data['status'] = FALSE;
			}
		}

		// Para locales con servicio de urgecias, valida telefono y modalidad de urgencias
		if ($this->input->post('urgencias') == '1')
		{
	        if(trim($this->input->post('telefono_urgencias')) == '')
			{
				$data['inputerror'][] = 'telefono_urgencias';
				$data['error_string'][] = 'Ingrese un Teléfono para Urgencias';
				$data['status'] = FALSE;
			}		
		}		

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	private function _validate_password()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		if(trim($this->input->post('password')) == '')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Ingrese nueva contraseña';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('password2')) == '')
		{
			$data['inputerror'][] = 'password2';
			$data['error_string'][] = 'Repita la nueva contraseña';
			$data['status'] = FALSE;
		}	

		// Valida longitud de la contraseña
		if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
			$data['status'] = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
		{
			if(trim($this->input->post('password')) != trim($this->input->post('password2')))
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Las contraseñas no coinciden';
				$data['status'] = FALSE;
			}
		}	


		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	private function _validate_banner()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		/* Se quita para permitir 'eliminar' el banner asignado al local 
		if(trim($this->input->post('texto_banner')) == '')
		{
			$data['inputerror'][] = 'texto_banner';
			$data['error_string'][] = 'Ingrese un texto';
			$data['status'] = FALSE;
		}	
		*/

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	private function _validate_precio_destacado($precio)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		if(trim($precio) == '')
		{
			$data['inputerror'][] = 'precio_local_destacado';
			$data['error_string'][] = 'Ingrese un precio';
			$data['status'] = FALSE;
		}	

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún local
		$count = $this->local->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'local')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen de local.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);

	}		
}