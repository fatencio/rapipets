<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Presentacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/presentacion_model','presentacion');
		$this->load->model('ABM/rubro_model','rubro');
		$this->load->library('validations');
		$this->load->helper('url');
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["rubros"] = $this->rubro->get_all();

		$this->load->view('admin/ABM/presentacion_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->presentacion->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $presentacion) 
		{
			$row = array();

			$row[] = $presentacion->nombre;
			$row[] = $presentacion->rubros;

				$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar"    onclick="edit_presentacion('."'".$presentacion->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_presentacion('."'".$presentacion->nombre."'".',' .$presentacion->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->presentacion->count_all(),
						"recordsFiltered" => $this->presentacion->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->presentacion->get_by_id($id);

		// Convierte lista de rubros a string para select multiple de la vista
		$rubros_list = '';

		foreach ($data->rubros as $rubro) {
			$rubros_list .= $rubro->rubro_id . ',';
		}

		$data->rubros_list = rtrim($rubros_list, ",");		

        echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'presentacion_nombre' => $this->input->post('nombre'),	
			);

		$insert = $this->presentacion->save($data);

         $data_rubros = $this->input->post('rubro_asignado');
         
         // Rubros asociados a la presentación
         for($i = 0; $i < count($data_rubros); $i++) 
         {
           $data_rubro = array(
         	    'presentacion_rubro_presentacion_id' => $insert,
				'presentacion_rubro_rubro_id'=> (int)$data_rubros[$i],
					
             );

         	$insert_rubro =  $this->presentacion->save_rubro($data_rubro);             
         }


		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'presentacion_nombre' => $this->input->post('nombre'),
				
			);

		$this->presentacion->update(array('presentacion_id' => $this->input->post('id')), $data);

         // Elimina rubros asignados a la presentación
         $this->presentacion->delete_presentacion_rubro_by_id($this->input->post('id'));


         // Inserta rubros asignados a la presentación
         $data_rubros = $this->input->post('rubro_asignado');
         
		for($i = 0; $i < count($data_rubros); $i++) 
         {
           $data_rubro = array(
         	    'presentacion_rubro_presentacion_id' => $this->input->post('id'),
				'presentacion_rubro_rubro_id'=> (int)$data_rubros[$i],
					
             );

         	$insert_rubro =  $this->presentacion->save_rubro($data_rubro);             
         }		

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$data = array ();
		$resultado = $this->presentacion->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}		

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->presentacion->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->presentacion->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe una Presentación con ese nombre';
			$data['status'] = FALSE;
		}	

		if($this->input->post('rubro_asignado') == ''){
		
			$data['inputerror'][] = 'rubro_asignado[]';
			$data['error_string'][] = 'Seleccione un rubro';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}