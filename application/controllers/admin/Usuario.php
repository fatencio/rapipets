<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Usuarios/usuario_model','usuario');

		$this->load->helper('form');
		$this->load->library('form_validation');		
	}


	public function index()
	{
		$this->load->helper('url');
		$this->load->view('admin/Usuarios/usuario_view');
	}


	public function ajax_list()
	{
		$list = $this->usuario->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $usuario) {
			$no++;
			$row = array();

			$row[] = $usuario->username;
			$row[] = $usuario->nombre;
			$row[] = $usuario->apellido;
			$row[] = $usuario->email;

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Modificar Password"    onclick="edit_password('."'".$usuario->id."'".')"><i class="fa fa-key"></i></a>
				<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar"    onclick="edit_usuario('."'".$usuario->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_usuario('."'".$usuario->nombre."'".',' .$usuario->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->usuario->count_all(),
						"recordsFiltered" => $this->usuario->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->usuario->get_by_id($id);

        echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'username' => $this->input->post('username'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'password' => $this->input->post('password'),
				'email' => $this->input->post('email'),
				'created_at' => date('Y-m-j H:i:s'),
			);

		$insert = $this->usuario->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'username' => $this->input->post('username'),
				'nombre' => $this->input->post('nombre'),
				'apellido' => $this->input->post('apellido'),
				'email' => $this->input->post('email'),
				'updated_at' => date('Y-m-j H:i:s'),
			);

		$this->usuario->update(array('id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update_password()
	{
		$this->_validate_password();

		$data = array(
				'password' => $this->input->post('password'),
				'updated_at' => date('Y-m-j H:i:s'),
			);

		$this->usuario->update_password(array('id' => $this->input->post('user_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$this->usuario->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('username')) == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Ingrese un nombre de Usuario';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('apellido')) == '')
		{
			$data['inputerror'][] = 'apellido';
			$data['error_string'][] = 'Ingrese un Apellido';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('email')) == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail';
			$data['status'] = FALSE;
		}								

		// Valida formato email
		if(!filter_var(trim($this->input->post('email')), FILTER_VALIDATE_EMAIL))
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail válido';
			$data['status'] = FALSE;
		}	

		// Para un usuario nuevo, valida contraseña
		if ($add)
		{
			if(trim($this->input->post('password')) == '')
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Ingrese una contraseña';
				$data['status'] = FALSE;
			}	

			if(trim($this->input->post('password2')) == '')
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Ingrese nuevamente la contraseña';
				$data['status'] = FALSE;
			}	

			// Valida longitud de la contraseña
			if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
				$data['status'] = FALSE;
			}

			// Valida contraseñas iguales
			if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
			{
				if(trim($this->input->post('password')) != trim($this->input->post('password2')))
				{
					$data['inputerror'][] = 'password2';
					$data['error_string'][] = 'Las contraseñas no coinciden';
					$data['status'] = FALSE;
				}
			}	
		}

		// Valida que no exista un registro con el mismo nombre de usuario
		if ($add)
			$duplicated = $this->usuario->check_duplicated('username', trim($this->input->post('username')));
		else
			$duplicated = $this->usuario->check_duplicated_edit($this->input->post('id'), 'username', trim($this->input->post('username')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'Ya existe un Usuario con ese nombre';
			$data['status'] = FALSE;
		}	

		// Valida que no exista un registro con el mismo email
		if ($add)
			$duplicated = $this->usuario->check_duplicated('email', trim($this->input->post('email')));
		else
			$duplicated = $this->usuario->check_duplicated_edit($this->input->post('id'), 'email', trim($this->input->post('email')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ya existe un Usuario con ese e-mail';
			$data['status'] = FALSE;
		}	
		
		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	private function _validate_password()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		// Valida contraseña
		if(trim($this->input->post('password_actual')) == '')
		{
			$data['inputerror'][] = 'password_actual';
			$data['error_string'][] = 'Ingrese contraseña actual';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('password')) == '')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Ingrese nueva contraseña';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('password2')) == '')
		{
			$data['inputerror'][] = 'password2';
			$data['error_string'][] = 'Repita la nueva contraseña';
			$data['status'] = FALSE;
		}	

		// Valida longitud de la contraseña
		if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
			$data['status'] = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
		{
			if(trim($this->input->post('password')) != trim($this->input->post('password2')))
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Las contraseñas no coinciden';
				$data['status'] = FALSE;
			}
		}	

		// Compara contraseña ingresada contra la registrada en la base de datos
		if (!$this->usuario->validate_password($this->input->post('user_id'), $this->input->post('password_actual')))
		{
			$data['inputerror'][] = 'password_actual';
			$data['error_string'][] = 'Password incorrecta';
			$data['status'] = FALSE;
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}