<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Servicio_Item extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/servicioitem_model','servicio_item');
		$this->load->model('ABM/servicio_model','servicio');
		$this->load->model('ABM/animal_model','animal');		
		$this->load->model('Tools/media_model','media');		
		$this->load->library('validations');		

		$this->load->helper('url');		
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["servicios"] = $this->servicio->get_all();
		$datos_vista["animales"] = $this->animal->get_all();

		$this->load->view('admin/ABM/servicio_item_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->servicio_item->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $servicio_item) {
			$no++;
			$row = array();

			$row[] = $servicio_item->servicio;
			$row[] = $servicio_item->nombre;
			$row[] = $servicio_item->animal;

			$row[] = '<img src="'. IMG_PATH . 'servicios/miniaturas/'. $servicio_item->imagen .'" style="max-width: 180px">';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_servicio_item('."'".$servicio_item->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_servicio_item('."'".$servicio_item->nombre."'".',' .$servicio_item->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->servicio_item->count_all(),
						"recordsFiltered" => $this->servicio_item->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->servicio_item->get_by_id($id);

		echo json_encode($data);
	}


	public function ajax_add()
	{
		$imagen = '';

		$this->_validate(true);

		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width');
			$big = $this->config->item('img_big_width');
			
			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'servicios');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$data = array(
				'servicio_item_nombre' => $this->input->post('nombre'),
				'servicio_item_detalle' => $this->input->post('detalle'),
				'servicio_item_servicio_id' => $this->input->post('servicio'),
				'servicio_item_animal_id' => $this->input->post('animal'),
				'servicio_item_imagen' => $imagen,
			);

		$insert = $this->servicio_item->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width');
			$big = $this->config->item('img_big_width');

			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'servicios');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$this->_validate();

		$data = array(
				'servicio_item_nombre' => $this->input->post('nombre'),
				'servicio_item_detalle' => $this->input->post('detalle'),
				'servicio_item_servicio_id' => $this->input->post('servicio'),			
				'servicio_item_animal_id' => $this->input->post('animal'),
				'servicio_item_imagen' => $imagen,
			);

		$this->servicio_item->update(array('servicio_item_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$data = array();

		$resultado = $this->servicio_item->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);		
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún artíulo
		$count = $this->servicio_item->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'servicios')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un nombre';
			$data['status'] = FALSE;
		}		

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->servicio_item->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->servicio_item->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe un Servicio Item con ese nombre';
			$data['status'] = FALSE;
		}	

        if($this->input->post('servicio') == '')
		{
			$data['inputerror'][] = 'servicio';
			$data['error_string'][] = 'Seleccione un servicio';
			$data['status'] = FALSE;
		}		

        if($this->input->post('animal') == '')
		{
			$data['inputerror'][] = 'animal';
			$data['error_string'][] = 'Seleccione un animal';
			$data['status'] = FALSE;
		}	

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}