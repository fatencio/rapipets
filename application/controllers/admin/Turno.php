<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Turno extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/turnocomentario_model','turno_comentario');			
		$this->load->model('Usuarios/local_model','local');
		$this->load->model('ABM/servicio_model','servicio');
		$this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista = array();

		$datos_vista["fechas"] = $this->turno->get_all_fechas();
		$datos_vista["clientes"] = $this->turno->get_all_clientes();
		$datos_vista["locales"] = $this->local->get_all();
		$datos_vista["servicios"] = $this->servicio->get_all();		

		$this->load->view('admin/Actividad/turno_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->turno->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $turno) {
			$no++;
			$row = array();

			$fecha = new DateTime($turno->fecha);
            $hora = new DateTime($turno->hora);  

			$row[] = $turno->numero; 

	        // Resalta turnos del dia
	        $hoy = $fecha->format('d/m/Y') == date('d/m/Y') ? "<div class='label label-success'>HOY</div>": "";

	        $fecha = $fecha->format('d/m/Y') . ' ' . $hora->format('H:i') . ' hs&nbsp;&nbsp;&nbsp;' . $hoy;

	    	$row[] = $fecha;
	        $row[] = $turno->servicio; // . ' - ' . $turno->servicio_item;			    	
	    	$row[] = '$ ' . $turno->total;	
			$row[] = $turno->local; 	    	
			$row[] = $turno->cliente;
		
			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Ver turno" onclick="detalle_turno('."'".$turno->id."'".')">ver turno</a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->turno->count_all(),
						"recordsFiltered" => $this->turno->count_filtered(),
						"data" => $data,
				);
		

		echo json_encode($output);
	}


	public function ajax_comentarios_list($turno_id)
	{
		$list = $this->turno_comentario->get_datatables($turno_id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $turno_comentario) 
		{
			$row = array();

			// Fecha
			$fecha = new DateTime($turno_comentario->fecha);
			$row[] = $fecha->format('d/m/Y') . '<br />&nbsp;&nbsp;<span class="text-center font-13">' . $fecha->format('H:i') . ' hs.</span>';

			// Cliente / Local
			if ($turno_comentario->cliente)
				$row[] = $turno_comentario->cliente;
			else
				$row[] = $turno_comentario->local;

			// Comentario
			$row[] = $turno_comentario->comentario;
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);


		echo json_encode($output);
	}	

	
	public function get_by_id($id){

		$puntaje = '';

		$data = $this->turno->get_by_id($id);

		$fecha = new DateTime($data->fecha);
		$data->fecha = $fecha->format('d/m/Y');

		// Si ya tiene califiacion la muestra
		if (!is_null($data->puntaje)){

			$puntaje = '<div>';

			for($x=1; $x<=5; $x++)
			{					
			if($x <= $data->puntaje)
				$puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
			else
				$puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
			}					

			$puntaje .= '</div>';
		}	

		$data->puntaje = $puntaje;	

		echo json_encode($data);	
	}

}