<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Local_Articulo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios/localarticulo_model','local_articulo');
		$this->load->library('tools');		
	}


	// Lista de productos de un local 
	// Se llama desde Dashboard - Local - Artículos del local
	public function ajax_list($id)
	{
		$list = $this->local_articulo->get_datatables($id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $articulo_local) 
		{
			$stock = $articulo_local->stock == 1 ? "<div class='label label-success'>EN STOCK</div>": "<div class='label label-danger'>SIN STOCK</div>";

			$estado = $articulo_local->estado == 1 ? "<div class='label label-info'>ACTIVO</div>": "<div class='label label-warning'>INACTIVO</div>";

			$descuento = $articulo_local->descuento == 0 ? "Sin desc.": $articulo_local->descuento . "%";

			$presentacion = $articulo_local->estado_presentacion == 0 ? '<div style="background: #F3B760;">' . $articulo_local->presentacion . '</div>': $articulo_local->presentacion;

			$precio = $articulo_local->precio_descuento != $articulo_local->precio ? "<div class='tachado'>$" . $articulo_local->precio . "</div> " . "$" . $articulo_local->precio_descuento : "$" . $articulo_local->precio;

			$no++;
			$row = array();

			$row[] = '<input type="checkbox" precio="' . $articulo_local->precio .'" stock="' . $articulo_local->stock .'" name="chkDescuento[]" id="chkDescuento" class="chkDescuento" value=" ' . $articulo_local->id . '">';
			
			$row[] = $articulo_local->articulo;
			$row[] = $articulo_local->tipo;
			$row[] = $articulo_local->presentacion;
			$row[] = $articulo_local->marca;
			$row[] = $precio;
			$row[] = $descuento;
			$row[] = $stock;
			$row[] = $estado;

			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_articulo_local('."'".$articulo_local->id."','".$articulo_local->precio."','".$articulo_local->descuento_id."','".$articulo_local->stock."'".",'".$articulo_local->local."','".$articulo_local->articulo."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->local_articulo->count_all($id),
						"recordsFiltered" => $this->local_articulo->count_filtered($id),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_descuento_masivo()
	{
		$i = 0;
		$precio = $this->input->post('precio');
		$stock = $this->input->post('stock');
		$porcentaje = $this->input->post('porcentaje');

		foreach ($this->input->post('local_articulo') as $local_articulo_id) {

			// Para articulos sin stock no aplica el descuento
			if ($stock[$i] != '0'){

				$descuento = ($precio[$i] * $porcentaje) / 100;
				$precio_descuento = $precio[$i] - $descuento;

				$data = array(
					'local_articulo_descuento_id' => $this->input->post('descuento_id'),
					'local_articulo_precio_descuento' => $precio_descuento
				);

				$this->local_articulo->update(array('local_articulo_id' => $local_articulo_id), $data);		
			}

			$i++;
		}

	
		echo json_encode(array("status" => TRUE));
	}



    public function ajax_update()
	{
		$this->_validate();

		$precio = $this->input->post('precio');
		$porcentaje = $this->input->post('porcentaje');

		$descuento = ($precio * $porcentaje) / 100;
		$precio_descuento = round($precio - $descuento);

		// Para articulos sin stock no aplica el descuento
		if ($this->input->post('stock') == '0'){
			$descuento_id = 5; // TODO: recuperar id sin descuento desde el modelo
			$precio_descuento = $precio;
		}
		else
			$descuento_id = $this->input->post('descuento_id');

		$data = array(

				'local_articulo_precio' => $precio,
				'local_articulo_stock' =>  $this->input->post('stock'),
				'local_articulo_descuento_id' => $descuento_id,
				'local_articulo_precio_descuento' => $precio_descuento
			);

		$this->local_articulo->update(array('local_articulo_id' => $this->input->post('articulo_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		if(trim($this->input->post('precio')) == '')
		{
			$data['inputerror'][] = 'precio';
			$data['error_string'][] = 'Ingrese un precio';
			$data['status'] = FALSE;
		}	

		// Valida que precio sea un numero entero positivo
		if(!$this->tools->is_natural($this->input->post('precio')))			
		{
			$data['inputerror'][] = 'precio';
			$data['error_string'][] = 'Precio debe ser un número entero mayor o igual que 0.';
			$data['status'] = FALSE;
		}			

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}

	}

}