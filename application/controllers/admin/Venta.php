<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Venta extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/mediopago_model','medio_pago');
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/ventacomentario_model','venta_comentario');		
		$this->load->model('Usuarios/local_model','local');

  		$CI = &get_instance();
    	$CI->config->load("mercadopago", TRUE);
    	$config = $CI->config->item('mercadopago');
    	$this->load->library('mercadopago', $config); 		
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista = array();

		$datos_vista["fechas"] = $this->venta->get_all_fechas();
		$datos_vista["clientes"] = $this->venta->get_all_clientes();
		$datos_vista["locales"] = $this->local->get_all();
		$datos_vista["medios_pago"] = $this->medio_pago->get_all();

		$this->load->view('admin/Actividad/venta_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->venta->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $venta) {
			$no++;
			$row = array();

			$row[] = $venta->numero; 

			$fecha = new DateTime($venta->fecha);
			$row[] = $fecha->format('d/m/Y');
			$row[] = $venta->forma_pago;

			switch ($venta->estado) {

				case 'reservado':
					$row[] = '<td class="hidden-xs"><span class="label label-info">Reservado</span></td>';
					break;

				case 'pendiente':
					$row[] = '<td class="hidden-xs"><span class="label label-info">Pendiente</span></td>';
					break;

				case 'concretado':
					$row[] = '<td class="hidden-xs"><span class="label label-success">Concretado</span></td>';
					break;

				case 'cancelado':
					$row[] = '<td class="hidden-xs"><span class="label label-danger">Cancelado</span></td>';
					break;

				case 'no concretado':
					$row[] = '<td class="hidden-xs"><span class="label label-danger">No Concretado</span></td>';
					break;					
			}		

			$row[] = $venta->cliente;
			$row[] = $venta->local;
			$row[] = "$ " . $venta->total;

			//add html for action
			if ($venta->tipo == 'venta')
				$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Ver Venta" onclick="detalle_venta('."'".$venta->id."'".')">ver venta</a>';
			else
				$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Ver Turno" onclick="detalle_turno('."'".$venta->id."'".')">ver turno</a>';			
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->venta->count_all(),
						"recordsFiltered" => $this->venta->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_excel_ventas()
	{
		$list = $this->venta->get_datatables_excel();
		$data = array();
		$precio_total = 0;

		foreach ($list as $venta) 
		{
			$fecha = new DateTime($venta->fecha);
						
			$row = array();

			$row[] = $venta->numero; 
			$row[] = $fecha->format('d/m/Y');
			$row[] = $venta->forma_pago;
			$row[] = $venta->estado;
			$row[] = $venta->cliente;
			$row[] = $venta->local;
			$row[] = $venta->marca != '' ? $venta->marca . ' - ' . $venta->articulo : $venta->articulo;
			$row[] = $venta->presentacion;
			$row[] = $venta->cantidad;			
			$row[] = $venta->precio;
			$row[] = $venta->precio * $venta->cantidad;

			$precio_total += $venta->precio * $venta->cantidad;
		
			$data[] = $row;
		}

		// Total Genaral
		$row = array();

		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = '<span style="font-weight: 900">TOTAL GENERAL</span>'; 
		$row[] = $precio_total; 

		$data[] = $row;

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->venta->count_all(),
						"recordsFiltered" => $this->venta->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_excel_turnos()
	{
		$list = $this->turno->get_datatables_excel();
		$data = array();
		$precio_total = 0;

		foreach ($list as $turno) 
		{
			$row = array();

			$fecha = new DateTime($turno->fecha);

			$row[] = $turno->numero; 			
			$row[] = $fecha->format('d/m/Y');
			$row[] = $turno->forma_pago;
			$row[] = $turno->estado;
			$row[] = $turno->cliente;
			$row[] = $turno->local;
			$row[] = $turno->servicio;
			$row[] = $turno->servicio_item;
			$row[] = $turno->precio;

			$precio_total += $turno->precio;
		
			$data[] = $row;
		}

		// Total Genaral
		$row = array();

		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = ''; 
		$row[] = '<strong>TOTAL GENERAL</strong>'; 
		$row[] = $precio_total; 

		$data[] = $row;

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->turno->count_all(),
						"recordsFiltered" => $this->turno->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_list_detalle($venta_id)
	{
		$list = $this->venta->get_datatables_detalle($venta_id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $venta_detalle) {
			$no++;
			$row = array();
			$precio_descuento = 0;

			// Calcula descuento
			if ($venta_detalle->descuento > 0){
				$descuento = ($venta_detalle->precio * $venta_detalle->descuento) / 100;
				$precio_descuento = $venta_detalle->precio - $descuento;
			}

			// Precio y precio con descuento
			if ($precio_descuento != 0){
				$precio = "<div class='tachado text-left pull-left'>$" . $venta_detalle->precio . "</div> " . "<div class='text-right pull-right'> $" . $precio_descuento . "</div>";

				// subtotal
				$subtotal = $venta_detalle->cantidad * $precio_descuento;
			}
			else{
				$precio = "<div class='text-right'> $" . $venta_detalle->precio;

				// subtotal
				$subtotal = $venta_detalle->cantidad * $venta_detalle->precio;				
			}

			$subtotal = "$" . $subtotal;	

			$row[] = $venta_detalle->marca . ' ' . $venta_detalle->articulo;
			$row[] = $venta_detalle->presentacion;
			$row[] = $venta_detalle->cantidad;
			$row[] = $precio;
			$row[] = $subtotal;
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);


		echo json_encode($output);
	}


	public function ajax_comentarios_list($venta_id)
	{
		$list = $this->venta_comentario->get_datatables($venta_id);
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $venta_comentario) 
		{
			$row = array();

			// Fecha
			$fecha = new DateTime($venta_comentario->fecha);
			$row[] = $fecha->format('d/m/Y') . '<br />&nbsp;&nbsp;<span class="text-center font-13">' . $fecha->format('H:i') . ' hs.</span>';

			// Cliente / Local
			if ($venta_comentario->cliente)
				$row[] = $venta_comentario->cliente;
			else
				$row[] = $venta_comentario->local;

			// Comentario
			$row[] = $venta_comentario->comentario;
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"data" => $data,
				);


		echo json_encode($output);
	}


	public function get_venta_by_id($venta_id){

		$puntaje = '';

		$data = $this->venta->get_venta_by_id($venta_id);

		$fecha = new DateTime($data->fecha);
		$data->fecha = $fecha->format('d/m/Y');

		// Si ya tiene califiacion la muestra
		if (!is_null($data->puntaje)){

			$puntaje = '<div>';

			for($x=1; $x<=5; $x++)
			{					
			if($x <= $data->puntaje)
				$puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starsi.png" width="20" height="20">';
			else
				$puntaje .= '<img class="push-5-r" src="' . BASE_PATH . '/assets/img/frontend/starno.png" width="20" height="20">';
			}					

			$puntaje .= '</div>';
		}	

		$data->puntaje = $puntaje;	

		echo json_encode($data);	
	}

	public function check_pagos_mp(){

		$id = 103;
		// Carga lista de pagos realizados por Mercadopago con estado 'pendiente'
		/*
	$filters = array(
		"site_id" => "MLA", // Argentina: MLA; Brasil: MLB
		"external_reference" => "103",

	);
*/
/*
	     $this->mercadopago->sandbox_mode(TRUE);
 
        $payment_info = $this->mercadopago->get_payment_info($id);
 
        if ($payment_info["status"] == 200) {
            $data["estado_pago"] = $payment_info["response"];
        }
*/
	// Search payment data according to filters
	// $data["estado_pago"] = $this->mercadopago->search_payment($filters);
		/*
		$payment_info = $this->mercadopago->get_payment_info('102');

		if ($payment_info["status"] == 200) {
			$data["estado_pago"] = $payment_info["response"];
		}
		*/
		$this->mercadopago->sandbox_mode(TRUE);

/*
		$filters = array (
				"site_id" => "MLA",
				"operation_type" => "regular_payment",
				"range" => "date_created",
				"begin_date" => "NOW-2MONTH",
				"end_date" => "NOW"
		 );
*/
		$filters = array (
			"status" => "approved",
			"operation_type" => "regular_payment",
			"range" => "date_created",
			"begin_date" => "NOW-1MONTH",
			"end_date" => "NOW"
		);

		
		$data["estado_pago"] =  $this->mercadopago->search_payment ($filters, 0, 10);


		echo json_encode($data);	
	}

}