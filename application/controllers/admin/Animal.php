<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Animal extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/animal_model','animal');
		$this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');
		$this->load->view('admin/ABM/animal_view');
  
  
	}


	public function ajax_list()
	{
		$list = $this->animal->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $animal) {
			$no++;
			$row = array();

			$row[] = $animal->nombre;

			if ($animal->conraza == 1)
				$row[] = '<td class="hidden-xs"><span class="label label-success">Si</span></td>';
			else
				$row[] = '<td class="hidden-xs"><span class="label label-danger">No</span></td>';

			if ($animal->contamanios == 1)
				$row[] = '<td class="hidden-xs"><span class="label label-success">Si</span></td>';
			else
				$row[] = '<td class="hidden-xs"><span class="label label-danger">No</span></td>';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_animal('."'".$animal->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_animal('."'".$animal->nombre."'".',' .$animal->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->animal->count_all(),
						"recordsFiltered" => $this->animal->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->animal->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'animal_nombre' => $this->input->post('nombre'),
				'animal_conraza' => $this->input->post('conraza'),
				'animal_contamanios' => $this->input->post('contamanios'),
			);

		$insert = $this->animal->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'animal_nombre' => $this->input->post('nombre'),
				'animal_conraza' => $this->input->post('conraza'),
				'animal_contamanios' => $this->input->post('contamanios'),
			);

		$this->animal->update(array('animal_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$data = array();

		$resultado = $this->animal->delete_by_id($id);

		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->animal->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->animal->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe un Animal con ese nombre';
			$data['status'] = FALSE;
		}	

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}