<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Actividad/notificacion_model','notificacion');
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');		
		$this->load->library('validations');
		$this->load->library('tools');
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista = array();

		$datos_vista["tipos"] = $this->notificacion->get_all_tipo();

		$this->load->view('admin/Actividad/notificacion_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->notificacion->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $notificacion) {
			$no++;
			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;

			// ver detalles 
			switch ($notificacion->tipo) {
				case 'venta':
				case 'aviso venta':
				case 'cancela venta':
				case 'mensaje venta':
					$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="ver venta" onclick="detalle_venta('."'".$notificacion->venta_id."'".')">ver venta</a>';
					break;

				case 'turno':
				case 'aviso turno':
				case 'cancela turno':
				case 'mensaje turno':
					$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="ver turno" onclick="detalle_turno('."'".$notificacion->turno_id."'".')">ver turno</a>';	
					break;				

				case 'calificación turno':
				case 'calificación venta':
				$row[] = '<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="moderar" onclick="return loadController(\'admin/Valoracion/index\')"; ">moderar</a>';	

					break;	
				default:
					$row[] = '';
					break;
			}

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->notificacion->count_all(),
						"recordsFiltered" => $this->notificacion->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_add()
	{
		$data = array(
				'notificacion_texto' => 'Test Cron',// $this->input->post('texto'),
			);

		$insert = $this->notificacion->save($data);

		echo "Cron Job!";//json_encode(array("status" => TRUE));
	}


	public function get_count_novistas_admin(){

		$data = $this->notificacion->get_count_novistas();
		echo json_encode($data);	
	}


	public function get_novistas_admin(){

		$data = array();

		$notificaciones = $this->notificacion->get_novistas();

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));
			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;

			$data[] = $row;
		}


		echo json_encode($data);	
	}


	public function get_recientes(){

		$data = array();

		$notificaciones = $this->notificacion->get_recientes();

		foreach ($notificaciones as $notificacion) {

			$row = array();

			// Calcula tiempo transcurrido de la notificación
			$fecha = new DateTime($notificacion->fecha, new DateTimeZone('America/Argentina/Buenos_Aires'));			

			$tiempo = $this->tools->hace_fecha($fecha);

			$row[] = $tiempo;
			$row[] = $notificacion->tipo;
			$row[] = $notificacion->texto;
			$row[] = $notificacion->leida;
			$row[] = $notificacion->venta_id;
			$row[] = $notificacion->turno_id;
			$row[] = $notificacion->vista;
			$row[] = $notificacion->id;

			$data[] = $row;
		}


		echo json_encode($data);	
	}


	public function set_leida_admin($id){

		$data = array(
				'notificacion_leida' => '1',
			);

		$this->notificacion->update(array('notificacion_id' => $id), $data);

		echo json_encode(0);	
	}


	public function set_vistas_admin(){

		$data = array(
				'notificacion_vista' => '1',
			);

		$this->notificacion->update(array('notificacion_vista' => '0'), $data);

		echo json_encode(0);	
	}


	public function test()
	{
		$this->load->helper('url');
		$this->load->view('admin/Actividad/notificacion_test');
	}


	public function delete_old()
	{
		$data = array();

		$resultado = $this->notificacion->delete_old();

		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}

}