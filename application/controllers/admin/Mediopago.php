<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mediopago extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/mediopago_model','medio_pago');
		$this->load->model('ABM/mediopagoitem_model','medio_pago_item');
		$this->load->model('Usuarios/localmediopago_model','local_medio_pago');
		$this->load->model('Tools/media_model','media');				
		$this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');
		$this->load->view('admin/ABM/mediopago_view');
	}


	public function ajax_list()
	{
		$list = $this->medio_pago->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $medio_pago) {
			$no++;
			$row = array();

			$row[] = $medio_pago->nombre;

			if ($medio_pago->estado == 1)
				$row[] = '<td class="hidden-xs"><span class="label label-success">Si</span></td>';
			else
				$row[] = '<td class="hidden-xs"><span class="label label-danger">No</span></td>';

			if ($medio_pago->items == 1)
				$row[] = '<td class="hidden-xs"><span class="label label-success">Si</span></td>';
			else
				$row[] = '<td class="hidden-xs"><span class="label label-danger">No</span></td>';		
				
			$row[] = '<img src="'. IMG_PATH . 'medios_pago/miniaturas/'. $medio_pago->imagen .'" >';	

			// Acciones

			// No permite eliminar Mercado Pago 
			if ($medio_pago->id == 1)
			{
				$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar"    onclick="edit_medio_pago('."'".$medio_pago->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';
			}
			else
			{
				$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar"    onclick="edit_medio_pago('."'".$medio_pago->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_medio_pago('."'".$medio_pago->nombre."'".',' .$medio_pago->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
			}
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->medio_pago->count_all(),
						"recordsFiltered" => $this->medio_pago->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->medio_pago->get_by_id($id);

        echo json_encode($data);
	}


	public function ajax_add()
	{
		$imagen = '';		

		$this->_validate(true);

		// Valida si se seleccionó una foto y si la foto del medio de pago es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width_medio_pago');
			$big = $this->config->item('img_big_width_medio_pago');
			
			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'medios_pago');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$data = array(
				'medio_pago_nombre' => $this->input->post('nombre'),
				'medio_pago_estado' => $this->input->post('estado'),
				'medio_pago_items' => $this->input->post('items'),	
				'medio_pago_imagen' => $imagen,		
			);

		$insert = $this->medio_pago->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$imagen = '';

		$this->_validate();

		// Valida si se seleccionó una foto y si la foto del medio de pago es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width_medio_pago');
			$big = $this->config->item('img_big_width_medio_pago');

			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'medios_pago');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$data = array(
				'medio_pago_nombre' => $this->input->post('nombre'),
				'medio_pago_estado' => $this->input->post('estado'),
				'medio_pago_items' => $this->input->post('hd_items'),		
				'medio_pago_imagen' => $imagen,			
			);

		$this->medio_pago->update(array('medio_pago_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$data = array();

		$resultado = $this->medio_pago->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	public function ajax_medios_pago_local($local_id = '')
	{
		$data = new stdClass();

		$data->medios_pago = $this->medio_pago->get_all_items(FALSE, TRUE);

		foreach ($data->medios_pago as $medio_pago) 
		{
			// Medios de pago del local
			$medio_pago_local = $this->local_medio_pago->get_by_medio_pago_local($medio_pago->id, $local_id);

			if ($medio_pago_local)
			{
				$medio_pago->local_medio_pago_id = $medio_pago_local->id;
			}

			// Carga items de cada medio_pago
			$medio_pago->items = $this->medio_pago_item->get_by_medio_pago($medio_pago->id);

			foreach ($medio_pago->items as $item) 
			{
				// Medio pago items del local
				if ($local_id != '')
				{
					$item_local = $this->local_medio_pago->get_by_medio_pago_item_local($item->id, $local_id);

					if ($item_local)
					{
						$item->local_medio_pago_id = $item_local->id;
					}					
				}
				
			}
		}

		echo json_encode($data);	
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}		

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->medio_pago->check_duplicated(trim($this->input->post('nombre')));
		else
			$duplicated = $this->medio_pago->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe un Medio_pago con ese nombre';
			$data['status'] = FALSE;
		}	

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún artíulo
		$count = $this->medio_pago->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'medios_pago')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen del medio de pago.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);
	}	

}