<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuarios/cliente_model','cliente');
		$this->load->model('Usuarios/frontend_model','frontend_user');				
		$this->load->model('Tools/provincia_model','provincia');	
		$this->load->model('Tools/media_model','media');	
		$this->load->model('Actividad/venta_model','venta');
		$this->load->model('Actividad/turno_model','turno');
		$this->load->model('Actividad/ventacomentario_model','venta_comentario');		
		$this->load->model('Actividad/turnocomentario_model','turno_comentario');		

        $this->load->library('validations');
	}


	public function index()
	{
		$this->load->helper('url');

		$datos_vista["provincias"] = $this->provincia->get_all();

		$this->load->view('admin/Usuarios/cliente_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->cliente->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $cliente) {

			$checked = $cliente->status == 1 ? "checked": "";
			$status = $cliente->status == 1 ? 0:1;

			$no++;
			$row = array();

			$row[] = $cliente->nombre;
            $row[] = $cliente->apellido;
            $row[] = $cliente->telefono;
            $row[] = $cliente->email;

			$row[] = '<img src="'. IMG_PATH . 'cliente/miniaturas/'. $cliente->avatar .'" width="100">';
			
			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Modificar Password"    onclick="edit_password('."'".$cliente->id."'".')"><i class="fa fa-key"></i></a>

				<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_cliente('."'".$cliente->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>

				<a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_cliente('."'".$cliente->nombre."'".',' .$cliente->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;

			 	<label class="css-input switch switch-sm switch-info" title="Activar / Desactivar">
			 		<input type="checkbox" name="switch_cliente"' . $checked . ' onclick="switch_cliente('. $cliente->id .')"><span></span>
             	</label>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->cliente->count_all(),
						"recordsFiltered" => $this->cliente->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->cliente->get_by_id($id);
		echo json_encode($data);
	}


	public function ajax_activar_desactivar($id)
	{
		if (isset($_SESSION['username']))
		{
			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$nuevo_status = 0;
			$cliente = $this->cliente->get_by_id($id);

			if($cliente->status == 0) $nuevo_status = 1;

			if ($nuevo_status == 0)
			{
				$data = array(
					'cliente_status' => $nuevo_status,

					// Audit trail
					'cliente_fecha_suspension' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_suspension' => $_SESSION['username']			
				);	
			}
			else
			{
				$data = array(
					'cliente_status' => $nuevo_status,

					// Audit trail
					'cliente_fecha_habilitacion' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_habilitacion' => $_SESSION['username']			
				);	
			}

			$this->cliente->update(array('cliente_id' => $id), $data);

			echo json_encode(array("status" => TRUE, "nuevo_status" => $nuevo_status));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}
	}


	public function ajax_add()
	{
		if (isset($_SESSION['username']))
		{
			$imagen = '';
			
			$this->_validate(true);
			

			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');
				
				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'cliente');
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));		

			$data = array(
					'cliente_nombre' => $this->input->post('nombre'),
					'cliente_apellido' => $this->input->post('apellido'),
					'cliente_telefono' => $this->input->post('telefono'),
					'cliente_email' => $this->input->post('email'),
					'cliente_direccion' => $this->input->post('direccion'),
					'cliente_localidad' => $this->input->post('localidad'),
					'cliente_provincia_id' => $this->input->post('provincia'),
					'cliente_password' => $this->input->post('password'),
					'cliente_avatar' => $imagen,

					// Audit trail
					'cliente_fecha_alta' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_alta' => $_SESSION['username']
				);

			$insert = $this->cliente->save($data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	public function ajax_update()
	{
		if (isset($_SESSION['username']))
		{		
			$this->_validate();

			// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
			// Imagen nueva
			if(is_uploaded_file($_FILES['foto']['tmp_name'])){

				$thumb = $this->config->item('img_thumb_width');
				$big = $this->config->item('img_big_width');

				$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'cliente');
			}
			// Imagen precargada
			else{
			
				$imagen = $this->input->post('imagen');		
			}	

			$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$data = array(
					'cliente_nombre' => $this->input->post('nombre'),
					'cliente_apellido' => $this->input->post('apellido'),
					'cliente_telefono' => $this->input->post('telefono'),
					'cliente_email' => $this->input->post('email'),
					'cliente_direccion' => $this->input->post('direccion'),
					'cliente_localidad' => $this->input->post('localidad'),
					'cliente_provincia_id' => $this->input->post('provincia'),
					'cliente_avatar' => $imagen,

					// Audit trail
					'cliente_fecha_modificacion' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_modificacion' => $_SESSION['username']				
				);

			$this->cliente->update(array('cliente_id' => $this->input->post('id')), $data);

			echo json_encode(array("status" => TRUE));
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}

	public function ajax_update_password()
	{
		$this->_validate_password();

		$data = array(
				'cliente_password' => $this->input->post('password'),

			);

		$this->cliente->update_password(array('cliente_id' => $this->input->post('cliente_id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	// Elimina Cliente solo si no tiene Ventas o Turnos asociados
	public function ajax_delete($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

	        // Valida si hay VENTAS del cliente
	        $ventas = $this->cliente->count_ventas($id);

	        // Valida si hay TURNOS del cliente
	        $turnos = $this->cliente->count_turnos($id);

	        if ($ventas == 0 && $turnos == 0)
	        {
				$ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

				$dataDelete = array(

						// Audit trail
						'cliente_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
						'cliente_usuario_baja' => $_SESSION['username']				
					);

				$this->cliente->update(array('cliente_id' => $id), $dataDelete);

				$data['status'] = true;
	        }
	        else
	        {
	        	$data['status'] = false;
	        	$data['mensaje'] = "No es posible eliminar este elemento ya que está siendo utilizado.";
	        }

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}


	// Elimina Cliente junto con Ventas o Turnos asociados
	public function ajax_delete_full($id)
	{
		if (isset($_SESSION['username']))
		{		
	        $data = array();

	        $ahora = new DateTime("now", new DateTimeZone('America/Argentina/Buenos_Aires'));	

			$dataDelete = array(
					// Audit trail
					'cliente_fecha_baja' => $ahora->format('Y-m-d H:i:s'),
					'cliente_usuario_baja' => $_SESSION['username']				
				);

	        $this->db->trans_begin();

	        // Elimina VENTAS y TURNOS del cliente
	      	$this->venta_comentario->delete_by_cliente($id);
	      	$this->venta->delete_detalle_by_cliente($id);
	        $this->venta->delete_by_cliente($id);
	        
	      	$this->turno_comentario->delete_by_cliente($id);
	        $this->turno->delete_by_cliente($id);

	        // Elimina CLIENTE
			$this->cliente->update(array('cliente_id' => $id), $dataDelete);

			$this->db->trans_commit();

			$data['status'] = true;

			echo json_encode($data);
		}
		else 
		{	
			echo json_encode(array("status" => FALSE, "login" => TRUE));
		}			
	}

	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un Nombre';
			$data['status'] = FALSE;
		}

	    if(trim($this->input->post('apellido')) == '')
		{
			$data['inputerror'][] = 'apellido';
			$data['error_string'][] = 'Ingrese un Apellido';
			$data['status'] = FALSE;
		}
		
		if(trim($this->input->post('email')) == '')
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail';
			$data['status'] = FALSE;
		}								

		// Valida formato email
		if(!filter_var(trim($this->input->post('email')), FILTER_VALIDATE_EMAIL))
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ingrese un E-mail válido';
			$data['status'] = FALSE;
		}	

		// Para un usuario nuevo, valida contraseña
		if ($add)
		{
			if(trim($this->input->post('password')) == '')
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Ingrese una contraseña';
				$data['status'] = FALSE;
			}	

			if(trim($this->input->post('password2')) == '')
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Ingrese nuevamente la contraseña';
				$data['status'] = FALSE;
			}	

			// Valida longitud de la contraseña
			if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
			{
				$data['inputerror'][] = 'password';
				$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
				$data['status'] = FALSE;
			}

			// Valida contraseñas iguales
			if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
			{
				if(trim($this->input->post('password')) != trim($this->input->post('password2')))
				{
					$data['inputerror'][] = 'password2';
					$data['error_string'][] = 'Las contraseñas no coinciden';
					$data['status'] = FALSE;
				}
			}	
		}

		// Valida que no exista una entidad con el mismo email (Cliente o Local)
		if ($add)
			$duplicated = $this->frontend_user->check_duplicated(trim($this->input->post('email')));
		else
			$duplicated = $this->frontend_user->check_duplicated_edit($this->input->post('id'), trim($this->input->post('email')));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'email';
			$data['error_string'][] = 'Ya existe un usuario con ese e-mail';
			$data['status'] = FALSE;
		}	

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}		

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

	private function _validate_password()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;
	

		if(trim($this->input->post('password')) == '')
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Ingrese nueva contraseña';
			$data['status'] = FALSE;
		}	

		if(trim($this->input->post('password2')) == '')
		{
			$data['inputerror'][] = 'password2';
			$data['error_string'][] = 'Repita la nueva contraseña';
			$data['status'] = FALSE;
		}	

		// Valida longitud de la contraseña
		if(trim($this->input->post('password')) != '' && strlen(trim($this->input->post('password'))) < 6)
		{
			$data['inputerror'][] = 'password';
			$data['error_string'][] = 'Longitud mínima de la contraseña: 6 caracteres';
			$data['status'] = FALSE;
		}

		// Valida contraseñas iguales
		if(trim($this->input->post('password')) != '' && trim($this->input->post('password2')) != '')
		{
			if(trim($this->input->post('password')) != trim($this->input->post('password2')))
			{
				$data['inputerror'][] = 'password2';
				$data['error_string'][] = 'Las contraseñas no coinciden';
				$data['status'] = FALSE;
			}
		}	


		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún cliente
		$count = $this->cliente->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'cliente')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen de  cliente.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);

	}	
}