<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mediopago_Item extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/mediopagoitem_model','medio_pago_item');
		$this->load->model('ABM/mediopago_model','medio_pago');
		$this->load->model('Tools/media_model','media');		
		$this->load->library('validations');		

		$this->load->helper('url');		
	}


	public function index()
	{
		$datos_vista = array();

		$datos_vista["medios_pago"] = $this->medio_pago->get_all_items(TRUE);

		$this->load->view('admin/ABM/mediopago_item_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->medio_pago_item->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $medio_pago_item) {
			$no++;
			$row = array();

			$row[] = $medio_pago_item->medio_pago;
			$row[] = $medio_pago_item->nombre;

			$row[] = '<img src="'. IMG_PATH . 'medios_pago/miniaturas/'. $medio_pago_item->imagen .'" style="max-width: 180px">';

			//add html for action
			$row[] = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_mediopago_item('."'".$medio_pago_item->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_mediopago_item('."'".$medio_pago_item->nombre."'".',' .$medio_pago_item->id. ')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->medio_pago_item->count_all(),
						"recordsFiltered" => $this->medio_pago_item->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->medio_pago_item->get_by_id($id);

		echo json_encode($data);
	}


	public function ajax_add()
	{
		$imagen = '';

		$this->_validate(true);

		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width_medio_pago');
			$big = $this->config->item('img_big_width_medio_pago');
			
			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'medios_pago');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$data = array(
				'medio_pago_item_nombre' => $this->input->post('nombre'),
				'medio_pago_item_medio_pago_id' => $this->input->post('medio_pago'),
				'medio_pago_item_imagen' => $imagen,
			);

		$insert = $this->medio_pago_item->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		// Valida si se seleccionó una foto y si la foto del articulo es precargada o nueva
		// Imagen nueva
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			$thumb = $this->config->item('img_thumb_width_medio_pago');
			$big = $this->config->item('img_big_width_medio_pago');

			$imagen = $this->media->resize_image_big_thumb($_FILES['foto'], $thumb, $big, 'medios_pago');
		}
		// Imagen precargada
		else{
		
			$imagen = $this->input->post('imagen');		
		}

		$this->_validate();

		$data = array(
				'medio_pago_item_nombre' => $this->input->post('nombre'),
				'medio_pago_item_medio_pago_id' => $this->input->post('medio_pago'),			
				'medio_pago_item_imagen' => $imagen,
			);

		$this->medio_pago_item->update(array('medio_pago_item_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{
		$data = array();

		// Valida que el item no esté asociado a un local
		$cuenta = $this->medio_pago_item->check_in_local($id);

		if ($cuenta == 0)
		{
			$resultado = $this->medio_pago_item->delete_by_id($id);
			$data = $this->validations->valida_db_error($resultado);
		}
		else
		{
			$resultado['code'] = 1451; // item ya utilizado en un local
			$data = $this->validations->valida_db_error($resultado);
		}

		echo json_encode($data);		
	}


	public function delete_imagen($name){

		$data = array();
		$data['mensaje'] = '';
		$data['status'] = TRUE;

		// Valida si la imagen $name está siendo utilizada en algún artíulo
		$count = $this->medio_pago_item->check_imagen($name);

		if ($count > 0)
		{
			$data['mensaje'] = "No es posible eliminar esta imagen ya que está siendo utilizada.";
			$data['status'] = FALSE;
		}
		else
		{
			if($this->media->delete_image_big_thumb($name, 'medios_pago')) {
			     $data['status'] = TRUE;
			}
			else {
				$data['mensaje'] = "Error al eliminar imagen.";
				$data['status'] = FALSE;
			}			

		}

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un nombre';
			$data['status'] = FALSE;
		}		

		// Valida que no exista un registro con el mismo nombre
		if ($add)
			$duplicated = $this->medio_pago_item->check_duplicated(trim($this->input->post('nombre')), $this->input->post('medio_pago'));
		else
			$duplicated = $this->medio_pago_item->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')), $this->input->post('medio_pago'));

		if ($duplicated > 0)
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ya existe un Item con ese nombre';
			$data['status'] = FALSE;
		}	

        if($this->input->post('medio_pago') == '')
		{
			$data['inputerror'][] = 'medio_pago';
			$data['error_string'][] = 'Seleccione un Medio de Pago';
			$data['status'] = FALSE;
		}		

		// Valida extension de la imagen subida
		if(is_uploaded_file($_FILES['foto']['tmp_name'])){

			if(!$this->media->check_image_extension($_FILES['foto']['name'])){

				$data['inputerror'][] = 'imagen';
				$data['error_string'][] = 'Tipo de archivo no permitido (tipos permitidos: gif, jpg o png)';
				$data['status'] = FALSE;				
			}
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}

}