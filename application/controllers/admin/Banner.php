<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banner extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ABM/banner_model','banner');
		$this->load->model('Tools/configuracion_model','configuracion');

		$this->load->helper('url');	
		$this->load->library('validations');	
	}


	public function index()
	{
		$datos_vista = array();

		$this->load->view('admin/ABM/banner_view', $datos_vista);
	}


	public function ajax_list()
	{
		$list = $this->banner->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $banner) {
			$no++;
			$row = array();
			$acciones = '';
			$vista_previa = '';			

			$checked = $banner->global == 1 ? "checked": "";

			$row[] = $banner->nombre;

			if ($banner->global == 1) $vista_previa = '<div class="text-celeste push-10 font-w600 text-center">GLOBAL</div>';

			$vista_previa .= '<div class="vista-previa-banner text-center" id="div_vista_previa_banner" style="color: ' . $banner->color . '; background: ' . $banner->fondo . '">' . $banner->texto . 
                        '</div>';

            $row[] = $vista_previa;

			$acciones = '<a class="btn btn-xs btn-primary" href="javascript:void(0)" title="Editar" onclick="edit_banner('."'".$banner->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>';

			// si es banner global, no permite eliminar
			if ($banner->global != 1) $acciones .= ' <a class="btn btn-xs btn-danger" href="javascript:void(0)" title="Eliminar" onclick="delete_banner('."'".$banner->nombre."'".',' .$banner->id. ')"><i class="glyphicon glyphicon-trash"></i></a>&nbsp;&nbsp;';

			$acciones .= '<br /><label class="css-input switch switch-sm switch-info" title="Banner Global">
			 		<input type="checkbox" name="switch_banner"' . $checked . ' onclick="switch_banner_global('. $banner->id .')"><span></span>
             	</label>';				

			
			$row[] = $acciones;

			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->banner->count_all(),
						"recordsFiltered" => $this->banner->count_filtered(),
						"data" => $data,
				);

		echo json_encode($output);
	}


	public function ajax_edit($id)
	{
		$data = $this->banner->get_by_id($id);

		echo json_encode($data);
	}


	public function ajax_add()
	{
		$this->_validate(true);

		$data = array(
				'banner_nombre' => $this->input->post('nombre'),
				'banner_texto' => $this->input->post('texto'),
				'banner_color' => $this->input->post('color_texto'),
				'banner_fondo' => $this->input->post('color_fondo'),
			);

		$insert = $this->banner->save($data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_update()
	{
		$this->_validate();

		$data = array(
				'banner_nombre' => $this->input->post('nombre'),
				'banner_texto' => $this->input->post('texto'),		
				'banner_color' => $this->input->post('color_texto'),
				'banner_fondo' => $this->input->post('color_fondo'),						
			);

		$this->banner->update(array('banner_id' => $this->input->post('id')), $data);

		echo json_encode(array("status" => TRUE));
	}


	public function ajax_delete($id)
	{   
		$data = array();

		$resultado = $this->banner->delete_by_id($id);
		$data = $this->validations->valida_db_error($resultado);

		echo json_encode($data);
	}


	private function _validate($add = false)
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if(trim($this->input->post('nombre')) == '')
		{
			$data['inputerror'][] = 'nombre';
			$data['error_string'][] = 'Ingrese un nombre';
			$data['status'] = FALSE;
		}		

        if($this->input->post('texto') == '')
		{
			$data['inputerror'][] = 'texto';
			$data['error_string'][] = 'Ingrese un texto';
			$data['status'] = FALSE;
		}
		else
		{
			// Valida que no exista un registro con el mismo nombre
			if ($add)
				$duplicated = $this->banner->check_duplicated(trim($this->input->post('nombre')));
			else
				$duplicated = $this->banner->check_duplicated_edit($this->input->post('id'), trim($this->input->post('nombre')));

			if ($duplicated > 0)
			{
				$data['inputerror'][] = 'nombre';
				$data['error_string'][] = 'Ya existe una Banner con ese nombre';
				$data['status'] = FALSE;
			}	
		}

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}


	public function ajax_banner_global($id)
	{
		$global = 0;
		$banner = $this->banner->get_by_id($id);

		if($banner->global == 0) $global = 1;

		// Quita la marca 'global' del banner anteriormente seteado como global
		$data = array(
				'banner_global' => 0,
			);

		$this->banner->update_all($data);
		
		// Setea el nuevo banner global
		$data = array(
				'banner_global' => $global,
			);

		$this->banner->update(array('banner_id' => $id), $data);

		echo json_encode(array("status" => TRUE, "global" => $global));
	}	


	/*
	public function ajax_set_nivel_banner($nivel)
	{
		
		// Setea el nuevo banner global
		$data = array(
				'configuracion_valor' => $nivel,
			);

		$this->configuracion->update(array('configuracion_item' => 'banner_descuentos_nivel_activo'), $data);

		echo json_encode(array("status" => TRUE));
	}	
	*/
}