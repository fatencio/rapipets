<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Validations {

	public function valida_db_error($resultado){

		$data = array();
		$data['status'] = true;

		if (isset($resultado))
		{
			if ($resultado != "")
			{
				$data['error'] = $resultado;

	            if ($resultado['code'] != 1451) {

			       $data['mensaje'] = "No es posible eliminar este elemento.";
			    }
			    else {

				   $data['mensaje'] = "No es posible eliminar este elemento ya que está siendo utilizado.";
			    }

				$data['status'] = false;
			}
		}

		return $data;
	}
}