<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Email_geotienda {

	protected $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
    }


    public function nuevo_cliente($mail, $password, $link){

    	// Asunto
    	$subject = 'Bienvenido a GeoTienda';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial; font-size:18px'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:20px; color:#FCAC0D'>Te da la bienvenida</p>
							<p style='font-size:18px'>Estás a un paso de poder realizar compras y reservar turnos online, eligiendo la tienda más cercana a tu hogar las 24hs. del día.<br /><br />
							Tus datos de acceso:<br />
							<strong>Usuario:</strong> $mail<br />
							<strong>Contraseña:</strong> $password<br />
							<br /><br />
							<p style='font-size:16px'>Para activar tu cuenta presioná el siguiente link:<br /><br />

							<a style='color:#89AEDC; text-decoration:none' href='" . $link . "'>Activar Cuenta</a>
							<br /><br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_registro_cliente');
    	$bcc = $this->CI->config->item('email_bcc_registro_cliente');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


    public function nuevo_local($mail, $password){

    	// Asunto
    	$subject = 'Bienvenido a GeoTienda';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial; font-size:18px'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:20px; color:#FCAC0D'>Te da la bienvenida</p>
							<p style='font-size:18px'>Ya podés vender todos tus productos y servicios online.<br /><br />
							Tus datos de acceso:<br />
							<strong>Usuario:</strong> $mail<br />
							<strong>Contraseña:</strong> $password<br />
							<br />
							<p style='font-size:16px'>Te recomendamos cambiar tu contraseña al ingresar por primera vez.<br /><br />
							<a style='color:#89AEDC; text-decoration:none' href='" . $this->CI->config->item('url_app') . "'>&iexcl;Comenzá ahora!</a>
							<br /><br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_registro_local');
		$bcc = $this->CI->config->item('email_bcc_registro_local');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
	}


	public function venta(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$cliente,
		$cliente_dir,
		$cliente_tel,
		$forma_pago, 
		$envio, 
		$observaciones, 
		$pedido_nro, 
		$pedido_fecha, 
		$pedido_total,
		$pedido_detalle){

    	// Asunto
    	$subject = 'GeoTienda - Confirmación de pedido';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:10px; color:#FCAC0D'>Confirmación de pedido</p>
							<p style='font-size:22px; margin-bottom:20px; color:#89AEDC'>&iexcl;Felicitaciones! Tu pedido a $local ha sido exitoso. </p>
							<p style='font-size:16px'>DATOS DE LA TIENDA<br />
							<strong>Nombre:</strong> $local<br />
							<strong>Dirección:</strong> $local_dir<br />
							<strong>Teléfono:</strong> $local_tel<br />
							<br />						
							<p style='font-size:16px'>DATOS DEL PEDIDO<br />
							<strong>Forma de Pago:</strong> $forma_pago<br />
							<strong>Envío:</strong> $envio<br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							<br />
							</p>
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>					
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>  											
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>

					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>
					 
					<tr>
						<td style='font-size:14px;' align='left'>
							<div>
								<strong>Recordá:</strong>
								<ul>
									<li style='padding: 6px'>
										Compras con <strong>ENVIO A DOMICILIO:</strong> en el caso de cancelar el envío, puedes hacerlo desde tu cuenta en la pestaña 'Mis Pedidos' o comunicarte rápidamente por teléfono con la tienda antes que lo envié.
									</li>
									<li style='padding: 6px'>
										Compras con <strong>RETIRO EN LA TIENDA:</strong> debes retirarlo con el número de orden de compra dentro de las 24 horas, sino será cancelado el pedido. En el caso que tuvieras que cancelar el pedido, puedes hacerlo desde tu cuenta en la pestaña 'Mis Pedidos' o comunicarte rápidamente por teléfono con la tienda.
									</li>
								</ul>
							</div>
						</td>
					</tr>				
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function venta_aviso_local(
		$mail, 
		$cliente,
		$cliente_nombre,
		$cliente_dir,
		$cliente_tel,
		$forma_pago, 
		$envio, 
		$observaciones, 
		$pedido_nro, 
		$pedido_fecha, 
		$pedido_total,
		$pedido_detalle){

    	// Asunto
    	$subject = 'GeoTienda - ¡Felicitaciones, has vendido! ';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:10px; color:#FCAC0D'>$cliente_nombre realizó un pedido. </p>			
							<p style='font-size:16px'>DATOS DEL CLIENTE<br />
							<strong>Nombre:</strong> $cliente<br />
							<strong>Teléfono:</strong> $cliente_tel<br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							</p>

							<p style='font-size:16px'>DATOS DEL PEDIDO<br />
							<strong>Forma de Pago:</strong> $forma_pago<br />
							<strong>Envío:</strong> $envio<br />";

		// Si el envio es a domicilio, muestra direccion de envio
		if ($envio == 'A domicilio') 
			$message .= "<strong>Dirección de envío:</strong> $cliente_dir<br />";

		$message .= "		
							</p>
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>					
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>  											
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>

					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>
					 
					<tr>
						<td style='font-size:14px;' align='left'>
							<div>
								<strong>Recordá:</strong>
								<ul>
									<li style='padding: 6px'>
										Compras con <strong>ENVIO A DOMICILIO:</strong> en el caso de no poder concretar el envío o debe cancelarlo por algún motivo, por favor comunicarse rápidamente con el cliente desde tu cuenta en la pestaña 'Pedidos' o telefónicamente.
									</li>
									<li style='padding: 6px'>
										Compras con <strong>RETIRO EN LA TIENDA:</strong> en el caso de no poder recibir al cliente para hacer la entrega del artículo y tiene que cancelarlo o pasarlo para otro día, por favor comunicarse cuanto antes con el mismo desde tu cuenta en la pestaña 'Pedidos' o telefónicamente.   
									</li>
								</ul>
							</div>
						</td>
					</tr>				
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function turno(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones,
		$hs_cancelacion){

    	// Asunto
    	$subject = 'GeoTienda - Confirmación de turno';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:10px; color:#FCAC0D'>Confirmación de turno</p>
							<p style='font-size:22px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:18px'>Tu turno fue reservado con éxito para el día<br />
							<strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							
							</p>
							<br />
							<p style='font-size:18px'>DATOS DE LA TIENDA<br />
							<strong>Nombre:</strong> $local<br />
							<strong>Dirección:</strong> $local_dir<br />
							<strong>Teléfono:</strong> $local_tel<br /><br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong><br />
							Para cancelarlo o pasarlo para otro día, deberás hacerlo con " . $hs_cancelacion . "hs. de anticipación desde tu cuenta en la pestaña 'Mis Turnos'  o comunicarte rápidamente por teléfono con la tienda.</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function turno_aviso_local(
		$mail, 
		$fecha,
		$hora,
		$cliente,
		$cliente_nombre,
		$cliente_tel,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones,
		$hs_cancelacion){

    	// Asunto
    	$subject = 'GeoTienda - ¡Felicitaciones, te han reservado un turno!';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:30px; margin-bottom:10px; color:#FCAC0D'>$cliente_nombre reservó un turno</p>
							<p style='font-size:22px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:18px'>El turno fue reservado con éxito para el día<br />
							<strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							</p>
							<br />
							<p style='font-size:18px'>DATOS DEL CLIENTE<br />
							<strong>Nombre:</strong> $cliente<br />
							<strong>Teléfono:</strong> $cliente_tel<br /><br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong><br />
							En el caso de no poder realizar el turno y tengas que cancelarlo o pasarlo para otro día, deberás hacerlo con $hs_cancelacion hs de anticipación al mismo. Y puedes avisarle al cliente desde tu cuenta en la pestaña de 'Turnos' o telefónicamente.</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_turno_aviso_cliente(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$motivo,
		$link){

    	// Asunto
    	$subject = 'GeoTienda - Hubo un inconveniente con el turno que reservaste';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Turno cancelado</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:16px'>Tu turno para el día <strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							ha sido cancelado por la tienda.<br />
							</p>
							<p style='font-size:16px'><strong>Motivo:</strong> $motivo</p>
							<p style='font-size:16px'><strong>Tienda:</strong> $local</p>				
							<p style='font-size:14px'>Si querés sacar turno para el mismo servicio presioná el siguiente link:</p>
							<p style='font-size:16px'><a style='padding-top: 5px' href='" . $link ."'>ingresar a $local</a></p>
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus turnos cancelados <br />en la pestaña HISTORIAL de TU CUENTA</strong></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_turno_confirmacion_local(
		$mail, 
		$cliente, 
		$cliente_email, 
		$cliente_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$motivo){

    	// Asunto
    	$subject = 'GeoTienda - Turno cancelado';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Turno cancelado</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:16px'>El turno para el día <strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							ha sido correctamente cancelado.<br />
							</p>
							<br />
							<p style='font-size:16px'><strong>¡No te preocupes!<br /> lo invitaremos a que reserve un turno para otro día.</strong></p>			
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus turnos cancelados <br />en la pestaña VENTAS de TU CUENTA</strong></p>											
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_turno_aviso_local(
		$mail, 
		$cliente, 
		$cliente_email, 
		$cliente_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$motivo){

    	// Asunto
    	$subject = 'GeoTienda - Hubo un inconveniente con un turno reservado';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Turno cancelado</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:16px'>El turno para el día<br />
							<strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							ha sido cancelado por el cliente.<br />
							</p>
							<p style='font-size:16px'><strong>Motivo:</strong> $motivo</p>
							<br />
							<p style='font-size:14px'><strong>¡No te preocupes!<br /> lo invitaremos a que reserve un turno para otro día.</strong></p>
							<br />
							<p style='font-size:15px'><strong>Podrás encontrar tus turnos cancelados <br />en la pestaña VENTAS de TU CUENTA</strong></p>							
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_turno_confirmacion_cliente(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$motivo,
		$link){

    	// Asunto
    	$subject = 'GeoTienda - Turno cancelado';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Turno cancelado</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:16px'>Tu turno para el día <strong>$fecha</strong> a las <strong>$hora hs.</strong><br />
							ha sido correctamente cancelado.<br /></p>
							<p style='font-size:18px'><strong>Tienda:</strong> $local</p>

							<p style='font-size:14px'>Si querés sacar turno para el mismo servicio presioná el siguiente link:</p>
							<p style='font-size:16px'><a style='padding-top: 5px' href='" . $link ."'>ingresar a $local</a></p>
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus turnos cancelados <br />en la pestaña HISTORIAL de TU CUENTA</strong></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_venta_aviso_cliente(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$pedido_nro, 		
		$pedido_detalle,		
		$pedido_total,	
		$pedido_fecha,
		$motivo,
		$link){

    	// Asunto
    	$subject = 'GeoTienda - Hubo un inconveniente con un pedido de compra que realizaste';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Pedido cancelado</p>
							<p style='font-size:16px'><strong>Motivo:</strong> $motivo</p>
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>	<br />	
								Tienda: <strong>$local</strong>			
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>

					<tr>
						<td align='center'>                     
							<p style='font-size:14px'>Si querés realizar el mismo pedido presioná el siguiente link:</p>
							<p style='font-size:16px'><a style='padding-top: 5px' href='" . $link ."'>ingresar a $local</a></p>
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus pedidos cancelados <br />en la pestaña HISTORIAL de TU CUENTA</strong></p>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_venta_confirmacion_local(
		$mail, 
		$cliente, 
		$cliente_email, 
		$cliente_tel, 
		$pedido_nro, 		
		$pedido_detalle,		
		$pedido_total,	
		$pedido_fecha,
		$motivo){

    	// Asunto
    	$subject = 'GeoTienda - Pedido de compra cancelado';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Pedido cancelado</p>			
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>	
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>		

					<tr>
						<td align='center'>
							<p style='font-size:16px'>¡No te preocupes!<br /> lo invitaremos a que compre otro día.</p>			
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus pedidos cancelados <br />en la pestaña VENTAS de TU CUENTA</strong></p>	
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_venta_aviso_local(
		$mail, 
		$cliente, 
		$cliente_email, 
		$cliente_tel, 
		$pedido_nro, 
		$pedido_detalle,		
		$pedido_total,	
		$pedido_fecha,
		$motivo){

    	// Asunto
    	$subject = 'GeoTienda - Hubo un inconveniente con un pedido de compra que hizo un cliente';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Pedido cancelado</p>
							<p style='font-size:16px'><strong>Motivo:</strong> $motivo</p>
							<br /><br />
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>					
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>

					<tr>
						<td align='center'>
							<p style='font-size:16px'>¡No te preocupes!<br /> lo invitaremos a que compre otro día.</p>			
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus pedidos cancelados <br />en la pestaña VENTAS de TU CUENTA</strong></p>	
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cancela_venta_confirmacion_cliente(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$pedido_nro, 
		$pedido_detalle,		
		$pedido_total,
		$pedido_fecha,
		$motivo,
		$link){

    	// Asunto
    	$subject = 'GeoTienda - Pedido de compra cancelado';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>Pedido cancelado</p>
						</td>
					</tr>

					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>	<br />	
								Tienda: <strong>$local</strong>	
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>

					<tr>
						<td align='center'>                     
							<p style='font-size:14px'>Si querés realizar el mismo pedido presioná el siguiente link:</p>
							<p style='font-size:16px'><a style='padding-top: 5px' href='" . $link ."'>ingresar a $local</a></p>
							<br /><br />
							<p style='font-size:15px'><strong>Podrás encontrar tus pedidos cancelados <br />en la pestaña HISTORIAL de TU CUENTA</strong></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function consulta($motivo, $nombre, $mail, $telefono, $mensaje){

    	// Asunto
    	$subject = 'GeoTienda - Consulta: ' . $motivo;

    	// Mensaje
    	$message = "
    		<strong>Motivo:</strong> $motivo<br />
			<strong>Nombre o Empresa:</strong> $nombre<br />
			<strong>E-mail:</strong> $mail<br />
			<strong>Teléfono:</strong> $telefono<br />
			<strong>Mensaje:</strong> $mensaje<br />";    	

    	// Destinatarios
    	$destinatarios = $this->CI->config->item('email_cc_consulta');
    	$bcc = $this->CI->config->item('email_bcc_consulta');

    	return $this->enviar(
			 $destinatarios, 					    	// [TO] destinatarios
			 '', 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function pedido_registro($nombre, $mail, $telefono, $mensaje){

    	// Asunto
    	$subject = 'GeoTienda - Pedido de registro de la tienda: ' . $nombre;

    	// Mensaje
    	$message = "
			<strong>Nombre o Empresa:</strong> $nombre<br />
			<strong>E-mail:</strong> $mail<br />
			<strong>Teléfono:</strong> $telefono<br />
			<strong>Mensaje:</strong> $mensaje<br />";    	

    	// Destinatarios
    	$destinatarios = $this->CI->config->item('email_cc_pedido_registro');
    	$bcc = $this->CI->config->item('email_bcc_pedido_registro');

    	return $this->enviar(
			 $destinatarios, 					    	// [TO] destinatarios
			 '', 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }

    
	public function recordatorio_cliente_calificacion_venta(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$cliente,
		$cliente_dir,
		$cliente_tel,
		$forma_pago, 
		$envio, 
		$observaciones, 
		$link,
		$pedido_nro, 
		$pedido_fecha, 
		$pedido_total,
		$pedido_detalle){

    	// Asunto
    	$subject = 'GeoTienda - ¿Cómo te fue con la compra?';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:20px; color:#FCAC0D'>¿Cómo te fue con la compra realizada en la tienda $local? </p>
						</td>
					</tr>

					<tr>
						<td align='center'>
							<p style='font-size:18px'>Haz clic en el siguiente enlace para calificar a la tienda:<br /><br />
							<a href='" . $link ."'> calificar tienda </a>
						</td>
					</tr>

					<tr>
						<td align='center'>					
							<p style='font-size:16px; margin-top: 10px'>DATOS DEL PEDIDO<br />
							<strong>Forma de Pago:</strong> $forma_pago<br />
							<strong>Envío:</strong> $envio<br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							<br />
							</p>
						</td>
					</tr>


					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>					
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>
					 			
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }
	

	public function recordatorio_local_calificacion_venta(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$cliente,
		$cliente_dir,
		$cliente_tel,
		$forma_pago, 
		$envio, 
		$observaciones, 
		$link,
		$pedido_nro, 
		$pedido_fecha, 
		$pedido_total,
		$pedido_detalle){

    	// Asunto
    	$subject = 'GeoTienda - ¿Cómo te fue con la venta?';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:20px; color:#FCAC0D'>¿Cómo te fue con la venta realizada al cliente $cliente? </p>
						</td>
					</tr>

					<tr>
						<td align='center'>
							<p style='font-size:18px'>Haz clic en el siguiente enlace para avisar si la venta fue concretada o no:<br /><br />
							<a href='" . $link ."'> notificar venta </a>
						</td>
					</tr>

					<tr>
						<td align='center'>					
							<p style='font-size:16px; margin-top: 10px'>DATOS DEL PEDIDO<br />
							<strong>Forma de Pago:</strong> $forma_pago<br />
							<strong>Envío:</strong> $envio<br />
							<strong>Mensaje opcional:</strong> $observaciones<br />
							<br />
							</p>
						</td>
					</tr>


					<!-- DETALLES PEDIDO -->
					<tr style='background-color: #EEEEEE; margin: 10px; font-size: 14px'>
						<td >
							<div style='padding:4px'>
								Orden de compra: <strong>$pedido_nro</strong><br />
								Fecha: <strong>$pedido_fecha</strong>					
							</div>
							
	                        <table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial;' >
								<tr >
									<td colspan='4'><hr /></td>
								</tr>  							
								
								<tr>
									<td><strong>Detalle</strong></td>
									<td ><strong>Cantidad</strong></td>       
									<td ><strong>Precio</strong></td>  	
									<td ><strong>Total</strong></td>	
								</tr>";		

								// Recorre lineas del pedido
								foreach ($pedido_detalle as $linea) {	

									$message .= 			
										"<tr>
											<td>" . $linea->marca . ' ' . $linea->articulo . " (" . $linea->presentacion . ")</td>
											<td>" . $linea->cantidad . "</td>       
											<td>$" . $linea->precio . "</td>  	
											<td>$" . $linea->cantidad * $linea->precio . "</td>  											
										</tr>";
								}	

								$message .= 
								"<tr >
									<td colspan='4'><hr /></td>
								</tr>  	
								
								<tr >
									<td style='text-align: right;' colspan='4'>
										<strong>Total  $" .  $pedido_total . "</strong>
									</td>
								</tr>                                                             	
							</table>
                        </td>
                     </tr>
					 			
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function recordatorio_cliente_calificacion_turno(
		$mail, 
		$local, 
		$local_dir, 
		$local_tel, 
		$link,
		$fecha,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones){

    	// Asunto
    	$subject = 'GeoTienda - ¿Cómo te fue con el servicio?';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:20px; margin-bottom:10px; color:#FCAC0D'>¿Cómo te fue con el servicio brindado por la tienda $local? </p>
						</td>

					</tr>

					<tr>
						<td align='center'>
							<p style='font-size:18px'>Haz clic en el siguiente enlace para calificar a la tienda:<br /><br />
							<a href='" . $link ."'> calificar tienda </a>
						</td>
					</tr>

					<tr>
						<td align='center'>		
							<p style='font-size:18px; margin-top: 10px'>DATOS DEL SERVICIO</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<strong>Fecha servicio:</strong> $fecha</p>
							<strong>Mensaje opcional:</strong> $observaciones<br />
							
							</p>
						</td>
					</tr>
					 			
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function recordatorio_local_calificacion_turno(
		$mail, 
		$cliente, 
		$link,
		$fecha,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones){

    	// Asunto
    	$subject = 'GeoTienda - ¿Cómo te fue con el servicio?';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:20px; margin-bottom:10px; color:#FCAC0D'>¿Cómo te fue con el servicio brindado al cliente $cliente? </p>
						</td>

					</tr>

					<tr>
						<td align='center'>
							<p style='font-size:18px'>Haz clic en el siguiente enlace para avisar si el servicio se concretó o no:<br /><br />
							<a href='" . $link ."'> notificar servicio </a>
						</td>
					</tr>

					<tr>
						<td align='center'>		
							<p style='font-size:18px; margin-top: 10px'>DATOS DEL SERVICIO</p>
							<p style='font-size:20px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<strong>Fecha servicio:</strong> $fecha</p>
							<strong>Mensaje opcional:</strong> $observaciones<br />
							
							</p>
						</td>
					</tr>
					 			
					<tr>
						<td style='font-size:20px;' align='center'>
						</td>
					</tr>

					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function recordatorio_cliente_turno(
		$mail, 
		$cliente_nombre,
		$local, 
		$local_dir, 
		$local_tel, 
		$fecha,
		$hora,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones,
		$hs_cancelacion){

    	// Asunto
    	$subject = 'GeoTienda - Recordatorio de turno';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $cliente_nombre! Se aproxima el turno que reservaste.</p>
							<p style='font-size:22px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:18px'>Tu turno fue reservado con éxito para el día<br />
							<strong>$fecha</strong> a las <strong>$hora hs.</strong></p><br />
							
							<p style='font-size:18px'>
							<strong>Tienda:</strong> $local<br />
							<strong>Teléfono:</strong> $local_tel<br /><br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong><br />
							Para cancelarlo o pasarlo para otro día, deberás hacerlo con " . $hs_cancelacion . "hs. de anticipación desde tu cuenta en la pestaña 'Mis Turnos'  o comunicarte rápidamente por teléfono con la tienda.</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function recordatorio_local_turno(
		$mail, 
		$fecha,
		$hora,
		$local,
		$cliente,
		$cliente_nombre,
		$cliente_tel,
		$servicio,
		$servicio_item,
		$precio,
		$observaciones,
		$hs_cancelacion){

    	// Asunto
    	$subject = 'GeoTienda - Recordatorio de turno';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $local! Se aproxima un turno que te reservaron.</p>
							<p style='font-size:22px; margin-bottom:20px; color:#89AEDC'>$servicio - $servicio_item ($$precio)</p>
							<p style='font-size:18px'>El turno fue reservado con éxito para el día<br />
							<strong>$fecha</strong> a las <strong>$hora hs.</strong></p><br />
							
							<br />
							<p style='font-size:18px'>DATOS DEL CLIENTE<br />
							<strong>Nombre:</strong> $cliente<br />
							<strong>Teléfono:</strong> $cliente_tel<br />
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong><br />
							En el caso de no poder realizar el turno y tengas que cancelarlo o pasarlo para otro día, deberás hacerlo con $hs_cancelacion hs de anticipación al mismo. Y puedes avisarle al cliente desde tu cuenta en la pestaña de 'Turnos' o telefónicamente.</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function cliente_nueva_vacuna(
		$mail, 
		$cliente_nombre,
		$local, 
		$mascota_nombre,
		$fecha_vacuna,
        $vacuna,
		$fecha_proxima_vacuna){

    	// Asunto
    	$subject = 'GeoTienda - Se agregaron vacunas a la cartilla de tu mascota';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $cliente_nombre! $local agregó en la cartilla de vacunas de $mascota_nombre la vacuna que le aplicó.</p>

							<br/>
							
							<p style='font-size:18px'>Fecha de aplicación: <strong>$fecha_vacuna</strong></p>
							<p style='font-size:18px'>Vacuna: <strong>$vacuna</strong></p>
							<p style='font-size:18px'>Próxima Vacuna: <font color=#FCAC0D> $fecha_proxima_vacuna </font></p> 
						
						    <br/>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong> aplicale en término las vacunas. Tu mascota depende de vos para estar sana.</strong></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_vacuna');
		$bcc = $this->CI->config->item('email_bcc_vacuna');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


    public function recordatorio_cliente_vacuna
    (
		$mail, 
		$cliente_nombre,
		$local, 
		$mascota_nombre,
		$fecha_vacuna,
		$fecha_proxima_vacuna,
        $vacuna
    )
    {
    	// Asunto
    	$subject = 'GeoTienda - Recordatorio de vacuna.';

    	// <p style='font-size:18px'>Próxima Vacuna: <font color=#FCAC0D> $fecha_proxima_vacuna </font></p> 

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $cliente_nombre! Se aproxima la próxima vacuna que le toca a $mascota_nombre.</p>

							<br/>
							
							<p style='font-size:18px'>Tienda: <strong>$local</strong></p>
							<p style='font-size:18px'>Fecha de aplicación: <strong>$fecha_vacuna</strong></p>
							<p style='font-size:18px'>Vacuna: <strong>$vacuna</strong></p><br/>

							<p style='font-size:20px'>Próxima vacuna: <strong style='color: #F3B760;'>$fecha_proxima_vacuna</strong></p>							
						
						    <br/>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong> aplicale en término las vacunas. Tu mascota depende de vos para estar sana.</p>
						</td>
					</tr>
					<tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_vacuna');
		$bcc = $this->CI->config->item('email_bcc_vacuna');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


    public function recordatorio_local_vacuna
    (
		$mail, 
		$cliente,
		$local, 
		$mascota_nombre,
		$fecha_vacuna,
		$fecha_proxima_vacuna,
        $vacuna
	){

    	// Asunto
    	$subject = 'GeoTienda - Recordatorio de vacuna.';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $local! Se aproxima la fecha para vacunar a la mascota de un cliente.</p>

							<br/>
							
							<p style='font-size:18px'>Cliente: <strong>$cliente</strong></p><br/>

							<p style='font-size:18px'>Mascota: <strong>$mascota_nombre</strong></p>
							<p style='font-size:18px'>Fecha de aplicación: <strong>$fecha_vacuna</strong></p>
							<p style='font-size:18px'>Vacuna: <strong>$vacuna</strong></p><br/>

							<p style='font-size:20px'>Próxima vacuna: <strong style='color: #F3B760;'>$fecha_proxima_vacuna</strong></p>							
						    <br/>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong> una vez aplicada la vacuna a la mascota, completá en su cartilla de vacunas los datos correspondientes y la fecha de la próxima vacuna que le toca.</p>
						</td>
					</tr>
					<tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_vacuna');
		$bcc = $this->CI->config->item('email_bcc_vacuna');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


    public function local_nueva_mascota(		
		$mail, 
		$cliente_nombre,
		$local, 
		$mascota_nombre){

    	// Asunto
    	$subject = 'GeoTienda - Tenés un nuevo cliente.';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:28px; margin-bottom:10px; color:#FCAC0D'>¡Hola $local! Tenés un nuevo cliente.</p>

							<br/>
							
							<p style='font-size:18px'><strong>$cliente_nombre</strong> asignó la cartilla de vacunas de <strong> $mascota_nombre </strong> para que lo atiendan con sus controles. </p>
							
						    <br/>
							
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:14px;' align='left'>		
							<p style='padding: 6px'><strong>Recordá:</strong> una vez aplicada la vacuna a la mascota, completá en su cartilla de vacunas los datos correspondientes y la fecha de la próxima vacuna que le toca.</p>
						</td>
					</tr>
					<tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_vacuna');
		$bcc = $this->CI->config->item('email_bcc_vacuna');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function comentario_venta_aviso_cliente(
		$mail, 
		$cliente_nombre,
		$local, 
		$venta,
        $comentario,
		$link){

    	// Asunto
    	$subject = "GeoTienda - ¡Tenés un mensaje de la tienda $local!";

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:26px; margin-bottom:10px; color:#FCAC0D'>¡Hola $cliente_nombre! $local te ha enviado un mensaje por el pedido de compra que realizaste.</p>

							<br/>
							
							<p style='font-size:20px'><strong>Compra: $venta</strong></p>

						</td>
					</tr>
					<tr>
						<td style='font-size:16px;' align='center'>		
							<p style='padding: 6px'><img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/comentario.png'/>&nbsp;&nbsp;&nbsp;'$comentario'</p>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:18px; margin-bottom:15px'><a href='" . $link ."'>Responder</a></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function comentario_venta_aviso_local(
		$mail, 
		$cliente_nombre,
		$local, 
		$venta,
        $comentario,
		$link){

    	// Asunto
    	$subject = "GeoTienda - ¡Tenés un mensaje de un cliente!";

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:26px; margin-bottom:10px; color:#FCAC0D'>¡Hola $local! Tenés un mensaje de $cliente_nombre.</p>

							<br/>
							
							<p style='font-size:20px'><strong>Pedido: $venta</strong></p>

						</td>
					</tr>
					<tr>
						<td style='font-size:16px;' align='center'>		
							<p style='padding: 6px'><img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/comentario.png'/>&nbsp;&nbsp;&nbsp;'$comentario'</p>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:18px; margin-bottom:15px'><a href='" . $link ."'>Responder</a></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_venta');
		$bcc = $this->CI->config->item('email_bcc_venta');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function comentario_turno_aviso_cliente(
		$mail, 
		$cliente_nombre,
		$local, 
		$turno,
        $comentario,
		$link){

    	// Asunto
    	$subject = "GeoTienda - ¡Tenés un mensaje de la tienda $local!";

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:26px; margin-bottom:10px; color:#FCAC0D'>¡Hola $cliente_nombre! $local te ha enviado un mensaje por el turno que reservaste.</p>

							<br/>
							
							<p style='font-size:20px'><strong>Turno: $turno</strong></p>

						</td>
					</tr>
					<tr>
						<td style='font-size:16px;' align='center'>		
							<p style='padding: 6px'><img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/comentario.png'/>&nbsp;&nbsp;&nbsp;'$comentario'</p>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:18px; margin-bottom:15px'><a href='" . $link ."'>Responder</a></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	public function comentario_turno_aviso_local(
		$mail, 
		$cliente_nombre,
		$local, 
		$turno,
        $comentario,
		$link){

    	// Asunto
    	$subject = "GeoTienda - ¡Tenés un mensaje de un cliente!";

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:26px; margin-bottom:10px; color:#FCAC0D'>¡Hola $local! Tenés un mensaje de $cliente_nombre.</p>

							<br/>
							
							<p style='font-size:20px'><strong>Turno: $turno</strong></p>

						</td>
					</tr>
					<tr>
						<td style='font-size:16px;' align='center'>		
							<p style='padding: 6px'><img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/comentario.png'/>&nbsp;&nbsp;&nbsp;'$comentario'</p>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:18px; margin-bottom:15px'><a href='" . $link ."'>Responder</a></p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>					
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_turno');
		$bcc = $this->CI->config->item('email_bcc_turno');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


    public function reset($mail, $link)
    {
    	// Asunto
    	$subject = 'GeoTienda - Restablecer contraseña';

    	// Mensaje
    	$message = "
    		<div style='border: 2px solid #89AEDC; max-width: 500px'>
				<table width='100%' align='center' cellspacing='0' cellpadding='10' border='0' style='font-family:Arial; font-size:18px'>
					<tr>
						<td align='center' bgcolor='#000000'>
							<img align='center' src='" . $this->CI->config->item('url_app') . "assets/img/frontend/logo-geotienda.png'/>
						</td>
					</tr>
					<tr>
						<td align='center'>
							<p style='font-size:26px; margin-bottom:20px; color:#FCAC0D'>Hemos recibido una petición para restablecer la contraseña de tu cuenta.</p>
							<p style='font-size:18px'>Si hiciste esta petición, haz clic en el siguiente enlace; si no hiciste esta petición puedes ignorar este correo.<br /><br />
							<strong>Enlace para restablecer tu contraseña</strong><br>
							<a href='" . $link ."'> Restablecer contraseña </a>
							</p>
						</td>
					</tr>
					<tr>
						<td style='font-size:20px; color:white' align='center'>
							<div style='background: #89AEDC; padding:20px; margin: 6px'>Gracias por elegir GeoTienda</div>
						</td>
					</tr>
				</table>
			</div>";    	

    	// Copia
    	$cc = $this->CI->config->item('email_cc_registro_cliente');
		$bcc = $this->CI->config->item('email_bcc_registro_cliente');

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }    


	public function test($mail)
	{
    	// Asunto
    	$subject = 'GeoTienda - Test';

    	// Mensaje
    	$message = 'test';
    	
    	// Copia
    	$cc = 'fatencio@gmail.com';
    	$bcc = 'fatencio@gmail.com';

    	return $this->enviar(
			 $mail, 					    			// [TO] cliente que se registra
			 $cc, 					    				// [CC] array de destinatarios adicionales
			 $bcc, 					    				// [BCC] array de destinatarios ocultos
			 $subject, 									// asunto
			 $message);									// mensaje
    }


	private function enviar($to, $cc, $bcc, $subject, $message)
	{
		$error = '';

		$mail_config = array (
		  'protocol' => 'smtp',
		  'smtp_host' => 'ssl://cloud1039.hostgator.com', 
		  'smtp_port' => 465,
		  'smtp_user' => 'info@geotienda.com',
		  'smtp_pass' => 'jUrH5%378*', 
		  'smtp_timeout' => 100,
		  'mailtype' => 'html',
		  'charset' => 'utf-8',
		  'wordwrap' => TRUE
		);

		$this->CI->load->library('email');

		$this->CI->email->clear();
		$this->CI->email->initialize($mail_config);
		$this->CI->email->set_mailtype("html");

		$this->CI->email->from($this->CI->config->item('email_from'), 'GeoTienda');
		$this->CI->email->to($to); 
		$this->CI->email->cc($cc); 
		$this->CI->email->bcc($bcc); 

		$this->CI->email->subject($subject);
		$this->CI->email->message($message);	

		try{

			// Amplia temporalmente el tiempo maximo de ejecución de un script a 5 minutos para evitar el error 
			// 'Maximum execution time of 30 seconds exceeded' al enviar un mail
			set_time_limit(300);

			$r = $this->CI->email->send(FALSE);

			if (!$r) {
			  	ob_start();
			  	$this->CI->email->print_debugger();
			  	$error = ob_end_clean();
			  	$errors[] = $error;
			}

		}
		catch (Exception $e)
		{
			$error = $e->getMessage();
		}

		return $error;

        //$this->session->set_flashdata("email_sent",$this->email->send()); 
	}
}