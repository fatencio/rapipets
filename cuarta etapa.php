ANTES DE PRODUCCION
===================


  - UNA VEZ BIEN PROBADO EN PRODUCCION -> Quitar querylog


  SI ALCANZO
  ----------
    - input en BUSCAR al dar enter / tab en LOCALIDAD (landing)

   	https://github.com/select2/select2/issues/1456
	https://gist.github.com/jgdev/00e1e696231d40741ee1cefb549d5ad2
	https://stackoverflow.com/questions/29543129/send-form-on-enter-key-press-with-select2


  


SQL
---
v_local_animales


CONFIG
------


TICKETS PENDIENTES
------------------





IDEAS PARA FILTROS
------------------
 - Post (sin ajax) en el link a cada local (m libre, pedidos ya)
 - 'Recordar' filtros al ingresar a un local
 - 'Recordar' filtros al volver a la lista de locales
 - Opción para limpiar todos los filtros
  

  

CUARTA ETAPA
============

	#12 Cambio de texto: "Local" por "Tienda" 			 		 1 hs  
	#13 Tipo de filtros 										13 hs      
	#17 Presentaciones x Rubros 					 	 		 7 hs  		
	#8  Locales destacados 										10 hs 	   
	#7	Veterinarias - Emergencias 24hs 	 					 6 hs 		
	#3  ABM Medios de Pago  (integrado con Mercadopago)  		12 hs
	#2 	Exportación del ABM de Ventas 		 					 7 hs 		
	#5  ABM de Localidades 										 3 hs
	#19 Crear nuevo producto (Tienda) 							 6 hs

	#9  Nueva pantalla: Beneficios para el cliente 		 		 3 hs 
	#14 Audit Trail 									 		 5 hs
	#15 INSTRAGRAM  									 		 1 hs
	------------------------------------------------------------------ 
	TOTAL 														73 hs  

														-> $21.900  ($300 x hora)
														-> 24 días hábiles (3,5 semanas)



#12 Cambio de texto: "Local" por "Tienda" 			21/06		 1 h  						
													23/06		 1 h  		+ 1

#13 Tipo de filtros 								12/06		 4 hs   
													13/06		 5 hs
													14/06		 5 hs
													15/06		 3 hs
													19/06		 5 hs
													20/06	 	 2 hs
													21/06	 	 4 hs
													22/06		 1 h
													26/06		 4 hs
													27/06		 3 hs
													28/06		 3 hs
													29/06		 3 hs	
													30/06		 4 hs												
													03/07		 2 hs
													04/07		 2 hs
													05/07		 1 h
													07/07		 1 h
													11/07		 1 h 		
													-----------------
													TOTAL 		 53 hs		+ 40


#17 Presentaciones x Rubros 					 	22/06		 2 hs
													23/06		 4 hs 		
													-----------------
													TOTAL 		 6 hs		- 4



#8  Locales destacados 								04/07		 1 h
													05/07		 1 h
													06/07		 2 hs
													07/07		 1 h
													08/07		 2 hs
													10/07		 2 hs
													11/07		 1 h
													08/08		 1 h 		
													-----------------
													TOTAL 		 11 hs		+ 1


#7	Veterinarias - Emergencias 24hs 	 			11/07		 1 h 
													12/07		 4 hs													
													13/07		 1 h 	
													-----------------
													TOTAL 		 6 hs		+ 0


#3  ABM Medios de Pago  (integrado con Mercadopago) 17/07		 1 h
													18/07		 1 h
													19/07		 4 hs
													20/07  		 3 hs
													21/07  		 6 hs
													22/07  		 1 h
													-----------------
													TOTAL 		 16 hs		+ 4

#2 	Exportación del ABM de Ventas 		 			24/07  		 2 hs
								 		 			25/07  		 1 hs
								 		 			26/07  		 2 hs
													-----------------
													TOTAL 		 5 hs		- 2



#5  ABM de Localidades (drop-down) 					28/07 		 4 hs 		+ 1


#19 Crear nuevo producto (Tienda) 					31/07 		 3 hs 		- 3			 

#9  Nueva pantalla: Beneficios para el cliente 		01/08	 	 2 hs 		- 1

#14 Audit Trail 									04/08	 	 5 hs 		
				 									05/08	 	 1 h       	
				 									08/08	 	 1 h       	+ 2

#15 INSTRAGRAM  									09/08		 1 h 		+ 0







QUINTA ETAPA
============

#6 	Nueva campo "Fecha de alta" 		 					 2 hs
#10 Foto de perfil para Clientes					 		 4 hs     	
#11 Locales Favoritos 										 9 hs       
#16 Eliminación lógica de registros antiguos 				 3 hs
#18 BACK END - Eliminación de registros del Tab Ventas 		 4 hs
------------------------------------------------------------------ 
TOTAL 														22 hs  



 		
BACKEND
-------

1- Tener un exportable del ABM Ventas en un excel. En el excel deberá aparecer la info según lo filtrado.

PARA VENTAS
NRO - FECHA - FORMA DE PAGO - ESTADO - CLIENTE - ARTICULO - CANTIDAD - PRECIO U. - TOTAL


PARA TURNOS
NRO - FECHA - FORMA DE PAGO - ESTADO - CLIENTE - SERVICIO - PRECIO 

En ambos casos (Ventas y Turnos) la info exportada tendrá un totalizador al final



En el mismo ABM Ventas tener una opción para eliminar registros de venta 
 - Borrado lógico
 - De a uno, o seleccionando varios registros



2.1- Tener un ABM de 2 niveles de formas de pago (similar a ABM Servicios / Items Servicio).

 ABM formas de pago: (Efectivo, Tarjetas de Crédito, Tarjetas de Débito) 
 ABM Item: (Visa, Mastercad, American, etc...) 

 - Crear tablas		pago  pago_item   	0,5 h
 - Crear ABM padre						1 h 
 - Crear ABM hijo						1,5 h
 - Revisión y ajustes					2 h
 ---------------------------------------------
 TOTAL									5 HS




2.2- Mercado Pago lo dejaríamos Stand by, por favor sacar esta opción (si pueder activado/desactivado desde el backend mucho mejor).

- Mercado Pago debería poder activarse / desactivarse globalmente ya que la idea es mas adelante es poner pagos online para todos por igual.

POR CONFIG
- Crear entrada en CONFIG						0,5 h
- Revisión y ajustes							0,5 h
 --------------------------------------------------
 TOTAL											1 HS


POR BACKEND
- Crear vista en Admin 'Configuración'			0,5 h
- Crear item en vista para Mercadopago			1 h
- Crear controller y model para configuración 	2 h
- Revisión y ajustes							1,5 h
 --------------------------------------------------
 TOTAL											5 HS




3.1- ABM de Localidades para restriniguir los resultados de la geolocalización



4- Agregar en el Tab Clientes y Locales una columna "Fecha de alta"

Este dato se mostraría en la vista principal

- Crear campo en tablas 				0,5 h
- Mostrar dato en tablas				0,5 h
- Revisión y ajustes					1 h
 ---------------------------------------------
 TOTAL									2 HS




5- Usar el teléfono alternativo para cargar el teléfono de emergencias. Esto será aplicable solamente para los locales tipo Veterinaria. Si la Tienda cambia desde su Dashboard el numero... en nuestro administrador debe cambiar también.

 - No es un dato obligatorio para veterinarias, solo lo ingresarían los que hagan emergencias. 
  - El campo debería llamarse 'Teléfono de Emergencia' y lo ingresaríamos desde el Back- End ya que nosotros primero les consultamos si hace emergencia... y en el caso que si haga ahí lo ingresamos.




6- Locales destacadaos, tendremos la posbilidad de marcar los locales destacados. Debemos tener un campo para ingresar el precio. 

- Crear campos en tabla local 		0,5 h
- Modificar ABM Locales 			1,5 h
- Revisión y ajustes				1 h
 -----------------------------------------
 TOTAL								3 HS




FRONTEND
--------

1- Nueva pantalla: el link del home "Registrala acá" irá a una nueva pantalla donde mostraremos los beneficios del cliente.
Por supuesta cambiaremos el texto del hyperlink.

Qué cambia con respecto a lo que está ahora? Actualmente el link 'Registrala acá' abre la página 'contacto_local' donde se muestra una imagen con los beneficios de Geotienda y un formulario de contacto.

En esta pantalla pondríamos los (Beneficios para los usuarios) cambiaría el titulo y la info de adentro... ya que mas abajo esta la pantalla de ''Suma tu comercio a GeoTienda'' de esta manera damos información para ambos bandos. 


- Crear nueva vista y link  		2 h
- Revisión y ajustes				1 h
 -----------------------------------------
 TOTAL								3 HS





2.1- El local pordá elegir los medios de pagos en función de lo creado en el ABM de pagos.

- Modificar vista Mis Datos del local  		2 h
- Modifica controller y model				3 h
- Revisión y ajustes						2 h
 -----------------------------------------------
 TOTAL										7 HS



2.2- El cliente verá los medios de pago de cada local. gráfico similar al que tiene (Pedidos Ya)

Al momento de finalizar el pedido, cómo aparecerían las opciones que tiene disponible el local para pagar?

Por ej, si está habilitada la opción 'Tarjeta de débito', aparecería como un item aparte, o junto a 'Efectivo'?

DUDAS PENDIENTES




2.3- Mercado Pago aparecerá solamente si está activado desde el Backend.

- Mostrar / ocultar opción de pago en checkout  0,5 h
- Revisión y ajustes							0,5 h
 ----------------------------------------------------
 TOTAL											1 H



3- El cliente solo podrá elegir entre las localidades precargadas (similar a PedidosYa)

DUDAS PENDIENTES
- Va esta opción o la 4


4- Agregar el autocompletar de google.

DUDAS PENDIENTES
 - Va esta opción o la 3





5- Dashboard usuario: el usuario podrá selecionar una foto para su perfil manteniendo los avatars.

El usuario pueda subir una foto suya.... o bien pueda usar cualquier avatar que esta actualmente en el sitio.

- Modificar dashboard cliente para ABM de avatar  2 h
- Modificar controller y model					  2 h
- Revisión y ajustes							  1 h
 ----------------------------------------------------
 TOTAL											  5 HS




6- Comercios favoritos.

Qué implica este punto?

la idea seria que si un usuario compra seguido en una misma tienda.... ya le quede como ''Favorito'' esa tienda o bien también pueda repetir las misma compra o reserva que efectuó la ultima ves.

- Qué condiciones hacen que un local pasa a ser 'favorito' para un cliente?
German: Por lo general un cliente compra en una misma tienda siempre...exepto que no le guste algo y lo valla a buscar a otra tienda

- Un local que pasa a ser 'favorito', puede dejar de serlo más adelante'? bajo qué condiciones?
German: Si puede dejarlo de ser en cualquier momento para el comprado, bajo ninguna restricción.

- En que vista / vistas aparecerían los locales favoritos de un cliente y cómo aparecerían?
German: Al momento de Loguearse el comprador vería a la ''Tienda favorita'' en la pantalla 2 y 3 al lado del nombre de la tienda un corazón que cuando es seleccionado por primera ves queda en rojo.... como diciendo que esa tienda es favorita. Dejo un ejemplo.


- En que vista / vistas aparecerían las opciones para repetir la misma compra o reserva y cómo aparecerían?
German: Por el momento este punto lo dejamos.... es complejo ya que por estos tiempos los negocios cambian seguido los precios.


DUDAS PENDIENTES
 - El usuario es el que define que un local es favorito o no al hacer clic en el corazón?
 - Además del corazón encendido, qué otra diferencia visual o funcional tendrán los locales favoritos? (x ej aparecer primero en los listados)


Pantalla 2
 - En el listado de locales el usuario podrá indicar que un local es favorito haciendo clic en el corazón
 - Similarmente, el usuario podrá indicar que un local ya no es favorito haciendo clic en el corazón en un local que ha sido indicado como favorito
- Visualmente un local favorito tiene el corazón de color, y no favorito sin color

Menú Principal
 - Dentro del menú principal del usuario (el que tiene los items Mis Pedidos, Mis Turnos, etc) se agregará un item 'Mis Favoritos'. Al pasar el mouse por encima o hacer clic desplegará un submenú con una lista de los locales favoritos del usuario.
- Al hacer clic en el nombre del local, llevará al usuario a la pantalla 3 del local correspondiente.









7- Reemplazar en todos los lugares que figure "Local" por "Tienda"

Hay varias referencias a 'negocio'... las cambio por 'tienda'? -> solo suplantar los que dicen LOCAL por Tienda.

- Reemplazar textos						  0,5 h
- Revisión y ajustes					  0,5 h
 ----------------------------------------------
 TOTAL									  1 HS



8.1- En la pantalla 2 y 3 se verán los locales con el teléfono de emergencias.  debe aparecer un cartel o botón que adentro aparezca el numero y con un mensaje como avisando previamente si hace ''Visita a domicilio'' o ''Debe llevar el animal a la veterinaria''.

- Modificar vistas 2 y 3				  2 h
- Revisión y ajustes					  1 h
 ----------------------------------------------
 TOTAL									  3 HS




8.2- Este teléfono debe ser configurable desde el dashboard local.




9- Los locales destacados estarán marcados con un cartel.

 - En la pantalla 2 la primer opción de ordenamiento sea 'Tiendas Destacadas' , seguido de 'Menor Distancia'




9.1 - En el dashboard del local agregar opción 'Aumentar Exposición' 

 - En el (Dashboard Tienda) en algún lugar tenga un botón 'Aumentar Exposición  y cuando lo seleccionan le aparezca el modal ese con la info de: *Tienda Destacada, *Publicidad extra en redes sociales, y al lado aparezca el campo con el precio que (Seria editable desde el Back-End). De esa manera si el local quiere ser (Tienda Destacada) puede desde su dashboard solicitarlo.
 
 - Cuando el local hace clic en 'Confirmar' enviar una notificación al Back- End.





10- Dejar los filtros como antes (independientes mostrando las cantidades), es factible sin desmejorar la performance del sitio? ---> 
ETAPA 3

- Investigar mejor alternativa			  2 h
- Modificar vistas 2 y 3				  8 h
- Revisión y ajustes					  4 h
 ----------------------------------------------
 TOTAL									  14 HS

DUDAS PENDIENTES
- Idea: hacer que los filtros seleccionados en la pantalla 2 se repliquen en la pantalla 3. Por ej, si el usuario filtró para ver solo los locales que venden Alimento para Gato, que en la pantalla del local seleccionado muestre solo los productos filtrados por Alimento para Gato.



11 - Agregar la red social INSTRAGRAM en el home.

- Agregar opcion en home				  0,5 h
- Revisión y ajustes					  0,5 h
 ----------------------------------------------
 TOTAL									  1 HS




12 - 

En el Dashboard Tienda (TAB Ventas): Los datos le deben permanecer 12 meses... después pasado esos debe borrarse automáticamente los que son de mas antigüedad. (borrado lógico)

En el Dashboard Usuarios (TAB Historial): Los datos deben permanecer 12 meses... después pasado esos debe borrarse automáticamente los que son de mas antigüedad. (borrado lógico)


BASE DE DATOS
----------------
1- Registrar fecha y usuario de alta-baja-modificacion-suspensión de usuario (cliente + local)

